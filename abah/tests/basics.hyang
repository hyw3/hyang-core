library(abah)

options(keep.source=TRUE)

x <- 2
stopifnot(eval(compile(quote(x + 1))) == 3)

checkCode <- function(expr, code, optimize = 2) {
    v <- compile(expr, options = list(optimize = optimize))
    d <- .Internal(decodeAbah(v))[[2]][-1]
    dd <- as.integer(eval(substitute(code), getNamespace("abah")))
    identical(d, dd)
}
x <- 2
stopifnot(checkCode(quote(x + 1),
                    c(GETVAR.OP, 1L,
                      LDCONST.OP, 2L,
                      ADD.OP, 0L,
                      RETURN.OP)))
f <- function(x) x
checkCode(quote({f(1); f(2)}),
          c(GETFUN.OP, 1L,
            PUSHCONSTARG.OP, 3L,
            CALL.OP, 4L,
            POP.OP,
            GETFUN.OP, 1L,
            PUSHCONSTARG.OP, 6L,
            CALL.OP, 7L,
            RETURN.OP))


f <- function(...) list(...)
stopifnot(identical(f(1, 2), abahfun(f)(1, 2)))

f <- function(...) list(x = ...)
stopifnot(identical(f(1, 2), abahfun(f)(1, 2)))


f <- function(x) substitute(x)
g <- function() f(1 + 2)
v1 <- g()
f <- abahfun(f)
g <- abahfun(g)
v2 <- g()
stopifnot(identical(v1, v2))

sr <- function(x) {
    n <- length(x)
    i <- 1
    s <- 0
    repeat {
        if (i > n) break
        s <- s + x[i]
        i <- i + 1
    }
    s
}
sw <- function(x) {
    n <- length(x)
    i <- 1
    s <- 0
    while (i <= n) {
        s <- s + x[i]
        i <- i + 1
    }
    s
}
sf <- function(x) {
    s <- 0
    for (y in x)
        s <- s + y
    s
}
src <- abahfun(sr)
swc <- abahfun(sw)
sfc <- abahfun(sf)
x <- 1 : 5
stopifnot(src(x) == sr(x))
stopifnot(swc(x) == sw(x))
stopifnot(sfc(x) == sf(x))

h <- ls(abah:::inlineHandlers, all = TRUE)
p <- sub("package:", "", sapply(h, find))
pp <- sapply(h, function(n) get(n, abah:::inlineHandlers)$package)
stopifnot(identical(p, pp))

stopifnot(all(sapply(abah:::safeBaseInternals,
                     function(f)
                     abah:::is.simpleInternal(get(f, "package:base")))))
