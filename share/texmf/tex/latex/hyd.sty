% hyd.sty ... Style for printing the Hyang manual
% Part of the Hyang package, http://www.hyang.org
% Hyang Programming Language
% Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
% Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
%
% This program is free software; you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation; either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesPackage{hyd}{}

\RequirePackage{ifthen}
\newboolean{hyd@has@ae}
\newboolean{hyd@use@ae}
\newboolean{hyd@use@hyper}
\newboolean{hyd@has@times}
\newboolean{hyd@use@times}
\newboolean{hyd@use@cm-super}
\newboolean{hyd@has@lm}
\newboolean{hyd@use@lm}
\newboolean{hyd@use@beramono}
\newboolean{hyd@use@inconsolata}
\DeclareOption{ae}{\setboolean{hyd@use@ae}{true}}
\DeclareOption{hyper}{\setboolean{hyd@use@hyper}{true}}
\DeclareOption{times}{\setboolean{hyd@use@times}{true}}
\DeclareOption{lm}{\setboolean{hyd@use@lm}{true}}
\DeclareOption{cm-super}{\setboolean{hyd@use@cm-super}{true}}
\DeclareOption{beramono}{\setboolean{hyd@use@beramono}{true}}
\DeclareOption{inconsolata}{\setboolean{hyd@use@inconsolata}{true}}
\ProcessOptions
\RequirePackage{longtable}
\setcounter{LTchunksize}{250}
\ifthenelse{\boolean{hyd@use@hyper}}
{\IfFileExists{hyperref.sty}{}{\setboolean{hyd@use@hyper}{false}
  \message{package hyperref not found}}}
{}

\RequirePackage{bm}              % standard boldsymbol
\RequirePackage{alltt}           % {verbatim} allowing \..
\RequirePackage{verbatim}        % small example code
\RequirePackage{url}             % set urls
\RequirePackage{textcomp}        % for \textquotesingle etc


\addtolength{\textheight}{12mm}
\addtolength{\topmargin}{-9mm}   % still fits on paper
\addtolength{\textwidth}{24mm}   % still fits on paper
\setlength{\oddsidemargin}{10mm}
\setlength{\evensidemargin}{\oddsidemargin}

\newenvironment{display}[0]%
  {\begin{list}{}{\setlength{\leftmargin}{30pt}}\item}%
  {\end{list}}
\newcommand{\HTML}{{\normalfont\textsc{html}}}
\newcommand{\hyang}{{\normalfont\textsf{hyang}}{}}
\newcommand{\hyangdash}{-}

% \def\href#1#2{\special{html:<a href="#1">}{#2}\special{html:</a>}}

\newcommand{\vneed}[1]{%
  \penalty-1000\vskip#1 plus 10pt minus #1\penalty-1000\vspace{-#1}}

\newcommand{\hydcontents}[1]{% modified \tableofcontents -- not \chapter
\section*{{#1}\@mkboth{\MakeUppercase#1}{\MakeUppercase#1}}
  \@starttoc{toc}}

\newcommand{\Header}[2]{%
  \vneed{1ex}
  \markboth{#1}{#1}
  \noindent
  \nopagebreak
  \begin{center}
  \ifthenelse{\boolean{hyd@use@hyper}}%
    {\def\@currentHref{page.\thepage}
    \hypertarget{Rfn.#1}{\index{#1@\texttt{#1}}}%
    \myaddcontentsline{toc}{subsection}{#1}%
    \pdfbookmark[1]{#1}{Rfn.#1}}
    {\addcontentsline{toc}{subsection}{#1}
      \index{#1@\texttt{#1}|textbf}}
    \hrule
    \parbox{0.95\textwidth}{%
      \begin{ldescription}[1.5in]
       \item[\texttt{#1}] \emph{#2}
      \end{ldescription}}
    \hrule
  \end{center}
  \nopagebreak}
%
%
%
% \alias{<alias>}{<header>}
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\alias}[2]{\hypertarget{Rfn.#1}{\index{#1@\texttt{#1} \textit{(\texttt{#2})}}}}}
{\newcommand{\alias}[2]{\index{#1@\texttt{#1} \textit{(\texttt{#2})}}}}
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\methalias}[2]{\hypertarget{Rfn.#1}{\relax}}}
{\newcommand{\methalias}[2]{}}
% \keyword{<topic>}{<header>}
\newcommand{\keyword}[2]{\index{$*$Topic{\large\ \textbf{#1}}!#2@\texttt{#2}}}
%
% used prior to 2.10.0 only
\newcommand{\Itemize}[1]{\begin{itemize}{#1}\end{itemize}}
\newcommand{\Enumerate}[1]{\begin{enumerate}{#1}\end{enumerate}}
\newcommand{\describe}[1]{\begin{description}{#1}\end{description}}

\newcommand{\Tabular}[2]{%
  \par\begin{longtable}{#1}
    #2
  \end{longtable}}

\newlength{\ldescriptionwidth}
\newcommand{\ldescriptionlabel}[1]{%
  \settowidth{\ldescriptionwidth}{{#1}}%
  \ifdim\ldescriptionwidth>\labelwidth
    {\parbox[b]{\labelwidth}%
      {\makebox[0pt][l]{#1}\\[1pt]\makebox{}}}%
  \else
    \makebox[\labelwidth][l]{{#1}}%
  \fi
  \hfil\relax}
\newenvironment{ldescription}[1][1in]%
  {\begin{list}{}%
    {\setlength{\labelwidth}{#1}%
      \setlength{\leftmargin}{\labelwidth}%
      \addtolength{\leftmargin}{\labelsep}%
      \renewcommand{\makelabel}{\ldescriptionlabel}}}%
  {\end{list}}

\newenvironment{hydsection}[1]{%
  \ifx\@empty#1\else\subsubsection*{#1}\fi
  \begin{list}{}{\setlength{\leftmargin}{0.25in}}\item}
  {\end{list}}

\newenvironment{Arguments}{%
  \begin{hydsection}{Arguments}}{\end{hydsection}}
\newenvironment{Author}{%
  \begin{hydsection}{Author(s)}}{\end{hydsection}}
\newenvironment{Description}{%
  \begin{hydsection}{Description}}{\end{hydsection}}
\newenvironment{Details}{%
  \begin{hydsection}{Details}}{\end{hydsection}}
\newenvironment{Examples}{%
  \begin{hydsection}{Examples}}{\end{hydsection}}
\newenvironment{Note}{%
  \begin{hydsection}{Note}}{\end{hydsection}}
\newenvironment{References}{%
  \begin{hydsection}{References}}{\end{hydsection}}
\newenvironment{SeeAlso}{%
  \begin{hydsection}{See Also}}{\end{hydsection}}
\newenvironment{Format}{%
  \begin{hydsection}{Format}}{\end{hydsection}}
\newenvironment{Source}{%
  \begin{hydsection}{Source}}{\end{hydsection}}
\newenvironment{Section}[1]{%
  \begin{hydsection}{#1}}{\end{hydsection}}
\newenvironment{Usage}{%
  \begin{hydsection}{Usage}}{\end{hydsection}}
\newenvironment{Value}{%
  \begin{hydsection}{Value}}{\end{hydsection}}

\newenvironment{SubSection}[1]{%
  \begin{list}{}{\setlength{\leftmargin}{0.1in}}\item \textbf{#1: }}{\end{list}}
\newenvironment{SubSubSection}[1]{%
  \begin{list}{}{\setlength{\leftmargin}{0.1in}}\item \textit{#1: }}{\end{list}}

\newenvironment{ExampleCode}{\small\verbatim}{\endverbatim}

\ifx\textbackslash\undefined%-- e.g. for MM
  \newcommand{\bsl}{\ifmmode\backslash\else$\backslash$\fi}
\else
  \newcommand{\bsl}{\ifmmode\backslash\else\textbackslash\fi}
\fi
%fails for index (but is not used there...)
\newcommand{\SIs}{\relax\ifmmode\leftarrow\else$\leftarrow$\fi}
\newcommand{\SIIs}{\relax\ifmmode<\leftarrow\else$<\leftarrow$\fi}
\newcommand{\Sbecomes}{\relax\ifmmode\rightarrow\else$\rightarrow$\fi}
%
\newcommand{\deqn}[2]{\[#1\]}
\newcommand{\eqn}[2]{$#1$}
\newcommand{\bold}[1]{\ifmmode\bm{#1}\else\textbf{#1}\fi}
%% Set \file in monospaced font, not sans-serif
\newcommand{\file}[1]{`\texttt{#1}'}

\newcommand{\Figure}[2]{\includegraphics[#2]{#1}}

\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\link}[1]{\hyperlink{Rfn.#1}{#1}\index{#1@\texttt{#1}}}}
{\newcommand{\link}[1]{#1\index{#1@\texttt{#1}}}}

\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand*{\hyanghref}{\begingroup\@makeother\#\@hyanghref}
  \newcommand*{\@hyanghref}[2]{\href{#1}{#2}\endgroup}}
{\newcommand*{\hyanghref}{\begingroup\@makeother\#\@hyanghref}
  \newcommand*{\@hyanghref}[2]{#2\footnote{\url{#1}}\endgroup}}

%% Set \email in monospaced font (like \url)
%\newcommand{\email}[1]{$\langle$\texttt{#1}$\rangle$}
\newcommand{\email}[1]{\normalfont\texttt{\textless#1\textgreater}}

%% \code without `-' ligatures

%{\catcode`\-=\active%
%   \global\def\code{\bgroup%
%     \catcode`\-=\active \let-\codedash%
%     \hyd@code}}
%     \def\codedash{-\discretionary{}{}{}}

%% If we simply do
%%   \DeclareRobustCommand\code{\bgroup\@noligs\@vobeyspaces\hyd@code}
%% then e.g.
%%   \code{\LinkA{attr<-}{attr<.hyangdash.}} 
%% will give an undefined control sequence `\< error when hyperref is
%% used (so that \LinkA uses \hyperlink).
%% Hence, use a noligs list without < and > for now, and use the same
%% list for \code and \samp and their variants.
\def\hyd@nolig@list{\do\`\do\,\do\'\do\-}
\def\hyd@noligs{\let\do\do@noligs \hyd@nolig@list}

\DeclareRobustCommand\code{\bgroup\hyd@noligs\@vobeyspaces\hyd@code}
\def\hyd@code#1{\normalfont\texttt{#1}\egroup}
\let\command=\code
\let\env=\code

\DeclareRobustCommand\samp{`\bgroup\hyd@noligs\@vobeyspaces\@sampx}
\def\@sampx#1{{\normalfont\texttt{#1}}\egroup'}
\let\option=\samp

\def\AsIs{\bgroup\let\do\@makeother\hyd@AsIs@dospecials\@noligs\obeylines\@vobeyspaces\parskip\z@skip\hyd@AsIsX}
\def\hyd@AsIs@dospecials{\do\$\do\&\do\#\do\^\do\_\do\%\do\~}
\def\hyd@AsIsX#1{\normalfont #1\egroup}

% This is a workaround for the old hydconv to handle \Sexpr by echoing it
% hyd2latex() should never let \Sexpr through to here.
\newcommand\Sexpr[2][]{{\normalfont\texttt{\bsl Sexpr[#1]\{#2\}}}}

\newcommand{\var}[1]{{\normalfont\textsl{#1}}}

\newcommand{\dfn}[1]{\textsl{#1}}
\let\Cite=\dfn

\newcommand{\acronym}[1]{\textsc{\lowercase{#1}}}
\newcommand{\kbd}[1]{{\normalfont\texttt{\textsl{#1}}}}

\newcommand{\strong}[1]{{\normalfont\fontseries{b}\selectfont #1}}
\let\pkg=\strong

\newcommand{\sQuote}[1]{`#1'}
\newcommand{\dQuote}[1]{``#1''}

\IfFileExists{ae.sty}{\setboolean{hyd@has@ae}{true}}{}
\ifthenelse{\boolean{hyd@use@ae}\and\boolean{hyd@has@ae}}{%
  \usepackage[T1]{fontenc}
  \usepackage{ae}
  \input{t1aett.fd}
  \DeclareFontShape{T1}{aett}{bx}{n}{<->ssub*aett/m/n}{}}{}
\IfFileExists{times.sty}{\setboolean{hyd@has@times}{true}}{}
\ifthenelse{\boolean{hyd@use@times}\and\boolean{hyd@has@times}}{%
  \usepackage[T1]{fontenc}
  \usepackage{times}}{}
\IfFileExists{lmodern.sty}{\setboolean{hyd@has@lm}{true}}{}
\ifthenelse{\boolean{hyd@use@lm}\and\boolean{hyd@has@lm}}{%
  \usepackage[T1]{fontenc}
  \usepackage{lmodern}}{}
\ifthenelse{\boolean{hyd@use@cm-super}}{%
  \usepackage[T1]{fontenc}}{}
\ifthenelse{\boolean{hyd@use@beramono}}{%
  \usepackage[scaled=.8]{beramono}}{}
%% it appears that all versions of zi4.sty support [noupquote],
%% whereas only those since 2013/06/09 of inconsolata.sty do: such
%% installations should also have zi4.sty
\ifthenelse{\boolean{hyd@use@inconsolata}}{%
 \IfFileExists{zi4.sty}{\usepackage[noupquote]{zi4}}{\usepackage{inconsolata}}}{}

%% needs to come after \code is defined
%% inspired by an earlier version of upquote.sty
\begingroup
\catcode`'=\active \catcode``=\active
\g@addto@macro\@noligs {\let`\textasciigrave \let'\textquotesingle}
\g@addto@macro\hyd@noligs {\let`\textasciigrave \let'\textquotesingle}
\endgroup

%% We use \pkg{verbatim} for our ExampleCode environment, which in its
%% \verbatim@font has an explicit \let\do\do@noligs\verbatim@nolig@list
%% rather than (the identical) \@noligs from the LaTeX2e kernel.
%% Hence, we add to \verbatim@font ... 
%% 
\g@addto@macro\verbatim@font\@noligs

\ifthenelse{\boolean{hyd@use@hyper}}{%
  \RequirePackage{color}    
  \def\myaddcontentsline#1#2#3{%
    \addtocontents{#1}{\protect\contentsline{#2}{#3}{\thepage}{page.\thepage}}}
  \RequirePackage{hyperref}
  \DeclareTextCommand{\hyangpercent}{PD1}{\045} % percent
  %% <NOTE>
  %% Formerly in Hyang's hyperref.cfg, possibly to be shared with Sweave.sty
  %% as well (but without setting pagebackref as this can give trouble
  %% for .bib entries containing URLs with '#' characters).
  \definecolor{Blue}{rgb}{0,0,0.8}
  \definecolor{Red}{rgb}{0.7,0,0}
  \hypersetup{%
    hyperindex,%
    colorlinks,%
    pagebackref,%
    linktocpage,%
    plainpages=false,%
    linkcolor=Blue,%
    citecolor=Blue,%
    urlcolor=Red,%
    pdfstartview=Fit,%
    pdfview={XYZ null null null}%
  }
  %% </NOTE>
  \renewcommand\tableofcontents{%
    \if@twocolumn
      \@restonecoltrue\onecolumn
    \else
      \@restonecolfalse
    \fi
    \chapter*{\contentsname
        \@mkboth{%
           \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
    \pdfbookmark{Contents}{contents}
    \@starttoc{toc}%
    \if@restonecol\twocolumn\fi
    }
  \renewenvironment{theindex}
  {\if@twocolumn
    \@restonecolfalse
    \else
    \@restonecoltrue
    \fi
    \columnseprule \z@
    \columnsep 35\p@
    \twocolumn[\@makeschapterhead{\indexname}]%
    \@mkboth{\MakeUppercase\indexname}%
    {\MakeUppercase\indexname}%
    \pdfbookmark{Index}{index}
    \myaddcontentsline{toc}{chapter}{Index}
    \thispagestyle{plain}\parindent\z@
    \parskip\z@ \@plus .3\p@\relax
    \raggedright
    \let\item\@idxitem}
  {\if@restonecol\onecolumn\else\clearpage\fi}
  }{
  \renewenvironment{theindex}
  {\if@twocolumn
    \@restonecolfalse
    \else
    \@restonecoltrue
    \fi
    \columnseprule \z@
    \columnsep 35\p@
    \twocolumn[\@makeschapterhead{\indexname}]%
    \@mkboth{\MakeUppercase\indexname}%
    {\MakeUppercase\indexname}%
    \addcontentsline{toc}{chapter}{Index}
    \thispagestyle{plain}\parindent\z@
    \parskip\z@ \@plus .3\p@\relax
    \raggedright
    \let\item\@idxitem}
  {\if@restonecol\onecolumn\else\clearpage\fi}
  }

% new definitions for Hyang >= 1.3.0
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\LinkA}[2]{\hyperlink{Rfn.#2}{#1}\index{#1@\texttt{#1}|textit}}}
{\newcommand{\LinkA}[2]{#1\index{#1@\texttt{#1}|textit}}}
%
% \alias{<alias>}{<header>}
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\aliasA}[3]{\hypertarget{Rfn.#3}{\index{#1@\texttt{#1} \textit{(\texttt{#2})}}}}}
{\newcommand{\aliasA}[3]{\index{#1@\texttt{#1} \textit{(\texttt{#2})}}}}
% \aliasB has no indexing.
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\aliasB}[3]{\hypertarget{Rfn.#3}{\relax}}}
{\newcommand{\aliasB}[3]{}}
\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\methaliasA}[3]{\hypertarget{Rfn.#3}{\relax}}}
{\newcommand{\methaliasA}[3]{}}

\ifthenelse{\boolean{hyd@use@hyper}}
{\newcommand{\HYPApkg}[1]{\href{https://hypa.hyang.org/package=#1}{\pkg{#1}}}}
{\newcommand{\HYPApkg}[1]{\pkg{#1}}}

\newcommand{\HeaderA}[3]{%
  \vneed{1ex}
  \markboth{#1}{#1}
  \noindent
  \nopagebreak
  \begin{center}
  \ifthenelse{\boolean{hyd@use@hyper}}%
    {\def\@currentHref{page.\thepage}
    \hypertarget{Rfn.#3}{\index{#1@\texttt{#1}}}%
    \myaddcontentsline{toc}{subsection}{#1}%
    \pdfbookmark[1]{#1}{Rfn.#3}}
    {\addcontentsline{toc}{subsection}{#1}
      \index{#1@\texttt{#1}|textbf}}
    \hrule
    \parbox{0.95\textwidth}{%
      \begin{ldescription}[1.5in]
       \item[\texttt{#1}] \emph{#2}
      \end{ldescription}}
    \hrule
  \end{center}
  \nopagebreak}
\DeclareTextCommandDefault{\hyangpercent}{\%{}}
%% for use with the output of encoded_text_to_latex
\ProvideTextCommandDefault{\textdegree}{\ensuremath{{^\circ}}}
\ProvideTextCommandDefault{\textonehalf}{\ensuremath{\frac12}}
\ProvideTextCommandDefault{\textonequarter}{\ensuremath{\frac14}}
\ProvideTextCommandDefault{\textthreequarters}{\ensuremath{\frac34}}
\ProvideTextCommandDefault{\textcent}{\TextSymbolUnavailable\textcent}
\ProvideTextCommandDefault{\textyen}{\TextSymbolUnavailable\textyen}
\ProvideTextCommandDefault{\textcurrency}{\TextSymbolUnavailable\textcurrency}
\ProvideTextCommandDefault{\textbrokenbar}{\TextSymbolUnavailable\textbrokenbar}
\ProvideTextCommandDefault{\texteuro}{\TextSymbolUnavailable\texteuro}
\providecommand{\mathonesuperior}{\ensuremath{^1}}
\providecommand{\mathtwosuperior}{\ensuremath{^2}}
\providecommand{\maththreesuperior}{\ensuremath{^3}}

\InputIfFileExists{hyd.cfg}{%
  \typeout{Reading personal defaults ...}}{}
