## ${HYANG_HOME}/share/make/basepkg.mk

.PHONY: front instdirs mkHyang mkHyang1 mkHyang2 mkHyangBase mkdesc mkdesc2 mkdemos mkdemos2 \
  mkexec mkman mkpo mksrc mksrc-win mksrc-win2 mkHyangSmrt mkecon mkeconutil \
  mkfigs

front:
	@for f in $(FRONTFILES); do \
	  if test -f $(srcdir)/$${f}; then \
	    $(INSTALL_DATA) $(srcdir)/$${f} \
	      $(top_builddir)/library/$(pkg); \
	  fi; \
	done

instdirs:
	@for D in $(INSTDIRS); do \
	 if test -d $(srcdir)/inst/$${D}; then \
	   $(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/$${D}; \
	   for f in `ls -d $(srcdir)/inst/$${D}/*`; do \
	     $(INSTALL_DATA) $${f} $(top_builddir)/library/$(pkg)/$${D}; \
	   done; \
	 fi; done

mkHyang1:
	@$(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/hyang
	@(f=$${TMPDIR:-/tmp}/hyang$$$$; \
	  if test "$(HYANG_KEEP_PKG_SOURCE)" = "yes"; then \
	    for hyangsrc in $(HYANGSRC); do \
	      $(ECHO) "#line 1 \"$${hyangsrc}\"" >> "$${f}"; \
	      cat $${hyangsrc} >> "$${f}"; \
	    done; \
	  else \
	    cat $(HYANGSRC) > "$${f}"; \
	  fi; \
	  $(SHELL) $(top_srcdir)/tools/move-if-change "$${f}" all.hyang)
	@if test -f $(srcdir)/HyPackLex;  then \
	  $(INSTALL_DATA) $(srcdir)/HyPackLex $(top_builddir)/library/$(pkg); \
	fi
	@rm -f $(top_builddir)/library/$(pkg)/Meta/nsInfo.hyds

mkHyang2:
	@$(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/hyang
	@(f=$${TMPDIR:-/tmp}/hyang$$$$; \
          $(ECHO) ".packageName <- \"$(pkg)\"" >  "$${f}"; \
	  if test "$(HYANG_KEEP_PKG_SOURCE)" = "yes"; then \
		for hyangsrc in `LC_COLLATE=C ls $(srcdir)/hyang/*.hyang`; do \
		  $(ECHO) "#line 1 \"$${hyangsrc}\"" >> "$${f}"; \
		    cat $${hyangsrc} >> "$${f}"; \
		done; \
	  else \
		cat `LC_COLLATE=C ls $(srcdir)/hyang/*.hyang` >> "$${f}"; \
	  fi; \
	  $(SHELL) $(top_srcdir)/tools/move-if-change "$${f}" all.hyang)
	@rm -f $(top_builddir)/library/$(pkg)/Meta/nsInfo.hyds
	@$(INSTALL_DATA) $(srcdir)/HyPackLex $(top_builddir)/library/$(pkg)
	@rm -f $(top_builddir)/library/$(pkg)/Meta/nsInfo.hyds

mkHyangBase:
	@$(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/hyang
	@(f=$${TMPDIR:-/tmp}/hyang$$$$; \
	  if test "$(HYANG_KEEP_PKG_SOURCE)" = "yes"; then \
	    $(ECHO) > "$${f}"; \
	    for hyangsrc in $(HYANGSRC); do \
	      $(ECHO) "#line 1 \"$${hyangsrc}\"" >> "$${f}"; \
	      cat $${hyangsrc} >> "$${f}"; \
	    done; \
	  else \
	    cat $(HYANGSRC) > "$${f}"; \
	  fi; \
	  f2=$${TMPDIR:-/tmp}/hyang2$$$$; \
	  sed -e "s:@WHICH@:${WHICH}:" "$${f}" > "$${f2}"; \
	  rm -f "$${f}"; \
	  $(SHELL) $(top_srcdir)/tools/move-if-change "$${f2}" all.hyang)
	@if ! test -f $(top_builddir)/library/$(pkg)/hyang/$(pkg); then \
	  $(INSTALL_DATA) all.hyang $(top_builddir)/library/$(pkg)/hyang/$(pkg); \
	else if test all.hyang -nt $(top_builddir)/library/$(pkg)/hyang/$(pkg); then \
	  $(INSTALL_DATA) all.hyang $(top_builddir)/library/$(pkg)/hyang/$(pkg); \
	  fi \
	fi

mkdesc:
	@if test -f DESCRIPTION; then \
	  if test "$(PKG_BUILT_STAMP)" != ""; then \
	    $(ECHO) "tools:::.install_package_description('.', '$(top_builddir)/library/${pkg}', '$(PKG_BUILT_STAMP)')" | \
	    HYANG_DEFAULT_PKGS=NULL $(HYANG_EXE) > /dev/null ; \
	  else \
	  $(ECHO) "tools:::.install_package_description('.', '$(top_builddir)/library/${pkg}')" | \
	  HYANG_DEFAULT_PKGS=NULL $(HYANG_EXE) > /dev/null ; \
	  fi; \
	fi

mkdesc2:
	@$(INSTALL_DATA) DESCRIPTION $(top_builddir)/library/$(pkg)
	@if test "$(PKG_BUILT_STAMP)" != ""; then \
	  $(ECHO) "Built: Hyang $(VERSION); ; $(PKG_BUILT_STAMP); $(HYANG_OSTYPE)" \
	     >> $(top_builddir)/library/$(pkg)/DESCRIPTION ; \
	else \
	  $(ECHO) "Built: Hyang $(VERSION); ; `TZ=UTC date`; $(HYANG_OSTYPE)" \
	     >> $(top_builddir)/library/$(pkg)/DESCRIPTION ; \
	fi

mkdemos:
	@$(ECHO) "tools:::.install_package_demos('$(srcdir)', '$(top_builddir)/library/$(pkg)')" | \
	  HYANG_DEFAULT_PKGS=NULL $(HYANG_EXE) > /dev/null

mkdemos2:
	@$(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/demo
	@for f in `ls -d $(srcdir)/demo/* | sed -e '/00Index/d'`; do \
	  $(INSTALL_DATA) "$${f}" $(top_builddir)/library/$(pkg)/demo; \
	done

mkexec:
	@if test -d $(srcdir)/exec; then \
	  $(MKINSTALLDIRS) $(top_builddir)/library/$(pkg)/exec; \
	  for f in  $(srcdir)/exec/*; do \
	    $(INSTALL_DATA) "$${f}" $(top_builddir)/library/$(pkg)/exec; \
	  done; \
	fi

mkecon:
	@$(INSTALL_DATA) all.hyang $(top_builddir)/library/$(pkg)/hyang/$(pkg)
	@$(ECHO) "tools:::econMakeToolRun(\"$(pkg)\")" | \
	  HYANG_DEFAULT_PKGS=$(DEFPKGS) LC_ALL=C $(HYANG_EXE) > /dev/null

mkeconutil: $(top_builddir)/library/$(pkg)/hyang/$(pkg).hydb

mkHyangSmrt:
	@$(INSTALL_DATA) all.hyang $(top_builddir)/library/$(pkg)/hyang/$(pkg)
	@rm -f $(top_builddir)/library/$(pkg)/hyang/$(pkg).rd?

mksrc:
	@if test -d src; then \
	  (cd src && $(MAKE)) || exit 1; \
	fi

mksrc-win2:
	@if test -d src; then \
	  (cd src && $(MAKE) -f Makefile.win EXT_LIBS="$(EXT_LIBS)") || exit 1; \
	fi

sysdata: $(srcdir)/hyang/sysdata.hyda
	@$(ECHO) "installing 'sysdata.hyda'"
	@$(ECHO) "tools:::sysdata2econtoolDB(\"$(srcdir)/hyang/sysdata.hyda\",\"$(top_builddir)/library/$(pkg)/hyang\")" | \
	  HYANG_DEFAULT_PKGS=NULL LC_ALL=C $(HYANG_EXE)

mkfigs:
	@if test -d  $(srcdir)/man/figures; then \
	  mkdir -p $(top_builddir)/library/$(pkg)/help/figures; \
	  cp $(srcdir)/man/figures/* \
	    $(top_builddir)/library/$(pkg)/help/figures; \
	fi

install-tests:
	@if test -d tests; then \
	  mkdir -p $(top_builddir)/library/$(pkg)/tests; \
	  cp tests/* $(top_builddir)/library/$(pkg)/tests; \
	fi

Makefile: $(srcdir)/Makefile.in $(top_builddir)/config.status
	@cd $(top_builddir) && $(SHELL) ./config.status $(subdir)/$@
DESCRIPTION: $(srcdir)/DESCRIPTION.in $(top_builddir)/config.status
	@cd $(top_builddir) && $(SHELL) ./config.status $(subdir)/$@

mostlyclean: clean
clean:
	@if test -d src; then (cd src && $(MAKE) $@); fi
	-@rm -f all.hyang .hyData
distclean: clean
	@if test -d src; then (cd src && $(MAKE) $@); fi
	-@rm -f Makefile DESCRIPTION
maintainer-clean: distclean

clean-win:
	@if test -d src; then \
	  $(MAKE) -C src -f Makefile.win clean; \
	fi
	-@rm -f all.hyang .hyData
distclean-win: clean-win
	-@rm -f DESCRIPTION


distdir: $(DISTFILES)
	@for f in $(DISTFILES); do \
	  test -f $(distdir)/$${f} \
	    || ln $(srcdir)/$${f} $(distdir)/$${f} 2>/dev/null \
	    || cp -p $(srcdir)/$${f} $(distdir)/$${f}; \
	done
	@for d in hyang data demo exec inst man noweb src po tests vignettes; do \
	  if test -d $(srcdir)/$${d}; then \
	    ((cd $(srcdir); \
	          $(TAR) -c -f - $(DISTDIR_TAR_EXCLUDE) $${d}) \
	        | (cd $(distdir); $(TAR) -x -f -)) \
	      || exit 1; \
	  fi; \
	done
