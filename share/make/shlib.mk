## ${HYANG_HOME}/share/make/shlib.mk

all: $(SHLIB)

$(SHLIB): $(OBJECTS)
	@if test  "z$(OBJECTS)" != "z"; then \
	  echo $(SHLIB_LINK) -o $@ $(OBJECTS) $(ALL_LIBS); \
	  $(SHLIB_LINK) -o $@ $(OBJECTS) $(ALL_LIBS); \
	fi

.PHONY: all shlib-clean

shlib-clean:
	@rm -Rf .libs _libs
	@rm -f $(OBJECTS) symbols.hyds


## FIXME: why not hyangscript?
symbols.hyds: $(OBJECTS)
	@$(ECHO) "tools:::.shlib_objects_symbol_tables()" | \
	  $(HYANG_HOME)/bin/hyang --vanilla --slave --args $(OBJECTS)
