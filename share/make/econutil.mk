## ${HYANG_HOME}/share/make/econutil.mk

.PHONY: HyangSmrt hyangecon hyangeconutil

$(top_builddir)/library/$(pkg)/hyang/$(pkg).hydb: all.hyang
	@$(INSTALL_DATA) all.hyang $(top_builddir)/library/$(pkg)/hyang/$(pkg)
	@if test -n "$(HYANG_NO_BASE_ABAH)"; then \
	 $(ECHO) "tools:::econMakeToolRun(\"$(pkg)\")" | \
	  HYANG_DEFAULT_PKGS=$(DEFPKGS) LC_ALL=C $(HYANG_EXE) > /dev/null; \
	else \
	 $(ECHO) "Trying for byte-compiling '$(pkg)' library"; \
	 $(ECHO) "tools:::econMakeToolRun(\"$(pkg)\")" | \
	  _HYANG_COMPILE_PKGS_=1 ABAH_SUPPRESS_ALL=1 \
	  HYANG_DEFAULT_PKGS=$(DEFPKGS) LC_ALL=C $(HYANG_EXE) > /dev/null; \
	fi

HyangSmrt: mkHyang mkHyangSmrt
hyangecon: mkHyang mkHyangSmrt mkecon
hyangeconutil: mkHyang mkHyangSmrt mkeconutil
