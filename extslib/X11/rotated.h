/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* ************************************************************************ */


/* Header file for the `xvertext 5.0' routines.

   Modified from Alan Richardson */


/* ************************************************************************ */

#ifndef _XVERTEXT_INCLUDED_
#define _XVERTEXT_INCLUDED_


#define XV_VERSION	5.0
#define XV_COPYRIGHT \
      "xvertext routines Copyright (c) 1993 Alan Richardson"


/* ---------------------------------------------------------------------- */


typedef enum {One_Font, Font_Set} hyang_FontType;

typedef struct hyang_XFont
{
    hyang_FontType type;
    XFontStruct *font;
    XFontSet fontset;
    int height;
    int ascent;
    int descent;
} hyang_XFont;


/* ---------------------------------------------------------------------- */

/* Protoized : C++ or ANSI C */
/* only XRotDrawString is used in Hyang */
double	XRotVersion(char*, int);
void	XRotSetMagnification(double);
void	XRotSetBoundingBoxPad(int);
int	XRotDrawString(Display*, XFontStruct*, double,
		       Drawable, GC, int, int, const char*);
int	XRotDrawImageString(Display*, XFontStruct*, double,
			    Drawable, GC, int, int, const char*);
int	XRotDrawAlignedString(Display*, XFontStruct*, double,
			      Drawable, GC, int, int, const char*, int);
int	XRotDrawAlignedImageString(Display*, XFontStruct*, double,
				   Drawable, GC, int, int, const char*, int);
XPoint *XRotTextExtents(Display*, XFontStruct*, double,
			int, int, const char*, int);

/* addition in 2.1.0 */
int	XRfRotDrawString(Display*, hyang_XFont*, double,
			 Drawable, GC, int, int, const char*);

/* ---------------------------------------------------------------------- */
#endif /* _XVERTEXT_INCLUDED_ */
