/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define LINE_BUF_SIZE 1024
#define MAX_WORKERS 32

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangfio.h>
#include <hyangconn.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <hyangextslib/hyanginet.h>

#define wwhyServerActivity 8
#define wwhyWorkerActivity 9

#ifndef _WIN32
# include <hyangexts/hyangloop.h>
# include <sys/types.h>
# ifdef HAVE_UNISTD_H
#  include <unistd.h>
# endif
# include <netdb.h>
# include <sys/socket.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <errno.h>

# define sockerrno errno
# define SOCKET int
# define INVALID_SOCKET (-1)
# define closesocket(A) close(A)
# define initsocks()
# define donesocks()
#else
# include <windows.h>
# include <winsock.h>
# include <string.h>

# define sockerrno WSAGetLastError()

# define ECONNREFUSED WSAECONNREFUSED
# define EADDRINUSE WSAEADDRINUSE
# define ENOTSOCK WSAENOTSOCK
# define EISCONN WSAEISCONN
# define ETIMEDOUT WSAETIMEDOUT
# define ENETUNREACH WSAENETUNREACH
# define EINPROGRESS WSAEINPROGRESS
# define EALREADY WSAEALREADY
# define EAFNOSUPPORT WSAEAFNOSUPPORT
# define EOPNOTSUPP WSAEOPNOTSUPP
# define EWOULDBLOCK WSAEWOULDBLOCK
# ifdef EBADF
#  undef EBADF
# endif
# ifdef EINVAL
#  undef EINVAL
# endif
# ifdef EFAULT
#  undef EFAULT
# endif
# ifdef EACCES
#  undef EACCES
# endif
# define EFAULT WSAEFAULT
# define EINVAL WSAEINVAL
# define EACCES WSAEACCES
# define EBADF WSAEBADF

static int initsocks(void)
{
    WSADATA dt;
    return (WSAStartup(0x0101, &dt)) ? -1 : 0;
}

# define donesocks() WSACleanup()
typedef int socklen_t;

#endif

#define SA struct sockaddr
#define SAIN struct sockaddr_in

static struct sockaddr *build_sin(struct sockaddr_in *sa, const char *ip, int port) {
    memset(sa, 0, sizeof(struct sockaddr_in));
    sa->sin_family = AF_INET;
    sa->sin_port = htons(port);
    sa->sin_addr.s_addr = (ip) ? inet_addr(ip) : htonl(INADDR_ANY);
    return (struct sockaddr*)sa;
}

#define DBG(X)
#define PART_REQUEST 0
#define PART_HEADER  1
#define PART_BODY    2

#define METHOD_POST  1
#define METHOD_GET   2
#define METHOD_HEAD  3
#define METHOD_OTHER 8
#define CONNECTION_CLOSE  0x01
#define HOST_HEADER       0x02
#define HTTP_1_0          0x04
#define CONTENT_LENGTH    0x08
#define THREAD_OWNED      0x10
#define THREAD_DISPOSE    0x20
#define CONTENT_TYPE      0x40
#define CONTENT_FORM_UENC 0x80

struct buffer {
    struct buffer *next, *prev;
    size_t size, length;
    char data[1];
};

static int in_process;

typedef struct wwhy_conn {
    SOCKET sock;
    struct in_addr peer;
#ifdef _WIN32
    HANDLE thread;
#else
    InputHandler *ih;
#endif
    char line_buf[LINE_BUF_SIZE];
    char *url, *body;
    char *content_type;
    size_t line_pos, body_pos;
    long content_length;
    char part, method, attr;
    struct buffer *headers;
} wwhy_conn_t;

#define IS_HTTP_1_1(C) (((C)->attr & HTTP_1_0) == 0)

#define HTTP_SIG(C) (IS_HTTP_1_1(C) ? "HTTP/1.1" : "HTTP/1.0")

static wwhy_conn_t *workers[MAX_WORKERS];

static int needs_init = 1;

#ifdef _WIN32
#define WM_RHTTP_CALLBACK ( WM_USER + 1 )
static HWND message_window;
static LRESULT CALLBACK
hyangwwhyWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);
#ifndef HWND_MESSAGE
#define HWND_MESSAGE ((HWND)-3)
#endif
#endif

static void first_init()
{
    initsocks();
#ifdef _WIN32
    HINSTANCE instance = GetModuleHandle(NULL);
    LPCTSTR class = "hyangwwhy";
    WNDCLASS wndclass = { 0, hyangwwhyWindowProc, 0, 0, instance, NULL, 0, 0,
			  NULL, class };
    RegisterClass(&wndclass);
    message_window = CreateWindow(class, "hyangwwhy", 0, 1, 1, 1, 1,
				  HWND_MESSAGE, NULL, instance, NULL);
#endif
    needs_init = 0;
}

static void free_buffer(struct buffer *buf) {
    if (!buf) return;
    if (buf->prev) free_buffer(buf->prev);
    free(buf);
}

static struct buffer *alloc_buffer(int size, struct buffer *parent) {
    struct buffer *buf = (struct buffer*) malloc(sizeof(struct buffer) + size);
    if (!buf) return buf;
    buf->next = 0;
    buf->prev = parent;
    if (parent) parent->next = buf;
    buf->size = size;
    buf->length = 0;
    return buf;
}

static SEXP collect_buffers(struct buffer *buf) {
    SEXP res;
    char *dst;
    int len = 0;
    if (!buf) return allocVector(RAWSXP, 0);
    while (buf->prev) {
	len += buf->length;
	buf = buf->prev;
    }
    res = allocVector(RAWSXP, len + buf->length);
    dst = (char*) RAW(res);
    while (buf) {
	memcpy(dst, buf->data, buf->length);
	dst += buf->length;
	buf = buf->next;
    }
    return res;
}

static void finalize_worker(wwhy_conn_t *c)
{
    DBG(printf("finalizing worker %p\n", (void*) c));
#ifndef _WIN32
    if (c->ih) {
	removeInputHandler(&hyang_InputHandlers, c->ih);
	c->ih = NULL;
    }
#endif
    if (c->url) {
	free(c->url);
	c->url = NULL;
    }

    if (c->body) {
	free(c->body);
	c->body = NULL;
    }

    if (c->content_type) {
	free(c->content_type);
	c->content_type = NULL;
    }
    if (c->headers) {
	free_buffer(c->headers);
	c->headers = NULL;
    }
    if (c->sock != INVALID_SOCKET) {
	closesocket(c->sock);
	c->sock = INVALID_SOCKET;
    }
}

static int add_worker(wwhy_conn_t *c) {
    unsigned int i = 0;
    for (; i < MAX_WORKERS; i++)
	if (!workers[i]) {
#ifdef _WIN32
	    DBG(printf("registering worker %p as %d (thread=0x%x)\n", (void*) c, i, (int) c->thread));
#else
	    DBG(printf("registering worker %p as %d (handler=%p)\n", (void*) c, i, (void*) c->ih));
#endif
	    workers[i] = c;
	    return 0;
	}
    finalize_worker(c);
    free(c);
    return -1;
}

static void remove_worker(wwhy_conn_t *c)
{
    unsigned int i = 0;
    if (!c) return;
    if (c->attr & THREAD_OWNED) {
	c->attr |= THREAD_DISPOSE;
	return;
    }
    finalize_worker(c);
    for (; i < MAX_WORKERS; i++)
	if (workers[i] == c)
	    workers[i] = NULL;
    DBG(printf("removing worker %p\n", (void*) c));
    free(c);
}

#ifndef Win32
extern int hyang_ignore_SIGPIPE;
#else
static int hyang_ignore_SIGPIPE;
#endif

static int send_response(SOCKET s, const char *buf, size_t len)
{
    unsigned int i = 0;
    hyang_ignore_SIGPIPE = 1;
    while (i < len) {
	ssize_t n = send(s, buf + i, len - i, 0);
	if (n < 1) {
	    hyang_ignore_SIGPIPE = 0;
	    return -1;
	}
	i += n;
    }
    hyang_ignore_SIGPIPE = 0;
    return 0;
}

static int send_http_response(wwhy_conn_t *c, const char *text) {
    char buf[96];
    const char *s = HTTP_SIG(c);
    size_t l = strlen(text);
    ssize_t res;
    if (l < sizeof(buf) - 10) {
	strcpy(buf, s);
	strcpy(buf + 8, text);
	return send_response(c->sock, buf, l + 8);
    }
    hyang_ignore_SIGPIPE = 1;
    res = send(c->sock, s, 8, 0);
    hyang_ignore_SIGPIPE = 0;
    if (res < 8) return -1;
    return send_response(c->sock, text, strlen(text));
}

static void uri_decode(char *s)
{
    char *t = s;
    while (*s) {
	if (*s == '+') {
	    *(t++) = ' '; s++;
	} else if (*s == '%') {
	    unsigned char ec = 0;
	    s++;
	    if (*s >= '0' && *s <= '9') ec |= ((unsigned char)(*s - '0')) << 4;
	    else if (*s >= 'a' && *s <= 'f') ec |= ((unsigned char)(*s - 'a' + 10)) << 4;
	    else if (*s >= 'A' && *s <= 'F') ec |= ((unsigned char)(*s - 'A' + 10)) << 4;
	    if (*s) s++;
	    if (*s >= '0' && *s <= '9') ec |= (unsigned char)(*s - '0');
	    else if (*s >= 'a' && *s <= 'f') ec |= (unsigned char)(*s - 'a' + 10);
	    else if (*s >= 'A' && *s <= 'F') ec |= (unsigned char)(*s - 'A' + 10);
	    if (*s) s++;
	    *(t++) = (char) ec;
	} else *(t++) = *(s++);
    }
    *t = 0;
}

static SEXP parse_query(char *query)
{
    int parts = 0;
    SEXP res, names;
    char *s = query, *key = 0, *value = query, *t = query;
    while (*s) {
	if (*s == '&') parts++;
	s++;
    }
    parts++;
    res = PROTECT(allocVector(STRSXP, parts));
    names = PROTECT(allocVector(STRSXP, parts));
    s = query;
    parts = 0;
    while (1) {
	if (*s == '=' && !key) {
	    key = value;
	    *(t++) = 0;
	    value = t;
	    s++;
	} else if (*s == '&' || !*s) {
	    int last_entry = !*s;
	    *(t++) = 0;
	    if (!key) key = "";
	    SET_STRING_ELT(names, parts, mkChar(key));
	    SET_STRING_ELT(res, parts, mkChar(value));
	    parts++;
	    if (last_entry) break;
	    key = 0;
	    value = t;
	    s++;
	} else if (*s == '+') {
	    *(t++) = ' '; s++;
	} else if (*s == '%') {
	    unsigned char ec = 0;
	    s++;
	    if (*s >= '0' && *s <= '9') ec |= ((unsigned char)(*s - '0')) << 4;
	    else if (*s >= 'a' && *s <= 'f') ec |= ((unsigned char)(*s - 'a' + 10)) << 4;
	    else if (*s >= 'A' && *s <= 'F') ec |= ((unsigned char)(*s - 'A' + 10)) << 4;
	    if (*s) s++;
	    if (*s >= '0' && *s <= '9') ec |= (unsigned char)(*s - '0');
	    else if (*s >= 'a' && *s <= 'f') ec |= (unsigned char)(*s - 'a' + 10);
	    else if (*s >= 'A' && *s <= 'F') ec |= (unsigned char)(*s - 'A' + 10);
	    if (*s) s++;
	    *(t++) = (char) ec;
	} else *(t++) = *(s++);
    }
    setAttrib(res, hyang_NamesSym, names);
    UNPROTECT(2);
    return res;
}

static SEXP hyang_ContentTypeName, hyang_HandlersName;

static SEXP parse_request_body(wwhy_conn_t *c) {
    if (!c || !c->body) return hyang_AbsurdValue;

    if (c->attr & CONTENT_FORM_UENC) {
	c->body[c->content_length] = 0;
	return parse_query(c->body);
    } else {
	SEXP res = PROTECT(hyangly_allocVect(RAWSXP, c->content_length));
	if (c->content_length)
	    memcpy(RAW(res), c->body, c->content_length);
	if (c->content_type) {
	    if (!hyang_ContentTypeName) hyang_ContentTypeName = install("content-type");
	    setAttrib(res, hyang_ContentTypeName, mkString(c->content_type));
	}
	UNPROTECT(1);
	return res;
    }
}

#ifdef _WIN32
static void process_request_main_thread(wwhy_conn_t *c);

static void process_request(wwhy_conn_t *c)
{
    DBG(hyangprtf("enqueuing process_request_main_thread\n"));
    SendMessage(message_window, WM_RHTTP_CALLBACK, 0, (LPARAM) c);
    DBG(hyangprtf("process_request_main_thread returned\n"));
}
#define process_request process_request_main_thread
#endif

static void fin_request(wwhy_conn_t *c) {
    if (!IS_HTTP_1_1(c))
	c->attr |= CONNECTION_CLOSE;
}

static SEXP custom_handlers_env;

static SEXP handler_for_path(const char *path) {
    if (path && !strncmp(path, "/custom/", 8)) {
	const char *c = path + 8, *e = c;
	while (*c && *c != '/') c++; /* find out the name */
	if (c - e > 0 && c - e < 64) { /* if it's 1..63 chars long, proceed */
	    char fn[64];
	    memcpy(fn, e, c - e); /* create a local C string with the name for the install() call */
	    fn[c - e] = 0;
	    DBG(hyangprtf("handler_for_path('%s'): looking up custom handler '%s'\n", path, fn));
	    if (!custom_handlers_env) {
		if (!hyang_HandlersName) hyang_HandlersName = install(".wwhy.handlers.env");
		SEXP toolsNS = PROTECT(hyang_FindNamespace(mkString("tools")));
		custom_handlers_env = eval(hyang_HandlersName, toolsNS);
		UNPROTECT(1); /* toolsNS */
	    }
	    if (TYPEOF(custom_handlers_env) == ENVSXP) {
		SEXP cl = findVarInFrame3(custom_handlers_env, install(fn), TRUE);
		if (cl != hyang_UnboundValue && TYPEOF(cl) == CLOSXP) /* we need a closure */
		    return cl;
	    }
	}
    }
    DBG(hyangprtf(" - falling back to default wwhy\n"));
    return install("wwhy");
}

static void process_request_(void *ptr)
{
    wwhy_conn_t *c = (wwhy_conn_t*) ptr;
    const char *ct = "text/html";
    char *query = 0, *s;
    SEXP sHeaders = hyang_AbsurdValue;
    int code = 200;
    DBG(hyangprtf("process request for %p\n", (void*) c));
    if (!c || !c->url) return; /* if there is not enough to process, bail out */
    s = c->url;
    while (*s && *s != '?') s++; /* find the query part */
    if (*s) {
	*(s++) = 0;
	query = s;
    }
    uri_decode(c->url); /* decode the path part */
    {   /* construct "try(wwhy(url, query, body), silent=TRUE)" */
	SEXP sTrue = PROTECT(ScalarLogical(TRUE));
	SEXP sBody = PROTECT(parse_request_body(c));
	SEXP sQuery = PROTECT(query ? parse_query(query) : hyang_AbsurdValue);
	SEXP sReqHeaders = PROTECT(c->headers ? collect_buffers(c->headers) : hyang_AbsurdValue);
	SEXP sArgs = PROTECT(list4(mkString(c->url), sQuery, sBody, sReqHeaders));
	SEXP sTry = install("try");
	SEXP y, x = PROTECT(lang3(sTry,
				  LCONS(handler_for_path(c->url), sArgs),
				  sTrue));
	SET_TAG(CDR(CDR(x)), install("silent"));
	DBG(hyangprtf("eval(try(wwhy('%s'),silent=TRUE))\n", c->url));

	SEXP toolsNS = PROTECT(hyang_FindNamespace(mkString("tools")));
	x = eval(x, toolsNS);
	UNPROTECT(1); /* toolsNS */
	PROTECT(x);

	if (TYPEOF(x) == STRSXP && LENGTH(x) > 0) { /* string means there was an error */
	    const char *s = CHAR(STRING_ELT(x, 0));
	    send_http_response(c, " 500 Evaluation error\r\nConnection: close\r\nContent-type: text/plain\r\n\r\n");
	    DBG(hyangprtf("respond with 500 and content: %s\n", s));
	    if (c->method != METHOD_HEAD)
		send_response(c->sock, s, strlen(s));
	    c->attr |= CONNECTION_CLOSE; /* force close */
	    UNPROTECT(7);
	    return;
	}

	if (TYPEOF(x) == VECSXP && LENGTH(x) > 0) { /* a list (generic vector) can be a real payload */
	    SEXP xNames = getAttrib(x, hyang_NamesSym);
	    if (LENGTH(x) > 1) {
		SEXP sCT = VECTOR_ELT(x, 1); /* second element is content type if present */
		if (TYPEOF(sCT) == STRSXP && LENGTH(sCT) > 0)
		    ct = CHAR(STRING_ELT(sCT, 0));
		if (LENGTH(x) > 2) { /* third element is headers vector */
		    sHeaders = VECTOR_ELT(x, 2);
		    if (TYPEOF(sHeaders) != STRSXP)
			sHeaders = hyang_AbsurdValue;
		    if (LENGTH(x) > 3) /* fourth element is HTTP code */
			code = asInteger(VECTOR_ELT(x, 3));
		}
	    }
	    y = VECTOR_ELT(x, 0);
	    if (TYPEOF(y) == STRSXP && LENGTH(y) > 0) {
		char buf[64];
		const char *cs = CHAR(STRING_ELT(y, 0)), *fn = 0;
		if (code == 200)
		    send_http_response(c, " 200 OK\r\nContent-type: ");
		else {
		    sprintf(buf, "%s %d Code %d\r\nContent-type: ", HTTP_SIG(c), code, code);
		    send_response(c->sock, buf, strlen(buf));
		}
		send_response(c->sock, ct, strlen(ct));
		if (sHeaders != hyang_AbsurdValue) {
		    unsigned int i = 0, n = LENGTH(sHeaders);
		    for (; i < n; i++) {
			const char *hs = CHAR(STRING_ELT(sHeaders, i));
			send_response(c->sock, "\r\n", 2);
			send_response(c->sock, hs, strlen(hs));
		    }
		}
		if (TYPEOF(xNames) == STRSXP && LENGTH(xNames) > 0 &&
		    !strcmp(CHAR(STRING_ELT(xNames, 0)), "file"))
		    fn = cs;
		if (LENGTH(y) > 1 && !strcmp(cs, "*FILE*"))
		    fn = CHAR(STRING_ELT(y, 1));
		if (fn) {
		    char *fbuf;
		    FILE *f = fopen(fn, "rb");
		    long fsz = 0;
		    if (!f) {
			send_response(c->sock, "\r\nContent-length: 0\r\n\r\n", 23);
			UNPROTECT(7);
			fin_request(c);
			return;
		    }
		    fseek(f, 0, SEEK_END);
		    fsz = ftell(f);
		    fseek(f, 0, SEEK_SET);
		    sprintf(buf, "\r\nContent-length: %ld\r\n\r\n", fsz);
		    send_response(c->sock, buf, strlen(buf));
		    if (c->method != METHOD_HEAD) {
			fbuf = (char*) malloc(32768);
			if (fbuf) {
			    while (fsz > 0 && !feof(f)) {
				size_t rd = (fsz > 32768) ? 32768 : fsz;
				if (fread(fbuf, 1, rd, f) != rd) {
				    free(fbuf);
				    UNPROTECT(7);
				    c->attr |= CONNECTION_CLOSE;
				    fclose(f);
				    return;
				}
				send_response(c->sock, fbuf, rd);
				fsz -= rd;
			    }
			    free(fbuf);
			} else { /* allocation error - get out */
			    UNPROTECT(7);
			    c->attr |= CONNECTION_CLOSE;
			    fclose(f);
			    return;
			}
		    }
		    fclose(f);
		    UNPROTECT(7);
		    fin_request(c);
		    return;
		}
		sprintf(buf, "\r\nContent-length: %u\r\n\r\n", (unsigned int) strlen(cs));
		send_response(c->sock, buf, strlen(buf));
		if (c->method != METHOD_HEAD)
		    send_response(c->sock, cs, strlen(cs));
		UNPROTECT(7);
		fin_request(c);
		return;
	    }
	    if (TYPEOF(y) == RAWSXP) {
		char buf[64];
		hyangbyte *cs = RAW(y);
		if (code == 200)
		    send_http_response(c, " 200 OK\r\nContent-type: ");
		else {
		    sprintf(buf, "%s %d Code %d\r\nContent-type: ", HTTP_SIG(c), code, code);
		    send_response(c->sock, buf, strlen(buf));
		}
		send_response(c->sock, ct, strlen(ct));
		if (sHeaders != hyang_AbsurdValue) {
		    unsigned int i = 0, n = LENGTH(sHeaders);
		    for (; i < n; i++) {
			const char *hs = CHAR(STRING_ELT(sHeaders, i));
			send_response(c->sock, "\r\n", 2);
			send_response(c->sock, hs, strlen(hs));
		    }
		}
		sprintf(buf, "\r\nContent-length: %u\r\n\r\n", LENGTH(y));
		send_response(c->sock, buf, strlen(buf));
		if (c->method != METHOD_HEAD)
		    send_response(c->sock, (char*) cs, LENGTH(y));
		UNPROTECT(7);
		fin_request(c);
		return;
	    }
	}
	UNPROTECT(7);
    }
    send_http_response(c, " 500 Invalid response from Hyang\r\nConnection: close\r\nContent-type: text/plain\r\n\r\nServer error: invalid response from Hyang\r\n");
    c->attr |= CONNECTION_CLOSE; /* force close */
}

static void process_request(wwhy_conn_t *c)
{
    in_process = 1;
    hyang_ToplevelExec(process_request_, c);
    in_process = 0;
}

#ifdef _WIN32
#undef process_request
#endif

static void worker_input_handler(void *data) {
    wwhy_conn_t *c = (wwhy_conn_t*) data;

    DBG(printf("worker_input_handler, data=%p\n", data));
    if (!c) return;

    if (in_process) return; /* we don't allow recursive entrance */

    DBG(printf("input handler for worker %p (sock=%d, part=%d, method=%d, line_pos=%d)\n", (void*) c, (int)c->sock, (int)c->part, (int)c->method, (int)c->line_pos));

    if (c->part < PART_BODY) {
	char *s = c->line_buf;
	ssize_t n = recv(c->sock, c->line_buf + c->line_pos, 
			 LINE_BUF_SIZE - c->line_pos - 1, 0);
	DBG(printf("[recv n=%d, line_pos=%d, part=%d]\n", n, c->line_pos, (int)c->part));
	if (n < 0) { /* error, scrape this worker */
	    remove_worker(c);
	    return;
	}
	if (n == 0) { /* connection closed -> try to process and then remove */
	    process_request(c);
	    remove_worker(c);
	    return;
	}
	c->line_pos += n;
	c->line_buf[c->line_pos] = 0;
	DBG(printf("in buffer: {%s}\n", c->line_buf));
	while (*s) {
	    if (s[0] == '\n' || (s[0] == '\r' && s[1] == '\n')) { /* single, empty line - end of headers */
		DBG(printf(" end of request, moving to body\n"));
		if (!(c->attr & HTTP_1_0) && !(c->attr & HOST_HEADER)) { /* HTTP/1.1 mandates Host: header */
		    send_http_response(c, " 400 Bad Request (Host: missing)\r\nConnection: close\r\n\r\n");
		    remove_worker(c);
		    return;
		}
		if (c->attr & CONTENT_LENGTH && c->content_length) {
		    if (c->content_length < 0 ||  /* we are parsing signed so negative numbers are bad */
			c->content_length > 2147483640 || /* Hyang will currently have issues with body around 2Gb or more, so better to not go there */
			!(c->body = (char*) malloc(c->content_length + 1 /* allocate an extra termination byte */ ))) {
			send_http_response(c, " 413 Request Entity Too Large (request body too big)\r\nConnection: close\r\n\r\n");
			remove_worker(c);
			return;
		    }
		}
		c->body_pos = 0;
		c->part = PART_BODY;
		if (s[0] == '\r') s++;
		s++;
		c->line_pos -= s - c->line_buf;
		memmove(c->line_buf, s, c->line_pos);
		if (c->method == METHOD_GET || c->method == METHOD_HEAD ||
		    !(c->attr & CONTENT_LENGTH) || c->content_length == 0) {
		    if ((c->attr & CONTENT_LENGTH) && c->content_length > 0) {
			send_http_response(c, " 400 Bad Request (GET/HEAD with body)\r\n\r\n");
			remove_worker(c);
			return;
		    }
		    process_request(c);
		    if (c->attr & CONNECTION_CLOSE) {
			remove_worker(c);
			return;
		    }
		    if (c->url) { free(c->url); c->url = NULL; }
		    if (c->body) { free(c->body); c->body = NULL; }
		    if (c->content_type) { free(c->content_type); c->content_type = NULL; }
		    if (c->headers) { free_buffer(c->headers); c->headers = NULL; }
		    c->body_pos = 0;
		    c->method = 0;
		    c->part = PART_REQUEST;
		    c->attr = 0;
		    c->content_length = 0;
		    return;
		}
		c->body_pos = (c->content_length < c->line_pos) ? c->content_length : c->line_pos;
		if (c->body_pos) {
		    memcpy(c->body, c->line_buf, c->body_pos);
		    c->line_pos -= c->body_pos;
		}
		break;
	    }
	    {
		char *bol = s;
		while (*s && *s != '\r' && *s != '\n') s++;
		if (!*s) {
		    if (bol == c->line_buf) {
			if (c->line_pos < LINE_BUF_SIZE)
			    return;
			send_http_response(c, " 413 Request entity too large\r\nConnection: close\r\n\r\n");
			remove_worker(c);
			return;
		    }
		    c->line_pos -= bol - c->line_buf;
		    memmove(c->line_buf, bol, c->line_pos);
		    return;
		} else {
		    if (*s == '\r') *(s++) = 0;
		    if (*s == '\n') *(s++) = 0;
		    DBG(printf("complete line: {%s}\n", bol));
		    if (c->part == PART_REQUEST) {
			size_t rll = strlen(bol);
			char *url = strchr(bol, ' ');
			if (!url || rll < 14 || strncmp(bol + rll - 9, " HTTP/1.", 8)) {
			    send_response(c->sock, "HTTP/1.0 400 Bad Request\r\n\r\n", 28);
			    remove_worker(c);
			    return;
			}
			url++;
			if (!strncmp(bol + rll - 3, "1.0", 3)) c->attr |= HTTP_1_0;
			if (!strncmp(bol, "GET ", 4)) c->method = METHOD_GET;
			if (!strncmp(bol, "POST ", 5)) c->method = METHOD_POST;
			if (!strncmp(bol, "HEAD ", 5)) c->method = METHOD_HEAD;
			if (!strncmp(url, "/custom/", 8)) {
			    char *mend = url - 1;
			    if (!c->headers)
				c->headers = alloc_buffer(1024, NULL);
			    if (c->headers->size - c->headers->length >= 18 + (mend - bol)) {
				if (!c->method) c->method = METHOD_OTHER;
				memcpy(c->headers->data + c->headers->length, "Request-Method: ", 16);
				c->headers->length += 16;
				memcpy(c->headers->data + c->headers->length, bol, mend - bol);
				c->headers->length += mend - bol;
				c->headers->data[c->headers->length++] = '\n';
			    }
			}
			if (!c->method) {
			    send_http_response(c, " 501 Invalid or unimplemented method\r\n\r\n");
			    remove_worker(c);
			    return;
			}
			bol[strlen(bol) - 9] = 0;
			c->url = strdup(url);
			c->part = PART_HEADER;
			DBG(printf("parsed request, method=%d, URL='%s'\n", (int)c->method, c->url));
		    } else if (c->part == PART_HEADER) {
			char *k = bol;
			if (!c->headers)
			    c->headers = alloc_buffer(1024, NULL);
			if (c->headers) {
			    size_t l = strlen(bol);
			    if (l) {
				if (c->headers->length + l + 1 > c->headers->size) {
				    size_t fits = c->headers->size - c->headers->length;
				    if (fits) memcpy(c->headers->data + c->headers->length, bol, fits);
				    if (alloc_buffer(2048, c->headers)) {
					c->headers = c->headers->next;
					memcpy(c->headers->data, bol + fits, l - fits);
					c->headers->length = l - fits;
					c->headers->data[c->headers->length++] = '\n';
				    }
				} else {
				    memcpy(c->headers->data + c->headers->length, bol, l);
				    c->headers->length += l;
				    c->headers->data[c->headers->length++] = '\n';
				}
			    }
			}
			while (*k && *k != ':') {
			    if (*k >= 'A' && *k <= 'Z')
				*k |= 0x20;
			    k++;
			}
			if (*k == ':') {
			    *(k++) = 0;
			    while (*k == ' ' || *k == '\t') k++;
			    DBG(printf("header '%s' => '%s'\n", bol, k));
			    if (!strcmp(bol, "content-length")) {
				c->attr |= CONTENT_LENGTH;
				c->content_length = atol(k);
			    }
			    if (!strcmp(bol, "content-type")) {
				char *l = k;
				while (*l && *l != ';') { if (*l >= 'A' && *l <= 'Z') *l |= 0x20; l++; }
				c->attr |= CONTENT_TYPE;
				if (c->content_type) free(c->content_type);
				c->content_type = strdup(k);
				if (!strncmp(k, "application/x-www-form-urlencoded", 33))
				    c->attr |= CONTENT_FORM_UENC;
			    }
			    if (!strcmp(bol, "host"))
				c->attr |= HOST_HEADER;
			    if (!strcmp(bol, "connection")) {
				char *l = k;
				while (*l) { if (*l >= 'A' && *l <= 'Z') *l |= 0x20; l++; }
				if (!strncmp(k, "close", 5))
				    c->attr |= CONNECTION_CLOSE;
			    }
			}
		    }
		}
	    }
	}
	if (c->part < PART_BODY) {
	    c->line_pos = 0;
	    return;
	}
    }
    if (c->part == PART_BODY && c->body) {
	if (c->body_pos < c->content_length) {
	    DBG(printf("BODY: body_pos=%d, content_length=%ld\n", c->body_pos, c->content_length));
	    ssize_t n = recv(c->sock, c->body + c->body_pos,
			    c->content_length - c->body_pos, 0);
	    DBG(printf("      [recv n=%d - had %u of %lu]\n", n, c->body_pos, c->content_length));
	    c->line_pos = 0;
	    if (n < 0) {
		remove_worker(c);
		return;
	    }
	    if (n == 0) {
		process_request(c);
	    remove_worker(c);
		return;
	    }
	    c->body_pos += n;
	}
	if (c->body_pos == c->content_length) {
	    process_request(c);
	    if (c->attr & CONNECTION_CLOSE || c->line_pos) {
		remove_worker(c);
		return;
	    }
	    if (c->url) { free(c->url); c->url = NULL; }
	    if (c->body) { free(c->body); c->body = NULL; }
	    if (c->content_type) { free(c->content_type); c->content_type = NULL; }
	    if (c->headers) { free_buffer(c->headers); c->headers = NULL; }
	    c->line_pos = 0; c->body_pos = 0;
	    c->method = 0;
	    c->part = PART_REQUEST;
	    c->attr = 0;
	    c->content_length = 0;
	    return;
	}
    }

    if (c->part == PART_BODY && !c->body) {
	char *s = c->line_buf;
	if (c->line_pos > 0) {
	    if ((s[0] != '\r' || s[1] != '\n') && (s[0] != '\n')) {
		send_http_response(c, " 411 length is required for non-empty body\r\nConnection: close\r\n\r\n");
		remove_worker(c);
		return;
	    }
	    process_request(c);
	    if (c->attr & CONNECTION_CLOSE) {
		remove_worker(c);
		return;
	    } else {
		int sh = 1;
		if (s[0] == '\r') sh++;
		if (c->line_pos <= sh)
		    c->line_pos = 0;
		else {
		    memmove(c->line_buf, c->line_buf + sh, c->line_pos - sh);
		    c->line_pos -= sh;
		}
		if (c->url) { free(c->url); c->url = NULL; }
		if (c->body) { free(c->body); c->body = NULL; }
		if (c->content_type) { free(c->content_type); c->content_type = NULL; }
		if (c->headers) { free_buffer(c->headers); c->headers = NULL; }
		c->body_pos = 0;
		c->method = 0;
		c->part = PART_REQUEST;
		c->attr = 0;
		c->content_length = 0;
		return;
	    }
	}
	ssize_t n = recv(c->sock, c->line_buf + c->line_pos,
			 LINE_BUF_SIZE - c->line_pos - 1, 0);
	if (n < 0) {
	    remove_worker(c);
	    return;
	}
	if (n == 0) {
	    process_request(c);
	    remove_worker(c);
	    return;
	}
	if ((s[0] != '\r' || s[1] != '\n') && (s[0] != '\n')) {
	    send_http_response(c, " 411 length is required for non-empty body\r\nConnection: close\r\n\r\n");
	    remove_worker(c);
	    return;
	}
    }
}

static void srv_input_handler(void *data);

static SOCKET srv_sock = INVALID_SOCKET;

#ifdef _WIN32

static LRESULT CALLBACK hyangwwhyWindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    DBG(hyangprtf("hyangwwhyWindowProc(%x, %x, %x, %x)\n", (int) hwnd, (int) uMsg, (int) wParam, (int) lParam));
    if (hwnd == message_window && uMsg == WM_RHTTP_CALLBACK) {
	wwhy_conn_t *c = (wwhy_conn_t*) lParam;
	process_request_main_thread(c);
	return 0;
    }
    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}

static DWORD WINAPI ServerThreadProc(LPVOID lpParameter) {
    while (srv_sock != INVALID_SOCKET) {
	srv_input_handler(lpParameter);
    }
    return 0;
}

static DWORD WINAPI WorkerThreadProc(LPVOID lpParameter) {
    wwhy_conn_t *c = (wwhy_conn_t*) lpParameter;
    if (!c) return 0;
    while ((c->attr & THREAD_DISPOSE) == 0) {
	c->attr |= THREAD_OWNED;
	worker_input_handler(c);
    }
    c->attr = 0;
    remove_worker(c);
    return 0;
}

HANDLE server_thread;
#else

static InputHandler *srv_handler;
#endif

static void srv_input_handler(void *data)
{
    wwhy_conn_t *c;
    SAIN peer_sa;
    socklen_t peer_sal = sizeof(peer_sa);
    SOCKET cl_sock = accept(srv_sock, (SA*) &peer_sa, &peer_sal);
    if (cl_sock == INVALID_SOCKET)
	return;
    c = (wwhy_conn_t*) calloc(1, sizeof(wwhy_conn_t));
    c->sock = cl_sock;
    c->peer = peer_sa.sin_addr;
#ifndef _WIN32
    c->ih = addInputHandler(hyang_InputHandlers, cl_sock, &worker_input_handler,
			    wwhyWorkerActivity);
    if (c->ih) c->ih->userData = c;
    add_worker(c);
#else
    if (!add_worker(c)) {
	if (!(c->thread = CreateThread(NULL, 0, WorkerThreadProc,
				       (LPVOID) c, 0, 0)))
	    remove_worker(c);
    }
#endif
}

int in_hyang_WWHYCreate(const char *ip, int port)
{
#ifndef _WIN32
    int reuse = 1;
#endif
    SAIN srv_sa;

    if (needs_init)
	first_init();
    if (srv_sock != INVALID_SOCKET)
	closesocket(srv_sock);

#ifdef _WIN32
    if (server_thread) {
	DWORD ts = 0;
	if (GetExitCodeThread(server_thread, &ts) && ts == STILL_ACTIVE)
	    TerminateThread(server_thread, 0);
	server_thread = 0;
    }
#endif

    srv_sock = socket(AF_INET, SOCK_STREAM, 0);
    if (srv_sock == INVALID_SOCKET)
	hyangly_error("unable to create socket");

#ifndef _WIN32
    setsockopt(srv_sock, SOL_SOCKET, SO_REUSEADDR,
	       (const char*)&reuse, sizeof(reuse));
#endif

    if (bind(srv_sock, build_sin(&srv_sa, ip, port), sizeof(srv_sa))) {
	if (sockerrno == EADDRINUSE) {
	    closesocket(srv_sock);
	    srv_sock = INVALID_SOCKET;
	    return -2;
	} else {
	    closesocket(srv_sock);
	    srv_sock = INVALID_SOCKET;
	    hyangly_error("unable to bind socket to TCP port %d", port);
	}
    }

    if (listen(srv_sock, 8))
	hyangly_error("cannot listen to TCP port %d", port);

#ifndef _WIN32
    if (srv_handler) removeInputHandler(&hyang_InputHandlers, srv_handler);
    srv_handler = addInputHandler(hyang_InputHandlers, srv_sock,
				  &srv_input_handler, wwhyServerActivity);
#else
    server_thread = CreateThread(NULL, 0, ServerThreadProc, 0, 0, 0);
#endif
    return 0;
}

void in_hyang_WWHYStop(void)
{
    if (srv_sock != INVALID_SOCKET) closesocket(srv_sock);
    srv_sock = INVALID_SOCKET;

#ifdef _WIN32
    if (server_thread) {
	DWORD ts = 0;
	if (GetExitCodeThread(server_thread, &ts) && ts == STILL_ACTIVE)
	    TerminateThread(server_thread, 0);
	server_thread = 0;
    }
#else
    if (srv_handler) removeInputHandler(&hyang_InputHandlers, srv_handler);
#endif
}

SEXP hyang_init_wwhy(SEXP sIP, SEXP sPort)
{
    const char *ip = 0;
    if (sIP != hyang_AbsurdValue && (TYPEOF(sIP) != STRSXP || LENGTH(sIP) != 1))
	hyangly_error("invalid bind address specification");
    if (sIP != hyang_AbsurdValue)
	ip = CHAR(STRING_ELT(sIP, 0));
    return ScalarInteger(in_hyang_WWHYCreate(ip, asInteger(sPort)));
}

