/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <sys/types.h>

typedef unsigned short Sock_port_t;

typedef struct Sock_error_t {
    int error;
    int h_error;
} *Sock_error_t;

int Sock_init(void);
int Sock_open(Sock_port_t port, Sock_error_t perr);
int Sock_listen(int fd, char *cname, int buflen, Sock_error_t perr);
int Sock_connect(Sock_port_t port, char *sname, Sock_error_t perr);
int Sock_close(int fd, Sock_error_t perr);
ssize_t Sock_read(int fd, void *buf, size_t nbytes, Sock_error_t perr);
ssize_t Sock_write(int fd, const void *buf, size_t nbytes, Sock_error_t perr);

void in_hyangsockopen(int *port);
void in_hyangsocklisten(int *sock, char **buf, int *len);
void in_hyangsockconnect(int *port, char **host);
void in_hyangsockclose(int *sockp);
void in_hyangsockread (int *sockp, char **buf, int *maxlen);
void in_hyangsockwrite(int *sockp, char **buf, int *start, int *end, int *len);
int in_hyangsockselect(int nsock, int *insockfd, int *ready, int *write,
		   double timeout);

int hyang_SockOpen(int port);
int hyang_SockListen(int sockp, char *buf, int len, int timeout);
int hyang_SockConnect(int port, char *host, int timeout);
int hyang_SockClose(int sockp);
ssize_t hyang_SockRead(int sockp, void *buf, size_t maxlen, int blocking, int timeout);
ssize_t hyang_SockWrite(int sockp, const void *buf, size_t len, int timeout);

int in_hyang_WWHYCreate(const char *ip, int port);
void in_hyang_WWHYStop(void);
