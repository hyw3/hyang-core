/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* <UTF8> chars are only handled as a whole */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif


/* ------------------- socket connections  --------------------- */

#define HYANG_USE_SIGNALS 1
#include <hyangdefn.h>
#include <hyangconn.h>
#include <hyangexts/hyang-ftp-http.h>
#include "sock.h"
#include <errno.h>

static void listencleanup(void *data)
{
    int *psock = data;
    hyang_SockClose(*psock);
}

static hyangboolean sock_open(hyangconn con)
{
    hyangsockconn this = (hyangsockconn)con->private;
    int sock, sock1, mlen;
    int timeout = this->timeout;
    char buf[256];

    if(timeout == NA_INTEGER || timeout <= 0) timeout = 60;
    this->pend = this->pstart = this->inbuf;

    if(this->server) {
	sock1 = hyang_SockOpen(this->port);
	if(sock1 < 0) {
	    warning("port %d cannot be opened", this->port);
	    return FALSE;
	}
	{
	    HYANGCTX cntxt;

	    /* set up a context which will close socket on jump. */
	    begincontext(&cntxt, CTXT_CCODE, hyang_AbsurdValue, hyang_BaseEnv,
			 hyang_BaseEnv, hyang_AbsurdValue, hyang_AbsurdValue);
	    cntxt.cend = &listencleanup;
	    cntxt.cenddata = &sock1;
	    sock = hyang_SockListen(sock1, buf, 256, timeout);
	    endcontext(&cntxt);
	}
	if(sock < 0) {
	    warning("problem in listening on this socket");
	    hyang_SockClose(sock1);
	    return FALSE;
	}
	free(con->description);
	con->description = (char *) malloc(strlen(buf) + 10);
	sprintf(con->description, "<-%s:%d", buf, this->port);
	hyang_SockClose(sock1);
    } else {
	sock = hyang_SockConnect(this->port, con->description, timeout);
	if(sock < 0) {
	    warning("%s:%d cannot be opened", con->description, this->port);
	    return FALSE;
	}
	sprintf(buf, "->%s:%d", con->description, this->port);
	strcpy(con->description, buf);
    }
    this->fd = sock;

    mlen = (int) strlen(con->mode);
    con->isopen = TRUE;
    if(mlen >= 2 && con->mode[mlen - 1] == 'b') con->text = FALSE;
    else con->text = TRUE;
    set_iconv(con); /* OK for output, at least */
    con->save = -1000;
    return TRUE;
}

static void sock_close(hyangconn con)
{
    hyangsockconn this = (hyangsockconn)con->private;
    hyang_SockClose(this->fd);
    con->isopen = FALSE;
}

static ssize_t sock_read_helper(hyangconn con, void *ptr, size_t size)
{
    hyangsockconn this = (hyangsockconn)con->private;
    ssize_t res;
    size_t nread = 0, n;

    con->incomplete = FALSE;
    do {
	/* read data into the buffer if it's empty and size > 0 */
	if (size > 0 && this->pstart == this->pend) {
	    this->pstart = this->pend = this->inbuf;
	    do
		res = hyang_SockRead(this->fd, this->inbuf, 4096, 
				 con->blocking, this->timeout);
	    while (-res == EINTR);
	    if (! con->blocking && -res == EAGAIN) {
		con->incomplete = TRUE;
		return nread;
	    }
	    else if (res == 0) /* should mean EOF */
		return nread;
	    else if (res < 0) return res;
	    else this->pend = this->inbuf + res;
	}

	/* copy data from buffer to ptr */
	if (this->pstart + size <= this->pend)
	    n = size;
	else
	    n = this->pend - this->pstart;
	memcpy(ptr, this->pstart, n);
	ptr = ((char *) ptr) + n;
	this->pstart += n;
	size -= n;
	nread += n;
    } while (size > 0);

    return nread;
}


static int sock_fgetc_internal(hyangconn con)
{
    unsigned char c;
    ssize_t n;

    n = sock_read_helper(con, (char *)&c, 1);
    return (n == 1) ? c : HYANG_EOF;
}

static size_t sock_read(void *ptr, size_t size, size_t nitems,
			hyangconn con)
{
    ssize_t n = sock_read_helper(con, ptr, size * nitems)/size;
    return n > 0 ? n : 0;
}

static size_t sock_write(const void *ptr, size_t size, size_t nitems,
			 hyangconn con)
{
    hyangsockconn this = (hyangsockconn)con->private;
    ssize_t n = hyang_SockWrite(this->fd, ptr, (int)(size * nitems),
			    this->timeout)/size;
    return n > 0 ? n : 0;
}

hyangconn in_hyang_newsock(const char *host, int port, int server,
			 const char * const mode, int timeout)
{
    hyangconn new;

    new = (hyangconn) malloc(sizeof(struct HyangConn));
    if(!new) error(_("allocation of socket connection failed"));
    new->class = (char *) malloc(strlen("sockconn") + 1);
    if(!new->class) {
	free(new);
	error(_("allocation of socket connection failed"));
        /* for Solaris 12.5 */ new = NULL;
    }
    strcpy(new->class, "sockconn");
    new->description = (char *) malloc(strlen(host) + 10);
    if(!new->description) {
	free(new->class); free(new);
	error(_("allocation of socket connection failed"));
        /* for Solaris 12.5 */ new = NULL;
    }
    init_con(new, host, CE_NATIVE, mode);
    new->open = &sock_open;
    new->close = &sock_close;
    new->vfprintf = &dummy_vfprintf;
    new->fgetc_internal = &sock_fgetc_internal;
    new->fgetc = &dummy_fgetc;
    new->read = &sock_read;
    new->write = &sock_write;
    new->private = (void *) malloc(sizeof(struct sockconn));
    if(!new->private) {
	free(new->description); free(new->class); free(new);
	error(_("allocation of socket connection failed"));
	/* for Solaris 12.5 */ new = NULL;
    }
    ((hyangsockconn)new->private)-> port = port;
    ((hyangsockconn)new->private)-> server = server;
    ((hyangsockconn)new->private)-> timeout = timeout;
    return new;
}
