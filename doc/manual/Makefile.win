#-*-Makefile-*-
#
# ${HYANG_HOME}/doc/manual/Makefile.win

ECHO = echo

include ../../src/hyangwin32/MkRules

ifeq "$(MIKTEX)" "TRUE"
HYANG_TEXOPTS = --include-directory=../../share/texmf/tex/latex
else
HYANG_TEXOPTS =
endif

HYANG_EXE = ../../$(BINDIR)/HyangTerm

include ../../share/make/vars.mk

srcdir = .
top_srcdir = ../..
subdir = doc/manual

SOURCES_TEXI = hyang-FAQ.texi hyang-get-started.texi hyang-data.texi hyang-library-reference.texi \
  hyang-language-concept.texi hyang-internals.texi hyang-language-reference.texi

OBJECTS_HTML = $(SOURCES_TEXI:.texi=.html)
OBJECTS_INFO = $(SOURCES_TEXI:.texi=.info)
OBJECTS_PDF = $(SOURCES_TEXI:.texi=.pdf)

PDFLATEX = pdflatex $(HYANG_TEXOPTS)
PDFTEX = pdftex $(HYANG_TEXOPTS)
BATCHPDFLATEX = $(PDFLATEX) -interaction=nonstopmode

MAKEINDEX = LC_ALL=C makeindex
TEXINDEX = LC_ALL=C texindex

MAKEINFO_HTML_OPTS = --html --no-split --css-include=$(srcdir)/hyangman.css
MAKEINFO_TEXT_OPTS = --number-sections --fill-column=76 --no-split --no-headers
TEXI2HTML = LC_ALL=C $(MAKEINFO) $(MAKEINFO_HTML_OPTS)
TEXI2TEXT = LC_ALL=C $(MAKEINFO) $(MAKEINFO_TEXT_OPTS)

HYANG_PPRSIZE = a4
HYANG_HYD4PDF = times,inconsolata,hyper

texinputs_BASE = $(HYANG_PKGS_BASE:=-pkg.tex) $(ABAH_COMPILER:=-pkg.tex)
texinputs_PREBUILT = $(HYANG_PKGS_PREBUILT:=-pkg.tex)
refman_DEPENDENCIES = version.tex $(top_srcdir)/share/texmf/tex/latex/hyd.sty
fullrefman_TEXINPUTS = $(texinputs_BASE) $(texinputs_PREBUILT)
HYSCM_CHECKIN = $(top_srcdir)/hyscmterm.id

texiincludes = version.texi $(srcdir)/hyang-defs.texi

.SUFFIXES:
.SUFFIXES: .html .info .texi .pdf

all: pdf

ifneq "$(MAKEINFO)" "missing"
.texi.html:
	@$(ECHO) "creating doc/manual/$@"
	@$(TEXI2HTML) -D UseExternalXrefs -I$(srcdir) $< -o $@.tmp || touch $@
	@$(SED) -f $(srcdir)/quot.sed $@.tmp > $@
	@rm -f $@.tmp

hyang-get-started.html: $(srcdir)/hyang-get-started.texi
	@$(ECHO) "creating doc/manual/$@"
	@$(TEXI2HTML) -I$(srcdir) $(srcdir)/hyang-get-started.texi -o $@.tmp || touch $@
	@$(SED) -f $(srcdir)/quot.sed $@.tmp > $@
	@rm -f $@.tmp

hyang-FAQ.html: hyang-FAQ.texi
	@$(ECHO) "creating doc/manual/$@"
	@LC_ALL=C $(MAKEINFO) --html --no-split --css-include=hyangfaq.css -D UseExternalXrefs -I$(srcdir) $< -o $@.tmp || touch $@
	@$(SED) -f $(srcdir)/quot.sed $@.tmp > $@
	@rm -f $@.tmp

.texi.info:
	LC_ALL=C $(MAKEINFO) --enable-encoding -D UseExternalXrefs -I$(srcdir) $<
else
.texi.html:
	@$(ECHO) "texi2any is not available"

hyang-get-started.html: $(srcdir)/hyang-get-started.texi
	@$(ECHO) "texi2any is not available"

hyang-FAQ.html: hyang-FAQ.texi
	@$(ECHO) "texi2any is not available"

.texi.info:
	@$(ECHO) "texi2any is not available"
endif

ifeq ($(strip $(TEXI2DVI)),)
.texi.pdf:
	$(PDFTEX) $<
	$(TEXINDEX) $*.cp $*.fn $*.vr
	$(PDFTEX) $<
	$(PDFTEX) $<
else
.texi.pdf:
	$(TEXI2DVI) --pdf --texinfo="@set UseExternalXrefs " $<
endif


html: $(OBJECTS_HTML)
$(OBJECTS_HTML): $(texiincludes) hyangman.css

info: $(OBJECTS_INFO)
$(OBJECTS_INFO): $(texiincludes)

pdf: fullrefman.pdf $(OBJECTS_PDF)
$(OBJECTS_PDF): $(texiincludes)

refman.pdf: $(refman_DEPENDENCIES) $(texinputs_BASE) refman.top refman.bot
	@$(ECHO) " PDF/LaTeX documentation: reference index ..."
	@(opt="$(HYANG_PPRSIZE)paper"; \
	  $(ECHO) "\\documentclass[$${opt}]{book}"; \
	  opt="$(HYANG_HYD4PDF)"; \
	  $(ECHO) "\\usepackage[$${opt}]{hyd}"; \
	  $(ECHO) "\\usepackage[utf8]{inputenc}"; \
	  cat $(srcdir)/refman.top; \
	  texinputs=`(for f in $(texinputs_BASE); \
	    do $(ECHO) $${f}; done) | LC_COLLATE=C $(SORT)`; \
	  for f in $${texinputs}; do $(ECHO) "\\input{$${f}}"; done; \
	  cat $(srcdir)/refman.bot) > refman.tex
	@$(RM) -f *.aux refman.toc refman.ind
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) refman.tex
	@$(MAKEINDEX) refman
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) refman.tex
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) refman.tex

fullrefman.pdf: $(fullrefman_TEXINPUTS) $(refman_DEPENDENCIES) \
   refman.top refman.bot
	@$(ECHO) " PDF/LaTeX documentation: full reference index ..."
	@(opt="$(HYANG_PPRSIZE)paper"; \
	  $(ECHO) "\\documentclass[$${opt}]{book}"; \
	  opt="$(HYANG_HYD4PDF)"; \
	  $(ECHO) "\\usepackage[$${opt}]{hyd}"; \
	  $(ECHO) "\\usepackage[utf8]{inputenc}"; \
	  cat $(srcdir)/refman.top; \
	  $(ECHO) "\\part{}"; \
	  texinputs=`(for f in $(texinputs_BASE); \
	    do $(ECHO) $${f}; done) | LC_COLLATE=C $(SORT)`; \
	  for f in $${texinputs}; do $(ECHO) "\\input{$${f}}"; done; \
	  $(ECHO) "\\part{}"; \
	  texinputs=`(for f in $(texinputs_PREBUILT); \
	    do $(ECHO) $${f}; done) | LC_COLLATE=C $(SORT)`; \
	  for f in $${texinputs}; do $(ECHO) "\\input{$${f}}"; done; \
	  cat $(srcdir)/refman.bot) > fullrefman.tex
	@$(RM) -f *.aux fullrefman.toc fullrefman.ind
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) fullrefman.tex
	@$(MAKEINDEX) fullrefman
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) fullrefman.tex
	@TEXINPUTS="$(top_srcdir)/share/texmf/tex/latex;$$TEXINPUTS" \
	  $(BATCHPDFLATEX) fullrefman.tex

version.tex: $(top_srcdir)/VERSION $(HYSCM_CHECKIN)
	@$(ECHO) "creating $(subdir)/$@"
	@(v=`cat $(top_srcdir)/VERSION`; \
	  v="$${v} (`cut -c-10 $(HYSCM_CHECKIN)`)"; \
	  $(ECHO) "$${v}") > $@

%-pkg.tex: FORCE
	@$(ECHO) "collecting LaTeX docs for package \`$*' ..."
	@$(ECHO) "tools:::.pkg2tex(\"$(top_srcdir)/library/$*\")" \
	    | LC_ALL=C $(HYANG_EXE) --slave --vanilla
FORCE:

version.texi: Makefile.win $(top_srcdir)/VERSION $(HYSCM_CHECKIN)
	@$(ECHO) "creating $(subdir)/$@"
	@(vv=`sed 's/ *(.*//' $(top_srcdir)/VERSION`;v=`echo $${vv}| sed 's/\([^ ]*\).*/\1/'`; \
	  $(ECHO) "@set VERSIONno $${v}" > $@; \
	  v="$${vv} (`cut -c-10 $(HYSCM_CHECKIN)`)"; \
	  $(ECHO) "@set VERSION $${v}" >> $@; \
	  hwv=$(shell ../../bin$(HYANG_ARCH)/hyangscript ../../src/hyangwin32/fixed/hyangwinver.hyang); \
	  $(ECHO) "@set HYANGWINVER $${hwv}" >> $@ )
	@if test "$(HYANG_PPRSIZE)" = "a4"; then \
	  $(ECHO) "@afourpaper" >> $@ ; \
	fi


mostlyclean: clean
clean:
	@-rm -f *.aux *.toc *refman.i?? *.out *.log
	@-rm -f *.cp *.cps *.en *.ens *.fn *.fns *.ky *.kys \
	  *.out *.pg *.pgs *.tmp *.tp *.vr *.vrs \
	  version.tex version.texi refman.tex fullrefman.tex *-pkg.tex
distclean: clean
	@-rm -f *.pdf *.info* *.html
maintainer-clean: distclean

ifneq "$(MAKEINFO)" "missing"
../FAQ: hyang-FAQ.texi
	@$(TEXI2TEXT) -o $@ $<
../RESOURCES: resources.texi
	@$(TEXI2TEXT) -o $@ $<
../html/resources.html: resources.texi
	@$(MAKEINFO) --html --no-split --no-headers \
	  --css-include=$(top_srcdir)/doc/html/hyang.css -o $@ $<
else
../FAQ: hyang-FAQ.texi
	@$(ECHO) "texi2any is not available"
../RESOURCES: resources.texi
	@$(ECHO) "texi2any is not available"
../html/resources.html: resources.texi
	@$(ECHO) "texi2any is not available"
endif

hyscmrepo: ../FAQ ../RESOURCES ../html/resources.html

HYANGVER = $(shell cut -d' ' -f1 $(top_srcdir)/VERSION | sed -n 1p)
HYANGPREFIX = $(shell ../../bin$(HYANG_ARCH)/hyangscript ../../src/hyangwin32/fixed/hyangwinver.hyang)
SEDVER = -e s/@HYANGVER@/$(HYANGVER)/g -e s/@HYANGWINVER@/$(HYANGPREFIX)/g

FAQ: hyangwin-FAQ ../html/hyangwin-FAQ.html

ifneq "$(MAKEINFO)" "missing"
../html/hyangwin-FAQ.html: hyangwin-FAQ.texi $(top_srcdir)/VERSION hyangman.css
	@echo "making hyangwin-FAQ.html"
	@$(SED) $(SEDVER) $< | \
	  $(MAKEINFO) --no-split --html --no-headers --number-sections --css-include=hyangman.css -o $@ 

hyangwin-FAQ: hyangwin-FAQ.texi $(top_srcdir)/VERSION
	@echo "making hyangwin-FAQ"
	@$(SED) $(SEDVER) $< | \
	  $(MAKEINFO) --no-headers --number-sections -o $@
else
../html/hyangwin-FAQ.html: hyangwin-FAQ.texi $(top_srcdir)/VERSION hyangman.css
	@$(ECHO) "texi2any is not available"

hyangwin-FAQ: hyangwin-FAQ.texi $(top_srcdir)/VERSION
	@$(ECHO) "texi2any is not available"
endif

hyangwin-FAQ.pdf: hyangwin-FAQ.texi $(top_srcdir)/VERSION
	@$(SED) $(SEDVER) $< > tmp.texi
	@$(TEXI2DVI) --pdf tmp.texi
	@$(TEXI2DVI) --pdf tmp.texi
	@mv tmp.pdf $@
	@rm tmp.*
