/* hyangconfig.h.  Generated automatically */
#ifndef HYANG_HYANGCONFIG_H
#define HYANG_HYANGCONFIG_H

#ifndef HYANG_CONFIG_H

#define HAVE_F77_UNDERSCORE 1
/* all Hyang platforms have this */
#define IEEE_754 1
/* #undef WORDS_BIGENDIAN */
#define HYANGINL inline
/* #undef HAVE_VISIBILITY_ATTRIBUTE */
/* all Hyang platforms have the next two */
#define SUPPORT_UTF8 1
#define SUPPORT_MBCS 1
#define ENABLE_NLS 1
/* #undef HAVE_AQUA */
/* Deprecated: use _OPENMP instead */
/* #undef SUPPORT_OPENMP */
#ifdef _WIN64
#define SIZEOF_SIZE_T 8
#else
#define SIZEOF_SIZE_T 4
#endif
/* #undef HAVE_ALLOCA_H */

#endif /* not HYANG_CONFIG_H */

#endif /* not HYANG_HYANGCONFIG_H */
