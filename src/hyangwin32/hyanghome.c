/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif
#undef sprintf

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

static char hyanghomebuf[MAX_PATH];

#define GOBACKONESLASH \
    p = strrchr(hyanghomebuf,'\\'); \
    if (!p) { \
	MessageBox(NULL, "Installation problem", "Terminating", \
		   MB_TASKMODAL | MB_ICONSTOP | MB_OK);\
	exit(1); \
    } \
    *p = '\0'

char *getHyangHOMElong(int m)
{
    char *p;

    GetModuleFileName(NULL, hyanghomebuf, MAX_PATH);
    for(int i=0; i < m; i++) {GOBACKONESLASH;}
    return (hyanghomebuf);
}

char *getHyangHOME(int m)
{
    char *p;
    int hasspace = 0;

    getHyangHOMElong(m);
    for (p = hyanghomebuf; *p; p++)
	if (isspace(*p)) { hasspace = 1; break; }
    if (hasspace)
	GetShortPathName(hyanghomebuf, hyanghomebuf, MAX_PATH);
    return (hyanghomebuf);
}


char *get_hyang_HOME(void)
{
    LONG rc;
    HKEY hkey;
    DWORD keytype = REG_SZ, cbData = sizeof(hyanghomebuf);

    if(getenv("HYANG_HOME")) {
	strncpy(hyanghomebuf, getenv("HYANG_HOME"), MAX_PATH);
	return (hyanghomebuf);
    }

    if (GetEnvironmentVariable ("HYANG_HOME", hyanghomebuf, sizeof (hyanghomebuf)) > 0)
	return (hyanghomebuf);

    rc = RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\HLF\\hyang", 0,
		      KEY_READ, &hkey);
    if (rc == ERROR_SUCCESS) {
	rc = RegQueryValueEx(hkey, "InstallPath", 0, &keytype,
			     (LPBYTE) hyanghomebuf, &cbData);
	RegCloseKey (hkey);
    } else return NULL;
    if (rc != ERROR_SUCCESS) return NULL;
    return hyanghomebuf;
}
