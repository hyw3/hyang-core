/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

struct structRPIPE {
    PROCESS_INFORMATION pi;
    HANDLE thread;
    HANDLE read, write;
    int exitcode, active, timedout;
    DWORD timeoutMillis;
};

typedef struct structRPIPE rpipe;

/*
 * runcmd and hyangPipeClose return the exit code of the process
 * if runcmd return NOLAUNCH, problems in process start
*/
#define runcmd hyangly_runcmd
int runcmd(const char *cmd, cetype_t enc, int wait, int visible, 
           const char *fin, const char *fout, const char *ferr);

int runcmd_timeout(const char *cmd, cetype_t enc, int wait, int visible, 
                   const char *fin, const char *fout, const char *ferr,
                   int timeout, int *timedout);

rpipe *hyangPipeOpen(const char *cmd, cetype_t enc, int visible, 
		 const char *finput, int io,
		 const char *fout, const char *ferr,
		 int timeout);
char  *hyangPipeGets(rpipe *r, char *buf, int len);
int hyangPipeGetC(rpipe *r);
int hyangPipeClose(rpipe *r, int *timedout);

char *runerror(void);

/* Changed in Hyang to be the conventional Unix value -- previously -1 */
#define NOLAUNCH 127
