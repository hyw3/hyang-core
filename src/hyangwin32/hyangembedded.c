/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <config.h>
#include <hyangdefn.h>
#include <hyangmbd.h>

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <stdio.h>
#include <hyangversion.h>
#include <hyangexts/hyangextstu.h>
#include "graphapp/ga.h"

#include <psignal.h>

extern int UserBreak;

extern char *getDLLVersion(), *getHyangUsr(), *get_hyang_HOME();
extern void hyang_DefParams(hyangstart), hyang_SetParams(hyangstart), hyang_setStartTime();
extern void ProcessEvents(void);
extern int hyang_ReplDLLdo1();

static int myReadConsole(const char *prompt, char *buf, int len,
			 int addtohistory)
{
    fputs(prompt, stdout);
    fflush(stdout);
    if(fgets(buf, len, stdin)) return 1;
    else return 0;
}

static void myWriteConsole(const char *buf, int len)
{
    printf("%s", buf);
}

static void myCallBack()
{
}

static void myBusy(int which)
{
}

static void my_onintr(int sig)
{
    UserBreak = 1;
}

extern hyangboolean hyang_Loadhyangconsole;

int hyangly_init_hyang(int argc, char **argv)
{
    structHyangStart rp;
    hyangstart Rp = &rp;
    char hyangversion[25], *hyangHome;

    snprintf(hyangversion, 25, "%s.%s", HYANG_MAJOR, HYANG_MINOR);
    if(strncmp(getDLLVersion(), hyangversion, 25) != 0) {
	fprintf(stderr, "Error: hyang.DLL version does not match\n");
	exit(1);
    }

    hyang_setStartTime();
    hyang_DefParams(Rp);
    if((hyangHome = get_hyang_HOME()) == NULL) {
	fprintf(stderr,
		"HYANG_HOME must be set in the environment or Registry\n");
	exit(2);
    }
    Rp->hyanghome = hyangHome;
    Rp->home = getHyangUsr();
    Rp->CharacterMode = LinkDLL;
    Rp->ReadConsole = myReadConsole;
    Rp->WriteConsole = myWriteConsole;
    Rp->CallBack = myCallBack;
    Rp->ShowMessage = askok;
    Rp->YesNoCancel = askyesnocancel;
    Rp->Busy = myBusy;

    Rp->hyang_Quiet = TRUE;
    Rp->hyang_Interactive = TRUE;
    Rp->RestoreAction = SA_RESTORE;
    Rp->SaveAction = SA_NOSAVE;
    hyang_SetParams(Rp);
    hyang_setCmdLineArgs(argc, argv);

    FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));

    signal(SIGBREAK, my_onintr);
    GA_initapp(0, 0);
    hyang_Loadhyangconsole = FALSE;
    readconsolecfg();

    return 0;
}

int hyangly_initEmbed(int argc, char **argv)
{
    hyangly_init_hyang(argc, argv);
    hyang_MainloopSetup();
    return(1);
}

void hyangly_endEmbed(int fatal)
{
    hyang_RunExitFinalizers();
    CleanEd();
    hyang_CleanTempDir();
    if(!fatal){
	hyangly_StopAllDevcs();
	AllDevicesKilled = TRUE;
    }
    if(!fatal && hyang_CollectWarnings)
	PrintWarnings();
    app_cleanup();
}
