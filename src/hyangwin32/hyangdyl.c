/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <string.h>
#include <stdlib.h>
#include <hyangdefn.h>
#include <hyangmath.h>
#include <direct.h>
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

typedef BOOL (WINAPI *PSDD)(LPCTSTR);

int setDLLSearchPath(const char *path)
{
    int res = 0;
    PSDD p = (PSDD) -1;
    static char wd[MAX_PATH] = "";

    if(p == (PSDD) -1)
	p = (PSDD) GetProcAddress(GetModuleHandle(TEXT("kernel32.dll")),
				  "SetDllDirectoryA");
    if(p) {
	res = p(path);
    } else {
	if(path) {
	    GetCurrentDirectory(MAX_PATH, wd);
	    SetCurrentDirectory(path);
	} else if (wd[0]) {
	    SetCurrentDirectory(wd);
	    wd[0] = '\0';
	}
    }
    return res;
}

#include <hyangexts/hyangdyl.h>
#include <hyangdyp.h>

static void fixPath(char *path)
{
    char *p;
    for(p = path; *p != '\0'; p++) if(*p == '\\') *p = '/';
}

static HINSTANCE hyang_loadLibr(const char *path, int asLocal, int now,
			       const char *search);
static DL_FUNC getRoutine(DllInfo *info, char const *name);
#ifdef CACHE_DLL_SYM
static void hyang_delCachedSyms(DllInfo *dll);
#endif

static void hyang_getDLLErr(char *buf, int len);
static void GetFullDLLPath(SEXP call, char *buf, const char *path);

static void closeLibrary(HINSTANCE handle)
{
    FreeLibrary(handle);
}

void InitFunctionHashing()
{
    hyang_osDynSym->loadLibrary = hyang_loadLibr;
    hyang_osDynSym->dlsym = getRoutine;
    hyang_osDynSym->closeLibrary = closeLibrary;
    hyang_osDynSym->getError = hyang_getDLLErr;

#ifdef CACHE_DLL_SYM
    hyang_osDynSym->deleteCachedSymbols = hyang_delCachedSyms;
    hyang_osDynSym->lookupCachedSymbol = hyangly_lookupCachedSym;
#endif

    hyang_osDynSym->fixPath = fixPath;
    hyang_osDynSym->getFullDLLPath = GetFullDLLPath;
}

#ifdef CACHE_DLL_SYM
static void hyang_delCachedSyms(DllInfo *dll)
{
    int i;
    for(i = nCPFun - 1; i >= 0; i--)
	if(!strcmp(CPFun[i].pkg, dll->name)) {
	    if(i < nCPFun - 1) {
		strcpy(CPFun[i].name, CPFun[--nCPFun].name);
		strcpy(CPFun[i].pkg, CPFun[nCPFun].pkg);
		CPFun[i].func = CPFun[nCPFun].func;
	    } else nCPFun--;
	}
}
#endif

#ifndef _MCW_EM
_CRTIMP unsigned int __cdecl
_controlfp (unsigned int unNew, unsigned int unMask);
_CRTIMP unsigned int __cdecl _clearfp (void);

#define	_MCW_EM		0x0008001F	/* Error masks */
#define	_MCW_IC		0x00040000	/* Infinity */
#define	_MCW_RC		0x00000300	/* Rounding */
#define	_MCW_PC		0x00030000	/* Precision */
#endif

HINSTANCE hyang_loadLibr(const char *path, int asLocal, int now,
			const char *search)
{
    HINSTANCE tdlh;
    unsigned int dllcw, rcw;
    int useSearch = search && search[0];

    rcw = _controlfp(0,0) & ~_MCW_IC;
    _clearfp();
    if(useSearch) setDLLSearchPath(search);
    tdlh = LoadLibrary(path);
    if(useSearch) setDLLSearchPath(NULL);
    dllcw = _controlfp(0,0) & ~_MCW_IC;
    if (dllcw != rcw) {
	_controlfp(rcw, _MCW_EM | _MCW_IC | _MCW_RC | _MCW_PC);
	if (LOGICAL(GetOption1(install("warn.FPU")))[0])
	    warning(_("DLL attempted to change FPU control word from %x to %x"),
		    rcw,dllcw);
    }
    return(tdlh);
}

static DL_FUNC getRoutine(DllInfo *info, char const *name)
{
    DL_FUNC f;
    f = (DL_FUNC) GetProcAddress(info->handle, name);
    return(f);
}

static void hyang_getDLLErr(char *buf, int len)
{
    LPSTR lpMsgBuf, p;
    char *q;
    FormatMessage(
	FORMAT_MESSAGE_ALLOCATE_BUFFER |
	FORMAT_MESSAGE_FROM_SYSTEM |
	FORMAT_MESSAGE_IGNORE_INSERTS,
	NULL,
	GetLastError(),
	MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
	(LPSTR) &lpMsgBuf,
	0,
	NULL
	);
    strcpy(buf, "LoadLibrary failure:  ");
    q = buf + strlen(buf);
    for (p = lpMsgBuf; *p; p++) if (*p != '\r') *q++ = *p;
    LocalFree(lpMsgBuf);
}

static void GetFullDLLPath(SEXP call, char *buf, const char *path)
{
    char *p;

    if ((path[0] != '/') && (path[0] != '\\') && (path[1] != ':')) {
	if (!getcwd(buf, MAX_PATH))
	    errorcall(call, _("cannot get working directory"));
	strcat(buf, "\\");
	strcat(buf, path);
    } else
	strcpy(buf, path);
    for (p = buf; *p; p++) if (*p == '\\') *p = '/';
}
