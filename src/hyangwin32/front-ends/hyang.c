/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdlib.h>
#include <string.h>

extern int hyangcmdfn (int cmdarg, int argc, char **argv);

int main (int argc, char **argv)
{
    int cmdarg = 0;

    if (argc > 1) {
	if (strcmp(argv[1], "-h") == 0
	 	|| strcmp(argv[1], "--help") == 0) cmdarg = 1;
	else {
	    for(int i = 1; i < argc; i++)
		if(strcmp(argv[i], "CMD") == 0) {
		    cmdarg = i + 1;
		    break;
		}
	    if (cmdarg >= 3) {
		char *Init = "HYANG_PROFILE_USER=\r", *Site="HYANG_PROFILE=\r",
		    *Env1="HYANG_ENVIRON=\r", *Env2="HYANG_ENVIRON_USER=\r";
		for(int i = 1; i < cmdarg; i++) {
		    char *a = argv[i];
		    if (strcmp(a, "--no-init-file") == 0 ||
			strcmp(a, "--vanilla") == 0) putenv(Init);
		    if (strcmp(a, "--no-site-file") == 0 ||
			strcmp(a, "--vanilla") == 0) putenv(Site);
		    if (strcmp(a, "--no-environ") == 0 ||
			strcmp(a, "--vanilla") == 0) {
			putenv(Env1); putenv(Env2);
		    }
		}
	    }
	}
    }

    exit(hyangcmdfn(cmdarg, argc, argv));
 }
