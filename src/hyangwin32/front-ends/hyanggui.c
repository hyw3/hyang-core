/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _WIN32_WINNT 0x0501
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <stdio.h>
#include <hyangversion.h>
#include <hyangstu.h>
#include <stdlib.h>

extern void cmdlineoptions(int, char **);
extern int setupui(void);
extern void hyangly_mainloop(void);
__declspec(dllimport) extern UImode CharacterMode;
extern void GA_exitapp(void);

extern char *getDLLVersion(void);

static char hyangversion[25];
char *gethyangVersion(void)
{
    snprintf(hyangversion, 25, "%s.%s", HYANG_MAJOR, HYANG_MINOR);
    return(hyangversion);
}

#include <wincon.h>
typedef BOOL (*AC)(DWORD);

int AppMain(int argc, char **argv)
{
    CharacterMode = hyangGui;
    if(strcmp(getDLLVersion(), gethyangVersion()) != 0) {
	MessageBox(0, "hyang.DLL version does not match", "Terminating",
		   MB_TASKMODAL | MB_ICONSTOP | MB_OK);
	exit(1);
    }
    cmdlineoptions(argc, argv);
    if (!setupui()) {
        MessageBox(0, "Error setting up console.  Try --vanilla option.",
                      "Terminating", MB_TASKMODAL | MB_ICONSTOP | MB_OK);
        GA_exitapp();
    }


    if (AttachConsole(ATTACH_PARENT_PROCESS))
    {
	freopen("CONIN$", "r", stdin);
	freopen("CONOUT$", "w", stdout);
	freopen("CONOUT$", "w", stderr);
    }

    hyangly_mainloop();
    return 0;
}
