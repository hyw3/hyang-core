/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <stdio.h>
#include <io.h>
#include <hyangversion.h>
#include <hyangstu.h>
#include <psignal.h>
#include "../getline/getline.h"

extern void cmdlineoptions(int, char **);
extern void readconsolecfg(void);
extern int GA_initapp(int, char **);
extern void hyangly_mainloop(void);
__declspec(dllimport) extern UImode CharacterMode;
__declspec(dllimport) extern int UserBreak;
__declspec(dllimport) extern int hyang_Interactive;
__declspec(dllimport) extern int hyang_HistorySize;
__declspec(dllimport) extern int hyang_RestoreHistory;
__declspec(dllimport) extern char *hyang_HistoryFile;

extern char *getDLLVersion(void);
extern void saveConsoleTitle(void);
extern void hyang_gl_tab_set(void);

static char hyangversion[25];
char *gethyangVersion(void)
{
    snprintf(hyangversion, 25, "%s.%s", HYANG_MAJOR, HYANG_MINOR);
    return(hyangversion);
}

static DWORD mainThreadId;

static void my_onintr(int nSig)
{
  UserBreak = 1;
  PostThreadMessage(mainThreadId,0,0,0);
}

int AppMain(int argc, char **argv)
{
    CharacterMode = hyangTerm;
    if(strcmp(getDLLVersion(), gethyangVersion()) != 0) {
	fprintf(stderr, "Error: hyang.DLL version does not match\n");
	exit(1);
    }
    if (isatty(0))
	FlushConsoleInputBuffer(GetStdHandle(STD_INPUT_HANDLE));
    cmdlineoptions(argc, argv);
    mainThreadId = GetCurrentThreadId() ;
    SetConsoleCtrlHandler(NULL, FALSE);
    signal(SIGBREAK, my_onintr);
    GA_initapp(0, NULL);
    readconsolecfg();
    if(hyang_Interactive) {
	hyang_gl_tab_set();
	gl_hist_init(hyang_HistorySize, 1);
	if (hyang_RestoreHistory) gl_loadhistory(hyang_HistoryFile);
	saveConsoleTitle();
#ifdef _WIN64
	SetConsoleTitle("HyangTerm (64-bit)");
#else
	SetConsoleTitle("HyangTerm (32-bit)");
#endif
    }
    hyangly_mainloop();
    return 0;
}
