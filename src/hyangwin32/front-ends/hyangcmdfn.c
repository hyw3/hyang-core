/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include <hyangversion.h>

#ifndef BINDIR
# define BINDIR "bin"
#endif

extern char *getHyangHOME(int), *getHyangUsr(void);

void hyang_Suicide(char *s)
{
    fprintf(stderr, "FATAL ERROR:%s\n", s);
    exit(2);
}

static int pwait(HANDLE p)
{
    DWORD ret;

    WaitForSingleObject(p, INFINITE);
    GetExitCodeProcess(p, &ret);
    return ret;
}

# include <sys/stat.h>

#if !defined(S_IFDIR) && defined(__S_IFDIR)
# define S_IFDIR __S_IFDIR
#endif

static int isDir(char *path)
{
    struct stat sb;
    int isdir = 0;
    if(path[0] && stat(path, &sb) == 0)
	isdir = (sb.st_mode & S_IFDIR) > 0;
    return isdir;
}


void hyangcmdusage (char *HYANGCMD)
{
    fprintf(stderr, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s",
	    "where 'command' is one of:\n",
	    "  INSTALL     Install add-on packages\n",
	    "  REMOVE      Remove add-on packages\n",
	    "  SHLIB       Make a DLL for use with dynload\n",
	    "  BATCH       Run Hyang in batch mode\n",
	    "  build       Build add-on packages\n",
	    "  check       Check add-on packages\n",
	    "  hyangprof   Post process Hyang profiling files\n",
	    "  hydconv     Convert hyd format to various other formats\n",
	    "  hyangdiff   difference Hyang output files\n",
	    "  hyd2pdf     Convert hyd format to PDF\n",
	    "  hyd2txt     Convert hyd format to pretty text\n",
	    "  Stangle     Extract S/hyang code from vignette\n",
	    "  Sweave      Process vignette documentation\n",
	    "  config      Obtain configuration information about Hyang\n"
	    "  open        Open a file via Windows file associations\n"
	    "  texify      Process a latex file\n"
	    );

    fprintf(stderr, "\n%s%s%s%s",
	    "Use\n  ", HYANGCMD, " command --help\n",
	    "for usage information for each command.\n\n");
}

#define PROCESS_CMD(ARG)	for (i = cmdarg + 1; i < argc; i++) {\
	    strcat(cmd, ARG);\
	    if (strlen(cmd) + strlen(argv[i]) > 9900) {\
		fprintf(stderr, "command line too long\n");\
		return(27);\
	    }\
	    strcat(cmd, argv[i]);\
	}\
	return(system(cmd))


extern int process_hyangenviron(const char *filename);
#define CMD_LEN 10000
int hyangcmdfn (int cmdarg, int argc, char **argv)
{
    int i, iused, status = 0;
    char *p, cmd[CMD_LEN];
    char HYANGCMD[] = "hyang CMD";
    int len = strlen(argv[0]);
    char env_path[MAX_PATH];
    int timing = 1;
    char *hyangHome = getHyangHOME(3);

    if(!strncmp(argv[0]+len-4, "hyangcmd", 4) ||
       !strncmp(argv[0]+len-4, "hyangcmd", 4) ||
       !strncmp(argv[0]+len-8, "hyangcmd.exe", 8) ||
       !strncmp(argv[0]+len-8, "hyangcmd.exe", 8))
	strcpy(HYANGCMD, "hyangcmd");


    if (argc <= cmdarg) {
	fprintf(stderr, "%s%s%s", "Usage: ", HYANGCMD, " command args\n\n");
	hyangcmdusage(HYANGCMD);
	return(0);
    }

    if (argc == cmdarg+1 &&
	(!strcmp(argv[cmdarg], "--help") || !strcmp(argv[cmdarg], "-h"))
	) {
	if(cmdarg >= 2 || (cmdarg == 1 && !strcmp(HYANGCMD, "hyangcmd"))) {
	    fprintf(stderr, "%s%s%s", "Usage: ", HYANGCMD, " command args\n\n");
	    hyangcmdusage(HYANGCMD);
	    return(0);
	}

	snprintf(cmd, CMD_LEN, "\"%s/%s/HyangTerm.exe\" --help", getHyangHOME(3), BINDIR);
	system(cmd);
	fprintf(stderr, "%s", "\n\nOr: hyang CMD command args\n\n");
	hyangcmdusage(HYANGCMD);
	return(0);
    }

    if (cmdarg == 0) {
	if (argc > 1 && !strcmp(argv[1], "HYANGHOME")) {
	    fprintf(stdout, "%s", getHyangHOME(3));
	    return(0);
	}
	snprintf(cmd, CMD_LEN, "\"\"%s/%s/HyangTerm.exe\"", getHyangHOME(3), BINDIR);
	for (i = cmdarg + 1; i < argc; i++){
	    strcat(cmd, " ");
	    if (strlen(cmd) + strlen(argv[i]) > 9900) {
		fprintf(stderr, "command line too long\n");
		return(27);
	    }
	    if(strchr(argv[i], ' ') || !strlen(argv[i])) {
		strcat(cmd, "\"");
		strcat(cmd, argv[i]);
		strcat(cmd, "\"");
	    } else strcat(cmd, argv[i]);
	}
	strcat(cmd, "\"");
	SetConsoleCtrlHandler(NULL, TRUE);
	return system(cmd);
    }


    char HYANGHOME[MAX_PATH];
    strcpy(HYANGHOME, "HYANG_HOME=");
    strcat(HYANGHOME, hyangHome);
    for (p = HYANGHOME; *p; p++) if (*p == '\\') *p = '/';
    putenv(HYANGHOME);

    strcpy(env_path, hyangHome); strcat(env_path, "/etc/hyangcmd_environ");
    process_hyangenviron(env_path);

    if (!strcmp(argv[cmdarg], "BATCH")) {
	char infile[MAX_PATH], outfile[MAX_PATH], *p, cmd_extra[CMD_LEN];
	DWORD ret;
	SECURITY_ATTRIBUTES sa;
	PROCESS_INFORMATION pi;
	STARTUPINFO si;
	HANDLE hOUT = INVALID_HANDLE_VALUE;

	cmd_extra[0] = '\0';
	if((p = getenv("HYANG_BATCH_OPTIONS")) && strlen(p)) {
	    if(1+strlen(p) >= CMD_LEN) {
		fprintf(stderr, "command line too long\n");
		return(27);
	    }
	    strcat(cmd_extra, " ");
	    strcat(cmd_extra, p);
	}

	for(i = cmdarg + 1, iused = cmdarg; i < argc; i++) {
	    if (!strcmp(argv[i], "-h") || !strcmp(argv[i], "--help")) {
		fprintf(stderr, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s\n",
"Usage: ", HYANGCMD, " BATCH [options] infile [outfile]\n\n",
"Run Hyang non-interactively with input from infile and place output (stdout\n",
"and stderr) to another file.  If not given, the name of the output file\n",
"is the one of the input file, with a possible '.hyang' extension stripped,\n",
"and '.hyout' appended.\n\n",
"Options:\n"
"  -h, --help		print short help message and exit\n",
"  -v, --version	print version info and exit\n",
"  --no-timing		do not report the timings\n",
"  --			end processing of options\n\n",
"Further arguments starting with a '-' are considered as options as long\n",
"as '--' was not encountered, and are passed on to the Hyang process, which\n",
"by default is started with '--restore --save'.\n\n",
"Report bugs to contributing@hyang.org.");
		return(0);
	    }
	    if (!strcmp(argv[i], "-v") || !strcmp(argv[i], "--version")) {
		fprintf(stderr, "BATCH %s\n%s%s%s\n", "1.2",
"Copyright (C) 2017-2019 The Hyang Language Foundation.\n",
"This is free software; see the GNU General Public Licence version 3\n",
"or later for copying conditions.  There is NO warranty.");
		return(0);
	    }
	    if (!strcmp(argv[i], "--no-timing")) {
		timing = 0;
		iused = i;
		continue;
	    }

	    if (!strcmp(argv[i], "--")) {
		iused = i;
		break;
	    }
	    if (argv[i][0] == '-') {
		if (strlen(cmd_extra) + strlen(argv[i]) > 9900) {
		    fprintf(stderr, "command line too long\n");
		    return(27);
		}
		strcat(cmd_extra, " ");
		strcat(cmd_extra, argv[i]);
		iused = i;
	    } else break;
	}
	if (iused+1 < argc)
	    strcpy(infile, argv[iused+1]);
	else {
	    fprintf(stderr, "no input file\n");
	    return(1);
	}
	if (iused+2 < argc)
	    strcpy(outfile, argv[iused+2]);
	else {
	    int len = strlen(infile);
	    strcpy(outfile, infile);
	    if (!strcmp(outfile+len-2, ".hyang")) strcat(outfile, "out");
	    else strcat(outfile, ".hyout");
	}

	snprintf(cmd, CMD_LEN, "\"%s/%s/HyangTerm.exe\" -f \"%s\" --restore --save",
		 getHyangHOME(3), BINDIR, infile);
	if(strlen(cmd) + strlen(cmd_extra) >= CMD_LEN) {
	    fprintf(stderr, "command line too long\n");
	    return(27);
	}
	strcat(cmd, cmd_extra);

	if(timing) putenv("HYANG_BATCH=1234");

	sa.nLength = sizeof(sa);
	sa.lpSecurityDescriptor = NULL;
	sa.bInheritHandle = TRUE;

	hOUT = CreateFile(outfile, GENERIC_WRITE, FILE_SHARE_READ,
			  &sa, CREATE_ALWAYS, 0, NULL);
	if (hOUT == INVALID_HANDLE_VALUE) {
	    fprintf(stderr, "unable to open output file\n");
	    return(2);
	}
	SetStdHandle(STD_OUTPUT_HANDLE, hOUT);
	SetStdHandle(STD_ERROR_HANDLE, hOUT);
	si.cb = sizeof(si);
	si.lpReserved = NULL;
	si.lpReserved2 = NULL;
	si.cbReserved2 = 0;
	si.lpDesktop = NULL;
	si.lpTitle = NULL;
	si.dwFlags = STARTF_USESHOWWINDOW;
	si.wShowWindow = SW_SHOWDEFAULT;
	ret = CreateProcess(0, cmd, &sa, &sa, TRUE, 0, NULL, NULL, &si, &pi);
	CloseHandle(hOUT);
	if (!ret) {
	    fprintf(stderr, "unable to run HyangTerm.exe\n");
	    return(3);
	}
	CloseHandle(pi.hThread);
	return(pwait(pi.hProcess));
    }

    char hyangversion[25];
    snprintf(hyangversion, 25, "HYANG_VERSION=%s.%s", HYANG_MAJOR, HYANG_MINOR);
    putenv(hyangversion);

    putenv("HYANG_CMD=hyang CMD");

    char *Path = malloc( strlen(getenv("PATH")) + MAX_PATH + 10 );
    if (!Path) {fprintf(stderr, "PATH too long\n"); return(4);}
    strcpy(Path, "PATH=");
    strcat(Path, hyangHome);
    strcat(Path, "\\");
    strcat(Path, BINDIR);
    strcat(Path, ";");
    strcat(Path, getenv("PATH"));
    putenv(Path);
    free(Path);

    char Rarch[30];
    strcpy(Rarch, "HYANG_ARCH=/");
    strcat(Rarch, HYANG_ARCH);
    putenv(Rarch);

    char Bindir[30];
    strcpy(Bindir, "BINDIR=");
    strcat(Bindir, BINDIR);
    putenv(Bindir);

    char Tmpdir[MAX_PATH+10];
    if ( (p = getenv("TMPDIR")) && isDir(p)) {
    } else {
	if ( (p = getenv("TEMP")) && isDir(p)) {
	    strcpy(Tmpdir, "TMPDIR=");
	    strcat(Tmpdir, p);
	    putenv(Tmpdir);
	} else if ( (p = getenv("TMP")) && isDir(p)) {
	    strcpy(Tmpdir, "TMPDIR=");
	    strcat(Tmpdir, p);
	    putenv(Tmpdir);
	} else {
	    strcpy(Tmpdir, "TMPDIR=");
	    strcat(Tmpdir, getHyangUsr());
	    putenv(Tmpdir);
	}
    }

    char HOME[MAX_PATH+10];
    if( !getenv("HOME") ) {
	strcpy(HOME, "HOME=");
	strcat(HOME, getHyangUsr());
	putenv(HOME);
    }


    if (!strcmp(argv[cmdarg], "INSTALL")) {
	fprintf(stderr, "In hyang CMD INSTALL\n");
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.install_packages() HYANG_DEFAULT_PKGS= LC_COLLATE=C --no-restore --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "REMOVE")) {
	snprintf(cmd, CMD_LEN,
		 "\"\"%s/%s/HyangTerm.exe\" -f \"%s/share/hyang/REMOVE.hyang\" HYANG_DEFAULT_PKGS=NULL --no-restore --slave --args",
		 getHyangHOME(3), BINDIR, getHyangHOME(3));
	for (i = cmdarg + 1; i < argc; i++){
	    strcat(cmd, " ");
	    if (strlen(cmd) + strlen(argv[i]) > 9900) {
		fprintf(stderr, "command line too long\n");
		return(27);
	    }

	    if(strchr(argv[i], ' ')) {
		strcat(cmd, "\"");
		strcat(cmd, argv[i]);
		strcat(cmd, "\"");
	    } else strcat(cmd, argv[i]);
	}

	strcat(cmd, "\"");
	return(system(cmd));
    } else if (!strcmp(argv[cmdarg], "build")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.build_packages() HYANG_DEFAULT_PKGS= LC_COLLATE=C --no-restore --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "check")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.check_packages() HYANG_DEFAULT_PKGS= LC_COLLATE=C --no-restore --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "hyangprof")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.hyangprof() HYANG_DEFAULT_PKGS=utils LC_COLLATE=C --vanilla --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
   } else if (!strcmp(argv[cmdarg], "texify")) {
	if (argc < cmdarg+2) {
	    fprintf(stderr, "\nUsage: %s texify [options] filename\n", HYANGCMD);
	    return(1);
	}
	snprintf(cmd, CMD_LEN,
		 "texify.exe -I \"%s/share/texmf/tex/latex\" -I \"%s/share/texmf/bibtex/bst\"", getHyangHOME(3), getHyangHOME(3));
	PROCESS_CMD(" ");
    } else if (!strcmp(argv[cmdarg], "SHLIB")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.SHLIB() HYANG_DEFAULT_PKGS=NULL --no-restore --slave --no-site-file --no-init-file --args",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD(" ");
    } else if (!strcmp(argv[cmdarg], "hyangdiff")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.hyangdiff() HYANG_DEFAULT_PKGS=NULL --vanilla --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "hydconv")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.hydconv() HYANG_DEFAULT_PKGS= LC_COLLATE=C --vanilla --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "hyd2txt")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::.hydconv() HYANG_DEFAULT_PKGS= LC_COLLATE=C --vanilla --slave --args nextArg-tnextArgtxt",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "hyd2pdf")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" -e tools:::..hyd2pdf() HYANG_DEFAULT_PKGS= LC_ALL=C --vanilla --slave --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "Sweave")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" --no-restore --slave -e utils:::.Sweave() --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else if (!strcmp(argv[cmdarg], "Stangle")) {
	snprintf(cmd, CMD_LEN,
		 "\"%s/%s/HyangTerm.exe\" --vanilla --slave -e utils:::.Stangle() --args ",
		 getHyangHOME(3), BINDIR);
	PROCESS_CMD("nextArg");
    } else {
	p = argv[cmdarg];
	if (!strcmp(p, "config"))
	    snprintf(cmd, CMD_LEN, "\"sh \"%s/bin/config.sh\"", hyangHome);
	else if (!strcmp(p, "open"))
	    snprintf(cmd, CMD_LEN, "\"\"%s/%s/open.exe\"", hyangHome, BINDIR);
	else {
	    if (!strcmp(".sh", p + strlen(p) - 3)) strcpy(cmd, "\"sh ");
	    else if (!strcmp(".pl", p + strlen(p) - 3)) strcpy(cmd, "\"perl ");
	    else strcpy(cmd, "\"");
	    strcat(cmd, p);
	}

	for (i = cmdarg + 1; i < argc; i++){
	    strcat(cmd, " ");
	    if (strlen(cmd) + strlen(argv[i]) > 9900) {
		fprintf(stderr, "command line too long\n");
		return(27);
	    }
	    if(strchr(argv[i], ' ')) {
		strcat(cmd, "\"");
		strcat(cmd, argv[i]);
		strcat(cmd, "\"");
	    } else strcat(cmd, argv[i]);
	}
	strcat(cmd, "\"");
	status = system(cmd);
    }
    return(status);
}
