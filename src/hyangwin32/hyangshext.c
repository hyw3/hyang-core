/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>
#include <shlobj.h>

static int ShellGetPersonalDirectory(char *folder)
{
    LPMALLOC g_pMalloc;
    LPITEMIDLIST pidlUser;
    int result;

    result = 0;

    if (SUCCEEDED(SHGetMalloc(&g_pMalloc))) {

	if (SUCCEEDED(SHGetSpecialFolderLocation(0, CSIDL_PERSONAL, &pidlUser))) {
	    if (SUCCEEDED(SHGetPathFromIDList(pidlUser, folder))) result = 1;
	    g_pMalloc->lpVtbl->Free(g_pMalloc, pidlUser);
	}
    }
    return(result);
}


static char hyangUser[MAX_PATH];
#include <winbase.h>
extern void hyang_Suicide(char *s);

char *getHyangUsr()
{
    char *p, *q;

    if ((p = getenv("HYANG_USER"))) {
	if(strlen(p) >= MAX_PATH) hyang_Suicide("Invalid HYANG_USER");
	strcpy(hyangUser, p);
    } else if ((p = getenv("HOME"))) {
	if(strlen(p) >= MAX_PATH) hyang_Suicide("Invalid HOME");
	strcpy(hyangUser, p);
    } else if (ShellGetPersonalDirectory(hyangUser)) {
    } else if ((p = getenv("HOMEDRIVE")) && (q = getenv("HOMEPATH"))) {
	if(strlen(p) >= MAX_PATH) hyang_Suicide("Invalid HOMEDRIVE");
	strcpy(hyangUser, p);
	if(strlen(hyangUser) + strlen(q) >= MAX_PATH)
	    hyang_Suicide("Invalid HOMEDRIVE+HOMEPATH");
	strcat(hyangUser, q);
    } else {
	GetCurrentDirectory(MAX_PATH, hyangUser);
    }
    p = hyangUser + (strlen(hyangUser) - 1);
    if (*p == '/' || *p == '\\') *p = '\0';
    return hyangUser;
}
