[Icons]
Name: "{group}\Hyang i386 @HYANGVER@"; Filename: "{app}\bin\i386\hyangGUI.exe"; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('i386')
Name: "{group}\Hyang x64 @HYANGVER@"; Filename: "{app}\bin\x64\hyangGUI.exe"; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('x64') and Is64BitInstallMode

Name: "{commondesktop}\hyang i386 @HYANGVER@"; Filename: "{app}\bin\i386\hyangGUI.exe"; MinVersion: 0,5.0; Tasks: desktopicon; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('i386')
Name: "{commondesktop}\hyang x64 @HYANGVER@"; Filename: "{app}\bin\x64\hyangGUI.exe"; MinVersion: 0,5.0; Tasks: desktopicon; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('x64') and Is64BitInstallMode

Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Hyang i386 @HYANGVER@"; Filename: "{app}\bin\i386\hyangGUI.exe"; Tasks: quicklaunchicon; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('i386')
Name: "{userappdata}\Microsoft\Internet Explorer\Quick Launch\Hyang x64 @HYANGVER@"; Filename: "{app}\bin\x64\hyangGUI.exe"; Tasks: quicklaunchicon; WorkingDir: "{app}"; Parameters: "--cd-to-userdocs"; Check: isComponentSelected('x64') and Is64BitInstallMode 

[Registry] 
Root: HKLM; Subkey: "Software\@Producer@"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode

Root: HKLM; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKLM; Subkey: "Software\@Producer@\hyang64\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode 
Root: HKLM; Subkey: "Software\@Producer@\hyang64\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode

Root: HKLM32; Subkey: "Software\@Producer@"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')

Root: HKLM32; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang32\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')
Root: HKLM32; Subkey: "Software\@Producer@\hyang32\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: IsAdmin and isComponentSelected('i386')

Root: HKCU; Subkey: "Software\@Producer@"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: NonAdmin
Root: HKCU; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: NonAdmin
Root: HKCU; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin
Root: HKCU; Subkey: "Software\@Producer@\hyang"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: NonAdmin
Root: HKCU; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: NonAdmin
Root: HKCU; Subkey: "Software\@Producer@\hyang\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin


Root: HKCU; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: NonAdmin and isComponentSelected('i386')
Root: HKCU; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('i386')
Root: HKCU; Subkey: "Software\@Producer@\hyang32"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('i386')
Root: HKCU; Subkey: "Software\@Producer@\hyang32\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: NonAdmin and isComponentSelected('i386')
Root: HKCU; Subkey: "Software\@Producer@\hyang32\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('i386')

Root: HKCU; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletekeyifempty; Tasks: recordversion; Check: NonAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKCU; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletevalue; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKCU; Subkey: "Software\@Producer@\hyang64"; Flags: uninsdeletevalue; ValueType: string; ValueName: "Current Version"; ValueData: "@HYANGVER@"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKCU; Subkey: "Software\@Producer@\hyang64\@HYANGVER@"; Flags: uninsdeletekey; Tasks: recordversion; Check: NonAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKCU; Subkey: "Software\@Producer@\hyang64\@HYANGVER@"; ValueType: string; ValueName: "InstallPath"; ValueData: "{app}"; Tasks: recordversion; Check: NonAdmin and isComponentSelected('x64') and Is64BitInstallMode

Root: HKCR; Subkey: ".hyData"; ValueType: string; ValueName: ""; ValueData: "RWorkspace"; Flags: uninsdeletevalue; Tasks: associate; Check: IsAdmin
Root: HKCR; Subkey: "RWorkspace"; ValueType: string; ValueName: ""; ValueData: "Hyang Workspace"; Flags: uninsdeletekey; Tasks: associate; Check: IsAdmin
Root: HKCR; Subkey: "RWorkspace\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\i386\hyangGui.exe,0"; Tasks: associate; Check: IsAdmin and isComponentSelected('i386')
Root: HKCR; Subkey: "RWorkspace\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\i386\hyangGui.exe"" ""%1"""; Tasks: associate; Check: IsAdmin and isComponentSelected('i386')
Root: HKCR; Subkey: "RWorkspace\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\bin\x64\hyangGui.exe,0"; Tasks: associate; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
Root: HKCR; Subkey: "RWorkspace\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\bin\x64\hyangGui.exe"" ""%1"""; Tasks: associate; Check: IsAdmin and isComponentSelected('x64') and Is64BitInstallMode
