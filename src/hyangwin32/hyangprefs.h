/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "hyangguicolors.h"

struct structGUI
{
    int MDI;
    int toolbar;
    int statusbar;
    int pagerMultiple;
    char font[50];
    int tt_font;
    int pointsize;
    char style[20];
    int crows, ccols, cx, cy, setWidthOnResize, prows, pcols,
	cbb, cbl, grx, gry;
    int cursor_blink;
    rgb guiColors[numGuiColors];
    rect MDIsize;
    char language[20];
    int buffered;
    char warning[256];
};
typedef struct structGUI *Gui;

int loadhyangconsole(Gui gui, const char *optf);
void getDefaults(Gui gui);
void getActive(Gui gui);
void applyGUI(Gui newGUI);
