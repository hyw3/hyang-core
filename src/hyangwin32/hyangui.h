/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#define RW_MDI         0x0001
#define RW_TOOLBAR     0x0010
#define RW_STATUSBAR   0x0100
#define RW_LARGEICONS   0x1000

#define CASCADE 1
#define TILEHORIZ 2
#define TILEVERT 3
#define MINIMIZE 4
#define RESTORE 5

extern int MDIset;

#include <hyangexts/hyangboolean.h>
#include <hyangexts/hyangextlib.h>
LibExtern int hyangGUIMDI;
LibExtern window hyangConsole;
LibExtern window RFrame;
LibExtern int Rwin_graphicsx, Rwin_graphicsy;
LibExtern hyangboolean AllDevicesKilled;
#undef LibExtern

typedef struct {
    menuitem m;
    char *name;
    char *action;
}  uitem;
typedef uitem *Uitem;

typedef struct {
    int numItems;
    Uitem *mItems;
} menuItems;

struct structHelpMenuItems {
    menuitem mhelp, mmanintro, mmanref, mmandata,
	mmanext, mmanint, mmanlang, mmanadmin, mmanSweave,
	mman0, mapropos, mhelpstart,
	mhelpsearch, msearchRsite, mFAQ, mrwFAQ, mHYPA;
    menu mman;
};
typedef struct structHelpMenuItems *HelpMenuItems;

struct structPkgMenuItems {
    menuitem mpkgl, mpkgm, mpkgi, mpkgil, mpkgu, mrepos;
};
typedef struct structPkgMenuItems *PkgMenuItems;

#include <hyangexts/hyangerrhand.h>
int check_doc_file(const char *);
void internal_shellexec(const char *);

int winaddmenu(const char * name, char *errmsg);
int winaddmenuitem(const char * item, const char * menu, const char * action, char *errmsg);
int windelmenu(const char * menu, char *errmsg);
void windelmenus(const char * prefix);
int windelmenuitem(const char * item,const  char * menu, char *errmsg);

int numwinmenus(void);
char *getusermenuname(int pos);
menuItems *wingetmenuitems(const char *mname, char *errmsg);
void freemenuitems(menuItems *items);

void hyangwin_fpset(void);

void hyangGUI_configure(void);
void readconsolecfg(void);
void breaktodebugger(void);

#define USE_MDI 1

#ifdef USE_MDI
int RgetMDIwidth(void);
int RgetMDIheight(void);
#endif

void menuconfig(control m);
void menuclear(control m);
int hyangGUIPackageMenu(PkgMenuItems pmenu);
void pkgmenuact(PkgMenuItems pmenu);
int hyangGUICommonHelp(menu m, HelpMenuItems hmenu);
void helpmenuact(HelpMenuItems hmenu);
void showstatusbar(void);

menu getGraphMenu(const char *);
