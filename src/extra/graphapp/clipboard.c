/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *
 * file: clipboard.c --
 * 
 *
 */

/*
   This file is an add-on  to GraphApp, a cross-platform C graphics library.
 */

#include "win-nls.h"
#include "internal.h"
#include "hyangui.h"


void copytoclipboard(drawing sb)
{
    HBITMAP hbmpOldDest, hbmpNew;
    HDC     hdcSrc, hdcDest;
    rect r;

    r = getrect(sb);
    hdcSrc =  get_context((object)sb);
    hdcDest = CreateCompatibleDC(hdcSrc);

    hbmpNew = CreateCompatibleBitmap(hdcSrc, r.width, r.height);
    hbmpOldDest = SelectObject(hdcDest, hbmpNew);
    BitBlt(hdcDest, 0, 0, r.width, r.height, hdcSrc, 0, 0, SRCCOPY);
    SelectObject(hdcDest, hbmpOldDest);
    DeleteDC(hdcDest);

    if (!OpenClipboard(NULL) || !EmptyClipboard()) {
	hyang_ShowMessage(G_("Unable to open the clipboard"));
	DeleteObject(hbmpNew);
	return;
    }
    SetClipboardData(CF_BITMAP, hbmpNew);
    CloseClipboard();
    return;
}

int copystringtoclipboard(const char *str)
{
    HGLOBAL hglb;
    char *s;
    int ll = strlen(str) + 1;

    if (!(hglb = GlobalAlloc(GHND, ll))){
	hyang_ShowMessage(G_("Insufficient memory: cell not copied to the clipboard"));
	return 1;
    }
    if (!(s = (char *)GlobalLock(hglb))){
	hyang_ShowMessage(G_("Insufficient memory: cell not copied to the clipboard"));
	return 1;
    }
    strcpy(s, str);
    GlobalUnlock(hglb);
    if (!OpenClipboard(NULL) || !EmptyClipboard()) {
	hyang_ShowMessage(G_("Unable to open the clipboard"));
	GlobalFree(hglb);
	return 1;
    }
    SetClipboardData(CF_TEXT, hglb);
    CloseClipboard();
    return 0;
}

int getstringfromclipboard(char * str, int n)
{
    HGLOBAL hglb;
    char *pc;

    if ( OpenClipboard(NULL) &&
	 (hglb = GetClipboardData(CF_TEXT)) &&
	 (pc = (char *)GlobalLock(hglb))) {
	strncpy(str, pc, n);
	str[n+1] = '\0';
	GlobalUnlock(hglb);
	CloseClipboard();
	return 0;
    } else return 1;
}

int clipboardhastext()
{
    return (int) IsClipboardFormatAvailable(CF_TEXT);
}
