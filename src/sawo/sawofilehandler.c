/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>


#ifdef HAVE_UTIMES
#include <sys/time.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#elif defined(_MSC_VER)
#include <direct.h>
#define F_OK 0
#define W_OK 2
#define R_OK 4
#define S_ISREG(m) (((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m) (((m) & S_IFMT) == S_IFDIR)
#endif

# ifndef MAXPATHLEN
# define MAXPATHLEN SAWO_PATH_LEN
# endif

#if defined(__MINGW32__) || defined(_MSC_VER)
#define ISWINDOWS 1
#else
#define ISWINDOWS 0
#endif

static const char *sawoGetFileType(int mode)
{
    if (S_ISREG(mode)) {
        return "file";
    }
    else if (S_ISDIR(mode)) {
        return "directory";
    }
#ifdef S_ISCHR
    else if (S_ISCHR(mode)) {
        return "characterSpecial";
    }
#endif
#ifdef S_ISBLK
    else if (S_ISBLK(mode)) {
        return "blockSpecial";
    }
#endif
#ifdef S_ISFIFO
    else if (S_ISFIFO(mode)) {
        return "fifo";
    }
#endif
#ifdef S_ISLNK
    else if (S_ISLNK(mode)) {
        return "link";
    }
#endif
#ifdef S_ISSOCK
    else if (S_ISSOCK(mode)) {
        return "socket";
    }
#endif
    return "unknown";
}

static void AppendStatElement(sawo_Interp *interp, sawo_Obj *listObj, const char *key, sawo_wide value)
{
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, key, -1));
    sawo_ListAppendElement(interp, listObj, sawo_NewIntObj(interp, value));
}

static int StoreStatData(sawo_Interp *interp, sawo_Obj *varName, const struct stat *sb)
{
    sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);
    AppendStatElement(interp, listObj, "dev", sb->st_dev);
    AppendStatElement(interp, listObj, "ino", sb->st_ino);
    AppendStatElement(interp, listObj, "mode", sb->st_mode);
    AppendStatElement(interp, listObj, "nlink", sb->st_nlink);
    AppendStatElement(interp, listObj, "uid", sb->st_uid);
    AppendStatElement(interp, listObj, "gid", sb->st_gid);
    AppendStatElement(interp, listObj, "size", sb->st_size);
    AppendStatElement(interp, listObj, "atime", sb->st_atime);
    AppendStatElement(interp, listObj, "mtime", sb->st_mtime);
    AppendStatElement(interp, listObj, "ctime", sb->st_ctime);
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, "type", -1));
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, sawoGetFileType((int)sb->st_mode), -1));

    if (varName) {
        sawo_Obj *objPtr = sawo_GetVariable(interp, varName, SAWO_NONE);
        if (objPtr) {
            if (sawo_DictSize(interp, objPtr) < 0) {
                sawo_SetResultFormatted(interp, "can't set \"%#s(dev)\": variable isn't array", varName);
                sawo_FreeNewObj(interp, listObj);
                return SAWO_ERR;
            }

            if (sawo_IsShared(objPtr))
                objPtr = sawo_DuplicateObj(interp, objPtr);

            sawo_ListAppendList(interp, objPtr, listObj);
            sawo_DictSize(interp, objPtr);
            sawo_InvalidateStringRep(objPtr);

            sawo_FreeNewObj(interp, listObj);
            listObj = objPtr;
        }
        sawo_SetVariable(interp, varName, listObj);
    }

    sawo_SetResult(interp, listObj);

    return SAWO_OK;
}

static int file_cmd_dirname(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *p = strrchr(path, '/');

    if (!p && path[0] == '.' && path[1] == '.' && path[2] == '\0') {
        sawo_SetResultString(interp, "..", -1);
    } else if (!p) {
        sawo_SetResultString(interp, ".", -1);
    }
    else if (p == path) {
        sawo_SetResultString(interp, "/", -1);
    }
    else if (ISWINDOWS && p[-1] == ':') {
        sawo_SetResultString(interp, path, p - path + 1);
    }
    else {
        sawo_SetResultString(interp, path, p - path);
    }
    return SAWO_OK;
}

static int file_cmd_rootname(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');
    const char *p = strrchr(path, '.');

    if (p == NULL || (lastSlash != NULL && lastSlash > p)) {
        sawo_SetResult(interp, argv[0]);
    }
    else {
        sawo_SetResultString(interp, path, p - path);
    }
    return SAWO_OK;
}

static int file_cmd_extension(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');
    const char *p = strrchr(path, '.');

    if (p == NULL || (lastSlash != NULL && lastSlash >= p)) {
        p = "";
    }
    sawo_SetResultString(interp, p, -1);
    return SAWO_OK;
}

static int file_cmd_tail(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');

    if (lastSlash) {
        sawo_SetResultString(interp, lastSlash + 1, -1);
    }
    else {
        sawo_SetResult(interp, argv[0]);
    }
    return SAWO_OK;
}

static int file_cmd_normalize(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef HAVE_REALPATH
    const char *path = sawo_String(argv[0]);
    char *newname = sawo_Alloc(MAXPATHLEN + 1);

    if (realpath(path, newname)) {
        sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, newname, -1));
        return SAWO_OK;
    }
    else {
        sawo_Free(newname);
        sawo_SetResultFormatted(interp, "can't normalize \"%#s\": %s", argv[0], strerror(errno));
        return SAWO_ERR;
    }
#else
    sawo_SetResultString(interp, "Not implemented", -1);
    return SAWO_ERR;
#endif
}

static int file_cmd_join(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    char *newname = sawo_Alloc(MAXPATHLEN + 1);
    char *last = newname;

    *newname = 0;

    for (i = 0; i < argc; i++) {
        int len;
        const char *part = sawo_GetString(argv[i], &len);

        if (*part == '/') {
            last = newname;
        }
        else if (ISWINDOWS && strchr(part, ':')) {
            last = newname;
        }
        else if (part[0] == '.') {
            if (part[1] == '/') {
                part += 2;
                len -= 2;
            }
            else if (part[1] == 0 && last != newname) {
                continue;
            }
        }

        if (last != newname && last[-1] != '/') {
            *last++ = '/';
        }

        if (len) {
            if (last + len - newname >= MAXPATHLEN) {
                sawo_Free(newname);
                sawo_SetResultString(interp, "Path too long", -1);
                return SAWO_ERR;
            }
            memcpy(last, part, len);
            last += len;
        }

        if (last > newname + 1 && last[-1] == '/') {
            if (!ISWINDOWS || !(last > newname + 2 && last[-2] == ':')) {
                *--last = 0;
            }
        }
    }

    *last = 0;

    sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, newname, last - newname));

    return SAWO_OK;
}

static int file_access(sawo_Interp *interp, sawo_Obj *filename, int mode)
{
    sawo_SetResultBool(interp, access(sawo_String(filename), mode) != -1);

    return SAWO_OK;
}

static int file_cmd_readable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], R_OK);
}

static int file_cmd_writable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], W_OK);
}

static int file_cmd_executable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef X_OK
    return file_access(interp, argv[0], X_OK);
#else
    sawo_SetResultBool(interp, 1);
    return SAWO_OK;
#endif
}

static int file_cmd_exists(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], F_OK);
}

static int file_cmd_delete(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int force = sawo_CompareStringImmediate(interp, argv[0], "-force");

    if (force || sawo_CompareStringImmediate(interp, argv[0], "--")) {
        argc++;
        argv--;
    }

    while (argc--) {
        const char *path = sawo_String(argv[0]);

        if (unlink(path) == -1 && errno != ENOENT) {
            if (rmdir(path) == -1) {
                if (!force || sawo_EvalPrefix(interp, "file delete force", 1, argv) != SAWO_OK) {
                    sawo_SetResultFormatted(interp, "couldn't delete file \"%s\": %s", path,
                        strerror(errno));
                    return SAWO_ERR;
                }
            }
        }
        argv++;
    }
    return SAWO_OK;
}

#ifdef HAVE_MKDIR_ONE_ARG
#define MKDIR_DEFAULT(PATHNAME) mkdir(PATHNAME)
#else
#define MKDIR_DEFAULT(PATHNAME) mkdir(PATHNAME, 0755)
#endif

static int mkdir_all(char *path)
{
    int ok = 1;

    goto first;

    while (ok--) {
        {
            char *slash = strrchr(path, '/');

            if (slash && slash != path) {
                *slash = 0;
                if (mkdir_all(path) != 0) {
                    return -1;
                }
                *slash = '/';
            }
        }
      first:
        if (MKDIR_DEFAULT(path) == 0) {
            return 0;
        }
        if (errno == ENOENT) {
            continue;
        }
        if (errno == EEXIST) {
            struct stat sb;

            if (stat(path, &sb) == 0 && S_ISDIR(sb.st_mode)) {
                return 0;
            }
            errno = EEXIST;
        }
        break;
    }
    return -1;
}

static int file_cmd_mkdir(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    while (argc--) {
        char *path = sawo_StrDup(sawo_String(argv[0]));
        int rc = mkdir_all(path);

        sawo_Free(path);
        if (rc != 0) {
            sawo_SetResultFormatted(interp, "can't create directory \"%#s\": %s", argv[0],
                strerror(errno));
            return SAWO_ERR;
        }
        argv++;
    }
    return SAWO_OK;
}

static int file_cmd_tempfile(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int fd = sawo_MakeTempFile(interp, (argc >= 1) ? sawo_String(argv[0]) : NULL);

    if (fd < 0) {
        return SAWO_ERR;
    }
    close(fd);

    return SAWO_OK;
}

static int file_cmd_rename(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *source;
    const char *dest;
    int force = 0;

    if (argc == 3) {
        if (!sawo_CompareStringImmediate(interp, argv[0], "-force")) {
            return -1;
        }
        force++;
        argv++;
        argc--;
    }

    source = sawo_String(argv[0]);
    dest = sawo_String(argv[1]);

    if (!force && access(dest, F_OK) == 0) {
        sawo_SetResultFormatted(interp, "error renaming \"%#s\" to \"%#s\": target exists", argv[0],
            argv[1]);
        return SAWO_ERR;
    }

    if (rename(source, dest) != 0) {
        sawo_SetResultFormatted(interp, "error renaming \"%#s\" to \"%#s\": %s", argv[0], argv[1],
            strerror(errno));
        return SAWO_ERR;
    }

    return SAWO_OK;
}

#if defined(HAVE_LINK) && defined(HAVE_SYMLINK)
static int file_cmd_link(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int ret;
    const char *source;
    const char *dest;
    static const char * const options[] = { "-hard", "-symbolic", NULL };
    enum { OPT_HARD, OPT_SYMBOLIC, };
    int option = OPT_HARD;

    if (argc == 3) {
        if (sawo_GetEnum(interp, argv[0], options, &option, NULL, SAWO_ENUM_ABBREV | SAWO_ERRMSG) != SAWO_OK) {
            return SAWO_ERR;
        }
        argv++;
        argc--;
    }

    dest = sawo_String(argv[0]);
    source = sawo_String(argv[1]);

    if (option == OPT_HARD) {
        ret = link(source, dest);
    }
    else {
        ret = symlink(source, dest);
    }

    if (ret != 0) {
        sawo_SetResultFormatted(interp, "error linking \"%#s\" to \"%#s\": %s", argv[0], argv[1],
            strerror(errno));
        return SAWO_ERR;
    }

    return SAWO_OK;
}
#endif

static int file_stat(sawo_Interp *interp, sawo_Obj *filename, struct stat *sb)
{
    const char *path = sawo_String(filename);

    if (stat(path, sb) == -1) {
        sawo_SetResultFormatted(interp, "could not read \"%#s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}

#ifdef HAVE_LSTAT
static int file_lstat(sawo_Interp *interp, sawo_Obj *filename, struct stat *sb)
{
    const char *path = sawo_String(filename);

    if (lstat(path, sb) == -1) {
        sawo_SetResultFormatted(interp, "could not read \"%#s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}
#else
#define file_lstat file_stat
#endif

static int file_cmd_atime(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_atime);
    return SAWO_OK;
}

static int file_cmd_mtime(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (argc == 2) {
#ifdef HAVE_UTIMES
        sawo_wide newtime;
        struct timeval times[2];

        if (sawo_GetWide(interp, argv[1], &newtime) != SAWO_OK) {
            return SAWO_ERR;
        }

        times[1].tv_sec = times[0].tv_sec = newtime;
        times[1].tv_usec = times[0].tv_usec = 0;

        if (utimes(sawo_String(argv[0]), times) != 0) {
            sawo_SetResultFormatted(interp, "can't set time on \"%#s\": %s", argv[0], strerror(errno));
            return SAWO_ERR;
        }
#else
        sawo_SetResultString(interp, "Not implemented", -1);
        return SAWO_ERR;
#endif
    }
    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_mtime);
    return SAWO_OK;
}

static int file_cmd_copy(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawo_EvalPrefix(interp, "file copy", argc, argv);
}

static int file_cmd_size(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_size);
    return SAWO_OK;
}

static int file_cmd_isdirectory(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = S_ISDIR(sb.st_mode);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}

static int file_cmd_isfile(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = S_ISREG(sb.st_mode);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}

#ifdef HAVE_GETEUID
static int file_cmd_owned(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = (geteuid() == sb.st_uid);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}
#endif

#if defined(HAVE_READLINK)
static int file_cmd_readlink(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    char *linkValue = sawo_Alloc(MAXPATHLEN + 1);

    int linkLength = readlink(path, linkValue, MAXPATHLEN);

    if (linkLength == -1) {
        sawo_Free(linkValue);
        sawo_SetResultFormatted(interp, "couldn't readlink \"%#s\": %s", argv[0], strerror(errno));
        return SAWO_ERR;
    }
    linkValue[linkLength] = 0;
    sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, linkValue, linkLength));
    return SAWO_OK;
}
#endif

static int file_cmd_type(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_lstat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultString(interp, sawoGetFileType((int)sb.st_mode), -1);
    return SAWO_OK;
}

#ifdef HAVE_LSTAT
static int file_cmd_lstat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_lstat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    return StoreStatData(interp, argc == 2 ? argv[1] : NULL, &sb);
}
#else
#define file_cmd_lstat file_cmd_stat
#endif

static int file_cmd_stat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    return StoreStatData(interp, argc == 2 ? argv[1] : NULL, &sb);
}

static const sawo_subcmd_type file_command_table[] = {
    {   "atime",
        "name",
        file_cmd_atime,
        1,
        1,
    },
    {   "mtime",
        "name ?time?",
        file_cmd_mtime,
        1,
        2,
    },
    {   "copy",
        "?-force? source dest",
        file_cmd_copy,
        2,
        3,
    },
    {   "dirname",
        "name",
        file_cmd_dirname,
        1,
        1,
    },
    {   "rootname",
        "name",
        file_cmd_rootname,
        1,
        1,
    },
    {   "extension",
        "name",
        file_cmd_extension,
        1,
        1,
    },
    {   "tail",
        "name",
        file_cmd_tail,
        1,
        1,
    },
    {   "normalize",
        "name",
        file_cmd_normalize,
        1,
        1,
    },
    {   "join",
        "name ?name ...?",
        file_cmd_join,
        1,
        -1,
    },
    {   "readable",
        "name",
        file_cmd_readable,
        1,
        1,
    },
    {   "writable",
        "name",
        file_cmd_writable,
        1,
        1,
    },
    {   "executable",
        "name",
        file_cmd_executable,
        1,
        1,
    },
    {   "exists",
        "name",
        file_cmd_exists,
        1,
        1,
    },
    {   "delete",
        "?-force|--? name ...",
        file_cmd_delete,
        1,
        -1,
    },
    {   "mkdir",
        "dir ...",
        file_cmd_mkdir,
        1,
        -1,
    },
    {   "tempfile",
        "?template?",
        file_cmd_tempfile,
        0,
        1,
    },
    {   "rename",
        "?-force? source dest",
        file_cmd_rename,
        2,
        3,
    },
#if defined(HAVE_LINK) && defined(HAVE_SYMLINK)
    {   "link",
        "?-symbolic|-hard? newname target",
        file_cmd_link,
        2,
        3,
    },
#endif
#if defined(HAVE_READLINK)
    {   "readlink",
        "name",
        file_cmd_readlink,
        1,
        1,
    },
#endif
    {   "size",
        "name",
        file_cmd_size,
        1,
        1,
    },
    {   "stat",
        "name ?var?",
        file_cmd_stat,
        1,
        2,
    },
    {   "lstat",
        "name ?var?",
        file_cmd_lstat,
        1,
        2,
    },
    {   "type",
        "name",
        file_cmd_type,
        1,
        1,
    },
#ifdef HAVE_GETEUID
    {   "owned",
        "name",
        file_cmd_owned,
        1,
        1,
    },
#endif
    {   "isdirectory",
        "name",
        file_cmd_isdirectory,
        1,
        1,
    },
    {   "isfile",
        "name",
        file_cmd_isfile,
        1,
        1,
    },
    {
        NULL
    }
};

static int sawo_CdCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path;

    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "dirname");
        return SAWO_ERR;
    }

    path = sawo_String(argv[1]);

    if (chdir(path) != 0) {
        sawo_SetResultFormatted(interp, "couldn't change working directory to \"%s\": %s", path,
            strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int sawo_PwdCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    char *cwd = sawo_Alloc(MAXPATHLEN);

    if (getcwd(cwd, MAXPATHLEN) == NULL) {
        sawo_SetResultString(interp, "Failed to get pwd", -1);
        sawo_Free(cwd);
        return SAWO_ERR;
    }
    else if (ISWINDOWS) {
        char *p = cwd;
        while ((p = strchr(p, '\\')) != NULL) {
            *p++ = '/';
        }
    }

    sawo_SetResultString(interp, cwd, -1);

    sawo_Free(cwd);
    return SAWO_OK;
}

int sawo_fileInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "file", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "file", sawo_SubCmdRaise, (void *)file_command_table, NULL);
    sawo_CreateCommand(interp, "pwd", sawo_PwdCmd, NULL, NULL);
    sawo_CreateCommand(interp, "cd", sawo_CdCmd, NULL, NULL);
    return SAWO_OK;
}


