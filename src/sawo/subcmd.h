/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef SAWO_SUBCMD_H
#define SAWO_SUBCMD_H

#ifdef __cplusplus
extern "C" {
#endif

#define SAWO_MODFLAG_HIDDEN   0x0001
#define SAWO_MODFLAG_FULLARGV 0x0002

typedef int sawo_subcmd_function(sawo_Interp *interp, int argc, sawo_Obj *const *argv);

typedef struct {
	const char *cmd;
	const char *args;
	sawo_subcmd_function *function;
	short minargs;
	short maxargs;
	unsigned short flags;
} sawo_subcmd_type;

const sawo_subcmd_type *
sawo_ParseSubCmd(sawo_Interp *interp, const sawo_subcmd_type *command_table, int argc, sawo_Obj *const *argv);

int sawo_SubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv);

int sawo_CallSubCmd(sawo_Interp *interp, const sawo_subcmd_type *ct, int argc, sawo_Obj *const *argv);

#ifdef __cplusplus
}
#endif

#endif

