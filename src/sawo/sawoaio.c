/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#include <sys/stat.h>
#endif

#if defined(HAVE_SYS_SOCKET_H) && defined(HAVE_SELECT) && defined(HAVE_NETINET_IN_H) && defined(HAVE_NETDB_H) && defined(HAVE_ARPA_INET_H)
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef HAVE_SYS_UN_H
#include <sys/un.h>
#endif
#else
#define SAWO_ANSIC
#endif

#if defined(SAWO_SSL)
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif

#define AIO_CMD_LEN 32
#define AIO_BUF_LEN 256

#ifndef HAVE_FTELLO
    #define ftello ftell
#endif
#ifndef HAVE_FSEEKO
    #define fseeko fseek
#endif

#define AIO_KEEPOPEN 1

#if defined(SAWO_IPV6)
#define IPV6 1
#else
#define IPV6 0
#ifndef PF_INET6
#define PF_INET6 0
#endif
#endif

#define sawoCheckStreamError(interp, af) af->fops->error(af)

struct AioFile;

typedef struct {
    int (*writer)(struct AioFile *af, const char *buf, int len);
    int (*reader)(struct AioFile *af, char *buf, int len);
    const char *(*getline)(struct AioFile *af, char *buf, int len);
    int (*error)(const struct AioFile *af);
    const char *(*strerror)(struct AioFile *af);
    int (*verify)(struct AioFile *af);
} sawoAioFopsType;

typedef struct AioFile
{
    FILE *fp;
    sawo_Obj *filename;
    int type;
    int openFlags;
    int fd;
    sawo_Obj *rEvent;
    sawo_Obj *wEvent;
    sawo_Obj *eEvent;
    int addr_family;
    void *ssl;
    const sawoAioFopsType *fops;
} AioFile;

static int stdio_writer(struct AioFile *af, const char *buf, int len)
{
    return fwrite(buf, 1, len, af->fp);
}

static int stdio_reader(struct AioFile *af, char *buf, int len)
{
    return fread(buf, 1, len, af->fp);
}

static const char *stdio_getline(struct AioFile *af, char *buf, int len)
{
    return fgets(buf, len, af->fp);
}

static int stdio_error(const AioFile *af)
{
    if (!ferror(af->fp)) {
        return SAWO_OK;
    }
    clearerr(af->fp);

    if (feof(af->fp) || errno == EAGAIN || errno == EINTR) {
        return SAWO_OK;
    }
#ifdef ECONNRESET
    if (errno == ECONNRESET) {
        return SAWO_OK;
    }
#endif
#ifdef ECONNABORTED
    if (errno != ECONNABORTED) {
        return SAWO_OK;
    }
#endif
    return SAWO_ERR;
}

static const char *stdio_strerror(struct AioFile *af)
{
    return strerror(errno);
}

static const sawoAioFopsType stdio_fops = {
    stdio_writer,
    stdio_reader,
    stdio_getline,
    stdio_error,
    stdio_strerror,
    NULL
};


static int sawoAioSubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv);
static AioFile *sawoMakeChannel(sawo_Interp *interp, FILE *fh, int fd, sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode);

static const char *sawoAioErrorString(AioFile *af)
{
    if (af && af->fops)
        return af->fops->strerror(af);

    return strerror(errno);
}

static void sawoAioSetError(sawo_Interp *interp, sawo_Obj *name)
{
    AioFile *af = sawo_CmdPrivData(interp);

    if (name) {
        sawo_SetResultFormatted(interp, "%#s: %s", name, sawoAioErrorString(af));
    }
    else {
        sawo_SetResultString(interp, sawoAioErrorString(af), -1);
    }
}

static void sawoAioDelRaise(sawo_Interp *interp, void *privData)
{
    AioFile *af = privData;

    SAWO_NOTUSED(interp);

    sawo_DecrRefCount(interp, af->filename);

#ifdef sawo_ext_eventloop
    sawo_DeleteFileHandler(interp, af->fp, SAWO_EVENT_READABLE | SAWO_EVENT_WRITABLE | SAWO_EVENT_EXCEPTION);
#endif

#if defined(SAWO_SSL)
    if (af->ssl != NULL) {
        SSL_free(af->ssl);
    }
#endif

    if (!(af->openFlags & AIO_KEEPOPEN)) {
        fclose(af->fp);
    }

    sawo_Free(af);
}

static int aio_cmd_read(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    char buf[AIO_BUF_LEN];
    sawo_Obj *objPtr;
    int nonewline = 0;
    sawo_wide neededLen = -1;

    if (argc && sawo_CompareStringImmediate(interp, argv[0], "-nonewline")) {
        nonewline = 1;
        argv++;
        argc--;
    }
    if (argc == 1) {
        if (sawo_GetWide(interp, argv[0], &neededLen) != SAWO_OK)
            return SAWO_ERR;
        if (neededLen < 0) {
            sawo_SetResultString(interp, "invalid parameter: negative len", -1);
            return SAWO_ERR;
        }
    }
    else if (argc) {
        return -1;
    }
    objPtr = sawo_NewStringObj(interp, NULL, 0);
    while (neededLen != 0) {
        int retval;
        int readlen;

        if (neededLen == -1) {
            readlen = AIO_BUF_LEN;
        }
        else {
            readlen = (neededLen > AIO_BUF_LEN ? AIO_BUF_LEN : neededLen);
        }
        retval = af->fops->reader(af, buf, readlen);
        if (retval > 0) {
            sawo_AppendString(interp, objPtr, buf, retval);
            if (neededLen != -1) {
                neededLen -= retval;
            }
        }
        if (retval != readlen)
            break;
    }

    if (sawoCheckStreamError(interp, af)) {
        sawo_FreeNewObj(interp, objPtr);
        return SAWO_ERR;
    }
    if (nonewline) {
        int len;
        const char *s = sawo_GetString(objPtr, &len);

        if (len > 0 && s[len - 1] == '\n') {
            objPtr->length--;
            objPtr->bytes[objPtr->length] = '\0';
        }
    }
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}

AioFile *sawo_AioFile(sawo_Interp *interp, sawo_Obj *command)
{
    sawo_Cmd *cmdPtr = sawo_GetCommand(interp, command, SAWO_ERRMSG);

    if (cmdPtr && !cmdPtr->israise && cmdPtr->u.native.cmdRaise == sawoAioSubCmdRaise) {
        return (AioFile *) cmdPtr->u.native.privData;
    }
    sawo_SetResultFormatted(interp, "Not a filehandle: \"%#s\"", command);
    return NULL;
}

FILE *sawo_AioFilehandle(sawo_Interp *interp, sawo_Obj *command)
{
    AioFile *af;

    af = sawo_AioFile(interp, command);
    if (af == NULL) {
        return NULL;
    }

    return af->fp;
}

static int aio_cmd_copy(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    sawo_wide count = 0;
    sawo_wide maxlen = SAWO_WIDE_MAX;
    AioFile *outf = sawo_AioFile(interp, argv[0]);

    if (outf == NULL) {
        return SAWO_ERR;
    }

    if (argc == 2) {
        if (sawo_GetWide(interp, argv[1], &maxlen) != SAWO_OK) {
            return SAWO_ERR;
        }
    }

    while (count < maxlen) {
        char ch;

        if (af->fops->reader(af, &ch, 1) != 1) {
            break;
        }
        if (outf->fops->writer(outf, &ch, 1) != 1) {
            break;
        }
        count++;
    }

    if (sawoCheckStreamError(interp, af) || sawoCheckStreamError(interp, outf)) {
        return SAWO_ERR;
    }

    sawo_SetResultInt(interp, count);

    return SAWO_OK;
}

static int aio_cmd_gets(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    char buf[AIO_BUF_LEN];
    sawo_Obj *objPtr;
    int len;

    errno = 0;

    objPtr = sawo_NewStringObj(interp, NULL, 0);
    while (1) {
        buf[AIO_BUF_LEN - 1] = '_';

        if (af->fops->getline(af, buf, AIO_BUF_LEN) == NULL)
            break;

        if (buf[AIO_BUF_LEN - 1] == '\0' && buf[AIO_BUF_LEN - 2] != '\n') {
            sawo_AppendString(interp, objPtr, buf, AIO_BUF_LEN - 1);
        }
        else {
            len = strlen(buf);

            if (len && (buf[len - 1] == '\n')) {
                len--;
            }

            sawo_AppendString(interp, objPtr, buf, len);
            break;
        }
    }

    if (sawoCheckStreamError(interp, af)) {
        sawo_FreeNewObj(interp, objPtr);
        return SAWO_ERR;
    }

    if (argc) {
        if (sawo_SetVariable(interp, argv[0], objPtr) != SAWO_OK) {
            sawo_FreeNewObj(interp, objPtr);
            return SAWO_ERR;
        }

        len = sawo_Length(objPtr);

        if (len == 0 && feof(af->fp)) {
            len = -1;
        }
        sawo_SetResultInt(interp, len);
    }
    else {
        sawo_SetResult(interp, objPtr);
    }
    return SAWO_OK;
}

static int aio_cmd_puts(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    int wlen;
    const char *wdata;
    sawo_Obj *strObj;

    if (argc == 2) {
        if (!sawo_CompareStringImmediate(interp, argv[0], "-nonewline")) {
            return -1;
        }
        strObj = argv[1];
    }
    else {
        strObj = argv[0];
    }

    wdata = sawo_GetString(strObj, &wlen);
    if (af->fops->writer(af, wdata, wlen) == wlen) {
        if (argc == 2 || af->fops->writer(af, "\n", 1) == 1) {
            return SAWO_OK;
        }
    }
    sawoAioSetError(interp, af->filename);
    return SAWO_ERR;
}

static int aio_cmd_isatty(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef HAVE_ISATTY
    AioFile *af = sawo_CmdPrivData(interp);
    sawo_SetResultInt(interp, isatty(fileno(af->fp)));
#else
    sawo_SetResultInt(interp, 0);
#endif

    return SAWO_OK;
}


static int aio_cmd_flush(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    if (fflush(af->fp) == EOF) {
        sawoAioSetError(interp, af->filename);
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int aio_cmd_eof(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResultInt(interp, feof(af->fp));
    return SAWO_OK;
}

static int aio_cmd_close(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc == 3) {
#if !defined(SAWO_ANSIC) && defined(HAVE_SHUTDOWN)
        static const char * const options[] = { "r", "w", NULL };
        enum { OPT_R, OPT_W, };
        int option;
        AioFile *af = sawo_CmdPrivData(interp);

        if (sawo_GetEnum(interp, argv[2], options, &option, NULL, SAWO_ERRMSG) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (shutdown(af->fd, option == OPT_R ? SHUT_RD : SHUT_WR) == 0) {
            return SAWO_OK;
        }
        sawoAioSetError(interp, NULL);
#else
        sawo_SetResultString(interp, "async close not supported", -1);
#endif
        return SAWO_ERR;
    }

    return sawo_DeleteCommand(interp, sawo_String(argv[0]));
}

static int aio_cmd_seek(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    int orig = SEEK_SET;
    sawo_wide offset;

    if (argc == 2) {
        if (sawo_CompareStringImmediate(interp, argv[1], "start"))
            orig = SEEK_SET;
        else if (sawo_CompareStringImmediate(interp, argv[1], "current"))
            orig = SEEK_CUR;
        else if (sawo_CompareStringImmediate(interp, argv[1], "end"))
            orig = SEEK_END;
        else {
            return -1;
        }
    }
    if (sawo_GetWide(interp, argv[0], &offset) != SAWO_OK) {
        return SAWO_ERR;
    }
    if (fseeko(af->fp, offset, orig) == -1) {
        sawoAioSetError(interp, af->filename);
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int aio_cmd_tell(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResultInt(interp, ftello(af->fp));
    return SAWO_OK;
}

static int aio_cmd_filename(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResult(interp, af->filename);
    return SAWO_OK;
}

#ifdef O_NDELAY
static int aio_cmd_ndelay(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    int fmode = fcntl(af->fd, F_GETFL);

    if (argc) {
        long nb;

        if (sawo_GetLong(interp, argv[0], &nb) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (nb) {
            fmode |= O_NDELAY;
        }
        else {
            fmode &= ~O_NDELAY;
        }
        (void)fcntl(af->fd, F_SETFL, fmode);
    }
    sawo_SetResultInt(interp, (fmode & O_NONBLOCK) ? 1 : 0);
    return SAWO_OK;
}
#endif

#ifdef HAVE_FSYNC
static int aio_cmd_sync(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    fflush(af->fp);
    fsync(af->fd);
    return SAWO_OK;
}
#endif

static int aio_cmd_buffering(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    static const char * const options[] = {
        "none",
        "line",
        "full",
        NULL
    };
    enum
    {
        OPT_NONE,
        OPT_LINE,
        OPT_FULL,
    };
    int option;

    if (sawo_GetEnum(interp, argv[0], options, &option, NULL, SAWO_ERRMSG) != SAWO_OK) {
        return SAWO_ERR;
    }
    switch (option) {
        case OPT_NONE:
            setvbuf(af->fp, NULL, _IONBF, 0);
            break;
        case OPT_LINE:
            setvbuf(af->fp, NULL, _IOLBF, BUFSIZ);
            break;
        case OPT_FULL:
            setvbuf(af->fp, NULL, _IOFBF, BUFSIZ);
            break;
    }
    return SAWO_OK;
}

#ifdef sawo_ext_eventloop
static void sawoAioFileEventFinalizer(sawo_Interp *interp, void *clientData)
{
    sawo_Obj **objPtrPtr = clientData;

    sawo_DecrRefCount(interp, *objPtrPtr);
    *objPtrPtr = NULL;
}

static int sawoAioFileEventHandler(sawo_Interp *interp, void *clientData, int mask)
{
    sawo_Obj **objPtrPtr = clientData;

    return sawo_EvalObjBackground(interp, *objPtrPtr);
}

static int aio_eventinfo(sawo_Interp *interp, AioFile * af, unsigned mask, sawo_Obj **scriptHandlerObj,
    int argc, sawo_Obj * const *argv)
{
    if (argc == 0) {
        if (*scriptHandlerObj) {
            sawo_SetResult(interp, *scriptHandlerObj);
        }
        return SAWO_OK;
    }

    if (*scriptHandlerObj) {
        sawo_DeleteFileHandler(interp, af->fp, mask);
    }

    if (sawo_Length(argv[0]) == 0) {
        return SAWO_OK;
    }

    sawo_IncrRefCount(argv[0]);
    *scriptHandlerObj = argv[0];

    sawo_CreateFileHandler(interp, af->fp, mask,
        sawoAioFileEventHandler, scriptHandlerObj, sawoAioFileEventFinalizer);

    return SAWO_OK;
}

static int aio_cmd_readable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_READABLE, &af->rEvent, argc, argv);
}

static int aio_cmd_writable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_WRITABLE, &af->wEvent, argc, argv);
}

static int aio_cmd_onexception(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_EXCEPTION, &af->eEvent, argc, argv);
}
#endif


static const sawo_subcmd_type aio_command_table[] = {
    {   "read",
        "?-nonewline? ?len?",
        aio_cmd_read,
        0,
        2,
    },
    {   "copyto",
        "handle ?size?",
        aio_cmd_copy,
        1,
        2,
    },
    {   "gets",
        "?var?",
        aio_cmd_gets,
        0,
        1,
    },
    {   "pin",
        "?-nonewline? str",
        aio_cmd_puts,
        1,
        2,
    },
    {   "isatty",
        NULL,
        aio_cmd_isatty,
        0,
        0,
    },
    {   "flush",
        NULL,
        aio_cmd_flush,
        0,
        0,
    },
    {   "eof",
        NULL,
        aio_cmd_eof,
        0,
        0,
    },
    {   "close",
        "?r(ead)|w(rite)?",
        aio_cmd_close,
        0,
        1,
        SAWO_MODFLAG_FULLARGV,
    },
    {   "seek",
        "offset ?start|current|end",
        aio_cmd_seek,
        1,
        2,
    },
    {   "tell",
        NULL,
        aio_cmd_tell,
        0,
        0,
    },
    {   "filename",
        NULL,
        aio_cmd_filename,
        0,
        0,
    },
#ifdef O_NDELAY
    {   "ndelay",
        "?0|1?",
        aio_cmd_ndelay,
        0,
        1,
    },
#endif
#ifdef HAVE_FSYNC
    {   "sync",
        NULL,
        aio_cmd_sync,
        0,
        0,
    },
#endif
    {   "buffering",
        "none|line|full",
        aio_cmd_buffering,
        1,
        1,
    },
#ifdef sawo_ext_eventloop
    {   "readable",
        "?readable-script?",
        aio_cmd_readable,
        0,
        1,
    },
    {   "writable",
        "?writable-script?",
        aio_cmd_writable,
        0,
        1,
    },
    {   "onexception",
        "?exception-script?",
        aio_cmd_onexception,
        0,
        1,
    },
#endif
    { NULL }
};

static int sawoAioSubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawo_CallSubCmd(interp, sawo_ParseSubCmd(interp, aio_command_table, argc, argv), argc, argv);
}

static int sawoAioOpenCommand(sawo_Interp *interp, int argc,
        sawo_Obj *const *argv)
{
    const char *mode;

    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "filename ?mode?");
        return SAWO_ERR;
    }

    mode = (argc == 3) ? sawo_String(argv[2]) : "r";

#ifdef sawo_ext_hyangcompat
    {
        const char *filename = sawo_String(argv[1]);

        if (*filename == '|') {
            sawo_Obj *evalObj[3];

            evalObj[0] = sawo_NewStringObj(interp, "::popen", -1);
            evalObj[1] = sawo_NewStringObj(interp, filename + 1, -1);
            evalObj[2] = sawo_NewStringObj(interp, mode, -1);

            return sawo_EvalObjVector(interp, 3, evalObj);
        }
    }
#endif
    return sawoMakeChannel(interp, NULL, -1, argv[1], "aio.handle%ld", 0, mode) ? SAWO_OK : SAWO_ERR;
}

static AioFile *sawoMakeChannel(sawo_Interp *interp, FILE *fh, int fd, sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode)
{
    AioFile *af;
    char buf[AIO_CMD_LEN];
    int openFlags = 0;

    snprintf(buf, sizeof(buf), hdlfmt, sawo_GetId(interp));

    if (fh) {
        openFlags = AIO_KEEPOPEN;
    }

    snprintf(buf, sizeof(buf), hdlfmt, sawo_GetId(interp));
    if (!filename) {
        filename = sawo_NewStringObj(interp, buf, -1);
    }

    sawo_IncrRefCount(filename);

    if (fh == NULL) {
#if !defined(SAWO_ANSIC)
        if (fd >= 0) {
            fh = fdopen(fd, mode);
        }
        else
#endif
            fh = fopen(sawo_String(filename), mode);

        if (fh == NULL) {
            sawoAioSetError(interp, filename);
#if !defined(SAWO_ANSIC)
            if (fd >= 0) {
                close(fd);
            }
#endif
            sawo_DecrRefCount(interp, filename);
            return NULL;
        }
    }

    af = sawo_Alloc(sizeof(*af));
    memset(af, 0, sizeof(*af));
    af->fp = fh;
    af->fd = fileno(fh);
    af->filename = filename;
#ifdef FD_CLOEXEC
    if ((openFlags & AIO_KEEPOPEN) == 0) {
        (void)fcntl(af->fd, F_SETFD, FD_CLOEXEC);
    }
#endif
    af->openFlags = openFlags;
    af->addr_family = family;
    af->fops = &stdio_fops;
    af->ssl = NULL;

    sawo_CreateCommand(interp, buf, sawoAioSubCmdRaise, af, sawoAioDelRaise);

    sawo_SetResult(interp, sawo_MakeGlobalNamespaceName(interp, sawo_NewStringObj(interp, buf, -1)));

    return af;
}


#if defined(HAVE_PIPE) || (defined(HAVE_SOCKETPAIR) && defined(HAVE_SYS_UN_H))
static int sawoMakeChannelPair(sawo_Interp *interp, int p[2], sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode[2])
{
    if (sawoMakeChannel(interp, NULL, p[0], filename, hdlfmt, family, mode[0])) {
        sawo_Obj *objPtr = sawo_NewListObj(interp, NULL, 0);
        sawo_ListAppendElement(interp, objPtr, sawo_GetResult(interp));

        if (sawoMakeChannel(interp, NULL, p[1], filename, hdlfmt, family, mode[1])) {
            sawo_ListAppendElement(interp, objPtr, sawo_GetResult(interp));
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;
        }
    }

    close(p[0]);
    close(p[1]);
    sawoAioSetError(interp, NULL);
    return SAWO_ERR;
}
#endif

int sawo_MakeTempFile(sawo_Interp *interp, const char *template)
{
#ifdef HAVE_MKSTEMP
    int fd;
    mode_t mask;
    sawo_Obj *filenameObj;

    if (template == NULL) {
        const char *tmpdir = getenv("TMPDIR");
        if (tmpdir == NULL || *tmpdir == '\0' || access(tmpdir, W_OK) != 0) {
            tmpdir = "/tmp/";
        }
        filenameObj = sawo_NewStringObj(interp, tmpdir, -1);
        if (tmpdir[0] && tmpdir[strlen(tmpdir) - 1] != '/') {
            sawo_AppendString(interp, filenameObj, "/", 1);
        }
        sawo_AppendString(interp, filenameObj, "hyang.tmp.XXXXXX", -1);
    }
    else {
        filenameObj = sawo_NewStringObj(interp, template, -1);
    }

#if defined(S_IRWXG) && defined(S_IRWXO)
    mask = umask(S_IXUSR | S_IRWXG | S_IRWXO);
#else
    mask = umask(S_IXUSR);
#endif

    fd = mkstemp(filenameObj->bytes);
    umask(mask);
    if (fd < 0) {
        sawoAioSetError(interp, filenameObj);
        sawo_FreeNewObj(interp, filenameObj);
        return -1;
    }

    sawo_SetResult(interp, filenameObj);
    return fd;
#else
    sawo_SetResultString(interp, "platform has no tempfile support", -1);
    return -1;
#endif
}

int sawo_aioInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "aio", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

#if defined(SAWO_SSL)
    sawo_CreateCommand(interp, "load_ssl_certs", sawoAioLoadSSLCertsCommand, NULL, NULL);
#endif

    sawo_CreateCommand(interp, "open", sawoAioOpenCommand, NULL, NULL);
#ifndef SAWO_ANSIC
    sawo_CreateCommand(interp, "socket", sawoAioSockCommand, NULL, NULL);
#endif

    sawoMakeChannel(interp, stdin, -1, NULL, "stdin", 0, "r");
    sawoMakeChannel(interp, stdout, -1, NULL, "stdout", 0, "w");
    sawoMakeChannel(interp, stderr, -1, NULL, "stderr", 0, "w");

    return SAWO_OK;
}

