/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#include <string.h>
#include <ctype.h>

#if (!defined(HAVE_VFORK) || !defined(HAVE_WAITPID)) && !defined(__MINGW32__)
static int sawo_ExecCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *cmdlineObj = sawo_NewEmptyStringObj(interp);
    int i, j;
    int rc;

    for (i = 1; i < argc; i++) {
        int len;
        const char *arg = sawo_GetString(argv[i], &len);

        if (i > 1) {
            sawo_AppendString(interp, cmdlineObj, " ", 1);
        }
        if (strpbrk(arg, "\\\" ") == NULL) {
            sawo_AppendString(interp, cmdlineObj, arg, len);
            continue;
        }

        sawo_AppendString(interp, cmdlineObj, "\"", 1);
        for (j = 0; j < len; j++) {
            if (arg[j] == '\\' || arg[j] == '"') {
                sawo_AppendString(interp, cmdlineObj, "\\", 1);
            }
            sawo_AppendString(interp, cmdlineObj, &arg[j], 1);
        }
        sawo_AppendString(interp, cmdlineObj, "\"", 1);
    }
    rc = system(sawo_String(cmdlineObj));

    sawo_FreeNewObj(interp, cmdlineObj);

    if (rc) {
        sawo_Obj *errorCode = sawo_NewListObj(interp, NULL, 0);
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, "CHILDSTATUS", -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, 0));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, rc));
        sawo_SetGlobalVariableStr(interp, "errorCode", errorCode);
        return SAWO_ERR;
    }

    return SAWO_OK;
}

int sawo_execInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "exec", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "exec", sawo_ExecCmd, NULL, NULL);
    return SAWO_OK;
}
#else


#include <errno.h>
#include <signal.h>

#if defined(__MINGW32__)
    #ifndef STRICT
    #define STRICT
    #endif
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <fcntl.h>

    typedef HANDLE fdtype;
    typedef HANDLE pidtype;
    #define SAWO_BAD_FD INVALID_HANDLE_VALUE
    #define SAWO_BAD_PID INVALID_HANDLE_VALUE
    #define sawoCloseFd CloseHandle

    #define WIFEXITED(STATUS) 1
    #define WEXITSTATUS(STATUS) (STATUS)
    #define WIFSIGNALED(STATUS) 0
    #define WTERMSIG(STATUS) 0
    #define WNOHANG 1

    static fdtype sawoFileno(FILE *fh);
    static pidtype sawoWaitPid(pidtype pid, int *status, int nohang);
    static fdtype sawoDupFd(fdtype infd);
    static fdtype sawoOpenForRead(const char *filename);
    static FILE *sawoFdOpenForRead(fdtype fd);
    static int sawoPipe(fdtype pipefd[2]);
    static pidtype sawoStartWinProcess(sawo_Interp *interp, char **argv, char *env,
        fdtype inputId, fdtype outputId, fdtype errorId);
    static int sawoErrno(void);
#else
    #include <unistd.h>
    #include <fcntl.h>
    #include <sys/wait.h>
    #include <sys/stat.h>

    typedef int fdtype;
    typedef int pidtype;
    #define sawoPipe pipe
    #define sawoErrno() errno
    #define SAWO_BAD_FD -1
    #define SAWO_BAD_PID -1
    #define sawoFileno fileno
    #define sawoReadFd read
    #define sawoCloseFd close
    #define sawoWaitPid waitpid
    #define sawoDupFd dup
    #define sawoFdOpenForRead(FD) fdopen((FD), "r")
    #define sawoOpenForRead(NAME) open((NAME), O_RDONLY, 0)

    #ifndef HAVE_EXECVPE
        #define execvpe(ARG0, ARGV, ENV) execvp(ARG0, ARGV)
    #endif
#endif

static const char *sawoStrError(void);
static char **sawoSaveEnv(char **env);
static void sawoRestoreEnv(char **env);
static int sawoCreatePipeline(sawo_Interp *interp, int argc, sawo_Obj *const *argv,
    pidtype **pidArrayPtr, fdtype *inPipePtr, fdtype *outPipePtr, fdtype *errFilePtr);
static void sawoDetachPids(sawo_Interp *interp, int numPids, const pidtype *pidPtr);
static int sawoCleanupChildren(sawo_Interp *interp, int numPids, pidtype *pidPtr, sawo_Obj *errStrObj);
static fdtype sawoCreateTemp(sawo_Interp *interp, const char *contents, int len);
static fdtype sawoOpenForWrite(const char *filename, int append);
static int sawoRewindFd(fdtype fd);

static void sawo_SetResultErrno(sawo_Interp *interp, const char *msg)
{
    sawo_SetResultFormatted(interp, "%s: %s", msg, sawoStrError());
}

static const char *sawoStrError(void)
{
    return strerror(sawoErrno());
}

static void sawo_RemoveTrailingNewline(sawo_Obj *objPtr)
{
    int len;
    const char *s = sawo_GetString(objPtr, &len);

    if (len > 0 && s[len - 1] == '\n') {
        objPtr->length--;
        objPtr->bytes[objPtr->length] = '\0';
    }
}

static int sawoAppendStreamToString(sawo_Interp *interp, fdtype fd, sawo_Obj *strObj)
{
    char buf[256];
    FILE *fh = sawoFdOpenForRead(fd);
    int ret = 0;

    if (fh == NULL) {
        return -1;
    }

    while (1) {
        int retval = fread(buf, 1, sizeof(buf), fh);
        if (retval > 0) {
            ret = 1;
            sawo_AppendString(interp, strObj, buf, retval);
        }
        if (retval != sizeof(buf)) {
            break;
        }
    }
    fclose(fh);
    return ret;
}

static char **sawoBuildEnv(sawo_Interp *interp)
{
    int i;
    int size;
    int num;
    int n;
    char **envptr;
    char *envdata;

    sawo_Obj *objPtr = sawo_GetGlobalVariableStr(interp, "env", SAWO_NONE);

    if (!objPtr) {
        return sawo_GetEnviron();
    }

    num = sawo_ListLength(interp, objPtr);
    if (num % 2) {
        num--;
    }
    size = sawo_Length(objPtr) + 2;

    envptr = sawo_Alloc(sizeof(*envptr) * (num / 2 + 1) + size);
    envdata = (char *)&envptr[num / 2 + 1];

    n = 0;
    for (i = 0; i < num; i += 2) {
        const char *s1, *s2;
        sawo_Obj *elemObj;

        sawo_ListIndex(interp, objPtr, i, &elemObj, SAWO_NONE);
        s1 = sawo_String(elemObj);
        sawo_ListIndex(interp, objPtr, i + 1, &elemObj, SAWO_NONE);
        s2 = sawo_String(elemObj);

        envptr[n] = envdata;
        envdata += sprintf(envdata, "%s=%s", s1, s2);
        envdata++;
        n++;
    }
    envptr[n] = NULL;
    *envdata = 0;

    return envptr;
}

static void sawoFreeEnv(char **env, char **original_environ)
{
    if (env != original_environ) {
        sawo_Free(env);
    }
}

#ifndef sawo_ext_signal

const char *sawo_SignalId(int sig)
{
    static char buf[10];
    snprintf(buf, sizeof(buf), "%d", sig);
    return buf;
}

const char *sawo_SignalName(int sig)
{
    return sawo_SignalId(sig);
}
#endif

static int sawoCheckWaitStatus(sawo_Interp *interp, pidtype pid, int waitStatus, sawo_Obj *errStrObj)
{
    sawo_Obj *errorCode;

    if (WIFEXITED(waitStatus) && WEXITSTATUS(waitStatus) == 0) {
        return SAWO_OK;
    }
    errorCode = sawo_NewListObj(interp, NULL, 0);

    if (WIFEXITED(waitStatus)) {
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, "CHILDSTATUS", -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, (long)pid));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, WEXITSTATUS(waitStatus)));
    }
    else {
        const char *type;
        const char *action;

        if (WIFSIGNALED(waitStatus)) {
            type = "CHILDKILLED";
            action = "killed";
        }
        else {
            type = "CHILDSUSP";
            action = "suspended";
        }

        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, type, -1));

        if (errStrObj) {
            sawo_AppendStrings(interp, errStrObj, "child ", action, " by signal ", sawo_SignalId(WTERMSIG(waitStatus)), "\n", NULL);
        }

        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, (long)pid));
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, sawo_SignalId(WTERMSIG(waitStatus)), -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, sawo_SignalName(WTERMSIG(waitStatus)), -1));
    }
    sawo_SetGlobalVariableStr(interp, "errorCode", errorCode);

    return SAWO_ERR;
}


struct WaitInfo
{
    pidtype pid;
    int status;
    int flags;
};

struct WaitInfoTable {
    struct WaitInfo *show;      
    int size;                   
    int used;                   
};


#define WI_DETACHED 2

#define WAIT_TABLE_GROW_BY 4

static void sawoFreeWaitInfoTable(struct sawo_Interp *interp, void *privData)
{
    struct WaitInfoTable *table = privData;

    sawo_Free(table->show);
    sawo_Free(table);
}

static struct WaitInfoTable *sawoAllocWaitInfoTable(void)
{
    struct WaitInfoTable *table = sawo_Alloc(sizeof(*table));
    table->show = NULL;
    table->size = table->used = 0;

    return table;
}

static int sawo_ExecCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    fdtype outputId;    
    fdtype errorId;     
    pidtype *pidPtr;
    int numPids, result;
    int child_siginfo = 1;
    sawo_Obj *childErrObj;
    sawo_Obj *errStrObj;

    if (argc > 1 && sawo_CompareStringImmediate(interp, argv[argc - 1], "&")) {
        sawo_Obj *listObj;
        int i;

        argc--;
        numPids = sawoCreatePipeline(interp, argc - 1, argv + 1, &pidPtr, NULL, NULL, NULL);
        if (numPids < 0) {
            return SAWO_ERR;
        }
        listObj = sawo_NewListObj(interp, NULL, 0);
        for (i = 0; i < numPids; i++) {
            sawo_ListAppendElement(interp, listObj, sawo_NewIntObj(interp, (long)pidPtr[i]));
        }
        sawo_SetResult(interp, listObj);
        sawoDetachPids(interp, numPids, pidPtr);
        sawo_Free(pidPtr);
        return SAWO_OK;
    }

    numPids =
        sawoCreatePipeline(interp, argc - 1, argv + 1, &pidPtr, NULL, &outputId, &errorId);

    if (numPids < 0) {
        return SAWO_ERR;
    }

    result = SAWO_OK;

    errStrObj = sawo_NewStringObj(interp, "", 0);

    
    if (outputId != SAWO_BAD_FD) {
        if (sawoAppendStreamToString(interp, outputId, errStrObj) < 0) {
            result = SAWO_ERR;
            sawo_SetResultErrno(interp, "error reading from output pipe");
        }
    }

    
    childErrObj = sawo_NewStringObj(interp, "", 0);
    sawo_IncrRefCount(childErrObj);

    if (sawoCleanupChildren(interp, numPids, pidPtr, childErrObj) != SAWO_OK) {
        result = SAWO_ERR;
    }

    if (errorId != SAWO_BAD_FD) {
        int ret;
        sawoRewindFd(errorId);
        ret = sawoAppendStreamToString(interp, errorId, errStrObj);
        if (ret < 0) {
            sawo_SetResultErrno(interp, "error reading from error pipe");
            result = SAWO_ERR;
        }
        else if (ret > 0) {
            child_siginfo = 0;
        }
    }

    if (child_siginfo) {
        
        sawo_AppendObj(interp, errStrObj, childErrObj);
    }
    sawo_DecrRefCount(interp, childErrObj);

    
    sawo_RemoveTrailingNewline(errStrObj);

    
    sawo_SetResult(interp, errStrObj);

    return result;
}

static void sawoReapDetachedPids(struct WaitInfoTable *table)
{
    struct WaitInfo *waitPtr;
    int count;
    int dest;

    if (!table) {
        return;
    }

    waitPtr = table->show;
    dest = 0;
    for (count = table->used; count > 0; waitPtr++, count--) {
        if (waitPtr->flags & WI_DETACHED) {
            int status;
            pidtype pid = sawoWaitPid(waitPtr->pid, &status, WNOHANG);
            if (pid == waitPtr->pid) {
                
                table->used--;
                continue;
            }
        }
        if (waitPtr != &table->show[dest]) {
            table->show[dest] = *waitPtr;
        }
        dest++;
    }
}

static pidtype sawoWaitForProcess(struct WaitInfoTable *table, pidtype pid, int *statusPtr)
{
    int i;

    
    for (i = 0; i < table->used; i++) {
        if (pid == table->show[i].pid) {
            
            sawoWaitPid(pid, statusPtr, 0);

            
            if (i != table->used - 1) {
                table->show[i] = table->show[table->used - 1];
            }
            table->used--;
            return pid;
        }
    }

    
    return SAWO_BAD_PID;
}

static void sawoDetachPids(sawo_Interp *interp, int numPids, const pidtype *pidPtr)
{
    int j;
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);

    for (j = 0; j < numPids; j++) {
        
        int i;
        for (i = 0; i < table->used; i++) {
            if (pidPtr[j] == table->show[i].pid) {
                table->show[i].flags |= WI_DETACHED;
                break;
            }
        }
    }
}

static FILE *sawoGetAioFilehandle(sawo_Interp *interp, const char *name)
{
    FILE *fh;
    sawo_Obj *fhObj;

    fhObj = sawo_NewStringObj(interp, name, -1);
    sawo_IncrRefCount(fhObj);
    fh = sawo_AioFilehandle(interp, fhObj);
    sawo_DecrRefCount(interp, fhObj);

    return fh;
}

static int
sawoCreatePipeline(sawo_Interp *interp, int argc, sawo_Obj *const *argv, pidtype **pidArrayPtr,
    fdtype *inPipePtr, fdtype *outPipePtr, fdtype *errFilePtr)
{
    pidtype *pidPtr = NULL;
    int numPids = 0;
    int cmdCount;
    const char *input = NULL;
    int input_len = 0;          

#define FILE_NAME   0           
#define FILE_APPEND 1           
#define FILE_HANDLE 2           
#define FILE_TEXT   3           

    int inputFile = FILE_NAME;  /* 1 means input is name of input file.
                                 * 2 means input is filehandle name.
                                 * 0 means input holds actual
                                 * text to be input to command. */

    int outputFile = FILE_NAME; /* 0 means output is the name of output file.
                                 * 1 means output is the name of output file, and append.
                                 * 2 means output is filehandle name.
                                 * All this is ignored if output is NULL
                                 */
    int errorFile = FILE_NAME;  /* 0 means error is the name of error file.
                                 * 1 means error is the name of error file, and append.
                                 * 2 means error is filehandle name.
                                 * All this is ignored if error is NULL
                                 */
    const char *output = NULL;  /* Holds name of output file to pipe to,
                                 * or NULL if output goes to stdout/pipe. */
    const char *error = NULL;   /* Holds name of stderr file to pipe to,
                                 * or NULL if stderr goes to stderr/pipe. */
    fdtype inputId = SAWO_BAD_FD;
    fdtype outputId = SAWO_BAD_FD;
    fdtype errorId = SAWO_BAD_FD;
    fdtype lastOutputId = SAWO_BAD_FD;
    fdtype pipeIds[2];           
    int firstArg, lastArg;      /* Indexes of first and last arguments in
                                 * current command. */
    int lastBar;
    int i;
    pidtype pid;
    char **save_environ;
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);

    char **arg_array = sawo_Alloc(sizeof(*arg_array) * (argc + 1));
    int arg_count = 0;

    sawoReapDetachedPids(table);

    if (inPipePtr != NULL) {
        *inPipePtr = SAWO_BAD_FD;
    }
    if (outPipePtr != NULL) {
        *outPipePtr = SAWO_BAD_FD;
    }
    if (errFilePtr != NULL) {
        *errFilePtr = SAWO_BAD_FD;
    }
    pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;

    cmdCount = 1;
    lastBar = -1;
    for (i = 0; i < argc; i++) {
        const char *arg = sawo_String(argv[i]);

        if (arg[0] == '<') {
            inputFile = FILE_NAME;
            input = arg + 1;
            if (*input == '<') {
                inputFile = FILE_TEXT;
                input_len = sawo_Length(argv[i]) - 2;
                input++;
            }
            else if (*input == '@') {
                inputFile = FILE_HANDLE;
                input++;
            }

            if (!*input && ++i < argc) {
                input = sawo_GetString(argv[i], &input_len);
            }
        }
        else if (arg[0] == '>') {
            int dup_error = 0;

            outputFile = FILE_NAME;

            output = arg + 1;
            if (*output == '>') {
                outputFile = FILE_APPEND;
                output++;
            }
            if (*output == '&') {
                output++;
                dup_error = 1;
            }
            if (*output == '@') {
                outputFile = FILE_HANDLE;
                output++;
            }
            if (!*output && ++i < argc) {
                output = sawo_String(argv[i]);
            }
            if (dup_error) {
                errorFile = outputFile;
                error = output;
            }
        }
        else if (arg[0] == '2' && arg[1] == '>') {
            error = arg + 2;
            errorFile = FILE_NAME;

            if (*error == '@') {
                errorFile = FILE_HANDLE;
                error++;
            }
            else if (*error == '>') {
                errorFile = FILE_APPEND;
                error++;
            }
            if (!*error && ++i < argc) {
                error = sawo_String(argv[i]);
            }
        }
        else {
            if (strcmp(arg, "|") == 0 || strcmp(arg, "|&") == 0) {
                if (i == lastBar + 1 || i == argc - 1) {
                    sawo_SetResultString(interp, "illegal use of | or |& in command", -1);
                    goto badargs;
                }
                lastBar = i;
                cmdCount++;
            }
            arg_array[arg_count++] = (char *)arg;
            continue;
        }

        if (i >= argc) {
            sawo_SetResultFormatted(interp, "can't specify \"%s\" as last word in command", arg);
            goto badargs;
        }
    }

    if (arg_count == 0) {
        sawo_SetResultString(interp, "didn't specify command to execute", -1);
badargs:
        sawo_Free(arg_array);
        return -1;
    }

    save_environ = sawoSaveEnv(sawoBuildEnv(interp));

    if (input != NULL) {
        if (inputFile == FILE_TEXT) {
            inputId = sawoCreateTemp(interp, input, input_len);
            if (inputId == SAWO_BAD_FD) {
                goto error;
            }
        }
        else if (inputFile == FILE_HANDLE) {
            FILE *fh = sawoGetAioFilehandle(interp, input);

            if (fh == NULL) {
                goto error;
            }
            inputId = sawoDupFd(sawoFileno(fh));
        }
        else {
            inputId = sawoOpenForRead(input);
            if (inputId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't read file \"%s\": %s", input, sawoStrError());
                goto error;
            }
        }
    }
    else if (inPipePtr != NULL) {
        if (sawoPipe(pipeIds) != 0) {
            sawo_SetResultErrno(interp, "couldn't create input pipe for command");
            goto error;
        }
        inputId = pipeIds[0];
        *inPipePtr = pipeIds[1];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }

    if (output != NULL) {
        if (outputFile == FILE_HANDLE) {
            FILE *fh = sawoGetAioFilehandle(interp, output);
            if (fh == NULL) {
                goto error;
            }
            fflush(fh);
            lastOutputId = sawoDupFd(sawoFileno(fh));
        }
        else {
            lastOutputId = sawoOpenForWrite(output, outputFile == FILE_APPEND);
            if (lastOutputId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't write file \"%s\": %s", output, sawoStrError());
                goto error;
            }
        }
    }
    else if (outPipePtr != NULL) {
        if (sawoPipe(pipeIds) != 0) {
            sawo_SetResultErrno(interp, "couldn't create output pipe");
            goto error;
        }
        lastOutputId = pipeIds[1];
        *outPipePtr = pipeIds[0];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }

    if (error != NULL) {
        if (errorFile == FILE_HANDLE) {
            if (strcmp(error, "1") == 0) {
                if (lastOutputId != SAWO_BAD_FD) {
                    errorId = sawoDupFd(lastOutputId);
                }
                else {
                    error = "stdout";
                }
            }
            if (errorId == SAWO_BAD_FD) {
                FILE *fh = sawoGetAioFilehandle(interp, error);
                if (fh == NULL) {
                    goto error;
                }
                fflush(fh);
                errorId = sawoDupFd(sawoFileno(fh));
            }
        }
        else {
            errorId = sawoOpenForWrite(error, errorFile == FILE_APPEND);
            if (errorId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't write file \"%s\": %s", error, sawoStrError());
                goto error;
            }
        }
    }
    else if (errFilePtr != NULL) {
        errorId = sawoCreateTemp(interp, NULL, 0);
        if (errorId == SAWO_BAD_FD) {
            goto error;
        }
        *errFilePtr = sawoDupFd(errorId);
    }


    pidPtr = sawo_Alloc(cmdCount * sizeof(*pidPtr));
    for (i = 0; i < numPids; i++) {
        pidPtr[i] = SAWO_BAD_PID;
    }
    for (firstArg = 0; firstArg < arg_count; numPids++, firstArg = lastArg + 1) {
        int pipe_dup_err = 0;
        fdtype origErrorId = errorId;

        for (lastArg = firstArg; lastArg < arg_count; lastArg++) {
            if (arg_array[lastArg][0] == '|') {
                if (arg_array[lastArg][1] == '&') {
                    pipe_dup_err = 1;
                }
                break;
            }
        }

        arg_array[lastArg] = NULL;
        if (lastArg == arg_count) {
            outputId = lastOutputId;
        }
        else {
            if (sawoPipe(pipeIds) != 0) {
                sawo_SetResultErrno(interp, "couldn't create pipe");
                goto error;
            }
            outputId = pipeIds[1];
        }

        
        if (pipe_dup_err) {
            errorId = outputId;
        }

        

#ifdef __MINGW32__
        pid = sawoStartWinProcess(interp, &arg_array[firstArg], save_environ ? save_environ[0] : NULL, inputId, outputId, errorId);
        if (pid == SAWO_BAD_PID) {
            sawo_SetResultFormatted(interp, "couldn't exec \"%s\"", arg_array[firstArg]);
            goto error;
        }
#else
        pid = vfork();
        if (pid < 0) {
            sawo_SetResultErrno(interp, "couldn't fork child process");
            goto error;
        }
        if (pid == 0) {
            

            if (inputId != -1) dup2(inputId, 0);
            if (outputId != -1) dup2(outputId, 1);
            if (errorId != -1) dup2(errorId, 2);

            for (i = 3; (i <= outputId) || (i <= inputId) || (i <= errorId); i++) {
                close(i);
            }

            
            (void)signal(SIGPIPE, SIG_DFL);

            execvpe(arg_array[firstArg], &arg_array[firstArg], sawo_GetEnviron());

            
            fprintf(stderr, "couldn't exec \"%s\"\n", arg_array[firstArg]);
            _exit(127);
        }
#endif

        

        if (table->used == table->size) {
            table->size += WAIT_TABLE_GROW_BY;
            table->show = sawo_Realloc(table->show, table->size * sizeof(*table->show));
        }

        table->show[table->used].pid = pid;
        table->show[table->used].flags = 0;
        table->used++;

        pidPtr[numPids] = pid;

        
        errorId = origErrorId;


        if (inputId != SAWO_BAD_FD) {
            sawoCloseFd(inputId);
        }
        if (outputId != SAWO_BAD_FD) {
            sawoCloseFd(outputId);
        }
        inputId = pipeIds[0];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }
    *pidArrayPtr = pidPtr;


  cleanup:
    if (inputId != SAWO_BAD_FD) {
        sawoCloseFd(inputId);
    }
    if (lastOutputId != SAWO_BAD_FD) {
        sawoCloseFd(lastOutputId);
    }
    if (errorId != SAWO_BAD_FD) {
        sawoCloseFd(errorId);
    }
    sawo_Free(arg_array);

    sawoRestoreEnv(save_environ);

    return numPids;


  error:
    if ((inPipePtr != NULL) && (*inPipePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*inPipePtr);
        *inPipePtr = SAWO_BAD_FD;
    }
    if ((outPipePtr != NULL) && (*outPipePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*outPipePtr);
        *outPipePtr = SAWO_BAD_FD;
    }
    if ((errFilePtr != NULL) && (*errFilePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*errFilePtr);
        *errFilePtr = SAWO_BAD_FD;
    }
    if (pipeIds[0] != SAWO_BAD_FD) {
        sawoCloseFd(pipeIds[0]);
    }
    if (pipeIds[1] != SAWO_BAD_FD) {
        sawoCloseFd(pipeIds[1]);
    }
    if (pidPtr != NULL) {
        for (i = 0; i < numPids; i++) {
            if (pidPtr[i] != SAWO_BAD_PID) {
                sawoDetachPids(interp, 1, &pidPtr[i]);
            }
        }
        sawo_Free(pidPtr);
    }
    numPids = -1;
    goto cleanup;
}


static int sawoCleanupChildren(sawo_Interp *interp, int numPids, pidtype *pidPtr, sawo_Obj *errStrObj)
{
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);
    int result = SAWO_OK;
    int i;

    
    for (i = 0; i < numPids; i++) {
        int waitStatus = 0;
        if (sawoWaitForProcess(table, pidPtr[i], &waitStatus) != SAWO_BAD_PID) {
            if (sawoCheckWaitStatus(interp, pidPtr[i], waitStatus, errStrObj) != SAWO_OK) {
                result = SAWO_ERR;
            }
        }
    }
    sawo_Free(pidPtr);

    return result;
}

int sawo_execInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "exec", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

#ifdef SIGPIPE
    (void)signal(SIGPIPE, SIG_IGN);
#endif

    sawo_CreateCommand(interp, "exec", sawo_ExecCmd, sawoAllocWaitInfoTable(), sawoFreeWaitInfoTable);
    return SAWO_OK;
}

#if defined(__MINGW32__)


static SECURITY_ATTRIBUTES *sawoStdSecAttrs(void)
{
    static SECURITY_ATTRIBUTES secAtts;

    secAtts.nLength = sizeof(SECURITY_ATTRIBUTES);
    secAtts.lpSecurityDescriptor = NULL;
    secAtts.bInheritHandle = TRUE;
    return &secAtts;
}

static int sawoErrno(void)
{
    switch (GetLastError()) {
    case ERROR_FILE_NOT_FOUND: return ENOENT;
    case ERROR_PATH_NOT_FOUND: return ENOENT;
    case ERROR_TOO_MANY_OPEN_FILES: return EMFILE;
    case ERROR_ACCESS_DENIED: return EACCES;
    case ERROR_INVALID_HANDLE: return EBADF;
    case ERROR_BAD_ENVIRONMENT: return E2BIG;
    case ERROR_BAD_FORMAT: return ENOEXEC;
    case ERROR_INVALID_ACCESS: return EACCES;
    case ERROR_INVALID_DRIVE: return ENOENT;
    case ERROR_CURRENT_DIRECTORY: return EACCES;
    case ERROR_NOT_SAME_DEVICE: return EXDEV;
    case ERROR_NO_MORE_FILES: return ENOENT;
    case ERROR_WRITE_PROTECT: return EROFS;
    case ERROR_BAD_UNIT: return ENXIO;
    case ERROR_NOT_READY: return EBUSY;
    case ERROR_BAD_COMMAND: return EIO;
    case ERROR_CRC: return EIO;
    case ERROR_BAD_LENGTH: return EIO;
    case ERROR_SEEK: return EIO;
    case ERROR_WRITE_FAULT: return EIO;
    case ERROR_READ_FAULT: return EIO;
    case ERROR_GEN_FAILURE: return EIO;
    case ERROR_SHARING_VIOLATION: return EACCES;
    case ERROR_LOCK_VIOLATION: return EACCES;
    case ERROR_SHARING_BUFFER_EXCEEDED: return ENFILE;
    case ERROR_HANDLE_DISK_FULL: return ENOSPC;
    case ERROR_NOT_SUPPORTED: return ENODEV;
    case ERROR_REM_NOT_LIST: return EBUSY;
    case ERROR_DUP_NAME: return EEXIST;
    case ERROR_BAD_NETPATH: return ENOENT;
    case ERROR_NETWORK_BUSY: return EBUSY;
    case ERROR_DEV_NOT_EXIST: return ENODEV;
    case ERROR_TOO_MANY_CMDS: return EAGAIN;
    case ERROR_ADAP_HDW_ERR: return EIO;
    case ERROR_BAD_NET_RESP: return EIO;
    case ERROR_UNEXP_NET_ERR: return EIO;
    case ERROR_NETNAME_DELETED: return ENOENT;
    case ERROR_NETWORK_ACCESS_DENIED: return EACCES;
    case ERROR_BAD_DEV_TYPE: return ENODEV;
    case ERROR_BAD_NET_NAME: return ENOENT;
    case ERROR_TOO_MANY_NAMES: return ENFILE;
    case ERROR_TOO_MANY_SESS: return EIO;
    case ERROR_SHARING_PAUSED: return EAGAIN;
    case ERROR_REDIR_PAUSED: return EAGAIN;
    case ERROR_FILE_EXISTS: return EEXIST;
    case ERROR_CANNOT_MAKE: return ENOSPC;
    case ERROR_OUT_OF_STRUCTURES: return ENFILE;
    case ERROR_ALREADY_ASSIGNED: return EEXIST;
    case ERROR_INVALID_PASSWORD: return EPERM;
    case ERROR_NET_WRITE_FAULT: return EIO;
    case ERROR_NO_RAISE_SLOTS: return EAGAIN;
    case ERROR_DISK_CHANGE: return EXDEV;
    case ERROR_BROKEN_PIPE: return EPIPE;
    case ERROR_OPEN_FAILED: return ENOENT;
    case ERROR_DISK_FULL: return ENOSPC;
    case ERROR_NO_MORE_SEARCH_HANDLES: return EMFILE;
    case ERROR_INVALID_TARGET_HANDLE: return EBADF;
    case ERROR_INVALID_NAME: return ENOENT;
    case ERROR_RAISE_NOT_FOUND: return ESRCH;
    case ERROR_WAIT_NO_CHILDREN: return ECHILD;
    case ERROR_CHILD_NOT_COMPLETE: return ECHILD;
    case ERROR_DIRECT_ACCESS_HANDLE: return EBADF;
    case ERROR_SEEK_ON_DEVICE: return ESPIPE;
    case ERROR_BUSY_DRIVE: return EAGAIN;
    case ERROR_DIR_NOT_EMPTY: return EEXIST;
    case ERROR_NOT_LOCKED: return EACCES;
    case ERROR_BAD_PATHNAME: return ENOENT;
    case ERROR_LOCK_FAILED: return EACCES;
    case ERROR_ALREADY_EXISTS: return EEXIST;
    case ERROR_FILENAME_EXCED_RANGE: return ENAMETOOLONG;
    case ERROR_BAD_PIPE: return EPIPE;
    case ERROR_PIPE_BUSY: return EAGAIN;
    case ERROR_PIPE_NOT_CONNECTED: return EPIPE;
    case ERROR_DIRECTORY: return ENOTDIR;
    }
    return EINVAL;
}

static int sawoPipe(fdtype pipefd[2])
{
    if (CreatePipe(&pipefd[0], &pipefd[1], NULL, 0)) {
        return 0;
    }
    return -1;
}

static fdtype sawoDupFd(fdtype infd)
{
    fdtype dupfd;
    pidtype pid = GetCurrentProcess();

    if (DuplicateHandle(pid, infd, pid, &dupfd, 0, TRUE, DUPLICATE_SAME_ACCESS)) {
        return dupfd;
    }
    return SAWO_BAD_FD;
}

static int sawoRewindFd(fdtype fd)
{
    return SetFilePointer(fd, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER ? -1 : 0;
}

#if 0
static int sawoReadFd(fdtype fd, char *buffer, size_t len)
{
    DWORD num;

    if (ReadFile(fd, buffer, len, &num, NULL)) {
        return num;
    }
    if (GetLastError() == ERROR_HANDLE_EOF || GetLastError() == ERROR_BROKEN_PIPE) {
        return 0;
    }
    return -1;
}
#endif

static FILE *sawoFdOpenForRead(fdtype fd)
{
    return _fdopen(_open_osfhandle((int)fd, _O_RDONLY | _O_TEXT), "r");
}

static fdtype sawoFileno(FILE *fh)
{
    return (fdtype)_get_osfhandle(_fileno(fh));
}

static fdtype sawoOpenForRead(const char *filename)
{
    return CreateFile(filename, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
        sawoStdSecAttrs(), OPEN_EXISTING, 0, NULL);
}

static fdtype sawoOpenForWrite(const char *filename, int append)
{
    return CreateFile(filename, append ? FILE_APPEND_DATA : GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
        sawoStdSecAttrs(), append ? OPEN_ALWAYS : CREATE_ALWAYS, 0, (HANDLE) NULL);
}

static FILE *sawoFdOpenForWrite(fdtype fd)
{
    return _fdopen(_open_osfhandle((int)fd, _O_TEXT), "w");
}

static pidtype sawoWaitPid(pidtype pid, int *status, int nohang)
{
    DWORD ret = WaitForSingleObject(pid, nohang ? 0 : INFINITE);
    if (ret == WAIT_TIMEOUT || ret == WAIT_FAILED) {
        
        return SAWO_BAD_PID;
    }
    GetExitCodeProcess(pid, &ret);
    *status = ret;
    CloseHandle(pid);
    return pid;
}

static HANDLE sawoCreateTemp(sawo_Interp *interp, const char *contents, int len)
{
    char name[MAX_PATH];
    HANDLE handle;

    if (!GetTempPath(MAX_PATH, name) || !GetTempFileName(name, "SAWO", 0, name)) {
        return SAWO_BAD_FD;
    }

    handle = CreateFile(name, GENERIC_READ | GENERIC_WRITE, 0, sawoStdSecAttrs(),
            CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
            NULL);

    if (handle == INVALID_HANDLE_VALUE) {
        goto error;
    }

    if (contents != NULL) {
        
        FILE *fh = sawoFdOpenForWrite(sawoDupFd(handle));
        if (fh == NULL) {
            goto error;
        }

        if (fwrite(contents, len, 1, fh) != 1) {
            fclose(fh);
            goto error;
        }
        fseek(fh, 0, SEEK_SET);
        fclose(fh);
    }
    return handle;

  error:
    sawo_SetResultErrno(interp, "failed to create temp file");
    CloseHandle(handle);
    DeleteFile(name);
    return SAWO_BAD_FD;
}

static int
sawoWinFindExecutable(const char *originalName, char fullPath[MAX_PATH])
{
    int i;
    static char extensions[][5] = {".exe", "", ".bat"};

    for (i = 0; i < (int) (sizeof(extensions) / sizeof(extensions[0])); i++) {
        snprintf(fullPath, MAX_PATH, "%s%s", originalName, extensions[i]);

        if (SearchPath(NULL, fullPath, NULL, MAX_PATH, fullPath, NULL) == 0) {
            continue;
        }
        if (GetFileAttributes(fullPath) & FILE_ATTRIBUTE_DIRECTORY) {
            continue;
        }
        return 0;
    }

    return -1;
}

static char **sawoSaveEnv(char **env)
{
    return env;
}

static void sawoRestoreEnv(char **env)
{
    sawoFreeEnv(env, sawo_GetEnviron());
}

static sawo_Obj *
sawoWinBuildCommandLine(sawo_Interp *interp, char **argv)
{
    char *start, *special;
    int quote, i;

    sawo_Obj *strObj = sawo_NewStringObj(interp, "", 0);

    for (i = 0; argv[i]; i++) {
        if (i > 0) {
            sawo_AppendString(interp, strObj, " ", 1);
        }

        if (argv[i][0] == '\0') {
            quote = 1;
        }
        else {
            quote = 0;
            for (start = argv[i]; *start != '\0'; start++) {
                if (isspace(UCHAR(*start))) {
                    quote = 1;
                    break;
                }
            }
        }
        if (quote) {
            sawo_AppendString(interp, strObj, "\"" , 1);
        }

        start = argv[i];
        for (special = argv[i]; ; ) {
            if ((*special == '\\') && (special[1] == '\\' ||
                    special[1] == '"' || (quote && special[1] == '\0'))) {
                sawo_AppendString(interp, strObj, start, special - start);
                start = special;
                while (1) {
                    special++;
                    if (*special == '"' || (quote && *special == '\0')) {

                        sawo_AppendString(interp, strObj, start, special - start);
                        break;
                    }
                    if (*special != '\\') {
                        break;
                    }
                }
                sawo_AppendString(interp, strObj, start, special - start);
                start = special;
            }
            if (*special == '"') {
        if (special == start) {
            sawo_AppendString(interp, strObj, "\"", 1);
        }
        else {
            sawo_AppendString(interp, strObj, start, special - start);
        }
                sawo_AppendString(interp, strObj, "\\\"", 2);
                start = special + 1;
            }
            if (*special == '\0') {
                break;
            }
            special++;
        }
        sawo_AppendString(interp, strObj, start, special - start);
        if (quote) {
            sawo_AppendString(interp, strObj, "\"", 1);
        }
    }
    return strObj;
}

static pidtype
sawoStartWinProcess(sawo_Interp *interp, char **argv, char *env, fdtype inputId, fdtype outputId, fdtype errorId)
{
    STARTUPINFO startInfo;
    PROCESS_INFORMATION procInfo;
    HANDLE hProcess, h;
    char execPath[MAX_PATH];
    pidtype pid = SAWO_BAD_PID;
    sawo_Obj *cmdLineObj;

    if (sawoWinFindExecutable(argv[0], execPath) < 0) {
        return SAWO_BAD_PID;
    }
    argv[0] = execPath;

    hProcess = GetCurrentProcess();
    cmdLineObj = sawoWinBuildCommandLine(interp, argv);


    ZeroMemory(&startInfo, sizeof(startInfo));
    startInfo.cb = sizeof(startInfo);
    startInfo.dwFlags   = STARTF_USESTDHANDLES;
    startInfo.hStdInput = INVALID_HANDLE_VALUE;
    startInfo.hStdOutput= INVALID_HANDLE_VALUE;
    startInfo.hStdError = INVALID_HANDLE_VALUE;

    if (inputId == SAWO_BAD_FD) {
        if (CreatePipe(&startInfo.hStdInput, &h, sawoStdSecAttrs(), 0) != FALSE) {
            CloseHandle(h);
        }
    } else {
        DuplicateHandle(hProcess, inputId, hProcess, &startInfo.hStdInput,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdInput == SAWO_BAD_FD) {
        goto end;
    }

    if (outputId == SAWO_BAD_FD) {
        startInfo.hStdOutput = CreateFile("NUL:", GENERIC_WRITE, 0,
                sawoStdSecAttrs(), OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    } else {
        DuplicateHandle(hProcess, outputId, hProcess, &startInfo.hStdOutput,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdOutput == SAWO_BAD_FD) {
        goto end;
    }

    if (errorId == SAWO_BAD_FD) {

        startInfo.hStdError = CreateFile("NUL:", GENERIC_WRITE, 0,
                sawoStdSecAttrs(), OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    } else {
        DuplicateHandle(hProcess, errorId, hProcess, &startInfo.hStdError,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdError == SAWO_BAD_FD) {
        goto end;
    }

    if (!CreateProcess(NULL, (char *)sawo_String(cmdLineObj), NULL, NULL, TRUE,
            0, env, NULL, &startInfo, &procInfo)) {
        goto end;
    }


    WaitForInputIdle(procInfo.hProcess, 5000);
    CloseHandle(procInfo.hThread);

    pid = procInfo.hProcess;

    end:
    sawo_FreeNewObj(interp, cmdLineObj);
    if (startInfo.hStdInput != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdInput);
    }
    if (startInfo.hStdOutput != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdOutput);
    }
    if (startInfo.hStdError != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdError);
    }
    return pid;
}
#else

static int sawoOpenForWrite(const char *filename, int append)
{
    return open(filename, O_WRONLY | O_CREAT | (append ? O_APPEND : O_TRUNC), 0666);
}

static int sawoRewindFd(int fd)
{
    return lseek(fd, 0L, SEEK_SET);
}

static int sawoCreateTemp(sawo_Interp *interp, const char *contents, int len)
{
    int fd = sawo_MakeTempFile(interp, NULL);

    if (fd != SAWO_BAD_FD) {
        unlink(sawo_String(sawo_GetResult(interp)));
        if (contents) {
            if (write(fd, contents, len) != len) {
                sawo_SetResultErrno(interp, "couldn't write temp file");
                close(fd);
                return -1;
            }
            lseek(fd, 0L, SEEK_SET);
        }
    }
    return fd;
}

static char **sawoSaveEnv(char **env)
{
    char **saveenv = sawo_GetEnviron();
    sawo_SetEnviron(env);
    return saveenv;
}

static void sawoRestoreEnv(char **env)
{
    sawoFreeEnv(sawo_GetEnviron(), env);
    sawo_SetEnviron(env);
}

#endif

#endif
