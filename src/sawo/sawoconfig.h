/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <hyangversion.h>

#define _GNU_SOURCE
#define SAWO_HYANG_COMPAT
#define SAWO_REFERENCES
#define SAWO_ANSIC
#define SAWO_REGEXP
#define HAVE_NO_AUTOCONF
#define _SAWOAUTOCONF_H
#define HYANG_LIBRARY "."
#define sawo_ext_bootstrap
#define sawo_ext_aio
#define sawo_ext_readdir
#define sawo_ext_regexp
#define sawo_ext_file
#define sawo_ext_glob
#define sawo_ext_exec
#define sawo_ext_clock
#define sawo_ext_array
#define sawo_ext_stdlib
#define sawo_ext_hyangcompat

#if defined(_MSC_VER)
#define HYANG_PLATFORM_OS "windows"
#define HYANG_PLATFORM_PLATFORM "windows"
#define HYANG_PLATFORM_PATH_SEPARATOR ";"
#define HAVE_MKDIR_ONE_ARG
#define HAVE_SYSTEM
#elif defined(__MINGW32__)
#define HYANG_PLATFORM_OS "mingw"
#define HYANG_PLATFORM_PLATFORM "windows"
#define HYANG_PLATFORM_PATH_SEPARATOR ";"
#define HAVE_MKDIR_ONE_ARG
#define HAVE_SYSTEM
#define HAVE_SYS_TIME_H
#define HAVE_DIRENT_H
#define HAVE_UNISTD_H
#else
#define HYANG_PLATFORM_OS "unknown"
#define HYANG_PLATFORM_PLATFORM "unix"
#define HYANG_PLATFORM_PATH_SEPARATOR ":"
#define HAVE_VFORK
#define HAVE_WAITPID
#define HAVE_ISATTY
#define HAVE_MKSTEMP
#define HAVE_LINK
#define HAVE_SYS_TIME_H
#define HAVE_DIRENT_H
#define HAVE_UNISTD_H
#endif

