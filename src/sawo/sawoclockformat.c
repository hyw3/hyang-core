/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>


#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

static int clock_cmd_format(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    char buf[100];
    time_t t;
    long seconds;

    const char *format = "%a %b %d %H:%M:%S %Z %Y";

    if (argc == 2 || (argc == 3 && !sawo_CompareStringImmediate(interp, argv[1], "-format"))) {
        return -1;
    }

    if (argc == 3) {
        format = sawo_String(argv[2]);
    }

    if (sawo_GetLong(interp, argv[0], &seconds) != SAWO_OK) {
        return SAWO_ERR;
    }
    t = seconds;

    if (strftime(buf, sizeof(buf), format, localtime(&t)) == 0) {
        sawo_SetResultString(interp, "format string too long", -1);
        return SAWO_ERR;
    }

    sawo_SetResultString(interp, buf, -1);

    return SAWO_OK;
}

#ifdef HAVE_STRPTIME
static int clock_cmd_scan(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    char *pt;
    struct tm tm;
    time_t now = time(0);

    if (!sawo_CompareStringImmediate(interp, argv[1], "-format")) {
        return -1;
    }

    localtime_r(&now, &tm);

    pt = strptime(sawo_String(argv[0]), sawo_String(argv[2]), &tm);
    if (pt == 0 || *pt != 0) {
        sawo_SetResultString(interp, "Failed to parse time according to format", -1);
        return SAWO_ERR;
    }

    sawo_SetResultInt(interp, mktime(&tm));

    return SAWO_OK;
}
#endif

static int clock_cmd_seconds(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_SetResultInt(interp, time(NULL));

    return SAWO_OK;
}

static int clock_cmd_micros(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    sawo_SetResultInt(interp, (sawo_wide) tv.tv_sec * 1000000 + tv.tv_usec);

    return SAWO_OK;
}

static int clock_cmd_millis(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    sawo_SetResultInt(interp, (sawo_wide) tv.tv_sec * 1000 + tv.tv_usec / 1000);

    return SAWO_OK;
}

static const sawo_subcmd_type clock_command_table[] = {
    {   "seconds",
        NULL,
        clock_cmd_seconds,
        0,
        0,

    },
    {   "clicks",
        NULL,
        clock_cmd_micros,
        0,
        0,

    },
    {   "microseconds",
        NULL,
        clock_cmd_micros,
        0,
        0,

    },
    {   "milliseconds",
        NULL,
        clock_cmd_millis,
        0,
        0,

    },
    {   "format",
        "seconds ?-format format?",
        clock_cmd_format,
        1,
        3,

    },
#ifdef HAVE_STRPTIME
    {   "scan",
        "str -format format",
        clock_cmd_scan,
        3,
        3,

    },
#endif
    { NULL }
};

int sawo_clockInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "clock", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "clock", sawo_SubCmdRaise, (void *)clock_command_table, NULL);
    return SAWO_OK;
}

