/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#ifndef SAWO_BOOTSTRAP_LIB_ONLY
#include <errno.h>
#include <string.h>

#ifdef USE_LINENOISE
#ifdef HAVE_UNISTD_H
    #include <unistd.h>
#endif
#include "linenoise.h"
#else
#define MAX_LINE_LEN 512
#endif

char *sawo_HistoryGetline(const char *prompt)
{
#ifdef USE_LINENOISE
    return linenoise(prompt);
#else
    int len;
    char *line = malloc(MAX_LINE_LEN);

    fputs(prompt, stdout);
    fflush(stdout);

    if (fgets(line, MAX_LINE_LEN, stdin) == NULL) {
        free(line);
        return NULL;
    }
    len = strlen(line);
    if (len && line[len - 1] == '\n') {
        line[len - 1] = '\0';
    }
    return line;
#endif
}

void sawo_HistoryLoad(const char *filename)
{
#ifdef USE_LINENOISE
    linenoiseHistoryLoad(filename);
#endif
}

void sawo_HistoryAdd(const char *line)
{
#ifdef USE_LINENOISE
    linenoiseHistoryAdd(line);
#endif
}

void sawo_HistorySave(const char *filename)
{
#ifdef USE_LINENOISE
    linenoiseHistorySave(filename);
#endif
}

void sawo_HistoryShow(void)
{
#ifdef USE_LINENOISE
    int i;
    int len;
    char **history = linenoiseHistory(&len);
    for (i = 0; i < len; i++) {
        printf("%4d %s\n", i + 1, history[i]);
    }
#endif
}

int sawo_InteractivePrompt(sawo_Interp *interp)
{
    int retcode = SAWO_OK;
    char *history_file = NULL;
#ifdef USE_LINENOISE
    const char *home;

    home = getenv("HOME");
    if (home && isatty(STDIN_FILENO)) {
        int history_len = strlen(home) + sizeof("/.sawo_history");
        history_file = sawo_Alloc(history_len);
        snprintf(history_file, history_len, "%s/.sawo_history", home);
        sawo_HistoryLoad(history_file);
    }
#endif

    printf("Welcome to Sawo version %d.%d\n",
        SAWO_VERSION / 100, SAWO_VERSION % 100);
    sawo_SetVariableStrWithStr(interp, SAWO_INTERACTIVE, "1");

    while (1) {
        sawo_Obj *scriptObjPtr;
        const char *result;
        int reslen;
        char prompt[20];

        if (retcode != SAWO_OK) {
            const char *retcodestr = sawo_ReturnCode(retcode);

            if (*retcodestr == '?') {
                snprintf(prompt, sizeof(prompt) - 3, "[%d] > ", retcode);
            }
            else {
                snprintf(prompt, sizeof(prompt) - 3, "[%s] > ", retcodestr);
            }
        }
        else {
            strcpy(prompt, "> ");
        }

        scriptObjPtr = sawo_NewStringObj(interp, "", 0);
        sawo_IncrRefCount(scriptObjPtr);
        while (1) {
            char state;
            char *line;

            line = sawo_HistoryGetline(prompt);
            if (line == NULL) {
                if (errno == EINTR) {
                    continue;
                }
                sawo_DecrRefCount(interp, scriptObjPtr);
                retcode = SAWO_OK;
                goto out;
            }
            if (sawo_Length(scriptObjPtr) != 0) {

                sawo_AppendString(interp, scriptObjPtr, "\n", 1);
            }
            sawo_AppendString(interp, scriptObjPtr, line, -1);
            free(line);
            if (sawo_ScriptIsComplete(interp, scriptObjPtr, &state))
                break;

            snprintf(prompt, sizeof(prompt), "%c> ", state);
        }
#ifdef USE_LINENOISE
        if (strcmp(sawo_String(scriptObjPtr), "h") == 0) {

            sawo_HistoryShow();
            sawo_DecrRefCount(interp, scriptObjPtr);
            continue;
        }

        sawo_HistoryAdd(sawo_String(scriptObjPtr));
        if (history_file) {
            sawo_HistorySave(history_file);
        }
#endif
        retcode = sawo_EvalObj(interp, scriptObjPtr);
        sawo_DecrRefCount(interp, scriptObjPtr);

        if (retcode == SAWO_EXIT) {
            break;
        }
        if (retcode == SAWO_ERR) {
            sawo_MakeErrorMessage(interp);
        }
        result = sawo_GetString(sawo_GetResult(interp), &reslen);
        if (reslen) {
            printf("%s\n", result);
        }
    }
  out:
    sawo_Free(history_file);
    return retcode;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern int sawo_initsawoInit(sawo_Interp *interp);

static void sawoSetArgv(sawo_Interp *interp, int argc, char *const argv[])
{
    int n;
    sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);


    for (n = 0; n < argc; n++) {
        sawo_Obj *obj = sawo_NewStringObj(interp, argv[n], -1);

        sawo_ListAppendElement(interp, listObj, obj);
    }

    sawo_SetVariableStr(interp, "argv", listObj);
    sawo_SetVariableStr(interp, "argc", sawo_NewIntObj(interp, argc));
}

static void sawoPrintErrorMessage(sawo_Interp *interp)
{
    sawo_MakeErrorMessage(interp);
    fprintf(stderr, "%s\n", sawo_String(sawo_GetResult(interp)));
}

void usage(const char* executable_name)
{
    printf("sawo version %d.%d\n", SAWO_VERSION / 100, SAWO_VERSION % 100);
    printf("Usage: %s\n", executable_name);
    printf("or   : %s [options] [filename]\n", executable_name);
    printf("\n");
    printf("Without options: Interactive mode\n");
    printf("\n");
    printf("Options:\n");
    printf("         --version  : prints the version string\n");
    printf("         --help     : prints this text\n");
    printf("         -e CMD     : executes command CMD\n");
    printf("                      NOTE: all subsequent options will be passed as arguments to the command\n");
    printf("         [filename] : executes the script contained in the named file\n");
    printf("                      NOTE: all subsequent options will be passed to the script\n\n");
}

int main(int argc, char *const argv[])
{
    int retcode;
    sawo_Interp *interp;
    char *const orig_argv0 = argv[0];

    if (argc > 1 && strcmp(argv[1], "--version") == 0) {
        printf("%d.%d\n", SAWO_VERSION / 100, SAWO_VERSION % 100);
        return 0;
    }
    else if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        usage(argv[0]);
        return 0;
    }

    interp = sawo_CreateInterp();
    sawo_RegisterCoreCommands(interp);

    if (sawo_InitStaticExtensions(interp) != SAWO_OK) {
        sawoPrintErrorMessage(interp);
    }

    sawo_SetVariableStrWithStr(interp, "sawo::argv0", orig_argv0);
    sawo_SetVariableStrWithStr(interp, SAWO_INTERACTIVE, argc == 1 ? "1" : "0");
    retcode = sawo_initsawoInit(interp);

    if (argc == 1) {
        if (retcode == SAWO_ERR) {
            sawoPrintErrorMessage(interp);
        }
        if (retcode != SAWO_EXIT) {
            sawoSetArgv(interp, 0, NULL);
            retcode = sawo_InteractivePrompt(interp);
        }
    }
    else {
        if (argc > 2 && strcmp(argv[1], "-e") == 0) {
            sawoSetArgv(interp, argc - 3, argv + 3);
            retcode = sawo_Eval(interp, argv[2]);
            if (retcode != SAWO_ERR) {
                printf("%s\n", sawo_String(sawo_GetResult(interp)));
            }
        }
        else {
            sawo_SetVariableStr(interp, "argv0", sawo_NewStringObj(interp, argv[1], -1));
            sawoSetArgv(interp, argc - 2, argv + 2);
            retcode = sawo_EvalFile(interp, argv[1]);
        }
        if (retcode == SAWO_ERR) {
            sawoPrintErrorMessage(interp);
        }
    }
    if (retcode == SAWO_EXIT) {
        retcode = sawo_GetExitCode(interp);
    }
    else if (retcode == SAWO_ERR) {
        retcode = 1;
    }
    else {
        retcode = 0;
    }
    sawo_FreeInterp(interp);
    return retcode;
}

#endif

