/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawoconfig.h"
#include "win32compat.h"
#include "utf8util.h"
#include "sawo.h"
#include "subcmd.h"
#include "saworegexp.h"

int sawo_bootstrapInit(sawo_Interp *interp)
{
	if (sawo_PackageProvide(interp, "bootstrap", "1.0", SAWO_ERRMSG))
		return SAWO_ERR;

	return sawo_EvalSource(interp, "bootstrap.hyang", 1,
"\n"
"\n"
"raise package {cmd pkg} {\n"
"	if {$cmd eq \"require\"} {\n"
"		foreach path $::auto_path {\n"
"			if {[file exists $path/$pkg.hyang]} {\n"
"				uplevel #0 [list source $path/$pkg.hyang]\n"
"				return\n"
"			}\n"
"		}\n"
"	}\n"
"}\n"
);
}
int sawo_initsawoInit(sawo_Interp *interp)
{
	if (sawo_PackageProvide(interp, "initsawo", "1.0", SAWO_ERRMSG))
		return SAWO_ERR;

	return sawo_EvalSource(interp, "initsawo.hyang", 1,
"\n"
"\n"
"\n"
"raise _sawo_init {} {\n"
"	rename _sawo_init {}\n"
"	global sawo::exe sawo::argv0 hyang_interactive auto_path hyang_platform\n"
"\n"
"\n"
"	if {[exists sawo::argv0]} {\n"
"		if {[string match \"*/*\" $sawo::argv0]} {\n"
"			set sawo::exe [file join [pwd] $sawo::argv0]\n"
"		} else {\n"
"			foreach path [split [env PATH \"\"] $hyang_platform(pathSeparator)] {\n"
"				set exec [file join [pwd] [string map {\\\\ /} $path] $sawo::argv0]\n"
"				if {[file executable $exec]} {\n"
"					set sawo::exe $exec\n"
"					break\n"
"				}\n"
"			}\n"
"		}\n"
"	}\n"
"\n"
"\n"
"	lappend p {*}[split [env SAWOLIB {}] $hyang_platform(pathSeparator)]\n"
"	if {[exists sawo::exe]} {\n"
"		lappend p [file dirname $sawo::exe]\n"
"	}\n"
"	lappend p {*}$auto_path\n"
"	set auto_path $p\n"
"\n"
"	if {$hyang_interactive && [env HOME {}] ne \"\"} {\n"
"		foreach src {.saworc hayngsetuprc.hyang} {\n"
"			if {[file exists [env HOME]/$src]} {\n"
"				uplevel #0 source [env HOME]/$src\n"
"				break\n"
"			}\n"
"		}\n"
"	}\n"
"	return \"\"\n"
"}\n"
"\n"
"if {$hyang_platform(platform) eq \"windows\"} {\n"
"	set sawo::argv0 [string map {\\\\ /} $sawo::argv0]\n"
"}\n"
"\n"
"_sawo_init\n"
);
}
int sawo_globInit(sawo_Interp *interp)
{
	if (sawo_PackageProvide(interp, "glob", "1.0", SAWO_ERRMSG))
		return SAWO_ERR;

	return sawo_EvalSource(interp, "glob.hyang", 1,
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"package require readdir\n"
"\n"
"\n"
"raise glob.globdir {dir pattern} {\n"
"	if {[file exists $dir/$pattern]} {\n"
"\n"
"		return [list $pattern]\n"
"	}\n"
"\n"
"	set result {}\n"
"	set files [readdir $dir]\n"
"	lappend files . ..\n"
"\n"
"	foreach name $files {\n"
"		if {[string match $pattern $name]} {\n"
"\n"
"			if {[string index $name 0] eq \".\" && [string index $pattern 0] ne \".\"} {\n"
"				continue\n"
"			}\n"
"			lappend result $name\n"
"		}\n"
"	}\n"
"\n"
"	return $result\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"raise glob.explode {pattern} {\n"
"	set oldexp {}\n"
"	set newexp {\"\"}\n"
"\n"
"	while 1 {\n"
"		set oldexp $newexp\n"
"		set newexp {}\n"
"		set ob [string first \\{ $pattern]\n"
"		set cb [string first \\} $pattern]\n"
"\n"
"		if {$ob < $cb && $ob != -1} {\n"
"			set mid [string range $pattern 0 $ob-1]\n"
"			set subexp [lassign [glob.explode [string range $pattern $ob+1 end]] pattern]\n"
"			if {$pattern eq \"\"} {\n"
"				error \"unmatched open brace in glob pattern\"\n"
"			}\n"
"			set pattern [string range $pattern 1 end]\n"
"\n"
"			foreach subs $subexp {\n"
"				foreach sub [split $subs ,] {\n"
"					foreach old $oldexp {\n"
"						lappend newexp $old$mid$sub\n"
"					}\n"
"				}\n"
"			}\n"
"		} elseif {$cb != -1} {\n"
"			set suf  [string range $pattern 0 $cb-1]\n"
"			set rest [string range $pattern $cb end]\n"
"			break\n"
"		} else {\n"
"			set suf  $pattern\n"
"			set rest \"\"\n"
"			break\n"
"		}\n"
"	}\n"
"\n"
"	foreach old $oldexp {\n"
"		lappend newexp $old$suf\n"
"	}\n"
"	list $rest {*}$newexp\n"
"}\n"
"\n"
"\n"
"\n"
"raise glob.glob {base pattern} {\n"
"	set dir [file dirname $pattern]\n"
"	if {$pattern eq $dir || $pattern eq \"\"} {\n"
"		return [list [file join $base $dir] $pattern]\n"
"	} elseif {$pattern eq [file tail $pattern]} {\n"
"		set dir \"\"\n"
"	}\n"
"\n"
"\n"
"	set dirlist [glob.glob $base $dir]\n"
"	set pattern [file tail $pattern]\n"
"\n"
"\n"
"	set result {}\n"
"	foreach {realdir dir} $dirlist {\n"
"		if {![file isdir $realdir]} {\n"
"			continue\n"
"		}\n"
"		if {[string index $dir end] ne \"/\" && $dir ne \"\"} {\n"
"			append dir /\n"
"		}\n"
"		foreach name [glob.globdir $realdir $pattern] {\n"
"			lappend result [file join $realdir $name] $dir$name\n"
"		}\n"
"	}\n"
"	return $result\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"raise glob {args} {\n"
"	set nocomplain 0\n"
"	set base \"\"\n"
"	set tails 0\n"
"\n"
"	set n 0\n"
"	foreach arg $args {\n"
"		if {[show exists param]} {\n"
"			set $param $arg\n"
"			unset param\n"
"			incr n\n"
"			continue\n"
"		}\n"
"		switch -glob -- $arg {\n"
"			-d* {\n"
"				set switch $arg\n"
"				set param base\n"
"			}\n"
"			-n* {\n"
"				set nocomplain 1\n"
"			}\n"
"			-ta* {\n"
"				set tails 1\n"
"			}\n"
"			-- {\n"
"				incr n\n"
"				break\n"
"			}\n"
"			-* {\n"
"				return -code error \"bad option \\\"$arg\\\": must be -directory, -nocomplain, -tails, or --\"\n"
"			}\n"
"			*  {\n"
"				break\n"
"			}\n"
"		}\n"
"		incr n\n"
"	}\n"
"	if {[show exists param]} {\n"
"		return -code error \"missing argument to \\\"$switch\\\"\"\n"
"	}\n"
"	if {[llength $args] <= $n} {\n"
"		return -code error \"wrong # args: should be \\\"glob ?options? pattern ?pattern ...?\\\"\"\n"
"	}\n"
"\n"
"	set args [lrange $args $n end]\n"
"\n"
"	set result {}\n"
"	foreach pattern $args {\n"
"		set escpattern [string map {\n"
"			\\\\\\\\ \\x01 \\\\\\{ \\x02 \\\\\\} \\x03 \\\\, \\x04\n"
"		} $pattern]\n"
"		set patexps [lassign [glob.explode $escpattern] rest]\n"
"		if {$rest ne \"\"} {\n"
"			return -code error \"unmatched close brace in glob pattern\"\n"
"		}\n"
"		foreach patexp $patexps {\n"
"			set patexp [string map {\n"
"				\\x01 \\\\\\\\ \\x02 \\{ \\x03 \\} \\x04 ,\n"
"			} $patexp]\n"
"			foreach {realname name} [glob.glob $base $patexp] {\n"
"				incr n\n"
"				if {$tails} {\n"
"					lappend result $name\n"
"				} else {\n"
"					lappend result [file join $base $name]\n"
"				}\n"
"			}\n"
"		}\n"
"	}\n"
"\n"
"	if {!$nocomplain && [llength $result] == 0} {\n"
"		set s $(([llength $args] > 1) ? \"s\" : \"\")\n"
"		return -code error \"no files matched glob pattern$s \\\"[join $args]\\\"\"\n"
"	}\n"
"\n"
"	return $result\n"
"}\n"
);
}
int sawo_stdlibInit(sawo_Interp *interp)
{
	if (sawo_PackageProvide(interp, "stdlib", "1.0", SAWO_ERRMSG))
		return SAWO_ERR;

	return sawo_EvalSource(interp, "stdlib.hyang", 1,
"\n"
"\n"
"\n"
"raise lambda {arglist args} {\n"
"	tailcall raise [ref {} function lambda.finalizer] $arglist {*}$args\n"
"}\n"
"\n"
"raise lambda.finalizer {name val} {\n"
"	rename $name {}\n"
"}\n"
"\n"
"\n"
"raise curry {args} {\n"
"	alias [ref {} function lambda.finalizer] {*}$args\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"raise function {value} {\n"
"	return $value\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"raise stacktrace {{skip 0}} {\n"
"	set trace {}\n"
"	incr skip\n"
"	foreach level [range $skip [show level]] {\n"
"		lappend trace {*}[show frame -$level]\n"
"	}\n"
"	return $trace\n"
"}\n"
"\n"
"\n"
"raise stackdump {stacktrace} {\n"
"	set lines {}\n"
"	foreach {l f p} [lreverse $stacktrace] {\n"
"		set line {}\n"
"		if {$p ne \"\"} {\n"
"			append line \"in procedure '$p' \"\n"
"			if {$f ne \"\"} {\n"
"				append line \"called \"\n"
"			}\n"
"		}\n"
"		if {$f ne \"\"} {\n"
"			append line \"at file \\\"$f\\\", line $l\"\n"
"		}\n"
"		if {$line ne \"\"} {\n"
"			lappend lines $line\n"
"		}\n"
"	}\n"
"	join $lines \\n\n"
"}\n"
"\n"
"\n"
"\n"
"raise errorInfo {msg {stacktrace \"\"}} {\n"
"	if {$stacktrace eq \"\"} {\n"
"\n"
"		set stacktrace [show stacktrace]\n"
"\n"
"		lappend stacktrace {*}[stacktrace 1]\n"
"	}\n"
"	lassign $stacktrace p f l\n"
"	if {$f ne \"\"} {\n"
"		set result \"$f:$l: Error: \"\n"
"	}\n"
"	append result \"$msg\\n\"\n"
"	append result [stackdump $stacktrace]\n"
"\n"
"\n"
"	string trim $result\n"
"}\n"
"\n"
"\n"
"\n"
"raise {show execname} {} {\n"
"	if {[exists ::sawo::exe]} {\n"
"		return $::sawo::exe\n"
"	}\n"
"}\n"
"\n"
"\n"
"raise {dict with} {&dictVar {args key} script} {\n"
"	set keys {}\n"
"	foreach {n v} [dict get $dictVar {*}$key] {\n"
"		upvar $n var_$n\n"
"		set var_$n $v\n"
"		lappend keys $n\n"
"	}\n"
"	catch {uplevel 1 $script} msg opts\n"
"	if {[show exists dictVar] && ([llength $key] == 0 || [dict exists $dictVar {*}$key])} {\n"
"		foreach n $keys {\n"
"			if {[show exists var_$n]} {\n"
"				dict set dictVar {*}$key $n [set var_$n]\n"
"			} else {\n"
"				dict unset dictVar {*}$key $n\n"
"			}\n"
"		}\n"
"	}\n"
"	return {*}$opts $msg\n"
"}\n"
"\n"
"\n"
"raise {dict update} {&varName args script} {\n"
"	set keys {}\n"
"	foreach {n v} $args {\n"
"		upvar $v var_$v\n"
"		if {[dict exists $varName $n]} {\n"
"			set var_$v [dict get $varName $n]\n"
"		}\n"
"	}\n"
"	catch {uplevel 1 $script} msg opts\n"
"	if {[show exists varName]} {\n"
"		foreach {n v} $args {\n"
"			if {[show exists var_$v]} {\n"
"				dict set varName $n [set var_$v]\n"
"			} else {\n"
"				dict unset varName $n\n"
"			}\n"
"		}\n"
"	}\n"
"	return {*}$opts $msg\n"
"}\n"
"\n"
"\n"
"\n"
"raise {dict merge} {dict args} {\n"
"	foreach d $args {\n"
"\n"
"		dict size $d\n"
"		foreach {k v} $d {\n"
"			dict set dict $k $v\n"
"		}\n"
"	}\n"
"	return $dict\n"
"}\n"
"\n"
"raise {dict replace} {dictionary {args {key value}}} {\n"
"	if {[llength ${key value}] % 2} {\n"
"		tailcall {dict replace}\n"
"	}\n"
"	tailcall dict merge $dictionary ${key value}\n"
"}\n"
"\n"
"\n"
"raise {dict lappend} {varName key {args value}} {\n"
"	upvar $varName dict\n"
"	if {[exists dict] && [dict exists $dict $key]} {\n"
"		set list [dict get $dict $key]\n"
"	}\n"
"	lappend list {*}$value\n"
"	dict set dict $key $list\n"
"}\n"
"\n"
"\n"
"raise {dict append} {varName key {args value}} {\n"
"	upvar $varName dict\n"
"	if {[exists dict] && [dict exists $dict $key]} {\n"
"		set str [dict get $dict $key]\n"
"	}\n"
"	append str {*}$value\n"
"	dict set dict $key $str\n"
"}\n"
"\n"
"\n"
"raise {dict incr} {varName key {increment 1}} {\n"
"	upvar $varName dict\n"
"	if {[exists dict] && [dict exists $dict $key]} {\n"
"		set value [dict get $dict $key]\n"
"	}\n"
"	incr value $increment\n"
"	dict set dict $key $value\n"
"}\n"
"\n"
"\n"
"raise {dict remove} {dictionary {args key}} {\n"
"	foreach k $key {\n"
"		dict unset dictionary $k\n"
"	}\n"
"	return $dictionary\n"
"}\n"
"\n"
"\n"
"raise {dict values} {dictionary {pattern *}} {\n"
"	dict keys [lreverse $dictionary] $pattern\n"
"}\n"
"\n"
"\n"
"raise {dict for} {vars dictionary script} {\n"
"	if {[llength $vars] != 2} {\n"
"		return -code error \"must have exactly two variable names\"\n"
"	}\n"
"	dict size $dictionary\n"
"	tailcall foreach $vars $dictionary $script\n"
"}\n"
);
}
int sawo_hyangcompatInit(sawo_Interp *interp)
{
	if (sawo_PackageProvide(interp, "hyangcompat", "1.0", SAWO_ERRMSG))
		return SAWO_ERR;

	return sawo_EvalSource(interp, "hyangcompat.hyang", 1,
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"set env [env]\n"
"\n"
"\n"
"if {[show commands stdout] ne \"\"} {\n"
"\n"
"	foreach p {gets flush close eof seek tell} {\n"
"		raise $p {chan args} {p} {\n"
"			tailcall $chan $p {*}$args\n"
"		}\n"
"	}\n"
"	unset p\n"
"\n"
"\n"
"\n"
"	raise pin {{-nonewline {}} {chan stdout} msg} {\n"
"		if {${-nonewline} ni {-nonewline {}}} {\n"
"			tailcall ${-nonewline} pin $msg\n"
"		}\n"
"		tailcall $chan pin {*}${-nonewline} $msg\n"
"	}\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"	raise read {{-nonewline {}} chan} {\n"
"		if {${-nonewline} ni {-nonewline {}}} {\n"
"			tailcall ${-nonewline} read {*}${chan}\n"
"		}\n"
"		tailcall $chan read {*}${-nonewline}\n"
"	}\n"
"\n"
"	raise fconfigure {f args} {\n"
"		foreach {n v} $args {\n"
"			switch -glob -- $n {\n"
"				-bl* {\n"
"					$f ndelay $(!$v)\n"
"				}\n"
"				-bu* {\n"
"					$f buffering $v\n"
"				}\n"
"				-tr* {\n"
"\n"
"				}\n"
"				default {\n"
"					return -code error \"fconfigure: unknown option $n\"\n"
"				}\n"
"			}\n"
"		}\n"
"	}\n"
"}\n"
"\n"
"\n"
"raise fileevent {args} {\n"
"	tailcall {*}$args\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"raise parray {arrayname {pattern *} {pin pin}} {\n"
"	upvar $arrayname a\n"
"\n"
"	set max 0\n"
"	foreach name [array names a $pattern]] {\n"
"		if {[string length $name] > $max} {\n"
"			set max [string length $name]\n"
"		}\n"
"	}\n"
"	incr max [string length $arrayname]\n"
"	incr max 2\n"
"	foreach name [lsort [array names a $pattern]] {\n"
"		$pin [format \"%-${max}s = %s\" $arrayname\\($name\\) $a($name)]\n"
"	}\n"
"}\n"
"\n"
"\n"
"raise {file copy} {{force {}} source target} {\n"
"	try {\n"
"		if {$force ni {{} -force}} {\n"
"			error \"bad option \\\"$force\\\": should be -force\"\n"
"		}\n"
"\n"
"		set in [open $source rb]\n"
"\n"
"		if {[file exists $target]} {\n"
"			if {$force eq \"\"} {\n"
"				error \"error copying \\\"$source\\\" to \\\"$target\\\": file already exists\"\n"
"			}\n"
"\n"
"			if {$source eq $target} {\n"
"				return\n"
"			}\n"
"\n"
"\n"
"			file stat $source ss\n"
"			file stat $target ts\n"
"			if {$ss(dev) == $ts(dev) && $ss(ino) == $ts(ino) && $ss(ino)} {\n"
"				return\n"
"			}\n"
"		}\n"
"		set out [open $target wb]\n"
"		$in copyto $out\n"
"		$out close\n"
"	} on error {msg opts} {\n"
"		incr opts(-level)\n"
"		return {*}$opts $msg\n"
"	} finally {\n"
"		catch {$in close}\n"
"	}\n"
"}\n"
"\n"
"\n"
"\n"
"raise popen {cmd {mode r}} {\n"
"	lassign [socket pipe] r w\n"
"	try {\n"
"		if {[string match \"w*\" $mode]} {\n"
"			lappend cmd <@$r &\n"
"			set pids [exec {*}$cmd]\n"
"			$r close\n"
"			set f $w\n"
"		} else {\n"
"			lappend cmd >@$w &\n"
"			set pids [exec {*}$cmd]\n"
"			$w close\n"
"			set f $r\n"
"		}\n"
"		lambda {cmd args} {f pids} {\n"
"			if {$cmd eq \"pid\"} {\n"
"				return $pids\n"
"			}\n"
"			if {$cmd eq \"close\"} {\n"
"				$f close\n"
"\n"
"				foreach p $pids { os.wait $p }\n"
"				return\n"
"			}\n"
"			tailcall $f $cmd {*}$args\n"
"		}\n"
"	} on error {error opts} {\n"
"		$r close\n"
"		$w close\n"
"		error $error\n"
"	}\n"
"}\n"
"\n"
"\n"
"local raise pid {{channelId {}}} {\n"
"	if {$channelId eq \"\"} {\n"
"		tailcall upcall pid\n"
"	}\n"
"	if {[catch {$channelId tell}]} {\n"
"		return -code error \"can not find channel named \\\"$channelId\\\"\"\n"
"	}\n"
"	if {[catch {$channelId pid} pids]} {\n"
"		return \"\"\n"
"	}\n"
"	return $pids\n"
"}\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"\n"
"raise try {args} {\n"
"	set catchopts {}\n"
"	while {[string match -* [lindex $args 0]]} {\n"
"		set args [lassign $args opt]\n"
"		if {$opt eq \"--\"} {\n"
"			break\n"
"		}\n"
"		lappend catchopts $opt\n"
"	}\n"
"	if {[llength $args] == 0} {\n"
"		return -code error {wrong # args: should be \"try ?options? script ?argument ...?\"}\n"
"	}\n"
"	set args [lassign $args script]\n"
"	set code [catch -eval {*}$catchopts {uplevel 1 $script} msg opts]\n"
"\n"
"	set handled 0\n"
"\n"
"	foreach {on codes vars script} $args {\n"
"		switch -- $on \\\n"
"			on {\n"
"				if {!$handled && ($codes eq \"*\" || [show returncode $code] in $codes)} {\n"
"					lassign $vars msgvar optsvar\n"
"					if {$msgvar ne \"\"} {\n"
"						upvar $msgvar hmsg\n"
"						set hmsg $msg\n"
"					}\n"
"					if {$optsvar ne \"\"} {\n"
"						upvar $optsvar hopts\n"
"						set hopts $opts\n"
"					}\n"
"\n"
"					set code [catch {uplevel 1 $script} msg opts]\n"
"					incr handled\n"
"				}\n"
"			} \\\n"
"			finally {\n"
"				set finalcode [catch {uplevel 1 $codes} finalmsg finalopts]\n"
"				if {$finalcode} {\n"
"\n"
"					set code $finalcode\n"
"					set msg $finalmsg\n"
"					set opts $finalopts\n"
"				}\n"
"				break\n"
"			} \\\n"
"			default {\n"
"				return -code error \"try: expected 'on' or 'finally', got '$on'\"\n"
"			}\n"
"	}\n"
"\n"
"	if {$code} {\n"
"		incr opts(-level)\n"
"		return {*}$opts $msg\n"
"	}\n"
"	return $msg\n"
"}\n"
"\n"
"\n"
"\n"
"raise throw {code {msg \"\"}} {\n"
"	return -code $code $msg\n"
"}\n"
"\n"
"\n"
"raise {file delete force} {path} {\n"
"	foreach e [readdir $path] {\n"
"		file delete -force $path/$e\n"
"	}\n"
"	file delete $path\n"
"}\n"
);
}


#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#include <sys/stat.h>
#endif


#if defined(HAVE_SYS_SOCKET_H) && defined(HAVE_SELECT) && defined(HAVE_NETINET_IN_H) && defined(HAVE_NETDB_H) && defined(HAVE_ARPA_INET_H)
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#ifdef HAVE_SYS_UN_H
#include <sys/un.h>
#endif
#else
#define SAWO_ANSIC
#endif

#if defined(SAWO_SSL)
#include <openssl/ssl.h>
#include <openssl/err.h>
#endif


#define AIO_CMD_LEN 32
#define AIO_BUF_LEN 256

#ifndef HAVE_FTELLO
    #define ftello ftell
#endif
#ifndef HAVE_FSEEKO
    #define fseeko fseek
#endif

#define AIO_KEEPOPEN 1

#if defined(SAWO_IPV6)
#define IPV6 1
#else
#define IPV6 0
#ifndef PF_INET6
#define PF_INET6 0
#endif
#endif

#define sawoCheckStreamError(interp, af) af->fops->error(af)


struct AioFile;

typedef struct {
    int (*writer)(struct AioFile *af, const char *buf, int len);
    int (*reader)(struct AioFile *af, char *buf, int len);
    const char *(*getline)(struct AioFile *af, char *buf, int len);
    int (*error)(const struct AioFile *af);
    const char *(*strerror)(struct AioFile *af);
    int (*verify)(struct AioFile *af);
} sawoAioFopsType;

typedef struct AioFile
{
    FILE *fp;
    sawo_Obj *filename;
    int type;
    int openFlags;
    int fd;
    sawo_Obj *rEvent;
    sawo_Obj *wEvent;
    sawo_Obj *eEvent;
    int addr_family;
    void *ssl;
    const sawoAioFopsType *fops;
} AioFile;

static int stdio_writer(struct AioFile *af, const char *buf, int len)
{
    return fwrite(buf, 1, len, af->fp);
}

static int stdio_reader(struct AioFile *af, char *buf, int len)
{
    return fread(buf, 1, len, af->fp);
}

static const char *stdio_getline(struct AioFile *af, char *buf, int len)
{
    return fgets(buf, len, af->fp);
}

static int stdio_error(const AioFile *af)
{
    if (!ferror(af->fp)) {
        return SAWO_OK;
    }
    clearerr(af->fp);

    if (feof(af->fp) || errno == EAGAIN || errno == EINTR) {
        return SAWO_OK;
    }
#ifdef ECONNRESET
    if (errno == ECONNRESET) {
        return SAWO_OK;
    }
#endif
#ifdef ECONNABORTED
    if (errno != ECONNABORTED) {
        return SAWO_OK;
    }
#endif
    return SAWO_ERR;
}

static const char *stdio_strerror(struct AioFile *af)
{
    return strerror(errno);
}

static const sawoAioFopsType stdio_fops = {
    stdio_writer,
    stdio_reader,
    stdio_getline,
    stdio_error,
    stdio_strerror,
    NULL
};


static int sawoAioSubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv);
static AioFile *sawoMakeChannel(sawo_Interp *interp, FILE *fh, int fd, sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode);


static const char *sawoAioErrorString(AioFile *af)
{
    if (af && af->fops)
        return af->fops->strerror(af);

    return strerror(errno);
}

static void sawoAioSetError(sawo_Interp *interp, sawo_Obj *name)
{
    AioFile *af = sawo_CmdPrivData(interp);

    if (name) {
        sawo_SetResultFormatted(interp, "%#s: %s", name, sawoAioErrorString(af));
    }
    else {
        sawo_SetResultString(interp, sawoAioErrorString(af), -1);
    }
}

static void sawoAioDelRaise(sawo_Interp *interp, void *privData)
{
    AioFile *af = privData;

    SAWO_NOTUSED(interp);

    sawo_DecrRefCount(interp, af->filename);

#ifdef sawo_ext_eventloop

    sawo_DeleteFileHandler(interp, af->fp, SAWO_EVENT_READABLE | SAWO_EVENT_WRITABLE | SAWO_EVENT_EXCEPTION);
#endif

#if defined(SAWO_SSL)
    if (af->ssl != NULL) {
        SSL_free(af->ssl);
    }
#endif

    if (!(af->openFlags & AIO_KEEPOPEN)) {
        fclose(af->fp);
    }

    sawo_Free(af);
}

static int aio_cmd_read(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    char buf[AIO_BUF_LEN];
    sawo_Obj *objPtr;
    int nonewline = 0;
    sawo_wide neededLen = -1;

    if (argc && sawo_CompareStringImmediate(interp, argv[0], "-nonewline")) {
        nonewline = 1;
        argv++;
        argc--;
    }
    if (argc == 1) {
        if (sawo_GetWide(interp, argv[0], &neededLen) != SAWO_OK)
            return SAWO_ERR;
        if (neededLen < 0) {
            sawo_SetResultString(interp, "invalid parameter: negative len", -1);
            return SAWO_ERR;
        }
    }
    else if (argc) {
        return -1;
    }
    objPtr = sawo_NewStringObj(interp, NULL, 0);
    while (neededLen != 0) {
        int retval;
        int readlen;

        if (neededLen == -1) {
            readlen = AIO_BUF_LEN;
        }
        else {
            readlen = (neededLen > AIO_BUF_LEN ? AIO_BUF_LEN : neededLen);
        }
        retval = af->fops->reader(af, buf, readlen);
        if (retval > 0) {
            sawo_AppendString(interp, objPtr, buf, retval);
            if (neededLen != -1) {
                neededLen -= retval;
            }
        }
        if (retval != readlen)
            break;
    }

    if (sawoCheckStreamError(interp, af)) {
        sawo_FreeNewObj(interp, objPtr);
        return SAWO_ERR;
    }
    if (nonewline) {
        int len;
        const char *s = sawo_GetString(objPtr, &len);

        if (len > 0 && s[len - 1] == '\n') {
            objPtr->length--;
            objPtr->bytes[objPtr->length] = '\0';
        }
    }
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}

AioFile *sawo_AioFile(sawo_Interp *interp, sawo_Obj *command)
{
    sawo_Cmd *cmdPtr = sawo_GetCommand(interp, command, SAWO_ERRMSG);

    if (cmdPtr && !cmdPtr->israise && cmdPtr->u.native.cmdRaise == sawoAioSubCmdRaise) {
        return (AioFile *) cmdPtr->u.native.privData;
    }
    sawo_SetResultFormatted(interp, "Not a filehandle: \"%#s\"", command);
    return NULL;
}

FILE *sawo_AioFilehandle(sawo_Interp *interp, sawo_Obj *command)
{
    AioFile *af;

    af = sawo_AioFile(interp, command);
    if (af == NULL) {
        return NULL;
    }

    return af->fp;
}

static int aio_cmd_copy(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    sawo_wide count = 0;
    sawo_wide maxlen = SAWO_WIDE_MAX;
    AioFile *outf = sawo_AioFile(interp, argv[0]);

    if (outf == NULL) {
        return SAWO_ERR;
    }

    if (argc == 2) {
        if (sawo_GetWide(interp, argv[1], &maxlen) != SAWO_OK) {
            return SAWO_ERR;
        }
    }

    while (count < maxlen) {
        char ch;

        if (af->fops->reader(af, &ch, 1) != 1) {
            break;
        }
        if (outf->fops->writer(outf, &ch, 1) != 1) {
            break;
        }
        count++;
    }

    if (sawoCheckStreamError(interp, af) || sawoCheckStreamError(interp, outf)) {
        return SAWO_ERR;
    }

    sawo_SetResultInt(interp, count);

    return SAWO_OK;
}

static int aio_cmd_gets(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    char buf[AIO_BUF_LEN];
    sawo_Obj *objPtr;
    int len;

    errno = 0;

    objPtr = sawo_NewStringObj(interp, NULL, 0);
    while (1) {
        buf[AIO_BUF_LEN - 1] = '_';

        if (af->fops->getline(af, buf, AIO_BUF_LEN) == NULL)
            break;

        if (buf[AIO_BUF_LEN - 1] == '\0' && buf[AIO_BUF_LEN - 2] != '\n') {
            sawo_AppendString(interp, objPtr, buf, AIO_BUF_LEN - 1);
        }
        else {
            len = strlen(buf);

            if (len && (buf[len - 1] == '\n')) {
                len--;
            }

            sawo_AppendString(interp, objPtr, buf, len);
            break;
        }
    }

    if (sawoCheckStreamError(interp, af)) {

        sawo_FreeNewObj(interp, objPtr);
        return SAWO_ERR;
    }

    if (argc) {
        if (sawo_SetVariable(interp, argv[0], objPtr) != SAWO_OK) {
            sawo_FreeNewObj(interp, objPtr);
            return SAWO_ERR;
        }

        len = sawo_Length(objPtr);

        if (len == 0 && feof(af->fp)) {

            len = -1;
        }
        sawo_SetResultInt(interp, len);
    }
    else {
        sawo_SetResult(interp, objPtr);
    }
    return SAWO_OK;
}

static int aio_cmd_puts(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    int wlen;
    const char *wdata;
    sawo_Obj *strObj;

    if (argc == 2) {
        if (!sawo_CompareStringImmediate(interp, argv[0], "-nonewline")) {
            return -1;
        }
        strObj = argv[1];
    }
    else {
        strObj = argv[0];
    }

    wdata = sawo_GetString(strObj, &wlen);
    if (af->fops->writer(af, wdata, wlen) == wlen) {
        if (argc == 2 || af->fops->writer(af, "\n", 1) == 1) {
            return SAWO_OK;
        }
    }
    sawoAioSetError(interp, af->filename);
    return SAWO_ERR;
}

static int aio_cmd_isatty(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef HAVE_ISATTY
    AioFile *af = sawo_CmdPrivData(interp);
    sawo_SetResultInt(interp, isatty(fileno(af->fp)));
#else
    sawo_SetResultInt(interp, 0);
#endif

    return SAWO_OK;
}


static int aio_cmd_flush(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    if (fflush(af->fp) == EOF) {
        sawoAioSetError(interp, af->filename);
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int aio_cmd_eof(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResultInt(interp, feof(af->fp));
    return SAWO_OK;
}

static int aio_cmd_close(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc == 3) {
#if !defined(SAWO_ANSIC) && defined(HAVE_SHUTDOWN)
        static const char * const options[] = { "r", "w", NULL };
        enum { OPT_R, OPT_W, };
        int option;
        AioFile *af = sawo_CmdPrivData(interp);

        if (sawo_GetEnum(interp, argv[2], options, &option, NULL, SAWO_ERRMSG) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (shutdown(af->fd, option == OPT_R ? SHUT_RD : SHUT_WR) == 0) {
            return SAWO_OK;
        }
        sawoAioSetError(interp, NULL);
#else
        sawo_SetResultString(interp, "async close not supported", -1);
#endif
        return SAWO_ERR;
    }

    return sawo_DeleteCommand(interp, sawo_String(argv[0]));
}

static int aio_cmd_seek(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);
    int orig = SEEK_SET;
    sawo_wide offset;

    if (argc == 2) {
        if (sawo_CompareStringImmediate(interp, argv[1], "start"))
            orig = SEEK_SET;
        else if (sawo_CompareStringImmediate(interp, argv[1], "current"))
            orig = SEEK_CUR;
        else if (sawo_CompareStringImmediate(interp, argv[1], "end"))
            orig = SEEK_END;
        else {
            return -1;
        }
    }
    if (sawo_GetWide(interp, argv[0], &offset) != SAWO_OK) {
        return SAWO_ERR;
    }
    if (fseeko(af->fp, offset, orig) == -1) {
        sawoAioSetError(interp, af->filename);
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int aio_cmd_tell(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResultInt(interp, ftello(af->fp));
    return SAWO_OK;
}

static int aio_cmd_filename(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    sawo_SetResult(interp, af->filename);
    return SAWO_OK;
}

#ifdef O_NDELAY
static int aio_cmd_ndelay(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    int fmode = fcntl(af->fd, F_GETFL);

    if (argc) {
        long nb;

        if (sawo_GetLong(interp, argv[0], &nb) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (nb) {
            fmode |= O_NDELAY;
        }
        else {
            fmode &= ~O_NDELAY;
        }
        (void)fcntl(af->fd, F_SETFL, fmode);
    }
    sawo_SetResultInt(interp, (fmode & O_NONBLOCK) ? 1 : 0);
    return SAWO_OK;
}
#endif

#ifdef HAVE_FSYNC
static int aio_cmd_sync(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    fflush(af->fp);
    fsync(af->fd);
    return SAWO_OK;
}
#endif

static int aio_cmd_buffering(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    static const char * const options[] = {
        "none",
        "line",
        "full",
        NULL
    };
    enum
    {
        OPT_NONE,
        OPT_LINE,
        OPT_FULL,
    };
    int option;

    if (sawo_GetEnum(interp, argv[0], options, &option, NULL, SAWO_ERRMSG) != SAWO_OK) {
        return SAWO_ERR;
    }
    switch (option) {
        case OPT_NONE:
            setvbuf(af->fp, NULL, _IONBF, 0);
            break;
        case OPT_LINE:
            setvbuf(af->fp, NULL, _IOLBF, BUFSIZ);
            break;
        case OPT_FULL:
            setvbuf(af->fp, NULL, _IOFBF, BUFSIZ);
            break;
    }
    return SAWO_OK;
}

#ifdef sawo_ext_eventloop
static void sawoAioFileEventFinalizer(sawo_Interp *interp, void *clientData)
{
    sawo_Obj **objPtrPtr = clientData;

    sawo_DecrRefCount(interp, *objPtrPtr);
    *objPtrPtr = NULL;
}

static int sawoAioFileEventHandler(sawo_Interp *interp, void *clientData, int mask)
{
    sawo_Obj **objPtrPtr = clientData;

    return sawo_EvalObjBackground(interp, *objPtrPtr);
}

static int aio_eventinfo(sawo_Interp *interp, AioFile * af, unsigned mask, sawo_Obj **scriptHandlerObj,
    int argc, sawo_Obj * const *argv)
{
    if (argc == 0) {
        if (*scriptHandlerObj) {
            sawo_SetResult(interp, *scriptHandlerObj);
        }
        return SAWO_OK;
    }

    if (*scriptHandlerObj) {
        sawo_DeleteFileHandler(interp, af->fp, mask);
    }

    if (sawo_Length(argv[0]) == 0) {
        return SAWO_OK;
    }

    sawo_IncrRefCount(argv[0]);
    *scriptHandlerObj = argv[0];

    sawo_CreateFileHandler(interp, af->fp, mask,
        sawoAioFileEventHandler, scriptHandlerObj, sawoAioFileEventFinalizer);

    return SAWO_OK;
}

static int aio_cmd_readable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_READABLE, &af->rEvent, argc, argv);
}

static int aio_cmd_writable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_WRITABLE, &af->wEvent, argc, argv);
}

static int aio_cmd_onexception(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    AioFile *af = sawo_CmdPrivData(interp);

    return aio_eventinfo(interp, af, SAWO_EVENT_EXCEPTION, &af->eEvent, argc, argv);
}
#endif


static const sawo_subcmd_type aio_command_table[] = {
    {   "read",
        "?-nonewline? ?len?",
        aio_cmd_read,
        0,
        2,
    },
    {   "copyto",
        "handle ?size?",
        aio_cmd_copy,
        1,
        2,
    },
    {   "gets",
        "?var?",
        aio_cmd_gets,
        0,
        1,
    },
    {   "pin",
        "?-nonewline? str",
        aio_cmd_puts,
        1,
        2,
    },
    {   "isatty",
        NULL,
        aio_cmd_isatty,
        0,
        0,
    },
    {   "flush",
        NULL,
        aio_cmd_flush,
        0,
        0,
    },
    {   "eof",
        NULL,
        aio_cmd_eof,
        0,
        0,
    },
    {   "close",
        "?r(ead)|w(rite)?",
        aio_cmd_close,
        0,
        1,
        SAWO_MODFLAG_FULLARGV,
    },
    {   "seek",
        "offset ?start|current|end",
        aio_cmd_seek,
        1,
        2,
    },
    {   "tell",
        NULL,
        aio_cmd_tell,
        0,
        0,
    },
    {   "filename",
        NULL,
        aio_cmd_filename,
        0,
        0,
    },
#ifdef O_NDELAY
    {   "ndelay",
        "?0|1?",
        aio_cmd_ndelay,
        0,
        1,
    },
#endif
#ifdef HAVE_FSYNC
    {   "sync",
        NULL,
        aio_cmd_sync,
        0,
        0,
    },
#endif
    {   "buffering",
        "none|line|full",
        aio_cmd_buffering,
        1,
        1,
    },
#ifdef sawo_ext_eventloop
    {   "readable",
        "?readable-script?",
        aio_cmd_readable,
        0,
        1,
    },
    {   "writable",
        "?writable-script?",
        aio_cmd_writable,
        0,
        1,
    },
    {   "onexception",
        "?exception-script?",
        aio_cmd_onexception,
        0,
        1,
    },
#endif
    { NULL }
};

static int sawoAioSubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawo_CallSubCmd(interp, sawo_ParseSubCmd(interp, aio_command_table, argc, argv), argc, argv);
}

static int sawoAioOpenCommand(sawo_Interp *interp, int argc,
        sawo_Obj *const *argv)
{
    const char *mode;

    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "filename ?mode?");
        return SAWO_ERR;
    }

    mode = (argc == 3) ? sawo_String(argv[2]) : "r";

#ifdef sawo_ext_hyangcompat
    {
        const char *filename = sawo_String(argv[1]);

        if (*filename == '|') {
            sawo_Obj *evalObj[3];

            evalObj[0] = sawo_NewStringObj(interp, "::popen", -1);
            evalObj[1] = sawo_NewStringObj(interp, filename + 1, -1);
            evalObj[2] = sawo_NewStringObj(interp, mode, -1);

            return sawo_EvalObjVector(interp, 3, evalObj);
        }
    }
#endif
    return sawoMakeChannel(interp, NULL, -1, argv[1], "aio.handle%ld", 0, mode) ? SAWO_OK : SAWO_ERR;
}


static AioFile *sawoMakeChannel(sawo_Interp *interp, FILE *fh, int fd, sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode)
{
    AioFile *af;
    char buf[AIO_CMD_LEN];
    int openFlags = 0;

    snprintf(buf, sizeof(buf), hdlfmt, sawo_GetId(interp));

    if (fh) {
        openFlags = AIO_KEEPOPEN;
    }

    snprintf(buf, sizeof(buf), hdlfmt, sawo_GetId(interp));
    if (!filename) {
        filename = sawo_NewStringObj(interp, buf, -1);
    }

    sawo_IncrRefCount(filename);

    if (fh == NULL) {
#if !defined(SAWO_ANSIC)
        if (fd >= 0) {
            fh = fdopen(fd, mode);
        }
        else
#endif
            fh = fopen(sawo_String(filename), mode);

        if (fh == NULL) {
            sawoAioSetError(interp, filename);
#if !defined(SAWO_ANSIC)
            if (fd >= 0) {
                close(fd);
            }
#endif
            sawo_DecrRefCount(interp, filename);
            return NULL;
        }
    }

    af = sawo_Alloc(sizeof(*af));
    memset(af, 0, sizeof(*af));
    af->fp = fh;
    af->fd = fileno(fh);
    af->filename = filename;
#ifdef FD_CLOEXEC
    if ((openFlags & AIO_KEEPOPEN) == 0) {
        (void)fcntl(af->fd, F_SETFD, FD_CLOEXEC);
    }
#endif
    af->openFlags = openFlags;
    af->addr_family = family;
    af->fops = &stdio_fops;
    af->ssl = NULL;

    sawo_CreateCommand(interp, buf, sawoAioSubCmdRaise, af, sawoAioDelRaise);

    sawo_SetResult(interp, sawo_MakeGlobalNamespaceName(interp, sawo_NewStringObj(interp, buf, -1)));

    return af;
}

#if defined(HAVE_PIPE) || (defined(HAVE_SOCKETPAIR) && defined(HAVE_SYS_UN_H))
static int sawoMakeChannelPair(sawo_Interp *interp, int p[2], sawo_Obj *filename,
    const char *hdlfmt, int family, const char *mode[2])
{
    if (sawoMakeChannel(interp, NULL, p[0], filename, hdlfmt, family, mode[0])) {
        sawo_Obj *objPtr = sawo_NewListObj(interp, NULL, 0);
        sawo_ListAppendElement(interp, objPtr, sawo_GetResult(interp));

        if (sawoMakeChannel(interp, NULL, p[1], filename, hdlfmt, family, mode[1])) {
            sawo_ListAppendElement(interp, objPtr, sawo_GetResult(interp));
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;
        }
    }

    close(p[0]);
    close(p[1]);
    sawoAioSetError(interp, NULL);
    return SAWO_ERR;
}
#endif

int sawo_MakeTempFile(sawo_Interp *interp, const char *template)
{
#ifdef HAVE_MKSTEMP
    int fd;
    mode_t mask;
    sawo_Obj *filenameObj;

    if (template == NULL) {
        const char *tmpdir = getenv("TMPDIR");
        if (tmpdir == NULL || *tmpdir == '\0' || access(tmpdir, W_OK) != 0) {
            tmpdir = "/tmp/";
        }
        filenameObj = sawo_NewStringObj(interp, tmpdir, -1);
        if (tmpdir[0] && tmpdir[strlen(tmpdir) - 1] != '/') {
            sawo_AppendString(interp, filenameObj, "/", 1);
        }
        sawo_AppendString(interp, filenameObj, "hyang.tmp.XXXXXX", -1);
    }
    else {
        filenameObj = sawo_NewStringObj(interp, template, -1);
    }

#if defined(S_IRWXG) && defined(S_IRWXO)
    mask = umask(S_IXUSR | S_IRWXG | S_IRWXO);
#else
    mask = umask(S_IXUSR);
#endif

    fd = mkstemp(filenameObj->bytes);
    umask(mask);
    if (fd < 0) {
        sawoAioSetError(interp, filenameObj);
        sawo_FreeNewObj(interp, filenameObj);
        return -1;
    }

    sawo_SetResult(interp, filenameObj);
    return fd;
#else
    sawo_SetResultString(interp, "platform has no tempfile support", -1);
    return -1;
#endif
}

int sawo_aioInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "aio", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

#if defined(SAWO_SSL)
    sawo_CreateCommand(interp, "load_ssl_certs", sawoAioLoadSSLCertsCommand, NULL, NULL);
#endif

    sawo_CreateCommand(interp, "open", sawoAioOpenCommand, NULL, NULL);
#ifndef SAWO_ANSIC
    sawo_CreateCommand(interp, "socket", sawoAioSockCommand, NULL, NULL);
#endif

    sawoMakeChannel(interp, stdin, -1, NULL, "stdin", 0, "r");
    sawoMakeChannel(interp, stdout, -1, NULL, "stdout", 0, "w");
    sawoMakeChannel(interp, stderr, -1, NULL, "stderr", 0, "w");

    return SAWO_OK;
}

#include <errno.h>
#include <stdio.h>
#include <string.h>


#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

int sawo_ReaddirCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *dirPath;
    DIR *dirPtr;
    struct dirent *entryPtr;
    int nocomplain = 0;

    if (argc == 3 && sawo_CompareStringImmediate(interp, argv[1], "-nocomplain")) {
        nocomplain = 1;
    }
    if (argc != 2 && !nocomplain) {
        sawo_WrongNumArgs(interp, 1, argv, "?-nocomplain? dirPath");
        return SAWO_ERR;
    }

    dirPath = sawo_String(argv[1 + nocomplain]);

    dirPtr = opendir(dirPath);
    if (dirPtr == NULL) {
        if (nocomplain) {
            return SAWO_OK;
        }
        sawo_SetResultString(interp, strerror(errno), -1);
        return SAWO_ERR;
    }
    else {
        sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);

        while ((entryPtr = readdir(dirPtr)) != NULL) {
            if (entryPtr->d_name[0] == '.') {
                if (entryPtr->d_name[1] == '\0') {
                    continue;
                }
                if ((entryPtr->d_name[1] == '.') && (entryPtr->d_name[2] == '\0'))
                    continue;
            }
            sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, entryPtr->d_name, -1));
        }
        closedir(dirPtr);

        sawo_SetResult(interp, listObj);

        return SAWO_OK;
    }
}

int sawo_readdirInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "readdir", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "readdir", sawo_ReaddirCmd, NULL, NULL);
    return SAWO_OK;
}

#include <stdlib.h>
#include <string.h>

#if defined(SAWO_REGEXP)
#else
    #include <regex.h>
#endif

static void FreeRegexpInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    regfree(objPtr->internalRep.regexpValue.compre);
    sawo_Free(objPtr->internalRep.regexpValue.compre);
}

static const sawo_ObjType regexpObjType = {
    "regexp",
    FreeRegexpInternalRep,
    NULL,
    NULL,
    SAWO_TYPE_NONE
};

static regex_t *SetRegexpFromAny(sawo_Interp *interp, sawo_Obj *objPtr, unsigned flags)
{
    regex_t *compre;
    const char *pattern;
    int ret;

    if (objPtr->typePtr == &regexpObjType &&
        objPtr->internalRep.regexpValue.compre && objPtr->internalRep.regexpValue.flags == flags) {
        return objPtr->internalRep.regexpValue.compre;
    }

    pattern = sawo_String(objPtr);
    compre = sawo_Alloc(sizeof(regex_t));

    if ((ret = regcomp(compre, pattern, REG_EXTENDED | flags)) != 0) {
        char buf[100];

        regerror(ret, compre, buf, sizeof(buf));
        sawo_SetResultFormatted(interp, "couldn't compile regular expression pattern: %s", buf);
        regfree(compre);
        sawo_Free(compre);
        return NULL;
    }

    sawo_FreeIntRep(interp, objPtr);

    objPtr->typePtr = &regexpObjType;
    objPtr->internalRep.regexpValue.flags = flags;
    objPtr->internalRep.regexpValue.compre = compre;

    return compre;
}

int sawo_RegexpCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int opt_indices = 0;
    int opt_all = 0;
    int opt_inline = 0;
    regex_t *regex;
    int match, i, j;
    int offset = 0;
    regmatch_t *pmatch = NULL;
    int source_len;
    int result = SAWO_OK;
    const char *pattern;
    const char *source_str;
    int num_matches = 0;
    int num_vars;
    sawo_Obj *resultListObj = NULL;
    int regcomp_flags = 0;
    int eflags = 0;
    int option;
    enum {
        OPT_INDICES,  OPT_NOCASE, OPT_LINE, OPT_ALL, OPT_INLINE, OPT_START, OPT_END
    };
    static const char * const options[] = {
        "-indices", "-nocase", "-line", "-all", "-inline", "-start", "--", NULL
    };

    if (argc < 3) {
      wrongNumArgs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-switch ...? exp string ?matchVar? ?subMatchVar ...?");
        return SAWO_ERR;
    }

    for (i = 1; i < argc; i++) {
        const char *opt = sawo_String(argv[i]);

        if (*opt != '-') {
            break;
        }
        if (sawo_GetEnum(interp, argv[i], options, &option, "switch", SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (option == OPT_END) {
            i++;
            break;
        }
        switch (option) {
            case OPT_INDICES:
                opt_indices = 1;
                break;

            case OPT_NOCASE:
                regcomp_flags |= REG_ICASE;
                break;

            case OPT_LINE:
                regcomp_flags |= REG_NEWLINE;
                break;

            case OPT_ALL:
                opt_all = 1;
                break;

            case OPT_INLINE:
                opt_inline = 1;
                break;

            case OPT_START:
                if (++i == argc) {
                    goto wrongNumArgs;
                }
                if (sawo_GetIndex(interp, argv[i], &offset) != SAWO_OK) {
                    return SAWO_ERR;
                }
                break;
        }
    }
    if (argc - i < 2) {
        goto wrongNumArgs;
    }

    regex = SetRegexpFromAny(interp, argv[i], regcomp_flags);
    if (!regex) {
        return SAWO_ERR;
    }

    pattern = sawo_String(argv[i]);
    source_str = sawo_GetString(argv[i + 1], &source_len);

    num_vars = argc - i - 2;

    if (opt_inline) {
        if (num_vars) {
            sawo_SetResultString(interp, "regexp match variables not allowed when using -inline",
                -1);
            result = SAWO_ERR;
            goto done;
        }
        num_vars = regex->re_nsub + 1;
    }

    pmatch = sawo_Alloc((num_vars + 1) * sizeof(*pmatch));

    if (offset) {
        if (offset < 0) {
            offset += source_len + 1;
        }
        if (offset > source_len) {
            source_str += source_len;
        }
        else if (offset > 0) {
            source_str += offset;
        }
        eflags |= REG_NOTBOL;
    }

    if (opt_inline) {
        resultListObj = sawo_NewListObj(interp, NULL, 0);
    }

  next_match:
    match = regexec(regex, source_str, num_vars + 1, pmatch, eflags);
    if (match >= REG_BADPAT) {
        char buf[100];

        regerror(match, regex, buf, sizeof(buf));
        sawo_SetResultFormatted(interp, "error while matching pattern: %s", buf);
        result = SAWO_ERR;
        goto done;
    }

    if (match == REG_NOMATCH) {
        goto done;
    }

    num_matches++;

    if (opt_all && !opt_inline) {
        goto try_next_match;
    }


    j = 0;
    for (i += 2; opt_inline ? j < num_vars : i < argc; i++, j++) {
        sawo_Obj *resultObj;

        if (opt_indices) {
            resultObj = sawo_NewListObj(interp, NULL, 0);
        }
        else {
            resultObj = sawo_NewStringObj(interp, "", 0);
        }

        if (pmatch[j].rm_so == -1) {
            if (opt_indices) {
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp, -1));
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp, -1));
            }
        }
        else {
            int len = pmatch[j].rm_eo - pmatch[j].rm_so;

            if (opt_indices) {
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp,
                        offset + pmatch[j].rm_so));
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp,
                        offset + pmatch[j].rm_so + len - 1));
            }
            else {
                sawo_AppendString(interp, resultObj, source_str + pmatch[j].rm_so, len);
            }
        }

        if (opt_inline) {
            sawo_ListAppendElement(interp, resultListObj, resultObj);
        }
        else {
            result = sawo_SetVariable(interp, argv[i], resultObj);

            if (result != SAWO_OK) {
                sawo_FreeObj(interp, resultObj);
                break;
            }
        }
    }

  try_next_match:
    if (opt_all && (pattern[0] != '^' || (regcomp_flags & REG_NEWLINE)) && *source_str) {
        if (pmatch[0].rm_eo) {
            offset += pmatch[0].rm_eo;
            source_str += pmatch[0].rm_eo;
        }
        else {
            source_str++;
            offset++;
        }
        if (*source_str) {
            eflags = REG_NOTBOL;
            goto next_match;
        }
    }

  done:
    if (result == SAWO_OK) {
        if (opt_inline) {
            sawo_SetResult(interp, resultListObj);
        }
        else {
            sawo_SetResultInt(interp, num_matches);
        }
    }

    sawo_Free(pmatch);
    return result;
}

#define MAX_SUB_MATCHES 50

int sawo_RegsubCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int regcomp_flags = 0;
    int regexec_flags = 0;
    int opt_all = 0;
    int offset = 0;
    regex_t *regex;
    const char *p;
    int result;
    regmatch_t pmatch[MAX_SUB_MATCHES + 1];
    int num_matches = 0;

    int i, j, n;
    sawo_Obj *varname;
    sawo_Obj *resultObj;
    const char *source_str;
    int source_len;
    const char *replace_str;
    int replace_len;
    const char *pattern;
    int option;
    enum {
        OPT_NOCASE, OPT_LINE, OPT_ALL, OPT_START, OPT_END
    };
    static const char * const options[] = {
        "-nocase", "-line", "-all", "-start", "--", NULL
    };

    if (argc < 4) {
      wrongNumArgs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-switch ...? exp string subSpec ?varName?");
        return SAWO_ERR;
    }

    for (i = 1; i < argc; i++) {
        const char *opt = sawo_String(argv[i]);

        if (*opt != '-') {
            break;
        }
        if (sawo_GetEnum(interp, argv[i], options, &option, "switch", SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (option == OPT_END) {
            i++;
            break;
        }
        switch (option) {
            case OPT_NOCASE:
                regcomp_flags |= REG_ICASE;
                break;

            case OPT_LINE:
                regcomp_flags |= REG_NEWLINE;
                break;

            case OPT_ALL:
                opt_all = 1;
                break;

            case OPT_START:
                if (++i == argc) {
                    goto wrongNumArgs;
                }
                if (sawo_GetIndex(interp, argv[i], &offset) != SAWO_OK) {
                    return SAWO_ERR;
                }
                break;
        }
    }
    if (argc - i != 3 && argc - i != 4) {
        goto wrongNumArgs;
    }

    regex = SetRegexpFromAny(interp, argv[i], regcomp_flags);
    if (!regex) {
        return SAWO_ERR;
    }
    pattern = sawo_String(argv[i]);

    source_str = sawo_GetString(argv[i + 1], &source_len);
    replace_str = sawo_GetString(argv[i + 2], &replace_len);
    varname = argv[i + 3];

    resultObj = sawo_NewStringObj(interp, "", 0);

    if (offset) {
        if (offset < 0) {
            offset += source_len + 1;
        }
        if (offset > source_len) {
            offset = source_len;
        }
        else if (offset < 0) {
            offset = 0;
        }
    }

    sawo_AppendString(interp, resultObj, source_str, offset);


    n = source_len - offset;
    p = source_str + offset;
    do {
        int match = regexec(regex, p, MAX_SUB_MATCHES, pmatch, regexec_flags);

        if (match >= REG_BADPAT) {
            char buf[100];

            regerror(match, regex, buf, sizeof(buf));
            sawo_SetResultFormatted(interp, "error while matching pattern: %s", buf);
            return SAWO_ERR;
        }
        if (match == REG_NOMATCH) {
            break;
        }

        num_matches++;

        sawo_AppendString(interp, resultObj, p, pmatch[0].rm_so);


        for (j = 0; j < replace_len; j++) {
            int idx;
            int c = replace_str[j];

            if (c == '&') {
                idx = 0;
            }
            else if (c == '\\' && j < replace_len) {
                c = replace_str[++j];
                if ((c >= '0') && (c <= '9')) {
                    idx = c - '0';
                }
                else if ((c == '\\') || (c == '&')) {
                    sawo_AppendString(interp, resultObj, replace_str + j, 1);
                    continue;
                }
                else {
                    sawo_AppendString(interp, resultObj, replace_str + j - 1, (j == replace_len) ? 1 : 2);
                    continue;
                }
            }
            else {
                sawo_AppendString(interp, resultObj, replace_str + j, 1);
                continue;
            }
            if ((idx < MAX_SUB_MATCHES) && pmatch[idx].rm_so != -1 && pmatch[idx].rm_eo != -1) {
                sawo_AppendString(interp, resultObj, p + pmatch[idx].rm_so,
                    pmatch[idx].rm_eo - pmatch[idx].rm_so);
            }
        }

        p += pmatch[0].rm_eo;
        n -= pmatch[0].rm_eo;

        if (!opt_all || n == 0) {
            break;
        }

        if ((regcomp_flags & REG_NEWLINE) == 0 && pattern[0] == '^') {
            break;
        }

        if (pattern[0] == '\0' && n) {
            sawo_AppendString(interp, resultObj, p, 1);
            p++;
            n--;
        }

        regexec_flags |= REG_NOTBOL;
    } while (n);

    sawo_AppendString(interp, resultObj, p, -1);

    if (argc - i == 4) {
        result = sawo_SetVariable(interp, varname, resultObj);

        if (result == SAWO_OK) {
            sawo_SetResultInt(interp, num_matches);
        }
        else {
            sawo_FreeObj(interp, resultObj);
        }
    }
    else {
        sawo_SetResult(interp, resultObj);
        result = SAWO_OK;
    }

    return result;
}

int sawo_regexpInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "regexp", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "regexp", sawo_RegexpCmd, NULL, NULL);
    sawo_CreateCommand(interp, "regsub", sawo_RegsubCmd, NULL, NULL);
    return SAWO_OK;
}

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>
#include <sys/stat.h>


#ifdef HAVE_UTIMES
#include <sys/time.h>
#endif
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#elif defined(_MSC_VER)
#include <direct.h>
#define F_OK 0
#define W_OK 2
#define R_OK 4
#define S_ISREG(m) (((m) & S_IFMT) == S_IFREG)
#define S_ISDIR(m) (((m) & S_IFMT) == S_IFDIR)
#endif

# ifndef MAXPATHLEN
# define MAXPATHLEN SAWO_PATH_LEN
# endif

#if defined(__MINGW32__) || defined(_MSC_VER)
#define ISWINDOWS 1
#else
#define ISWINDOWS 0
#endif


static const char *sawoGetFileType(int mode)
{
    if (S_ISREG(mode)) {
        return "file";
    }
    else if (S_ISDIR(mode)) {
        return "directory";
    }
#ifdef S_ISCHR
    else if (S_ISCHR(mode)) {
        return "characterSpecial";
    }
#endif
#ifdef S_ISBLK
    else if (S_ISBLK(mode)) {
        return "blockSpecial";
    }
#endif
#ifdef S_ISFIFO
    else if (S_ISFIFO(mode)) {
        return "fifo";
    }
#endif
#ifdef S_ISLNK
    else if (S_ISLNK(mode)) {
        return "link";
    }
#endif
#ifdef S_ISSOCK
    else if (S_ISSOCK(mode)) {
        return "socket";
    }
#endif
    return "unknown";
}

static void AppendStatElement(sawo_Interp *interp, sawo_Obj *listObj, const char *key, sawo_wide value)
{
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, key, -1));
    sawo_ListAppendElement(interp, listObj, sawo_NewIntObj(interp, value));
}

static int StoreStatData(sawo_Interp *interp, sawo_Obj *varName, const struct stat *sb)
{
    
    sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);

    AppendStatElement(interp, listObj, "dev", sb->st_dev);
    AppendStatElement(interp, listObj, "ino", sb->st_ino);
    AppendStatElement(interp, listObj, "mode", sb->st_mode);
    AppendStatElement(interp, listObj, "nlink", sb->st_nlink);
    AppendStatElement(interp, listObj, "uid", sb->st_uid);
    AppendStatElement(interp, listObj, "gid", sb->st_gid);
    AppendStatElement(interp, listObj, "size", sb->st_size);
    AppendStatElement(interp, listObj, "atime", sb->st_atime);
    AppendStatElement(interp, listObj, "mtime", sb->st_mtime);
    AppendStatElement(interp, listObj, "ctime", sb->st_ctime);
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, "type", -1));
    sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, sawoGetFileType((int)sb->st_mode), -1));

    
    if (varName) {
        sawo_Obj *objPtr = sawo_GetVariable(interp, varName, SAWO_NONE);
        if (objPtr) {
            if (sawo_DictSize(interp, objPtr) < 0) {
                
                sawo_SetResultFormatted(interp, "can't set \"%#s(dev)\": variable isn't array", varName);
                sawo_FreeNewObj(interp, listObj);
                return SAWO_ERR;
            }

            if (sawo_IsShared(objPtr))
                objPtr = sawo_DuplicateObj(interp, objPtr);

            
            sawo_ListAppendList(interp, objPtr, listObj);
            sawo_DictSize(interp, objPtr);
            sawo_InvalidateStringRep(objPtr);

            sawo_FreeNewObj(interp, listObj);
            listObj = objPtr;
        }
        sawo_SetVariable(interp, varName, listObj);
    }

    
    sawo_SetResult(interp, listObj);

    return SAWO_OK;
}

static int file_cmd_dirname(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *p = strrchr(path, '/');

    if (!p && path[0] == '.' && path[1] == '.' && path[2] == '\0') {
        sawo_SetResultString(interp, "..", -1);
    } else if (!p) {
        sawo_SetResultString(interp, ".", -1);
    }
    else if (p == path) {
        sawo_SetResultString(interp, "/", -1);
    }
    else if (ISWINDOWS && p[-1] == ':') {
        
        sawo_SetResultString(interp, path, p - path + 1);
    }
    else {
        sawo_SetResultString(interp, path, p - path);
    }
    return SAWO_OK;
}

static int file_cmd_rootname(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');
    const char *p = strrchr(path, '.');

    if (p == NULL || (lastSlash != NULL && lastSlash > p)) {
        sawo_SetResult(interp, argv[0]);
    }
    else {
        sawo_SetResultString(interp, path, p - path);
    }
    return SAWO_OK;
}

static int file_cmd_extension(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');
    const char *p = strrchr(path, '.');

    if (p == NULL || (lastSlash != NULL && lastSlash >= p)) {
        p = "";
    }
    sawo_SetResultString(interp, p, -1);
    return SAWO_OK;
}

static int file_cmd_tail(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    const char *lastSlash = strrchr(path, '/');

    if (lastSlash) {
        sawo_SetResultString(interp, lastSlash + 1, -1);
    }
    else {
        sawo_SetResult(interp, argv[0]);
    }
    return SAWO_OK;
}

static int file_cmd_normalize(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef HAVE_REALPATH
    const char *path = sawo_String(argv[0]);
    char *newname = sawo_Alloc(MAXPATHLEN + 1);

    if (realpath(path, newname)) {
        sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, newname, -1));
        return SAWO_OK;
    }
    else {
        sawo_Free(newname);
        sawo_SetResultFormatted(interp, "can't normalize \"%#s\": %s", argv[0], strerror(errno));
        return SAWO_ERR;
    }
#else
    sawo_SetResultString(interp, "Not implemented", -1);
    return SAWO_ERR;
#endif
}

static int file_cmd_join(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    char *newname = sawo_Alloc(MAXPATHLEN + 1);
    char *last = newname;

    *newname = 0;

    
    for (i = 0; i < argc; i++) {
        int len;
        const char *part = sawo_GetString(argv[i], &len);

        if (*part == '/') {
            
            last = newname;
        }
        else if (ISWINDOWS && strchr(part, ':')) {
            
            last = newname;
        }
        else if (part[0] == '.') {
            if (part[1] == '/') {
                part += 2;
                len -= 2;
            }
            else if (part[1] == 0 && last != newname) {
                
                continue;
            }
        }

        
        if (last != newname && last[-1] != '/') {
            *last++ = '/';
        }

        if (len) {
            if (last + len - newname >= MAXPATHLEN) {
                sawo_Free(newname);
                sawo_SetResultString(interp, "Path too long", -1);
                return SAWO_ERR;
            }
            memcpy(last, part, len);
            last += len;
        }

        
        if (last > newname + 1 && last[-1] == '/') {
            
            if (!ISWINDOWS || !(last > newname + 2 && last[-2] == ':')) {
                *--last = 0;
            }
        }
    }

    *last = 0;

    

    sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, newname, last - newname));

    return SAWO_OK;
}

static int file_access(sawo_Interp *interp, sawo_Obj *filename, int mode)
{
    sawo_SetResultBool(interp, access(sawo_String(filename), mode) != -1);

    return SAWO_OK;
}

static int file_cmd_readable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], R_OK);
}

static int file_cmd_writable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], W_OK);
}

static int file_cmd_executable(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#ifdef X_OK
    return file_access(interp, argv[0], X_OK);
#else
    
    sawo_SetResultBool(interp, 1);
    return SAWO_OK;
#endif
}

static int file_cmd_exists(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return file_access(interp, argv[0], F_OK);
}

static int file_cmd_delete(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int force = sawo_CompareStringImmediate(interp, argv[0], "-force");

    if (force || sawo_CompareStringImmediate(interp, argv[0], "--")) {
        argc++;
        argv--;
    }

    while (argc--) {
        const char *path = sawo_String(argv[0]);

        if (unlink(path) == -1 && errno != ENOENT) {
            if (rmdir(path) == -1) {
                
                if (!force || sawo_EvalPrefix(interp, "file delete force", 1, argv) != SAWO_OK) {
                    sawo_SetResultFormatted(interp, "couldn't delete file \"%s\": %s", path,
                        strerror(errno));
                    return SAWO_ERR;
                }
            }
        }
        argv++;
    }
    return SAWO_OK;
}

#ifdef HAVE_MKDIR_ONE_ARG
#define MKDIR_DEFAULT(PATHNAME) mkdir(PATHNAME)
#else
#define MKDIR_DEFAULT(PATHNAME) mkdir(PATHNAME, 0755)
#endif

static int mkdir_all(char *path)
{
    int ok = 1;

    
    goto first;

    while (ok--) {
        
        {
            char *slash = strrchr(path, '/');

            if (slash && slash != path) {
                *slash = 0;
                if (mkdir_all(path) != 0) {
                    return -1;
                }
                *slash = '/';
            }
        }
      first:
        if (MKDIR_DEFAULT(path) == 0) {
            return 0;
        }
        if (errno == ENOENT) {
            
            continue;
        }
        
        if (errno == EEXIST) {
            struct stat sb;

            if (stat(path, &sb) == 0 && S_ISDIR(sb.st_mode)) {
                return 0;
            }
            
            errno = EEXIST;
        }
        
        break;
    }
    return -1;
}

static int file_cmd_mkdir(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    while (argc--) {
        char *path = sawo_StrDup(sawo_String(argv[0]));
        int rc = mkdir_all(path);

        sawo_Free(path);
        if (rc != 0) {
            sawo_SetResultFormatted(interp, "can't create directory \"%#s\": %s", argv[0],
                strerror(errno));
            return SAWO_ERR;
        }
        argv++;
    }
    return SAWO_OK;
}

static int file_cmd_tempfile(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int fd = sawo_MakeTempFile(interp, (argc >= 1) ? sawo_String(argv[0]) : NULL);

    if (fd < 0) {
        return SAWO_ERR;
    }
    close(fd);

    return SAWO_OK;
}

static int file_cmd_rename(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *source;
    const char *dest;
    int force = 0;

    if (argc == 3) {
        if (!sawo_CompareStringImmediate(interp, argv[0], "-force")) {
            return -1;
        }
        force++;
        argv++;
        argc--;
    }

    source = sawo_String(argv[0]);
    dest = sawo_String(argv[1]);

    if (!force && access(dest, F_OK) == 0) {
        sawo_SetResultFormatted(interp, "error renaming \"%#s\" to \"%#s\": target exists", argv[0],
            argv[1]);
        return SAWO_ERR;
    }

    if (rename(source, dest) != 0) {
        sawo_SetResultFormatted(interp, "error renaming \"%#s\" to \"%#s\": %s", argv[0], argv[1],
            strerror(errno));
        return SAWO_ERR;
    }

    return SAWO_OK;
}

#if defined(HAVE_LINK) && defined(HAVE_SYMLINK)
static int file_cmd_link(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int ret;
    const char *source;
    const char *dest;
    static const char * const options[] = { "-hard", "-symbolic", NULL };
    enum { OPT_HARD, OPT_SYMBOLIC, };
    int option = OPT_HARD;

    if (argc == 3) {
        if (sawo_GetEnum(interp, argv[0], options, &option, NULL, SAWO_ENUM_ABBREV | SAWO_ERRMSG) != SAWO_OK) {
            return SAWO_ERR;
        }
        argv++;
        argc--;
    }

    dest = sawo_String(argv[0]);
    source = sawo_String(argv[1]);

    if (option == OPT_HARD) {
        ret = link(source, dest);
    }
    else {
        ret = symlink(source, dest);
    }

    if (ret != 0) {
        sawo_SetResultFormatted(interp, "error linking \"%#s\" to \"%#s\": %s", argv[0], argv[1],
            strerror(errno));
        return SAWO_ERR;
    }

    return SAWO_OK;
}
#endif

static int file_stat(sawo_Interp *interp, sawo_Obj *filename, struct stat *sb)
{
    const char *path = sawo_String(filename);

    if (stat(path, sb) == -1) {
        sawo_SetResultFormatted(interp, "could not read \"%#s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}

#ifdef HAVE_LSTAT
static int file_lstat(sawo_Interp *interp, sawo_Obj *filename, struct stat *sb)
{
    const char *path = sawo_String(filename);

    if (lstat(path, sb) == -1) {
        sawo_SetResultFormatted(interp, "could not read \"%#s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}
#else
#define file_lstat file_stat
#endif

static int file_cmd_atime(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_atime);
    return SAWO_OK;
}

static int file_cmd_mtime(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (argc == 2) {
#ifdef HAVE_UTIMES
        sawo_wide newtime;
        struct timeval times[2];

        if (sawo_GetWide(interp, argv[1], &newtime) != SAWO_OK) {
            return SAWO_ERR;
        }

        times[1].tv_sec = times[0].tv_sec = newtime;
        times[1].tv_usec = times[0].tv_usec = 0;

        if (utimes(sawo_String(argv[0]), times) != 0) {
            sawo_SetResultFormatted(interp, "can't set time on \"%#s\": %s", argv[0], strerror(errno));
            return SAWO_ERR;
        }
#else
        sawo_SetResultString(interp, "Not implemented", -1);
        return SAWO_ERR;
#endif
    }
    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_mtime);
    return SAWO_OK;
}

static int file_cmd_copy(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawo_EvalPrefix(interp, "file copy", argc, argv);
}

static int file_cmd_size(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sb.st_size);
    return SAWO_OK;
}

static int file_cmd_isdirectory(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = S_ISDIR(sb.st_mode);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}

static int file_cmd_isfile(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = S_ISREG(sb.st_mode);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}

#ifdef HAVE_GETEUID
static int file_cmd_owned(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;
    int ret = 0;

    if (file_stat(interp, argv[0], &sb) == SAWO_OK) {
        ret = (geteuid() == sb.st_uid);
    }
    sawo_SetResultInt(interp, ret);
    return SAWO_OK;
}
#endif

#if defined(HAVE_READLINK)
static int file_cmd_readlink(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path = sawo_String(argv[0]);
    char *linkValue = sawo_Alloc(MAXPATHLEN + 1);

    int linkLength = readlink(path, linkValue, MAXPATHLEN);

    if (linkLength == -1) {
        sawo_Free(linkValue);
        sawo_SetResultFormatted(interp, "couldn't readlink \"%#s\": %s", argv[0], strerror(errno));
        return SAWO_ERR;
    }
    linkValue[linkLength] = 0;
    sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, linkValue, linkLength));
    return SAWO_OK;
}
#endif

static int file_cmd_type(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_lstat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResultString(interp, sawoGetFileType((int)sb.st_mode), -1);
    return SAWO_OK;
}

#ifdef HAVE_LSTAT
static int file_cmd_lstat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_lstat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    return StoreStatData(interp, argc == 2 ? argv[1] : NULL, &sb);
}
#else
#define file_cmd_lstat file_cmd_stat
#endif

static int file_cmd_stat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct stat sb;

    if (file_stat(interp, argv[0], &sb) != SAWO_OK) {
        return SAWO_ERR;
    }
    return StoreStatData(interp, argc == 2 ? argv[1] : NULL, &sb);
}

static const sawo_subcmd_type file_command_table[] = {
    {   "atime",
        "name",
        file_cmd_atime,
        1,
        1,
        
    },
    {   "mtime",
        "name ?time?",
        file_cmd_mtime,
        1,
        2,
        
    },
    {   "copy",
        "?-force? source dest",
        file_cmd_copy,
        2,
        3,
        
    },
    {   "dirname",
        "name",
        file_cmd_dirname,
        1,
        1,
        
    },
    {   "rootname",
        "name",
        file_cmd_rootname,
        1,
        1,
        
    },
    {   "extension",
        "name",
        file_cmd_extension,
        1,
        1,
        
    },
    {   "tail",
        "name",
        file_cmd_tail,
        1,
        1,
        
    },
    {   "normalize",
        "name",
        file_cmd_normalize,
        1,
        1,
        
    },
    {   "join",
        "name ?name ...?",
        file_cmd_join,
        1,
        -1,
        
    },
    {   "readable",
        "name",
        file_cmd_readable,
        1,
        1,
        
    },
    {   "writable",
        "name",
        file_cmd_writable,
        1,
        1,
        
    },
    {   "executable",
        "name",
        file_cmd_executable,
        1,
        1,
        
    },
    {   "exists",
        "name",
        file_cmd_exists,
        1,
        1,
        
    },
    {   "delete",
        "?-force|--? name ...",
        file_cmd_delete,
        1,
        -1,
        
    },
    {   "mkdir",
        "dir ...",
        file_cmd_mkdir,
        1,
        -1,
        
    },
    {   "tempfile",
        "?template?",
        file_cmd_tempfile,
        0,
        1,
        
    },
    {   "rename",
        "?-force? source dest",
        file_cmd_rename,
        2,
        3,
        
    },
#if defined(HAVE_LINK) && defined(HAVE_SYMLINK)
    {   "link",
        "?-symbolic|-hard? newname target",
        file_cmd_link,
        2,
        3,
        
    },
#endif
#if defined(HAVE_READLINK)
    {   "readlink",
        "name",
        file_cmd_readlink,
        1,
        1,
        
    },
#endif
    {   "size",
        "name",
        file_cmd_size,
        1,
        1,
        
    },
    {   "stat",
        "name ?var?",
        file_cmd_stat,
        1,
        2,
        
    },
    {   "lstat",
        "name ?var?",
        file_cmd_lstat,
        1,
        2,
        
    },
    {   "type",
        "name",
        file_cmd_type,
        1,
        1,
        
    },
#ifdef HAVE_GETEUID
    {   "owned",
        "name",
        file_cmd_owned,
        1,
        1,
        
    },
#endif
    {   "isdirectory",
        "name",
        file_cmd_isdirectory,
        1,
        1,
        
    },
    {   "isfile",
        "name",
        file_cmd_isfile,
        1,
        1,
        
    },
    {
        NULL
    }
};

static int sawo_CdCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *path;

    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "dirname");
        return SAWO_ERR;
    }

    path = sawo_String(argv[1]);

    if (chdir(path) != 0) {
        sawo_SetResultFormatted(interp, "couldn't change working directory to \"%s\": %s", path,
            strerror(errno));
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int sawo_PwdCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    char *cwd = sawo_Alloc(MAXPATHLEN);

    if (getcwd(cwd, MAXPATHLEN) == NULL) {
        sawo_SetResultString(interp, "Failed to get pwd", -1);
        sawo_Free(cwd);
        return SAWO_ERR;
    }
    else if (ISWINDOWS) {
        
        char *p = cwd;
        while ((p = strchr(p, '\\')) != NULL) {
            *p++ = '/';
        }
    }

    sawo_SetResultString(interp, cwd, -1);

    sawo_Free(cwd);
    return SAWO_OK;
}

int sawo_fileInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "file", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "file", sawo_SubCmdRaise, (void *)file_command_table, NULL);
    sawo_CreateCommand(interp, "pwd", sawo_PwdCmd, NULL, NULL);
    sawo_CreateCommand(interp, "cd", sawo_CdCmd, NULL, NULL);
    return SAWO_OK;
}

#include <string.h>
#include <ctype.h>


#if (!defined(HAVE_VFORK) || !defined(HAVE_WAITPID)) && !defined(__MINGW32__)
static int sawo_ExecCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *cmdlineObj = sawo_NewEmptyStringObj(interp);
    int i, j;
    int rc;

    
    for (i = 1; i < argc; i++) {
        int len;
        const char *arg = sawo_GetString(argv[i], &len);

        if (i > 1) {
            sawo_AppendString(interp, cmdlineObj, " ", 1);
        }
        if (strpbrk(arg, "\\\" ") == NULL) {
            
            sawo_AppendString(interp, cmdlineObj, arg, len);
            continue;
        }

        sawo_AppendString(interp, cmdlineObj, "\"", 1);
        for (j = 0; j < len; j++) {
            if (arg[j] == '\\' || arg[j] == '"') {
                sawo_AppendString(interp, cmdlineObj, "\\", 1);
            }
            sawo_AppendString(interp, cmdlineObj, &arg[j], 1);
        }
        sawo_AppendString(interp, cmdlineObj, "\"", 1);
    }
    rc = system(sawo_String(cmdlineObj));

    sawo_FreeNewObj(interp, cmdlineObj);

    if (rc) {
        sawo_Obj *errorCode = sawo_NewListObj(interp, NULL, 0);
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, "CHILDSTATUS", -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, 0));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, rc));
        sawo_SetGlobalVariableStr(interp, "errorCode", errorCode);
        return SAWO_ERR;
    }

    return SAWO_OK;
}

int sawo_execInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "exec", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "exec", sawo_ExecCmd, NULL, NULL);
    return SAWO_OK;
}
#else


#include <errno.h>
#include <signal.h>

#if defined(__MINGW32__)
    
    #ifndef STRICT
    #define STRICT
    #endif
    #define WIN32_LEAN_AND_MEAN
    #include <windows.h>
    #include <fcntl.h>

    typedef HANDLE fdtype;
    typedef HANDLE pidtype;
    #define SAWO_BAD_FD INVALID_HANDLE_VALUE
    #define SAWO_BAD_PID INVALID_HANDLE_VALUE
    #define sawoCloseFd CloseHandle

    #define WIFEXITED(STATUS) 1
    #define WEXITSTATUS(STATUS) (STATUS)
    #define WIFSIGNALED(STATUS) 0
    #define WTERMSIG(STATUS) 0
    #define WNOHANG 1

    static fdtype sawoFileno(FILE *fh);
    static pidtype sawoWaitPid(pidtype pid, int *status, int nohang);
    static fdtype sawoDupFd(fdtype infd);
    static fdtype sawoOpenForRead(const char *filename);
    static FILE *sawoFdOpenForRead(fdtype fd);
    static int sawoPipe(fdtype pipefd[2]);
    static pidtype sawoStartWinProcess(sawo_Interp *interp, char **argv, char *env,
        fdtype inputId, fdtype outputId, fdtype errorId);
    static int sawoErrno(void);
#else
    #include <unistd.h>
    #include <fcntl.h>
    #include <sys/wait.h>
    #include <sys/stat.h>

    typedef int fdtype;
    typedef int pidtype;
    #define sawoPipe pipe
    #define sawoErrno() errno
    #define SAWO_BAD_FD -1
    #define SAWO_BAD_PID -1
    #define sawoFileno fileno
    #define sawoReadFd read
    #define sawoCloseFd close
    #define sawoWaitPid waitpid
    #define sawoDupFd dup
    #define sawoFdOpenForRead(FD) fdopen((FD), "r")
    #define sawoOpenForRead(NAME) open((NAME), O_RDONLY, 0)

    #ifndef HAVE_EXECVPE
        #define execvpe(ARG0, ARGV, ENV) execvp(ARG0, ARGV)
    #endif
#endif

static const char *sawoStrError(void);
static char **sawoSaveEnv(char **env);
static void sawoRestoreEnv(char **env);
static int sawoCreatePipeline(sawo_Interp *interp, int argc, sawo_Obj *const *argv,
    pidtype **pidArrayPtr, fdtype *inPipePtr, fdtype *outPipePtr, fdtype *errFilePtr);
static void sawoDetachPids(sawo_Interp *interp, int numPids, const pidtype *pidPtr);
static int sawoCleanupChildren(sawo_Interp *interp, int numPids, pidtype *pidPtr, sawo_Obj *errStrObj);
static fdtype sawoCreateTemp(sawo_Interp *interp, const char *contents, int len);
static fdtype sawoOpenForWrite(const char *filename, int append);
static int sawoRewindFd(fdtype fd);

static void sawo_SetResultErrno(sawo_Interp *interp, const char *msg)
{
    sawo_SetResultFormatted(interp, "%s: %s", msg, sawoStrError());
}

static const char *sawoStrError(void)
{
    return strerror(sawoErrno());
}

static void sawo_RemoveTrailingNewline(sawo_Obj *objPtr)
{
    int len;
    const char *s = sawo_GetString(objPtr, &len);

    if (len > 0 && s[len - 1] == '\n') {
        objPtr->length--;
        objPtr->bytes[objPtr->length] = '\0';
    }
}

static int sawoAppendStreamToString(sawo_Interp *interp, fdtype fd, sawo_Obj *strObj)
{
    char buf[256];
    FILE *fh = sawoFdOpenForRead(fd);
    int ret = 0;

    if (fh == NULL) {
        return -1;
    }

    while (1) {
        int retval = fread(buf, 1, sizeof(buf), fh);
        if (retval > 0) {
            ret = 1;
            sawo_AppendString(interp, strObj, buf, retval);
        }
        if (retval != sizeof(buf)) {
            break;
        }
    }
    fclose(fh);
    return ret;
}

static char **sawoBuildEnv(sawo_Interp *interp)
{
    int i;
    int size;
    int num;
    int n;
    char **envptr;
    char *envdata;

    sawo_Obj *objPtr = sawo_GetGlobalVariableStr(interp, "env", SAWO_NONE);

    if (!objPtr) {
        return sawo_GetEnviron();
    }


    
    num = sawo_ListLength(interp, objPtr);
    if (num % 2) {
        
        num--;
    }
    size = sawo_Length(objPtr) + 2;

    envptr = sawo_Alloc(sizeof(*envptr) * (num / 2 + 1) + size);
    envdata = (char *)&envptr[num / 2 + 1];

    n = 0;
    for (i = 0; i < num; i += 2) {
        const char *s1, *s2;
        sawo_Obj *elemObj;

        sawo_ListIndex(interp, objPtr, i, &elemObj, SAWO_NONE);
        s1 = sawo_String(elemObj);
        sawo_ListIndex(interp, objPtr, i + 1, &elemObj, SAWO_NONE);
        s2 = sawo_String(elemObj);

        envptr[n] = envdata;
        envdata += sprintf(envdata, "%s=%s", s1, s2);
        envdata++;
        n++;
    }
    envptr[n] = NULL;
    *envdata = 0;

    return envptr;
}

static void sawoFreeEnv(char **env, char **original_environ)
{
    if (env != original_environ) {
        sawo_Free(env);
    }
}

#ifndef sawo_ext_signal

const char *sawo_SignalId(int sig)
{
    static char buf[10];
    snprintf(buf, sizeof(buf), "%d", sig);
    return buf;
}

const char *sawo_SignalName(int sig)
{
    return sawo_SignalId(sig);
}
#endif

static int sawoCheckWaitStatus(sawo_Interp *interp, pidtype pid, int waitStatus, sawo_Obj *errStrObj)
{
    sawo_Obj *errorCode;

    if (WIFEXITED(waitStatus) && WEXITSTATUS(waitStatus) == 0) {
        return SAWO_OK;
    }
    errorCode = sawo_NewListObj(interp, NULL, 0);

    if (WIFEXITED(waitStatus)) {
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, "CHILDSTATUS", -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, (long)pid));
        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, WEXITSTATUS(waitStatus)));
    }
    else {
        const char *type;
        const char *action;

        if (WIFSIGNALED(waitStatus)) {
            type = "CHILDKILLED";
            action = "killed";
        }
        else {
            type = "CHILDSUSP";
            action = "suspended";
        }

        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, type, -1));

        if (errStrObj) {
            sawo_AppendStrings(interp, errStrObj, "child ", action, " by signal ", sawo_SignalId(WTERMSIG(waitStatus)), "\n", NULL);
        }

        sawo_ListAppendElement(interp, errorCode, sawo_NewIntObj(interp, (long)pid));
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, sawo_SignalId(WTERMSIG(waitStatus)), -1));
        sawo_ListAppendElement(interp, errorCode, sawo_NewStringObj(interp, sawo_SignalName(WTERMSIG(waitStatus)), -1));
    }
    sawo_SetGlobalVariableStr(interp, "errorCode", errorCode);

    return SAWO_ERR;
}


struct WaitInfo
{
    pidtype pid;                
    int status;                 
    int flags;                  
};

struct WaitInfoTable {
    struct WaitInfo *show;      
    int size;                   
    int used;                   
};


#define WI_DETACHED 2

#define WAIT_TABLE_GROW_BY 4

static void sawoFreeWaitInfoTable(struct sawo_Interp *interp, void *privData)
{
    struct WaitInfoTable *table = privData;

    sawo_Free(table->show);
    sawo_Free(table);
}

static struct WaitInfoTable *sawoAllocWaitInfoTable(void)
{
    struct WaitInfoTable *table = sawo_Alloc(sizeof(*table));
    table->show = NULL;
    table->size = table->used = 0;

    return table;
}

static int sawo_ExecCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    fdtype outputId;    
    fdtype errorId;     
    pidtype *pidPtr;
    int numPids, result;
    int child_siginfo = 1;
    sawo_Obj *childErrObj;
    sawo_Obj *errStrObj;

    if (argc > 1 && sawo_CompareStringImmediate(interp, argv[argc - 1], "&")) {
        sawo_Obj *listObj;
        int i;

        argc--;
        numPids = sawoCreatePipeline(interp, argc - 1, argv + 1, &pidPtr, NULL, NULL, NULL);
        if (numPids < 0) {
            return SAWO_ERR;
        }
        
        listObj = sawo_NewListObj(interp, NULL, 0);
        for (i = 0; i < numPids; i++) {
            sawo_ListAppendElement(interp, listObj, sawo_NewIntObj(interp, (long)pidPtr[i]));
        }
        sawo_SetResult(interp, listObj);
        sawoDetachPids(interp, numPids, pidPtr);
        sawo_Free(pidPtr);
        return SAWO_OK;
    }

    numPids =
        sawoCreatePipeline(interp, argc - 1, argv + 1, &pidPtr, NULL, &outputId, &errorId);

    if (numPids < 0) {
        return SAWO_ERR;
    }

    result = SAWO_OK;

    errStrObj = sawo_NewStringObj(interp, "", 0);

    
    if (outputId != SAWO_BAD_FD) {
        if (sawoAppendStreamToString(interp, outputId, errStrObj) < 0) {
            result = SAWO_ERR;
            sawo_SetResultErrno(interp, "error reading from output pipe");
        }
    }

    
    childErrObj = sawo_NewStringObj(interp, "", 0);
    sawo_IncrRefCount(childErrObj);

    if (sawoCleanupChildren(interp, numPids, pidPtr, childErrObj) != SAWO_OK) {
        result = SAWO_ERR;
    }

    if (errorId != SAWO_BAD_FD) {
        int ret;
        sawoRewindFd(errorId);
        ret = sawoAppendStreamToString(interp, errorId, errStrObj);
        if (ret < 0) {
            sawo_SetResultErrno(interp, "error reading from error pipe");
            result = SAWO_ERR;
        }
        else if (ret > 0) {
            
            child_siginfo = 0;
        }
    }

    if (child_siginfo) {
        
        sawo_AppendObj(interp, errStrObj, childErrObj);
    }
    sawo_DecrRefCount(interp, childErrObj);

    
    sawo_RemoveTrailingNewline(errStrObj);

    
    sawo_SetResult(interp, errStrObj);

    return result;
}

static void sawoReapDetachedPids(struct WaitInfoTable *table)
{
    struct WaitInfo *waitPtr;
    int count;
    int dest;

    if (!table) {
        return;
    }

    waitPtr = table->show;
    dest = 0;
    for (count = table->used; count > 0; waitPtr++, count--) {
        if (waitPtr->flags & WI_DETACHED) {
            int status;
            pidtype pid = sawoWaitPid(waitPtr->pid, &status, WNOHANG);
            if (pid == waitPtr->pid) {
                
                table->used--;
                continue;
            }
        }
        if (waitPtr != &table->show[dest]) {
            table->show[dest] = *waitPtr;
        }
        dest++;
    }
}

static pidtype sawoWaitForProcess(struct WaitInfoTable *table, pidtype pid, int *statusPtr)
{
    int i;

    
    for (i = 0; i < table->used; i++) {
        if (pid == table->show[i].pid) {
            
            sawoWaitPid(pid, statusPtr, 0);

            
            if (i != table->used - 1) {
                table->show[i] = table->show[table->used - 1];
            }
            table->used--;
            return pid;
        }
    }

    
    return SAWO_BAD_PID;
}

static void sawoDetachPids(sawo_Interp *interp, int numPids, const pidtype *pidPtr)
{
    int j;
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);

    for (j = 0; j < numPids; j++) {
        
        int i;
        for (i = 0; i < table->used; i++) {
            if (pidPtr[j] == table->show[i].pid) {
                table->show[i].flags |= WI_DETACHED;
                break;
            }
        }
    }
}

static FILE *sawoGetAioFilehandle(sawo_Interp *interp, const char *name)
{
    FILE *fh;
    sawo_Obj *fhObj;

    fhObj = sawo_NewStringObj(interp, name, -1);
    sawo_IncrRefCount(fhObj);
    fh = sawo_AioFilehandle(interp, fhObj);
    sawo_DecrRefCount(interp, fhObj);

    return fh;
}

static int
sawoCreatePipeline(sawo_Interp *interp, int argc, sawo_Obj *const *argv, pidtype **pidArrayPtr,
    fdtype *inPipePtr, fdtype *outPipePtr, fdtype *errFilePtr)
{
    pidtype *pidPtr = NULL;         /* Points to malloc-ed array holding all
                                 * the pids of child processes. */
    int numPids = 0;            /* Actual number of processes that exist
                                 * at *pidPtr right now. */
    int cmdCount;               /* Count of number of distinct commands
                                 * found in argc/argv. */
    const char *input = NULL;   /* Describes input for pipeline, depending
                                 * on "inputFile".  NULL means take input
                                 * from stdin/pipe. */
    int input_len = 0;          

#define FILE_NAME   0           
#define FILE_APPEND 1           
#define FILE_HANDLE 2           
#define FILE_TEXT   3           

    int inputFile = FILE_NAME;  /* 1 means input is name of input file.
                                 * 2 means input is filehandle name.
                                 * 0 means input holds actual
                                 * text to be input to command. */

    int outputFile = FILE_NAME; /* 0 means output is the name of output file.
                                 * 1 means output is the name of output file, and append.
                                 * 2 means output is filehandle name.
                                 * All this is ignored if output is NULL
                                 */
    int errorFile = FILE_NAME;  /* 0 means error is the name of error file.
                                 * 1 means error is the name of error file, and append.
                                 * 2 means error is filehandle name.
                                 * All this is ignored if error is NULL
                                 */
    const char *output = NULL;  /* Holds name of output file to pipe to,
                                 * or NULL if output goes to stdout/pipe. */
    const char *error = NULL;   /* Holds name of stderr file to pipe to,
                                 * or NULL if stderr goes to stderr/pipe. */
    fdtype inputId = SAWO_BAD_FD;
    fdtype outputId = SAWO_BAD_FD;
    fdtype errorId = SAWO_BAD_FD;
    fdtype lastOutputId = SAWO_BAD_FD;
    fdtype pipeIds[2];           
    int firstArg, lastArg;      /* Indexes of first and last arguments in
                                 * current command. */
    int lastBar;
    int i;
    pidtype pid;
    char **save_environ;
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);

    
    char **arg_array = sawo_Alloc(sizeof(*arg_array) * (argc + 1));
    int arg_count = 0;

    sawoReapDetachedPids(table);

    if (inPipePtr != NULL) {
        *inPipePtr = SAWO_BAD_FD;
    }
    if (outPipePtr != NULL) {
        *outPipePtr = SAWO_BAD_FD;
    }
    if (errFilePtr != NULL) {
        *errFilePtr = SAWO_BAD_FD;
    }
    pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;

    cmdCount = 1;
    lastBar = -1;
    for (i = 0; i < argc; i++) {
        const char *arg = sawo_String(argv[i]);

        if (arg[0] == '<') {
            inputFile = FILE_NAME;
            input = arg + 1;
            if (*input == '<') {
                inputFile = FILE_TEXT;
                input_len = sawo_Length(argv[i]) - 2;
                input++;
            }
            else if (*input == '@') {
                inputFile = FILE_HANDLE;
                input++;
            }

            if (!*input && ++i < argc) {
                input = sawo_GetString(argv[i], &input_len);
            }
        }
        else if (arg[0] == '>') {
            int dup_error = 0;

            outputFile = FILE_NAME;

            output = arg + 1;
            if (*output == '>') {
                outputFile = FILE_APPEND;
                output++;
            }
            if (*output == '&') {
                
                output++;
                dup_error = 1;
            }
            if (*output == '@') {
                outputFile = FILE_HANDLE;
                output++;
            }
            if (!*output && ++i < argc) {
                output = sawo_String(argv[i]);
            }
            if (dup_error) {
                errorFile = outputFile;
                error = output;
            }
        }
        else if (arg[0] == '2' && arg[1] == '>') {
            error = arg + 2;
            errorFile = FILE_NAME;

            if (*error == '@') {
                errorFile = FILE_HANDLE;
                error++;
            }
            else if (*error == '>') {
                errorFile = FILE_APPEND;
                error++;
            }
            if (!*error && ++i < argc) {
                error = sawo_String(argv[i]);
            }
        }
        else {
            if (strcmp(arg, "|") == 0 || strcmp(arg, "|&") == 0) {
                if (i == lastBar + 1 || i == argc - 1) {
                    sawo_SetResultString(interp, "illegal use of | or |& in command", -1);
                    goto badargs;
                }
                lastBar = i;
                cmdCount++;
            }
            
            arg_array[arg_count++] = (char *)arg;
            continue;
        }

        if (i >= argc) {
            sawo_SetResultFormatted(interp, "can't specify \"%s\" as last word in command", arg);
            goto badargs;
        }
    }

    if (arg_count == 0) {
        sawo_SetResultString(interp, "didn't specify command to execute", -1);
badargs:
        sawo_Free(arg_array);
        return -1;
    }

    
    save_environ = sawoSaveEnv(sawoBuildEnv(interp));

    if (input != NULL) {
        if (inputFile == FILE_TEXT) {
            inputId = sawoCreateTemp(interp, input, input_len);
            if (inputId == SAWO_BAD_FD) {
                goto error;
            }
        }
        else if (inputFile == FILE_HANDLE) {
            
            FILE *fh = sawoGetAioFilehandle(interp, input);

            if (fh == NULL) {
                goto error;
            }
            inputId = sawoDupFd(sawoFileno(fh));
        }
        else {
            inputId = sawoOpenForRead(input);
            if (inputId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't read file \"%s\": %s", input, sawoStrError());
                goto error;
            }
        }
    }
    else if (inPipePtr != NULL) {
        if (sawoPipe(pipeIds) != 0) {
            sawo_SetResultErrno(interp, "couldn't create input pipe for command");
            goto error;
        }
        inputId = pipeIds[0];
        *inPipePtr = pipeIds[1];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }

    if (output != NULL) {
        if (outputFile == FILE_HANDLE) {
            FILE *fh = sawoGetAioFilehandle(interp, output);
            if (fh == NULL) {
                goto error;
            }
            fflush(fh);
            lastOutputId = sawoDupFd(sawoFileno(fh));
        }
        else {
            lastOutputId = sawoOpenForWrite(output, outputFile == FILE_APPEND);
            if (lastOutputId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't write file \"%s\": %s", output, sawoStrError());
                goto error;
            }
        }
    }
    else if (outPipePtr != NULL) {
        if (sawoPipe(pipeIds) != 0) {
            sawo_SetResultErrno(interp, "couldn't create output pipe");
            goto error;
        }
        lastOutputId = pipeIds[1];
        *outPipePtr = pipeIds[0];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }
    
    if (error != NULL) {
        if (errorFile == FILE_HANDLE) {
            if (strcmp(error, "1") == 0) {
                
                if (lastOutputId != SAWO_BAD_FD) {
                    errorId = sawoDupFd(lastOutputId);
                }
                else {
                    
                    error = "stdout";
                }
            }
            if (errorId == SAWO_BAD_FD) {
                FILE *fh = sawoGetAioFilehandle(interp, error);
                if (fh == NULL) {
                    goto error;
                }
                fflush(fh);
                errorId = sawoDupFd(sawoFileno(fh));
            }
        }
        else {
            errorId = sawoOpenForWrite(error, errorFile == FILE_APPEND);
            if (errorId == SAWO_BAD_FD) {
                sawo_SetResultFormatted(interp, "couldn't write file \"%s\": %s", error, sawoStrError());
                goto error;
            }
        }
    }
    else if (errFilePtr != NULL) {
        errorId = sawoCreateTemp(interp, NULL, 0);
        if (errorId == SAWO_BAD_FD) {
            goto error;
        }
        *errFilePtr = sawoDupFd(errorId);
    }


    pidPtr = sawo_Alloc(cmdCount * sizeof(*pidPtr));
    for (i = 0; i < numPids; i++) {
        pidPtr[i] = SAWO_BAD_PID;
    }
    for (firstArg = 0; firstArg < arg_count; numPids++, firstArg = lastArg + 1) {
        int pipe_dup_err = 0;
        fdtype origErrorId = errorId;

        for (lastArg = firstArg; lastArg < arg_count; lastArg++) {
            if (arg_array[lastArg][0] == '|') {
                if (arg_array[lastArg][1] == '&') {
                    pipe_dup_err = 1;
                }
                break;
            }
        }
        
        arg_array[lastArg] = NULL;
        if (lastArg == arg_count) {
            outputId = lastOutputId;
        }
        else {
            if (sawoPipe(pipeIds) != 0) {
                sawo_SetResultErrno(interp, "couldn't create pipe");
                goto error;
            }
            outputId = pipeIds[1];
        }

        
        if (pipe_dup_err) {
            errorId = outputId;
        }

        

#ifdef __MINGW32__
        pid = sawoStartWinProcess(interp, &arg_array[firstArg], save_environ ? save_environ[0] : NULL, inputId, outputId, errorId);
        if (pid == SAWO_BAD_PID) {
            sawo_SetResultFormatted(interp, "couldn't exec \"%s\"", arg_array[firstArg]);
            goto error;
        }
#else
        pid = vfork();
        if (pid < 0) {
            sawo_SetResultErrno(interp, "couldn't fork child process");
            goto error;
        }
        if (pid == 0) {
            

            if (inputId != -1) dup2(inputId, 0);
            if (outputId != -1) dup2(outputId, 1);
            if (errorId != -1) dup2(errorId, 2);

            for (i = 3; (i <= outputId) || (i <= inputId) || (i <= errorId); i++) {
                close(i);
            }

            
            (void)signal(SIGPIPE, SIG_DFL);

            execvpe(arg_array[firstArg], &arg_array[firstArg], sawo_GetEnviron());

            
            fprintf(stderr, "couldn't exec \"%s\"\n", arg_array[firstArg]);
            _exit(127);
        }
#endif

        

        if (table->used == table->size) {
            table->size += WAIT_TABLE_GROW_BY;
            table->show = sawo_Realloc(table->show, table->size * sizeof(*table->show));
        }

        table->show[table->used].pid = pid;
        table->show[table->used].flags = 0;
        table->used++;

        pidPtr[numPids] = pid;

        
        errorId = origErrorId;


        if (inputId != SAWO_BAD_FD) {
            sawoCloseFd(inputId);
        }
        if (outputId != SAWO_BAD_FD) {
            sawoCloseFd(outputId);
        }
        inputId = pipeIds[0];
        pipeIds[0] = pipeIds[1] = SAWO_BAD_FD;
    }
    *pidArrayPtr = pidPtr;


  cleanup:
    if (inputId != SAWO_BAD_FD) {
        sawoCloseFd(inputId);
    }
    if (lastOutputId != SAWO_BAD_FD) {
        sawoCloseFd(lastOutputId);
    }
    if (errorId != SAWO_BAD_FD) {
        sawoCloseFd(errorId);
    }
    sawo_Free(arg_array);

    sawoRestoreEnv(save_environ);

    return numPids;


  error:
    if ((inPipePtr != NULL) && (*inPipePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*inPipePtr);
        *inPipePtr = SAWO_BAD_FD;
    }
    if ((outPipePtr != NULL) && (*outPipePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*outPipePtr);
        *outPipePtr = SAWO_BAD_FD;
    }
    if ((errFilePtr != NULL) && (*errFilePtr != SAWO_BAD_FD)) {
        sawoCloseFd(*errFilePtr);
        *errFilePtr = SAWO_BAD_FD;
    }
    if (pipeIds[0] != SAWO_BAD_FD) {
        sawoCloseFd(pipeIds[0]);
    }
    if (pipeIds[1] != SAWO_BAD_FD) {
        sawoCloseFd(pipeIds[1]);
    }
    if (pidPtr != NULL) {
        for (i = 0; i < numPids; i++) {
            if (pidPtr[i] != SAWO_BAD_PID) {
                sawoDetachPids(interp, 1, &pidPtr[i]);
            }
        }
        sawo_Free(pidPtr);
    }
    numPids = -1;
    goto cleanup;
}


static int sawoCleanupChildren(sawo_Interp *interp, int numPids, pidtype *pidPtr, sawo_Obj *errStrObj)
{
    struct WaitInfoTable *table = sawo_CmdPrivData(interp);
    int result = SAWO_OK;
    int i;

    
    for (i = 0; i < numPids; i++) {
        int waitStatus = 0;
        if (sawoWaitForProcess(table, pidPtr[i], &waitStatus) != SAWO_BAD_PID) {
            if (sawoCheckWaitStatus(interp, pidPtr[i], waitStatus, errStrObj) != SAWO_OK) {
                result = SAWO_ERR;
            }
        }
    }
    sawo_Free(pidPtr);

    return result;
}

int sawo_execInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "exec", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

#ifdef SIGPIPE
    (void)signal(SIGPIPE, SIG_IGN);
#endif

    sawo_CreateCommand(interp, "exec", sawo_ExecCmd, sawoAllocWaitInfoTable(), sawoFreeWaitInfoTable);
    return SAWO_OK;
}

#if defined(__MINGW32__)


static SECURITY_ATTRIBUTES *sawoStdSecAttrs(void)
{
    static SECURITY_ATTRIBUTES secAtts;

    secAtts.nLength = sizeof(SECURITY_ATTRIBUTES);
    secAtts.lpSecurityDescriptor = NULL;
    secAtts.bInheritHandle = TRUE;
    return &secAtts;
}

static int sawoErrno(void)
{
    switch (GetLastError()) {
    case ERROR_FILE_NOT_FOUND: return ENOENT;
    case ERROR_PATH_NOT_FOUND: return ENOENT;
    case ERROR_TOO_MANY_OPEN_FILES: return EMFILE;
    case ERROR_ACCESS_DENIED: return EACCES;
    case ERROR_INVALID_HANDLE: return EBADF;
    case ERROR_BAD_ENVIRONMENT: return E2BIG;
    case ERROR_BAD_FORMAT: return ENOEXEC;
    case ERROR_INVALID_ACCESS: return EACCES;
    case ERROR_INVALID_DRIVE: return ENOENT;
    case ERROR_CURRENT_DIRECTORY: return EACCES;
    case ERROR_NOT_SAME_DEVICE: return EXDEV;
    case ERROR_NO_MORE_FILES: return ENOENT;
    case ERROR_WRITE_PROTECT: return EROFS;
    case ERROR_BAD_UNIT: return ENXIO;
    case ERROR_NOT_READY: return EBUSY;
    case ERROR_BAD_COMMAND: return EIO;
    case ERROR_CRC: return EIO;
    case ERROR_BAD_LENGTH: return EIO;
    case ERROR_SEEK: return EIO;
    case ERROR_WRITE_FAULT: return EIO;
    case ERROR_READ_FAULT: return EIO;
    case ERROR_GEN_FAILURE: return EIO;
    case ERROR_SHARING_VIOLATION: return EACCES;
    case ERROR_LOCK_VIOLATION: return EACCES;
    case ERROR_SHARING_BUFFER_EXCEEDED: return ENFILE;
    case ERROR_HANDLE_DISK_FULL: return ENOSPC;
    case ERROR_NOT_SUPPORTED: return ENODEV;
    case ERROR_REM_NOT_LIST: return EBUSY;
    case ERROR_DUP_NAME: return EEXIST;
    case ERROR_BAD_NETPATH: return ENOENT;
    case ERROR_NETWORK_BUSY: return EBUSY;
    case ERROR_DEV_NOT_EXIST: return ENODEV;
    case ERROR_TOO_MANY_CMDS: return EAGAIN;
    case ERROR_ADAP_HDW_ERR: return EIO;
    case ERROR_BAD_NET_RESP: return EIO;
    case ERROR_UNEXP_NET_ERR: return EIO;
    case ERROR_NETNAME_DELETED: return ENOENT;
    case ERROR_NETWORK_ACCESS_DENIED: return EACCES;
    case ERROR_BAD_DEV_TYPE: return ENODEV;
    case ERROR_BAD_NET_NAME: return ENOENT;
    case ERROR_TOO_MANY_NAMES: return ENFILE;
    case ERROR_TOO_MANY_SESS: return EIO;
    case ERROR_SHARING_PAUSED: return EAGAIN;
    case ERROR_REDIR_PAUSED: return EAGAIN;
    case ERROR_FILE_EXISTS: return EEXIST;
    case ERROR_CANNOT_MAKE: return ENOSPC;
    case ERROR_OUT_OF_STRUCTURES: return ENFILE;
    case ERROR_ALREADY_ASSIGNED: return EEXIST;
    case ERROR_INVALID_PASSWORD: return EPERM;
    case ERROR_NET_WRITE_FAULT: return EIO;
    case ERROR_NO_RAISE_SLOTS: return EAGAIN;
    case ERROR_DISK_CHANGE: return EXDEV;
    case ERROR_BROKEN_PIPE: return EPIPE;
    case ERROR_OPEN_FAILED: return ENOENT;
    case ERROR_DISK_FULL: return ENOSPC;
    case ERROR_NO_MORE_SEARCH_HANDLES: return EMFILE;
    case ERROR_INVALID_TARGET_HANDLE: return EBADF;
    case ERROR_INVALID_NAME: return ENOENT;
    case ERROR_RAISE_NOT_FOUND: return ESRCH;
    case ERROR_WAIT_NO_CHILDREN: return ECHILD;
    case ERROR_CHILD_NOT_COMPLETE: return ECHILD;
    case ERROR_DIRECT_ACCESS_HANDLE: return EBADF;
    case ERROR_SEEK_ON_DEVICE: return ESPIPE;
    case ERROR_BUSY_DRIVE: return EAGAIN;
    case ERROR_DIR_NOT_EMPTY: return EEXIST;
    case ERROR_NOT_LOCKED: return EACCES;
    case ERROR_BAD_PATHNAME: return ENOENT;
    case ERROR_LOCK_FAILED: return EACCES;
    case ERROR_ALREADY_EXISTS: return EEXIST;
    case ERROR_FILENAME_EXCED_RANGE: return ENAMETOOLONG;
    case ERROR_BAD_PIPE: return EPIPE;
    case ERROR_PIPE_BUSY: return EAGAIN;
    case ERROR_PIPE_NOT_CONNECTED: return EPIPE;
    case ERROR_DIRECTORY: return ENOTDIR;
    }
    return EINVAL;
}

static int sawoPipe(fdtype pipefd[2])
{
    if (CreatePipe(&pipefd[0], &pipefd[1], NULL, 0)) {
        return 0;
    }
    return -1;
}

static fdtype sawoDupFd(fdtype infd)
{
    fdtype dupfd;
    pidtype pid = GetCurrentProcess();

    if (DuplicateHandle(pid, infd, pid, &dupfd, 0, TRUE, DUPLICATE_SAME_ACCESS)) {
        return dupfd;
    }
    return SAWO_BAD_FD;
}

static int sawoRewindFd(fdtype fd)
{
    return SetFilePointer(fd, 0, NULL, FILE_BEGIN) == INVALID_SET_FILE_POINTER ? -1 : 0;
}

#if 0
static int sawoReadFd(fdtype fd, char *buffer, size_t len)
{
    DWORD num;

    if (ReadFile(fd, buffer, len, &num, NULL)) {
        return num;
    }
    if (GetLastError() == ERROR_HANDLE_EOF || GetLastError() == ERROR_BROKEN_PIPE) {
        return 0;
    }
    return -1;
}
#endif

static FILE *sawoFdOpenForRead(fdtype fd)
{
    return _fdopen(_open_osfhandle((int)fd, _O_RDONLY | _O_TEXT), "r");
}

static fdtype sawoFileno(FILE *fh)
{
    return (fdtype)_get_osfhandle(_fileno(fh));
}

static fdtype sawoOpenForRead(const char *filename)
{
    return CreateFile(filename, GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE,
        sawoStdSecAttrs(), OPEN_EXISTING, 0, NULL);
}

static fdtype sawoOpenForWrite(const char *filename, int append)
{
    return CreateFile(filename, append ? FILE_APPEND_DATA : GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE,
        sawoStdSecAttrs(), append ? OPEN_ALWAYS : CREATE_ALWAYS, 0, (HANDLE) NULL);
}

static FILE *sawoFdOpenForWrite(fdtype fd)
{
    return _fdopen(_open_osfhandle((int)fd, _O_TEXT), "w");
}

static pidtype sawoWaitPid(pidtype pid, int *status, int nohang)
{
    DWORD ret = WaitForSingleObject(pid, nohang ? 0 : INFINITE);
    if (ret == WAIT_TIMEOUT || ret == WAIT_FAILED) {
        
        return SAWO_BAD_PID;
    }
    GetExitCodeProcess(pid, &ret);
    *status = ret;
    CloseHandle(pid);
    return pid;
}

static HANDLE sawoCreateTemp(sawo_Interp *interp, const char *contents, int len)
{
    char name[MAX_PATH];
    HANDLE handle;

    if (!GetTempPath(MAX_PATH, name) || !GetTempFileName(name, "SAWO", 0, name)) {
        return SAWO_BAD_FD;
    }

    handle = CreateFile(name, GENERIC_READ | GENERIC_WRITE, 0, sawoStdSecAttrs(),
            CREATE_ALWAYS, FILE_ATTRIBUTE_TEMPORARY | FILE_FLAG_DELETE_ON_CLOSE,
            NULL);

    if (handle == INVALID_HANDLE_VALUE) {
        goto error;
    }

    if (contents != NULL) {
        
        FILE *fh = sawoFdOpenForWrite(sawoDupFd(handle));
        if (fh == NULL) {
            goto error;
        }

        if (fwrite(contents, len, 1, fh) != 1) {
            fclose(fh);
            goto error;
        }
        fseek(fh, 0, SEEK_SET);
        fclose(fh);
    }
    return handle;

  error:
    sawo_SetResultErrno(interp, "failed to create temp file");
    CloseHandle(handle);
    DeleteFile(name);
    return SAWO_BAD_FD;
}

static int
sawoWinFindExecutable(const char *originalName, char fullPath[MAX_PATH])
{
    int i;
    static char extensions[][5] = {".exe", "", ".bat"};

    for (i = 0; i < (int) (sizeof(extensions) / sizeof(extensions[0])); i++) {
        snprintf(fullPath, MAX_PATH, "%s%s", originalName, extensions[i]);

        if (SearchPath(NULL, fullPath, NULL, MAX_PATH, fullPath, NULL) == 0) {
            continue;
        }
        if (GetFileAttributes(fullPath) & FILE_ATTRIBUTE_DIRECTORY) {
            continue;
        }
        return 0;
    }

    return -1;
}

static char **sawoSaveEnv(char **env)
{
    return env;
}

static void sawoRestoreEnv(char **env)
{
    sawoFreeEnv(env, sawo_GetEnviron());
}

static sawo_Obj *
sawoWinBuildCommandLine(sawo_Interp *interp, char **argv)
{
    char *start, *special;
    int quote, i;

    sawo_Obj *strObj = sawo_NewStringObj(interp, "", 0);

    for (i = 0; argv[i]; i++) {
        if (i > 0) {
            sawo_AppendString(interp, strObj, " ", 1);
        }

        if (argv[i][0] == '\0') {
            quote = 1;
        }
        else {
            quote = 0;
            for (start = argv[i]; *start != '\0'; start++) {
                if (isspace(UCHAR(*start))) {
                    quote = 1;
                    break;
                }
            }
        }
        if (quote) {
            sawo_AppendString(interp, strObj, "\"" , 1);
        }

        start = argv[i];
        for (special = argv[i]; ; ) {
            if ((*special == '\\') && (special[1] == '\\' ||
                    special[1] == '"' || (quote && special[1] == '\0'))) {
                sawo_AppendString(interp, strObj, start, special - start);
                start = special;
                while (1) {
                    special++;
                    if (*special == '"' || (quote && *special == '\0')) {

                        sawo_AppendString(interp, strObj, start, special - start);
                        break;
                    }
                    if (*special != '\\') {
                        break;
                    }
                }
                sawo_AppendString(interp, strObj, start, special - start);
                start = special;
            }
            if (*special == '"') {
        if (special == start) {
            sawo_AppendString(interp, strObj, "\"", 1);
        }
        else {
            sawo_AppendString(interp, strObj, start, special - start);
        }
                sawo_AppendString(interp, strObj, "\\\"", 2);
                start = special + 1;
            }
            if (*special == '\0') {
                break;
            }
            special++;
        }
        sawo_AppendString(interp, strObj, start, special - start);
        if (quote) {
            sawo_AppendString(interp, strObj, "\"", 1);
        }
    }
    return strObj;
}

static pidtype
sawoStartWinProcess(sawo_Interp *interp, char **argv, char *env, fdtype inputId, fdtype outputId, fdtype errorId)
{
    STARTUPINFO startInfo;
    PROCESS_INFORMATION procInfo;
    HANDLE hProcess, h;
    char execPath[MAX_PATH];
    pidtype pid = SAWO_BAD_PID;
    sawo_Obj *cmdLineObj;

    if (sawoWinFindExecutable(argv[0], execPath) < 0) {
        return SAWO_BAD_PID;
    }
    argv[0] = execPath;

    hProcess = GetCurrentProcess();
    cmdLineObj = sawoWinBuildCommandLine(interp, argv);


    ZeroMemory(&startInfo, sizeof(startInfo));
    startInfo.cb = sizeof(startInfo);
    startInfo.dwFlags   = STARTF_USESTDHANDLES;
    startInfo.hStdInput = INVALID_HANDLE_VALUE;
    startInfo.hStdOutput= INVALID_HANDLE_VALUE;
    startInfo.hStdError = INVALID_HANDLE_VALUE;

    if (inputId == SAWO_BAD_FD) {
        if (CreatePipe(&startInfo.hStdInput, &h, sawoStdSecAttrs(), 0) != FALSE) {
            CloseHandle(h);
        }
    } else {
        DuplicateHandle(hProcess, inputId, hProcess, &startInfo.hStdInput,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdInput == SAWO_BAD_FD) {
        goto end;
    }

    if (outputId == SAWO_BAD_FD) {
        startInfo.hStdOutput = CreateFile("NUL:", GENERIC_WRITE, 0,
                sawoStdSecAttrs(), OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    } else {
        DuplicateHandle(hProcess, outputId, hProcess, &startInfo.hStdOutput,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdOutput == SAWO_BAD_FD) {
        goto end;
    }

    if (errorId == SAWO_BAD_FD) {

        startInfo.hStdError = CreateFile("NUL:", GENERIC_WRITE, 0,
                sawoStdSecAttrs(), OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    } else {
        DuplicateHandle(hProcess, errorId, hProcess, &startInfo.hStdError,
                0, TRUE, DUPLICATE_SAME_ACCESS);
    }
    if (startInfo.hStdError == SAWO_BAD_FD) {
        goto end;
    }

    if (!CreateProcess(NULL, (char *)sawo_String(cmdLineObj), NULL, NULL, TRUE,
            0, env, NULL, &startInfo, &procInfo)) {
        goto end;
    }


    WaitForInputIdle(procInfo.hProcess, 5000);
    CloseHandle(procInfo.hThread);

    pid = procInfo.hProcess;

    end:
    sawo_FreeNewObj(interp, cmdLineObj);
    if (startInfo.hStdInput != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdInput);
    }
    if (startInfo.hStdOutput != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdOutput);
    }
    if (startInfo.hStdError != SAWO_BAD_FD) {
        CloseHandle(startInfo.hStdError);
    }
    return pid;
}
#else

static int sawoOpenForWrite(const char *filename, int append)
{
    return open(filename, O_WRONLY | O_CREAT | (append ? O_APPEND : O_TRUNC), 0666);
}

static int sawoRewindFd(int fd)
{
    return lseek(fd, 0L, SEEK_SET);
}

static int sawoCreateTemp(sawo_Interp *interp, const char *contents, int len)
{
    int fd = sawo_MakeTempFile(interp, NULL);

    if (fd != SAWO_BAD_FD) {
        unlink(sawo_String(sawo_GetResult(interp)));
        if (contents) {
            if (write(fd, contents, len) != len) {
                sawo_SetResultErrno(interp, "couldn't write temp file");
                close(fd);
                return -1;
            }
            lseek(fd, 0L, SEEK_SET);
        }
    }
    return fd;
}

static char **sawoSaveEnv(char **env)
{
    char **saveenv = sawo_GetEnviron();
    sawo_SetEnviron(env);
    return saveenv;
}

static void sawoRestoreEnv(char **env)
{
    sawoFreeEnv(sawo_GetEnviron(), env);
    sawo_SetEnviron(env);
}
#endif
#endif


#ifndef _XOPEN_SOURCE
#define _XOPEN_SOURCE 500
#endif

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>


#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif

static int clock_cmd_format(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    
    char buf[100];
    time_t t;
    long seconds;

    const char *format = "%a %b %d %H:%M:%S %Z %Y";

    if (argc == 2 || (argc == 3 && !sawo_CompareStringImmediate(interp, argv[1], "-format"))) {
        return -1;
    }

    if (argc == 3) {
        format = sawo_String(argv[2]);
    }

    if (sawo_GetLong(interp, argv[0], &seconds) != SAWO_OK) {
        return SAWO_ERR;
    }
    t = seconds;

    if (strftime(buf, sizeof(buf), format, localtime(&t)) == 0) {
        sawo_SetResultString(interp, "format string too long", -1);
        return SAWO_ERR;
    }

    sawo_SetResultString(interp, buf, -1);

    return SAWO_OK;
}

#ifdef HAVE_STRPTIME
static int clock_cmd_scan(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    char *pt;
    struct tm tm;
    time_t now = time(0);

    if (!sawo_CompareStringImmediate(interp, argv[1], "-format")) {
        return -1;
    }

    
    localtime_r(&now, &tm);

    pt = strptime(sawo_String(argv[0]), sawo_String(argv[2]), &tm);
    if (pt == 0 || *pt != 0) {
        sawo_SetResultString(interp, "Failed to parse time according to format", -1);
        return SAWO_ERR;
    }

    
    sawo_SetResultInt(interp, mktime(&tm));

    return SAWO_OK;
}
#endif

static int clock_cmd_seconds(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_SetResultInt(interp, time(NULL));

    return SAWO_OK;
}

static int clock_cmd_micros(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    sawo_SetResultInt(interp, (sawo_wide) tv.tv_sec * 1000000 + tv.tv_usec);

    return SAWO_OK;
}

static int clock_cmd_millis(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);

    sawo_SetResultInt(interp, (sawo_wide) tv.tv_sec * 1000 + tv.tv_usec / 1000);

    return SAWO_OK;
}

static const sawo_subcmd_type clock_command_table[] = {
    {   "seconds",
        NULL,
        clock_cmd_seconds,
        0,
        0,
        
    },
    {   "clicks",
        NULL,
        clock_cmd_micros,
        0,
        0,
        
    },
    {   "microseconds",
        NULL,
        clock_cmd_micros,
        0,
        0,
        
    },
    {   "milliseconds",
        NULL,
        clock_cmd_millis,
        0,
        0,
        
    },
    {   "format",
        "seconds ?-format format?",
        clock_cmd_format,
        1,
        3,
        
    },
#ifdef HAVE_STRPTIME
    {   "scan",
        "str -format format",
        clock_cmd_scan,
        3,
        3,
        
    },
#endif
    { NULL }
};

int sawo_clockInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "clock", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "clock", sawo_SubCmdRaise, (void *)clock_command_table, NULL);
    return SAWO_OK;
}

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>


static int array_cmd_exists(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    
    sawo_SetResultInt(interp, sawo_GetVariable(interp, argv[0], 0) != 0);
    return SAWO_OK;
}

static int array_cmd_get(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    sawo_Obj *patternObj;

    if (!objPtr) {
        return SAWO_OK;
    }

    patternObj = (argc == 1) ? NULL : argv[1];

    
    if (patternObj == NULL || sawo_CompareStringImmediate(interp, patternObj, "*")) {
        if (sawo_IsList(objPtr) && sawo_ListLength(interp, objPtr) % 2 == 0) {
            
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;
        }
    }

    
    return sawo_DictValues(interp, objPtr, patternObj);
}

static int array_cmd_names(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);

    if (!objPtr) {
        return SAWO_OK;
    }

    return sawo_DictKeys(interp, objPtr, argc == 1 ? NULL : argv[1]);
}

static int array_cmd_unset(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    int len;
    sawo_Obj *resultObj;
    sawo_Obj *objPtr;
    sawo_Obj **dictValuesObj;

    if (argc == 1 || sawo_CompareStringImmediate(interp, argv[1], "*")) {
        
        sawo_UnsetVariable(interp, argv[0], SAWO_NONE);
        return SAWO_OK;
    }

    objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);

    if (objPtr == NULL) {
        
        return SAWO_OK;
    }

    if (sawo_DictPairs(interp, objPtr, &dictValuesObj, &len) != SAWO_OK) {
        return SAWO_ERR;
    }

    
    resultObj = sawo_NewDictObj(interp, NULL, 0);

    for (i = 0; i < len; i += 2) {
        if (!sawo_StringMatchObj(interp, argv[1], dictValuesObj[i], 0)) {
            sawo_DictAddElement(interp, resultObj, dictValuesObj[i], dictValuesObj[i + 1]);
        }
    }
    sawo_Free(dictValuesObj);

    sawo_SetVariable(interp, argv[0], resultObj);
    return SAWO_OK;
}

static int array_cmd_size(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;
    int len = 0;

    
    objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    if (objPtr) {
        len = sawo_DictSize(interp, objPtr);
        if (len < 0) {
            return SAWO_ERR;
        }
    }

    sawo_SetResultInt(interp, len);

    return SAWO_OK;
}

static int array_cmd_stat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    if (objPtr) {
        return sawo_DictInfo(interp, objPtr);
    }
    sawo_SetResultFormatted(interp, "\"%#s\" isn't an array", argv[0], NULL);
    return SAWO_ERR;
}

static int array_cmd_set(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    int len;
    sawo_Obj *listObj = argv[1];
    sawo_Obj *dictObj;

    len = sawo_ListLength(interp, listObj);
    if (len % 2) {
        sawo_SetResultString(interp, "list must have an even number of elements", -1);
        return SAWO_ERR;
    }

    dictObj = sawo_GetVariable(interp, argv[0], SAWO_UNSHARED);
    if (!dictObj) {
        
        return sawo_SetVariable(interp, argv[0], listObj);
    }
    else if (sawo_DictSize(interp, dictObj) < 0) {
        return SAWO_ERR;
    }

    if (sawo_IsShared(dictObj)) {
        dictObj = sawo_DuplicateObj(interp, dictObj);
    }

    for (i = 0; i < len; i += 2) {
        sawo_Obj *nameObj;
        sawo_Obj *valueObj;

        sawo_ListIndex(interp, listObj, i, &nameObj, SAWO_NONE);
        sawo_ListIndex(interp, listObj, i + 1, &valueObj, SAWO_NONE);

        sawo_DictAddElement(interp, dictObj, nameObj, valueObj);
    }
    return sawo_SetVariable(interp, argv[0], dictObj);
}

static const sawo_subcmd_type array_command_table[] = {
        {       "exists",
                "arrayName",
                array_cmd_exists,
                1,
                1,
                
        },
        {       "get",
                "arrayName ?pattern?",
                array_cmd_get,
                1,
                2,
                
        },
        {       "names",
                "arrayName ?pattern?",
                array_cmd_names,
                1,
                2,
                
        },
        {       "set",
                "arrayName list",
                array_cmd_set,
                2,
                2,
                
        },
        {       "size",
                "arrayName",
                array_cmd_size,
                1,
                1,
                
        },
        {       "stat",
                "arrayName",
                array_cmd_stat,
                1,
                1,
                
        },
        {       "unset",
                "arrayName ?pattern?",
                array_cmd_unset,
                1,
                2,
                
        },
        {       NULL
        }
};

int sawo_arrayInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "array", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "array", sawo_SubCmdRaise, (void *)array_command_table, NULL);
    return SAWO_OK;
}
int sawo_InitStaticExtensions(sawo_Interp *interp)
{
extern int sawo_bootstrapInit(sawo_Interp *);
extern int sawo_aioInit(sawo_Interp *);
extern int sawo_readdirInit(sawo_Interp *);
extern int sawo_regexpInit(sawo_Interp *);
extern int sawo_fileInit(sawo_Interp *);
extern int sawo_globInit(sawo_Interp *);
extern int sawo_execInit(sawo_Interp *);
extern int sawo_clockInit(sawo_Interp *);
extern int sawo_arrayInit(sawo_Interp *);
extern int sawo_stdlibInit(sawo_Interp *);
extern int sawo_hyangcompatInit(sawo_Interp *);
sawo_bootstrapInit(interp);
sawo_aioInit(interp);
sawo_readdirInit(interp);
sawo_regexpInit(interp);
sawo_fileInit(interp);
sawo_globInit(interp);
sawo_execInit(interp);
sawo_clockInit(interp);
sawo_arrayInit(interp);
sawo_stdlibInit(interp);
sawo_hyangcompatInit(interp);
return SAWO_OK;
}
#define SAWO_OPTIMIZATION        

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <setjmp.h>


#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_BACKTRACE
#include <execinfo.h>
#endif
#ifdef HAVE_CRT_EXTERNS_H
#include <crt_externs.h>
#endif


#include <math.h>





#ifndef HYANG_LIBRARY
#define HYANG_LIBRARY "."
#endif
#ifndef HYANG_PLATFORM_OS
#define HYANG_PLATFORM_OS "unknown"
#endif
#ifndef HYANG_PLATFORM_PLATFORM
#define HYANG_PLATFORM_PLATFORM "unknown"
#endif
#ifndef HYANG_PLATFORM_PATH_SEPARATOR
#define HYANG_PLATFORM_PATH_SEPARATOR ":"
#endif







#ifdef SAWO_MAINTAINER
#define SAWO_DEBUG_COMMAND
#define SAWO_DEBUG_PANIC
#endif



#define SAWO_INTEGER_SPACE 24

const char *sawo_tt_name(int type);

#ifdef SAWO_DEBUG_PANIC
static void sawoPanicDump(int fail_condition, const char *fmt, ...);
#define sawoPanic(X) sawoPanicDump X
#else
#define sawoPanic(X)
#endif


static char sawoEmptyStringRep[] = "";

static void sawoFreeCallFrame(sawo_Interp *interp, sawo_CallFrame *cf, int action);
static int ListSetIndex(sawo_Interp *interp, sawo_Obj *listPtr, int listindex, sawo_Obj *newObjPtr,
    int flags);
static int sawoDeleteLocalRaises(sawo_Interp *interp, sawo_Stack *localCommands);
static sawo_Obj *sawoExpandDictSugar(sawo_Interp *interp, sawo_Obj *objPtr);
static void SetDictSubstFromAny(sawo_Interp *interp, sawo_Obj *objPtr);
static sawo_Obj **sawoDictPairs(sawo_Obj *dictPtr, int *len);
static void sawoSetFailedEnumResult(sawo_Interp *interp, const char *arg, const char *badtype,
    const char *prefix, const char *const *tablePtr, const char *name);
static int sawoCallProcedure(sawo_Interp *interp, sawo_Cmd *cmd, int argc, sawo_Obj *const *argv);
static int sawoGetWideNoErr(sawo_Interp *interp, sawo_Obj *objPtr, sawo_wide * widePtr);
static int sawoSign(sawo_wide w);
static int sawoValidName(sawo_Interp *interp, const char *type, sawo_Obj *nameObjPtr);
static void sawoPrngSeed(sawo_Interp *interp, unsigned char *seed, int seedLen);
static void sawoRandomBytes(sawo_Interp *interp, void *dest, unsigned int len);



#define sawoWideValue(objPtr) (objPtr)->internalRep.wideValue

#define sawoObjTypeName(O) ((O)->typePtr ? (O)->typePtr->name : "none")

static int utf8_tounicode_case(const char *s, int *uc, int upper)
{
    int l = utf8_tounicode(s, uc);
    if (upper) {
        *uc = utf8_upper(*uc);
    }
    return l;
}


#define SAWO_CHARSET_SCAN 2
#define SAWO_CHARSET_GLOB 0

static const char *sawoCharsetMatch(const char *pattern, int c, int flags)
{
    int not = 0;
    int pchar;
    int match = 0;
    int nocase = 0;

    if (flags & SAWO_NOCASE) {
        nocase++;
        c = utf8_upper(c);
    }

    if (flags & SAWO_CHARSET_SCAN) {
        if (*pattern == '^') {
            not++;
            pattern++;
        }

        
        if (*pattern == ']') {
            goto first;
        }
    }

    while (*pattern && *pattern != ']') {
        
        if (pattern[0] == '\\') {
first:
            pattern += utf8_tounicode_case(pattern, &pchar, nocase);
        }
        else {
            
            int start;
            int end;

            pattern += utf8_tounicode_case(pattern, &start, nocase);
            if (pattern[0] == '-' && pattern[1]) {
                
                pattern += utf8_tounicode(pattern, &pchar);
                pattern += utf8_tounicode_case(pattern, &end, nocase);

                
                if ((c >= start && c <= end) || (c >= end && c <= start)) {
                    match = 1;
                }
                continue;
            }
            pchar = start;
        }

        if (pchar == c) {
            match = 1;
        }
    }
    if (not) {
        match = !match;
    }

    return match ? pattern : NULL;
}



static int sawoGlobMatch(const char *pattern, const char *string, int nocase)
{
    int c;
    int pchar;
    while (*pattern) {
        switch (pattern[0]) {
            case '*':
                while (pattern[1] == '*') {
                    pattern++;
                }
                pattern++;
                if (!pattern[0]) {
                    return 1;   
                }
                while (*string) {
                    
                    if (sawoGlobMatch(pattern, string, nocase))
                        return 1;       
                    string += utf8_tounicode(string, &c);
                }
                return 0;       

            case '?':
                string += utf8_tounicode(string, &c);
                break;

            case '[': {
                    string += utf8_tounicode(string, &c);
                    pattern = sawoCharsetMatch(pattern + 1, c, nocase ? SAWO_NOCASE : 0);
                    if (!pattern) {
                        return 0;
                    }
                    if (!*pattern) {
                        
                        continue;
                    }
                    break;
                }
            case '\\':
                if (pattern[1]) {
                    pattern++;
                }
                
            default:
                string += utf8_tounicode_case(string, &c, nocase);
                utf8_tounicode_case(pattern, &pchar, nocase);
                if (pchar != c) {
                    return 0;
                }
                break;
        }
        pattern += utf8_tounicode_case(pattern, &pchar, nocase);
        if (!*string) {
            while (*pattern == '*') {
                pattern++;
            }
            break;
        }
    }
    if (!*pattern && !*string) {
        return 1;
    }
    return 0;
}

static int sawoStringCompare(const char *s1, int l1, const char *s2, int l2)
{
    if (l1 < l2) {
        return memcmp(s1, s2, l1) <= 0 ? -1 : 1;
    }
    else if (l2 < l1) {
        return memcmp(s1, s2, l2) >= 0 ? 1 : -1;
    }
    else {
        return sawoSign(memcmp(s1, s2, l1));
    }
}

static int sawoStringCompareLen(const char *s1, const char *s2, int maxchars, int nocase)
{
    while (*s1 && *s2 && maxchars) {
        int c1, c2;
        s1 += utf8_tounicode_case(s1, &c1, nocase);
        s2 += utf8_tounicode_case(s2, &c2, nocase);
        if (c1 != c2) {
            return sawoSign(c1 - c2);
        }
        maxchars--;
    }
    if (!maxchars) {
        return 0;
    }
    
    if (*s1) {
        return 1;
    }
    if (*s2) {
        return -1;
    }
    return 0;
}

static int sawoStringFirst(const char *s1, int l1, const char *s2, int l2, int idx)
{
    int i;
    int l1bytelen;

    if (!l1 || !l2 || l1 > l2) {
        return -1;
    }
    if (idx < 0)
        idx = 0;
    s2 += utf8_index(s2, idx);

    l1bytelen = utf8_index(s1, l1);

    for (i = idx; i <= l2 - l1; i++) {
        int c;
        if (memcmp(s2, s1, l1bytelen) == 0) {
            return i;
        }
        s2 += utf8_tounicode(s2, &c);
    }
    return -1;
}

static int sawoStringLast(const char *s1, int l1, const char *s2, int l2)
{
    const char *p;

    if (!l1 || !l2 || l1 > l2)
        return -1;

    
    for (p = s2 + l2 - 1; p != s2 - 1; p--) {
        if (*p == *s1 && memcmp(s1, p, l1) == 0) {
            return p - s2;
        }
    }
    return -1;
}

#ifdef SAWO_UTF8
static int sawoStringLastUtf8(const char *s1, int l1, const char *s2, int l2)
{
    int n = sawoStringLast(s1, utf8_index(s1, l1), s2, utf8_index(s2, l2));
    if (n > 0) {
        n = utf8_strlen(s2, n);
    }
    return n;
}
#endif

static int sawoCheckConversion(const char *str, const char *endptr)
{
    if (str[0] == '\0' || str == endptr) {
        return SAWO_ERR;
    }

    if (endptr[0] != '\0') {
        while (*endptr) {
            if (!isspace(UCHAR(*endptr))) {
                return SAWO_ERR;
            }
            endptr++;
        }
    }
    return SAWO_OK;
}

static int sawoNumberBase(const char *str, int *base, int *sign)
{
    int i = 0;

    *base = 10;

    while (isspace(UCHAR(str[i]))) {
        i++;
    }

    if (str[i] == '-') {
        *sign = -1;
        i++;
    }
    else {
        if (str[i] == '+') {
            i++;
        }
        *sign = 1;
    }

    if (str[i] != '0') {
        
        return 0;
    }

    
    switch (str[i + 1]) {
        case 'x': case 'X': *base = 16; break;
        case 'o': case 'O': *base = 8; break;
        case 'b': case 'B': *base = 2; break;
        default: return 0;
    }
    i += 2;
    
    if (str[i] != '-' && str[i] != '+' && !isspace(UCHAR(str[i]))) {
        
        return i;
    }
    
    *base = 10;
    return 0;
}

static long sawo_strtol(const char *str, char **endptr)
{
    int sign;
    int base;
    int i = sawoNumberBase(str, &base, &sign);

    if (base != 10) {
        long value = strtol(str + i, endptr, base);
        if (endptr == NULL || *endptr != str + i) {
            return value * sign;
        }
    }

    
    return strtol(str, endptr, 10);
}


static sawo_wide sawo_strtoull(const char *str, char **endptr)
{
#ifdef HAVE_LONG_LONG
    int sign;
    int base;
    int i = sawoNumberBase(str, &base, &sign);

    if (base != 10) {
        sawo_wide value = strtoull(str + i, endptr, base);
        if (endptr == NULL || *endptr != str + i) {
            return value * sign;
        }
    }

    
    return strtoull(str, endptr, 10);
#else
    return (unsigned long)sawo_strtol(str, endptr);
#endif
}

int sawo_StringToWide(const char *str, sawo_wide * widePtr, int base)
{
    char *endptr;

    if (base) {
        *widePtr = strtoull(str, &endptr, base);
    }
    else {
        *widePtr = sawo_strtoull(str, &endptr);
    }

    return sawoCheckConversion(str, endptr);
}

int sawo_StringToDouble(const char *str, double *doublePtr)
{
    char *endptr;

    
    errno = 0;

    *doublePtr = strtod(str, &endptr);

    return sawoCheckConversion(str, endptr);
}

static sawo_wide sawoPowWide(sawo_wide b, sawo_wide e)
{
    sawo_wide i, res = 1;

    if ((b == 0 && e != 0) || (e < 0))
        return 0;
    for (i = 0; i < e; i++) {
        res *= b;
    }
    return res;
}

#ifdef SAWO_DEBUG_PANIC
static void sawoPanicDump(int condition, const char *fmt, ...)
{
    va_list ap;

    if (!condition) {
        return;
    }

    va_start(ap, fmt);

    fprintf(stderr, "\nSAWO INTERPRETER PANIC: ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n\n");
    va_end(ap);

#ifdef HAVE_BACKTRACE
    {
        void *array[40];
        int size, i;
        char **strings;

        size = backtrace(array, 40);
        strings = backtrace_symbols(array, size);
        for (i = 0; i < size; i++)
            fprintf(stderr, "[backtrace] %s\n", strings[i]);
        fprintf(stderr, "[backtrace] Include the above lines and the output\n");
        fprintf(stderr, "[backtrace] of 'nm <executable>' in the bug report.\n");
    }
#endif

    exit(1);
}
#endif


void *sawo_Alloc(int size)
{
    return size ? malloc(size) : NULL;
}

void sawo_Free(void *ptr)
{
    free(ptr);
}

void *sawo_Realloc(void *ptr, int size)
{
    return realloc(ptr, size);
}

char *sawo_StrDup(const char *s)
{
    return strdup(s);
}

char *sawo_StrDupLen(const char *s, int l)
{
    char *copy = sawo_Alloc(l + 1);

    memcpy(copy, s, l + 1);
    copy[l] = 0;                
    return copy;
}



static sawo_wide sawoClock(void)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return (sawo_wide) tv.tv_sec * 1000000 + tv.tv_usec;
}



static void sawoExpandHashTableIfNeeded(sawo_HashTable *ht);
static unsigned int sawoHashTableNextPower(unsigned int size);
static sawo_HashEntry *sawoInsertHashEntry(sawo_HashTable *ht, const void *key, int replace);




unsigned int sawo_IntHashFunction(unsigned int key)
{
    key += ~(key << 15);
    key ^= (key >> 10);
    key += (key << 3);
    key ^= (key >> 6);
    key += ~(key << 11);
    key ^= (key >> 16);
    return key;
}

unsigned int sawo_GenHashFunction(const unsigned char *buf, int len)
{
    unsigned int h = 0;

    while (len--)
        h += (h << 3) + *buf++;
    return h;
}




static void sawoResetHashTable(sawo_HashTable *ht)
{
    ht->table = NULL;
    ht->size = 0;
    ht->sizemask = 0;
    ht->used = 0;
    ht->collisions = 0;
#ifdef SAWO_RANDOMISE_HASH
    ht->uniq = (rand() ^ time(NULL) ^ clock());
#else
    ht->uniq = 0;
#endif
}

static void sawoInitHashTableIterator(sawo_HashTable *ht, sawo_HashTableIterator *iter)
{
    iter->ht = ht;
    iter->index = -1;
    iter->entry = NULL;
    iter->nextEntry = NULL;
}


int sawo_InitHashTable(sawo_HashTable *ht, const sawo_HashTableType *type, void *privDataPtr)
{
    sawoResetHashTable(ht);
    ht->type = type;
    ht->privdata = privDataPtr;
    return SAWO_OK;
}

void sawo_ResizeHashTable(sawo_HashTable *ht)
{
    int minimal = ht->used;

    if (minimal < SAWO_HT_INITIAL_SIZE)
        minimal = SAWO_HT_INITIAL_SIZE;
    sawo_ExpandHashTable(ht, minimal);
}


void sawo_ExpandHashTable(sawo_HashTable *ht, unsigned int size)
{
    sawo_HashTable n;            
    unsigned int realsize = sawoHashTableNextPower(size), i;

     if (size <= ht->used)
        return;

    sawo_InitHashTable(&n, ht->type, ht->privdata);
    n.size = realsize;
    n.sizemask = realsize - 1;
    n.table = sawo_Alloc(realsize * sizeof(sawo_HashEntry *));
    
    n.uniq = ht->uniq;

    
    memset(n.table, 0, realsize * sizeof(sawo_HashEntry *));

    n.used = ht->used;
    for (i = 0; ht->used > 0; i++) {
        sawo_HashEntry *he, *nextHe;

        if (ht->table[i] == NULL)
            continue;

        
        he = ht->table[i];
        while (he) {
            unsigned int h;

            nextHe = he->next;
            
            h = sawo_HashKey(ht, he->key) & n.sizemask;
            he->next = n.table[h];
            n.table[h] = he;
            ht->used--;
            
            he = nextHe;
        }
    }
    assert(ht->used == 0);
    sawo_Free(ht->table);

    
    *ht = n;
}


int sawo_AddHashEntry(sawo_HashTable *ht, const void *key, void *val)
{
    sawo_HashEntry *entry;

    entry = sawoInsertHashEntry(ht, key, 0);
    if (entry == NULL)
        return SAWO_ERR;

    
    sawo_SetHashKey(ht, entry, key);
    sawo_SetHashVal(ht, entry, val);
    return SAWO_OK;
}


int sawo_ReplaceHashEntry(sawo_HashTable *ht, const void *key, void *val)
{
    int existed;
    sawo_HashEntry *entry;

    entry = sawoInsertHashEntry(ht, key, 1);
    if (entry->key) {
        if (ht->type->valDestructor && ht->type->valDup) {
            void *newval = ht->type->valDup(ht->privdata, val);
            ht->type->valDestructor(ht->privdata, entry->u.val);
            entry->u.val = newval;
        }
        else {
            sawo_FreeEntryVal(ht, entry);
            sawo_SetHashVal(ht, entry, val);
        }
        existed = 1;
    }
    else {
        
        sawo_SetHashKey(ht, entry, key);
        sawo_SetHashVal(ht, entry, val);
        existed = 0;
    }

    return existed;
}


int sawo_DeleteHashEntry(sawo_HashTable *ht, const void *key)
{
    unsigned int h;
    sawo_HashEntry *he, *prevHe;

    if (ht->used == 0)
        return SAWO_ERR;
    h = sawo_HashKey(ht, key) & ht->sizemask;
    he = ht->table[h];

    prevHe = NULL;
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key)) {
            
            if (prevHe)
                prevHe->next = he->next;
            else
                ht->table[h] = he->next;
            sawo_FreeEntryKey(ht, he);
            sawo_FreeEntryVal(ht, he);
            sawo_Free(he);
            ht->used--;
            return SAWO_OK;
        }
        prevHe = he;
        he = he->next;
    }
    return SAWO_ERR;             
}


int sawo_FreeHashTable(sawo_HashTable *ht)
{
    unsigned int i;

    
    for (i = 0; ht->used > 0; i++) {
        sawo_HashEntry *he, *nextHe;

        if ((he = ht->table[i]) == NULL)
            continue;
        while (he) {
            nextHe = he->next;
            sawo_FreeEntryKey(ht, he);
            sawo_FreeEntryVal(ht, he);
            sawo_Free(he);
            ht->used--;
            he = nextHe;
        }
    }
    
    sawo_Free(ht->table);
    
    sawoResetHashTable(ht);
    return SAWO_OK;              
}

sawo_HashEntry *sawo_FindHashEntry(sawo_HashTable *ht, const void *key)
{
    sawo_HashEntry *he;
    unsigned int h;

    if (ht->used == 0)
        return NULL;
    h = sawo_HashKey(ht, key) & ht->sizemask;
    he = ht->table[h];
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key))
            return he;
        he = he->next;
    }
    return NULL;
}

sawo_HashTableIterator *sawo_GetHashTableIterator(sawo_HashTable *ht)
{
    sawo_HashTableIterator *iter = sawo_Alloc(sizeof(*iter));
    sawoInitHashTableIterator(ht, iter);
    return iter;
}

sawo_HashEntry *sawo_NextHashEntry(sawo_HashTableIterator *iter)
{
    while (1) {
        if (iter->entry == NULL) {
            iter->index++;
            if (iter->index >= (signed)iter->ht->size)
                break;
            iter->entry = iter->ht->table[iter->index];
        }
        else {
            iter->entry = iter->nextEntry;
        }
        if (iter->entry) {
            iter->nextEntry = iter->entry->next;
            return iter->entry;
        }
    }
    return NULL;
}




static void sawoExpandHashTableIfNeeded(sawo_HashTable *ht)
{
    if (ht->size == 0)
        sawo_ExpandHashTable(ht, SAWO_HT_INITIAL_SIZE);
    if (ht->size == ht->used)
        sawo_ExpandHashTable(ht, ht->size * 2);
}


static unsigned int sawoHashTableNextPower(unsigned int size)
{
    unsigned int i = SAWO_HT_INITIAL_SIZE;

    if (size >= 2147483648U)
        return 2147483648U;
    while (1) {
        if (i >= size)
            return i;
        i *= 2;
    }
}

static sawo_HashEntry *sawoInsertHashEntry(sawo_HashTable *ht, const void *key, int replace)
{
    unsigned int h;
    sawo_HashEntry *he;

    
    sawoExpandHashTableIfNeeded(ht);

    
    h = sawo_HashKey(ht, key) & ht->sizemask;
    
    he = ht->table[h];
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key))
            return replace ? he : NULL;
        he = he->next;
    }

    
    he = sawo_Alloc(sizeof(*he));
    he->next = ht->table[h];
    ht->table[h] = he;
    ht->used++;
    he->key = NULL;

    return he;
}



static unsigned int sawoStringCopyHTHashFunction(const void *key)
{
    return sawo_GenHashFunction(key, strlen(key));
}

static void *sawoStringCopyHTDup(void *privdata, const void *key)
{
    return sawo_StrDup(key);
}

static int sawoStringCopyHTKeyCompare(void *privdata, const void *key1, const void *key2)
{
    return strcmp(key1, key2) == 0;
}

static void sawoStringCopyHTKeyDestructor(void *privdata, void *key)
{
    sawo_Free(key);
}

static const sawo_HashTableType sawoPackageHashTableType = {
    sawoStringCopyHTHashFunction,     
    sawoStringCopyHTDup,              
    NULL,                            
    sawoStringCopyHTKeyCompare,       
    sawoStringCopyHTKeyDestructor,    
    NULL                             
};

typedef struct AssocDataValue
{
    sawo_InterpDeleteRaise *delRaise;
    void *data;
} AssocDataValue;

static void sawoAssocDataHashTableValueDestructor(void *privdata, void *data)
{
    AssocDataValue *assocPtr = (AssocDataValue *) data;

    if (assocPtr->delRaise != NULL)
        assocPtr->delRaise((sawo_Interp *)privdata, assocPtr->data);
    sawo_Free(data);
}

static const sawo_HashTableType sawoAssocDataHashTableType = {
    sawoStringCopyHTHashFunction,    
    sawoStringCopyHTDup,             
    NULL,                           
    sawoStringCopyHTKeyCompare,      
    sawoStringCopyHTKeyDestructor,   
    sawoAssocDataHashTableValueDestructor        
};

void sawo_InitStack(sawo_Stack *stack)
{
    stack->len = 0;
    stack->maxlen = 0;
    stack->vector = NULL;
}

void sawo_FreeStack(sawo_Stack *stack)
{
    sawo_Free(stack->vector);
}

int sawo_StackLen(sawo_Stack *stack)
{
    return stack->len;
}

void sawo_StackPush(sawo_Stack *stack, void *element)
{
    int neededLen = stack->len + 1;

    if (neededLen > stack->maxlen) {
        stack->maxlen = neededLen < 20 ? 20 : neededLen * 2;
        stack->vector = sawo_Realloc(stack->vector, sizeof(void *) * stack->maxlen);
    }
    stack->vector[stack->len] = element;
    stack->len++;
}

void *sawo_StackPop(sawo_Stack *stack)
{
    if (stack->len == 0)
        return NULL;
    stack->len--;
    return stack->vector[stack->len];
}

void *sawo_StackPeek(sawo_Stack *stack)
{
    if (stack->len == 0)
        return NULL;
    return stack->vector[stack->len - 1];
}

void sawo_FreeStackElements(sawo_Stack *stack, void (*freeFunc) (void *ptr))
{
    int i;

    for (i = 0; i < stack->len; i++)
        freeFunc(stack->vector[i]);
}



#define SAWO_TT_NONE    0          
#define SAWO_TT_STR     1          
#define SAWO_TT_ESC     2          
#define SAWO_TT_VAR     3          
#define SAWO_TT_DICTSUGAR   4      
#define SAWO_TT_CMD     5          

#define SAWO_TT_SEP     6          
#define SAWO_TT_EOL     7          
#define SAWO_TT_EOF     8          

#define SAWO_TT_LINE    9          
#define SAWO_TT_WORD   10          


#define SAWO_TT_SUBEXPR_START  11
#define SAWO_TT_SUBEXPR_END    12
#define SAWO_TT_SUBEXPR_COMMA  13
#define SAWO_TT_EXPR_INT       14
#define SAWO_TT_EXPR_DOUBLE    15

#define SAWO_TT_EXPRSUGAR      16  


#define SAWO_TT_EXPR_OP        20

#define TOKEN_IS_SEP(type) (type >= SAWO_TT_SEP && type <= SAWO_TT_EOF)

struct sawoParseMissing {
    int ch;             
    int line;           
};

struct sawoParserCtx
{
    const char *p;              
    int len;                    
    int linenr;                 
    const char *tstart;
    const char *tend;           
    int tline;                  
    int tt;                     
    int eof;                    
    int inquote;                
    int comment;                
    struct sawoParseMissing missing;   
};

static int sawoParseScript(struct sawoParserCtx *pc);
static int sawoParseSep(struct sawoParserCtx *pc);
static int sawoParseEol(struct sawoParserCtx *pc);
static int sawoParseCmd(struct sawoParserCtx *pc);
static int sawoParseQuote(struct sawoParserCtx *pc);
static int sawoParseVar(struct sawoParserCtx *pc);
static int sawoParseBrace(struct sawoParserCtx *pc);
static int sawoParseStr(struct sawoParserCtx *pc);
static int sawoParseComment(struct sawoParserCtx *pc);
static void sawoParseSubCmd(struct sawoParserCtx *pc);
static int sawoParseSubQuote(struct sawoParserCtx *pc);
static sawo_Obj *sawoParserGetTokenObj(sawo_Interp *interp, struct sawoParserCtx *pc);

static void sawoParserInit(struct sawoParserCtx *pc, const char *prg, int len, int linenr)
{
    pc->p = prg;
    pc->len = len;
    pc->tstart = NULL;
    pc->tend = NULL;
    pc->tline = 0;
    pc->tt = SAWO_TT_NONE;
    pc->eof = 0;
    pc->inquote = 0;
    pc->linenr = linenr;
    pc->comment = 1;
    pc->missing.ch = ' ';
    pc->missing.line = linenr;
}

static int sawoParseScript(struct sawoParserCtx *pc)
{
    while (1) {                 
        if (!pc->len) {
            pc->tstart = pc->p;
            pc->tend = pc->p - 1;
            pc->tline = pc->linenr;
            pc->tt = SAWO_TT_EOL;
            pc->eof = 1;
            return SAWO_OK;
        }
        switch (*(pc->p)) {
            case '\\':
                if (*(pc->p + 1) == '\n' && !pc->inquote) {
                    return sawoParseSep(pc);
                }
                pc->comment = 0;
                return sawoParseStr(pc);
            case ' ':
            case '\t':
            case '\r':
            case '\f':
                if (!pc->inquote)
                    return sawoParseSep(pc);
                pc->comment = 0;
                return sawoParseStr(pc);
            case '\n':
            case ';':
                pc->comment = 1;
                if (!pc->inquote)
                    return sawoParseEol(pc);
                return sawoParseStr(pc);
            case '[':
                pc->comment = 0;
                return sawoParseCmd(pc);
            case '$':
                pc->comment = 0;
                if (sawoParseVar(pc) == SAWO_ERR) {
                    
                    pc->tstart = pc->tend = pc->p++;
                    pc->len--;
                    pc->tt = SAWO_TT_ESC;
                }
                return SAWO_OK;
            case '#':
                if (pc->comment) {
                    sawoParseComment(pc);
                    continue;
                }
                return sawoParseStr(pc);
            default:
                pc->comment = 0;
                return sawoParseStr(pc);
        }
        return SAWO_OK;
    }
}

static int sawoParseSep(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    while (isspace(UCHAR(*pc->p)) || (*pc->p == '\\' && *(pc->p + 1) == '\n')) {
        if (*pc->p == '\n') {
            break;
        }
        if (*pc->p == '\\') {
            pc->p++;
            pc->len--;
            pc->linenr++;
        }
        pc->p++;
        pc->len--;
    }
    pc->tend = pc->p - 1;
    pc->tt = SAWO_TT_SEP;
    return SAWO_OK;
}

static int sawoParseEol(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    while (isspace(UCHAR(*pc->p)) || *pc->p == ';') {
        if (*pc->p == '\n')
            pc->linenr++;
        pc->p++;
        pc->len--;
    }
    pc->tend = pc->p - 1;
    pc->tt = SAWO_TT_EOL;
    return SAWO_OK;
}


static void sawoParseSubBrace(struct sawoParserCtx *pc)
{
    int level = 1;

    
    pc->p++;
    pc->len--;
    while (pc->len) {
        switch (*pc->p) {
            case '\\':
                if (pc->len > 1) {
                    if (*++pc->p == '\n') {
                        pc->linenr++;
                    }
                    pc->len--;
                }
                break;

            case '{':
                level++;
                break;

            case '}':
                if (--level == 0) {
                    pc->tend = pc->p - 1;
                    pc->p++;
                    pc->len--;
                    return;
                }
                break;

            case '\n':
                pc->linenr++;
                break;
        }
        pc->p++;
        pc->len--;
    }
    pc->missing.ch = '{';
    pc->missing.line = pc->tline;
    pc->tend = pc->p - 1;
}

static int sawoParseSubQuote(struct sawoParserCtx *pc)
{
    int tt = SAWO_TT_STR;
    int line = pc->tline;

    
    pc->p++;
    pc->len--;
    while (pc->len) {
        switch (*pc->p) {
            case '\\':
                if (pc->len > 1) {
                    if (*++pc->p == '\n') {
                        pc->linenr++;
                    }
                    pc->len--;
                    tt = SAWO_TT_ESC;
                }
                break;

            case '"':
                pc->tend = pc->p - 1;
                pc->p++;
                pc->len--;
                return tt;

            case '[':
                sawoParseSubCmd(pc);
                tt = SAWO_TT_ESC;
                continue;

            case '\n':
                pc->linenr++;
                break;

            case '$':
                tt = SAWO_TT_ESC;
                break;
        }
        pc->p++;
        pc->len--;
    }
    pc->missing.ch = '"';
    pc->missing.line = line;
    pc->tend = pc->p - 1;
    return tt;
}

static void sawoParseSubCmd(struct sawoParserCtx *pc)
{
    int level = 1;
    int startofword = 1;
    int line = pc->tline;

    
    pc->p++;
    pc->len--;
    while (pc->len) {
        switch (*pc->p) {
            case '\\':
                if (pc->len > 1) {
                    if (*++pc->p == '\n') {
                        pc->linenr++;
                    }
                    pc->len--;
                }
                break;

            case '[':
                level++;
                break;

            case ']':
                if (--level == 0) {
                    pc->tend = pc->p - 1;
                    pc->p++;
                    pc->len--;
                    return;
                }
                break;

            case '"':
                if (startofword) {
                    sawoParseSubQuote(pc);
                    continue;
                }
                break;

            case '{':
                sawoParseSubBrace(pc);
                startofword = 0;
                continue;

            case '\n':
                pc->linenr++;
                break;
        }
        startofword = isspace(UCHAR(*pc->p));
        pc->p++;
        pc->len--;
    }
    pc->missing.ch = '[';
    pc->missing.line = line;
    pc->tend = pc->p - 1;
}

static int sawoParseBrace(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p + 1;
    pc->tline = pc->linenr;
    pc->tt = SAWO_TT_STR;
    sawoParseSubBrace(pc);
    return SAWO_OK;
}

static int sawoParseCmd(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p + 1;
    pc->tline = pc->linenr;
    pc->tt = SAWO_TT_CMD;
    sawoParseSubCmd(pc);
    return SAWO_OK;
}

static int sawoParseQuote(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p + 1;
    pc->tline = pc->linenr;
    pc->tt = sawoParseSubQuote(pc);
    return SAWO_OK;
}

static int sawoParseVar(struct sawoParserCtx *pc)
{
    
    pc->p++;
    pc->len--;

#ifdef EXPRSUGAR_BRACKET
    if (*pc->p == '[') {
        
        sawoParseCmd(pc);
        pc->tt = SAWO_TT_EXPRSUGAR;
        return SAWO_OK;
    }
#endif

    pc->tstart = pc->p;
    pc->tt = SAWO_TT_VAR;
    pc->tline = pc->linenr;

    if (*pc->p == '{') {
        pc->tstart = ++pc->p;
        pc->len--;

        while (pc->len && *pc->p != '}') {
            if (*pc->p == '\n') {
                pc->linenr++;
            }
            pc->p++;
            pc->len--;
        }
        pc->tend = pc->p - 1;
        if (pc->len) {
            pc->p++;
            pc->len--;
        }
    }
    else {
        while (1) {
            
            if (pc->p[0] == ':' && pc->p[1] == ':') {
                while (*pc->p == ':') {
                    pc->p++;
                    pc->len--;
                }
                continue;
            }
            if (isalnum(UCHAR(*pc->p)) || *pc->p == '_' || UCHAR(*pc->p) >= 0x80) {
                pc->p++;
                pc->len--;
                continue;
            }
            break;
        }
        
        if (*pc->p == '(') {
            int count = 1;
            const char *paren = NULL;

            pc->tt = SAWO_TT_DICTSUGAR;

            while (count && pc->len) {
                pc->p++;
                pc->len--;
                if (*pc->p == '\\' && pc->len >= 1) {
                    pc->p++;
                    pc->len--;
                }
                else if (*pc->p == '(') {
                    count++;
                }
                else if (*pc->p == ')') {
                    paren = pc->p;
                    count--;
                }
            }
            if (count == 0) {
                pc->p++;
                pc->len--;
            }
            else if (paren) {
                
                paren++;
                pc->len += (pc->p - paren);
                pc->p = paren;
            }
#ifndef EXPRSUGAR_BRACKET
            if (*pc->tstart == '(') {
                pc->tt = SAWO_TT_EXPRSUGAR;
            }
#endif
        }
        pc->tend = pc->p - 1;
    }
    if (pc->tstart == pc->p) {
        pc->p--;
        pc->len++;
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int sawoParseStr(struct sawoParserCtx *pc)
{
    if (pc->tt == SAWO_TT_SEP || pc->tt == SAWO_TT_EOL ||
        pc->tt == SAWO_TT_NONE || pc->tt == SAWO_TT_STR) {
        
        if (*pc->p == '{') {
            return sawoParseBrace(pc);
        }
        if (*pc->p == '"') {
            pc->inquote = 1;
            pc->p++;
            pc->len--;
            
            pc->missing.line = pc->tline;
        }
    }
    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    while (1) {
        if (pc->len == 0) {
            if (pc->inquote) {
                pc->missing.ch = '"';
            }
            pc->tend = pc->p - 1;
            pc->tt = SAWO_TT_ESC;
            return SAWO_OK;
        }
        switch (*pc->p) {
            case '\\':
                if (!pc->inquote && *(pc->p + 1) == '\n') {
                    pc->tend = pc->p - 1;
                    pc->tt = SAWO_TT_ESC;
                    return SAWO_OK;
                }
                if (pc->len >= 2) {
                    if (*(pc->p + 1) == '\n') {
                        pc->linenr++;
                    }
                    pc->p++;
                    pc->len--;
                }
                else if (pc->len == 1) {
                    
                    pc->missing.ch = '\\';
                }
                break;
            case '(':
                
                if (pc->len > 1 && pc->p[1] != '$') {
                    break;
                }
                
            case ')':
                
                if (*pc->p == '(' || pc->tt == SAWO_TT_VAR) {
                    if (pc->p == pc->tstart) {
                        
                        pc->p++;
                        pc->len--;
                    }
                    pc->tend = pc->p - 1;
                    pc->tt = SAWO_TT_ESC;
                    return SAWO_OK;
                }
                break;

            case '$':
            case '[':
                pc->tend = pc->p - 1;
                pc->tt = SAWO_TT_ESC;
                return SAWO_OK;
            case ' ':
            case '\t':
            case '\n':
            case '\r':
            case '\f':
            case ';':
                if (!pc->inquote) {
                    pc->tend = pc->p - 1;
                    pc->tt = SAWO_TT_ESC;
                    return SAWO_OK;
                }
                else if (*pc->p == '\n') {
                    pc->linenr++;
                }
                break;
            case '"':
                if (pc->inquote) {
                    pc->tend = pc->p - 1;
                    pc->tt = SAWO_TT_ESC;
                    pc->p++;
                    pc->len--;
                    pc->inquote = 0;
                    return SAWO_OK;
                }
                break;
        }
        pc->p++;
        pc->len--;
    }
    return SAWO_OK;              
}

static int sawoParseComment(struct sawoParserCtx *pc)
{
    while (*pc->p) {
        if (*pc->p == '\\') {
            pc->p++;
            pc->len--;
            if (pc->len == 0) {
                pc->missing.ch = '\\';
                return SAWO_OK;
            }
            if (*pc->p == '\n') {
                pc->linenr++;
            }
        }
        else if (*pc->p == '\n') {
            pc->p++;
            pc->len--;
            pc->linenr++;
            break;
        }
        pc->p++;
        pc->len--;
    }
    return SAWO_OK;
}


static int xdigitval(int c)
{
    if (c >= '0' && c <= '9')
        return c - '0';
    if (c >= 'a' && c <= 'f')
        return c - 'a' + 10;
    if (c >= 'A' && c <= 'F')
        return c - 'A' + 10;
    return -1;
}

static int odigitval(int c)
{
    if (c >= '0' && c <= '7')
        return c - '0';
    return -1;
}

static int sawoEscape(char *dest, const char *s, int slen)
{
    char *p = dest;
    int i, len;

    for (i = 0; i < slen; i++) {
        switch (s[i]) {
            case '\\':
                switch (s[i + 1]) {
                    case 'a':
                        *p++ = 0x7;
                        i++;
                        break;
                    case 'b':
                        *p++ = 0x8;
                        i++;
                        break;
                    case 'f':
                        *p++ = 0xc;
                        i++;
                        break;
                    case 'n':
                        *p++ = 0xa;
                        i++;
                        break;
                    case 'r':
                        *p++ = 0xd;
                        i++;
                        break;
                    case 't':
                        *p++ = 0x9;
                        i++;
                        break;
                    case 'u':
                    case 'U':
                    case 'x':
                        {
                            unsigned val = 0;
                            int k;
                            int maxchars = 2;

                            i++;

                            if (s[i] == 'U') {
                                maxchars = 8;
                            }
                            else if (s[i] == 'u') {
                                if (s[i + 1] == '{') {
                                    maxchars = 6;
                                    i++;
                                }
                                else {
                                    maxchars = 4;
                                }
                            }

                            for (k = 0; k < maxchars; k++) {
                                int c = xdigitval(s[i + k + 1]);
                                if (c == -1) {
                                    break;
                                }
                                val = (val << 4) | c;
                            }
                            
                            if (s[i] == '{') {
                                if (k == 0 || val > 0x1fffff || s[i + k + 1] != '}') {
                                    
                                    i--;
                                    k = 0;
                                }
                                else {
                                    
                                    k++;
                                }
                            }
                            if (k) {
                                
                                if (s[i] == 'x') {
                                    *p++ = val;
                                }
                                else {
                                    p += utf8_fromunicode(p, val);
                                }
                                i += k;
                                break;
                            }
                            
                            *p++ = s[i];
                        }
                        break;
                    case 'v':
                        *p++ = 0xb;
                        i++;
                        break;
                    case '\0':
                        *p++ = '\\';
                        i++;
                        break;
                    case '\n':
                        
                        *p++ = ' ';
                        do {
                            i++;
                        } while (s[i + 1] == ' ' || s[i + 1] == '\t');
                        break;
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                        
                        {
                            int val = 0;
                            int c = odigitval(s[i + 1]);

                            val = c;
                            c = odigitval(s[i + 2]);
                            if (c == -1) {
                                *p++ = val;
                                i++;
                                break;
                            }
                            val = (val * 8) + c;
                            c = odigitval(s[i + 3]);
                            if (c == -1) {
                                *p++ = val;
                                i += 2;
                                break;
                            }
                            val = (val * 8) + c;
                            *p++ = val;
                            i += 3;
                        }
                        break;
                    default:
                        *p++ = s[i + 1];
                        i++;
                        break;
                }
                break;
            default:
                *p++ = s[i];
                break;
        }
    }
    len = p - dest;
    *p = '\0';
    return len;
}

static sawo_Obj *sawoParserGetTokenObj(sawo_Interp *interp, struct sawoParserCtx *pc)
{
    const char *start, *end;
    char *token;
    int len;

    start = pc->tstart;
    end = pc->tend;
    if (start > end) {
        len = 0;
        token = sawo_Alloc(1);
        token[0] = '\0';
    }
    else {
        len = (end - start) + 1;
        token = sawo_Alloc(len + 1);
        if (pc->tt != SAWO_TT_ESC) {
            
            memcpy(token, start, len);
            token[len] = '\0';
        }
        else {
            
            len = sawoEscape(token, start, len);
        }
    }

    return sawo_NewStringObjNoAlloc(interp, token, len);
}

static int sawoParseListSep(struct sawoParserCtx *pc);
static int sawoParseListStr(struct sawoParserCtx *pc);
static int sawoParseListQuote(struct sawoParserCtx *pc);

static int sawoParseList(struct sawoParserCtx *pc)
{
    if (isspace(UCHAR(*pc->p))) {
        return sawoParseListSep(pc);
    }
    switch (*pc->p) {
        case '"':
            return sawoParseListQuote(pc);

        case '{':
            return sawoParseBrace(pc);

        default:
            if (pc->len) {
                return sawoParseListStr(pc);
            }
            break;
    }

    pc->tstart = pc->tend = pc->p;
    pc->tline = pc->linenr;
    pc->tt = SAWO_TT_EOL;
    pc->eof = 1;
    return SAWO_OK;
}

static int sawoParseListSep(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    while (isspace(UCHAR(*pc->p))) {
        if (*pc->p == '\n') {
            pc->linenr++;
        }
        pc->p++;
        pc->len--;
    }
    pc->tend = pc->p - 1;
    pc->tt = SAWO_TT_SEP;
    return SAWO_OK;
}

static int sawoParseListQuote(struct sawoParserCtx *pc)
{
    pc->p++;
    pc->len--;

    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    pc->tt = SAWO_TT_STR;

    while (pc->len) {
        switch (*pc->p) {
            case '\\':
                pc->tt = SAWO_TT_ESC;
                if (--pc->len == 0) {
                    
                    pc->tend = pc->p;
                    return SAWO_OK;
                }
                pc->p++;
                break;
            case '\n':
                pc->linenr++;
                break;
            case '"':
                pc->tend = pc->p - 1;
                pc->p++;
                pc->len--;
                return SAWO_OK;
        }
        pc->p++;
        pc->len--;
    }

    pc->tend = pc->p - 1;
    return SAWO_OK;
}

static int sawoParseListStr(struct sawoParserCtx *pc)
{
    pc->tstart = pc->p;
    pc->tline = pc->linenr;
    pc->tt = SAWO_TT_STR;

    while (pc->len) {
        if (isspace(UCHAR(*pc->p))) {
            pc->tend = pc->p - 1;
            return SAWO_OK;
        }
        if (*pc->p == '\\') {
            if (--pc->len == 0) {
                
                pc->tend = pc->p;
                return SAWO_OK;
            }
            pc->tt = SAWO_TT_ESC;
            pc->p++;
        }
        pc->p++;
        pc->len--;
    }
    pc->tend = pc->p - 1;
    return SAWO_OK;
}



sawo_Obj *sawo_NewObj(sawo_Interp *interp)
{
    sawo_Obj *objPtr;

    
    if (interp->freeList != NULL) {
        
        objPtr = interp->freeList;
        interp->freeList = objPtr->nextObjPtr;
    }
    else {
        
        objPtr = sawo_Alloc(sizeof(*objPtr));
    }

    objPtr->refCount = 0;

    
    objPtr->prevObjPtr = NULL;
    objPtr->nextObjPtr = interp->liveList;
    if (interp->liveList)
        interp->liveList->prevObjPtr = objPtr;
    interp->liveList = objPtr;

    return objPtr;
}

void sawo_FreeObj(sawo_Interp *interp, sawo_Obj *objPtr)
{
    
    sawoPanic((objPtr->refCount != 0, "!!!Object %p freed with bad refcount %d, type=%s", objPtr,
        objPtr->refCount, objPtr->typePtr ? objPtr->typePtr->name : "<none>"));

    
    sawo_FreeIntRep(interp, objPtr);
    
    if (objPtr->bytes != NULL) {
        if (objPtr->bytes != sawoEmptyStringRep)
            sawo_Free(objPtr->bytes);
    }
    
    if (objPtr->prevObjPtr)
        objPtr->prevObjPtr->nextObjPtr = objPtr->nextObjPtr;
    if (objPtr->nextObjPtr)
        objPtr->nextObjPtr->prevObjPtr = objPtr->prevObjPtr;
    if (interp->liveList == objPtr)
        interp->liveList = objPtr->nextObjPtr;
#ifdef SAWO_DISABLE_OBJECT_POOL
    sawo_Free(objPtr);
#else
    
    objPtr->prevObjPtr = NULL;
    objPtr->nextObjPtr = interp->freeList;
    if (interp->freeList)
        interp->freeList->prevObjPtr = objPtr;
    interp->freeList = objPtr;
    objPtr->refCount = -1;
#endif
}


void sawo_InvalidateStringRep(sawo_Obj *objPtr)
{
    if (objPtr->bytes != NULL) {
        if (objPtr->bytes != sawoEmptyStringRep)
            sawo_Free(objPtr->bytes);
    }
    objPtr->bytes = NULL;
}


sawo_Obj *sawo_DuplicateObj(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_Obj *dupPtr;

    dupPtr = sawo_NewObj(interp);
    if (objPtr->bytes == NULL) {
        
        dupPtr->bytes = NULL;
    }
    else if (objPtr->length == 0) {
        
        dupPtr->bytes = sawoEmptyStringRep;
        dupPtr->length = 0;
        dupPtr->typePtr = NULL;
        return dupPtr;
    }
    else {
        dupPtr->bytes = sawo_Alloc(objPtr->length + 1);
        dupPtr->length = objPtr->length;
        
        memcpy(dupPtr->bytes, objPtr->bytes, objPtr->length + 1);
    }

    
    dupPtr->typePtr = objPtr->typePtr;
    if (objPtr->typePtr != NULL) {
        if (objPtr->typePtr->dupIntRepRaise == NULL) {
            dupPtr->internalRep = objPtr->internalRep;
        }
        else {
            
            objPtr->typePtr->dupIntRepRaise(interp, objPtr, dupPtr);
        }
    }
    return dupPtr;
}

const char *sawo_GetString(sawo_Obj *objPtr, int *lenPtr)
{
    if (objPtr->bytes == NULL) {
        
        sawoPanic((objPtr->typePtr->updateStringRaise == NULL, "updateStringRaise called against '%s' type.", objPtr->typePtr->name));
        objPtr->typePtr->updateStringRaise(objPtr);
    }
    if (lenPtr)
        *lenPtr = objPtr->length;
    return objPtr->bytes;
}


int sawo_Length(sawo_Obj *objPtr)
{
    if (objPtr->bytes == NULL) {
        
        sawoPanic((objPtr->typePtr->updateStringRaise == NULL, "updateStringRaise called against '%s' type.", objPtr->typePtr->name));
        objPtr->typePtr->updateStringRaise(objPtr);
    }
    return objPtr->length;
}


const char *sawo_String(sawo_Obj *objPtr)
{
    if (objPtr->bytes == NULL) {
        
        sawoPanic((objPtr->typePtr == NULL, "updateStringRaise called against typeless value."));
        sawoPanic((objPtr->typePtr->updateStringRaise == NULL, "updateStringRaise called against '%s' type.", objPtr->typePtr->name));
        objPtr->typePtr->updateStringRaise(objPtr);
    }
    return objPtr->bytes;
}

static void sawoSetStringBytes(sawo_Obj *objPtr, const char *str)
{
    objPtr->bytes = sawo_StrDup(str);
    objPtr->length = strlen(str);
}

static void FreeDictSubstInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupDictSubstInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);

static const sawo_ObjType dictSubstObjType = {
    "dict-substitution",
    FreeDictSubstInternalRep,
    DupDictSubstInternalRep,
    NULL,
    SAWO_TYPE_NONE,
};

static void FreeInterpolatedInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_DecrRefCount(interp, objPtr->internalRep.dictSubstValue.indexObjPtr);
}

static const sawo_ObjType interpolatedObjType = {
    "interpolated",
    FreeInterpolatedInternalRep,
    NULL,
    NULL,
    SAWO_TYPE_NONE,
};

static void DupStringInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);
static int SetStringFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);

static const sawo_ObjType stringObjType = {
    "string",
    NULL,
    DupStringInternalRep,
    NULL,
    SAWO_TYPE_REFERENCES,
};

static void DupStringInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    SAWO_NOTUSED(interp);

    dupPtr->internalRep.strValue.maxLength = srcPtr->length;
    dupPtr->internalRep.strValue.charLength = srcPtr->internalRep.strValue.charLength;
}

static int SetStringFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (objPtr->typePtr != &stringObjType) {
        
        if (objPtr->bytes == NULL) {
            
            sawoPanic((objPtr->typePtr->updateStringRaise == NULL, "updateStringRaise called against '%s' type.", objPtr->typePtr->name));
            objPtr->typePtr->updateStringRaise(objPtr);
        }
        
        sawo_FreeIntRep(interp, objPtr);
        
        objPtr->typePtr = &stringObjType;
        objPtr->internalRep.strValue.maxLength = objPtr->length;
        
        objPtr->internalRep.strValue.charLength = -1;
    }
    return SAWO_OK;
}

int sawo_Utf8Length(sawo_Interp *interp, sawo_Obj *objPtr)
{
#ifdef SAWO_UTF8
    SetStringFromAny(interp, objPtr);

    if (objPtr->internalRep.strValue.charLength < 0) {
        objPtr->internalRep.strValue.charLength = utf8_strlen(objPtr->bytes, objPtr->length);
    }
    return objPtr->internalRep.strValue.charLength;
#else
    return sawo_Length(objPtr);
#endif
}


sawo_Obj *sawo_NewStringObj(sawo_Interp *interp, const char *s, int len)
{
    sawo_Obj *objPtr = sawo_NewObj(interp);

    
    if (len == -1)
        len = strlen(s);
    
    if (len == 0) {
        objPtr->bytes = sawoEmptyStringRep;
    }
    else {
        objPtr->bytes = sawo_Alloc(len + 1);
        memcpy(objPtr->bytes, s, len);
        objPtr->bytes[len] = '\0';
    }
    objPtr->length = len;

    
    objPtr->typePtr = NULL;
    return objPtr;
}


sawo_Obj *sawo_NewStringObjUtf8(sawo_Interp *interp, const char *s, int charlen)
{
#ifdef SAWO_UTF8
    
    int bytelen = utf8_index(s, charlen);

    sawo_Obj *objPtr = sawo_NewStringObj(interp, s, bytelen);

    
    objPtr->typePtr = &stringObjType;
    objPtr->internalRep.strValue.maxLength = bytelen;
    objPtr->internalRep.strValue.charLength = charlen;

    return objPtr;
#else
    return sawo_NewStringObj(interp, s, charlen);
#endif
}

sawo_Obj *sawo_NewStringObjNoAlloc(sawo_Interp *interp, char *s, int len)
{
    sawo_Obj *objPtr = sawo_NewObj(interp);

    objPtr->bytes = s;
    objPtr->length = (len == -1) ? strlen(s) : len;
    objPtr->typePtr = NULL;
    return objPtr;
}

static void StringAppendString(sawo_Obj *objPtr, const char *str, int len)
{
    int needlen;

    if (len == -1)
        len = strlen(str);
    needlen = objPtr->length + len;
    if (objPtr->internalRep.strValue.maxLength < needlen ||
        objPtr->internalRep.strValue.maxLength == 0) {
        needlen *= 2;
        
        if (needlen < 7) {
            needlen = 7;
        }
        if (objPtr->bytes == sawoEmptyStringRep) {
            objPtr->bytes = sawo_Alloc(needlen + 1);
        }
        else {
            objPtr->bytes = sawo_Realloc(objPtr->bytes, needlen + 1);
        }
        objPtr->internalRep.strValue.maxLength = needlen;
    }
    memcpy(objPtr->bytes + objPtr->length, str, len);
    objPtr->bytes[objPtr->length + len] = '\0';

    if (objPtr->internalRep.strValue.charLength >= 0) {
        
        objPtr->internalRep.strValue.charLength += utf8_strlen(objPtr->bytes + objPtr->length, len);
    }
    objPtr->length += len;
}

void sawo_AppendString(sawo_Interp *interp, sawo_Obj *objPtr, const char *str, int len)
{
    sawoPanic((sawo_IsShared(objPtr), "sawo_AppendString called with shared object"));
    SetStringFromAny(interp, objPtr);
    StringAppendString(objPtr, str, len);
}

void sawo_AppendObj(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *appendObjPtr)
{
    int len;
    const char *str = sawo_GetString(appendObjPtr, &len);
    sawo_AppendString(interp, objPtr, str, len);
}

void sawo_AppendStrings(sawo_Interp *interp, sawo_Obj *objPtr, ...)
{
    va_list ap;

    SetStringFromAny(interp, objPtr);
    va_start(ap, objPtr);
    while (1) {
        const char *s = va_arg(ap, const char *);

        if (s == NULL)
            break;
        sawo_AppendString(interp, objPtr, s, -1);
    }
    va_end(ap);
}

int sawo_StringEqObj(sawo_Obj *aObjPtr, sawo_Obj *bObjPtr)
{
    if (aObjPtr == bObjPtr) {
        return 1;
    }
    else {
        int Alen, Blen;
        const char *sA = sawo_GetString(aObjPtr, &Alen);
        const char *sB = sawo_GetString(bObjPtr, &Blen);

        return Alen == Blen && memcmp(sA, sB, Alen) == 0;
    }
}

int sawo_StringMatchObj(sawo_Interp *interp, sawo_Obj *patternObjPtr, sawo_Obj *objPtr, int nocase)
{
    return sawoGlobMatch(sawo_String(patternObjPtr), sawo_String(objPtr), nocase);
}

int sawo_StringCompareObj(sawo_Interp *interp, sawo_Obj *firstObjPtr, sawo_Obj *secondObjPtr, int nocase)
{
    int l1, l2;
    const char *s1 = sawo_GetString(firstObjPtr, &l1);
    const char *s2 = sawo_GetString(secondObjPtr, &l2);

    if (nocase) {
        
        return sawoStringCompareLen(s1, s2, -1, nocase);
    }
    return sawoStringCompare(s1, l1, s2, l2);
}

int sawo_StringCompareLenObj(sawo_Interp *interp, sawo_Obj *firstObjPtr, sawo_Obj *secondObjPtr, int nocase)
{
    const char *s1 = sawo_String(firstObjPtr);
    const char *s2 = sawo_String(secondObjPtr);

    return sawoStringCompareLen(s1, s2, sawo_Utf8Length(interp, firstObjPtr), nocase);
}

static int sawoRelToAbsIndex(int len, int idx)
{
    if (idx < 0)
        return len + idx;
    return idx;
}

static void sawoRelToAbsRange(int len, int *firstPtr, int *lastPtr, int *rangeLenPtr)
{
    int rangeLen;

    if (*firstPtr > *lastPtr) {
        rangeLen = 0;
    }
    else {
        rangeLen = *lastPtr - *firstPtr + 1;
        if (rangeLen) {
            if (*firstPtr < 0) {
                rangeLen += *firstPtr;
                *firstPtr = 0;
            }
            if (*lastPtr >= len) {
                rangeLen -= (*lastPtr - (len - 1));
                *lastPtr = len - 1;
            }
        }
    }
    if (rangeLen < 0)
        rangeLen = 0;

    *rangeLenPtr = rangeLen;
}

static int sawoStringGetRange(sawo_Interp *interp, sawo_Obj *firstObjPtr, sawo_Obj *lastObjPtr,
    int len, int *first, int *last, int *range)
{
    if (sawo_GetIndex(interp, firstObjPtr, first) != SAWO_OK) {
        return SAWO_ERR;
    }
    if (sawo_GetIndex(interp, lastObjPtr, last) != SAWO_OK) {
        return SAWO_ERR;
    }
    *first = sawoRelToAbsIndex(len, *first);
    *last = sawoRelToAbsIndex(len, *last);
    sawoRelToAbsRange(len, first, last, range);
    return SAWO_OK;
}

sawo_Obj *sawo_StringByteRangeObj(sawo_Interp *interp,
    sawo_Obj *strObjPtr, sawo_Obj *firstObjPtr, sawo_Obj *lastObjPtr)
{
    int first, last;
    const char *str;
    int rangeLen;
    int bytelen;

    str = sawo_GetString(strObjPtr, &bytelen);

    if (sawoStringGetRange(interp, firstObjPtr, lastObjPtr, bytelen, &first, &last, &rangeLen) != SAWO_OK) {
        return NULL;
    }

    if (first == 0 && rangeLen == bytelen) {
        return strObjPtr;
    }
    return sawo_NewStringObj(interp, str + first, rangeLen);
}

sawo_Obj *sawo_StringRangeObj(sawo_Interp *interp,
    sawo_Obj *strObjPtr, sawo_Obj *firstObjPtr, sawo_Obj *lastObjPtr)
{
#ifdef SAWO_UTF8
    int first, last;
    const char *str;
    int len, rangeLen;
    int bytelen;

    str = sawo_GetString(strObjPtr, &bytelen);
    len = sawo_Utf8Length(interp, strObjPtr);

    if (sawoStringGetRange(interp, firstObjPtr, lastObjPtr, len, &first, &last, &rangeLen) != SAWO_OK) {
        return NULL;
    }

    if (first == 0 && rangeLen == len) {
        return strObjPtr;
    }
    if (len == bytelen) {
        
        return sawo_NewStringObj(interp, str + first, rangeLen);
    }
    return sawo_NewStringObjUtf8(interp, str + utf8_index(str, first), rangeLen);
#else
    return sawo_StringByteRangeObj(interp, strObjPtr, firstObjPtr, lastObjPtr);
#endif
}

sawo_Obj *sawoStringReplaceObj(sawo_Interp *interp,
    sawo_Obj *strObjPtr, sawo_Obj *firstObjPtr, sawo_Obj *lastObjPtr, sawo_Obj *newStrObj)
{
    int first, last;
    const char *str;
    int len, rangeLen;
    sawo_Obj *objPtr;

    len = sawo_Utf8Length(interp, strObjPtr);

    if (sawoStringGetRange(interp, firstObjPtr, lastObjPtr, len, &first, &last, &rangeLen) != SAWO_OK) {
        return NULL;
    }

    if (last < first) {
        return strObjPtr;
    }

    str = sawo_String(strObjPtr);

    
    objPtr = sawo_NewStringObjUtf8(interp, str, first);

    
    if (newStrObj) {
        sawo_AppendObj(interp, objPtr, newStrObj);
    }

    
    sawo_AppendString(interp, objPtr, str + utf8_index(str, last + 1), len - last - 1);

    return objPtr;
}

static void sawoStrCopyUpperLower(char *dest, const char *str, int uc)
{
    while (*str) {
        int c;
        str += utf8_tounicode(str, &c);
        dest += utf8_getchars(dest, uc ? utf8_upper(c) : utf8_lower(c));
    }
    *dest = 0;
}

static sawo_Obj *sawoStringToLower(sawo_Interp *interp, sawo_Obj *strObjPtr)
{
    char *buf;
    int len;
    const char *str;

    SetStringFromAny(interp, strObjPtr);

    str = sawo_GetString(strObjPtr, &len);

#ifdef SAWO_UTF8
    len *= 2;
#endif
    buf = sawo_Alloc(len + 1);
    sawoStrCopyUpperLower(buf, str, 0);
    return sawo_NewStringObjNoAlloc(interp, buf, -1);
}

static sawo_Obj *sawoStringToUpper(sawo_Interp *interp, sawo_Obj *strObjPtr)
{
    char *buf;
    const char *str;
    int len;

    if (strObjPtr->typePtr != &stringObjType) {
        SetStringFromAny(interp, strObjPtr);
    }

    str = sawo_GetString(strObjPtr, &len);

#ifdef SAWO_UTF8
    len *= 2;
#endif
    buf = sawo_Alloc(len + 1);
    sawoStrCopyUpperLower(buf, str, 1);
    return sawo_NewStringObjNoAlloc(interp, buf, -1);
}

static sawo_Obj *sawoStringToTitle(sawo_Interp *interp, sawo_Obj *strObjPtr)
{
    char *buf, *p;
    int len;
    int c;
    const char *str;

    str = sawo_GetString(strObjPtr, &len);
    if (len == 0) {
        return strObjPtr;
    }
#ifdef SAWO_UTF8
    len *= 2;
#endif
    buf = p = sawo_Alloc(len + 1);

    str += utf8_tounicode(str, &c);
    p += utf8_getchars(p, utf8_title(c));

    sawoStrCopyUpperLower(p, str, 0);

    return sawo_NewStringObjNoAlloc(interp, buf, -1);
}

static const char *utf8_memchr(const char *str, int len, int c)
{
#ifdef SAWO_UTF8
    while (len) {
        int sc;
        int n = utf8_tounicode(str, &sc);
        if (sc == c) {
            return str;
        }
        str += n;
        len -= n;
    }
    return NULL;
#else
    return memchr(str, c, len);
#endif
}

static const char *sawoFindTrimLeft(const char *str, int len, const char *trimchars, int trimlen)
{
    while (len) {
        int c;
        int n = utf8_tounicode(str, &c);

        if (utf8_memchr(trimchars, trimlen, c) == NULL) {
            
            break;
        }
        str += n;
        len -= n;
    }
    return str;
}

static const char *sawoFindTrimRight(const char *str, int len, const char *trimchars, int trimlen)
{
    str += len;

    while (len) {
        int c;
        int n = utf8_prev_len(str, len);

        len -= n;
        str -= n;

        n = utf8_tounicode(str, &c);

        if (utf8_memchr(trimchars, trimlen, c) == NULL) {
            return str + n;
        }
    }

    return NULL;
}

static const char default_trim_chars[] = " \t\n\r";

static int default_trim_chars_len = sizeof(default_trim_chars);

static sawo_Obj *sawoStringTrimLeft(sawo_Interp *interp, sawo_Obj *strObjPtr, sawo_Obj *trimcharsObjPtr)
{
    int len;
    const char *str = sawo_GetString(strObjPtr, &len);
    const char *trimchars = default_trim_chars;
    int trimcharslen = default_trim_chars_len;
    const char *newstr;

    if (trimcharsObjPtr) {
        trimchars = sawo_GetString(trimcharsObjPtr, &trimcharslen);
    }

    newstr = sawoFindTrimLeft(str, len, trimchars, trimcharslen);
    if (newstr == str) {
        return strObjPtr;
    }

    return sawo_NewStringObj(interp, newstr, len - (newstr - str));
}

static sawo_Obj *sawoStringTrimRight(sawo_Interp *interp, sawo_Obj *strObjPtr, sawo_Obj *trimcharsObjPtr)
{
    int len;
    const char *trimchars = default_trim_chars;
    int trimcharslen = default_trim_chars_len;
    const char *nontrim;

    if (trimcharsObjPtr) {
        trimchars = sawo_GetString(trimcharsObjPtr, &trimcharslen);
    }

    SetStringFromAny(interp, strObjPtr);

    len = sawo_Length(strObjPtr);
    nontrim = sawoFindTrimRight(strObjPtr->bytes, len, trimchars, trimcharslen);

    if (nontrim == NULL) {
        
        return sawo_NewEmptyStringObj(interp);
    }
    if (nontrim == strObjPtr->bytes + len) {
        
        return strObjPtr;
    }

    if (sawo_IsShared(strObjPtr)) {
        strObjPtr = sawo_NewStringObj(interp, strObjPtr->bytes, (nontrim - strObjPtr->bytes));
    }
    else {
        
        strObjPtr->bytes[nontrim - strObjPtr->bytes] = 0;
        strObjPtr->length = (nontrim - strObjPtr->bytes);
    }

    return strObjPtr;
}

static sawo_Obj *sawoStringTrim(sawo_Interp *interp, sawo_Obj *strObjPtr, sawo_Obj *trimcharsObjPtr)
{
    
    sawo_Obj *objPtr = sawoStringTrimLeft(interp, strObjPtr, trimcharsObjPtr);

    
    strObjPtr = sawoStringTrimRight(interp, objPtr, trimcharsObjPtr);

    
    if (objPtr != strObjPtr && objPtr->refCount == 0) {
        
        sawo_FreeNewObj(interp, objPtr);
    }

    return strObjPtr;
}


#ifdef HAVE_ISASCII
#define sawo_isascii isascii
#else
static int sawo_isascii(int c)
{
    return !(c & ~0x7f);
}
#endif

static int sawoStringIs(sawo_Interp *interp, sawo_Obj *strObjPtr, sawo_Obj *strClass, int strict)
{
    static const char * const strclassnames[] = {
        "integer", "alpha", "alnum", "ascii", "digit",
        "double", "lower", "upper", "space", "xdigit",
        "control", "print", "graph", "punct",
        NULL
    };
    enum {
        STR_IS_INTEGER, STR_IS_ALPHA, STR_IS_ALNUM, STR_IS_ASCII, STR_IS_DIGIT,
        STR_IS_DOUBLE, STR_IS_LOWER, STR_IS_UPPER, STR_IS_SPACE, STR_IS_XDIGIT,
        STR_IS_CONTROL, STR_IS_PRINT, STR_IS_GRAPH, STR_IS_PUNCT
    };
    int strclass;
    int len;
    int i;
    const char *str;
    int (*isclassfunc)(int c) = NULL;

    if (sawo_GetEnum(interp, strClass, strclassnames, &strclass, "class", SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
        return SAWO_ERR;
    }

    str = sawo_GetString(strObjPtr, &len);
    if (len == 0) {
        sawo_SetResultBool(interp, !strict);
        return SAWO_OK;
    }

    switch (strclass) {
        case STR_IS_INTEGER:
            {
                sawo_wide w;
                sawo_SetResultBool(interp, sawoGetWideNoErr(interp, strObjPtr, &w) == SAWO_OK);
                return SAWO_OK;
            }

        case STR_IS_DOUBLE:
            {
                double d;
                sawo_SetResultBool(interp, sawo_GetDouble(interp, strObjPtr, &d) == SAWO_OK && errno != ERANGE);
                return SAWO_OK;
            }

        case STR_IS_ALPHA: isclassfunc = isalpha; break;
        case STR_IS_ALNUM: isclassfunc = isalnum; break;
        case STR_IS_ASCII: isclassfunc = sawo_isascii; break;
        case STR_IS_DIGIT: isclassfunc = isdigit; break;
        case STR_IS_LOWER: isclassfunc = islower; break;
        case STR_IS_UPPER: isclassfunc = isupper; break;
        case STR_IS_SPACE: isclassfunc = isspace; break;
        case STR_IS_XDIGIT: isclassfunc = isxdigit; break;
        case STR_IS_CONTROL: isclassfunc = iscntrl; break;
        case STR_IS_PRINT: isclassfunc = isprint; break;
        case STR_IS_GRAPH: isclassfunc = isgraph; break;
        case STR_IS_PUNCT: isclassfunc = ispunct; break;
        default:
            return SAWO_ERR;
    }

    for (i = 0; i < len; i++) {
        if (!isclassfunc(str[i])) {
            sawo_SetResultBool(interp, 0);
            return SAWO_OK;
        }
    }
    sawo_SetResultBool(interp, 1);
    return SAWO_OK;
}



static const sawo_ObjType comparedStringObjType = {
    "compared-string",
    NULL,
    NULL,
    NULL,
    SAWO_TYPE_REFERENCES,
};

int sawo_CompareStringImmediate(sawo_Interp *interp, sawo_Obj *objPtr, const char *str)
{
    if (objPtr->typePtr == &comparedStringObjType && objPtr->internalRep.ptr == str) {
        return 1;
    }
    else {
        const char *objStr = sawo_String(objPtr);

        if (strcmp(str, objStr) != 0)
            return 0;

        if (objPtr->typePtr != &comparedStringObjType) {
            sawo_FreeIntRep(interp, objPtr);
            objPtr->typePtr = &comparedStringObjType;
        }
        objPtr->internalRep.ptr = (char *)str;  
        return 1;
    }
}

static int qsortCompareStringPointers(const void *a, const void *b)
{
    char *const *sa = (char *const *)a;
    char *const *sb = (char *const *)b;

    return strcmp(*sa, *sb);
}



static void FreeSourceInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupSourceInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);

static const sawo_ObjType sourceObjType = {
    "source",
    FreeSourceInternalRep,
    DupSourceInternalRep,
    NULL,
    SAWO_TYPE_REFERENCES,
};

void FreeSourceInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_DecrRefCount(interp, objPtr->internalRep.sourceValue.fileNameObj);
}

void DupSourceInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    dupPtr->internalRep.sourceValue = srcPtr->internalRep.sourceValue;
    sawo_IncrRefCount(dupPtr->internalRep.sourceValue.fileNameObj);
}

static void sawoSetSourceInfo(sawo_Interp *interp, sawo_Obj *objPtr,
    sawo_Obj *fileNameObj, int lineNumber)
{
    sawoPanic((sawo_IsShared(objPtr), "sawoSetSourceInfo called with shared object"));
    sawoPanic((objPtr->typePtr != NULL, "sawoSetSourceInfo called with typed object"));
    sawo_IncrRefCount(fileNameObj);
    objPtr->internalRep.sourceValue.fileNameObj = fileNameObj;
    objPtr->internalRep.sourceValue.lineNumber = lineNumber;
    objPtr->typePtr = &sourceObjType;
}

static const sawo_ObjType scriptLineObjType = {
    "scriptline",
    NULL,
    NULL,
    NULL,
    SAWO_NONE,
};

static sawo_Obj *sawoNewScriptLineObj(sawo_Interp *interp, int argc, int line)
{
    sawo_Obj *objPtr;

#ifdef DEBUG_SHOW_SCRIPT
    char buf[100];
    snprintf(buf, sizeof(buf), "line=%d, argc=%d", line, argc);
    objPtr = sawo_NewStringObj(interp, buf, -1);
#else
    objPtr = sawo_NewEmptyStringObj(interp);
#endif
    objPtr->typePtr = &scriptLineObjType;
    objPtr->internalRep.scriptLineValue.argc = argc;
    objPtr->internalRep.scriptLineValue.line = line;

    return objPtr;
}

static void FreeScriptInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupScriptInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);

static const sawo_ObjType scriptObjType = {
    "script",
    FreeScriptInternalRep,
    DupScriptInternalRep,
    NULL,
    SAWO_TYPE_REFERENCES,
};

typedef struct ScriptToken
{
    sawo_Obj *objPtr;
    int type;
} ScriptToken;

typedef struct ScriptObj
{
    ScriptToken *token;         
    sawo_Obj *fileNameObj;       
    int len;                    
    int substFlags;             
    int inUse;                  /* Used to share a ScriptObj. Currently
                                   only used by sawo_EvalObj() as protection against
                                   shimmering of the currently evaluated object. */
    int firstline;              
    int linenr;                 
    int missing;                
} ScriptObj;

static void sawoSetScriptFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);
static int sawoParseCheckMissing(sawo_Interp *interp, int ch);
static ScriptObj *sawoGetScript(sawo_Interp *interp, sawo_Obj *objPtr);

void FreeScriptInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    int i;
    struct ScriptObj *script = (void *)objPtr->internalRep.ptr;

    if (--script->inUse != 0)
        return;
    for (i = 0; i < script->len; i++) {
        sawo_DecrRefCount(interp, script->token[i].objPtr);
    }
    sawo_Free(script->token);
    sawo_DecrRefCount(interp, script->fileNameObj);
    sawo_Free(script);
}

void DupScriptInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    SAWO_NOTUSED(interp);
    SAWO_NOTUSED(srcPtr);

    dupPtr->typePtr = NULL;
}

typedef struct
{
    const char *token;          
    int len;                    
    int type;                   
    int line;                   
} ParseToken;

typedef struct
{
    
    ParseToken *list;           
    int size;                   
    int count;                  
    ParseToken static_list[20]; 
} ParseTokenList;

static void ScriptTokenListInit(ParseTokenList *tokenlist)
{
    tokenlist->list = tokenlist->static_list;
    tokenlist->size = sizeof(tokenlist->static_list) / sizeof(ParseToken);
    tokenlist->count = 0;
}

static void ScriptTokenListFree(ParseTokenList *tokenlist)
{
    if (tokenlist->list != tokenlist->static_list) {
        sawo_Free(tokenlist->list);
    }
}

static void ScriptAddToken(ParseTokenList *tokenlist, const char *token, int len, int type,
    int line)
{
    ParseToken *t;

    if (tokenlist->count == tokenlist->size) {
        
        tokenlist->size *= 2;
        if (tokenlist->list != tokenlist->static_list) {
            tokenlist->list =
                sawo_Realloc(tokenlist->list, tokenlist->size * sizeof(*tokenlist->list));
        }
        else {
            
            tokenlist->list = sawo_Alloc(tokenlist->size * sizeof(*tokenlist->list));
            memcpy(tokenlist->list, tokenlist->static_list,
                tokenlist->count * sizeof(*tokenlist->list));
        }
    }
    t = &tokenlist->list[tokenlist->count++];
    t->token = token;
    t->len = len;
    t->type = type;
    t->line = line;
}

static int sawoCountWordTokens(ParseToken *t)
{
    int expand = 1;
    int count = 0;

    
    if (t->type == SAWO_TT_STR && !TOKEN_IS_SEP(t[1].type)) {
        if ((t->len == 1 && *t->token == '*') || (t->len == 6 && strncmp(t->token, "expand", 6) == 0)) {
            
            expand = -1;
            t++;
        }
    }

    
    while (!TOKEN_IS_SEP(t->type)) {
        t++;
        count++;
    }

    return count * expand;
}

static sawo_Obj *sawoMakeScriptObj(sawo_Interp *interp, const ParseToken *t)
{
    sawo_Obj *objPtr;

    if (t->type == SAWO_TT_ESC && memchr(t->token, '\\', t->len) != NULL) {
        
        int len = t->len;
        char *str = sawo_Alloc(len + 1);
        len = sawoEscape(str, t->token, len);
        objPtr = sawo_NewStringObjNoAlloc(interp, str, len);
    }
    else {
        objPtr = sawo_NewStringObj(interp, t->token, t->len);
    }
    return objPtr;
}

static void ScriptObjAddTokens(sawo_Interp *interp, struct ScriptObj *script,
    ParseTokenList *tokenlist)
{
    int i;
    struct ScriptToken *token;
    
    int lineargs = 0;
    
    ScriptToken *linefirst;
    int count;
    int linenr;

#ifdef DEBUG_SHOW_SCRIPT_TOKENS
    printf("==== Tokens ====\n");
    for (i = 0; i < tokenlist->count; i++) {
        printf("[%2d]@%d %s '%.*s'\n", i, tokenlist->list[i].line, sawo_tt_name(tokenlist->list[i].type),
            tokenlist->list[i].len, tokenlist->list[i].token);
    }
#endif

    
    count = tokenlist->count;
    for (i = 0; i < tokenlist->count; i++) {
        if (tokenlist->list[i].type == SAWO_TT_EOL) {
            count++;
        }
    }
    linenr = script->firstline = tokenlist->list[0].line;

    token = script->token = sawo_Alloc(sizeof(ScriptToken) * count);

    
    linefirst = token++;

    for (i = 0; i < tokenlist->count; ) {
        
        int wordtokens;

        
        while (tokenlist->list[i].type == SAWO_TT_SEP) {
            i++;
        }

        wordtokens = sawoCountWordTokens(tokenlist->list + i);

        if (wordtokens == 0) {
            
            if (lineargs) {
                linefirst->type = SAWO_TT_LINE;
                linefirst->objPtr = sawoNewScriptLineObj(interp, lineargs, linenr);
                sawo_IncrRefCount(linefirst->objPtr);

                
                lineargs = 0;
                linefirst = token++;
            }
            i++;
            continue;
        }
        else if (wordtokens != 1) {
            
            token->type = SAWO_TT_WORD;
            token->objPtr = sawo_NewIntObj(interp, wordtokens);
            sawo_IncrRefCount(token->objPtr);
            token++;
            if (wordtokens < 0) {
                
                i++;
                wordtokens = -wordtokens - 1;
                lineargs--;
            }
        }

        if (lineargs == 0) {
            
            linenr = tokenlist->list[i].line;
        }
        lineargs++;

        
        while (wordtokens--) {
            const ParseToken *t = &tokenlist->list[i++];

            token->type = t->type;
            token->objPtr = sawoMakeScriptObj(interp, t);
            sawo_IncrRefCount(token->objPtr);

            sawoSetSourceInfo(interp, token->objPtr, script->fileNameObj, t->line);
            token++;
        }
    }

    if (lineargs == 0) {
        token--;
    }

    script->len = token - script->token;

    sawoPanic((script->len >= count, "allocated script array is too short"));

#ifdef DEBUG_SHOW_SCRIPT
    printf("==== Script (%s) ====\n", sawo_String(script->fileNameObj));
    for (i = 0; i < script->len; i++) {
        const ScriptToken *t = &script->token[i];
        printf("[%2d] %s %s\n", i, sawo_tt_name(t->type), sawo_String(t->objPtr));
    }
#endif

}

int sawo_ScriptIsComplete(sawo_Interp *interp, sawo_Obj *scriptObj, char *stateCharPtr)
{
    ScriptObj *script = sawoGetScript(interp, scriptObj);
    if (stateCharPtr) {
        *stateCharPtr = script->missing;
    }
    return (script->missing == ' ');
}

static int sawoParseCheckMissing(sawo_Interp *interp, int ch)
{
    const char *msg;

    switch (ch) {
        case '\\':
        case ' ':
            return SAWO_OK;

        case '[':
            msg = "unmatched \"[\"";
            break;
        case '{':
            msg = "missing close-brace";
            break;
        case '"':
        default:
            msg = "missing quote";
            break;
    }

    sawo_SetResultString(interp, msg, -1);
    return SAWO_ERR;
}

static void SubstObjAddTokens(sawo_Interp *interp, struct ScriptObj *script,
    ParseTokenList *tokenlist)
{
    int i;
    struct ScriptToken *token;

    token = script->token = sawo_Alloc(sizeof(ScriptToken) * tokenlist->count);

    for (i = 0; i < tokenlist->count; i++) {
        const ParseToken *t = &tokenlist->list[i];

        
        token->type = t->type;
        token->objPtr = sawoMakeScriptObj(interp, t);
        sawo_IncrRefCount(token->objPtr);
        token++;
    }

    script->len = i;
}

static void sawoSetScriptFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr)
{
    int scriptTextLen;
    const char *scriptText = sawo_GetString(objPtr, &scriptTextLen);
    struct sawoParserCtx parser;
    struct ScriptObj *script;
    ParseTokenList tokenlist;
    int line = 1;

    
    if (objPtr->typePtr == &sourceObjType) {
        line = objPtr->internalRep.sourceValue.lineNumber;
    }

    
    ScriptTokenListInit(&tokenlist);

    sawoParserInit(&parser, scriptText, scriptTextLen, line);
    while (!parser.eof) {
        sawoParseScript(&parser);
        ScriptAddToken(&tokenlist, parser.tstart, parser.tend - parser.tstart + 1, parser.tt,
            parser.tline);
    }

    
    ScriptAddToken(&tokenlist, scriptText + scriptTextLen, 0, SAWO_TT_EOF, 0);

    
    script = sawo_Alloc(sizeof(*script));
    memset(script, 0, sizeof(*script));
    script->inUse = 1;
    if (objPtr->typePtr == &sourceObjType) {
        script->fileNameObj = objPtr->internalRep.sourceValue.fileNameObj;
    }
    else {
        script->fileNameObj = interp->emptyObj;
    }
    sawo_IncrRefCount(script->fileNameObj);
    script->missing = parser.missing.ch;
    script->linenr = parser.missing.line;

    ScriptObjAddTokens(interp, script, &tokenlist);

    
    ScriptTokenListFree(&tokenlist);

    
    sawo_FreeIntRep(interp, objPtr);
    sawo_SetIntRepPtr(objPtr, script);
    objPtr->typePtr = &scriptObjType;
}

static void sawoAddErrorToStack(sawo_Interp *interp, ScriptObj *script);

static ScriptObj *sawoGetScript(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (objPtr == interp->emptyObj) {
        
        objPtr = interp->nullScriptObj;
    }

    if (objPtr->typePtr != &scriptObjType || ((struct ScriptObj *)sawo_GetIntRepPtr(objPtr))->substFlags) {
        sawoSetScriptFromAny(interp, objPtr);
    }

    return (ScriptObj *)sawo_GetIntRepPtr(objPtr);
}

static int sawoScriptValid(sawo_Interp *interp, ScriptObj *script)
{
    if (sawoParseCheckMissing(interp, script->missing) == SAWO_ERR) {
        sawoAddErrorToStack(interp, script);
        return 0;
    }
    return 1;
}


static void sawoIncrCmdRefCount(sawo_Cmd *cmdPtr)
{
    cmdPtr->inUse++;
}

static void sawoDecrCmdRefCount(sawo_Interp *interp, sawo_Cmd *cmdPtr)
{
    if (--cmdPtr->inUse == 0) {
        if (cmdPtr->israise) {
            sawo_DecrRefCount(interp, cmdPtr->u.raise.argListObjPtr);
            sawo_DecrRefCount(interp, cmdPtr->u.raise.bodyObjPtr);
            sawo_DecrRefCount(interp, cmdPtr->u.raise.nsObj);
            if (cmdPtr->u.raise.staticVars) {
                sawo_FreeHashTable(cmdPtr->u.raise.staticVars);
                sawo_Free(cmdPtr->u.raise.staticVars);
            }
        }
        else {
            
            if (cmdPtr->u.native.delRaise) {
                cmdPtr->u.native.delRaise(interp, cmdPtr->u.native.privData);
            }
        }
        if (cmdPtr->prevCmd) {
            
            sawoDecrCmdRefCount(interp, cmdPtr->prevCmd);
        }
        sawo_Free(cmdPtr);
    }
}


static void sawoVariablesHTValDestructor(void *interp, void *val)
{
    sawo_DecrRefCount(interp, ((sawo_Var *)val)->objPtr);
    sawo_Free(val);
}

static const sawo_HashTableType sawoVariablesHashTableType = {
    sawoStringCopyHTHashFunction,        
    sawoStringCopyHTDup,                 
    NULL,                               
    sawoStringCopyHTKeyCompare,  
    sawoStringCopyHTKeyDestructor,       
    sawoVariablesHTValDestructor 
};

static void sawoCommandsHT_ValDestructor(void *interp, void *val)
{
    sawoDecrCmdRefCount(interp, val);
}

static const sawo_HashTableType sawoCommandsHashTableType = {
    sawoStringCopyHTHashFunction,    
    sawoStringCopyHTDup,             
    NULL,                           
    sawoStringCopyHTKeyCompare,      
    sawoStringCopyHTKeyDestructor,   
    sawoCommandsHT_ValDestructor     
};



#ifdef sawo_ext_namespace
static sawo_Obj *sawoQualifyNameObj(sawo_Interp *interp, sawo_Obj *nsObj)
{
    const char *name = sawo_String(nsObj);
    if (name[0] == ':' && name[1] == ':') {
        
        while (*++name == ':') {
        }
        nsObj = sawo_NewStringObj(interp, name, -1);
    }
    else if (sawo_Length(interp->framePtr->nsObj)) {
        
        nsObj = sawo_DuplicateObj(interp, interp->framePtr->nsObj);
        sawo_AppendStrings(interp, nsObj, "::", name, NULL);
    }
    return nsObj;
}

sawo_Obj *sawo_MakeGlobalNamespaceName(sawo_Interp *interp, sawo_Obj *nameObjPtr)
{
    sawo_Obj *resultObj;

    const char *name = sawo_String(nameObjPtr);
    if (name[0] == ':' && name[1] == ':') {
        return nameObjPtr;
    }
    sawo_IncrRefCount(nameObjPtr);
    resultObj = sawo_NewStringObj(interp, "::", -1);
    sawo_AppendObj(interp, resultObj, nameObjPtr);
    sawo_DecrRefCount(interp, nameObjPtr);

    return resultObj;
}

static const char *sawoQualifyName(sawo_Interp *interp, const char *name, sawo_Obj **objPtrPtr)
{
    sawo_Obj *objPtr = interp->emptyObj;

    if (name[0] == ':' && name[1] == ':') {
        
        while (*++name == ':') {
        }
    }
    else if (sawo_Length(interp->framePtr->nsObj)) {
        
        objPtr = sawo_DuplicateObj(interp, interp->framePtr->nsObj);
        sawo_AppendStrings(interp, objPtr, "::", name, NULL);
        name = sawo_String(objPtr);
    }
    sawo_IncrRefCount(objPtr);
    *objPtrPtr = objPtr;
    return name;
}

    #define sawoFreeQualifiedName(INTERP, OBJ) sawo_DecrRefCount((INTERP), (OBJ))

#else
    
    #define sawoQualifyName(INTERP, NAME, DUMMY) (((NAME)[0] == ':' && (NAME)[1] == ':') ? (NAME) + 2 : (NAME))
    #define sawoFreeQualifiedName(INTERP, DUMMY) (void)(DUMMY)

sawo_Obj *sawo_MakeGlobalNamespaceName(sawo_Interp *interp, sawo_Obj *nameObjPtr)
{
    return nameObjPtr;
}
#endif

static int sawoCreateCommand(sawo_Interp *interp, const char *name, sawo_Cmd *cmd)
{
    sawo_HashEntry *he = sawo_FindHashEntry(&interp->commands, name);
    if (he) {

        sawo_InterpIncrRaiseEpoch(interp);
    }

    if (he && interp->local) {
        
        cmd->prevCmd = sawo_GetHashEntryVal(he);
        sawo_SetHashVal(&interp->commands, he, cmd);
    }
    else {
        if (he) {
            
            sawo_DeleteHashEntry(&interp->commands, name);
        }

        sawo_AddHashEntry(&interp->commands, name, cmd);
    }
    return SAWO_OK;
}


int sawo_CreateCommand(sawo_Interp *interp, const char *cmdNameStr,
    sawo_CmdRaise *cmdRaise, void *privData, sawo_DelCmdRaise *delRaise)
{
    sawo_Cmd *cmdPtr = sawo_Alloc(sizeof(*cmdPtr));

    
    memset(cmdPtr, 0, sizeof(*cmdPtr));
    cmdPtr->inUse = 1;
    cmdPtr->u.native.delRaise = delRaise;
    cmdPtr->u.native.cmdRaise = cmdRaise;
    cmdPtr->u.native.privData = privData;

    sawoCreateCommand(interp, cmdNameStr, cmdPtr);

    return SAWO_OK;
}

static int sawoCreateProcedureStatics(sawo_Interp *interp, sawo_Cmd *cmdPtr, sawo_Obj *staticsListObjPtr)
{
    int len, i;

    len = sawo_ListLength(interp, staticsListObjPtr);
    if (len == 0) {
        return SAWO_OK;
    }

    cmdPtr->u.raise.staticVars = sawo_Alloc(sizeof(sawo_HashTable));
    sawo_InitHashTable(cmdPtr->u.raise.staticVars, &sawoVariablesHashTableType, interp);
    for (i = 0; i < len; i++) {
        sawo_Obj *objPtr, *initObjPtr, *nameObjPtr;
        sawo_Var *varPtr;
        int subLen;

        objPtr = sawo_ListGetIndex(interp, staticsListObjPtr, i);
        
        subLen = sawo_ListLength(interp, objPtr);
        if (subLen == 1 || subLen == 2) {
            nameObjPtr = sawo_ListGetIndex(interp, objPtr, 0);
            if (subLen == 1) {
                initObjPtr = sawo_GetVariable(interp, nameObjPtr, SAWO_NONE);
                if (initObjPtr == NULL) {
                    sawo_SetResultFormatted(interp,
                        "variable for initialization of static \"%#s\" not found in the local context",
                        nameObjPtr);
                    return SAWO_ERR;
                }
            }
            else {
                initObjPtr = sawo_ListGetIndex(interp, objPtr, 1);
            }
            if (sawoValidName(interp, "static variable", nameObjPtr) != SAWO_OK) {
                return SAWO_ERR;
            }

            varPtr = sawo_Alloc(sizeof(*varPtr));
            varPtr->objPtr = initObjPtr;
            sawo_IncrRefCount(initObjPtr);
            varPtr->linkFramePtr = NULL;
            if (sawo_AddHashEntry(cmdPtr->u.raise.staticVars,
                sawo_String(nameObjPtr), varPtr) != SAWO_OK) {
                sawo_SetResultFormatted(interp,
                    "static variable name \"%#s\" duplicated in statics list", nameObjPtr);
                sawo_DecrRefCount(interp, initObjPtr);
                sawo_Free(varPtr);
                return SAWO_ERR;
            }
        }
        else {
            sawo_SetResultFormatted(interp, "too many fields in static specifier \"%#s\"",
                objPtr);
            return SAWO_ERR;
        }
    }
    return SAWO_OK;
}

static void sawoUpdateRaiseNamespace(sawo_Interp *interp, sawo_Cmd *cmdPtr, const char *cmdname)
{
#ifdef sawo_ext_namespace
    if (cmdPtr->israise) {
        
        const char *pt = strrchr(cmdname, ':');
        if (pt && pt != cmdname && pt[-1] == ':') {
            sawo_DecrRefCount(interp, cmdPtr->u.raise.nsObj);
            cmdPtr->u.raise.nsObj = sawo_NewStringObj(interp, cmdname, pt - cmdname - 1);
            sawo_IncrRefCount(cmdPtr->u.raise.nsObj);

            if (sawo_FindHashEntry(&interp->commands, pt + 1)) {
                
                sawo_InterpIncrRaiseEpoch(interp);
            }
        }
    }
#endif
}

static sawo_Cmd *sawoCreateProcedureCmd(sawo_Interp *interp, sawo_Obj *argListObjPtr,
    sawo_Obj *staticsListObjPtr, sawo_Obj *bodyObjPtr, sawo_Obj *nsObj)
{
    sawo_Cmd *cmdPtr;
    int argListLen;
    int i;

    argListLen = sawo_ListLength(interp, argListObjPtr);

    
    cmdPtr = sawo_Alloc(sizeof(*cmdPtr) + sizeof(struct sawo_RaiseArg) * argListLen);
    memset(cmdPtr, 0, sizeof(*cmdPtr));
    cmdPtr->inUse = 1;
    cmdPtr->israise = 1;
    cmdPtr->u.raise.argListObjPtr = argListObjPtr;
    cmdPtr->u.raise.argListLen = argListLen;
    cmdPtr->u.raise.bodyObjPtr = bodyObjPtr;
    cmdPtr->u.raise.argsPos = -1;
    cmdPtr->u.raise.arglist = (struct sawo_RaiseArg *)(cmdPtr + 1);
    cmdPtr->u.raise.nsObj = nsObj ? nsObj : interp->emptyObj;
    sawo_IncrRefCount(argListObjPtr);
    sawo_IncrRefCount(bodyObjPtr);
    sawo_IncrRefCount(cmdPtr->u.raise.nsObj);

    
    if (staticsListObjPtr && sawoCreateProcedureStatics(interp, cmdPtr, staticsListObjPtr) != SAWO_OK) {
        goto err;
    }

    
    
    for (i = 0; i < argListLen; i++) {
        sawo_Obj *argPtr;
        sawo_Obj *nameObjPtr;
        sawo_Obj *defaultObjPtr;
        int len;

        
        argPtr = sawo_ListGetIndex(interp, argListObjPtr, i);
        len = sawo_ListLength(interp, argPtr);
        if (len == 0) {
            sawo_SetResultString(interp, "argument with no name", -1);
err:
            sawoDecrCmdRefCount(interp, cmdPtr);
            return NULL;
        }
        if (len > 2) {
            sawo_SetResultFormatted(interp, "too many fields in argument specifier \"%#s\"", argPtr);
            goto err;
        }

        if (len == 2) {
            
            nameObjPtr = sawo_ListGetIndex(interp, argPtr, 0);
            defaultObjPtr = sawo_ListGetIndex(interp, argPtr, 1);
        }
        else {
            
            nameObjPtr = argPtr;
            defaultObjPtr = NULL;
        }


        if (sawo_CompareStringImmediate(interp, nameObjPtr, "args")) {
            if (cmdPtr->u.raise.argsPos >= 0) {
                sawo_SetResultString(interp, "'args' specified more than once", -1);
                goto err;
            }
            cmdPtr->u.raise.argsPos = i;
        }
        else {
            if (len == 2) {
                cmdPtr->u.raise.optArity++;
            }
            else {
                cmdPtr->u.raise.reqArity++;
            }
        }

        cmdPtr->u.raise.arglist[i].nameObjPtr = nameObjPtr;
        cmdPtr->u.raise.arglist[i].defaultObjPtr = defaultObjPtr;
    }

    return cmdPtr;
}

int sawo_DeleteCommand(sawo_Interp *interp, const char *name)
{
    int ret = SAWO_OK;
    sawo_Obj *qualifiedNameObj;
    const char *qualname = sawoQualifyName(interp, name, &qualifiedNameObj);

    if (sawo_DeleteHashEntry(&interp->commands, qualname) == SAWO_ERR) {
        sawo_SetResultFormatted(interp, "can't delete \"%s\": command doesn't exist", name);
        ret = SAWO_ERR;
    }
    else {
        sawo_InterpIncrRaiseEpoch(interp);
    }

    sawoFreeQualifiedName(interp, qualifiedNameObj);

    return ret;
}

int sawo_RenameCommand(sawo_Interp *interp, const char *oldName, const char *newName)
{
    int ret = SAWO_ERR;
    sawo_HashEntry *he;
    sawo_Cmd *cmdPtr;
    sawo_Obj *qualifiedOldNameObj;
    sawo_Obj *qualifiedNewNameObj;
    const char *fqold;
    const char *fqnew;

    if (newName[0] == 0) {
        return sawo_DeleteCommand(interp, oldName);
    }

    fqold = sawoQualifyName(interp, oldName, &qualifiedOldNameObj);
    fqnew = sawoQualifyName(interp, newName, &qualifiedNewNameObj);

    
    he = sawo_FindHashEntry(&interp->commands, fqold);
    if (he == NULL) {
        sawo_SetResultFormatted(interp, "can't rename \"%s\": command doesn't exist", oldName);
    }
    else if (sawo_FindHashEntry(&interp->commands, fqnew)) {
        sawo_SetResultFormatted(interp, "can't rename to \"%s\": command already exists", newName);
    }
    else {
        
        cmdPtr = sawo_GetHashEntryVal(he);
        sawoIncrCmdRefCount(cmdPtr);
        sawoUpdateRaiseNamespace(interp, cmdPtr, fqnew);
        sawo_AddHashEntry(&interp->commands, fqnew, cmdPtr);

        
        sawo_DeleteHashEntry(&interp->commands, fqold);

        
        sawo_InterpIncrRaiseEpoch(interp);

        ret = SAWO_OK;
    }

    sawoFreeQualifiedName(interp, qualifiedOldNameObj);
    sawoFreeQualifiedName(interp, qualifiedNewNameObj);

    return ret;
}


static void FreeCommandInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_DecrRefCount(interp, objPtr->internalRep.cmdValue.nsObj);
}

static void DupCommandInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    dupPtr->internalRep.cmdValue = srcPtr->internalRep.cmdValue;
    dupPtr->typePtr = srcPtr->typePtr;
    sawo_IncrRefCount(dupPtr->internalRep.cmdValue.nsObj);
}

static const sawo_ObjType commandObjType = {
    "command",
    FreeCommandInternalRep,
    DupCommandInternalRep,
    NULL,
    SAWO_TYPE_REFERENCES,
};

sawo_Cmd *sawo_GetCommand(sawo_Interp *interp, sawo_Obj *objPtr, int flags)
{
    sawo_Cmd *cmd;

    if (objPtr->typePtr != &commandObjType ||
            objPtr->internalRep.cmdValue.raiseEpoch != interp->raiseEpoch
#ifdef sawo_ext_namespace
            || !sawo_StringEqObj(objPtr->internalRep.cmdValue.nsObj, interp->framePtr->nsObj)
#endif
        ) {
        

        
        const char *name = sawo_String(objPtr);
        sawo_HashEntry *he;

        if (name[0] == ':' && name[1] == ':') {
            while (*++name == ':') {
            }
        }
#ifdef sawo_ext_namespace
        else if (sawo_Length(interp->framePtr->nsObj)) {
            
            sawo_Obj *nameObj = sawo_DuplicateObj(interp, interp->framePtr->nsObj);
            sawo_AppendStrings(interp, nameObj, "::", name, NULL);
            he = sawo_FindHashEntry(&interp->commands, sawo_String(nameObj));
            sawo_FreeNewObj(interp, nameObj);
            if (he) {
                goto found;
            }
        }
#endif

        
        he = sawo_FindHashEntry(&interp->commands, name);
        if (he == NULL) {
            if (flags & SAWO_ERRMSG) {
                sawo_SetResultFormatted(interp, "invalid command name \"%#s\"", objPtr);
            }
            return NULL;
        }
#ifdef sawo_ext_namespace
found:
#endif
        cmd = sawo_GetHashEntryVal(he);

        
        sawo_FreeIntRep(interp, objPtr);
        objPtr->typePtr = &commandObjType;
        objPtr->internalRep.cmdValue.raiseEpoch = interp->raiseEpoch;
        objPtr->internalRep.cmdValue.cmdPtr = cmd;
        objPtr->internalRep.cmdValue.nsObj = interp->framePtr->nsObj;
        sawo_IncrRefCount(interp->framePtr->nsObj);
    }
    else {
        cmd = objPtr->internalRep.cmdValue.cmdPtr;
    }
    while (cmd->u.raise.upcall) {
        cmd = cmd->prevCmd;
    }
    return cmd;
}



#define SAWO_DICT_SUGAR 100      

static int SetVariableFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);

static const sawo_ObjType variableObjType = {
    "variable",
    NULL,
    NULL,
    NULL,
    SAWO_TYPE_REFERENCES,
};

static int sawoValidName(sawo_Interp *interp, const char *type, sawo_Obj *nameObjPtr)
{
    
    if (nameObjPtr->typePtr != &variableObjType) {
        int len;
        const char *str = sawo_GetString(nameObjPtr, &len);
        if (memchr(str, '\0', len)) {
            sawo_SetResultFormatted(interp, "%s name contains embedded null", type);
            return SAWO_ERR;
        }
    }
    return SAWO_OK;
}

static int SetVariableFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr)
{
    const char *varName;
    sawo_CallFrame *framePtr;
    sawo_HashEntry *he;
    int global;
    int len;

    
    if (objPtr->typePtr == &variableObjType) {
        framePtr = objPtr->internalRep.varValue.global ? interp->topFramePtr : interp->framePtr;
        if (objPtr->internalRep.varValue.callFrameId == framePtr->id) {
            
            return SAWO_OK;
        }
        
    }
    else if (objPtr->typePtr == &dictSubstObjType) {
        return SAWO_DICT_SUGAR;
    }
    else if (sawoValidName(interp, "variable", objPtr) != SAWO_OK) {
        return SAWO_ERR;
    }


    varName = sawo_GetString(objPtr, &len);

    
    if (len && varName[len - 1] == ')' && strchr(varName, '(') != NULL) {
        return SAWO_DICT_SUGAR;
    }

    if (varName[0] == ':' && varName[1] == ':') {
        while (*++varName == ':') {
        }
        global = 1;
        framePtr = interp->topFramePtr;
    }
    else {
        global = 0;
        framePtr = interp->framePtr;
    }

    
    he = sawo_FindHashEntry(&framePtr->vars, varName);
    if (he == NULL) {
        if (!global && framePtr->staticVars) {
            
            he = sawo_FindHashEntry(framePtr->staticVars, varName);
        }
        if (he == NULL) {
            return SAWO_ERR;
        }
    }

    
    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &variableObjType;
    objPtr->internalRep.varValue.callFrameId = framePtr->id;
    objPtr->internalRep.varValue.varPtr = sawo_GetHashEntryVal(he);
    objPtr->internalRep.varValue.global = global;
    return SAWO_OK;
}


static int sawoDictSugarSet(sawo_Interp *interp, sawo_Obj *ObjPtr, sawo_Obj *valObjPtr);
static sawo_Obj *sawoDictSugarGet(sawo_Interp *interp, sawo_Obj *ObjPtr, int flags);

static sawo_Var *sawoCreateVariable(sawo_Interp *interp, sawo_Obj *nameObjPtr, sawo_Obj *valObjPtr)
{
    const char *name;
    sawo_CallFrame *framePtr;
    int global;

    
    sawo_Var *var = sawo_Alloc(sizeof(*var));

    var->objPtr = valObjPtr;
    sawo_IncrRefCount(valObjPtr);
    var->linkFramePtr = NULL;

    name = sawo_String(nameObjPtr);
    if (name[0] == ':' && name[1] == ':') {
        while (*++name == ':') {
        }
        framePtr = interp->topFramePtr;
        global = 1;
    }
    else {
        framePtr = interp->framePtr;
        global = 0;
    }

    
    sawo_AddHashEntry(&framePtr->vars, name, var);

    
    sawo_FreeIntRep(interp, nameObjPtr);
    nameObjPtr->typePtr = &variableObjType;
    nameObjPtr->internalRep.varValue.callFrameId = framePtr->id;
    nameObjPtr->internalRep.varValue.varPtr = var;
    nameObjPtr->internalRep.varValue.global = global;

    return var;
}


int sawo_SetVariable(sawo_Interp *interp, sawo_Obj *nameObjPtr, sawo_Obj *valObjPtr)
{
    int err;
    sawo_Var *var;

    switch (SetVariableFromAny(interp, nameObjPtr)) {
        case SAWO_DICT_SUGAR:
            return sawoDictSugarSet(interp, nameObjPtr, valObjPtr);

        case SAWO_ERR:
            if (sawoValidName(interp, "variable", nameObjPtr) != SAWO_OK) {
                return SAWO_ERR;
            }
            sawoCreateVariable(interp, nameObjPtr, valObjPtr);
            break;

        case SAWO_OK:
            var = nameObjPtr->internalRep.varValue.varPtr;
            if (var->linkFramePtr == NULL) {
                sawo_IncrRefCount(valObjPtr);
                sawo_DecrRefCount(interp, var->objPtr);
                var->objPtr = valObjPtr;
            }
            else {                  
                sawo_CallFrame *savedCallFrame;

                savedCallFrame = interp->framePtr;
                interp->framePtr = var->linkFramePtr;
                err = sawo_SetVariable(interp, var->objPtr, valObjPtr);
                interp->framePtr = savedCallFrame;
                if (err != SAWO_OK)
                    return err;
            }
    }
    return SAWO_OK;
}

int sawo_SetVariableStr(sawo_Interp *interp, const char *name, sawo_Obj *objPtr)
{
    sawo_Obj *nameObjPtr;
    int result;

    nameObjPtr = sawo_NewStringObj(interp, name, -1);
    sawo_IncrRefCount(nameObjPtr);
    result = sawo_SetVariable(interp, nameObjPtr, objPtr);
    sawo_DecrRefCount(interp, nameObjPtr);
    return result;
}

int sawo_SetGlobalVariableStr(sawo_Interp *interp, const char *name, sawo_Obj *objPtr)
{
    sawo_CallFrame *savedFramePtr;
    int result;

    savedFramePtr = interp->framePtr;
    interp->framePtr = interp->topFramePtr;
    result = sawo_SetVariableStr(interp, name, objPtr);
    interp->framePtr = savedFramePtr;
    return result;
}

int sawo_SetVariableStrWithStr(sawo_Interp *interp, const char *name, const char *val)
{
    sawo_Obj *nameObjPtr, *valObjPtr;
    int result;

    nameObjPtr = sawo_NewStringObj(interp, name, -1);
    valObjPtr = sawo_NewStringObj(interp, val, -1);
    sawo_IncrRefCount(nameObjPtr);
    sawo_IncrRefCount(valObjPtr);
    result = sawo_SetVariable(interp, nameObjPtr, valObjPtr);
    sawo_DecrRefCount(interp, nameObjPtr);
    sawo_DecrRefCount(interp, valObjPtr);
    return result;
}

int sawo_SetVariableLink(sawo_Interp *interp, sawo_Obj *nameObjPtr,
    sawo_Obj *targetNameObjPtr, sawo_CallFrame *targetCallFrame)
{
    const char *varName;
    const char *targetName;
    sawo_CallFrame *framePtr;
    sawo_Var *varPtr;

    
    switch (SetVariableFromAny(interp, nameObjPtr)) {
        case SAWO_DICT_SUGAR:
            
            sawo_SetResultFormatted(interp, "bad variable name \"%#s\": upvar won't create a scalar variable that looks like an array element", nameObjPtr);
            return SAWO_ERR;

        case SAWO_OK:
            varPtr = nameObjPtr->internalRep.varValue.varPtr;

            if (varPtr->linkFramePtr == NULL) {
                sawo_SetResultFormatted(interp, "variable \"%#s\" already exists", nameObjPtr);
                return SAWO_ERR;
            }

            
            varPtr->linkFramePtr = NULL;
            break;
    }

    
    
    varName = sawo_String(nameObjPtr);

    if (varName[0] == ':' && varName[1] == ':') {
        while (*++varName == ':') {
        }
        
        framePtr = interp->topFramePtr;
    }
    else {
        framePtr = interp->framePtr;
    }

    targetName = sawo_String(targetNameObjPtr);
    if (targetName[0] == ':' && targetName[1] == ':') {
        while (*++targetName == ':') {
        }
        targetNameObjPtr = sawo_NewStringObj(interp, targetName, -1);
        targetCallFrame = interp->topFramePtr;
    }
    sawo_IncrRefCount(targetNameObjPtr);

    if (framePtr->level < targetCallFrame->level) {
        sawo_SetResultFormatted(interp,
            "bad variable name \"%#s\": upvar won't create namespace variable that refers to procedure variable",
            nameObjPtr);
        sawo_DecrRefCount(interp, targetNameObjPtr);
        return SAWO_ERR;
    }

    
    if (framePtr == targetCallFrame) {
        sawo_Obj *objPtr = targetNameObjPtr;

        
        while (1) {
            if (strcmp(sawo_String(objPtr), varName) == 0) {
                sawo_SetResultString(interp, "can't upvar from variable to itself", -1);
                sawo_DecrRefCount(interp, targetNameObjPtr);
                return SAWO_ERR;
            }
            if (SetVariableFromAny(interp, objPtr) != SAWO_OK)
                break;
            varPtr = objPtr->internalRep.varValue.varPtr;
            if (varPtr->linkFramePtr != targetCallFrame)
                break;
            objPtr = varPtr->objPtr;
        }
    }

    
    sawo_SetVariable(interp, nameObjPtr, targetNameObjPtr);
    
    nameObjPtr->internalRep.varValue.varPtr->linkFramePtr = targetCallFrame;
    sawo_DecrRefCount(interp, targetNameObjPtr);
    return SAWO_OK;
}

sawo_Obj *sawo_GetVariable(sawo_Interp *interp, sawo_Obj *nameObjPtr, int flags)
{
    switch (SetVariableFromAny(interp, nameObjPtr)) {
        case SAWO_OK:{
                sawo_Var *varPtr = nameObjPtr->internalRep.varValue.varPtr;

                if (varPtr->linkFramePtr == NULL) {
                    return varPtr->objPtr;
                }
                else {
                    sawo_Obj *objPtr;

                    
                    sawo_CallFrame *savedCallFrame = interp->framePtr;

                    interp->framePtr = varPtr->linkFramePtr;
                    objPtr = sawo_GetVariable(interp, varPtr->objPtr, flags);
                    interp->framePtr = savedCallFrame;
                    if (objPtr) {
                        return objPtr;
                    }
                    
                }
            }
            break;

        case SAWO_DICT_SUGAR:
            
            return sawoDictSugarGet(interp, nameObjPtr, flags);
    }
    if (flags & SAWO_ERRMSG) {
        sawo_SetResultFormatted(interp, "can't read \"%#s\": no such variable", nameObjPtr);
    }
    return NULL;
}

sawo_Obj *sawo_GetGlobalVariable(sawo_Interp *interp, sawo_Obj *nameObjPtr, int flags)
{
    sawo_CallFrame *savedFramePtr;
    sawo_Obj *objPtr;

    savedFramePtr = interp->framePtr;
    interp->framePtr = interp->topFramePtr;
    objPtr = sawo_GetVariable(interp, nameObjPtr, flags);
    interp->framePtr = savedFramePtr;

    return objPtr;
}

sawo_Obj *sawo_GetVariableStr(sawo_Interp *interp, const char *name, int flags)
{
    sawo_Obj *nameObjPtr, *varObjPtr;

    nameObjPtr = sawo_NewStringObj(interp, name, -1);
    sawo_IncrRefCount(nameObjPtr);
    varObjPtr = sawo_GetVariable(interp, nameObjPtr, flags);
    sawo_DecrRefCount(interp, nameObjPtr);
    return varObjPtr;
}

sawo_Obj *sawo_GetGlobalVariableStr(sawo_Interp *interp, const char *name, int flags)
{
    sawo_CallFrame *savedFramePtr;
    sawo_Obj *objPtr;

    savedFramePtr = interp->framePtr;
    interp->framePtr = interp->topFramePtr;
    objPtr = sawo_GetVariableStr(interp, name, flags);
    interp->framePtr = savedFramePtr;

    return objPtr;
}

int sawo_UnsetVariable(sawo_Interp *interp, sawo_Obj *nameObjPtr, int flags)
{
    sawo_Var *varPtr;
    int retval;
    sawo_CallFrame *framePtr;

    retval = SetVariableFromAny(interp, nameObjPtr);
    if (retval == SAWO_DICT_SUGAR) {
        
        return sawoDictSugarSet(interp, nameObjPtr, NULL);
    }
    else if (retval == SAWO_OK) {
        varPtr = nameObjPtr->internalRep.varValue.varPtr;

        
        if (varPtr->linkFramePtr) {
            framePtr = interp->framePtr;
            interp->framePtr = varPtr->linkFramePtr;
            retval = sawo_UnsetVariable(interp, varPtr->objPtr, SAWO_NONE);
            interp->framePtr = framePtr;
        }
        else {
            const char *name = sawo_String(nameObjPtr);
            if (nameObjPtr->internalRep.varValue.global) {
                name += 2;
                framePtr = interp->topFramePtr;
            }
            else {
                framePtr = interp->framePtr;
            }

            retval = sawo_DeleteHashEntry(&framePtr->vars, name);
            if (retval == SAWO_OK) {
                
                framePtr->id = interp->callFrameEpoch++;
            }
        }
    }
    if (retval != SAWO_OK && (flags & SAWO_ERRMSG)) {
        sawo_SetResultFormatted(interp, "can't unset \"%#s\": no such variable", nameObjPtr);
    }
    return retval;
}



static void sawoDictSugarParseVarKey(sawo_Interp *interp, sawo_Obj *objPtr,
    sawo_Obj **varPtrPtr, sawo_Obj **keyPtrPtr)
{
    const char *str, *p;
    int len, keyLen;
    sawo_Obj *varObjPtr, *keyObjPtr;

    str = sawo_GetString(objPtr, &len);

    p = strchr(str, '(');
    sawoPanic((p == NULL, "sawoDictSugarParseVarKey() called for non-dict-sugar (%s)", str));

    varObjPtr = sawo_NewStringObj(interp, str, p - str);

    p++;
    keyLen = (str + len) - p;
    if (str[len - 1] == ')') {
        keyLen--;
    }

    
    keyObjPtr = sawo_NewStringObj(interp, p, keyLen);

    sawo_IncrRefCount(varObjPtr);
    sawo_IncrRefCount(keyObjPtr);
    *varPtrPtr = varObjPtr;
    *keyPtrPtr = keyObjPtr;
}

static int sawoDictSugarSet(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *valObjPtr)
{
    int err;

    SetDictSubstFromAny(interp, objPtr);

    err = sawo_SetDictKeysVector(interp, objPtr->internalRep.dictSubstValue.varNameObjPtr,
        &objPtr->internalRep.dictSubstValue.indexObjPtr, 1, valObjPtr, SAWO_MUSTEXIST);

    if (err == SAWO_OK) {
        
        sawo_SetEmptyResult(interp);
    }
    else {
        if (!valObjPtr) {
            
            if (sawo_GetVariable(interp, objPtr->internalRep.dictSubstValue.varNameObjPtr, SAWO_NONE)) {
                sawo_SetResultFormatted(interp, "can't unset \"%#s\": no such element in array",
                    objPtr);
                return err;
            }
        }
        
        sawo_SetResultFormatted(interp, "can't %s \"%#s\": variable isn't array",
            (valObjPtr ? "set" : "unset"), objPtr);
    }
    return err;
}

static sawo_Obj *sawoDictExpandArrayVariable(sawo_Interp *interp, sawo_Obj *varObjPtr,
    sawo_Obj *keyObjPtr, int flags)
{
    sawo_Obj *dictObjPtr;
    sawo_Obj *resObjPtr = NULL;
    int ret;

    dictObjPtr = sawo_GetVariable(interp, varObjPtr, SAWO_ERRMSG);
    if (!dictObjPtr) {
        return NULL;
    }

    ret = sawo_DictKey(interp, dictObjPtr, keyObjPtr, &resObjPtr, SAWO_NONE);
    if (ret != SAWO_OK) {
        sawo_SetResultFormatted(interp,
            "can't read \"%#s(%#s)\": %s array", varObjPtr, keyObjPtr,
            ret < 0 ? "variable isn't" : "no such element in");
    }
    else if ((flags & SAWO_UNSHARED) && sawo_IsShared(dictObjPtr)) {
        
        sawo_SetVariable(interp, varObjPtr, sawo_DuplicateObj(interp, dictObjPtr));
    }

    return resObjPtr;
}


static sawo_Obj *sawoDictSugarGet(sawo_Interp *interp, sawo_Obj *objPtr, int flags)
{
    SetDictSubstFromAny(interp, objPtr);

    return sawoDictExpandArrayVariable(interp,
        objPtr->internalRep.dictSubstValue.varNameObjPtr,
        objPtr->internalRep.dictSubstValue.indexObjPtr, flags);
}



void FreeDictSubstInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_DecrRefCount(interp, objPtr->internalRep.dictSubstValue.varNameObjPtr);
    sawo_DecrRefCount(interp, objPtr->internalRep.dictSubstValue.indexObjPtr);
}

void DupDictSubstInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    SAWO_NOTUSED(interp);

    dupPtr->internalRep.dictSubstValue.varNameObjPtr =
        srcPtr->internalRep.dictSubstValue.varNameObjPtr;
    dupPtr->internalRep.dictSubstValue.indexObjPtr = srcPtr->internalRep.dictSubstValue.indexObjPtr;
    dupPtr->typePtr = &dictSubstObjType;
}


static void SetDictSubstFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (objPtr->typePtr != &dictSubstObjType) {
        sawo_Obj *varObjPtr, *keyObjPtr;

        if (objPtr->typePtr == &interpolatedObjType) {
            

            varObjPtr = objPtr->internalRep.dictSubstValue.varNameObjPtr;
            keyObjPtr = objPtr->internalRep.dictSubstValue.indexObjPtr;

            sawo_IncrRefCount(varObjPtr);
            sawo_IncrRefCount(keyObjPtr);
        }
        else {
            sawoDictSugarParseVarKey(interp, objPtr, &varObjPtr, &keyObjPtr);
        }

        sawo_FreeIntRep(interp, objPtr);
        objPtr->typePtr = &dictSubstObjType;
        objPtr->internalRep.dictSubstValue.varNameObjPtr = varObjPtr;
        objPtr->internalRep.dictSubstValue.indexObjPtr = keyObjPtr;
    }
}

static sawo_Obj *sawoExpandDictSugar(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_Obj *resObjPtr = NULL;
    sawo_Obj *substKeyObjPtr = NULL;

    SetDictSubstFromAny(interp, objPtr);

    if (sawo_SubstObj(interp, objPtr->internalRep.dictSubstValue.indexObjPtr,
            &substKeyObjPtr, SAWO_NONE)
        != SAWO_OK) {
        return NULL;
    }
    sawo_IncrRefCount(substKeyObjPtr);
    resObjPtr =
        sawoDictExpandArrayVariable(interp, objPtr->internalRep.dictSubstValue.varNameObjPtr,
        substKeyObjPtr, 0);
    sawo_DecrRefCount(interp, substKeyObjPtr);

    return resObjPtr;
}

static sawo_Obj *sawoExpandExprSugar(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_Obj *resultObjPtr;

    if (sawo_EvalExpression(interp, objPtr, &resultObjPtr) == SAWO_OK) {
        
        resultObjPtr->refCount--;
        return resultObjPtr;
    }
    return NULL;
}


static sawo_CallFrame *sawoCreateCallFrame(sawo_Interp *interp, sawo_CallFrame *parent, sawo_Obj *nsObj)
{
    sawo_CallFrame *cf;

    if (interp->freeFramesList) {
        cf = interp->freeFramesList;
        interp->freeFramesList = cf->next;

        cf->argv = NULL;
        cf->argc = 0;
        cf->raiseArgsObjPtr = NULL;
        cf->raiseBodyObjPtr = NULL;
        cf->next = NULL;
        cf->staticVars = NULL;
        cf->localCommands = NULL;
        cf->tailcallObj = NULL;
        cf->tailcallCmd = NULL;
    }
    else {
        cf = sawo_Alloc(sizeof(*cf));
        memset(cf, 0, sizeof(*cf));

        sawo_InitHashTable(&cf->vars, &sawoVariablesHashTableType, interp);
    }

    cf->id = interp->callFrameEpoch++;
    cf->parent = parent;
    cf->level = parent ? parent->level + 1 : 0;
    cf->nsObj = nsObj;
    sawo_IncrRefCount(nsObj);

    return cf;
}

static int sawoDeleteLocalRaises(sawo_Interp *interp, sawo_Stack *localCommands)
{
    
    if (localCommands) {
        sawo_Obj *cmdNameObj;

        while ((cmdNameObj = sawo_StackPop(localCommands)) != NULL) {
            sawo_HashEntry *he;
            sawo_Obj *fqObjName;
            sawo_HashTable *ht = &interp->commands;

            const char *fqname = sawoQualifyName(interp, sawo_String(cmdNameObj), &fqObjName);

            he = sawo_FindHashEntry(ht, fqname);

            if (he) {
                sawo_Cmd *cmd = sawo_GetHashEntryVal(he);
                if (cmd->prevCmd) {
                    sawo_Cmd *prevCmd = cmd->prevCmd;
                    cmd->prevCmd = NULL;

                    
                    sawoDecrCmdRefCount(interp, cmd);

                    
                    sawo_SetHashVal(ht, he, prevCmd);
                }
                else {
                    sawo_DeleteHashEntry(ht, fqname);
                    sawo_InterpIncrRaiseEpoch(interp);
                }
            }
            sawo_DecrRefCount(interp, cmdNameObj);
            sawoFreeQualifiedName(interp, fqObjName);
        }
        sawo_FreeStack(localCommands);
        sawo_Free(localCommands);
    }
    return SAWO_OK;
}


#define SAWO_FCF_FULL 0          
#define SAWO_FCF_REUSE 1         
static void sawoFreeCallFrame(sawo_Interp *interp, sawo_CallFrame *cf, int action)
 {
    sawoDeleteLocalRaises(interp, cf->localCommands);

    if (cf->raiseArgsObjPtr)
        sawo_DecrRefCount(interp, cf->raiseArgsObjPtr);
    if (cf->raiseBodyObjPtr)
        sawo_DecrRefCount(interp, cf->raiseBodyObjPtr);
    sawo_DecrRefCount(interp, cf->nsObj);
    if (action == SAWO_FCF_FULL || cf->vars.size != SAWO_HT_INITIAL_SIZE)
        sawo_FreeHashTable(&cf->vars);
    else {
        int i;
        sawo_HashEntry **table = cf->vars.table, *he;

        for (i = 0; i < SAWO_HT_INITIAL_SIZE; i++) {
            he = table[i];
            while (he != NULL) {
                sawo_HashEntry *nextEntry = he->next;
                sawo_Var *varPtr = sawo_GetHashEntryVal(he);

                sawo_DecrRefCount(interp, varPtr->objPtr);
                sawo_Free(sawo_GetHashEntryKey(he));
                sawo_Free(varPtr);
                sawo_Free(he);
                table[i] = NULL;
                he = nextEntry;
            }
        }
        cf->vars.used = 0;
    }
    cf->next = interp->freeFramesList;
    interp->freeFramesList = cf;
}


#ifdef SAWO_REFERENCES

static void sawoReferencesHTValDestructor(void *interp, void *val)
{
    sawo_Reference *refPtr = (void *)val;

    sawo_DecrRefCount(interp, refPtr->objPtr);
    if (refPtr->finalizerCmdNamePtr != NULL) {
        sawo_DecrRefCount(interp, refPtr->finalizerCmdNamePtr);
    }
    sawo_Free(val);
}

static unsigned int sawoReferencesHTHashFunction(const void *key)
{
    
    const unsigned long *widePtr = key;
    unsigned int intValue = (unsigned int)*widePtr;

    return sawo_IntHashFunction(intValue);
}

static void *sawoReferencesHTKeyDup(void *privdata, const void *key)
{
    void *copy = sawo_Alloc(sizeof(unsigned long));

    SAWO_NOTUSED(privdata);

    memcpy(copy, key, sizeof(unsigned long));
    return copy;
}

static int sawoReferencesHTKeyCompare(void *privdata, const void *key1, const void *key2)
{
    SAWO_NOTUSED(privdata);

    return memcmp(key1, key2, sizeof(unsigned long)) == 0;
}

static void sawoReferencesHTKeyDestructor(void *privdata, void *key)
{
    SAWO_NOTUSED(privdata);

    sawo_Free(key);
}

static const sawo_HashTableType sawoReferencesHashTableType = {
    sawoReferencesHTHashFunction,        
    sawoReferencesHTKeyDup,      
    NULL,                       
    sawoReferencesHTKeyCompare,  
    sawoReferencesHTKeyDestructor,       
    sawoReferencesHTValDestructor        
};



#define SAWO_REFERENCE_SPACE (35+SAWO_REFERENCE_TAGLEN)

static int sawoFormatReference(char *buf, sawo_Reference *refPtr, unsigned long id)
{
    const char *fmt = "<reference.<%s>.%020lu>";

    sprintf(buf, fmt, refPtr->tag, id);
    return SAWO_REFERENCE_SPACE;
}

static void UpdateStringOfReference(struct sawo_Obj *objPtr);

static const sawo_ObjType referenceObjType = {
    "reference",
    NULL,
    NULL,
    UpdateStringOfReference,
    SAWO_TYPE_REFERENCES,
};

static void UpdateStringOfReference(struct sawo_Obj *objPtr)
{
    char buf[SAWO_REFERENCE_SPACE + 1];

    sawoFormatReference(buf, objPtr->internalRep.refValue.refPtr, objPtr->internalRep.refValue.id);
    sawoSetStringBytes(objPtr, buf);
}

static int isrefchar(int c)
{
    return (c == '_' || isalnum(c));
}

static int SetReferenceFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    unsigned long value;
    int i, len;
    const char *str, *start, *end;
    char refId[21];
    sawo_Reference *refPtr;
    sawo_HashEntry *he;
    char *endptr;

    
    str = sawo_GetString(objPtr, &len);
    
    if (len < SAWO_REFERENCE_SPACE)
        goto badformat;
    
    start = str;
    end = str + len - 1;
    while (*start == ' ')
        start++;
    while (*end == ' ' && end > start)
        end--;
    if (end - start + 1 != SAWO_REFERENCE_SPACE)
        goto badformat;
    
    if (memcmp(start, "<reference.<", 12) != 0)
        goto badformat;
    if (start[12 + SAWO_REFERENCE_TAGLEN] != '>' || end[0] != '>')
        goto badformat;
    
    for (i = 0; i < SAWO_REFERENCE_TAGLEN; i++) {
        if (!isrefchar(start[12 + i]))
            goto badformat;
    }
    
    memcpy(refId, start + 14 + SAWO_REFERENCE_TAGLEN, 20);
    refId[20] = '\0';
    
    value = strtoul(refId, &endptr, 10);
    if (sawoCheckConversion(refId, endptr) != SAWO_OK)
        goto badformat;
    
    he = sawo_FindHashEntry(&interp->references, &value);
    if (he == NULL) {
        sawo_SetResultFormatted(interp, "invalid reference id \"%#s\"", objPtr);
        return SAWO_ERR;
    }
    refPtr = sawo_GetHashEntryVal(he);
    
    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &referenceObjType;
    objPtr->internalRep.refValue.id = value;
    objPtr->internalRep.refValue.refPtr = refPtr;
    return SAWO_OK;

  badformat:
    sawo_SetResultFormatted(interp, "expected reference but got \"%#s\"", objPtr);
    return SAWO_ERR;
}

sawo_Obj *sawo_NewReference(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *tagPtr, sawo_Obj *cmdNamePtr)
{
    struct sawo_Reference *refPtr;
    unsigned long id;
    sawo_Obj *refObjPtr;
    const char *tag;
    int tagLen, i;

    
    sawo_CollectIfNeeded(interp);

    refPtr = sawo_Alloc(sizeof(*refPtr));
    refPtr->objPtr = objPtr;
    sawo_IncrRefCount(objPtr);
    refPtr->finalizerCmdNamePtr = cmdNamePtr;
    if (cmdNamePtr)
        sawo_IncrRefCount(cmdNamePtr);
    id = interp->referenceNextId++;
    sawo_AddHashEntry(&interp->references, &id, refPtr);
    refObjPtr = sawo_NewObj(interp);
    refObjPtr->typePtr = &referenceObjType;
    refObjPtr->bytes = NULL;
    refObjPtr->internalRep.refValue.id = id;
    refObjPtr->internalRep.refValue.refPtr = refPtr;
    interp->referenceNextId++;
    tag = sawo_GetString(tagPtr, &tagLen);
    if (tagLen > SAWO_REFERENCE_TAGLEN)
        tagLen = SAWO_REFERENCE_TAGLEN;
    for (i = 0; i < SAWO_REFERENCE_TAGLEN; i++) {
        if (i < tagLen && isrefchar(tag[i]))
            refPtr->tag[i] = tag[i];
        else
            refPtr->tag[i] = '_';
    }
    refPtr->tag[SAWO_REFERENCE_TAGLEN] = '\0';
    return refObjPtr;
}

sawo_Reference *sawo_GetReference(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (objPtr->typePtr != &referenceObjType && SetReferenceFromAny(interp, objPtr) == SAWO_ERR)
        return NULL;
    return objPtr->internalRep.refValue.refPtr;
}

int sawo_SetFinalizer(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *cmdNamePtr)
{
    sawo_Reference *refPtr;

    if ((refPtr = sawo_GetReference(interp, objPtr)) == NULL)
        return SAWO_ERR;
    sawo_IncrRefCount(cmdNamePtr);
    if (refPtr->finalizerCmdNamePtr)
        sawo_DecrRefCount(interp, refPtr->finalizerCmdNamePtr);
    refPtr->finalizerCmdNamePtr = cmdNamePtr;
    return SAWO_OK;
}

int sawo_GetFinalizer(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj **cmdNamePtrPtr)
{
    sawo_Reference *refPtr;

    if ((refPtr = sawo_GetReference(interp, objPtr)) == NULL)
        return SAWO_ERR;
    *cmdNamePtrPtr = refPtr->finalizerCmdNamePtr;
    return SAWO_OK;
}



static const sawo_HashTableType sawoRefMarkHashTableType = {
    sawoReferencesHTHashFunction,        
    sawoReferencesHTKeyDup,      
    NULL,                       
    sawoReferencesHTKeyCompare,  
    sawoReferencesHTKeyDestructor,       
    NULL                        
};


int sawo_Collect(sawo_Interp *interp)
{
    int collected = 0;
    return collected;
}

#define SAWO_COLLECT_ID_PERIOD 5000
#define SAWO_COLLECT_TIME_PERIOD 300

void sawo_CollectIfNeeded(sawo_Interp *interp)
{
    unsigned long elapsedId;
    int elapsedTime;

    elapsedId = interp->referenceNextId - interp->lastCollectId;
    elapsedTime = time(NULL) - interp->lastCollectTime;


    if (elapsedId > SAWO_COLLECT_ID_PERIOD || elapsedTime > SAWO_COLLECT_TIME_PERIOD) {
        sawo_Collect(interp);
    }
}
#endif

int sawo_IsBigEndian(void)
{
    union {
        unsigned short s;
        unsigned char c[2];
    } uval = {0x0102};

    return uval.c[0] == 1;
}


sawo_Interp *sawo_CreateInterp(void)
{
    sawo_Interp *i = sawo_Alloc(sizeof(*i));

    memset(i, 0, sizeof(*i));

    i->maxCallFrameDepth = SAWO_MAX_CALLFRAME_DEPTH;
    i->maxEvalDepth = SAWO_MAX_EVAL_DEPTH;
    i->lastCollectTime = time(NULL);

    sawo_InitHashTable(&i->commands, &sawoCommandsHashTableType, i);
#ifdef SAWO_REFERENCES
    sawo_InitHashTable(&i->references, &sawoReferencesHashTableType, i);
#endif
    sawo_InitHashTable(&i->assocData, &sawoAssocDataHashTableType, i);
    sawo_InitHashTable(&i->packages, &sawoPackageHashTableType, NULL);
    i->emptyObj = sawo_NewEmptyStringObj(i);
    i->trueObj = sawo_NewIntObj(i, 1);
    i->falseObj = sawo_NewIntObj(i, 0);
    i->framePtr = i->topFramePtr = sawoCreateCallFrame(i, NULL, i->emptyObj);
    i->errorFileNameObj = i->emptyObj;
    i->result = i->emptyObj;
    i->stackTrace = sawo_NewListObj(i, NULL, 0);
    i->unknown = sawo_NewStringObj(i, "unknown", -1);
    i->errorRaise = i->emptyObj;
    i->currentScriptObj = sawo_NewEmptyStringObj(i);
    i->nullScriptObj = sawo_NewEmptyStringObj(i);
    sawo_IncrRefCount(i->emptyObj);
    sawo_IncrRefCount(i->errorFileNameObj);
    sawo_IncrRefCount(i->result);
    sawo_IncrRefCount(i->stackTrace);
    sawo_IncrRefCount(i->unknown);
    sawo_IncrRefCount(i->currentScriptObj);
    sawo_IncrRefCount(i->nullScriptObj);
    sawo_IncrRefCount(i->errorRaise);
    sawo_IncrRefCount(i->trueObj);
    sawo_IncrRefCount(i->falseObj);

    
    sawo_SetVariableStrWithStr(i, SAWO_LIBPATH, HYANG_LIBRARY);
    sawo_SetVariableStrWithStr(i, SAWO_INTERACTIVE, "0");

    sawo_SetVariableStrWithStr(i, "hyang_platform(engine)", "sawo");
    sawo_SetVariableStrWithStr(i, "hyang_platform(os)", HYANG_PLATFORM_OS);
    sawo_SetVariableStrWithStr(i, "hyang_platform(platform)", HYANG_PLATFORM_PLATFORM);
    sawo_SetVariableStrWithStr(i, "hyang_platform(pathSeparator)", HYANG_PLATFORM_PATH_SEPARATOR);
    sawo_SetVariableStrWithStr(i, "hyang_platform(byteOrder)", sawo_IsBigEndian() ? "bigEndian" : "littleEndian");
    sawo_SetVariableStrWithStr(i, "hyang_platform(threaded)", "0");
    sawo_SetVariableStr(i, "hyang_platform(pointerSize)", sawo_NewIntObj(i, sizeof(void *)));
    sawo_SetVariableStr(i, "hyang_platform(wordSize)", sawo_NewIntObj(i, sizeof(sawo_wide)));

    return i;
}

void sawo_FreeInterp(sawo_Interp *i)
{
    sawo_CallFrame *cf, *cfx;

    sawo_Obj *objPtr, *nextObjPtr;

    
    for (cf = i->framePtr; cf; cf = cfx) {
        cfx = cf->parent;
        sawoFreeCallFrame(i, cf, SAWO_FCF_FULL);
    }

    sawo_DecrRefCount(i, i->emptyObj);
    sawo_DecrRefCount(i, i->trueObj);
    sawo_DecrRefCount(i, i->falseObj);
    sawo_DecrRefCount(i, i->result);
    sawo_DecrRefCount(i, i->stackTrace);
    sawo_DecrRefCount(i, i->errorRaise);
    sawo_DecrRefCount(i, i->unknown);
    sawo_DecrRefCount(i, i->errorFileNameObj);
    sawo_DecrRefCount(i, i->currentScriptObj);
    sawo_DecrRefCount(i, i->nullScriptObj);
    sawo_FreeHashTable(&i->commands);
#ifdef SAWO_REFERENCES
    sawo_FreeHashTable(&i->references);
#endif
    sawo_FreeHashTable(&i->packages);
    sawo_Free(i->prngState);
    sawo_FreeHashTable(&i->assocData);

#ifdef SAWO_MAINTAINER
    if (i->liveList != NULL) {
        objPtr = i->liveList;

        printf("\n-------------------------------------\n");
        printf("Objects still in the free list:\n");
        while (objPtr) {
            const char *type = objPtr->typePtr ? objPtr->typePtr->name : "string";

            if (objPtr->bytes && strlen(objPtr->bytes) > 20) {
                printf("%p (%d) %-10s: '%.20s...'\n",
                    (void *)objPtr, objPtr->refCount, type, objPtr->bytes);
            }
            else {
                printf("%p (%d) %-10s: '%s'\n",
                    (void *)objPtr, objPtr->refCount, type, objPtr->bytes ? objPtr->bytes : "(null)");
            }
            if (objPtr->typePtr == &sourceObjType) {
                printf("FILE %s LINE %d\n",
                    sawo_String(objPtr->internalRep.sourceValue.fileNameObj),
                    objPtr->internalRep.sourceValue.lineNumber);
            }
            objPtr = objPtr->nextObjPtr;
        }
        printf("-------------------------------------\n\n");
        sawoPanic((1, "Live list non empty freeing the interpreter! Leak?"));
    }
#endif

    
    objPtr = i->freeList;
    while (objPtr) {
        nextObjPtr = objPtr->nextObjPtr;
        sawo_Free(objPtr);
        objPtr = nextObjPtr;
    }

    
    for (cf = i->freeFramesList; cf; cf = cfx) {
        cfx = cf->next;
        if (cf->vars.table)
            sawo_FreeHashTable(&cf->vars);
        sawo_Free(cf);
    }

    
    sawo_Free(i);
}

sawo_CallFrame *sawo_GetCallFrameByLevel(sawo_Interp *interp, sawo_Obj *levelObjPtr)
{
    long level;
    const char *str;
    sawo_CallFrame *framePtr;

    if (levelObjPtr) {
        str = sawo_String(levelObjPtr);
        if (str[0] == '#') {
            char *endptr;

            level = sawo_strtol(str + 1, &endptr);
            if (str[1] == '\0' || endptr[0] != '\0') {
                level = -1;
            }
        }
        else {
            if (sawo_GetLong(interp, levelObjPtr, &level) != SAWO_OK || level < 0) {
                level = -1;
            }
            else {
                
                level = interp->framePtr->level - level;
            }
        }
    }
    else {
        str = "1";              
        level = interp->framePtr->level - 1;
    }

    if (level == 0) {
        return interp->topFramePtr;
    }
    if (level > 0) {
        
        for (framePtr = interp->framePtr; framePtr; framePtr = framePtr->parent) {
            if (framePtr->level == level) {
                return framePtr;
            }
        }
    }

    sawo_SetResultFormatted(interp, "bad level \"%s\"", str);
    return NULL;
}

static sawo_CallFrame *sawoGetCallFrameByInteger(sawo_Interp *interp, sawo_Obj *levelObjPtr)
{
    long level;
    sawo_CallFrame *framePtr;

    if (sawo_GetLong(interp, levelObjPtr, &level) == SAWO_OK) {
        if (level <= 0) {
            
            level = interp->framePtr->level + level;
        }

        if (level == 0) {
            return interp->topFramePtr;
        }

        
        for (framePtr = interp->framePtr; framePtr; framePtr = framePtr->parent) {
            if (framePtr->level == level) {
                return framePtr;
            }
        }
    }

    sawo_SetResultFormatted(interp, "bad level \"%#s\"", levelObjPtr);
    return NULL;
}

static void sawoResetStackTrace(sawo_Interp *interp)
{
    sawo_DecrRefCount(interp, interp->stackTrace);
    interp->stackTrace = sawo_NewListObj(interp, NULL, 0);
    sawo_IncrRefCount(interp->stackTrace);
}

static void sawoSetStackTrace(sawo_Interp *interp, sawo_Obj *stackTraceObj)
{
    int len;

    
    sawo_IncrRefCount(stackTraceObj);
    sawo_DecrRefCount(interp, interp->stackTrace);
    interp->stackTrace = stackTraceObj;
    interp->errorFlag = 1;

    len = sawo_ListLength(interp, interp->stackTrace);
    if (len >= 3) {
        if (sawo_Length(sawo_ListGetIndex(interp, interp->stackTrace, len - 2)) == 0) {
            interp->addStackTrace = 1;
        }
    }
}

static void sawoAppendStackTrace(sawo_Interp *interp, const char *raisename,
    sawo_Obj *fileNameObj, int linenr)
{
    if (strcmp(raisename, "unknown") == 0) {
        raisename = "";
    }
    if (!*raisename && !sawo_Length(fileNameObj)) {
        
        return;
    }

    if (sawo_IsShared(interp->stackTrace)) {
        sawo_DecrRefCount(interp, interp->stackTrace);
        interp->stackTrace = sawo_DuplicateObj(interp, interp->stackTrace);
        sawo_IncrRefCount(interp->stackTrace);
    }

    
    if (!*raisename && sawo_Length(fileNameObj)) {
        
        int len = sawo_ListLength(interp, interp->stackTrace);

        if (len >= 3) {
            sawo_Obj *objPtr = sawo_ListGetIndex(interp, interp->stackTrace, len - 3);
            if (sawo_Length(objPtr)) {
                
                objPtr = sawo_ListGetIndex(interp, interp->stackTrace, len - 2);
                if (sawo_Length(objPtr) == 0) {
                    
                    ListSetIndex(interp, interp->stackTrace, len - 2, fileNameObj, 0);
                    ListSetIndex(interp, interp->stackTrace, len - 1, sawo_NewIntObj(interp, linenr), 0);
                    return;
                }
            }
        }
    }

    sawo_ListAppendElement(interp, interp->stackTrace, sawo_NewStringObj(interp, raisename, -1));
    sawo_ListAppendElement(interp, interp->stackTrace, fileNameObj);
    sawo_ListAppendElement(interp, interp->stackTrace, sawo_NewIntObj(interp, linenr));
}

int sawo_SetAssocData(sawo_Interp *interp, const char *key, sawo_InterpDeleteRaise * delRaise,
    void *data)
{
    AssocDataValue *assocEntryPtr = (AssocDataValue *) sawo_Alloc(sizeof(AssocDataValue));

    assocEntryPtr->delRaise = delRaise;
    assocEntryPtr->data = data;
    return sawo_AddHashEntry(&interp->assocData, key, assocEntryPtr);
}

void *sawo_GetAssocData(sawo_Interp *interp, const char *key)
{
    sawo_HashEntry *entryPtr = sawo_FindHashEntry(&interp->assocData, key);

    if (entryPtr != NULL) {
        AssocDataValue *assocEntryPtr = sawo_GetHashEntryVal(entryPtr);
        return assocEntryPtr->data;
    }
    return NULL;
}

int sawo_DeleteAssocData(sawo_Interp *interp, const char *key)
{
    return sawo_DeleteHashEntry(&interp->assocData, key);
}

int sawo_GetExitCode(sawo_Interp *interp)
{
    return interp->exitCode;
}

static void UpdateStringOfInt(struct sawo_Obj *objPtr);
static int SetIntFromAny(sawo_Interp *interp, sawo_Obj *objPtr, int flags);

static const sawo_ObjType intObjType = {
    "int",
    NULL,
    NULL,
    UpdateStringOfInt,
    SAWO_TYPE_NONE,
};

static const sawo_ObjType coercedDoubleObjType = {
    "coerced-double",
    NULL,
    NULL,
    UpdateStringOfInt,
    SAWO_TYPE_NONE,
};


static void UpdateStringOfInt(struct sawo_Obj *objPtr)
{
    char buf[SAWO_INTEGER_SPACE + 1];
    sawo_wide wideValue = sawoWideValue(objPtr);
    int pos = 0;

    if (wideValue == 0) {
        buf[pos++] = '0';
    }
    else {
        char tmp[SAWO_INTEGER_SPACE];
        int num = 0;
        int i;

        if (wideValue < 0) {
            buf[pos++] = '-';
            i = wideValue % 10;
            tmp[num++] = (i > 0) ? (10 - i) : -i;
            wideValue /= -10;
        }

        while (wideValue) {
            tmp[num++] = wideValue % 10;
            wideValue /= 10;
        }

        for (i = 0; i < num; i++) {
            buf[pos++] = '0' + tmp[num - i - 1];
        }
    }
    buf[pos] = 0;

    sawoSetStringBytes(objPtr, buf);
}

static int SetIntFromAny(sawo_Interp *interp, sawo_Obj *objPtr, int flags)
{
    sawo_wide wideValue;
    const char *str;

    if (objPtr->typePtr == &coercedDoubleObjType) {
        
        objPtr->typePtr = &intObjType;
        return SAWO_OK;
    }

    
    str = sawo_String(objPtr);
    
    if (sawo_StringToWide(str, &wideValue, 0) != SAWO_OK) {
        if (flags & SAWO_ERRMSG) {
            sawo_SetResultFormatted(interp, "expected integer but got \"%#s\"", objPtr);
        }
        return SAWO_ERR;
    }
    if ((wideValue == SAWO_WIDE_MIN || wideValue == SAWO_WIDE_MAX) && errno == ERANGE) {
        sawo_SetResultString(interp, "Integer value too big to be represented", -1);
        return SAWO_ERR;
    }
    
    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &intObjType;
    objPtr->internalRep.wideValue = wideValue;
    return SAWO_OK;
}

#ifdef SAWO_OPTIMIZATION
static int sawoIsWide(sawo_Obj *objPtr)
{
    return objPtr->typePtr == &intObjType;
}
#endif

int sawo_GetWide(sawo_Interp *interp, sawo_Obj *objPtr, sawo_wide * widePtr)
{
    if (objPtr->typePtr != &intObjType && SetIntFromAny(interp, objPtr, SAWO_ERRMSG) == SAWO_ERR)
        return SAWO_ERR;
    *widePtr = sawoWideValue(objPtr);
    return SAWO_OK;
}


static int sawoGetWideNoErr(sawo_Interp *interp, sawo_Obj *objPtr, sawo_wide * widePtr)
{
    if (objPtr->typePtr != &intObjType && SetIntFromAny(interp, objPtr, SAWO_NONE) == SAWO_ERR)
        return SAWO_ERR;
    *widePtr = sawoWideValue(objPtr);
    return SAWO_OK;
}

int sawo_GetLong(sawo_Interp *interp, sawo_Obj *objPtr, long *longPtr)
{
    sawo_wide wideValue;
    int retval;

    retval = sawo_GetWide(interp, objPtr, &wideValue);
    if (retval == SAWO_OK) {
        *longPtr = (long)wideValue;
        return SAWO_OK;
    }
    return SAWO_ERR;
}

sawo_Obj *sawo_NewIntObj(sawo_Interp *interp, sawo_wide wideValue)
{
    sawo_Obj *objPtr;

    objPtr = sawo_NewObj(interp);
    objPtr->typePtr = &intObjType;
    objPtr->bytes = NULL;
    objPtr->internalRep.wideValue = wideValue;
    return objPtr;
}

#define SAWO_DOUBLE_SPACE 30

static void UpdateStringOfDouble(struct sawo_Obj *objPtr);
static int SetDoubleFromAny(sawo_Interp *interp, sawo_Obj *objPtr);

static const sawo_ObjType doubleObjType = {
    "double",
    NULL,
    NULL,
    UpdateStringOfDouble,
    SAWO_TYPE_NONE,
};

#ifndef HAVE_ISNAN
#undef isnan
#define isnan(X) ((X) != (X))
#endif
#ifndef HAVE_ISINF
#undef isinf
#define isinf(X) (1.0 / (X) == 0.0)
#endif

static void UpdateStringOfDouble(struct sawo_Obj *objPtr)
{
    double value = objPtr->internalRep.doubleValue;

    if (isnan(value)) {
        sawoSetStringBytes(objPtr, "NaN");
        return;
    }
    if (isinf(value)) {
        if (value < 0) {
            sawoSetStringBytes(objPtr, "-Inf");
        }
        else {
            sawoSetStringBytes(objPtr, "Inf");
        }
        return;
    }
    {
        char buf[SAWO_DOUBLE_SPACE + 1];
        int i;
        int len = sprintf(buf, "%.12g", value);

        
        for (i = 0; i < len; i++) {
            if (buf[i] == '.' || buf[i] == 'e') {
#if defined(SAWO_SPRINTF_DOUBLE_NEEDS_FIX)
                char *e = strchr(buf, 'e');
                if (e && (e[1] == '-' || e[1] == '+') && e[2] == '0') {
                    
                    e += 2;
                    memmove(e, e + 1, len - (e - buf));
                }
#endif
                break;
            }
        }
        if (buf[i] == '\0') {
            buf[i++] = '.';
            buf[i++] = '0';
            buf[i] = '\0';
        }
        sawoSetStringBytes(objPtr, buf);
    }
}

static int SetDoubleFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    double doubleValue;
    sawo_wide wideValue;
    const char *str;

    str = sawo_String(objPtr);

#ifdef HAVE_LONG_LONG
    
#define MIN_INT_IN_DOUBLE -(1LL << 53)
#define MAX_INT_IN_DOUBLE -(MIN_INT_IN_DOUBLE + 1)

    if (objPtr->typePtr == &intObjType
        && sawoWideValue(objPtr) >= MIN_INT_IN_DOUBLE
        && sawoWideValue(objPtr) <= MAX_INT_IN_DOUBLE) {

        
        objPtr->typePtr = &coercedDoubleObjType;
        return SAWO_OK;
    }
    else
#endif
    if (sawo_StringToWide(str, &wideValue, 10) == SAWO_OK) {
        
        sawo_FreeIntRep(interp, objPtr);
        objPtr->typePtr = &coercedDoubleObjType;
        objPtr->internalRep.wideValue = wideValue;
        return SAWO_OK;
    }
    else {
        
        if (sawo_StringToDouble(str, &doubleValue) != SAWO_OK) {
            sawo_SetResultFormatted(interp, "expected floating-point number but got \"%#s\"", objPtr);
            return SAWO_ERR;
        }
        
        sawo_FreeIntRep(interp, objPtr);
    }
    objPtr->typePtr = &doubleObjType;
    objPtr->internalRep.doubleValue = doubleValue;
    return SAWO_OK;
}

int sawo_GetDouble(sawo_Interp *interp, sawo_Obj *objPtr, double *doublePtr)
{
    if (objPtr->typePtr == &coercedDoubleObjType) {
        *doublePtr = sawoWideValue(objPtr);
        return SAWO_OK;
    }
    if (objPtr->typePtr != &doubleObjType && SetDoubleFromAny(interp, objPtr) == SAWO_ERR)
        return SAWO_ERR;

    if (objPtr->typePtr == &coercedDoubleObjType) {
        *doublePtr = sawoWideValue(objPtr);
    }
    else {
        *doublePtr = objPtr->internalRep.doubleValue;
    }
    return SAWO_OK;
}

sawo_Obj *sawo_NewDoubleObj(sawo_Interp *interp, double doubleValue)
{
    sawo_Obj *objPtr;

    objPtr = sawo_NewObj(interp);
    objPtr->typePtr = &doubleObjType;
    objPtr->bytes = NULL;
    objPtr->internalRep.doubleValue = doubleValue;
    return objPtr;
}

static void ListInsertElements(sawo_Obj *listPtr, int idx, int elemc, sawo_Obj *const *elemVec);
static void ListAppendElement(sawo_Obj *listPtr, sawo_Obj *objPtr);
static void FreeListInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupListInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);
static void UpdateStringOfList(struct sawo_Obj *objPtr);
static int SetListFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);

static const sawo_ObjType listObjType = {
    "list",
    FreeListInternalRep,
    DupListInternalRep,
    UpdateStringOfList,
    SAWO_TYPE_NONE,
};

void FreeListInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    int i;

    for (i = 0; i < objPtr->internalRep.listValue.len; i++) {
        sawo_DecrRefCount(interp, objPtr->internalRep.listValue.ele[i]);
    }
    sawo_Free(objPtr->internalRep.listValue.ele);
}

void DupListInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    int i;

    SAWO_NOTUSED(interp);

    dupPtr->internalRep.listValue.len = srcPtr->internalRep.listValue.len;
    dupPtr->internalRep.listValue.maxLen = srcPtr->internalRep.listValue.maxLen;
    dupPtr->internalRep.listValue.ele =
        sawo_Alloc(sizeof(sawo_Obj *) * srcPtr->internalRep.listValue.maxLen);
    memcpy(dupPtr->internalRep.listValue.ele, srcPtr->internalRep.listValue.ele,
        sizeof(sawo_Obj *) * srcPtr->internalRep.listValue.len);
    for (i = 0; i < dupPtr->internalRep.listValue.len; i++) {
        sawo_IncrRefCount(dupPtr->internalRep.listValue.ele[i]);
    }
    dupPtr->typePtr = &listObjType;
}

#define SAWO_ELESTR_SIMPLE 0
#define SAWO_ELESTR_BRACE 1
#define SAWO_ELESTR_QUOTE 2
static unsigned char ListElementQuotingType(const char *s, int len)
{
    int i, level, blevel, trySimple = 1;

    
    if (len == 0)
        return SAWO_ELESTR_BRACE;
    if (s[0] == '"' || s[0] == '{') {
        trySimple = 0;
        goto testbrace;
    }
    for (i = 0; i < len; i++) {
        switch (s[i]) {
            case ' ':
            case '$':
            case '"':
            case '[':
            case ']':
            case ';':
            case '\\':
            case '\r':
            case '\n':
            case '\t':
            case '\f':
            case '\v':
                trySimple = 0;
                
            case '{':
            case '}':
                goto testbrace;
        }
    }
    return SAWO_ELESTR_SIMPLE;

  testbrace:
    
    if (s[len - 1] == '\\')
        return SAWO_ELESTR_QUOTE;
    level = 0;
    blevel = 0;
    for (i = 0; i < len; i++) {
        switch (s[i]) {
            case '{':
                level++;
                break;
            case '}':
                level--;
                if (level < 0)
                    return SAWO_ELESTR_QUOTE;
                break;
            case '[':
                blevel++;
                break;
            case ']':
                blevel--;
                break;
            case '\\':
                if (s[i + 1] == '\n')
                    return SAWO_ELESTR_QUOTE;
                else if (s[i + 1] != '\0')
                    i++;
                break;
        }
    }
    if (blevel < 0) {
        return SAWO_ELESTR_QUOTE;
    }

    if (level == 0) {
        if (!trySimple)
            return SAWO_ELESTR_BRACE;
        for (i = 0; i < len; i++) {
            switch (s[i]) {
                case ' ':
                case '$':
                case '"':
                case '[':
                case ']':
                case ';':
                case '\\':
                case '\r':
                case '\n':
                case '\t':
                case '\f':
                case '\v':
                    return SAWO_ELESTR_BRACE;
                    break;
            }
        }
        return SAWO_ELESTR_SIMPLE;
    }
    return SAWO_ELESTR_QUOTE;
}

static int BackslashQuoteString(const char *s, int len, char *q)
{
    char *p = q;

    while (len--) {
        switch (*s) {
            case ' ':
            case '$':
            case '"':
            case '[':
            case ']':
            case '{':
            case '}':
            case ';':
            case '\\':
                *p++ = '\\';
                *p++ = *s++;
                break;
            case '\n':
                *p++ = '\\';
                *p++ = 'n';
                s++;
                break;
            case '\r':
                *p++ = '\\';
                *p++ = 'r';
                s++;
                break;
            case '\t':
                *p++ = '\\';
                *p++ = 't';
                s++;
                break;
            case '\f':
                *p++ = '\\';
                *p++ = 'f';
                s++;
                break;
            case '\v':
                *p++ = '\\';
                *p++ = 'v';
                s++;
                break;
            default:
                *p++ = *s++;
                break;
        }
    }
    *p = '\0';

    return p - q;
}

static void sawoMakeListStringRep(sawo_Obj *objPtr, sawo_Obj **objv, int objc)
{
    #define STATIC_QUOTING_LEN 32
    int i, bufLen, realLength;
    const char *strRep;
    char *p;
    unsigned char *quotingType, staticQuoting[STATIC_QUOTING_LEN];

    
    if (objc > STATIC_QUOTING_LEN) {
        quotingType = sawo_Alloc(objc);
    }
    else {
        quotingType = staticQuoting;
    }
    bufLen = 0;
    for (i = 0; i < objc; i++) {
        int len;

        strRep = sawo_GetString(objv[i], &len);
        quotingType[i] = ListElementQuotingType(strRep, len);
        switch (quotingType[i]) {
            case SAWO_ELESTR_SIMPLE:
                if (i != 0 || strRep[0] != '#') {
                    bufLen += len;
                    break;
                }
                
                quotingType[i] = SAWO_ELESTR_BRACE;
                
            case SAWO_ELESTR_BRACE:
                bufLen += len + 2;
                break;
            case SAWO_ELESTR_QUOTE:
                bufLen += len * 2;
                break;
        }
        bufLen++;               
    }
    bufLen++;

    
    p = objPtr->bytes = sawo_Alloc(bufLen + 1);
    realLength = 0;
    for (i = 0; i < objc; i++) {
        int len, qlen;

        strRep = sawo_GetString(objv[i], &len);

        switch (quotingType[i]) {
            case SAWO_ELESTR_SIMPLE:
                memcpy(p, strRep, len);
                p += len;
                realLength += len;
                break;
            case SAWO_ELESTR_BRACE:
                *p++ = '{';
                memcpy(p, strRep, len);
                p += len;
                *p++ = '}';
                realLength += len + 2;
                break;
            case SAWO_ELESTR_QUOTE:
                if (i == 0 && strRep[0] == '#') {
                    *p++ = '\\';
                    realLength++;
                }
                qlen = BackslashQuoteString(strRep, len, p);
                p += qlen;
                realLength += qlen;
                break;
        }
        
        if (i + 1 != objc) {
            *p++ = ' ';
            realLength++;
        }
    }
    *p = '\0';                  
    objPtr->length = realLength;

    if (quotingType != staticQuoting) {
        sawo_Free(quotingType);
    }
}

static void UpdateStringOfList(struct sawo_Obj *objPtr)
{
    sawoMakeListStringRep(objPtr, objPtr->internalRep.listValue.ele, objPtr->internalRep.listValue.len);
}

static int SetListFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr)
{
    struct sawoParserCtx parser;
    const char *str;
    int strLen;
    sawo_Obj *fileNameObj;
    int linenr;

    if (objPtr->typePtr == &listObjType) {
        return SAWO_OK;
    }

    if (sawo_IsDict(objPtr) && objPtr->bytes == NULL) {
        sawo_Obj **listObjPtrPtr;
        int len;
        int i;

        listObjPtrPtr = sawoDictPairs(objPtr, &len);
        for (i = 0; i < len; i++) {
            sawo_IncrRefCount(listObjPtrPtr[i]);
        }

        
        sawo_FreeIntRep(interp, objPtr);
        objPtr->typePtr = &listObjType;
        objPtr->internalRep.listValue.len = len;
        objPtr->internalRep.listValue.maxLen = len;
        objPtr->internalRep.listValue.ele = listObjPtrPtr;

        return SAWO_OK;
    }

    
    if (objPtr->typePtr == &sourceObjType) {
        fileNameObj = objPtr->internalRep.sourceValue.fileNameObj;
        linenr = objPtr->internalRep.sourceValue.lineNumber;
    }
    else {
        fileNameObj = interp->emptyObj;
        linenr = 1;
    }
    sawo_IncrRefCount(fileNameObj);

    
    str = sawo_GetString(objPtr, &strLen);

    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &listObjType;
    objPtr->internalRep.listValue.len = 0;
    objPtr->internalRep.listValue.maxLen = 0;
    objPtr->internalRep.listValue.ele = NULL;

    
    if (strLen) {
        sawoParserInit(&parser, str, strLen, linenr);
        while (!parser.eof) {
            sawo_Obj *elementPtr;

            sawoParseList(&parser);
            if (parser.tt != SAWO_TT_STR && parser.tt != SAWO_TT_ESC)
                continue;
            elementPtr = sawoParserGetTokenObj(interp, &parser);
            sawoSetSourceInfo(interp, elementPtr, fileNameObj, parser.tline);
            ListAppendElement(objPtr, elementPtr);
        }
    }
    sawo_DecrRefCount(interp, fileNameObj);
    return SAWO_OK;
}

sawo_Obj *sawo_NewListObj(sawo_Interp *interp, sawo_Obj *const *elements, int len)
{
    sawo_Obj *objPtr;

    objPtr = sawo_NewObj(interp);
    objPtr->typePtr = &listObjType;
    objPtr->bytes = NULL;
    objPtr->internalRep.listValue.ele = NULL;
    objPtr->internalRep.listValue.len = 0;
    objPtr->internalRep.listValue.maxLen = 0;

    if (len) {
        ListInsertElements(objPtr, 0, len, elements);
    }

    return objPtr;
}

static void sawoListGetElements(sawo_Interp *interp, sawo_Obj *listObj, int *listLen,
    sawo_Obj ***listVec)
{
    *listLen = sawo_ListLength(interp, listObj);
    *listVec = listObj->internalRep.listValue.ele;
}


static int sawoSign(sawo_wide w)
{
    if (w == 0) {
        return 0;
    }
    else if (w < 0) {
        return -1;
    }
    return 1;
}


struct lsort_info {
    jmp_buf jmpbuf;
    sawo_Obj *command;
    sawo_Interp *interp;
    enum {
        SAWO_LSORT_ASCII,
        SAWO_LSORT_NOCASE,
        SAWO_LSORT_INTEGER,
        SAWO_LSORT_REAL,
        SAWO_LSORT_COMMAND
    } type;
    int order;
    int index;
    int indexed;
    int unique;
    int (*subfn)(sawo_Obj **, sawo_Obj **);
};

static struct lsort_info *sort_info;

static int ListSortIndexHelper(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    sawo_Obj *lObj, *rObj;

    if (sawo_ListIndex(sort_info->interp, *lhsObj, sort_info->index, &lObj, SAWO_ERRMSG) != SAWO_OK ||
        sawo_ListIndex(sort_info->interp, *rhsObj, sort_info->index, &rObj, SAWO_ERRMSG) != SAWO_OK) {
        longjmp(sort_info->jmpbuf, SAWO_ERR);
    }
    return sort_info->subfn(&lObj, &rObj);
}


static int ListSortString(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    return sawo_StringCompareObj(sort_info->interp, *lhsObj, *rhsObj, 0) * sort_info->order;
}

static int ListSortStringNoCase(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    return sawo_StringCompareObj(sort_info->interp, *lhsObj, *rhsObj, 1) * sort_info->order;
}

static int ListSortInteger(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    sawo_wide lhs = 0, rhs = 0;

    if (sawo_GetWide(sort_info->interp, *lhsObj, &lhs) != SAWO_OK ||
        sawo_GetWide(sort_info->interp, *rhsObj, &rhs) != SAWO_OK) {
        longjmp(sort_info->jmpbuf, SAWO_ERR);
    }

    return sawoSign(lhs - rhs) * sort_info->order;
}

static int ListSortReal(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    double lhs = 0, rhs = 0;

    if (sawo_GetDouble(sort_info->interp, *lhsObj, &lhs) != SAWO_OK ||
        sawo_GetDouble(sort_info->interp, *rhsObj, &rhs) != SAWO_OK) {
        longjmp(sort_info->jmpbuf, SAWO_ERR);
    }
    if (lhs == rhs) {
        return 0;
    }
    if (lhs > rhs) {
        return sort_info->order;
    }
    return -sort_info->order;
}

static int ListSortCommand(sawo_Obj **lhsObj, sawo_Obj **rhsObj)
{
    sawo_Obj *compare_script;
    int rc;

    sawo_wide ret = 0;

    
    compare_script = sawo_DuplicateObj(sort_info->interp, sort_info->command);
    sawo_ListAppendElement(sort_info->interp, compare_script, *lhsObj);
    sawo_ListAppendElement(sort_info->interp, compare_script, *rhsObj);

    rc = sawo_EvalObj(sort_info->interp, compare_script);

    if (rc != SAWO_OK || sawo_GetWide(sort_info->interp, sawo_GetResult(sort_info->interp), &ret) != SAWO_OK) {
        longjmp(sort_info->jmpbuf, rc);
    }

    return sawoSign(ret) * sort_info->order;
}

static void ListRemoveDuplicates(sawo_Obj *listObjPtr, int (*comp)(sawo_Obj **lhs, sawo_Obj **rhs))
{
    int src;
    int dst = 0;
    sawo_Obj **ele = listObjPtr->internalRep.listValue.ele;

    for (src = 1; src < listObjPtr->internalRep.listValue.len; src++) {
        if (comp(&ele[dst], &ele[src]) == 0) {
            
            sawo_DecrRefCount(sort_info->interp, ele[dst]);
        }
        else {
            
            dst++;
        }
        ele[dst] = ele[src];
    }
    
    ele[++dst] = ele[src];

    
    listObjPtr->internalRep.listValue.len = dst;
}


static int ListSortElements(sawo_Interp *interp, sawo_Obj *listObjPtr, struct lsort_info *show)
{
    struct lsort_info *prev_info;

    typedef int (qsort_comparator) (const void *, const void *);
    int (*fn) (sawo_Obj **, sawo_Obj **);
    sawo_Obj **vector;
    int len;
    int rc;

    sawoPanic((sawo_IsShared(listObjPtr), "ListSortElements called with shared object"));
    SetListFromAny(interp, listObjPtr);

    
    prev_info = sort_info;
    sort_info = show;

    vector = listObjPtr->internalRep.listValue.ele;
    len = listObjPtr->internalRep.listValue.len;
    switch (show->type) {
        case SAWO_LSORT_ASCII:
            fn = ListSortString;
            break;
        case SAWO_LSORT_NOCASE:
            fn = ListSortStringNoCase;
            break;
        case SAWO_LSORT_INTEGER:
            fn = ListSortInteger;
            break;
        case SAWO_LSORT_REAL:
            fn = ListSortReal;
            break;
        case SAWO_LSORT_COMMAND:
            fn = ListSortCommand;
            break;
        default:
            fn = NULL;          
            sawoPanic((1, "ListSort called with invalid sort type"));
            return -1; 
    }

    if (show->indexed) {
        
        show->subfn = fn;
        fn = ListSortIndexHelper;
    }

    if ((rc = setjmp(show->jmpbuf)) == 0) {
        qsort(vector, len, sizeof(sawo_Obj *), (qsort_comparator *) fn);

        if (show->unique && len > 1) {
            ListRemoveDuplicates(listObjPtr, fn);
        }

        sawo_InvalidateStringRep(listObjPtr);
    }
    sort_info = prev_info;

    return rc;
}

static void ListInsertElements(sawo_Obj *listPtr, int idx, int elemc, sawo_Obj *const *elemVec)
{
    int currentLen = listPtr->internalRep.listValue.len;
    int requiredLen = currentLen + elemc;
    int i;
    sawo_Obj **point;

    if (requiredLen > listPtr->internalRep.listValue.maxLen) {
        if (requiredLen < 2) {
            
            requiredLen = 4;
        }
        else {
            requiredLen *= 2;
        }

        listPtr->internalRep.listValue.ele = sawo_Realloc(listPtr->internalRep.listValue.ele,
            sizeof(sawo_Obj *) * requiredLen);

        listPtr->internalRep.listValue.maxLen = requiredLen;
    }
    if (idx < 0) {
        idx = currentLen;
    }
    point = listPtr->internalRep.listValue.ele + idx;
    memmove(point + elemc, point, (currentLen - idx) * sizeof(sawo_Obj *));
    for (i = 0; i < elemc; ++i) {
        point[i] = elemVec[i];
        sawo_IncrRefCount(point[i]);
    }
    listPtr->internalRep.listValue.len += elemc;
}

static void ListAppendElement(sawo_Obj *listPtr, sawo_Obj *objPtr)
{
    ListInsertElements(listPtr, -1, 1, &objPtr);
}

static void ListAppendList(sawo_Obj *listPtr, sawo_Obj *appendListPtr)
{
    ListInsertElements(listPtr, -1,
        appendListPtr->internalRep.listValue.len, appendListPtr->internalRep.listValue.ele);
}

void sawo_ListAppendElement(sawo_Interp *interp, sawo_Obj *listPtr, sawo_Obj *objPtr)
{
    sawoPanic((sawo_IsShared(listPtr), "sawo_ListAppendElement called with shared object"));
    SetListFromAny(interp, listPtr);
    sawo_InvalidateStringRep(listPtr);
    ListAppendElement(listPtr, objPtr);
}

void sawo_ListAppendList(sawo_Interp *interp, sawo_Obj *listPtr, sawo_Obj *appendListPtr)
{
    sawoPanic((sawo_IsShared(listPtr), "sawo_ListAppendList called with shared object"));
    SetListFromAny(interp, listPtr);
    SetListFromAny(interp, appendListPtr);
    sawo_InvalidateStringRep(listPtr);
    ListAppendList(listPtr, appendListPtr);
}

int sawo_ListLength(sawo_Interp *interp, sawo_Obj *objPtr)
{
    SetListFromAny(interp, objPtr);
    return objPtr->internalRep.listValue.len;
}

void sawo_ListInsertElements(sawo_Interp *interp, sawo_Obj *listPtr, int idx,
    int objc, sawo_Obj *const *objVec)
{
    sawoPanic((sawo_IsShared(listPtr), "sawo_ListInsertElement called with shared object"));
    SetListFromAny(interp, listPtr);
    if (idx >= 0 && idx > listPtr->internalRep.listValue.len)
        idx = listPtr->internalRep.listValue.len;
    else if (idx < 0)
        idx = 0;
    sawo_InvalidateStringRep(listPtr);
    ListInsertElements(listPtr, idx, objc, objVec);
}

sawo_Obj *sawo_ListGetIndex(sawo_Interp *interp, sawo_Obj *listPtr, int idx)
{
    SetListFromAny(interp, listPtr);
    if ((idx >= 0 && idx >= listPtr->internalRep.listValue.len) ||
        (idx < 0 && (-idx - 1) >= listPtr->internalRep.listValue.len)) {
        return NULL;
    }
    if (idx < 0)
        idx = listPtr->internalRep.listValue.len + idx;
    return listPtr->internalRep.listValue.ele[idx];
}

int sawo_ListIndex(sawo_Interp *interp, sawo_Obj *listPtr, int idx, sawo_Obj **objPtrPtr, int flags)
{
    *objPtrPtr = sawo_ListGetIndex(interp, listPtr, idx);
    if (*objPtrPtr == NULL) {
        if (flags & SAWO_ERRMSG) {
            sawo_SetResultString(interp, "list index out of range", -1);
        }
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int ListSetIndex(sawo_Interp *interp, sawo_Obj *listPtr, int idx,
    sawo_Obj *newObjPtr, int flags)
{
    SetListFromAny(interp, listPtr);
    if ((idx >= 0 && idx >= listPtr->internalRep.listValue.len) ||
        (idx < 0 && (-idx - 1) >= listPtr->internalRep.listValue.len)) {
        if (flags & SAWO_ERRMSG) {
            sawo_SetResultString(interp, "list index out of range", -1);
        }
        return SAWO_ERR;
    }
    if (idx < 0)
        idx = listPtr->internalRep.listValue.len + idx;
    sawo_DecrRefCount(interp, listPtr->internalRep.listValue.ele[idx]);
    listPtr->internalRep.listValue.ele[idx] = newObjPtr;
    sawo_IncrRefCount(newObjPtr);
    return SAWO_OK;
}

int sawo_ListSetIndex(sawo_Interp *interp, sawo_Obj *varNamePtr,
    sawo_Obj *const *indexv, int indexc, sawo_Obj *newObjPtr)
{
    sawo_Obj *varObjPtr, *objPtr, *listObjPtr;
    int shared, i, idx;

    varObjPtr = objPtr = sawo_GetVariable(interp, varNamePtr, SAWO_ERRMSG | SAWO_UNSHARED);
    if (objPtr == NULL)
        return SAWO_ERR;
    if ((shared = sawo_IsShared(objPtr)))
        varObjPtr = objPtr = sawo_DuplicateObj(interp, objPtr);
    for (i = 0; i < indexc - 1; i++) {
        listObjPtr = objPtr;
        if (sawo_GetIndex(interp, indexv[i], &idx) != SAWO_OK)
            goto err;
        if (sawo_ListIndex(interp, listObjPtr, idx, &objPtr, SAWO_ERRMSG) != SAWO_OK) {
            goto err;
        }
        if (sawo_IsShared(objPtr)) {
            objPtr = sawo_DuplicateObj(interp, objPtr);
            ListSetIndex(interp, listObjPtr, idx, objPtr, SAWO_NONE);
        }
        sawo_InvalidateStringRep(listObjPtr);
    }
    if (sawo_GetIndex(interp, indexv[indexc - 1], &idx) != SAWO_OK)
        goto err;
    if (ListSetIndex(interp, objPtr, idx, newObjPtr, SAWO_ERRMSG) == SAWO_ERR)
        goto err;
    sawo_InvalidateStringRep(objPtr);
    sawo_InvalidateStringRep(varObjPtr);
    if (sawo_SetVariable(interp, varNamePtr, varObjPtr) != SAWO_OK)
        goto err;
    sawo_SetResult(interp, varObjPtr);
    return SAWO_OK;
  err:
    if (shared) {
        sawo_FreeNewObj(interp, varObjPtr);
    }
    return SAWO_ERR;
}

sawo_Obj *sawo_ListJoin(sawo_Interp *interp, sawo_Obj *listObjPtr, const char *joinStr, int joinStrLen)
{
    int i;
    int listLen = sawo_ListLength(interp, listObjPtr);
    sawo_Obj *resObjPtr = sawo_NewEmptyStringObj(interp);

    for (i = 0; i < listLen; ) {
        sawo_AppendObj(interp, resObjPtr, sawo_ListGetIndex(interp, listObjPtr, i));
        if (++i != listLen) {
            sawo_AppendString(interp, resObjPtr, joinStr, joinStrLen);
        }
    }
    return resObjPtr;
}

sawo_Obj *sawo_ConcatObj(sawo_Interp *interp, int objc, sawo_Obj *const *objv)
{
    int i;

    for (i = 0; i < objc; i++) {
        if (!sawo_IsList(objv[i]))
            break;
    }
    if (i == objc) {
        sawo_Obj *objPtr = sawo_NewListObj(interp, NULL, 0);

        for (i = 0; i < objc; i++)
            ListAppendList(objPtr, objv[i]);
        return objPtr;
    }
    else {
        
        int len = 0, objLen;
        char *bytes, *p;

        
        for (i = 0; i < objc; i++) {
            len += sawo_Length(objv[i]);
        }
        if (objc)
            len += objc - 1;
        
        p = bytes = sawo_Alloc(len + 1);
        for (i = 0; i < objc; i++) {
            const char *s = sawo_GetString(objv[i], &objLen);

            
            while (objLen && isspace(UCHAR(*s))) {
                s++;
                objLen--;
                len--;
            }
            
            while (objLen && isspace(UCHAR(s[objLen - 1]))) {
                
                if (objLen > 1 && s[objLen - 2] == '\\') {
                    break;
                }
                objLen--;
                len--;
            }
            memcpy(p, s, objLen);
            p += objLen;
            if (i + 1 != objc) {
                if (objLen)
                    *p++ = ' ';
                else {
                    len--;
                }
            }
        }
        *p = '\0';
        return sawo_NewStringObjNoAlloc(interp, bytes, len);
    }
}

sawo_Obj *sawo_ListRange(sawo_Interp *interp, sawo_Obj *listObjPtr, sawo_Obj *firstObjPtr,
    sawo_Obj *lastObjPtr)
{
    int first, last;
    int len, rangeLen;

    if (sawo_GetIndex(interp, firstObjPtr, &first) != SAWO_OK ||
        sawo_GetIndex(interp, lastObjPtr, &last) != SAWO_OK)
        return NULL;
    len = sawo_ListLength(interp, listObjPtr);   
    first = sawoRelToAbsIndex(len, first);
    last = sawoRelToAbsIndex(len, last);
    sawoRelToAbsRange(len, &first, &last, &rangeLen);
    if (first == 0 && last == len) {
        return listObjPtr;
    }
    return sawo_NewListObj(interp, listObjPtr->internalRep.listValue.ele + first, rangeLen);
}

static void FreeDictInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupDictInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);
static void UpdateStringOfDict(struct sawo_Obj *objPtr);
static int SetDictFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);


static unsigned int sawoObjectHTHashFunction(const void *key)
{
    int len;
    const char *str = sawo_GetString((sawo_Obj *)key, &len);
    return sawo_GenHashFunction((const unsigned char *)str, len);
}

static int sawoObjectHTKeyCompare(void *privdata, const void *key1, const void *key2)
{
    return sawo_StringEqObj((sawo_Obj *)key1, (sawo_Obj *)key2);
}

static void *sawoObjectHTKeyValDup(void *privdata, const void *val)
{
    sawo_IncrRefCount((sawo_Obj *)val);
    return (void *)val;
}

static void sawoObjectHTKeyValDestructor(void *interp, void *val)
{
    sawo_DecrRefCount(interp, (sawo_Obj *)val);
}

static const sawo_HashTableType sawoDictHashTableType = {
    sawoObjectHTHashFunction,    
    sawoObjectHTKeyValDup,       
    sawoObjectHTKeyValDup,       
    sawoObjectHTKeyCompare,      
    sawoObjectHTKeyValDestructor,    
    sawoObjectHTKeyValDestructor 
};

static const sawo_ObjType dictObjType = {
    "dict",
    FreeDictInternalRep,
    DupDictInternalRep,
    UpdateStringOfDict,
    SAWO_TYPE_NONE,
};

void FreeDictInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    SAWO_NOTUSED(interp);

    sawo_FreeHashTable(objPtr->internalRep.ptr);
    sawo_Free(objPtr->internalRep.ptr);
}

void DupDictInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    sawo_HashTable *ht, *dupHt;
    sawo_HashTableIterator htiter;
    sawo_HashEntry *he;

    
    ht = srcPtr->internalRep.ptr;
    dupHt = sawo_Alloc(sizeof(*dupHt));
    sawo_InitHashTable(dupHt, &sawoDictHashTableType, interp);
    if (ht->size != 0)
        sawo_ExpandHashTable(dupHt, ht->size);
    
    sawoInitHashTableIterator(ht, &htiter);
    while ((he = sawo_NextHashEntry(&htiter)) != NULL) {
        sawo_AddHashEntry(dupHt, he->key, he->u.val);
    }

    dupPtr->internalRep.ptr = dupHt;
    dupPtr->typePtr = &dictObjType;
}

static sawo_Obj **sawoDictPairs(sawo_Obj *dictPtr, int *len)
{
    sawo_HashTable *ht;
    sawo_HashTableIterator htiter;
    sawo_HashEntry *he;
    sawo_Obj **objv;
    int i;

    ht = dictPtr->internalRep.ptr;

    
    objv = sawo_Alloc((ht->used * 2) * sizeof(sawo_Obj *));
    sawoInitHashTableIterator(ht, &htiter);
    i = 0;
    while ((he = sawo_NextHashEntry(&htiter)) != NULL) {
        objv[i++] = sawo_GetHashEntryKey(he);
        objv[i++] = sawo_GetHashEntryVal(he);
    }
    *len = i;
    return objv;
}

static void UpdateStringOfDict(struct sawo_Obj *objPtr)
{
    
    int len;
    sawo_Obj **objv = sawoDictPairs(objPtr, &len);

    
    sawoMakeListStringRep(objPtr, objv, len);

    sawo_Free(objv);
}

static int SetDictFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr)
{
    int listlen;

    if (objPtr->typePtr == &dictObjType) {
        return SAWO_OK;
    }

    if (sawo_IsList(objPtr) && sawo_IsShared(objPtr)) {
        sawo_String(objPtr);
    }

    
    listlen = sawo_ListLength(interp, objPtr);
    if (listlen % 2) {
        sawo_SetResultString(interp, "missing value to go with key", -1);
        return SAWO_ERR;
    }
    else {
        
        sawo_HashTable *ht;
        int i;

        ht = sawo_Alloc(sizeof(*ht));
        sawo_InitHashTable(ht, &sawoDictHashTableType, interp);

        for (i = 0; i < listlen; i += 2) {
            sawo_Obj *keyObjPtr = sawo_ListGetIndex(interp, objPtr, i);
            sawo_Obj *valObjPtr = sawo_ListGetIndex(interp, objPtr, i + 1);

            sawo_ReplaceHashEntry(ht, keyObjPtr, valObjPtr);
        }

        sawo_FreeIntRep(interp, objPtr);
        objPtr->typePtr = &dictObjType;
        objPtr->internalRep.ptr = ht;

        return SAWO_OK;
    }
}



static int DictAddElement(sawo_Interp *interp, sawo_Obj *objPtr,
    sawo_Obj *keyObjPtr, sawo_Obj *valueObjPtr)
{
    sawo_HashTable *ht = objPtr->internalRep.ptr;

    if (valueObjPtr == NULL) {  
        return sawo_DeleteHashEntry(ht, keyObjPtr);
    }
    sawo_ReplaceHashEntry(ht, keyObjPtr, valueObjPtr);
    return SAWO_OK;
}

int sawo_DictAddElement(sawo_Interp *interp, sawo_Obj *objPtr,
    sawo_Obj *keyObjPtr, sawo_Obj *valueObjPtr)
{
    sawoPanic((sawo_IsShared(objPtr), "sawo_DictAddElement called with shared object"));
    if (SetDictFromAny(interp, objPtr) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_InvalidateStringRep(objPtr);
    return DictAddElement(interp, objPtr, keyObjPtr, valueObjPtr);
}

sawo_Obj *sawo_NewDictObj(sawo_Interp *interp, sawo_Obj *const *elements, int len)
{
    sawo_Obj *objPtr;
    int i;

    sawoPanic((len % 2, "sawo_NewDictObj() 'len' argument must be even"));

    objPtr = sawo_NewObj(interp);
    objPtr->typePtr = &dictObjType;
    objPtr->bytes = NULL;
    objPtr->internalRep.ptr = sawo_Alloc(sizeof(sawo_HashTable));
    sawo_InitHashTable(objPtr->internalRep.ptr, &sawoDictHashTableType, interp);
    for (i = 0; i < len; i += 2)
        DictAddElement(interp, objPtr, elements[i], elements[i + 1]);
    return objPtr;
}

int sawo_DictKey(sawo_Interp *interp, sawo_Obj *dictPtr, sawo_Obj *keyPtr,
    sawo_Obj **objPtrPtr, int flags)
{
    sawo_HashEntry *he;
    sawo_HashTable *ht;

    if (SetDictFromAny(interp, dictPtr) != SAWO_OK) {
        return -1;
    }
    ht = dictPtr->internalRep.ptr;
    if ((he = sawo_FindHashEntry(ht, keyPtr)) == NULL) {
        if (flags & SAWO_ERRMSG) {
            sawo_SetResultFormatted(interp, "key \"%#s\" not known in dictionary", keyPtr);
        }
        return SAWO_ERR;
    }
    *objPtrPtr = he->u.val;
    return SAWO_OK;
}


int sawo_DictPairs(sawo_Interp *interp, sawo_Obj *dictPtr, sawo_Obj ***objPtrPtr, int *len)
{
    if (SetDictFromAny(interp, dictPtr) != SAWO_OK) {
        return SAWO_ERR;
    }
    *objPtrPtr = sawoDictPairs(dictPtr, len);

    return SAWO_OK;
}



int sawo_DictKeysVector(sawo_Interp *interp, sawo_Obj *dictPtr,
    sawo_Obj *const *keyv, int keyc, sawo_Obj **objPtrPtr, int flags)
{
    int i;

    if (keyc == 0) {
        *objPtrPtr = dictPtr;
        return SAWO_OK;
    }

    for (i = 0; i < keyc; i++) {
        sawo_Obj *objPtr;

        int rc = sawo_DictKey(interp, dictPtr, keyv[i], &objPtr, flags);
        if (rc != SAWO_OK) {
            return rc;
        }
        dictPtr = objPtr;
    }
    *objPtrPtr = dictPtr;
    return SAWO_OK;
}

int sawo_SetDictKeysVector(sawo_Interp *interp, sawo_Obj *varNamePtr,
    sawo_Obj *const *keyv, int keyc, sawo_Obj *newObjPtr, int flags)
{
    sawo_Obj *varObjPtr, *objPtr, *dictObjPtr;
    int shared, i;

    varObjPtr = objPtr = sawo_GetVariable(interp, varNamePtr, flags);
    if (objPtr == NULL) {
        if (newObjPtr == NULL && (flags & SAWO_MUSTEXIST)) {
            
            return SAWO_ERR;
        }
        varObjPtr = objPtr = sawo_NewDictObj(interp, NULL, 0);
        if (sawo_SetVariable(interp, varNamePtr, objPtr) != SAWO_OK) {
            sawo_FreeNewObj(interp, varObjPtr);
            return SAWO_ERR;
        }
    }
    if ((shared = sawo_IsShared(objPtr)))
        varObjPtr = objPtr = sawo_DuplicateObj(interp, objPtr);
    for (i = 0; i < keyc; i++) {
        dictObjPtr = objPtr;

        
        if (SetDictFromAny(interp, dictObjPtr) != SAWO_OK) {
            goto err;
        }

        if (i == keyc - 1) {
            
            if (sawo_DictAddElement(interp, objPtr, keyv[keyc - 1], newObjPtr) != SAWO_OK) {
                if (newObjPtr || (flags & SAWO_MUSTEXIST)) {
                    goto err;
                }
            }
            break;
        }

        
        sawo_InvalidateStringRep(dictObjPtr);
        if (sawo_DictKey(interp, dictObjPtr, keyv[i], &objPtr,
                newObjPtr ? SAWO_NONE : SAWO_ERRMSG) == SAWO_OK) {
            if (sawo_IsShared(objPtr)) {
                objPtr = sawo_DuplicateObj(interp, objPtr);
                DictAddElement(interp, dictObjPtr, keyv[i], objPtr);
            }
        }
        else {
            if (newObjPtr == NULL) {
                goto err;
            }
            objPtr = sawo_NewDictObj(interp, NULL, 0);
            DictAddElement(interp, dictObjPtr, keyv[i], objPtr);
        }
    }
    
    sawo_InvalidateStringRep(objPtr);
    sawo_InvalidateStringRep(varObjPtr);
    if (sawo_SetVariable(interp, varNamePtr, varObjPtr) != SAWO_OK) {
        goto err;
    }
    sawo_SetResult(interp, varObjPtr);
    return SAWO_OK;
  err:
    if (shared) {
        sawo_FreeNewObj(interp, varObjPtr);
    }
    return SAWO_ERR;
}

static void UpdateStringOfIndex(struct sawo_Obj *objPtr);
static int SetIndexFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);

static const sawo_ObjType indexObjType = {
    "index",
    NULL,
    NULL,
    UpdateStringOfIndex,
    SAWO_TYPE_NONE,
};

static void UpdateStringOfIndex(struct sawo_Obj *objPtr)
{
    if (objPtr->internalRep.intValue == -1) {
        sawoSetStringBytes(objPtr, "end");
    }
    else {
        char buf[SAWO_INTEGER_SPACE + 1];
        if (objPtr->internalRep.intValue >= 0) {
            sprintf(buf, "%d", objPtr->internalRep.intValue);
        }
        else {
            
            sprintf(buf, "end%d", objPtr->internalRep.intValue + 1);
        }
        sawoSetStringBytes(objPtr, buf);
    }
}

static int SetIndexFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    int idx, end = 0;
    const char *str;
    char *endptr;

    
    str = sawo_String(objPtr);

    
    if (strncmp(str, "end", 3) == 0) {
        end = 1;
        str += 3;
        idx = 0;
    }
    else {
        idx = sawo_strtol(str, &endptr);

        if (endptr == str) {
            goto badindex;
        }
        str = endptr;
    }

    
    if (*str == '+' || *str == '-') {
        int sign = (*str == '+' ? 1 : -1);

        idx += sign * sawo_strtol(++str, &endptr);
        if (str == endptr || *endptr) {
            goto badindex;
        }
        str = endptr;
    }
    
    while (isspace(UCHAR(*str))) {
        str++;
    }
    if (*str) {
        goto badindex;
    }
    if (end) {
        if (idx > 0) {
            idx = INT_MAX;
        }
        else {
            
            idx--;
        }
    }
    else if (idx < 0) {
        idx = -INT_MAX;
    }

    
    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &indexObjType;
    objPtr->internalRep.intValue = idx;
    return SAWO_OK;

  badindex:
    sawo_SetResultFormatted(interp,
        "bad index \"%#s\": must be integer?[+-]integer? or end?[+-]integer?", objPtr);
    return SAWO_ERR;
}

int sawo_GetIndex(sawo_Interp *interp, sawo_Obj *objPtr, int *indexPtr)
{
    
    if (objPtr->typePtr == &intObjType) {
        sawo_wide val = sawoWideValue(objPtr);

        if (val < 0)
            *indexPtr = -INT_MAX;
        else if (val > INT_MAX)
            *indexPtr = INT_MAX;
        else
            *indexPtr = (int)val;
        return SAWO_OK;
    }
    if (objPtr->typePtr != &indexObjType && SetIndexFromAny(interp, objPtr) == SAWO_ERR)
        return SAWO_ERR;
    *indexPtr = objPtr->internalRep.intValue;
    return SAWO_OK;
}



static const char * const sawoReturnCodes[] = {
    "ok",
    "error",
    "return",
    "break",
    "continue",
    "signal",
    "exit",
    "eval",
    NULL
};

#define sawoReturnCodesSize (sizeof(sawoReturnCodes)/sizeof(*sawoReturnCodes))

static const sawo_ObjType returnCodeObjType = {
    "return-code",
    NULL,
    NULL,
    NULL,
    SAWO_TYPE_NONE,
};

const char *sawo_ReturnCode(int code)
{
    if (code < 0 || code >= (int)sawoReturnCodesSize) {
        return "?";
    }
    else {
        return sawoReturnCodes[code];
    }
}

static int SetReturnCodeFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    int returnCode;
    sawo_wide wideValue;

    
    if (sawoGetWideNoErr(interp, objPtr, &wideValue) != SAWO_ERR)
        returnCode = (int)wideValue;
    else if (sawo_GetEnum(interp, objPtr, sawoReturnCodes, &returnCode, NULL, SAWO_NONE) != SAWO_OK) {
        sawo_SetResultFormatted(interp, "expected return code but got \"%#s\"", objPtr);
        return SAWO_ERR;
    }
    
    sawo_FreeIntRep(interp, objPtr);
    objPtr->typePtr = &returnCodeObjType;
    objPtr->internalRep.intValue = returnCode;
    return SAWO_OK;
}

int sawo_GetReturnCode(sawo_Interp *interp, sawo_Obj *objPtr, int *intPtr)
{
    if (objPtr->typePtr != &returnCodeObjType && SetReturnCodeFromAny(interp, objPtr) == SAWO_ERR)
        return SAWO_ERR;
    *intPtr = objPtr->internalRep.intValue;
    return SAWO_OK;
}

static int sawoParseExprOperator(struct sawoParserCtx *pc);
static int sawoParseExprNumber(struct sawoParserCtx *pc);
static int sawoParseExprIrrational(struct sawoParserCtx *pc);




enum
{
    
    
    SAWO_EXPROP_MUL = SAWO_TT_EXPR_OP,             
    SAWO_EXPROP_DIV,
    SAWO_EXPROP_MOD,
    SAWO_EXPROP_SUB,
    SAWO_EXPROP_ADD,
    SAWO_EXPROP_LSHIFT,
    SAWO_EXPROP_RSHIFT,
    SAWO_EXPROP_ROTL,
    SAWO_EXPROP_ROTR,
    SAWO_EXPROP_LT,
    SAWO_EXPROP_GT,
    SAWO_EXPROP_LTE,
    SAWO_EXPROP_GTE,
    SAWO_EXPROP_NUMEQ,
    SAWO_EXPROP_NUMNE,
    SAWO_EXPROP_BITAND,          
    SAWO_EXPROP_BITXOR,
    SAWO_EXPROP_BITOR,

    
    SAWO_EXPROP_LOGICAND,        
    SAWO_EXPROP_LOGICAND_LEFT,
    SAWO_EXPROP_LOGICAND_RIGHT,

    
    SAWO_EXPROP_LOGICOR,         
    SAWO_EXPROP_LOGICOR_LEFT,
    SAWO_EXPROP_LOGICOR_RIGHT,

    
    
    SAWO_EXPROP_TERNARY,         
    SAWO_EXPROP_TERNARY_LEFT,
    SAWO_EXPROP_TERNARY_RIGHT,

    
    SAWO_EXPROP_COLON,           
    SAWO_EXPROP_COLON_LEFT,
    SAWO_EXPROP_COLON_RIGHT,

    SAWO_EXPROP_POW,             


    SAWO_EXPROP_STREQ,           
    SAWO_EXPROP_STRNE,
    SAWO_EXPROP_STRIN,
    SAWO_EXPROP_STRNI,


    SAWO_EXPROP_NOT,             
    SAWO_EXPROP_BITNOT,
    SAWO_EXPROP_UNARYMINUS,
    SAWO_EXPROP_UNARYPLUS,

    
    SAWO_EXPROP_FUNC_FIRST,      
    SAWO_EXPROP_FUNC_INT = SAWO_EXPROP_FUNC_FIRST,
    SAWO_EXPROP_FUNC_WIDE,
    SAWO_EXPROP_FUNC_ABS,
    SAWO_EXPROP_FUNC_DOUBLE,
    SAWO_EXPROP_FUNC_ROUND,
    SAWO_EXPROP_FUNC_RAND,
    SAWO_EXPROP_FUNC_SRAND,

    
    SAWO_EXPROP_FUNC_SIN,        
    SAWO_EXPROP_FUNC_COS,
    SAWO_EXPROP_FUNC_TAN,
    SAWO_EXPROP_FUNC_ASIN,
    SAWO_EXPROP_FUNC_ACOS,
    SAWO_EXPROP_FUNC_ATAN,
    SAWO_EXPROP_FUNC_SINH,
    SAWO_EXPROP_FUNC_COSH,
    SAWO_EXPROP_FUNC_TANH,
    SAWO_EXPROP_FUNC_CEIL,
    SAWO_EXPROP_FUNC_FLOOR,
    SAWO_EXPROP_FUNC_EXP,
    SAWO_EXPROP_FUNC_LOG,
    SAWO_EXPROP_FUNC_LOG10,
    SAWO_EXPROP_FUNC_SQRT,
    SAWO_EXPROP_FUNC_POW,
};

struct sawoExprState
{
    sawo_Obj **stack;
    int stacklen;
    int opcode;
    int skip;
};


typedef struct sawo_ExprOperator
{
    const char *name;
    int (*funcop) (sawo_Interp *interp, struct sawoExprState * e);
    unsigned char precedence;
    unsigned char arity;
    unsigned char lazy;
    unsigned char namelen;
} sawo_ExprOperator;

static void ExprPush(struct sawoExprState *e, sawo_Obj *obj)
{
    sawo_IncrRefCount(obj);
    e->stack[e->stacklen++] = obj;
}

static sawo_Obj *ExprPop(struct sawoExprState *e)
{
    return e->stack[--e->stacklen];
}

static int sawoExprOpNumUnary(sawo_Interp *interp, struct sawoExprState *e)
{
    int intresult = 1;
    int rc = SAWO_OK;
    sawo_Obj *A = ExprPop(e);
    double dA, dC = 0;
    sawo_wide wA, wC = 0;

    if ((A->typePtr != &doubleObjType || A->bytes) && sawoGetWideNoErr(interp, A, &wA) == SAWO_OK) {
        switch (e->opcode) {
            case SAWO_EXPROP_FUNC_INT:
            case SAWO_EXPROP_FUNC_WIDE:
            case SAWO_EXPROP_FUNC_ROUND:
            case SAWO_EXPROP_UNARYPLUS:
                wC = wA;
                break;
            case SAWO_EXPROP_FUNC_DOUBLE:
                dC = wA;
                intresult = 0;
                break;
            case SAWO_EXPROP_FUNC_ABS:
                wC = wA >= 0 ? wA : -wA;
                break;
            case SAWO_EXPROP_UNARYMINUS:
                wC = -wA;
                break;
            case SAWO_EXPROP_NOT:
                wC = !wA;
                break;
            default:
                abort();
        }
    }
    else if ((rc = sawo_GetDouble(interp, A, &dA)) == SAWO_OK) {
        switch (e->opcode) {
            case SAWO_EXPROP_FUNC_INT:
            case SAWO_EXPROP_FUNC_WIDE:
                wC = dA;
                break;
            case SAWO_EXPROP_FUNC_ROUND:
                wC = dA < 0 ? (dA - 0.5) : (dA + 0.5);
                break;
            case SAWO_EXPROP_FUNC_DOUBLE:
            case SAWO_EXPROP_UNARYPLUS:
                dC = dA;
                intresult = 0;
                break;
            case SAWO_EXPROP_FUNC_ABS:
                dC = dA >= 0 ? dA : -dA;
                intresult = 0;
                break;
            case SAWO_EXPROP_UNARYMINUS:
                dC = -dA;
                intresult = 0;
                break;
            case SAWO_EXPROP_NOT:
                wC = !dA;
                break;
            default:
                abort();
        }
    }

    if (rc == SAWO_OK) {
        if (intresult) {
            ExprPush(e, sawo_NewIntObj(interp, wC));
        }
        else {
            ExprPush(e, sawo_NewDoubleObj(interp, dC));
        }
    }

    sawo_DecrRefCount(interp, A);

    return rc;
}

static double sawoRandDouble(sawo_Interp *interp)
{
    unsigned long x;
    sawoRandomBytes(interp, &x, sizeof(x));

    return (double)x / (unsigned long)~0;
}

static int sawoExprOpIntUnary(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *A = ExprPop(e);
    sawo_wide wA;

    int rc = sawo_GetWide(interp, A, &wA);
    if (rc == SAWO_OK) {
        switch (e->opcode) {
            case SAWO_EXPROP_BITNOT:
                ExprPush(e, sawo_NewIntObj(interp, ~wA));
                break;
            case SAWO_EXPROP_FUNC_SRAND:
                sawoPrngSeed(interp, (unsigned char *)&wA, sizeof(wA));
                ExprPush(e, sawo_NewDoubleObj(interp, sawoRandDouble(interp)));
                break;
            default:
                abort();
        }
    }

    sawo_DecrRefCount(interp, A);

    return rc;
}

static int sawoExprOpNone(sawo_Interp *interp, struct sawoExprState *e)
{
    sawoPanic((e->opcode != SAWO_EXPROP_FUNC_RAND, "sawoExprOpNone only support rand()"));

    ExprPush(e, sawo_NewDoubleObj(interp, sawoRandDouble(interp)));

    return SAWO_OK;
}

#ifdef SAWO_MATH_FUNCTIONS
static int sawoExprOpDoubleUnary(sawo_Interp *interp, struct sawoExprState *e)
{
    int rc;
    sawo_Obj *A = ExprPop(e);
    double dA, dC;

    rc = sawo_GetDouble(interp, A, &dA);
    if (rc == SAWO_OK) {
        switch (e->opcode) {
            case SAWO_EXPROP_FUNC_SIN:
                dC = sin(dA);
                break;
            case SAWO_EXPROP_FUNC_COS:
                dC = cos(dA);
                break;
            case SAWO_EXPROP_FUNC_TAN:
                dC = tan(dA);
                break;
            case SAWO_EXPROP_FUNC_ASIN:
                dC = asin(dA);
                break;
            case SAWO_EXPROP_FUNC_ACOS:
                dC = acos(dA);
                break;
            case SAWO_EXPROP_FUNC_ATAN:
                dC = atan(dA);
                break;
            case SAWO_EXPROP_FUNC_SINH:
                dC = sinh(dA);
                break;
            case SAWO_EXPROP_FUNC_COSH:
                dC = cosh(dA);
                break;
            case SAWO_EXPROP_FUNC_TANH:
                dC = tanh(dA);
                break;
            case SAWO_EXPROP_FUNC_CEIL:
                dC = ceil(dA);
                break;
            case SAWO_EXPROP_FUNC_FLOOR:
                dC = floor(dA);
                break;
            case SAWO_EXPROP_FUNC_EXP:
                dC = exp(dA);
                break;
            case SAWO_EXPROP_FUNC_LOG:
                dC = log(dA);
                break;
            case SAWO_EXPROP_FUNC_LOG10:
                dC = log10(dA);
                break;
            case SAWO_EXPROP_FUNC_SQRT:
                dC = sqrt(dA);
                break;
            default:
                abort();
        }
        ExprPush(e, sawo_NewDoubleObj(interp, dC));
    }

    sawo_DecrRefCount(interp, A);

    return rc;
}
#endif


static int sawoExprOpIntBin(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *B = ExprPop(e);
    sawo_Obj *A = ExprPop(e);
    sawo_wide wA, wB;
    int rc = SAWO_ERR;

    if (sawo_GetWide(interp, A, &wA) == SAWO_OK && sawo_GetWide(interp, B, &wB) == SAWO_OK) {
        sawo_wide wC;

        rc = SAWO_OK;

        switch (e->opcode) {
            case SAWO_EXPROP_LSHIFT:
                wC = wA << wB;
                break;
            case SAWO_EXPROP_RSHIFT:
                wC = wA >> wB;
                break;
            case SAWO_EXPROP_BITAND:
                wC = wA & wB;
                break;
            case SAWO_EXPROP_BITXOR:
                wC = wA ^ wB;
                break;
            case SAWO_EXPROP_BITOR:
                wC = wA | wB;
                break;
            case SAWO_EXPROP_MOD:
                if (wB == 0) {
                    wC = 0;
                    sawo_SetResultString(interp, "Division by zero", -1);
                    rc = SAWO_ERR;
                }
                else {
                    int negative = 0;

                    if (wB < 0) {
                        wB = -wB;
                        wA = -wA;
                        negative = 1;
                    }
                    wC = wA % wB;
                    if (wC < 0) {
                        wC += wB;
                    }
                    if (negative) {
                        wC = -wC;
                    }
                }
                break;
            case SAWO_EXPROP_ROTL:
            case SAWO_EXPROP_ROTR:{
                    
                    unsigned long uA = (unsigned long)wA;
                    unsigned long uB = (unsigned long)wB;
                    const unsigned int S = sizeof(unsigned long) * 8;

                    
                    uB %= S;

                    if (e->opcode == SAWO_EXPROP_ROTR) {
                        uB = S - uB;
                    }
                    wC = (unsigned long)(uA << uB) | (uA >> (S - uB));
                    break;
                }
            default:
                abort();
        }
        ExprPush(e, sawo_NewIntObj(interp, wC));

    }

    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, B);

    return rc;
}



static int sawoExprOpBin(sawo_Interp *interp, struct sawoExprState *e)
{
    int intresult = 1;
    int rc = SAWO_OK;
    double dA, dB, dC = 0;
    sawo_wide wA, wB, wC = 0;

    sawo_Obj *B = ExprPop(e);
    sawo_Obj *A = ExprPop(e);

    if ((A->typePtr != &doubleObjType || A->bytes) &&
        (B->typePtr != &doubleObjType || B->bytes) &&
        sawoGetWideNoErr(interp, A, &wA) == SAWO_OK && sawoGetWideNoErr(interp, B, &wB) == SAWO_OK) {

        

        switch (e->opcode) {
            case SAWO_EXPROP_POW:
            case SAWO_EXPROP_FUNC_POW:
                wC = sawoPowWide(wA, wB);
                break;
            case SAWO_EXPROP_ADD:
                wC = wA + wB;
                break;
            case SAWO_EXPROP_SUB:
                wC = wA - wB;
                break;
            case SAWO_EXPROP_MUL:
                wC = wA * wB;
                break;
            case SAWO_EXPROP_DIV:
                if (wB == 0) {
                    sawo_SetResultString(interp, "Division by zero", -1);
                    rc = SAWO_ERR;
                }
                else {
                    if (wB < 0) {
                        wB = -wB;
                        wA = -wA;
                    }
                    wC = wA / wB;
                    if (wA % wB < 0) {
                        wC--;
                    }
                }
                break;
            case SAWO_EXPROP_LT:
                wC = wA < wB;
                break;
            case SAWO_EXPROP_GT:
                wC = wA > wB;
                break;
            case SAWO_EXPROP_LTE:
                wC = wA <= wB;
                break;
            case SAWO_EXPROP_GTE:
                wC = wA >= wB;
                break;
            case SAWO_EXPROP_NUMEQ:
                wC = wA == wB;
                break;
            case SAWO_EXPROP_NUMNE:
                wC = wA != wB;
                break;
            default:
                abort();
        }
    }
    else if (sawo_GetDouble(interp, A, &dA) == SAWO_OK && sawo_GetDouble(interp, B, &dB) == SAWO_OK) {
        intresult = 0;
        switch (e->opcode) {
            case SAWO_EXPROP_POW:
            case SAWO_EXPROP_FUNC_POW:
#ifdef SAWO_MATH_FUNCTIONS
                dC = pow(dA, dB);
#else
                sawo_SetResultString(interp, "unsupported", -1);
                rc = SAWO_ERR;
#endif
                break;
            case SAWO_EXPROP_ADD:
                dC = dA + dB;
                break;
            case SAWO_EXPROP_SUB:
                dC = dA - dB;
                break;
            case SAWO_EXPROP_MUL:
                dC = dA * dB;
                break;
            case SAWO_EXPROP_DIV:
                if (dB == 0) {
#ifdef INFINITY
                    dC = dA < 0 ? -INFINITY : INFINITY;
#else
                    dC = (dA < 0 ? -1.0 : 1.0) * strtod("Inf", NULL);
#endif
                }
                else {
                    dC = dA / dB;
                }
                break;
            case SAWO_EXPROP_LT:
                wC = dA < dB;
                intresult = 1;
                break;
            case SAWO_EXPROP_GT:
                wC = dA > dB;
                intresult = 1;
                break;
            case SAWO_EXPROP_LTE:
                wC = dA <= dB;
                intresult = 1;
                break;
            case SAWO_EXPROP_GTE:
                wC = dA >= dB;
                intresult = 1;
                break;
            case SAWO_EXPROP_NUMEQ:
                wC = dA == dB;
                intresult = 1;
                break;
            case SAWO_EXPROP_NUMNE:
                wC = dA != dB;
                intresult = 1;
                break;
            default:
                abort();
        }
    }
    else {
        

        
        int i = sawo_StringCompareObj(interp, A, B, 0);

        switch (e->opcode) {
            case SAWO_EXPROP_LT:
                wC = i < 0;
                break;
            case SAWO_EXPROP_GT:
                wC = i > 0;
                break;
            case SAWO_EXPROP_LTE:
                wC = i <= 0;
                break;
            case SAWO_EXPROP_GTE:
                wC = i >= 0;
                break;
            case SAWO_EXPROP_NUMEQ:
                wC = i == 0;
                break;
            case SAWO_EXPROP_NUMNE:
                wC = i != 0;
                break;
            default:
                rc = SAWO_ERR;
                break;
        }
    }

    if (rc == SAWO_OK) {
        if (intresult) {
            ExprPush(e, sawo_NewIntObj(interp, wC));
        }
        else {
            ExprPush(e, sawo_NewDoubleObj(interp, dC));
        }
    }

    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, B);

    return rc;
}

static int sawoSearchList(sawo_Interp *interp, sawo_Obj *listObjPtr, sawo_Obj *valObj)
{
    int listlen;
    int i;

    listlen = sawo_ListLength(interp, listObjPtr);
    for (i = 0; i < listlen; i++) {
        if (sawo_StringEqObj(sawo_ListGetIndex(interp, listObjPtr, i), valObj)) {
            return 1;
        }
    }
    return 0;
}

static int sawoExprOpStrBin(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *B = ExprPop(e);
    sawo_Obj *A = ExprPop(e);

    sawo_wide wC;

    switch (e->opcode) {
        case SAWO_EXPROP_STREQ:
        case SAWO_EXPROP_STRNE:
            wC = sawo_StringEqObj(A, B);
            if (e->opcode == SAWO_EXPROP_STRNE) {
                wC = !wC;
            }
            break;
        case SAWO_EXPROP_STRIN:
            wC = sawoSearchList(interp, B, A);
            break;
        case SAWO_EXPROP_STRNI:
            wC = !sawoSearchList(interp, B, A);
            break;
        default:
            abort();
    }
    ExprPush(e, sawo_NewIntObj(interp, wC));

    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, B);

    return SAWO_OK;
}

static int ExprBool(sawo_Interp *interp, sawo_Obj *obj)
{
    long l;
    double d;

    if (sawo_GetLong(interp, obj, &l) == SAWO_OK) {
        return l != 0;
    }
    if (sawo_GetDouble(interp, obj, &d) == SAWO_OK) {
        return d != 0;
    }
    return -1;
}

static int sawoExprOpAndLeft(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *skip = ExprPop(e);
    sawo_Obj *A = ExprPop(e);
    int rc = SAWO_OK;

    switch (ExprBool(interp, A)) {
        case 0:
            
            e->skip = sawoWideValue(skip);
            ExprPush(e, sawo_NewIntObj(interp, 0));
            break;

        case 1:
            
            break;

        case -1:
            
            rc = SAWO_ERR;
    }
    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, skip);

    return rc;
}

static int sawoExprOpOrLeft(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *skip = ExprPop(e);
    sawo_Obj *A = ExprPop(e);
    int rc = SAWO_OK;

    switch (ExprBool(interp, A)) {
        case 0:
            
            break;

        case 1:
            
            e->skip = sawoWideValue(skip);
            ExprPush(e, sawo_NewIntObj(interp, 1));
            break;

        case -1:
            
            rc = SAWO_ERR;
            break;
    }
    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, skip);

    return rc;
}

static int sawoExprOpAndOrRight(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *A = ExprPop(e);
    int rc = SAWO_OK;

    switch (ExprBool(interp, A)) {
        case 0:
            ExprPush(e, sawo_NewIntObj(interp, 0));
            break;

        case 1:
            ExprPush(e, sawo_NewIntObj(interp, 1));
            break;

        case -1:
            
            rc = SAWO_ERR;
            break;
    }
    sawo_DecrRefCount(interp, A);

    return rc;
}

static int sawoExprOpTernaryLeft(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *skip = ExprPop(e);
    sawo_Obj *A = ExprPop(e);
    int rc = SAWO_OK;

    
    ExprPush(e, A);

    switch (ExprBool(interp, A)) {
        case 0:
            
            e->skip = sawoWideValue(skip);
            
            ExprPush(e, sawo_NewIntObj(interp, 0));
            break;

        case 1:
            
            break;

        case -1:
            
            rc = SAWO_ERR;
            break;
    }
    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, skip);

    return rc;
}

static int sawoExprOpColonLeft(sawo_Interp *interp, struct sawoExprState *e)
{
    sawo_Obj *skip = ExprPop(e);
    sawo_Obj *B = ExprPop(e);
    sawo_Obj *A = ExprPop(e);

    
    if (ExprBool(interp, A)) {
        
        e->skip = sawoWideValue(skip);
        
        ExprPush(e, B);
    }

    sawo_DecrRefCount(interp, skip);
    sawo_DecrRefCount(interp, A);
    sawo_DecrRefCount(interp, B);
    return SAWO_OK;
}

static int sawoExprOpNull(sawo_Interp *interp, struct sawoExprState *e)
{
    return SAWO_OK;
}

enum
{
    LAZY_NONE,
    LAZY_OP,
    LAZY_LEFT,
    LAZY_RIGHT
};

#define OPRINIT(N, P, A, F) {N, F, P, A, LAZY_NONE, sizeof(N) - 1}
#define OPRINIT_LAZY(N, P, A, F, L) {N, F, P, A, L, sizeof(N) - 1}

static const struct sawo_ExprOperator sawo_ExprOperators[] = {
    OPRINIT("*", 110, 2, sawoExprOpBin),
    OPRINIT("/", 110, 2, sawoExprOpBin),
    OPRINIT("%", 110, 2, sawoExprOpIntBin),

    OPRINIT("-", 100, 2, sawoExprOpBin),
    OPRINIT("+", 100, 2, sawoExprOpBin),

    OPRINIT("<<", 90, 2, sawoExprOpIntBin),
    OPRINIT(">>", 90, 2, sawoExprOpIntBin),

    OPRINIT("<<<", 90, 2, sawoExprOpIntBin),
    OPRINIT(">>>", 90, 2, sawoExprOpIntBin),

    OPRINIT("<", 80, 2, sawoExprOpBin),
    OPRINIT(">", 80, 2, sawoExprOpBin),
    OPRINIT("<=", 80, 2, sawoExprOpBin),
    OPRINIT(">=", 80, 2, sawoExprOpBin),

    OPRINIT("==", 70, 2, sawoExprOpBin),
    OPRINIT("!=", 70, 2, sawoExprOpBin),

    OPRINIT("&", 50, 2, sawoExprOpIntBin),
    OPRINIT("^", 49, 2, sawoExprOpIntBin),
    OPRINIT("|", 48, 2, sawoExprOpIntBin),

    OPRINIT_LAZY("&&", 10, 2, NULL, LAZY_OP),
    OPRINIT_LAZY(NULL, 10, 2, sawoExprOpAndLeft, LAZY_LEFT),
    OPRINIT_LAZY(NULL, 10, 2, sawoExprOpAndOrRight, LAZY_RIGHT),

    OPRINIT_LAZY("||", 9, 2, NULL, LAZY_OP),
    OPRINIT_LAZY(NULL, 9, 2, sawoExprOpOrLeft, LAZY_LEFT),
    OPRINIT_LAZY(NULL, 9, 2, sawoExprOpAndOrRight, LAZY_RIGHT),

    OPRINIT_LAZY("?", 5, 2, sawoExprOpNull, LAZY_OP),
    OPRINIT_LAZY(NULL, 5, 2, sawoExprOpTernaryLeft, LAZY_LEFT),
    OPRINIT_LAZY(NULL, 5, 2, sawoExprOpNull, LAZY_RIGHT),

    OPRINIT_LAZY(":", 5, 2, sawoExprOpNull, LAZY_OP),
    OPRINIT_LAZY(NULL, 5, 2, sawoExprOpColonLeft, LAZY_LEFT),
    OPRINIT_LAZY(NULL, 5, 2, sawoExprOpNull, LAZY_RIGHT),

    OPRINIT("**", 250, 2, sawoExprOpBin),

    OPRINIT("eq", 60, 2, sawoExprOpStrBin),
    OPRINIT("ne", 60, 2, sawoExprOpStrBin),

    OPRINIT("in", 55, 2, sawoExprOpStrBin),
    OPRINIT("ni", 55, 2, sawoExprOpStrBin),

    OPRINIT("!", 150, 1, sawoExprOpNumUnary),
    OPRINIT("~", 150, 1, sawoExprOpIntUnary),
    OPRINIT(NULL, 150, 1, sawoExprOpNumUnary),
    OPRINIT(NULL, 150, 1, sawoExprOpNumUnary),



    OPRINIT("int", 200, 1, sawoExprOpNumUnary),
    OPRINIT("wide", 200, 1, sawoExprOpNumUnary),
    OPRINIT("abs", 200, 1, sawoExprOpNumUnary),
    OPRINIT("double", 200, 1, sawoExprOpNumUnary),
    OPRINIT("round", 200, 1, sawoExprOpNumUnary),
    OPRINIT("rand", 200, 0, sawoExprOpNone),
    OPRINIT("srand", 200, 1, sawoExprOpIntUnary),

#ifdef SAWO_MATH_FUNCTIONS
    OPRINIT("sin", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("cos", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("tan", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("asin", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("acos", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("atan", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("sinh", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("cosh", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("tanh", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("ceil", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("floor", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("exp", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("log", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("log10", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("sqrt", 200, 1, sawoExprOpDoubleUnary),
    OPRINIT("pow", 200, 2, sawoExprOpBin),
#endif
};
#undef OPRINIT
#undef OPRINIT_LAZY

#define SAWO_EXPR_OPERATORS_NUM \
    (sizeof(sawo_ExprOperators)/sizeof(struct sawo_ExprOperator))

static int sawoParseExpression(struct sawoParserCtx *pc)
{
    
    while (isspace(UCHAR(*pc->p)) || (*(pc->p) == '\\' && *(pc->p + 1) == '\n')) {
        if (*pc->p == '\n') {
            pc->linenr++;
        }
        pc->p++;
        pc->len--;
    }

    
    pc->tline = pc->linenr;
    pc->tstart = pc->p;

    if (pc->len == 0) {
        pc->tend = pc->p;
        pc->tt = SAWO_TT_EOL;
        pc->eof = 1;
        return SAWO_OK;
    }
    switch (*(pc->p)) {
        case '(':
                pc->tt = SAWO_TT_SUBEXPR_START;
                goto singlechar;
        case ')':
                pc->tt = SAWO_TT_SUBEXPR_END;
                goto singlechar;
        case ',':
            pc->tt = SAWO_TT_SUBEXPR_COMMA;
singlechar:
            pc->tend = pc->p;
            pc->p++;
            pc->len--;
            break;
        case '[':
            return sawoParseCmd(pc);
        case '$':
            if (sawoParseVar(pc) == SAWO_ERR)
                return sawoParseExprOperator(pc);
            else {
                
                if (pc->tt == SAWO_TT_EXPRSUGAR) {
                    return SAWO_ERR;
                }
                return SAWO_OK;
            }
            break;
        case '0':
        case '1':
        case '2':
        case '3':
        case '4':
        case '5':
        case '6':
        case '7':
        case '8':
        case '9':
        case '.':
            return sawoParseExprNumber(pc);
        case '"':
            return sawoParseQuote(pc);
        case '{':
            return sawoParseBrace(pc);

        case 'N':
        case 'I':
        case 'n':
        case 'i':
            if (sawoParseExprIrrational(pc) == SAWO_ERR)
                return sawoParseExprOperator(pc);
            break;
        default:
            return sawoParseExprOperator(pc);
            break;
    }
    return SAWO_OK;
}

static int sawoParseExprNumber(struct sawoParserCtx *pc)
{
    char *end;

    
    pc->tt = SAWO_TT_EXPR_INT;

    sawo_strtoull(pc->p, (char **)&pc->p);
    
    if (strchr("eENnIi.", *pc->p) || pc->p == pc->tstart) {
        if (strtod(pc->tstart, &end)) {  }
        if (end == pc->tstart)
            return SAWO_ERR;
        if (end > pc->p) {
            
            pc->tt = SAWO_TT_EXPR_DOUBLE;
            pc->p = end;
        }
    }
    pc->tend = pc->p - 1;
    pc->len -= (pc->p - pc->tstart);
    return SAWO_OK;
}

static int sawoParseExprIrrational(struct sawoParserCtx *pc)
{
    const char *irrationals[] = { "NaN", "nan", "NAN", "Inf", "inf", "INF", NULL };
    int i;

    for (i = 0; irrationals[i]; i++) {
        const char *irr = irrationals[i];

        if (strncmp(irr, pc->p, 3) == 0) {
            pc->p += 3;
            pc->len -= 3;
            pc->tend = pc->p - 1;
            pc->tt = SAWO_TT_EXPR_DOUBLE;
            return SAWO_OK;
        }
    }
    return SAWO_ERR;
}

static int sawoParseExprOperator(struct sawoParserCtx *pc)
{
    int i;
    int bestIdx = -1, bestLen = 0;

    
    for (i = 0; i < (signed)SAWO_EXPR_OPERATORS_NUM; i++) {
        const char * const opname = sawo_ExprOperators[i].name;
        const int oplen = sawo_ExprOperators[i].namelen;

        if (opname == NULL || opname[0] != pc->p[0]) {
            continue;
        }

        if (oplen > bestLen && strncmp(opname, pc->p, oplen) == 0) {
            bestIdx = i + SAWO_TT_EXPR_OP;
            bestLen = oplen;
        }
    }
    if (bestIdx == -1) {
        return SAWO_ERR;
    }

    
    if (bestIdx >= SAWO_EXPROP_FUNC_FIRST) {
        const char *p = pc->p + bestLen;
        int len = pc->len - bestLen;

        while (len && isspace(UCHAR(*p))) {
            len--;
            p++;
        }
        if (*p != '(') {
            return SAWO_ERR;
        }
    }
    pc->tend = pc->p + bestLen - 1;
    pc->p += bestLen;
    pc->len -= bestLen;

    pc->tt = bestIdx;
    return SAWO_OK;
}

static const struct sawo_ExprOperator *sawoExprOperatorInfoByOpcode(int opcode)
{
    static sawo_ExprOperator dummy_op;
    if (opcode < SAWO_TT_EXPR_OP) {
        return &dummy_op;
    }
    return &sawo_ExprOperators[opcode - SAWO_TT_EXPR_OP];
}

const char *sawo_tt_name(int type)
{
    static const char * const tt_names[SAWO_TT_EXPR_OP] =
        { "NIL", "STR", "ESC", "VAR", "ARY", "CMD", "SEP", "EOL", "EOF", "LIN", "WRD", "(((", ")))", ",,,", "INT",
            "DBL", "$()" };
    if (type < SAWO_TT_EXPR_OP) {
        return tt_names[type];
    }
    else {
        const struct sawo_ExprOperator *op = sawoExprOperatorInfoByOpcode(type);
        static char buf[20];

        if (op->name) {
            return op->name;
        }
        sprintf(buf, "(%d)", type);
        return buf;
    }
}

static void FreeExprInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupExprInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);
static int SetExprFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr);

static const sawo_ObjType exprObjType = {
    "expression",
    FreeExprInternalRep,
    DupExprInternalRep,
    NULL,
    SAWO_TYPE_REFERENCES,
};


typedef struct ExprByteCode
{
    ScriptToken *token;         
    int len;                    
    int inUse;                  
} ExprByteCode;

static void ExprFreeByteCode(sawo_Interp *interp, ExprByteCode * expr)
{
    int i;

    for (i = 0; i < expr->len; i++) {
        sawo_DecrRefCount(interp, expr->token[i].objPtr);
    }
    sawo_Free(expr->token);
    sawo_Free(expr);
}

static void FreeExprInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    ExprByteCode *expr = (void *)objPtr->internalRep.ptr;

    if (expr) {
        if (--expr->inUse != 0) {
            return;
        }

        ExprFreeByteCode(interp, expr);
    }
}

static void DupExprInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    SAWO_NOTUSED(interp);
    SAWO_NOTUSED(srcPtr);

    
    dupPtr->typePtr = NULL;
}


static int ExprCheckCorrectness(ExprByteCode * expr)
{
    int i;
    int stacklen = 0;
    int ternary = 0;

    for (i = 0; i < expr->len; i++) {
        ScriptToken *t = &expr->token[i];
        const struct sawo_ExprOperator *op = sawoExprOperatorInfoByOpcode(t->type);

        stacklen -= op->arity;
        if (stacklen < 0) {
            break;
        }
        if (t->type == SAWO_EXPROP_TERNARY || t->type == SAWO_EXPROP_TERNARY_LEFT) {
            ternary++;
        }
        else if (t->type == SAWO_EXPROP_COLON || t->type == SAWO_EXPROP_COLON_LEFT) {
            ternary--;
        }

        
        stacklen++;
    }
    if (stacklen != 1 || ternary != 0) {
        return SAWO_ERR;
    }
    return SAWO_OK;
}

static int ExprAddLazyOperator(sawo_Interp *interp, ExprByteCode * expr, ParseToken *t)
{
    int i;

    int leftindex, arity, offset;

    
    leftindex = expr->len - 1;

    arity = 1;
    while (arity) {
        ScriptToken *tt = &expr->token[leftindex];

        if (tt->type >= SAWO_TT_EXPR_OP) {
            arity += sawoExprOperatorInfoByOpcode(tt->type)->arity;
        }
        arity--;
        if (--leftindex < 0) {
            return SAWO_ERR;
        }
    }
    leftindex++;

    
    memmove(&expr->token[leftindex + 2], &expr->token[leftindex],
        sizeof(*expr->token) * (expr->len - leftindex));
    expr->len += 2;
    offset = (expr->len - leftindex) - 1;

    expr->token[leftindex + 1].type = t->type + 1;
    expr->token[leftindex + 1].objPtr = interp->emptyObj;

    expr->token[leftindex].type = SAWO_TT_EXPR_INT;
    expr->token[leftindex].objPtr = sawo_NewIntObj(interp, offset);

    
    expr->token[expr->len].objPtr = interp->emptyObj;
    expr->token[expr->len].type = t->type + 2;
    expr->len++;

    
    for (i = leftindex - 1; i > 0; i--) {
        const struct sawo_ExprOperator *op = sawoExprOperatorInfoByOpcode(expr->token[i].type);
        if (op->lazy == LAZY_LEFT) {
            if (sawoWideValue(expr->token[i - 1].objPtr) + i - 1 >= leftindex) {
                sawoWideValue(expr->token[i - 1].objPtr) += 2;
            }
        }
    }
    return SAWO_OK;
}

static int ExprAddOperator(sawo_Interp *interp, ExprByteCode * expr, ParseToken *t)
{
    struct ScriptToken *token = &expr->token[expr->len];
    const struct sawo_ExprOperator *op = sawoExprOperatorInfoByOpcode(t->type);

    if (op->lazy == LAZY_OP) {
        if (ExprAddLazyOperator(interp, expr, t) != SAWO_OK) {
            sawo_SetResultFormatted(interp, "Expression has bad operands to %s", op->name);
            return SAWO_ERR;
        }
    }
    else {
        token->objPtr = interp->emptyObj;
        token->type = t->type;
        expr->len++;
    }
    return SAWO_OK;
}

static int ExprTernaryGetColonLeftIndex(ExprByteCode *expr, int right_index)
{
    int ternary_count = 1;

    right_index--;

    while (right_index > 1) {
        if (expr->token[right_index].type == SAWO_EXPROP_TERNARY_LEFT) {
            ternary_count--;
        }
        else if (expr->token[right_index].type == SAWO_EXPROP_COLON_RIGHT) {
            ternary_count++;
        }
        else if (expr->token[right_index].type == SAWO_EXPROP_COLON_LEFT && ternary_count == 1) {
            return right_index;
        }
        right_index--;
    }

    
    return -1;
}

static int ExprTernaryGetMoveIndices(ExprByteCode *expr, int right_index, int *prev_right_index, int *prev_left_index)
{
    int i = right_index - 1;
    int ternary_count = 1;

    while (i > 1) {
        if (expr->token[i].type == SAWO_EXPROP_TERNARY_LEFT) {
            if (--ternary_count == 0 && expr->token[i - 2].type == SAWO_EXPROP_COLON_RIGHT) {
                *prev_right_index = i - 2;
                *prev_left_index = ExprTernaryGetColonLeftIndex(expr, *prev_right_index);
                return 1;
            }
        }
        else if (expr->token[i].type == SAWO_EXPROP_COLON_RIGHT) {
            if (ternary_count == 0) {
                return 0;
            }
            ternary_count++;
        }
        i--;
    }
    return 0;
}

static void ExprTernaryReorderExpression(sawo_Interp *interp, ExprByteCode *expr)
{
    int i;

    for (i = expr->len - 1; i > 1; i--) {
        int prev_right_index;
        int prev_left_index;
        int j;
        ScriptToken tmp;

        if (expr->token[i].type != SAWO_EXPROP_COLON_RIGHT) {
            continue;
        }

        
        if (ExprTernaryGetMoveIndices(expr, i, &prev_right_index, &prev_left_index) == 0) {
            continue;
        }

        tmp = expr->token[prev_right_index];
        for (j = prev_right_index; j < i; j++) {
            expr->token[j] = expr->token[j + 1];
        }
        expr->token[i] = tmp;

        sawoWideValue(expr->token[prev_left_index-1].objPtr) += (i - prev_right_index);

        
        i++;
    }
}

static ExprByteCode *ExprCreateByteCode(sawo_Interp *interp, const ParseTokenList *tokenlist, sawo_Obj *fileNameObj)
{
    sawo_Stack stack;
    ExprByteCode *expr;
    int ok = 1;
    int i;
    int prevtt = SAWO_TT_NONE;
    int have_ternary = 0;

    
    int count = tokenlist->count - 1;

    expr = sawo_Alloc(sizeof(*expr));
    expr->inUse = 1;
    expr->len = 0;

    sawo_InitStack(&stack);

    for (i = 0; i < tokenlist->count; i++) {
        ParseToken *t = &tokenlist->list[i];
        const struct sawo_ExprOperator *op = sawoExprOperatorInfoByOpcode(t->type);

        if (op->lazy == LAZY_OP) {
            count += 2;
            
            if (t->type == SAWO_EXPROP_TERNARY) {
                have_ternary = 1;
            }
        }
    }

    expr->token = sawo_Alloc(sizeof(ScriptToken) * count);

    for (i = 0; i < tokenlist->count && ok; i++) {
        ParseToken *t = &tokenlist->list[i];

        
        struct ScriptToken *token = &expr->token[expr->len];

        if (t->type == SAWO_TT_EOL) {
            break;
        }

        switch (t->type) {
            case SAWO_TT_STR:
            case SAWO_TT_ESC:
            case SAWO_TT_VAR:
            case SAWO_TT_DICTSUGAR:
            case SAWO_TT_EXPRSUGAR:
            case SAWO_TT_CMD:
                token->type = t->type;
strexpr:
                token->objPtr = sawo_NewStringObj(interp, t->token, t->len);
                if (t->type == SAWO_TT_CMD) {
                    
                    sawoSetSourceInfo(interp, token->objPtr, fileNameObj, t->line);
                }
                expr->len++;
                break;

            case SAWO_TT_EXPR_INT:
            case SAWO_TT_EXPR_DOUBLE:
                {
                    char *endptr;
                    if (t->type == SAWO_TT_EXPR_INT) {
                        token->objPtr = sawo_NewIntObj(interp, sawo_strtoull(t->token, &endptr));
                    }
                    else {
                        token->objPtr = sawo_NewDoubleObj(interp, strtod(t->token, &endptr));
                    }
                    if (endptr != t->token + t->len) {
                        
                        sawo_FreeNewObj(interp, token->objPtr);
                        token->type = SAWO_TT_STR;
                        goto strexpr;
                    }
                    token->type = t->type;
                    expr->len++;
                }
                break;

            case SAWO_TT_SUBEXPR_START:
                sawo_StackPush(&stack, t);
                prevtt = SAWO_TT_NONE;
                continue;

            case SAWO_TT_SUBEXPR_COMMA:
                
                continue;

            case SAWO_TT_SUBEXPR_END:
                ok = 0;
                while (sawo_StackLen(&stack)) {
                    ParseToken *tt = sawo_StackPop(&stack);

                    if (tt->type == SAWO_TT_SUBEXPR_START) {
                        ok = 1;
                        break;
                    }

                    if (ExprAddOperator(interp, expr, tt) != SAWO_OK) {
                        goto err;
                    }
                }
                if (!ok) {
                    sawo_SetResultString(interp, "Unexpected close parenthesis", -1);
                    goto err;
                }
                break;


            default:{
                    
                    const struct sawo_ExprOperator *op;
                    ParseToken *tt;

                    
                    if (prevtt == SAWO_TT_NONE || prevtt >= SAWO_TT_EXPR_OP) {
                        if (t->type == SAWO_EXPROP_SUB) {
                            t->type = SAWO_EXPROP_UNARYMINUS;
                        }
                        else if (t->type == SAWO_EXPROP_ADD) {
                            t->type = SAWO_EXPROP_UNARYPLUS;
                        }
                    }

                    op = sawoExprOperatorInfoByOpcode(t->type);

                    
                    while ((tt = sawo_StackPeek(&stack)) != NULL) {
                        const struct sawo_ExprOperator *tt_op =
                            sawoExprOperatorInfoByOpcode(tt->type);

                        

                        if (op->arity != 1 && tt_op->precedence >= op->precedence) {
                            if (ExprAddOperator(interp, expr, tt) != SAWO_OK) {
                                ok = 0;
                                goto err;
                            }
                            sawo_StackPop(&stack);
                        }
                        else {
                            break;
                        }
                    }
                    sawo_StackPush(&stack, t);
                    break;
                }
        }
        prevtt = t->type;
    }

    
    while (sawo_StackLen(&stack)) {
        ParseToken *tt = sawo_StackPop(&stack);

        if (tt->type == SAWO_TT_SUBEXPR_START) {
            ok = 0;
            sawo_SetResultString(interp, "Missing close parenthesis", -1);
            goto err;
        }
        if (ExprAddOperator(interp, expr, tt) != SAWO_OK) {
            ok = 0;
            goto err;
        }
    }

    if (have_ternary) {
        ExprTernaryReorderExpression(interp, expr);
    }

  err:
    
    sawo_FreeStack(&stack);

    for (i = 0; i < expr->len; i++) {
        sawo_IncrRefCount(expr->token[i].objPtr);
    }

    if (!ok) {
        ExprFreeByteCode(interp, expr);
        return NULL;
    }

    return expr;
}


static int SetExprFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr)
{
    int exprTextLen;
    const char *exprText;
    struct sawoParserCtx parser;
    struct ExprByteCode *expr;
    ParseTokenList tokenlist;
    int line;
    sawo_Obj *fileNameObj;
    int rc = SAWO_ERR;

    
    if (objPtr->typePtr == &sourceObjType) {
        fileNameObj = objPtr->internalRep.sourceValue.fileNameObj;
        line = objPtr->internalRep.sourceValue.lineNumber;
    }
    else {
        fileNameObj = interp->emptyObj;
        line = 1;
    }
    sawo_IncrRefCount(fileNameObj);

    exprText = sawo_GetString(objPtr, &exprTextLen);

    
    ScriptTokenListInit(&tokenlist);

    sawoParserInit(&parser, exprText, exprTextLen, line);
    while (!parser.eof) {
        if (sawoParseExpression(&parser) != SAWO_OK) {
            ScriptTokenListFree(&tokenlist);
          invalidexpr:
            sawo_SetResultFormatted(interp, "syntax error in expression: \"%#s\"", objPtr);
            expr = NULL;
            goto err;
        }

        ScriptAddToken(&tokenlist, parser.tstart, parser.tend - parser.tstart + 1, parser.tt,
            parser.tline);
    }

#ifdef DEBUG_SHOW_EXPR_TOKENS
    {
        int i;
        printf("==== Expr Tokens (%s) ====\n", sawo_String(fileNameObj));
        for (i = 0; i < tokenlist.count; i++) {
            printf("[%2d]@%d %s '%.*s'\n", i, tokenlist.list[i].line, sawo_tt_name(tokenlist.list[i].type),
                tokenlist.list[i].len, tokenlist.list[i].token);
        }
    }
#endif

    if (sawoParseCheckMissing(interp, parser.missing.ch) == SAWO_ERR) {
        ScriptTokenListFree(&tokenlist);
        sawo_DecrRefCount(interp, fileNameObj);
        return SAWO_ERR;
    }

    
    expr = ExprCreateByteCode(interp, &tokenlist, fileNameObj);

    
    ScriptTokenListFree(&tokenlist);

    if (!expr) {
        goto err;
    }

#ifdef DEBUG_SHOW_EXPR
    {
        int i;

        printf("==== Expr ====\n");
        for (i = 0; i < expr->len; i++) {
            ScriptToken *t = &expr->token[i];

            printf("[%2d] %s '%s'\n", i, sawo_tt_name(t->type), sawo_String(t->objPtr));
        }
    }
#endif

    
    if (ExprCheckCorrectness(expr) != SAWO_OK) {
        ExprFreeByteCode(interp, expr);
        goto invalidexpr;
    }

    rc = SAWO_OK;

  err:
    
    sawo_DecrRefCount(interp, fileNameObj);
    sawo_FreeIntRep(interp, objPtr);
    sawo_SetIntRepPtr(objPtr, expr);
    objPtr->typePtr = &exprObjType;
    return rc;
}

static ExprByteCode *sawoGetExpression(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (objPtr->typePtr != &exprObjType) {
        if (SetExprFromAny(interp, objPtr) != SAWO_OK) {
            return NULL;
        }
    }
    return (ExprByteCode *) sawo_GetIntRepPtr(objPtr);
}

#ifdef SAWO_OPTIMIZATION
static sawo_Obj *sawoExprIntValOrVar(sawo_Interp *interp, const ScriptToken *token)
{
    if (token->type == SAWO_TT_EXPR_INT)
        return token->objPtr;
    else if (token->type == SAWO_TT_VAR)
        return sawo_GetVariable(interp, token->objPtr, SAWO_NONE);
    else if (token->type == SAWO_TT_DICTSUGAR)
        return sawoExpandDictSugar(interp, token->objPtr);
    else
        return NULL;
}
#endif

#define SAWO_EE_STATICSTACK_LEN 10

int sawo_EvalExpression(sawo_Interp *interp, sawo_Obj *exprObjPtr, sawo_Obj **exprResultPtrPtr)
{
    ExprByteCode *expr;
    sawo_Obj *staticStack[SAWO_EE_STATICSTACK_LEN];
    int i;
    int retcode = SAWO_OK;
    struct sawoExprState e;

    expr = sawoGetExpression(interp, exprObjPtr);
    if (!expr) {
        return SAWO_ERR;         
    }

#ifdef SAWO_OPTIMIZATION
    {
        sawo_Obj *objPtr;


        switch (expr->len) {
            case 1:
                objPtr = sawoExprIntValOrVar(interp, &expr->token[0]);
                if (objPtr) {
                    sawo_IncrRefCount(objPtr);
                    *exprResultPtrPtr = objPtr;
                    return SAWO_OK;
                }
                break;

            case 2:
                if (expr->token[1].type == SAWO_EXPROP_NOT) {
                    objPtr = sawoExprIntValOrVar(interp, &expr->token[0]);

                    if (objPtr && sawoIsWide(objPtr)) {
                        *exprResultPtrPtr = sawoWideValue(objPtr) ? interp->falseObj : interp->trueObj;
                        sawo_IncrRefCount(*exprResultPtrPtr);
                        return SAWO_OK;
                    }
                }
                break;

            case 3:
                objPtr = sawoExprIntValOrVar(interp, &expr->token[0]);
                if (objPtr && sawoIsWide(objPtr)) {
                    sawo_Obj *objPtr2 = sawoExprIntValOrVar(interp, &expr->token[1]);
                    if (objPtr2 && sawoIsWide(objPtr2)) {
                        sawo_wide wideValueA = sawoWideValue(objPtr);
                        sawo_wide wideValueB = sawoWideValue(objPtr2);
                        int cmpRes;
                        switch (expr->token[2].type) {
                            case SAWO_EXPROP_LT:
                                cmpRes = wideValueA < wideValueB;
                                break;
                            case SAWO_EXPROP_LTE:
                                cmpRes = wideValueA <= wideValueB;
                                break;
                            case SAWO_EXPROP_GT:
                                cmpRes = wideValueA > wideValueB;
                                break;
                            case SAWO_EXPROP_GTE:
                                cmpRes = wideValueA >= wideValueB;
                                break;
                            case SAWO_EXPROP_NUMEQ:
                                cmpRes = wideValueA == wideValueB;
                                break;
                            case SAWO_EXPROP_NUMNE:
                                cmpRes = wideValueA != wideValueB;
                                break;
                            default:
                                goto noopt;
                        }
                        *exprResultPtrPtr = cmpRes ? interp->trueObj : interp->falseObj;
                        sawo_IncrRefCount(*exprResultPtrPtr);
                        return SAWO_OK;
                    }
                }
                break;
        }
    }
noopt:
#endif

    expr->inUse++;

    

    if (expr->len > SAWO_EE_STATICSTACK_LEN)
        e.stack = sawo_Alloc(sizeof(sawo_Obj *) * expr->len);
    else
        e.stack = staticStack;

    e.stacklen = 0;

    
    for (i = 0; i < expr->len && retcode == SAWO_OK; i++) {
        sawo_Obj *objPtr;

        switch (expr->token[i].type) {
            case SAWO_TT_EXPR_INT:
            case SAWO_TT_EXPR_DOUBLE:
            case SAWO_TT_STR:
                ExprPush(&e, expr->token[i].objPtr);
                break;

            case SAWO_TT_VAR:
                objPtr = sawo_GetVariable(interp, expr->token[i].objPtr, SAWO_ERRMSG);
                if (objPtr) {
                    ExprPush(&e, objPtr);
                }
                else {
                    retcode = SAWO_ERR;
                }
                break;

            case SAWO_TT_DICTSUGAR:
                objPtr = sawoExpandDictSugar(interp, expr->token[i].objPtr);
                if (objPtr) {
                    ExprPush(&e, objPtr);
                }
                else {
                    retcode = SAWO_ERR;
                }
                break;

            case SAWO_TT_ESC:
                retcode = sawo_SubstObj(interp, expr->token[i].objPtr, &objPtr, SAWO_NONE);
                if (retcode == SAWO_OK) {
                    ExprPush(&e, objPtr);
                }
                break;

            case SAWO_TT_CMD:
                retcode = sawo_EvalObj(interp, expr->token[i].objPtr);
                if (retcode == SAWO_OK) {
                    ExprPush(&e, sawo_GetResult(interp));
                }
                break;

            default:{
                    
                    e.skip = 0;
                    e.opcode = expr->token[i].type;

                    retcode = sawoExprOperatorInfoByOpcode(e.opcode)->funcop(interp, &e);
                    
                    i += e.skip;
                    continue;
                }
        }
    }

    expr->inUse--;

    if (retcode == SAWO_OK) {
        *exprResultPtrPtr = ExprPop(&e);
    }
    else {
        for (i = 0; i < e.stacklen; i++) {
            sawo_DecrRefCount(interp, e.stack[i]);
        }
    }
    if (e.stack != staticStack) {
        sawo_Free(e.stack);
    }
    return retcode;
}

int sawo_GetBoolFromExpr(sawo_Interp *interp, sawo_Obj *exprObjPtr, int *boolPtr)
{
    int retcode;
    sawo_wide wideValue;
    double doubleValue;
    sawo_Obj *exprResultPtr;

    retcode = sawo_EvalExpression(interp, exprObjPtr, &exprResultPtr);
    if (retcode != SAWO_OK)
        return retcode;

    if (sawoGetWideNoErr(interp, exprResultPtr, &wideValue) != SAWO_OK) {
        if (sawo_GetDouble(interp, exprResultPtr, &doubleValue) != SAWO_OK) {
            sawo_DecrRefCount(interp, exprResultPtr);
            return SAWO_ERR;
        }
        else {
            sawo_DecrRefCount(interp, exprResultPtr);
            *boolPtr = doubleValue != 0;
            return SAWO_OK;
        }
    }
    *boolPtr = wideValue != 0;

    sawo_DecrRefCount(interp, exprResultPtr);
    return SAWO_OK;
}




typedef struct ScanFmtPartDescr
{
    char *arg;                  
    char *prefix;               
    size_t width;               
    int pos;                    
    char type;                  
    char modifier;              
} ScanFmtPartDescr;


typedef struct ScanFmtStringObj
{
    sawo_wide size;              
    char *stringRep;            
    size_t count;               
    size_t convCount;           
    size_t maxPos;              
    const char *error;          
    char *scratch;              
    ScanFmtPartDescr descr[1];  
} ScanFmtStringObj;


static void FreeScanFmtInternalRep(sawo_Interp *interp, sawo_Obj *objPtr);
static void DupScanFmtInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr);
static void UpdateStringOfScanFmt(sawo_Obj *objPtr);

static const sawo_ObjType scanFmtStringObjType = {
    "scanformatstring",
    FreeScanFmtInternalRep,
    DupScanFmtInternalRep,
    UpdateStringOfScanFmt,
    SAWO_TYPE_NONE,
};

void FreeScanFmtInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    SAWO_NOTUSED(interp);
    sawo_Free((char *)objPtr->internalRep.ptr);
    objPtr->internalRep.ptr = 0;
}

void DupScanFmtInternalRep(sawo_Interp *interp, sawo_Obj *srcPtr, sawo_Obj *dupPtr)
{
    size_t size = (size_t) ((ScanFmtStringObj *) srcPtr->internalRep.ptr)->size;
    ScanFmtStringObj *newVec = (ScanFmtStringObj *) sawo_Alloc(size);

    SAWO_NOTUSED(interp);
    memcpy(newVec, srcPtr->internalRep.ptr, size);
    dupPtr->internalRep.ptr = newVec;
    dupPtr->typePtr = &scanFmtStringObjType;
}

static void UpdateStringOfScanFmt(sawo_Obj *objPtr)
{
    sawoSetStringBytes(objPtr, ((ScanFmtStringObj *) objPtr->internalRep.ptr)->stringRep);
}


static int SetScanFmtFromAny(sawo_Interp *interp, sawo_Obj *objPtr)
{
    ScanFmtStringObj *fmtObj;
    char *buffer;
    int maxCount, i, approxSize, lastPos = -1;
    const char *fmt = objPtr->bytes;
    int maxFmtLen = objPtr->length;
    const char *fmtEnd = fmt + maxFmtLen;
    int curr;

    sawo_FreeIntRep(interp, objPtr);
    
    for (i = 0, maxCount = 0; i < maxFmtLen; ++i)
        if (fmt[i] == '%')
            ++maxCount;
    
    approxSize = sizeof(ScanFmtStringObj)       
        +(maxCount + 1) * sizeof(ScanFmtPartDescr)      
        +maxFmtLen * sizeof(char) + 3 + 1       
        + maxFmtLen * sizeof(char) + 1  
        + maxFmtLen * sizeof(char)      
        +(maxCount + 1) * sizeof(char)  
        +1;                     
    fmtObj = (ScanFmtStringObj *) sawo_Alloc(approxSize);
    memset(fmtObj, 0, approxSize);
    fmtObj->size = approxSize;
    fmtObj->maxPos = 0;
    fmtObj->scratch = (char *)&fmtObj->descr[maxCount + 1];
    fmtObj->stringRep = fmtObj->scratch + maxFmtLen + 3 + 1;
    memcpy(fmtObj->stringRep, fmt, maxFmtLen);
    buffer = fmtObj->stringRep + maxFmtLen + 1;
    objPtr->internalRep.ptr = fmtObj;
    objPtr->typePtr = &scanFmtStringObjType;
    for (i = 0, curr = 0; fmt < fmtEnd; ++fmt) {
        int width = 0, skip;
        ScanFmtPartDescr *descr = &fmtObj->descr[curr];

        fmtObj->count++;
        descr->width = 0;       
        
        if (*fmt != '%' || fmt[1] == '%') {
            descr->type = 0;
            descr->prefix = &buffer[i];
            for (; fmt < fmtEnd; ++fmt) {
                if (*fmt == '%') {
                    if (fmt[1] != '%')
                        break;
                    ++fmt;
                }
                buffer[i++] = *fmt;
            }
            buffer[i++] = 0;
        }
        
        ++fmt;
        
        if (fmt >= fmtEnd)
            goto done;
        descr->pos = 0;         
        if (*fmt == '*') {
            descr->pos = -1;    
            ++fmt;
        }
        else
            fmtObj->convCount++;        
        
        if (sscanf(fmt, "%d%n", &width, &skip) == 1) {
            fmt += skip;
            
            if (descr->pos != -1 && *fmt == '$') {
                int prev;

                ++fmt;
                descr->pos = width;
                width = 0;
                
                if ((lastPos == 0 && descr->pos > 0)
                    || (lastPos > 0 && descr->pos == 0)) {
                    fmtObj->error = "cannot mix \"%\" and \"%n$\" conversion specifiers";
                    return SAWO_ERR;
                }
                
                for (prev = 0; prev < curr; ++prev) {
                    if (fmtObj->descr[prev].pos == -1)
                        continue;
                    if (fmtObj->descr[prev].pos == descr->pos) {
                        fmtObj->error =
                            "variable is assigned by multiple \"%n$\" conversion specifiers";
                        return SAWO_ERR;
                    }
                }
                
                if (sscanf(fmt, "%d%n", &width, &skip) == 1) {
                    descr->width = width;
                    fmt += skip;
                }
                if (descr->pos > 0 && (size_t) descr->pos > fmtObj->maxPos)
                    fmtObj->maxPos = descr->pos;
            }
            else {
                
                descr->width = width;
            }
        }
        
        if (lastPos == -1)
            lastPos = descr->pos;
        
        if (*fmt == '[') {
            int swapped = 1, beg = i, end, j;

            descr->type = '[';
            descr->arg = &buffer[i];
            ++fmt;
            if (*fmt == '^')
                buffer[i++] = *fmt++;
            if (*fmt == ']')
                buffer[i++] = *fmt++;
            while (*fmt && *fmt != ']')
                buffer[i++] = *fmt++;
            if (*fmt != ']') {
                fmtObj->error = "unmatched [ in format string";
                return SAWO_ERR;
            }
            end = i;
            buffer[i++] = 0;
            
            while (swapped) {
                swapped = 0;
                for (j = beg + 1; j < end - 1; ++j) {
                    if (buffer[j] == '-' && buffer[j - 1] > buffer[j + 1]) {
                        char tmp = buffer[j - 1];

                        buffer[j - 1] = buffer[j + 1];
                        buffer[j + 1] = tmp;
                        swapped = 1;
                    }
                }
            }
        }
        else {
            
            if (strchr("hlL", *fmt) != 0)
                descr->modifier = tolower((int)*fmt++);

            descr->type = *fmt;
            if (strchr("efgcsndoxui", *fmt) == 0) {
                fmtObj->error = "bad scan conversion character";
                return SAWO_ERR;
            }
            else if (*fmt == 'c' && descr->width != 0) {
                fmtObj->error = "field width may not be specified in %c " "conversion";
                return SAWO_ERR;
            }
            else if (*fmt == 'u' && descr->modifier == 'l') {
                fmtObj->error = "unsigned wide not supported";
                return SAWO_ERR;
            }
        }
        curr++;
    }
  done:
    return SAWO_OK;
}



#define FormatGetCnvCount(_fo_) \
    ((ScanFmtStringObj*)((_fo_)->internalRep.ptr))->convCount
#define FormatGetMaxPos(_fo_) \
    ((ScanFmtStringObj*)((_fo_)->internalRep.ptr))->maxPos
#define FormatGetError(_fo_) \
    ((ScanFmtStringObj*)((_fo_)->internalRep.ptr))->error

static sawo_Obj *sawoScanAString(sawo_Interp *interp, const char *sdescr, const char *str)
{
    char *buffer = sawo_StrDup(str);
    char *p = buffer;

    while (*str) {
        int c;
        int n;

        if (!sdescr && isspace(UCHAR(*str)))
            break;              

        n = utf8_tounicode(str, &c);
        if (sdescr && !sawoCharsetMatch(sdescr, c, SAWO_CHARSET_SCAN))
            break;
        while (n--)
            *p++ = *str++;
    }
    *p = 0;
    return sawo_NewStringObjNoAlloc(interp, buffer, p - buffer);
}


static int ScanOneEntry(sawo_Interp *interp, const char *str, int pos, int strLen,
    ScanFmtStringObj * fmtObj, long idx, sawo_Obj **valObjPtr)
{
    const char *tok;
    const ScanFmtPartDescr *descr = &fmtObj->descr[idx];
    size_t scanned = 0;
    size_t anchor = pos;
    int i;
    sawo_Obj *tmpObj = NULL;

    
    *valObjPtr = 0;
    if (descr->prefix) {
        for (i = 0; pos < strLen && descr->prefix[i]; ++i) {
            
            if (isspace(UCHAR(descr->prefix[i])))
                while (pos < strLen && isspace(UCHAR(str[pos])))
                    ++pos;
            else if (descr->prefix[i] != str[pos])
                break;          
            else
                ++pos;          
        }
        if (pos >= strLen) {
            return -1;          
        }
        else if (descr->prefix[i] != 0)
            return 0;           
    }
    
    if (descr->type != 'c' && descr->type != '[' && descr->type != 'n')
        while (isspace(UCHAR(str[pos])))
            ++pos;
    
    scanned = pos - anchor;

    
    if (descr->type == 'n') {
        
        *valObjPtr = sawo_NewIntObj(interp, anchor + scanned);
    }
    else if (pos >= strLen) {
        
        return -1;
    }
    else if (descr->type == 'c') {
            int c;
            scanned += utf8_tounicode(&str[pos], &c);
            *valObjPtr = sawo_NewIntObj(interp, c);
            return scanned;
    }
    else {
        
        if (descr->width > 0) {
            size_t sLen = utf8_strlen(&str[pos], strLen - pos);
            size_t tLen = descr->width > sLen ? sLen : descr->width;

            tmpObj = sawo_NewStringObjUtf8(interp, str + pos, tLen);
            tok = tmpObj->bytes;
        }
        else {
            
            tok = &str[pos];
        }
        switch (descr->type) {
            case 'd':
            case 'o':
            case 'x':
            case 'u':
            case 'i':{
                    char *endp; 
                    sawo_wide w;

                    int base = descr->type == 'o' ? 8
                        : descr->type == 'x' ? 16 : descr->type == 'i' ? 0 : 10;

                    
                    if (base == 0) {
                        w = sawo_strtoull(tok, &endp);
                    }
                    else {
                        w = strtoull(tok, &endp, base);
                    }

                    if (endp != tok) {
                        
                        *valObjPtr = sawo_NewIntObj(interp, w);

                        
                        scanned += endp - tok;
                    }
                    else {
                        scanned = *tok ? 0 : -1;
                    }
                    break;
                }
            case 's':
            case '[':{
                    *valObjPtr = sawoScanAString(interp, descr->arg, tok);
                    scanned += sawo_Length(*valObjPtr);
                    break;
                }
            case 'e':
            case 'f':
            case 'g':{
                    char *endp;
                    double value = strtod(tok, &endp);

                    if (endp != tok) {
                        
                        *valObjPtr = sawo_NewDoubleObj(interp, value);
                        
                        scanned += endp - tok;
                    }
                    else {
                        scanned = *tok ? 0 : -1;
                    }
                    break;
                }
        }
        if (tmpObj) {
            sawo_FreeNewObj(interp, tmpObj);
        }
    }
    return scanned;
}


sawo_Obj *sawo_ScanString(sawo_Interp *interp, sawo_Obj *strObjPtr, sawo_Obj *fmtObjPtr, int flags)
{
    size_t i, pos;
    int scanned = 1;
    const char *str = sawo_String(strObjPtr);
    int strLen = sawo_Utf8Length(interp, strObjPtr);
    sawo_Obj *resultList = 0;
    sawo_Obj **resultVec = 0;
    int resultc;
    sawo_Obj *emptyStr = 0;
    ScanFmtStringObj *fmtObj;

    
    sawoPanic((fmtObjPtr->typePtr != &scanFmtStringObjType, "sawo_ScanString() for non-scan format"));

    fmtObj = (ScanFmtStringObj *) fmtObjPtr->internalRep.ptr;
    
    if (fmtObj->error != 0) {
        if (flags & SAWO_ERRMSG)
            sawo_SetResultString(interp, fmtObj->error, -1);
        return 0;
    }
    
    emptyStr = sawo_NewEmptyStringObj(interp);
    sawo_IncrRefCount(emptyStr);
    
    resultList = sawo_NewListObj(interp, NULL, 0);
    if (fmtObj->maxPos > 0) {
        for (i = 0; i < fmtObj->maxPos; ++i)
            sawo_ListAppendElement(interp, resultList, emptyStr);
        sawoListGetElements(interp, resultList, &resultc, &resultVec);
    }
    
    for (i = 0, pos = 0; i < fmtObj->count; ++i) {
        ScanFmtPartDescr *descr = &(fmtObj->descr[i]);
        sawo_Obj *value = 0;

        
        if (descr->type == 0)
            continue;
        
        if (scanned > 0)
            scanned = ScanOneEntry(interp, str, pos, strLen, fmtObj, i, &value);
        
        if (scanned == -1 && i == 0)
            goto eof;
        
        pos += scanned;

        
        if (value == 0)
            value = sawo_NewEmptyStringObj(interp);
        
        if (descr->pos == -1) {
            sawo_FreeNewObj(interp, value);
        }
        else if (descr->pos == 0)
            
            sawo_ListAppendElement(interp, resultList, value);
        else if (resultVec[descr->pos - 1] == emptyStr) {
            
            sawo_DecrRefCount(interp, resultVec[descr->pos - 1]);
            sawo_IncrRefCount(value);
            resultVec[descr->pos - 1] = value;
        }
        else {
            
            sawo_FreeNewObj(interp, value);
            goto err;
        }
    }
    sawo_DecrRefCount(interp, emptyStr);
    return resultList;
  eof:
    sawo_DecrRefCount(interp, emptyStr);
    sawo_FreeNewObj(interp, resultList);
    return (sawo_Obj *)EOF;
  err:
    sawo_DecrRefCount(interp, emptyStr);
    sawo_FreeNewObj(interp, resultList);
    return 0;
}


static void sawoPrngInit(sawo_Interp *interp)
{
#define PRNG_SEED_SIZE 256
    int i;
    unsigned int *seed;
    time_t t = time(NULL);

    interp->prngState = sawo_Alloc(sizeof(sawo_PrngState));

    seed = sawo_Alloc(PRNG_SEED_SIZE * sizeof(*seed));
    for (i = 0; i < PRNG_SEED_SIZE; i++) {
        seed[i] = (rand() ^ t ^ clock());
    }
    sawoPrngSeed(interp, (unsigned char *)seed, PRNG_SEED_SIZE * sizeof(*seed));
    sawo_Free(seed);
}


static void sawoRandomBytes(sawo_Interp *interp, void *dest, unsigned int len)
{
    sawo_PrngState *prng;
    unsigned char *destByte = (unsigned char *)dest;
    unsigned int si, sj, x;

    
    if (interp->prngState == NULL)
        sawoPrngInit(interp);
    prng = interp->prngState;
    
    for (x = 0; x < len; x++) {
        prng->i = (prng->i + 1) & 0xff;
        si = prng->sbox[prng->i];
        prng->j = (prng->j + si) & 0xff;
        sj = prng->sbox[prng->j];
        prng->sbox[prng->i] = sj;
        prng->sbox[prng->j] = si;
        *destByte++ = prng->sbox[(si + sj) & 0xff];
    }
}


static void sawoPrngSeed(sawo_Interp *interp, unsigned char *seed, int seedLen)
{
    int i;
    sawo_PrngState *prng;

    
    if (interp->prngState == NULL)
        sawoPrngInit(interp);
    prng = interp->prngState;

    
    for (i = 0; i < 256; i++)
        prng->sbox[i] = i;
    
    for (i = 0; i < seedLen; i++) {
        unsigned char t;

        t = prng->sbox[i & 0xFF];
        prng->sbox[i & 0xFF] = prng->sbox[seed[i]];
        prng->sbox[seed[i]] = t;
    }
    prng->i = prng->j = 0;

    for (i = 0; i < 256; i += seedLen) {
        sawoRandomBytes(interp, seed, seedLen);
    }
}


static int sawo_IncrCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_wide wideValue, increment = 1;
    sawo_Obj *intObjPtr;

    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?increment?");
        return SAWO_ERR;
    }
    if (argc == 3) {
        if (sawo_GetWide(interp, argv[2], &increment) != SAWO_OK)
            return SAWO_ERR;
    }
    intObjPtr = sawo_GetVariable(interp, argv[1], SAWO_UNSHARED);
    if (!intObjPtr) {
        
        wideValue = 0;
    }
    else if (sawo_GetWide(interp, intObjPtr, &wideValue) != SAWO_OK) {
        return SAWO_ERR;
    }
    if (!intObjPtr || sawo_IsShared(intObjPtr)) {
        intObjPtr = sawo_NewIntObj(interp, wideValue + increment);
        if (sawo_SetVariable(interp, argv[1], intObjPtr) != SAWO_OK) {
            sawo_FreeNewObj(interp, intObjPtr);
            return SAWO_ERR;
        }
    }
    else {
        
        sawo_InvalidateStringRep(intObjPtr);
        sawoWideValue(intObjPtr) = wideValue + increment;

        if (argv[1]->typePtr != &variableObjType) {
            
            sawo_SetVariable(interp, argv[1], intObjPtr);
        }
    }
    sawo_SetResult(interp, intObjPtr);
    return SAWO_OK;
}


#define SAWO_EVAL_SARGV_LEN 8    
#define SAWO_EVAL_SINTV_LEN 8    


static int sawoUnknown(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int retcode;

    if (interp->unknown_called > 50) {
        return SAWO_ERR;
    }


    
    if (sawo_GetCommand(interp, interp->unknown, SAWO_NONE) == NULL)
        return SAWO_ERR;

    interp->unknown_called++;
    
    retcode = sawo_EvalObjPrefix(interp, interp->unknown, argc, argv);
    interp->unknown_called--;

    return retcode;
}

static int sawoInvokeCommand(sawo_Interp *interp, int objc, sawo_Obj *const *objv)
{
    int retcode;
    sawo_Cmd *cmdPtr;

#if 0
    printf("invoke");
    int j;
    for (j = 0; j < objc; j++) {
        printf(" '%s'", sawo_String(objv[j]));
    }
    printf("\n");
#endif

    if (interp->framePtr->tailcallCmd) {
        
        cmdPtr = interp->framePtr->tailcallCmd;
        interp->framePtr->tailcallCmd = NULL;
    }
    else {
        cmdPtr = sawo_GetCommand(interp, objv[0], SAWO_ERRMSG);
        if (cmdPtr == NULL) {
            return sawoUnknown(interp, objc, objv);
        }
        sawoIncrCmdRefCount(cmdPtr);
    }

    if (interp->evalDepth == interp->maxEvalDepth) {
        sawo_SetResultString(interp, "Infinite eval recursion", -1);
        retcode = SAWO_ERR;
        goto out;
    }
    interp->evalDepth++;

    
    sawo_SetEmptyResult(interp);
    if (cmdPtr->israise) {
        retcode = sawoCallProcedure(interp, cmdPtr, objc, objv);
    }
    else {
        interp->cmdPrivData = cmdPtr->u.native.privData;
        retcode = cmdPtr->u.native.cmdRaise(interp, objc, objv);
    }
    interp->evalDepth--;

out:
    sawoDecrCmdRefCount(interp, cmdPtr);

    return retcode;
}

int sawo_EvalObjVector(sawo_Interp *interp, int objc, sawo_Obj *const *objv)
{
    int i, retcode;

    
    for (i = 0; i < objc; i++)
        sawo_IncrRefCount(objv[i]);

    retcode = sawoInvokeCommand(interp, objc, objv);

    
    for (i = 0; i < objc; i++)
        sawo_DecrRefCount(interp, objv[i]);

    return retcode;
}

int sawo_EvalObjPrefix(sawo_Interp *interp, sawo_Obj *prefix, int objc, sawo_Obj *const *objv)
{
    int ret;
    sawo_Obj **nargv = sawo_Alloc((objc + 1) * sizeof(*nargv));

    nargv[0] = prefix;
    memcpy(&nargv[1], &objv[0], sizeof(nargv[0]) * objc);
    ret = sawo_EvalObjVector(interp, objc + 1, nargv);
    sawo_Free(nargv);
    return ret;
}

static void sawoAddErrorToStack(sawo_Interp *interp, ScriptObj *script)
{
    if (!interp->errorFlag) {
        
        interp->errorFlag = 1;
        sawo_IncrRefCount(script->fileNameObj);
        sawo_DecrRefCount(interp, interp->errorFileNameObj);
        interp->errorFileNameObj = script->fileNameObj;
        interp->errorLine = script->linenr;

        sawoResetStackTrace(interp);
        
        interp->addStackTrace++;
    }

    
    if (interp->addStackTrace > 0) {
        

        sawoAppendStackTrace(interp, sawo_String(interp->errorRaise), script->fileNameObj, script->linenr);

        if (sawo_Length(script->fileNameObj)) {
            interp->addStackTrace = 0;
        }

        sawo_DecrRefCount(interp, interp->errorRaise);
        interp->errorRaise = interp->emptyObj;
        sawo_IncrRefCount(interp->errorRaise);
    }
}

static int sawoSubstOneToken(sawo_Interp *interp, const ScriptToken *token, sawo_Obj **objPtrPtr)
{
    sawo_Obj *objPtr;

    switch (token->type) {
        case SAWO_TT_STR:
        case SAWO_TT_ESC:
            objPtr = token->objPtr;
            break;
        case SAWO_TT_VAR:
            objPtr = sawo_GetVariable(interp, token->objPtr, SAWO_ERRMSG);
            break;
        case SAWO_TT_DICTSUGAR:
            objPtr = sawoExpandDictSugar(interp, token->objPtr);
            break;
        case SAWO_TT_EXPRSUGAR:
            objPtr = sawoExpandExprSugar(interp, token->objPtr);
            break;
        case SAWO_TT_CMD:
            switch (sawo_EvalObj(interp, token->objPtr)) {
                case SAWO_OK:
                case SAWO_RETURN:
                    objPtr = interp->result;
                    break;
                case SAWO_BREAK:
                    
                    return SAWO_BREAK;
                case SAWO_CONTINUE:
                    
                    return SAWO_CONTINUE;
                default:
                    return SAWO_ERR;
            }
            break;
        default:
            sawoPanic((1,
                "default token type (%d) reached " "in sawo_SubstObj().", token->type));
            objPtr = NULL;
            break;
    }
    if (objPtr) {
        *objPtrPtr = objPtr;
        return SAWO_OK;
    }
    return SAWO_ERR;
}

static sawo_Obj *sawoInterpolateTokens(sawo_Interp *interp, const ScriptToken * token, int tokens, int flags)
{
    int totlen = 0, i;
    sawo_Obj **intv;
    sawo_Obj *sintv[SAWO_EVAL_SINTV_LEN];
    sawo_Obj *objPtr;
    char *s;

    if (tokens <= SAWO_EVAL_SINTV_LEN)
        intv = sintv;
    else
        intv = sawo_Alloc(sizeof(sawo_Obj *) * tokens);

    for (i = 0; i < tokens; i++) {
        switch (sawoSubstOneToken(interp, &token[i], &intv[i])) {
            case SAWO_OK:
            case SAWO_RETURN:
                break;
            case SAWO_BREAK:
                if (flags & SAWO_SUBST_FLAG) {
                    
                    tokens = i;
                    continue;
                }
                
                
            case SAWO_CONTINUE:
                if (flags & SAWO_SUBST_FLAG) {
                    intv[i] = NULL;
                    continue;
                }
                
                
            default:
                while (i--) {
                    sawo_DecrRefCount(interp, intv[i]);
                }
                if (intv != sintv) {
                    sawo_Free(intv);
                }
                return NULL;
        }
        sawo_IncrRefCount(intv[i]);
        sawo_String(intv[i]);
        totlen += intv[i]->length;
    }

    
    if (tokens == 1 && intv[0] && intv == sintv) {
        sawo_DecrRefCount(interp, intv[0]);
        return intv[0];
    }

    objPtr = sawo_NewStringObjNoAlloc(interp, NULL, 0);

    if (tokens == 4 && token[0].type == SAWO_TT_ESC && token[1].type == SAWO_TT_ESC
        && token[2].type == SAWO_TT_VAR) {
        
        objPtr->typePtr = &interpolatedObjType;
        objPtr->internalRep.dictSubstValue.varNameObjPtr = token[0].objPtr;
        objPtr->internalRep.dictSubstValue.indexObjPtr = intv[2];
        sawo_IncrRefCount(intv[2]);
    }
    else if (tokens && intv[0] && intv[0]->typePtr == &sourceObjType) {
        
        sawoSetSourceInfo(interp, objPtr, intv[0]->internalRep.sourceValue.fileNameObj, intv[0]->internalRep.sourceValue.lineNumber);
    }


    s = objPtr->bytes = sawo_Alloc(totlen + 1);
    objPtr->length = totlen;
    for (i = 0; i < tokens; i++) {
        if (intv[i]) {
            memcpy(s, intv[i]->bytes, intv[i]->length);
            s += intv[i]->length;
            sawo_DecrRefCount(interp, intv[i]);
        }
    }
    objPtr->bytes[totlen] = '\0';
    
    if (intv != sintv) {
        sawo_Free(intv);
    }

    return objPtr;
}


static int sawoEvalObjList(sawo_Interp *interp, sawo_Obj *listPtr)
{
    int retcode = SAWO_OK;

    sawoPanic((sawo_IsList(listPtr) == 0, "sawoEvalObjList() invoked on non-list."));

    if (listPtr->internalRep.listValue.len) {
        sawo_IncrRefCount(listPtr);
        retcode = sawoInvokeCommand(interp,
            listPtr->internalRep.listValue.len,
            listPtr->internalRep.listValue.ele);
        sawo_DecrRefCount(interp, listPtr);
    }
    return retcode;
}

int sawo_EvalObjList(sawo_Interp *interp, sawo_Obj *listPtr)
{
    SetListFromAny(interp, listPtr);
    return sawoEvalObjList(interp, listPtr);
}

int sawo_EvalObj(sawo_Interp *interp, sawo_Obj *scriptObjPtr)
{
    int i;
    ScriptObj *script;
    ScriptToken *token;
    int retcode = SAWO_OK;
    sawo_Obj *sargv[SAWO_EVAL_SARGV_LEN], **argv = NULL;
    sawo_Obj *prevScriptObj;

    if (sawo_IsList(scriptObjPtr) && scriptObjPtr->bytes == NULL) {
        return sawoEvalObjList(interp, scriptObjPtr);
    }

    sawo_IncrRefCount(scriptObjPtr);     
    script = sawoGetScript(interp, scriptObjPtr);
    if (!sawoScriptValid(interp, script)) {
        sawo_DecrRefCount(interp, scriptObjPtr);
        return SAWO_ERR;
    }

    sawo_SetEmptyResult(interp);

    token = script->token;

#ifdef SAWO_OPTIMIZATION
    if (script->len == 0) {
        sawo_DecrRefCount(interp, scriptObjPtr);
        return SAWO_OK;
    }
    if (script->len == 3
        && token[1].objPtr->typePtr == &commandObjType
        && token[1].objPtr->internalRep.cmdValue.cmdPtr->israise == 0
        && token[1].objPtr->internalRep.cmdValue.cmdPtr->u.native.cmdRaise == sawo_IncrCoreCommand
        && token[2].objPtr->typePtr == &variableObjType) {

        sawo_Obj *objPtr = sawo_GetVariable(interp, token[2].objPtr, SAWO_NONE);

        if (objPtr && !sawo_IsShared(objPtr) && objPtr->typePtr == &intObjType) {
            sawoWideValue(objPtr)++;
            sawo_InvalidateStringRep(objPtr);
            sawo_DecrRefCount(interp, scriptObjPtr);
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;
        }
    }
#endif

    script->inUse++;

    
    prevScriptObj = interp->currentScriptObj;
    interp->currentScriptObj = scriptObjPtr;

    interp->errorFlag = 0;
    argv = sargv;

    for (i = 0; i < script->len && retcode == SAWO_OK; ) {
        int argc;
        int j;

        
        argc = token[i].objPtr->internalRep.scriptLineValue.argc;
        script->linenr = token[i].objPtr->internalRep.scriptLineValue.line;

        
        if (argc > SAWO_EVAL_SARGV_LEN)
            argv = sawo_Alloc(sizeof(sawo_Obj *) * argc);

        
        i++;

        for (j = 0; j < argc; j++) {
            long wordtokens = 1;
            int expand = 0;
            sawo_Obj *wordObjPtr = NULL;

            if (token[i].type == SAWO_TT_WORD) {
                wordtokens = sawoWideValue(token[i++].objPtr);
                if (wordtokens < 0) {
                    expand = 1;
                    wordtokens = -wordtokens;
                }
            }

            if (wordtokens == 1) {

                switch (token[i].type) {
                    case SAWO_TT_ESC:
                    case SAWO_TT_STR:
                        wordObjPtr = token[i].objPtr;
                        break;
                    case SAWO_TT_VAR:
                        wordObjPtr = sawo_GetVariable(interp, token[i].objPtr, SAWO_ERRMSG);
                        break;
                    case SAWO_TT_EXPRSUGAR:
                        wordObjPtr = sawoExpandExprSugar(interp, token[i].objPtr);
                        break;
                    case SAWO_TT_DICTSUGAR:
                        wordObjPtr = sawoExpandDictSugar(interp, token[i].objPtr);
                        break;
                    case SAWO_TT_CMD:
                        retcode = sawo_EvalObj(interp, token[i].objPtr);
                        if (retcode == SAWO_OK) {
                            wordObjPtr = sawo_GetResult(interp);
                        }
                        break;
                    default:
                        sawoPanic((1, "default token type reached " "in sawo_EvalObj()."));
                }
            }
            else {
                wordObjPtr = sawoInterpolateTokens(interp, token + i, wordtokens, SAWO_NONE);
            }

            if (!wordObjPtr) {
                if (retcode == SAWO_OK) {
                    retcode = SAWO_ERR;
                }
                break;
            }

            sawo_IncrRefCount(wordObjPtr);
            i += wordtokens;

            if (!expand) {
                argv[j] = wordObjPtr;
            }
            else {
                
                int len = sawo_ListLength(interp, wordObjPtr);
                int newargc = argc + len - 1;
                int k;

                if (len > 1) {
                    if (argv == sargv) {
                        if (newargc > SAWO_EVAL_SARGV_LEN) {
                            argv = sawo_Alloc(sizeof(*argv) * newargc);
                            memcpy(argv, sargv, sizeof(*argv) * j);
                        }
                    }
                    else {
                        
                        argv = sawo_Realloc(argv, sizeof(*argv) * newargc);
                    }
                }

                
                for (k = 0; k < len; k++) {
                    argv[j++] = wordObjPtr->internalRep.listValue.ele[k];
                    sawo_IncrRefCount(wordObjPtr->internalRep.listValue.ele[k]);
                }

                sawo_DecrRefCount(interp, wordObjPtr);

                
                j--;
                argc += len - 1;
            }
        }

        if (retcode == SAWO_OK && argc) {
            
            retcode = sawoInvokeCommand(interp, argc, argv);
            
            if (sawo_CheckSignal(interp)) {
                retcode = SAWO_SIGNAL;
            }
        }

        
        while (j-- > 0) {
            sawo_DecrRefCount(interp, argv[j]);
        }

        if (argv != sargv) {
            sawo_Free(argv);
            argv = sargv;
        }
    }

    
    if (retcode == SAWO_ERR) {
        sawoAddErrorToStack(interp, script);
    }
    
    else if (retcode != SAWO_RETURN || interp->returnCode != SAWO_ERR) {
        
        interp->addStackTrace = 0;
    }

    
    interp->currentScriptObj = prevScriptObj;

    sawo_FreeIntRep(interp, scriptObjPtr);
    scriptObjPtr->typePtr = &scriptObjType;
    sawo_SetIntRepPtr(scriptObjPtr, script);
    sawo_DecrRefCount(interp, scriptObjPtr);

    return retcode;
}

static int sawoSetRaiseArg(sawo_Interp *interp, sawo_Obj *argNameObj, sawo_Obj *argValObj)
{
    int retcode;
    
    const char *varname = sawo_String(argNameObj);
    if (*varname == '&') {
        
        sawo_Obj *objPtr;
        sawo_CallFrame *savedCallFrame = interp->framePtr;

        interp->framePtr = interp->framePtr->parent;
        objPtr = sawo_GetVariable(interp, argValObj, SAWO_ERRMSG);
        interp->framePtr = savedCallFrame;
        if (!objPtr) {
            return SAWO_ERR;
        }

        
        objPtr = sawo_NewStringObj(interp, varname + 1, -1);
        sawo_IncrRefCount(objPtr);
        retcode = sawo_SetVariableLink(interp, objPtr, argValObj, interp->framePtr->parent);
        sawo_DecrRefCount(interp, objPtr);
    }
    else {
        retcode = sawo_SetVariable(interp, argNameObj, argValObj);
    }
    return retcode;
}

static void sawoSetRaiseWrongArgs(sawo_Interp *interp, sawo_Obj *raiseNameObj, sawo_Cmd *cmd)
{
    
    sawo_Obj *argmsg = sawo_NewStringObj(interp, "", 0);
    int i;

    for (i = 0; i < cmd->u.raise.argListLen; i++) {
        sawo_AppendString(interp, argmsg, " ", 1);

        if (i == cmd->u.raise.argsPos) {
            if (cmd->u.raise.arglist[i].defaultObjPtr) {
                
                sawo_AppendString(interp, argmsg, "?", 1);
                sawo_AppendObj(interp, argmsg, cmd->u.raise.arglist[i].defaultObjPtr);
                sawo_AppendString(interp, argmsg, " ...?", -1);
            }
            else {
                
                sawo_AppendString(interp, argmsg, "?arg...?", -1);
            }
        }
        else {
            if (cmd->u.raise.arglist[i].defaultObjPtr) {
                sawo_AppendString(interp, argmsg, "?", 1);
                sawo_AppendObj(interp, argmsg, cmd->u.raise.arglist[i].nameObjPtr);
                sawo_AppendString(interp, argmsg, "?", 1);
            }
            else {
                const char *arg = sawo_String(cmd->u.raise.arglist[i].nameObjPtr);
                if (*arg == '&') {
                    arg++;
                }
                sawo_AppendString(interp, argmsg, arg, -1);
            }
        }
    }
    sawo_SetResultFormatted(interp, "wrong # args: should be \"%#s%#s\"", raiseNameObj, argmsg);
    sawo_FreeNewObj(interp, argmsg);
}

#ifdef sawo_ext_namespace
int sawo_EvalNamespace(sawo_Interp *interp, sawo_Obj *scriptObj, sawo_Obj *nsObj)
{
    sawo_CallFrame *callFramePtr;
    int retcode;

    
    callFramePtr = sawoCreateCallFrame(interp, interp->framePtr, nsObj);
    callFramePtr->argv = &interp->emptyObj;
    callFramePtr->argc = 0;
    callFramePtr->raiseArgsObjPtr = NULL;
    callFramePtr->raiseBodyObjPtr = scriptObj;
    callFramePtr->staticVars = NULL;
    callFramePtr->fileNameObj = interp->emptyObj;
    callFramePtr->line = 0;
    sawo_IncrRefCount(scriptObj);
    interp->framePtr = callFramePtr;

    
    if (interp->framePtr->level == interp->maxCallFrameDepth) {
        sawo_SetResultString(interp, "Too many nested calls. Infinite recursion?", -1);
        retcode = SAWO_ERR;
    }
    else {
        
        retcode = sawo_EvalObj(interp, scriptObj);
    }

    
    interp->framePtr = interp->framePtr->parent;
    sawoFreeCallFrame(interp, callFramePtr, SAWO_FCF_REUSE);

    return retcode;
}
#endif

static int sawoCallProcedure(sawo_Interp *interp, sawo_Cmd *cmd, int argc, sawo_Obj *const *argv)
{
    sawo_CallFrame *callFramePtr;
    int i, d, retcode, optargs;
    ScriptObj *script;

    
    if (argc - 1 < cmd->u.raise.reqArity ||
        (cmd->u.raise.argsPos < 0 && argc - 1 > cmd->u.raise.reqArity + cmd->u.raise.optArity)) {
        sawoSetRaiseWrongArgs(interp, argv[0], cmd);
        return SAWO_ERR;
    }

    if (sawo_Length(cmd->u.raise.bodyObjPtr) == 0) {
        
        return SAWO_OK;
    }

    
    if (interp->framePtr->level == interp->maxCallFrameDepth) {
        sawo_SetResultString(interp, "Too many nested calls. Infinite recursion?", -1);
        return SAWO_ERR;
    }

    
    callFramePtr = sawoCreateCallFrame(interp, interp->framePtr, cmd->u.raise.nsObj);
    callFramePtr->argv = argv;
    callFramePtr->argc = argc;
    callFramePtr->raiseArgsObjPtr = cmd->u.raise.argListObjPtr;
    callFramePtr->raiseBodyObjPtr = cmd->u.raise.bodyObjPtr;
    callFramePtr->staticVars = cmd->u.raise.staticVars;

    
    script = sawoGetScript(interp, interp->currentScriptObj);
    callFramePtr->fileNameObj = script->fileNameObj;
    callFramePtr->line = script->linenr;

    sawo_IncrRefCount(cmd->u.raise.argListObjPtr);
    sawo_IncrRefCount(cmd->u.raise.bodyObjPtr);
    interp->framePtr = callFramePtr;

    
    optargs = (argc - 1 - cmd->u.raise.reqArity);

    
    i = 1;
    for (d = 0; d < cmd->u.raise.argListLen; d++) {
        sawo_Obj *nameObjPtr = cmd->u.raise.arglist[d].nameObjPtr;
        if (d == cmd->u.raise.argsPos) {
            
            sawo_Obj *listObjPtr;
            int argsLen = 0;
            if (cmd->u.raise.reqArity + cmd->u.raise.optArity < argc - 1) {
                argsLen = argc - 1 - (cmd->u.raise.reqArity + cmd->u.raise.optArity);
            }
            listObjPtr = sawo_NewListObj(interp, &argv[i], argsLen);

            
            if (cmd->u.raise.arglist[d].defaultObjPtr) {
                nameObjPtr =cmd->u.raise.arglist[d].defaultObjPtr;
            }
            retcode = sawo_SetVariable(interp, nameObjPtr, listObjPtr);
            if (retcode != SAWO_OK) {
                goto badargset;
            }

            i += argsLen;
            continue;
        }

        
        if (cmd->u.raise.arglist[d].defaultObjPtr == NULL || optargs-- > 0) {
            retcode = sawoSetRaiseArg(interp, nameObjPtr, argv[i++]);
        }
        else {
            
            retcode = sawo_SetVariable(interp, nameObjPtr, cmd->u.raise.arglist[d].defaultObjPtr);
        }
        if (retcode != SAWO_OK) {
            goto badargset;
        }
    }

    
    retcode = sawo_EvalObj(interp, cmd->u.raise.bodyObjPtr);

badargset:

    
    interp->framePtr = interp->framePtr->parent;
    sawoFreeCallFrame(interp, callFramePtr, SAWO_FCF_REUSE);

    
    if (interp->framePtr->tailcallObj) {
        do {
            sawo_Obj *tailcallObj = interp->framePtr->tailcallObj;

            interp->framePtr->tailcallObj = NULL;

            if (retcode == SAWO_EVAL) {
                retcode = sawo_EvalObjList(interp, tailcallObj);
                if (retcode == SAWO_RETURN) {
                    interp->returnLevel++;
                }
            }
            sawo_DecrRefCount(interp, tailcallObj);
        } while (interp->framePtr->tailcallObj);

        
        if (interp->framePtr->tailcallCmd) {
            sawoDecrCmdRefCount(interp, interp->framePtr->tailcallCmd);
            interp->framePtr->tailcallCmd = NULL;
        }
    }

    
    if (retcode == SAWO_RETURN) {
        if (--interp->returnLevel <= 0) {
            retcode = interp->returnCode;
            interp->returnCode = SAWO_OK;
            interp->returnLevel = 0;
        }
    }
    else if (retcode == SAWO_ERR) {
        interp->addStackTrace++;
        sawo_DecrRefCount(interp, interp->errorRaise);
        interp->errorRaise = argv[0];
        sawo_IncrRefCount(interp->errorRaise);
    }

    return retcode;
}

int sawo_EvalSource(sawo_Interp *interp, const char *filename, int lineno, const char *script)
{
    int retval;
    sawo_Obj *scriptObjPtr;

    scriptObjPtr = sawo_NewStringObj(interp, script, -1);
    sawo_IncrRefCount(scriptObjPtr);

    if (filename) {
        sawo_Obj *prevScriptObj;

        sawoSetSourceInfo(interp, scriptObjPtr, sawo_NewStringObj(interp, filename, -1), lineno);

        prevScriptObj = interp->currentScriptObj;
        interp->currentScriptObj = scriptObjPtr;

        retval = sawo_EvalObj(interp, scriptObjPtr);

        interp->currentScriptObj = prevScriptObj;
    }
    else {
        retval = sawo_EvalObj(interp, scriptObjPtr);
    }
    sawo_DecrRefCount(interp, scriptObjPtr);
    return retval;
}

int sawo_Eval(sawo_Interp *interp, const char *script)
{
    return sawo_EvalObj(interp, sawo_NewStringObj(interp, script, -1));
}


int sawo_EvalGlobal(sawo_Interp *interp, const char *script)
{
    int retval;
    sawo_CallFrame *savedFramePtr = interp->framePtr;

    interp->framePtr = interp->topFramePtr;
    retval = sawo_Eval(interp, script);
    interp->framePtr = savedFramePtr;

    return retval;
}

int sawo_EvalFileGlobal(sawo_Interp *interp, const char *filename)
{
    int retval;
    sawo_CallFrame *savedFramePtr = interp->framePtr;

    interp->framePtr = interp->topFramePtr;
    retval = sawo_EvalFile(interp, filename);
    interp->framePtr = savedFramePtr;

    return retval;
}

#include <sys/stat.h>

int sawo_EvalFile(sawo_Interp *interp, const char *filename)
{
    FILE *fp;
    char *buf;
    sawo_Obj *scriptObjPtr;
    sawo_Obj *prevScriptObj;
    struct stat sb;
    int retcode;
    int readlen;

    if (stat(filename, &sb) != 0 || (fp = fopen(filename, "rt")) == NULL) {
        sawo_SetResultFormatted(interp, "couldn't read file \"%s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    if (sb.st_size == 0) {
        fclose(fp);
        return SAWO_OK;
    }

    buf = sawo_Alloc(sb.st_size + 1);
    readlen = fread(buf, 1, sb.st_size, fp);
    if (ferror(fp)) {
        fclose(fp);
        sawo_Free(buf);
        sawo_SetResultFormatted(interp, "failed to load file \"%s\": %s", filename, strerror(errno));
        return SAWO_ERR;
    }
    fclose(fp);
    buf[readlen] = 0;

    scriptObjPtr = sawo_NewStringObjNoAlloc(interp, buf, readlen);
    sawoSetSourceInfo(interp, scriptObjPtr, sawo_NewStringObj(interp, filename, -1), 1);
    sawo_IncrRefCount(scriptObjPtr);

    prevScriptObj = interp->currentScriptObj;
    interp->currentScriptObj = scriptObjPtr;

    retcode = sawo_EvalObj(interp, scriptObjPtr);

    
    if (retcode == SAWO_RETURN) {
        if (--interp->returnLevel <= 0) {
            retcode = interp->returnCode;
            interp->returnCode = SAWO_OK;
            interp->returnLevel = 0;
        }
    }
    if (retcode == SAWO_ERR) {
        
        interp->addStackTrace++;
    }

    interp->currentScriptObj = prevScriptObj;

    sawo_DecrRefCount(interp, scriptObjPtr);

    return retcode;
}

static void sawoParseSubst(struct sawoParserCtx *pc, int flags)
{
    pc->tstart = pc->p;
    pc->tline = pc->linenr;

    if (pc->len == 0) {
        pc->tend = pc->p;
        pc->tt = SAWO_TT_EOL;
        pc->eof = 1;
        return;
    }
    if (*pc->p == '[' && !(flags & SAWO_SUBST_NOCMD)) {
        sawoParseCmd(pc);
        return;
    }
    if (*pc->p == '$' && !(flags & SAWO_SUBST_NOVAR)) {
        if (sawoParseVar(pc) == SAWO_OK) {
            return;
        }
        
        pc->tstart = pc->p;
        flags |= SAWO_SUBST_NOVAR;
    }
    while (pc->len) {
        if (*pc->p == '$' && !(flags & SAWO_SUBST_NOVAR)) {
            break;
        }
        if (*pc->p == '[' && !(flags & SAWO_SUBST_NOCMD)) {
            break;
        }
        if (*pc->p == '\\' && pc->len > 1) {
            pc->p++;
            pc->len--;
        }
        pc->p++;
        pc->len--;
    }
    pc->tend = pc->p - 1;
    pc->tt = (flags & SAWO_SUBST_NOESC) ? SAWO_TT_STR : SAWO_TT_ESC;
}


static int SetSubstFromAny(sawo_Interp *interp, struct sawo_Obj *objPtr, int flags)
{
    int scriptTextLen;
    const char *scriptText = sawo_GetString(objPtr, &scriptTextLen);
    struct sawoParserCtx parser;
    struct ScriptObj *script = sawo_Alloc(sizeof(*script));
    ParseTokenList tokenlist;

    
    ScriptTokenListInit(&tokenlist);

    sawoParserInit(&parser, scriptText, scriptTextLen, 1);
    while (1) {
        sawoParseSubst(&parser, flags);
        if (parser.eof) {
            
            break;
        }
        ScriptAddToken(&tokenlist, parser.tstart, parser.tend - parser.tstart + 1, parser.tt,
            parser.tline);
    }

    
    script->inUse = 1;
    script->substFlags = flags;
    script->fileNameObj = interp->emptyObj;
    sawo_IncrRefCount(script->fileNameObj);
    SubstObjAddTokens(interp, script, &tokenlist);

    
    ScriptTokenListFree(&tokenlist);

#ifdef DEBUG_SHOW_SUBST
    {
        int i;

        printf("==== Subst ====\n");
        for (i = 0; i < script->len; i++) {
            printf("[%2d] %s '%s'\n", i, sawo_tt_name(script->token[i].type),
                sawo_String(script->token[i].objPtr));
        }
    }
#endif

    
    sawo_FreeIntRep(interp, objPtr);
    sawo_SetIntRepPtr(objPtr, script);
    objPtr->typePtr = &scriptObjType;
    return SAWO_OK;
}

static ScriptObj *sawo_GetSubst(sawo_Interp *interp, sawo_Obj *objPtr, int flags)
{
    if (objPtr->typePtr != &scriptObjType || ((ScriptObj *)sawo_GetIntRepPtr(objPtr))->substFlags != flags)
        SetSubstFromAny(interp, objPtr, flags);
    return (ScriptObj *) sawo_GetIntRepPtr(objPtr);
}

int sawo_SubstObj(sawo_Interp *interp, sawo_Obj *substObjPtr, sawo_Obj **resObjPtrPtr, int flags)
{
    ScriptObj *script = sawo_GetSubst(interp, substObjPtr, flags);

    sawo_IncrRefCount(substObjPtr);      
    script->inUse++;

    *resObjPtrPtr = sawoInterpolateTokens(interp, script->token, script->len, flags);

    script->inUse--;
    sawo_DecrRefCount(interp, substObjPtr);
    if (*resObjPtrPtr == NULL) {
        return SAWO_ERR;
    }
    return SAWO_OK;
}

void sawo_WrongNumArgs(sawo_Interp *interp, int argc, sawo_Obj *const *argv, const char *msg)
{
    sawo_Obj *objPtr;
    sawo_Obj *listObjPtr = sawo_NewListObj(interp, argv, argc);

    if (*msg) {
        sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp, msg, -1));
    }
    sawo_IncrRefCount(listObjPtr);
    objPtr = sawo_ListJoin(interp, listObjPtr, " ", 1);
    sawo_DecrRefCount(interp, listObjPtr);

    sawo_IncrRefCount(objPtr);
    sawo_SetResultFormatted(interp, "wrong # args: should be \"%#s\"", objPtr);
    sawo_DecrRefCount(interp, objPtr);
}

typedef void sawoHashtableIteratorCallbackType(sawo_Interp *interp, sawo_Obj *listObjPtr,
    sawo_HashEntry *he, int type);

#define sawoTrivialMatch(pattern)    (strpbrk((pattern), "*[?\\") == NULL)

static sawo_Obj *sawoHashtablePatternMatch(sawo_Interp *interp, sawo_HashTable *ht, sawo_Obj *patternObjPtr,
    sawoHashtableIteratorCallbackType *callback, int type)
{
    sawo_HashEntry *he;
    sawo_Obj *listObjPtr = sawo_NewListObj(interp, NULL, 0);

    
    if (patternObjPtr && sawoTrivialMatch(sawo_String(patternObjPtr))) {
        he = sawo_FindHashEntry(ht, sawo_String(patternObjPtr));
        if (he) {
            callback(interp, listObjPtr, he, type);
        }
    }
    else {
        sawo_HashTableIterator htiter;
        sawoInitHashTableIterator(ht, &htiter);
        while ((he = sawo_NextHashEntry(&htiter)) != NULL) {
            if (patternObjPtr == NULL || sawoGlobMatch(sawo_String(patternObjPtr), he->key, 0)) {
                callback(interp, listObjPtr, he, type);
            }
        }
    }
    return listObjPtr;
}


#define SAWO_CMDLIST_COMMANDS 0
#define SAWO_CMDLIST_RAISES 1
#define SAWO_CMDLIST_CHANNELS 2

static void sawoCommandMatch(sawo_Interp *interp, sawo_Obj *listObjPtr,
    sawo_HashEntry *he, int type)
{
    sawo_Cmd *cmdPtr = sawo_GetHashEntryVal(he);
    sawo_Obj *objPtr;

    if (type == SAWO_CMDLIST_RAISES && !cmdPtr->israise) {
        
        return;
    }

    objPtr = sawo_NewStringObj(interp, he->key, -1);
    sawo_IncrRefCount(objPtr);

    if (type != SAWO_CMDLIST_CHANNELS || sawo_AioFilehandle(interp, objPtr)) {
        sawo_ListAppendElement(interp, listObjPtr, objPtr);
    }
    sawo_DecrRefCount(interp, objPtr);
}


static sawo_Obj *sawoCommandsList(sawo_Interp *interp, sawo_Obj *patternObjPtr, int type)
{
    return sawoHashtablePatternMatch(interp, &interp->commands, patternObjPtr, sawoCommandMatch, type);
}


#define SAWO_VARLIST_GLOBALS 0
#define SAWO_VARLIST_LOCALS 1
#define SAWO_VARLIST_VARS 2

#define SAWO_VARLIST_VALUES 0x1000

static void sawoVariablesMatch(sawo_Interp *interp, sawo_Obj *listObjPtr,
    sawo_HashEntry *he, int type)
{
    sawo_Var *varPtr = sawo_GetHashEntryVal(he);

    if (type != SAWO_VARLIST_LOCALS || varPtr->linkFramePtr == NULL) {
        sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp, he->key, -1));
        if (type & SAWO_VARLIST_VALUES) {
            sawo_ListAppendElement(interp, listObjPtr, varPtr->objPtr);
        }
    }
}


static sawo_Obj *sawoVariablesList(sawo_Interp *interp, sawo_Obj *patternObjPtr, int mode)
{
    if (mode == SAWO_VARLIST_LOCALS && interp->framePtr == interp->topFramePtr) {
        return interp->emptyObj;
    }
    else {
        sawo_CallFrame *framePtr = (mode == SAWO_VARLIST_GLOBALS) ? interp->topFramePtr : interp->framePtr;
        return sawoHashtablePatternMatch(interp, &framePtr->vars, patternObjPtr, sawoVariablesMatch, mode);
    }
}

static int sawoInfoLevel(sawo_Interp *interp, sawo_Obj *levelObjPtr,
    sawo_Obj **objPtrPtr, int info_level_cmd)
{
    sawo_CallFrame *targetCallFrame;

    targetCallFrame = sawoGetCallFrameByInteger(interp, levelObjPtr);
    if (targetCallFrame == NULL) {
        return SAWO_ERR;
    }
    
    if (targetCallFrame == interp->topFramePtr) {
        sawo_SetResultFormatted(interp, "bad level \"%#s\"", levelObjPtr);
        return SAWO_ERR;
    }
    if (info_level_cmd) {
        *objPtrPtr = sawo_NewListObj(interp, targetCallFrame->argv, targetCallFrame->argc);
    }
    else {
        sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);

        sawo_ListAppendElement(interp, listObj, targetCallFrame->argv[0]);
        sawo_ListAppendElement(interp, listObj, targetCallFrame->fileNameObj);
        sawo_ListAppendElement(interp, listObj, sawo_NewIntObj(interp, targetCallFrame->line));
        *objPtrPtr = listObj;
    }
    return SAWO_OK;
}



static int sawo_PinCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "?-nonewline? string");
        return SAWO_ERR;
    }
    if (argc == 3) {
        if (!sawo_CompareStringImmediate(interp, argv[1], "-nonewline")) {
            sawo_SetResultString(interp, "The second argument must " "be -nonewline", -1);
            return SAWO_ERR;
        }
        else {
            fputs(sawo_String(argv[2]), stdout);
        }
    }
    else {
        puts(sawo_String(argv[1]));
    }
    return SAWO_OK;
}


static int sawoAddMulHelper(sawo_Interp *interp, int argc, sawo_Obj *const *argv, int op)
{
    sawo_wide wideValue, res;
    double doubleValue, doubleRes;
    int i;

    res = (op == SAWO_EXPROP_ADD) ? 0 : 1;

    for (i = 1; i < argc; i++) {
        if (sawo_GetWide(interp, argv[i], &wideValue) != SAWO_OK)
            goto trydouble;
        if (op == SAWO_EXPROP_ADD)
            res += wideValue;
        else
            res *= wideValue;
    }
    sawo_SetResultInt(interp, res);
    return SAWO_OK;
  trydouble:
    doubleRes = (double)res;
    for (; i < argc; i++) {
        if (sawo_GetDouble(interp, argv[i], &doubleValue) != SAWO_OK)
            return SAWO_ERR;
        if (op == SAWO_EXPROP_ADD)
            doubleRes += doubleValue;
        else
            doubleRes *= doubleValue;
    }
    sawo_SetResult(interp, sawo_NewDoubleObj(interp, doubleRes));
    return SAWO_OK;
}


static int sawoSubDivHelper(sawo_Interp *interp, int argc, sawo_Obj *const *argv, int op)
{
    sawo_wide wideValue, res = 0;
    double doubleValue, doubleRes = 0;
    int i = 2;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "number ?number ... number?");
        return SAWO_ERR;
    }
    else if (argc == 2) {
        if (sawo_GetWide(interp, argv[1], &wideValue) != SAWO_OK) {
            if (sawo_GetDouble(interp, argv[1], &doubleValue) != SAWO_OK) {
                return SAWO_ERR;
            }
            else {
                if (op == SAWO_EXPROP_SUB)
                    doubleRes = -doubleValue;
                else
                    doubleRes = 1.0 / doubleValue;
                sawo_SetResult(interp, sawo_NewDoubleObj(interp, doubleRes));
                return SAWO_OK;
            }
        }
        if (op == SAWO_EXPROP_SUB) {
            res = -wideValue;
            sawo_SetResultInt(interp, res);
        }
        else {
            doubleRes = 1.0 / wideValue;
            sawo_SetResult(interp, sawo_NewDoubleObj(interp, doubleRes));
        }
        return SAWO_OK;
    }
    else {
        if (sawo_GetWide(interp, argv[1], &res) != SAWO_OK) {
            if (sawo_GetDouble(interp, argv[1], &doubleRes)
                != SAWO_OK) {
                return SAWO_ERR;
            }
            else {
                goto trydouble;
            }
        }
    }
    for (i = 2; i < argc; i++) {
        if (sawo_GetWide(interp, argv[i], &wideValue) != SAWO_OK) {
            doubleRes = (double)res;
            goto trydouble;
        }
        if (op == SAWO_EXPROP_SUB)
            res -= wideValue;
        else
            res /= wideValue;
    }
    sawo_SetResultInt(interp, res);
    return SAWO_OK;
  trydouble:
    for (; i < argc; i++) {
        if (sawo_GetDouble(interp, argv[i], &doubleValue) != SAWO_OK)
            return SAWO_ERR;
        if (op == SAWO_EXPROP_SUB)
            doubleRes -= doubleValue;
        else
            doubleRes /= doubleValue;
    }
    sawo_SetResult(interp, sawo_NewDoubleObj(interp, doubleRes));
    return SAWO_OK;
}



static int sawo_AddCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoAddMulHelper(interp, argc, argv, SAWO_EXPROP_ADD);
}


static int sawo_MulCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoAddMulHelper(interp, argc, argv, SAWO_EXPROP_MUL);
}


static int sawo_SubCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoSubDivHelper(interp, argc, argv, SAWO_EXPROP_SUB);
}


static int sawo_DivCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoSubDivHelper(interp, argc, argv, SAWO_EXPROP_DIV);
}


static int sawo_SetCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?newValue?");
        return SAWO_ERR;
    }
    if (argc == 2) {
        sawo_Obj *objPtr;

        objPtr = sawo_GetVariable(interp, argv[1], SAWO_ERRMSG);
        if (!objPtr)
            return SAWO_ERR;
        sawo_SetResult(interp, objPtr);
        return SAWO_OK;
    }
    
    if (sawo_SetVariable(interp, argv[1], argv[2]) != SAWO_OK)
        return SAWO_ERR;
    sawo_SetResult(interp, argv[2]);
    return SAWO_OK;
}

static int sawo_UnsetCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i = 1;
    int complain = 1;

    while (i < argc) {
        if (sawo_CompareStringImmediate(interp, argv[i], "--")) {
            i++;
            break;
        }
        if (sawo_CompareStringImmediate(interp, argv[i], "-nocomplain")) {
            complain = 0;
            i++;
            continue;
        }
        break;
    }

    while (i < argc) {
        if (sawo_UnsetVariable(interp, argv[i], complain ? SAWO_ERRMSG : SAWO_NONE) != SAWO_OK
            && complain) {
            return SAWO_ERR;
        }
        i++;
    }
    return SAWO_OK;
}


static int sawo_WhileCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "condition body");
        return SAWO_ERR;
    }

    
    while (1) {
        int boolean, retval;

        if ((retval = sawo_GetBoolFromExpr(interp, argv[1], &boolean)) != SAWO_OK)
            return retval;
        if (!boolean)
            break;

        if ((retval = sawo_EvalObj(interp, argv[2])) != SAWO_OK) {
            switch (retval) {
                case SAWO_BREAK:
                    goto out;
                    break;
                case SAWO_CONTINUE:
                    continue;
                    break;
                default:
                    return retval;
            }
        }
    }
  out:
    sawo_SetEmptyResult(interp);
    return SAWO_OK;
}


static int sawo_ForCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int retval;
    int boolean = 1;
    sawo_Obj *varNamePtr = NULL;
    sawo_Obj *stopVarNamePtr = NULL;

    if (argc != 5) {
        sawo_WrongNumArgs(interp, 1, argv, "start test next body");
        return SAWO_ERR;
    }

    
    if ((retval = sawo_EvalObj(interp, argv[1])) != SAWO_OK) {
        return retval;
    }

    retval = sawo_GetBoolFromExpr(interp, argv[2], &boolean);


#ifdef SAWO_OPTIMIZATION
    if (retval == SAWO_OK && boolean) {
        ScriptObj *incrScript;
        ExprByteCode *expr;
        sawo_wide stop, currentVal;
        sawo_Obj *objPtr;
        int cmpOffset;

        
        expr = sawoGetExpression(interp, argv[2]);
        incrScript = sawoGetScript(interp, argv[3]);

        
        if (incrScript == NULL || incrScript->len != 3 || !expr || expr->len != 3) {
            goto evalstart;
        }
        
        if (incrScript->token[1].type != SAWO_TT_ESC ||
            expr->token[0].type != SAWO_TT_VAR ||
            (expr->token[1].type != SAWO_TT_EXPR_INT && expr->token[1].type != SAWO_TT_VAR)) {
            goto evalstart;
        }

        if (expr->token[2].type == SAWO_EXPROP_LT) {
            cmpOffset = 0;
        }
        else if (expr->token[2].type == SAWO_EXPROP_LTE) {
            cmpOffset = 1;
        }
        else {
            goto evalstart;
        }

        
        if (!sawo_CompareStringImmediate(interp, incrScript->token[1].objPtr, "incr")) {
            goto evalstart;
        }

        
        if (!sawo_StringEqObj(incrScript->token[2].objPtr, expr->token[0].objPtr)) {
            goto evalstart;
        }

        
        if (expr->token[1].type == SAWO_TT_EXPR_INT) {
            if (sawo_GetWide(interp, expr->token[1].objPtr, &stop) == SAWO_ERR) {
                goto evalstart;
            }
        }
        else {
            stopVarNamePtr = expr->token[1].objPtr;
            sawo_IncrRefCount(stopVarNamePtr);
            
            stop = 0;
        }

        
        varNamePtr = expr->token[0].objPtr;
        sawo_IncrRefCount(varNamePtr);

        objPtr = sawo_GetVariable(interp, varNamePtr, SAWO_NONE);
        if (objPtr == NULL || sawo_GetWide(interp, objPtr, &currentVal) != SAWO_OK) {
            goto testcond;
        }

        
        while (retval == SAWO_OK) {
            
            

            
            if (stopVarNamePtr) {
                objPtr = sawo_GetVariable(interp, stopVarNamePtr, SAWO_NONE);
                if (objPtr == NULL || sawo_GetWide(interp, objPtr, &stop) != SAWO_OK) {
                    goto testcond;
                }
            }

            if (currentVal >= stop + cmpOffset) {
                break;
            }

            
            retval = sawo_EvalObj(interp, argv[4]);
            if (retval == SAWO_OK || retval == SAWO_CONTINUE) {
                retval = SAWO_OK;

                objPtr = sawo_GetVariable(interp, varNamePtr, SAWO_ERRMSG);

                
                if (objPtr == NULL) {
                    retval = SAWO_ERR;
                    goto out;
                }
                if (!sawo_IsShared(objPtr) && objPtr->typePtr == &intObjType) {
                    currentVal = ++sawoWideValue(objPtr);
                    sawo_InvalidateStringRep(objPtr);
                }
                else {
                    if (sawo_GetWide(interp, objPtr, &currentVal) != SAWO_OK ||
                        sawo_SetVariable(interp, varNamePtr, sawo_NewIntObj(interp,
                                ++currentVal)) != SAWO_OK) {
                        goto evalnext;
                    }
                }
            }
        }
        goto out;
    }
  evalstart:
#endif

    while (boolean && (retval == SAWO_OK || retval == SAWO_CONTINUE)) {
        
        retval = sawo_EvalObj(interp, argv[4]);

        if (retval == SAWO_OK || retval == SAWO_CONTINUE) {
            
          evalnext:
            retval = sawo_EvalObj(interp, argv[3]);
            if (retval == SAWO_OK || retval == SAWO_CONTINUE) {
                
              testcond:
                retval = sawo_GetBoolFromExpr(interp, argv[2], &boolean);
            }
        }
    }
  out:
    if (stopVarNamePtr) {
        sawo_DecrRefCount(interp, stopVarNamePtr);
    }
    if (varNamePtr) {
        sawo_DecrRefCount(interp, varNamePtr);
    }

    if (retval == SAWO_CONTINUE || retval == SAWO_BREAK || retval == SAWO_OK) {
        sawo_SetEmptyResult(interp);
        return SAWO_OK;
    }

    return retval;
}


static int sawo_LoopCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int retval;
    sawo_wide i;
    sawo_wide limit;
    sawo_wide incr = 1;
    sawo_Obj *bodyObjPtr;

    if (argc != 5 && argc != 6) {
        sawo_WrongNumArgs(interp, 1, argv, "var first limit ?incr? body");
        return SAWO_ERR;
    }

    if (sawo_GetWide(interp, argv[2], &i) != SAWO_OK ||
        sawo_GetWide(interp, argv[3], &limit) != SAWO_OK ||
          (argc == 6 && sawo_GetWide(interp, argv[4], &incr) != SAWO_OK)) {
        return SAWO_ERR;
    }
    bodyObjPtr = (argc == 5) ? argv[4] : argv[5];

    retval = sawo_SetVariable(interp, argv[1], argv[2]);

    while (((i < limit && incr > 0) || (i > limit && incr < 0)) && retval == SAWO_OK) {
        retval = sawo_EvalObj(interp, bodyObjPtr);
        if (retval == SAWO_OK || retval == SAWO_CONTINUE) {
            sawo_Obj *objPtr = sawo_GetVariable(interp, argv[1], SAWO_ERRMSG);

            retval = SAWO_OK;

            
            i += incr;

            if (objPtr && !sawo_IsShared(objPtr) && objPtr->typePtr == &intObjType) {
                if (argv[1]->typePtr != &variableObjType) {
                    if (sawo_SetVariable(interp, argv[1], objPtr) != SAWO_OK) {
                        return SAWO_ERR;
                    }
                }
                sawoWideValue(objPtr) = i;
                sawo_InvalidateStringRep(objPtr);

                if (argv[1]->typePtr != &variableObjType) {
                    if (sawo_SetVariable(interp, argv[1], objPtr) != SAWO_OK) {
                        retval = SAWO_ERR;
                        break;
                    }
                }
            }
            else {
                objPtr = sawo_NewIntObj(interp, i);
                retval = sawo_SetVariable(interp, argv[1], objPtr);
                if (retval != SAWO_OK) {
                    sawo_FreeNewObj(interp, objPtr);
                }
            }
        }
    }

    if (retval == SAWO_OK || retval == SAWO_CONTINUE || retval == SAWO_BREAK) {
        sawo_SetEmptyResult(interp);
        return SAWO_OK;
    }
    return retval;
}

typedef struct {
    sawo_Obj *objPtr;
    int idx;
} sawo_ListIter;

static void sawoListIterInit(sawo_ListIter *iter, sawo_Obj *objPtr)
{
    iter->objPtr = objPtr;
    iter->idx = 0;
}

static sawo_Obj *sawoListIterNext(sawo_Interp *interp, sawo_ListIter *iter)
{
    if (iter->idx >= sawo_ListLength(interp, iter->objPtr)) {
        return NULL;
    }
    return iter->objPtr->internalRep.listValue.ele[iter->idx++];
}

static int sawoListIterDone(sawo_Interp *interp, sawo_ListIter *iter)
{
    return iter->idx >= sawo_ListLength(interp, iter->objPtr);
}


static int sawoForeachMapHelper(sawo_Interp *interp, int argc, sawo_Obj *const *argv, int doMap)
{
    int result = SAWO_OK;
    int i, numargs;
    sawo_ListIter twoiters[2];   
    sawo_ListIter *iters;
    sawo_Obj *script;
    sawo_Obj *resultObj;

    if (argc < 4 || argc % 2 != 0) {
        sawo_WrongNumArgs(interp, 1, argv, "varList list ?varList list ...? script");
        return SAWO_ERR;
    }
    script = argv[argc - 1];    
    numargs = (argc - 1 - 1);    

    if (numargs == 2) {
        iters = twoiters;
    }
    else {
        iters = sawo_Alloc(numargs * sizeof(*iters));
    }
    for (i = 0; i < numargs; i++) {
        sawoListIterInit(&iters[i], argv[i + 1]);
        if (i % 2 == 0 && sawoListIterDone(interp, &iters[i])) {
            result = SAWO_ERR;
        }
    }
    if (result != SAWO_OK) {
        sawo_SetResultString(interp, "foreach varlist is empty", -1);
        return result;
    }

    if (doMap) {
        resultObj = sawo_NewListObj(interp, NULL, 0);
    }
    else {
        resultObj = interp->emptyObj;
    }
    sawo_IncrRefCount(resultObj);

    while (1) {
        
        for (i = 0; i < numargs; i += 2) {
            if (!sawoListIterDone(interp, &iters[i + 1])) {
                break;
            }
        }
        if (i == numargs) {
            
            break;
        }

        
        for (i = 0; i < numargs; i += 2) {
            sawo_Obj *varName;

            
            sawoListIterInit(&iters[i], argv[i + 1]);
            while ((varName = sawoListIterNext(interp, &iters[i])) != NULL) {
                sawo_Obj *valObj = sawoListIterNext(interp, &iters[i + 1]);
                if (!valObj) {
                    
                    valObj = interp->emptyObj;
                }
                
                sawo_IncrRefCount(valObj);
                result = sawo_SetVariable(interp, varName, valObj);
                sawo_DecrRefCount(interp, valObj);
                if (result != SAWO_OK) {
                    goto err;
                }
            }
        }
        switch (result = sawo_EvalObj(interp, script)) {
            case SAWO_OK:
                if (doMap) {
                    sawo_ListAppendElement(interp, resultObj, interp->result);
                }
                break;
            case SAWO_CONTINUE:
                break;
            case SAWO_BREAK:
                goto out;
            default:
                goto err;
        }
    }
  out:
    result = SAWO_OK;
    sawo_SetResult(interp, resultObj);
  err:
    sawo_DecrRefCount(interp, resultObj);
    if (numargs > 2) {
        sawo_Free(iters);
    }
    return result;
}


static int sawo_ForeachCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoForeachMapHelper(interp, argc, argv, 0);
}


static int sawo_LmapCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    return sawoForeachMapHelper(interp, argc, argv, 1);
}


static int sawo_LassignCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int result = SAWO_ERR;
    int i;
    sawo_ListIter iter;
    sawo_Obj *resultObj;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "varList list ?varName ...?");
        return SAWO_ERR;
    }

    sawoListIterInit(&iter, argv[1]);

    for (i = 2; i < argc; i++) {
        sawo_Obj *valObj = sawoListIterNext(interp, &iter);
        result = sawo_SetVariable(interp, argv[i], valObj ? valObj : interp->emptyObj);
        if (result != SAWO_OK) {
            return result;
        }
    }

    resultObj = sawo_NewListObj(interp, NULL, 0);
    while (!sawoListIterDone(interp, &iter)) {
        sawo_ListAppendElement(interp, resultObj, sawoListIterNext(interp, &iter));
    }

    sawo_SetResult(interp, resultObj);

    return SAWO_OK;
}


static int sawo_IfCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int boolean, retval, current = 1, falsebody = 0;

    if (argc >= 3) {
        while (1) {
            
            if (current >= argc)
                goto err;
            if ((retval = sawo_GetBoolFromExpr(interp, argv[current++], &boolean))
                != SAWO_OK)
                return retval;
            
            if (current >= argc)
                goto err;
            if (sawo_CompareStringImmediate(interp, argv[current], "then"))
                current++;
            
            if (current >= argc)
                goto err;
            if (boolean)
                return sawo_EvalObj(interp, argv[current]);
            
            if (++current >= argc) {
                sawo_SetResult(interp, sawo_NewEmptyStringObj(interp));
                return SAWO_OK;
            }
            falsebody = current++;
            if (sawo_CompareStringImmediate(interp, argv[falsebody], "else")) {
                
                if (current != argc - 1)
                    goto err;
                return sawo_EvalObj(interp, argv[current]);
            }
            else if (sawo_CompareStringImmediate(interp, argv[falsebody], "elseif"))
                continue;
            
            else if (falsebody != argc - 1)
                goto err;
            return sawo_EvalObj(interp, argv[falsebody]);
        }
        return SAWO_OK;
    }
  err:
    sawo_WrongNumArgs(interp, 1, argv, "condition ?then? trueBody ?elseif ...? ?else? falseBody");
    return SAWO_ERR;
}



int sawo_CommandMatchObj(sawo_Interp *interp, sawo_Obj *commandObj, sawo_Obj *patternObj,
    sawo_Obj *stringObj, int nocase)
{
    sawo_Obj *parms[4];
    int argc = 0;
    long eq;
    int rc;

    parms[argc++] = commandObj;
    if (nocase) {
        parms[argc++] = sawo_NewStringObj(interp, "-nocase", -1);
    }
    parms[argc++] = patternObj;
    parms[argc++] = stringObj;

    rc = sawo_EvalObjVector(interp, argc, parms);

    if (rc != SAWO_OK || sawo_GetLong(interp, sawo_GetResult(interp), &eq) != SAWO_OK) {
        eq = -rc;
    }

    return eq;
}

enum
{ SWITCH_EXACT, SWITCH_GLOB, SWITCH_RE, SWITCH_CMD };


static int sawo_SwitchCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int matchOpt = SWITCH_EXACT, opt = 1, patCount, i;
    sawo_Obj *command = 0, *const *caseList = 0, *strObj;
    sawo_Obj *script = 0;

    if (argc < 3) {
      wrongnumargs:
        sawo_WrongNumArgs(interp, 1, argv, "?options? string "
            "pattern body ... ?default body?   or   " "{pattern body ?pattern body ...?}");
        return SAWO_ERR;
    }
    for (opt = 1; opt < argc; ++opt) {
        const char *option = sawo_String(argv[opt]);

        if (*option != '-')
            break;
        else if (strncmp(option, "--", 2) == 0) {
            ++opt;
            break;
        }
        else if (strncmp(option, "-exact", 2) == 0)
            matchOpt = SWITCH_EXACT;
        else if (strncmp(option, "-glob", 2) == 0)
            matchOpt = SWITCH_GLOB;
        else if (strncmp(option, "-regexp", 2) == 0)
            matchOpt = SWITCH_RE;
        else if (strncmp(option, "-command", 2) == 0) {
            matchOpt = SWITCH_CMD;
            if ((argc - opt) < 2)
                goto wrongnumargs;
            command = argv[++opt];
        }
        else {
            sawo_SetResultFormatted(interp,
                "bad option \"%#s\": must be -exact, -glob, -regexp, -command raisename or --",
                argv[opt]);
            return SAWO_ERR;
        }
        if ((argc - opt) < 2)
            goto wrongnumargs;
    }
    strObj = argv[opt++];
    patCount = argc - opt;
    if (patCount == 1) {
        sawo_Obj **vector;

        sawoListGetElements(interp, argv[opt], &patCount, &vector);
        caseList = vector;
    }
    else
        caseList = &argv[opt];
    if (patCount == 0 || patCount % 2 != 0)
        goto wrongnumargs;
    for (i = 0; script == 0 && i < patCount; i += 2) {
        sawo_Obj *patObj = caseList[i];

        if (!sawo_CompareStringImmediate(interp, patObj, "default")
            || i < (patCount - 2)) {
            switch (matchOpt) {
                case SWITCH_EXACT:
                    if (sawo_StringEqObj(strObj, patObj))
                        script = caseList[i + 1];
                    break;
                case SWITCH_GLOB:
                    if (sawo_StringMatchObj(interp, patObj, strObj, 0))
                        script = caseList[i + 1];
                    break;
                case SWITCH_RE:
                    command = sawo_NewStringObj(interp, "regexp", -1);
                    
                case SWITCH_CMD:{
                        int rc = sawo_CommandMatchObj(interp, command, patObj, strObj, 0);

                        if (argc - opt == 1) {
                            sawo_Obj **vector;

                            sawoListGetElements(interp, argv[opt], &patCount, &vector);
                            caseList = vector;
                        }
                        
                        if (rc < 0) {
                            return -rc;
                        }
                        if (rc)
                            script = caseList[i + 1];
                        break;
                    }
            }
        }
        else {
            script = caseList[i + 1];
        }
    }
    for (; i < patCount && sawo_CompareStringImmediate(interp, script, "-"); i += 2)
        script = caseList[i + 1];
    if (script && sawo_CompareStringImmediate(interp, script, "-")) {
        sawo_SetResultFormatted(interp, "no body specified for pattern \"%#s\"", caseList[i - 2]);
        return SAWO_ERR;
    }
    sawo_SetEmptyResult(interp);
    if (script) {
        return sawo_EvalObj(interp, script);
    }
    return SAWO_OK;
}


static int sawo_ListCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *listObjPtr;

    listObjPtr = sawo_NewListObj(interp, argv + 1, argc - 1);
    sawo_SetResult(interp, listObjPtr);
    return SAWO_OK;
}


static int sawo_LindexCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr, *listObjPtr;
    int i;
    int idx;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "list ?index ...?");
        return SAWO_ERR;
    }
    objPtr = argv[1];
    sawo_IncrRefCount(objPtr);
    for (i = 2; i < argc; i++) {
        listObjPtr = objPtr;
        if (sawo_GetIndex(interp, argv[i], &idx) != SAWO_OK) {
            sawo_DecrRefCount(interp, listObjPtr);
            return SAWO_ERR;
        }
        if (sawo_ListIndex(interp, listObjPtr, idx, &objPtr, SAWO_NONE) != SAWO_OK) {
            sawo_DecrRefCount(interp, listObjPtr);
            sawo_SetEmptyResult(interp);
            return SAWO_OK;
        }
        sawo_IncrRefCount(objPtr);
        sawo_DecrRefCount(interp, listObjPtr);
    }
    sawo_SetResult(interp, objPtr);
    sawo_DecrRefCount(interp, objPtr);
    return SAWO_OK;
}


static int sawo_LlengthCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "list");
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sawo_ListLength(interp, argv[1]));
    return SAWO_OK;
}


static int sawo_LsearchCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    static const char * const options[] = {
        "-bool", "-not", "-nocase", "-exact", "-glob", "-regexp", "-all", "-inline", "-command",
            NULL
    };
    enum
    { OPT_BOOL, OPT_NOT, OPT_NOCASE, OPT_EXACT, OPT_GLOB, OPT_REGEXP, OPT_ALL, OPT_INLINE,
            OPT_COMMAND };
    int i;
    int opt_bool = 0;
    int opt_not = 0;
    int opt_nocase = 0;
    int opt_all = 0;
    int opt_inline = 0;
    int opt_match = OPT_EXACT;
    int listlen;
    int rc = SAWO_OK;
    sawo_Obj *listObjPtr = NULL;
    sawo_Obj *commandObj = NULL;

    if (argc < 3) {
      wrongargs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-exact|-glob|-regexp|-command 'command'? ?-bool|-inline? ?-not? ?-nocase? ?-all? list value");
        return SAWO_ERR;
    }

    for (i = 1; i < argc - 2; i++) {
        int option;

        if (sawo_GetEnum(interp, argv[i], options, &option, NULL, SAWO_ERRMSG) != SAWO_OK) {
            return SAWO_ERR;
        }
        switch (option) {
            case OPT_BOOL:
                opt_bool = 1;
                opt_inline = 0;
                break;
            case OPT_NOT:
                opt_not = 1;
                break;
            case OPT_NOCASE:
                opt_nocase = 1;
                break;
            case OPT_INLINE:
                opt_inline = 1;
                opt_bool = 0;
                break;
            case OPT_ALL:
                opt_all = 1;
                break;
            case OPT_COMMAND:
                if (i >= argc - 2) {
                    goto wrongargs;
                }
                commandObj = argv[++i];
                
            case OPT_EXACT:
            case OPT_GLOB:
            case OPT_REGEXP:
                opt_match = option;
                break;
        }
    }

    argv += i;

    if (opt_all) {
        listObjPtr = sawo_NewListObj(interp, NULL, 0);
    }
    if (opt_match == OPT_REGEXP) {
        commandObj = sawo_NewStringObj(interp, "regexp", -1);
    }
    if (commandObj) {
        sawo_IncrRefCount(commandObj);
    }

    listlen = sawo_ListLength(interp, argv[0]);
    for (i = 0; i < listlen; i++) {
        int eq = 0;
        sawo_Obj *objPtr = sawo_ListGetIndex(interp, argv[0], i);

        switch (opt_match) {
            case OPT_EXACT:
                eq = sawo_StringCompareObj(interp, argv[1], objPtr, opt_nocase) == 0;
                break;

            case OPT_GLOB:
                eq = sawo_StringMatchObj(interp, argv[1], objPtr, opt_nocase);
                break;

            case OPT_REGEXP:
            case OPT_COMMAND:
                eq = sawo_CommandMatchObj(interp, commandObj, argv[1], objPtr, opt_nocase);
                if (eq < 0) {
                    if (listObjPtr) {
                        sawo_FreeNewObj(interp, listObjPtr);
                    }
                    rc = SAWO_ERR;
                    goto done;
                }
                break;
        }

        
        if (!eq && opt_bool && opt_not && !opt_all) {
            continue;
        }

        if ((!opt_bool && eq == !opt_not) || (opt_bool && (eq || opt_all))) {
            
            sawo_Obj *resultObj;

            if (opt_bool) {
                resultObj = sawo_NewIntObj(interp, eq ^ opt_not);
            }
            else if (!opt_inline) {
                resultObj = sawo_NewIntObj(interp, i);
            }
            else {
                resultObj = objPtr;
            }

            if (opt_all) {
                sawo_ListAppendElement(interp, listObjPtr, resultObj);
            }
            else {
                sawo_SetResult(interp, resultObj);
                goto done;
            }
        }
    }

    if (opt_all) {
        sawo_SetResult(interp, listObjPtr);
    }
    else {
        
        if (opt_bool) {
            sawo_SetResultBool(interp, opt_not);
        }
        else if (!opt_inline) {
            sawo_SetResultInt(interp, -1);
        }
    }

  done:
    if (commandObj) {
        sawo_DecrRefCount(interp, commandObj);
    }
    return rc;
}


static int sawo_LappendCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *listObjPtr;
    int new_obj = 0;
    int i;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?value value ...?");
        return SAWO_ERR;
    }
    listObjPtr = sawo_GetVariable(interp, argv[1], SAWO_UNSHARED);
    if (!listObjPtr) {
        
        listObjPtr = sawo_NewListObj(interp, NULL, 0);
        new_obj = 1;
    }
    else if (sawo_IsShared(listObjPtr)) {
        listObjPtr = sawo_DuplicateObj(interp, listObjPtr);
        new_obj = 1;
    }
    for (i = 2; i < argc; i++)
        sawo_ListAppendElement(interp, listObjPtr, argv[i]);
    if (sawo_SetVariable(interp, argv[1], listObjPtr) != SAWO_OK) {
        if (new_obj)
            sawo_FreeNewObj(interp, listObjPtr);
        return SAWO_ERR;
    }
    sawo_SetResult(interp, listObjPtr);
    return SAWO_OK;
}


static int sawo_LinsertCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int idx, len;
    sawo_Obj *listPtr;

    if (argc < 3) {
        sawo_WrongNumArgs(interp, 1, argv, "list index ?element ...?");
        return SAWO_ERR;
    }
    listPtr = argv[1];
    if (sawo_IsShared(listPtr))
        listPtr = sawo_DuplicateObj(interp, listPtr);
    if (sawo_GetIndex(interp, argv[2], &idx) != SAWO_OK)
        goto err;
    len = sawo_ListLength(interp, listPtr);
    if (idx >= len)
        idx = len;
    else if (idx < 0)
        idx = len + idx + 1;
    sawo_ListInsertElements(interp, listPtr, idx, argc - 3, &argv[3]);
    sawo_SetResult(interp, listPtr);
    return SAWO_OK;
  err:
    if (listPtr != argv[1]) {
        sawo_FreeNewObj(interp, listPtr);
    }
    return SAWO_ERR;
}


static int sawo_LreplaceCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int first, last, len, rangeLen;
    sawo_Obj *listObj;
    sawo_Obj *newListObj;

    if (argc < 4) {
        sawo_WrongNumArgs(interp, 1, argv, "list first last ?element ...?");
        return SAWO_ERR;
    }
    if (sawo_GetIndex(interp, argv[2], &first) != SAWO_OK ||
        sawo_GetIndex(interp, argv[3], &last) != SAWO_OK) {
        return SAWO_ERR;
    }

    listObj = argv[1];
    len = sawo_ListLength(interp, listObj);

    first = sawoRelToAbsIndex(len, first);
    last = sawoRelToAbsIndex(len, last);
    sawoRelToAbsRange(len, &first, &last, &rangeLen);


    
    if (first < len) {
        
    }
    else if (len == 0) {
        
        first = 0;
    }
    else {
        sawo_SetResultString(interp, "list doesn't contain element ", -1);
        sawo_AppendObj(interp, sawo_GetResult(interp), argv[2]);
        return SAWO_ERR;
    }

    
    newListObj = sawo_NewListObj(interp, listObj->internalRep.listValue.ele, first);

    
    ListInsertElements(newListObj, -1, argc - 4, argv + 4);

    
    ListInsertElements(newListObj, -1, len - first - rangeLen, listObj->internalRep.listValue.ele + first + rangeLen);

    sawo_SetResult(interp, newListObj);
    return SAWO_OK;
}


static int sawo_LsetCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc < 3) {
        sawo_WrongNumArgs(interp, 1, argv, "listVar ?index...? newVal");
        return SAWO_ERR;
    }
    else if (argc == 3) {
        
        if (sawo_SetVariable(interp, argv[1], argv[2]) != SAWO_OK)
            return SAWO_ERR;
        sawo_SetResult(interp, argv[2]);
        return SAWO_OK;
    }
    return sawo_ListSetIndex(interp, argv[1], argv + 2, argc - 3, argv[argc - 1]);
}


static int sawo_LsortCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const argv[])
{
    static const char * const options[] = {
        "-ascii", "-nocase", "-increasing", "-decreasing", "-command", "-integer", "-real", "-index", "-unique", NULL
    };
    enum
    { OPT_ASCII, OPT_NOCASE, OPT_INCREASING, OPT_DECREASING, OPT_COMMAND, OPT_INTEGER, OPT_REAL, OPT_INDEX, OPT_UNIQUE };
    sawo_Obj *resObj;
    int i;
    int retCode;

    struct lsort_info show;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "?options? list");
        return SAWO_ERR;
    }

    show.type = SAWO_LSORT_ASCII;
    show.order = 1;
    show.indexed = 0;
    show.unique = 0;
    show.command = NULL;
    show.interp = interp;

    for (i = 1; i < (argc - 1); i++) {
        int option;

        if (sawo_GetEnum(interp, argv[i], options, &option, NULL, SAWO_ENUM_ABBREV | SAWO_ERRMSG)
            != SAWO_OK)
            return SAWO_ERR;
        switch (option) {
            case OPT_ASCII:
                show.type = SAWO_LSORT_ASCII;
                break;
            case OPT_NOCASE:
                show.type = SAWO_LSORT_NOCASE;
                break;
            case OPT_INTEGER:
                show.type = SAWO_LSORT_INTEGER;
                break;
            case OPT_REAL:
                show.type = SAWO_LSORT_REAL;
                break;
            case OPT_INCREASING:
                show.order = 1;
                break;
            case OPT_DECREASING:
                show.order = -1;
                break;
            case OPT_UNIQUE:
                show.unique = 1;
                break;
            case OPT_COMMAND:
                if (i >= (argc - 2)) {
                    sawo_SetResultString(interp, "\"-command\" option must be followed by comparison command", -1);
                    return SAWO_ERR;
                }
                show.type = SAWO_LSORT_COMMAND;
                show.command = argv[i + 1];
                i++;
                break;
            case OPT_INDEX:
                if (i >= (argc - 2)) {
                    sawo_SetResultString(interp, "\"-index\" option must be followed by list index", -1);
                    return SAWO_ERR;
                }
                if (sawo_GetIndex(interp, argv[i + 1], &show.index) != SAWO_OK) {
                    return SAWO_ERR;
                }
                show.indexed = 1;
                i++;
                break;
        }
    }
    resObj = sawo_DuplicateObj(interp, argv[argc - 1]);
    retCode = ListSortElements(interp, resObj, &show);
    if (retCode == SAWO_OK) {
        sawo_SetResult(interp, resObj);
    }
    else {
        sawo_FreeNewObj(interp, resObj);
    }
    return retCode;
}


static int sawo_AppendCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *stringObjPtr;
    int i;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?value ...?");
        return SAWO_ERR;
    }
    if (argc == 2) {
        stringObjPtr = sawo_GetVariable(interp, argv[1], SAWO_ERRMSG);
        if (!stringObjPtr)
            return SAWO_ERR;
    }
    else {
        int new_obj = 0;
        stringObjPtr = sawo_GetVariable(interp, argv[1], SAWO_UNSHARED);
        if (!stringObjPtr) {
            
            stringObjPtr = sawo_NewEmptyStringObj(interp);
            new_obj = 1;
        }
        else if (sawo_IsShared(stringObjPtr)) {
            new_obj = 1;
            stringObjPtr = sawo_DuplicateObj(interp, stringObjPtr);
        }
        for (i = 2; i < argc; i++) {
            sawo_AppendObj(interp, stringObjPtr, argv[i]);
        }
        if (sawo_SetVariable(interp, argv[1], stringObjPtr) != SAWO_OK) {
            if (new_obj) {
                sawo_FreeNewObj(interp, stringObjPtr);
            }
            return SAWO_ERR;
        }
    }
    sawo_SetResult(interp, stringObjPtr);
    return SAWO_OK;
}


static int sawo_DebugCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
#if !defined(SAWO_DEBUG_COMMAND)
    sawo_SetResultString(interp, "unsupported", -1);
    return SAWO_ERR;
#endif
}


static int sawo_EvalCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int rc;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "arg ?arg ...?");
        return SAWO_ERR;
    }

    if (argc == 2) {
        rc = sawo_EvalObj(interp, argv[1]);
    }
    else {
        rc = sawo_EvalObj(interp, sawo_ConcatObj(interp, argc - 1, argv + 1));
    }

    if (rc == SAWO_ERR) {
        
        interp->addStackTrace++;
    }
    return rc;
}


static int sawo_UplevelCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc >= 2) {
        int retcode;
        sawo_CallFrame *savedCallFrame, *targetCallFrame;
        const char *str;

        
        savedCallFrame = interp->framePtr;

        
        str = sawo_String(argv[1]);
        if ((str[0] >= '0' && str[0] <= '9') || str[0] == '#') {
            targetCallFrame = sawo_GetCallFrameByLevel(interp, argv[1]);
            argc--;
            argv++;
        }
        else {
            targetCallFrame = sawo_GetCallFrameByLevel(interp, NULL);
        }
        if (targetCallFrame == NULL) {
            return SAWO_ERR;
        }
        if (argc < 2) {
            sawo_WrongNumArgs(interp, 1, argv - 1, "?level? command ?arg ...?");
            return SAWO_ERR;
        }
        
        interp->framePtr = targetCallFrame;
        if (argc == 2) {
            retcode = sawo_EvalObj(interp, argv[1]);
        }
        else {
            retcode = sawo_EvalObj(interp, sawo_ConcatObj(interp, argc - 1, argv + 1));
        }
        interp->framePtr = savedCallFrame;
        return retcode;
    }
    else {
        sawo_WrongNumArgs(interp, 1, argv, "?level? command ?arg ...?");
        return SAWO_ERR;
    }
}


static int sawo_ExprCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *exprResultPtr;
    int retcode;

    if (argc == 2) {
        retcode = sawo_EvalExpression(interp, argv[1], &exprResultPtr);
    }
    else if (argc > 2) {
        sawo_Obj *objPtr;

        objPtr = sawo_ConcatObj(interp, argc - 1, argv + 1);
        sawo_IncrRefCount(objPtr);
        retcode = sawo_EvalExpression(interp, objPtr, &exprResultPtr);
        sawo_DecrRefCount(interp, objPtr);
    }
    else {
        sawo_WrongNumArgs(interp, 1, argv, "expression ?...?");
        return SAWO_ERR;
    }
    if (retcode != SAWO_OK)
        return retcode;
    sawo_SetResult(interp, exprResultPtr);
    sawo_DecrRefCount(interp, exprResultPtr);
    return SAWO_OK;
}


static int sawo_BreakCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 1) {
        sawo_WrongNumArgs(interp, 1, argv, "");
        return SAWO_ERR;
    }
    return SAWO_BREAK;
}


static int sawo_ContinueCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 1) {
        sawo_WrongNumArgs(interp, 1, argv, "");
        return SAWO_ERR;
    }
    return SAWO_CONTINUE;
}


static int sawo_ReturnCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    sawo_Obj *stackTraceObj = NULL;
    sawo_Obj *errorCodeObj = NULL;
    int returnCode = SAWO_OK;
    long level = 1;

    for (i = 1; i < argc - 1; i += 2) {
        if (sawo_CompareStringImmediate(interp, argv[i], "-code")) {
            if (sawo_GetReturnCode(interp, argv[i + 1], &returnCode) == SAWO_ERR) {
                return SAWO_ERR;
            }
        }
        else if (sawo_CompareStringImmediate(interp, argv[i], "-errorinfo")) {
            stackTraceObj = argv[i + 1];
        }
        else if (sawo_CompareStringImmediate(interp, argv[i], "-errorcode")) {
            errorCodeObj = argv[i + 1];
        }
        else if (sawo_CompareStringImmediate(interp, argv[i], "-level")) {
            if (sawo_GetLong(interp, argv[i + 1], &level) != SAWO_OK || level < 0) {
                sawo_SetResultFormatted(interp, "bad level \"%#s\"", argv[i + 1]);
                return SAWO_ERR;
            }
        }
        else {
            break;
        }
    }

    if (i != argc - 1 && i != argc) {
        sawo_WrongNumArgs(interp, 1, argv,
            "?-code code? ?-errorinfo stacktrace? ?-level level? ?result?");
    }

    
    if (stackTraceObj && returnCode == SAWO_ERR) {
        sawoSetStackTrace(interp, stackTraceObj);
    }
    
    if (errorCodeObj && returnCode == SAWO_ERR) {
        sawo_SetGlobalVariableStr(interp, "errorCode", errorCodeObj);
    }
    interp->returnCode = returnCode;
    interp->returnLevel = level;

    if (i == argc - 1) {
        sawo_SetResult(interp, argv[i]);
    }
    return SAWO_RETURN;
}


static int sawo_TailcallCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (interp->framePtr->level == 0) {
        sawo_SetResultString(interp, "tailcall can only be called from a raise or lambda", -1);
        return SAWO_ERR;
    }
    else if (argc >= 2) {
        
        sawo_CallFrame *cf = interp->framePtr->parent;

        sawo_Cmd *cmdPtr = sawo_GetCommand(interp, argv[1], SAWO_ERRMSG);
        if (cmdPtr == NULL) {
            return SAWO_ERR;
        }

        sawoPanic((cf->tailcallCmd != NULL, "Already have a tailcallCmd"));

        
        sawoIncrCmdRefCount(cmdPtr);
        cf->tailcallCmd = cmdPtr;

        
        sawoPanic((cf->tailcallObj != NULL, "Already have a tailcallobj"));

        cf->tailcallObj = sawo_NewListObj(interp, argv + 1, argc - 1);
        sawo_IncrRefCount(cf->tailcallObj);

        
        return SAWO_EVAL;
    }
    return SAWO_OK;
}

static int sawoAliasCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *cmdList;
    sawo_Obj *prefixListObj = sawo_CmdPrivData(interp);

    
    cmdList = sawo_DuplicateObj(interp, prefixListObj);
    sawo_ListInsertElements(interp, cmdList, sawo_ListLength(interp, cmdList), argc - 1, argv + 1);

    return sawoEvalObjList(interp, cmdList);
}

static void sawoAliasCmdDelete(sawo_Interp *interp, void *privData)
{
    sawo_Obj *prefixListObj = privData;
    sawo_DecrRefCount(interp, prefixListObj);
}

static int sawo_AliasCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *prefixListObj;
    const char *newname;

    if (argc < 3) {
        sawo_WrongNumArgs(interp, 1, argv, "newname command ?args ...?");
        return SAWO_ERR;
    }

    prefixListObj = sawo_NewListObj(interp, argv + 2, argc - 2);
    sawo_IncrRefCount(prefixListObj);
    newname = sawo_String(argv[1]);
    if (newname[0] == ':' && newname[1] == ':') {
        while (*++newname == ':') {
        }
    }

    sawo_SetResult(interp, argv[1]);

    return sawo_CreateCommand(interp, newname, sawoAliasCmd, prefixListObj, sawoAliasCmdDelete);
}


static int sawo_RaiseCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Cmd *cmd;

    if (argc != 4 && argc != 5) {
        sawo_WrongNumArgs(interp, 1, argv, "name arglist ?statics? body");
        return SAWO_ERR;
    }

    if (sawoValidName(interp, "procedure", argv[1]) != SAWO_OK) {
        return SAWO_ERR;
    }

    if (argc == 4) {
        cmd = sawoCreateProcedureCmd(interp, argv[2], NULL, argv[3], NULL);
    }
    else {
        cmd = sawoCreateProcedureCmd(interp, argv[2], argv[3], argv[4], NULL);
    }

    if (cmd) {
        
        sawo_Obj *qualifiedCmdNameObj;
        const char *cmdname = sawoQualifyName(interp, sawo_String(argv[1]), &qualifiedCmdNameObj);

        sawoCreateCommand(interp, cmdname, cmd);

        
        sawoUpdateRaiseNamespace(interp, cmd, cmdname);

        sawoFreeQualifiedName(interp, qualifiedCmdNameObj);

        
        sawo_SetResult(interp, argv[1]);
        return SAWO_OK;
    }
    return SAWO_ERR;
}


static int sawo_LocalCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int retcode;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "cmd ?args ...?");
        return SAWO_ERR;
    }

    
    interp->local++;
    retcode = sawo_EvalObjVector(interp, argc - 1, argv + 1);
    interp->local--;


    
    if (retcode == 0) {
        sawo_Obj *cmdNameObj = sawo_GetResult(interp);

        if (sawo_GetCommand(interp, cmdNameObj, SAWO_ERRMSG) == NULL) {
            return SAWO_ERR;
        }
        if (interp->framePtr->localCommands == NULL) {
            interp->framePtr->localCommands = sawo_Alloc(sizeof(*interp->framePtr->localCommands));
            sawo_InitStack(interp->framePtr->localCommands);
        }
        sawo_IncrRefCount(cmdNameObj);
        sawo_StackPush(interp->framePtr->localCommands, cmdNameObj);
    }

    return retcode;
}


static int sawo_UpcallCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "cmd ?args ...?");
        return SAWO_ERR;
    }
    else {
        int retcode;

        sawo_Cmd *cmdPtr = sawo_GetCommand(interp, argv[1], SAWO_ERRMSG);
        if (cmdPtr == NULL || !cmdPtr->israise || !cmdPtr->prevCmd) {
            sawo_SetResultFormatted(interp, "no previous command: \"%#s\"", argv[1]);
            return SAWO_ERR;
        }
        
        cmdPtr->u.raise.upcall++;
        sawoIncrCmdRefCount(cmdPtr);

        
        retcode = sawo_EvalObjVector(interp, argc - 1, argv + 1);

        
        cmdPtr->u.raise.upcall--;
        sawoDecrCmdRefCount(interp, cmdPtr);

        return retcode;
    }
}


static int sawo_ApplyCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "lambdaExpr ?arg ...?");
        return SAWO_ERR;
    }
    else {
        int ret;
        sawo_Cmd *cmd;
        sawo_Obj *argListObjPtr;
        sawo_Obj *bodyObjPtr;
        sawo_Obj *nsObj = NULL;
        sawo_Obj **nargv;

        int len = sawo_ListLength(interp, argv[1]);
        if (len != 2 && len != 3) {
            sawo_SetResultFormatted(interp, "can't interpret \"%#s\" as a lambda expression", argv[1]);
            return SAWO_ERR;
        }

        if (len == 3) {
#ifdef sawo_ext_namespace
            
            nsObj = sawoQualifyNameObj(interp, sawo_ListGetIndex(interp, argv[1], 2));
#else
            sawo_SetResultString(interp, "namespaces not enabled", -1);
            return SAWO_ERR;
#endif
        }
        argListObjPtr = sawo_ListGetIndex(interp, argv[1], 0);
        bodyObjPtr = sawo_ListGetIndex(interp, argv[1], 1);

        cmd = sawoCreateProcedureCmd(interp, argListObjPtr, NULL, bodyObjPtr, nsObj);

        if (cmd) {
            
            nargv = sawo_Alloc((argc - 2 + 1) * sizeof(*nargv));
            nargv[0] = sawo_NewStringObj(interp, "apply lambdaExpr", -1);
            sawo_IncrRefCount(nargv[0]);
            memcpy(&nargv[1], argv + 2, (argc - 2) * sizeof(*nargv));
            ret = sawoCallProcedure(interp, cmd, argc - 2 + 1, nargv);
            sawo_DecrRefCount(interp, nargv[0]);
            sawo_Free(nargv);

            sawoDecrCmdRefCount(interp, cmd);
            return ret;
        }
        return SAWO_ERR;
    }
}



static int sawo_ConcatCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_SetResult(interp, sawo_ConcatObj(interp, argc - 1, argv + 1));
    return SAWO_OK;
}


static int sawo_UpvarCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    sawo_CallFrame *targetCallFrame;

    
    if (argc > 3 && (argc % 2 == 0)) {
        targetCallFrame = sawo_GetCallFrameByLevel(interp, argv[1]);
        argc--;
        argv++;
    }
    else {
        targetCallFrame = sawo_GetCallFrameByLevel(interp, NULL);
    }
    if (targetCallFrame == NULL) {
        return SAWO_ERR;
    }

    
    if (argc < 3) {
        sawo_WrongNumArgs(interp, 1, argv, "?level? otherVar localVar ?otherVar localVar ...?");
        return SAWO_ERR;
    }

    
    for (i = 1; i < argc; i += 2) {
        if (sawo_SetVariableLink(interp, argv[i + 1], argv[i], targetCallFrame) != SAWO_OK)
            return SAWO_ERR;
    }
    return SAWO_OK;
}


static int sawo_GlobalCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?varName ...?");
        return SAWO_ERR;
    }
    
    if (interp->framePtr->level == 0)
        return SAWO_OK;          
    for (i = 1; i < argc; i++) {
        
        const char *name = sawo_String(argv[i]);
        if (name[0] != ':' || name[1] != ':') {
            if (sawo_SetVariableLink(interp, argv[i], argv[i], interp->topFramePtr) != SAWO_OK)
                return SAWO_ERR;
        }
    }
    return SAWO_OK;
}

static sawo_Obj *sawoStringMap(sawo_Interp *interp, sawo_Obj *mapListObjPtr,
    sawo_Obj *objPtr, int nocase)
{
    int numMaps;
    const char *str, *noMatchStart = NULL;
    int strLen, i;
    sawo_Obj *resultObjPtr;

    numMaps = sawo_ListLength(interp, mapListObjPtr);
    if (numMaps % 2) {
        sawo_SetResultString(interp, "list must contain an even number of elements", -1);
        return NULL;
    }

    str = sawo_String(objPtr);
    strLen = sawo_Utf8Length(interp, objPtr);

    
    resultObjPtr = sawo_NewStringObj(interp, "", 0);
    while (strLen) {
        for (i = 0; i < numMaps; i += 2) {
            sawo_Obj *objPtr;
            const char *k;
            int kl;

            objPtr = sawo_ListGetIndex(interp, mapListObjPtr, i);
            k = sawo_String(objPtr);
            kl = sawo_Utf8Length(interp, objPtr);

            if (strLen >= kl && kl) {
                int rc;
                rc = sawoStringCompareLen(str, k, kl, nocase);
                if (rc == 0) {
                    if (noMatchStart) {
                        sawo_AppendString(interp, resultObjPtr, noMatchStart, str - noMatchStart);
                        noMatchStart = NULL;
                    }
                    sawo_AppendObj(interp, resultObjPtr, sawo_ListGetIndex(interp, mapListObjPtr, i + 1));
                    str += utf8_index(str, kl);
                    strLen -= kl;
                    break;
                }
            }
        }
        if (i == numMaps) {     
            int c;
            if (noMatchStart == NULL)
                noMatchStart = str;
            str += utf8_tounicode(str, &c);
            strLen--;
        }
    }
    if (noMatchStart) {
        sawo_AppendString(interp, resultObjPtr, noMatchStart, str - noMatchStart);
    }
    return resultObjPtr;
}


static int sawo_StringCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int len;
    int opt_case = 1;
    int option;
    static const char * const options[] = {
        "bytelength", "length", "compare", "match", "equal", "is", "byterange", "range", "replace",
        "map", "repeat", "reverse", "index", "first", "last", "cat",
        "trim", "trimleft", "trimright", "tolower", "toupper", "totitle", NULL
    };
    enum
    {
        OPT_BYTELENGTH, OPT_LENGTH, OPT_COMPARE, OPT_MATCH, OPT_EQUAL, OPT_IS, OPT_BYTERANGE, OPT_RANGE, OPT_REPLACE,
        OPT_MAP, OPT_REPEAT, OPT_REVERSE, OPT_INDEX, OPT_FIRST, OPT_LAST, OPT_CAT,
        OPT_TRIM, OPT_TRIMLEFT, OPT_TRIMRIGHT, OPT_TOLOWER, OPT_TOUPPER, OPT_TOTITLE
    };
    static const char * const nocase_options[] = {
        "-nocase", NULL
    };
    static const char * const nocase_length_options[] = {
        "-nocase", "-length", NULL
    };

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "option ?arguments ...?");
        return SAWO_ERR;
    }
    if (sawo_GetEnum(interp, argv[1], options, &option, NULL,
            SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK)
        return SAWO_ERR;

    switch (option) {
        case OPT_LENGTH:
        case OPT_BYTELENGTH:
            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "string");
                return SAWO_ERR;
            }
            if (option == OPT_LENGTH) {
                len = sawo_Utf8Length(interp, argv[2]);
            }
            else {
                len = sawo_Length(argv[2]);
            }
            sawo_SetResultInt(interp, len);
            return SAWO_OK;

        case OPT_CAT:{
                sawo_Obj *objPtr;
                if (argc == 3) {
                    
                    objPtr = argv[2];
                }
                else {
                    int i;

                    objPtr = sawo_NewStringObj(interp, "", 0);

                    for (i = 2; i < argc; i++) {
                        sawo_AppendObj(interp, objPtr, argv[i]);
                    }
                }
                sawo_SetResult(interp, objPtr);
                return SAWO_OK;
            }

        case OPT_COMPARE:
        case OPT_EQUAL:
            {
                
                long opt_length = -1;
                int n = argc - 4;
                int i = 2;
                while (n > 0) {
                    int subopt;
                    if (sawo_GetEnum(interp, argv[i++], nocase_length_options, &subopt, NULL,
                            SAWO_ENUM_ABBREV) != SAWO_OK) {
badcompareargs:
                        sawo_WrongNumArgs(interp, 2, argv, "?-nocase? ?-length int? string1 string2");
                        return SAWO_ERR;
                    }
                    if (subopt == 0) {
                        
                        opt_case = 0;
                        n--;
                    }
                    else {
                        
                        if (n < 2) {
                            goto badcompareargs;
                        }
                        if (sawo_GetLong(interp, argv[i++], &opt_length) != SAWO_OK) {
                            return SAWO_ERR;
                        }
                        n -= 2;
                    }
                }
                if (n) {
                    goto badcompareargs;
                }
                argv += argc - 2;
                if (opt_length < 0 && option != OPT_COMPARE && opt_case) {
                    
                    sawo_SetResultBool(interp, sawo_StringEqObj(argv[0], argv[1]));
                }
                else {
                    if (opt_length >= 0) {
                        n = sawoStringCompareLen(sawo_String(argv[0]), sawo_String(argv[1]), opt_length, !opt_case);
                    }
                    else {
                        n = sawo_StringCompareObj(interp, argv[0], argv[1], !opt_case);
                    }
                    sawo_SetResultInt(interp, option == OPT_COMPARE ? n : n == 0);
                }
                return SAWO_OK;
            }

        case OPT_MATCH:
            if (argc != 4 &&
                (argc != 5 ||
                    sawo_GetEnum(interp, argv[2], nocase_options, &opt_case, NULL,
                        SAWO_ENUM_ABBREV) != SAWO_OK)) {
                sawo_WrongNumArgs(interp, 2, argv, "?-nocase? pattern string");
                return SAWO_ERR;
            }
            if (opt_case == 0) {
                argv++;
            }
            sawo_SetResultBool(interp, sawo_StringMatchObj(interp, argv[2], argv[3], !opt_case));
            return SAWO_OK;

        case OPT_MAP:{
                sawo_Obj *objPtr;

                if (argc != 4 &&
                    (argc != 5 ||
                        sawo_GetEnum(interp, argv[2], nocase_options, &opt_case, NULL,
                            SAWO_ENUM_ABBREV) != SAWO_OK)) {
                    sawo_WrongNumArgs(interp, 2, argv, "?-nocase? mapList string");
                    return SAWO_ERR;
                }

                if (opt_case == 0) {
                    argv++;
                }
                objPtr = sawoStringMap(interp, argv[2], argv[3], !opt_case);
                if (objPtr == NULL) {
                    return SAWO_ERR;
                }
                sawo_SetResult(interp, objPtr);
                return SAWO_OK;
            }

        case OPT_RANGE:
        case OPT_BYTERANGE:{
                sawo_Obj *objPtr;

                if (argc != 5) {
                    sawo_WrongNumArgs(interp, 2, argv, "string first last");
                    return SAWO_ERR;
                }
                if (option == OPT_RANGE) {
                    objPtr = sawo_StringRangeObj(interp, argv[2], argv[3], argv[4]);
                }
                else
                {
                    objPtr = sawo_StringByteRangeObj(interp, argv[2], argv[3], argv[4]);
                }

                if (objPtr == NULL) {
                    return SAWO_ERR;
                }
                sawo_SetResult(interp, objPtr);
                return SAWO_OK;
            }

        case OPT_REPLACE:{
                sawo_Obj *objPtr;

                if (argc != 5 && argc != 6) {
                    sawo_WrongNumArgs(interp, 2, argv, "string first last ?string?");
                    return SAWO_ERR;
                }
                objPtr = sawoStringReplaceObj(interp, argv[2], argv[3], argv[4], argc == 6 ? argv[5] : NULL);
                if (objPtr == NULL) {
                    return SAWO_ERR;
                }
                sawo_SetResult(interp, objPtr);
                return SAWO_OK;
            }


        case OPT_REPEAT:{
                sawo_Obj *objPtr;
                sawo_wide count;

                if (argc != 4) {
                    sawo_WrongNumArgs(interp, 2, argv, "string count");
                    return SAWO_ERR;
                }
                if (sawo_GetWide(interp, argv[3], &count) != SAWO_OK) {
                    return SAWO_ERR;
                }
                objPtr = sawo_NewStringObj(interp, "", 0);
                if (count > 0) {
                    while (count--) {
                        sawo_AppendObj(interp, objPtr, argv[2]);
                    }
                }
                sawo_SetResult(interp, objPtr);
                return SAWO_OK;
            }

        case OPT_REVERSE:{
                char *buf, *p;
                const char *str;
                int len;
                int i;

                if (argc != 3) {
                    sawo_WrongNumArgs(interp, 2, argv, "string");
                    return SAWO_ERR;
                }

                str = sawo_GetString(argv[2], &len);
                buf = sawo_Alloc(len + 1);
                p = buf + len;
                *p = 0;
                for (i = 0; i < len; ) {
                    int c;
                    int l = utf8_tounicode(str, &c);
                    memcpy(p - l, str, l);
                    p -= l;
                    i += l;
                    str += l;
                }
                sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, buf, len));
                return SAWO_OK;
            }

        case OPT_INDEX:{
                int idx;
                const char *str;

                if (argc != 4) {
                    sawo_WrongNumArgs(interp, 2, argv, "string index");
                    return SAWO_ERR;
                }
                if (sawo_GetIndex(interp, argv[3], &idx) != SAWO_OK) {
                    return SAWO_ERR;
                }
                str = sawo_String(argv[2]);
                len = sawo_Utf8Length(interp, argv[2]);
                if (idx != INT_MIN && idx != INT_MAX) {
                    idx = sawoRelToAbsIndex(len, idx);
                }
                if (idx < 0 || idx >= len || str == NULL) {
                    sawo_SetResultString(interp, "", 0);
                }
                else if (len == sawo_Length(argv[2])) {
                    
                    sawo_SetResultString(interp, str + idx, 1);
                }
                else {
                    int c;
                    int i = utf8_index(str, idx);
                    sawo_SetResultString(interp, str + i, utf8_tounicode(str + i, &c));
                }
                return SAWO_OK;
            }

        case OPT_FIRST:
        case OPT_LAST:{
                int idx = 0, l1, l2;
                const char *s1, *s2;

                if (argc != 4 && argc != 5) {
                    sawo_WrongNumArgs(interp, 2, argv, "subString string ?index?");
                    return SAWO_ERR;
                }
                s1 = sawo_String(argv[2]);
                s2 = sawo_String(argv[3]);
                l1 = sawo_Utf8Length(interp, argv[2]);
                l2 = sawo_Utf8Length(interp, argv[3]);
                if (argc == 5) {
                    if (sawo_GetIndex(interp, argv[4], &idx) != SAWO_OK) {
                        return SAWO_ERR;
                    }
                    idx = sawoRelToAbsIndex(l2, idx);
                }
                else if (option == OPT_LAST) {
                    idx = l2;
                }
                if (option == OPT_FIRST) {
                    sawo_SetResultInt(interp, sawoStringFirst(s1, l1, s2, l2, idx));
                }
                else {
#ifdef SAWO_UTF8
                    sawo_SetResultInt(interp, sawoStringLastUtf8(s1, l1, s2, idx));
#else
                    sawo_SetResultInt(interp, sawoStringLast(s1, l1, s2, idx));
#endif
                }
                return SAWO_OK;
            }

        case OPT_TRIM:
        case OPT_TRIMLEFT:
        case OPT_TRIMRIGHT:{
                sawo_Obj *trimchars;

                if (argc != 3 && argc != 4) {
                    sawo_WrongNumArgs(interp, 2, argv, "string ?trimchars?");
                    return SAWO_ERR;
                }
                trimchars = (argc == 4 ? argv[3] : NULL);
                if (option == OPT_TRIM) {
                    sawo_SetResult(interp, sawoStringTrim(interp, argv[2], trimchars));
                }
                else if (option == OPT_TRIMLEFT) {
                    sawo_SetResult(interp, sawoStringTrimLeft(interp, argv[2], trimchars));
                }
                else if (option == OPT_TRIMRIGHT) {
                    sawo_SetResult(interp, sawoStringTrimRight(interp, argv[2], trimchars));
                }
                return SAWO_OK;
            }

        case OPT_TOLOWER:
        case OPT_TOUPPER:
        case OPT_TOTITLE:
            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "string");
                return SAWO_ERR;
            }
            if (option == OPT_TOLOWER) {
                sawo_SetResult(interp, sawoStringToLower(interp, argv[2]));
            }
            else if (option == OPT_TOUPPER) {
                sawo_SetResult(interp, sawoStringToUpper(interp, argv[2]));
            }
            else {
                sawo_SetResult(interp, sawoStringToTitle(interp, argv[2]));
            }
            return SAWO_OK;

        case OPT_IS:
            if (argc == 4 || (argc == 5 && sawo_CompareStringImmediate(interp, argv[3], "-strict"))) {
                return sawoStringIs(interp, argv[argc - 1], argv[2], argc == 5);
            }
            sawo_WrongNumArgs(interp, 2, argv, "class ?-strict? str");
            return SAWO_ERR;
    }
    return SAWO_OK;
}


static int sawo_TimeCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    long i, count = 1;
    sawo_wide start, elapsed;
    char buf[60];
    const char *fmt = "%" SAWO_WIDE_MODIFIER " microseconds per iteration";

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "script ?count?");
        return SAWO_ERR;
    }
    if (argc == 3) {
        if (sawo_GetLong(interp, argv[2], &count) != SAWO_OK)
            return SAWO_ERR;
    }
    if (count < 0)
        return SAWO_OK;
    i = count;
    start = sawoClock();
    while (i-- > 0) {
        int retval;

        retval = sawo_EvalObj(interp, argv[1]);
        if (retval != SAWO_OK) {
            return retval;
        }
    }
    elapsed = sawoClock() - start;
    sprintf(buf, fmt, count == 0 ? 0 : elapsed / count);
    sawo_SetResultString(interp, buf, -1);
    return SAWO_OK;
}


static int sawo_ExitCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    long exitCode = 0;

    if (argc > 2) {
        sawo_WrongNumArgs(interp, 1, argv, "?exitCode?");
        return SAWO_ERR;
    }
    if (argc == 2) {
        if (sawo_GetLong(interp, argv[1], &exitCode) != SAWO_OK)
            return SAWO_ERR;
    }
    interp->exitCode = exitCode;
    return SAWO_EXIT;
}


static int sawo_CatchCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int exitCode = 0;
    int i;
    int sig = 0;

    
    sawo_wide ignore_mask = (1 << SAWO_EXIT) | (1 << SAWO_EVAL) | (1 << SAWO_SIGNAL);
    static const int max_ignore_code = sizeof(ignore_mask) * 8;

    sawo_SetGlobalVariableStr(interp, "errorCode", sawo_NewStringObj(interp, "NONE", -1));

    for (i = 1; i < argc - 1; i++) {
        const char *arg = sawo_String(argv[i]);
        sawo_wide option;
        int ignore;

        
        if (strcmp(arg, "--") == 0) {
            i++;
            break;
        }
        if (*arg != '-') {
            break;
        }

        if (strncmp(arg, "-no", 3) == 0) {
            arg += 3;
            ignore = 1;
        }
        else {
            arg++;
            ignore = 0;
        }

        if (sawo_StringToWide(arg, &option, 10) != SAWO_OK) {
            option = -1;
        }
        if (option < 0) {
            option = sawo_FindByName(arg, sawoReturnCodes, sawoReturnCodesSize);
        }
        if (option < 0) {
            goto wrongargs;
        }

        if (ignore) {
            ignore_mask |= ((sawo_wide)1 << option);
        }
        else {
            ignore_mask &= (~((sawo_wide)1 << option));
        }
    }

    argc -= i;
    if (argc < 1 || argc > 3) {
      wrongargs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-?no?code ... --? script ?resultVarName? ?optionVarName?");
        return SAWO_ERR;
    }
    argv += i;

    if ((ignore_mask & (1 << SAWO_SIGNAL)) == 0) {
        sig++;
    }

    interp->signal_level += sig;
    if (sawo_CheckSignal(interp)) {
        
        exitCode = SAWO_SIGNAL;
    }
    else {
        exitCode = sawo_EvalObj(interp, argv[0]);
        
        interp->errorFlag = 0;
    }
    interp->signal_level -= sig;

    
    if (exitCode >= 0 && exitCode < max_ignore_code && (((unsigned sawo_wide)1 << exitCode) & ignore_mask)) {
        
        return exitCode;
    }

    if (sig && exitCode == SAWO_SIGNAL) {
        
        if (interp->signal_set_result) {
            interp->signal_set_result(interp, interp->sigmask);
        }
        else {
            sawo_SetResultInt(interp, interp->sigmask);
        }
        interp->sigmask = 0;
    }

    if (argc >= 2) {
        if (sawo_SetVariable(interp, argv[1], sawo_GetResult(interp)) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (argc == 3) {
            sawo_Obj *optListObj = sawo_NewListObj(interp, NULL, 0);

            sawo_ListAppendElement(interp, optListObj, sawo_NewStringObj(interp, "-code", -1));
            sawo_ListAppendElement(interp, optListObj,
                sawo_NewIntObj(interp, exitCode == SAWO_RETURN ? interp->returnCode : exitCode));
            sawo_ListAppendElement(interp, optListObj, sawo_NewStringObj(interp, "-level", -1));
            sawo_ListAppendElement(interp, optListObj, sawo_NewIntObj(interp, interp->returnLevel));
            if (exitCode == SAWO_ERR) {
                sawo_Obj *errorCode;
                sawo_ListAppendElement(interp, optListObj, sawo_NewStringObj(interp, "-errorinfo",
                    -1));
                sawo_ListAppendElement(interp, optListObj, interp->stackTrace);

                errorCode = sawo_GetGlobalVariableStr(interp, "errorCode", SAWO_NONE);
                if (errorCode) {
                    sawo_ListAppendElement(interp, optListObj, sawo_NewStringObj(interp, "-errorcode", -1));
                    sawo_ListAppendElement(interp, optListObj, errorCode);
                }
            }
            if (sawo_SetVariable(interp, argv[2], optListObj) != SAWO_OK) {
                return SAWO_ERR;
            }
        }
    }
    sawo_SetResultInt(interp, exitCode);
    return SAWO_OK;
}

#ifdef SAWO_REFERENCES


static int sawo_RefCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 3 && argc != 4) {
        sawo_WrongNumArgs(interp, 1, argv, "string tag ?finalizer?");
        return SAWO_ERR;
    }
    if (argc == 3) {
        sawo_SetResult(interp, sawo_NewReference(interp, argv[1], argv[2], NULL));
    }
    else {
        sawo_SetResult(interp, sawo_NewReference(interp, argv[1], argv[2], argv[3]));
    }
    return SAWO_OK;
}


static int sawo_GetrefCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Reference *refPtr;

    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "reference");
        return SAWO_ERR;
    }
    if ((refPtr = sawo_GetReference(interp, argv[1])) == NULL)
        return SAWO_ERR;
    sawo_SetResult(interp, refPtr->objPtr);
    return SAWO_OK;
}


static int sawo_SetrefCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Reference *refPtr;

    if (argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "reference newValue");
        return SAWO_ERR;
    }
    if ((refPtr = sawo_GetReference(interp, argv[1])) == NULL)
        return SAWO_ERR;
    sawo_IncrRefCount(argv[2]);
    sawo_DecrRefCount(interp, refPtr->objPtr);
    refPtr->objPtr = argv[2];
    sawo_SetResult(interp, argv[2]);
    return SAWO_OK;
}


static int sawo_CollectCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 1) {
        sawo_WrongNumArgs(interp, 1, argv, "");
        return SAWO_ERR;
    }
    sawo_SetResultInt(interp, sawo_Collect(interp));

    
    while (interp->freeList) {
        sawo_Obj *nextObjPtr = interp->freeList->nextObjPtr;
        sawo_Free(interp->freeList);
        interp->freeList = nextObjPtr;
    }

    return SAWO_OK;
}


static int sawo_FinalizeCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "reference ?finalizerRaise?");
        return SAWO_ERR;
    }
    if (argc == 2) {
        sawo_Obj *cmdNamePtr;

        if (sawo_GetFinalizer(interp, argv[1], &cmdNamePtr) != SAWO_OK)
            return SAWO_ERR;
        if (cmdNamePtr != NULL) 
            sawo_SetResult(interp, cmdNamePtr);
    }
    else {
        if (sawo_SetFinalizer(interp, argv[1], argv[2]) != SAWO_OK)
            return SAWO_ERR;
        sawo_SetResult(interp, argv[2]);
    }
    return SAWO_OK;
}


static int sawoInfoReferences(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *listObjPtr;
    sawo_HashTableIterator htiter;
    sawo_HashEntry *he;

    listObjPtr = sawo_NewListObj(interp, NULL, 0);

    sawoInitHashTableIterator(&interp->references, &htiter);
    while ((he = sawo_NextHashEntry(&htiter)) != NULL) {
        char buf[SAWO_REFERENCE_SPACE + 1];
        sawo_Reference *refPtr = sawo_GetHashEntryVal(he);
        const unsigned long *refId = he->key;

        sawoFormatReference(buf, refPtr, *refId);
        sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp, buf, -1));
    }
    sawo_SetResult(interp, listObjPtr);
    return SAWO_OK;
}
#endif


static int sawo_RenameCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "oldName newName");
        return SAWO_ERR;
    }

    if (sawoValidName(interp, "new procedure", argv[2])) {
        return SAWO_ERR;
    }

    return sawo_RenameCommand(interp, sawo_String(argv[1]), sawo_String(argv[2]));
}

#define SAWO_DICTMATCH_VALUES 0x0001

typedef void sawoDictMatchCallbackType(sawo_Interp *interp, sawo_Obj *listObjPtr, sawo_HashEntry *he, int type);

static void sawoDictMatchKeys(sawo_Interp *interp, sawo_Obj *listObjPtr, sawo_HashEntry *he, int type)
{
    sawo_ListAppendElement(interp, listObjPtr, (sawo_Obj *)he->key);
    if (type & SAWO_DICTMATCH_VALUES) {
        sawo_ListAppendElement(interp, listObjPtr, sawo_GetHashEntryVal(he));
    }
}

static sawo_Obj *sawoDictPatternMatch(sawo_Interp *interp, sawo_HashTable *ht, sawo_Obj *patternObjPtr,
    sawoDictMatchCallbackType *callback, int type)
{
    sawo_HashEntry *he;
    sawo_Obj *listObjPtr = sawo_NewListObj(interp, NULL, 0);

    
    sawo_HashTableIterator htiter;
    sawoInitHashTableIterator(ht, &htiter);
    while ((he = sawo_NextHashEntry(&htiter)) != NULL) {
        if (patternObjPtr == NULL || sawoGlobMatch(sawo_String(patternObjPtr), sawo_String((sawo_Obj *)he->key), 0)) {
            callback(interp, listObjPtr, he, type);
        }
    }

    return listObjPtr;
}


int sawo_DictKeys(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *patternObjPtr)
{
    if (SetDictFromAny(interp, objPtr) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResult(interp, sawoDictPatternMatch(interp, objPtr->internalRep.ptr, patternObjPtr, sawoDictMatchKeys, 0));
    return SAWO_OK;
}

int sawo_DictValues(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *patternObjPtr)
{
    if (SetDictFromAny(interp, objPtr) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResult(interp, sawoDictPatternMatch(interp, objPtr->internalRep.ptr, patternObjPtr, sawoDictMatchKeys, SAWO_DICTMATCH_VALUES));
    return SAWO_OK;
}

int sawo_DictSize(sawo_Interp *interp, sawo_Obj *objPtr)
{
    if (SetDictFromAny(interp, objPtr) != SAWO_OK) {
        return -1;
    }
    return ((sawo_HashTable *)objPtr->internalRep.ptr)->used;
}

int sawo_DictInfo(sawo_Interp *interp, sawo_Obj *objPtr)
{
    sawo_HashTable *ht;
    unsigned int i;

    if (SetDictFromAny(interp, objPtr) != SAWO_OK) {
        return SAWO_ERR;
    }

    ht = (sawo_HashTable *)objPtr->internalRep.ptr;

    
    printf("%d entries in table, %d buckets\n", ht->used, ht->size);

    for (i = 0; i < ht->size; i++) {
        sawo_HashEntry *he = ht->table[i];

        if (he) {
            printf("%d: ", i);

            while (he) {
                printf(" %s", sawo_String(he->key));
                he = he->next;
            }
            printf("\n");
        }
    }
    return SAWO_OK;
}

static int sawo_EvalEnsemble(sawo_Interp *interp, const char *basecmd, const char *subcmd, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *prefixObj = sawo_NewStringObj(interp, basecmd, -1);

    sawo_AppendString(interp, prefixObj, " ", 1);
    sawo_AppendString(interp, prefixObj, subcmd, -1);

    return sawo_EvalObjPrefix(interp, prefixObj, argc, argv);
}


static int sawo_DictCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;
    int option;
    static const char * const options[] = {
        "create", "get", "set", "unset", "exists", "keys", "size", "show",
        "merge", "with", "append", "lappend", "incr", "remove", "values", "for",
        "replace", "update", NULL
    };
    enum
    {
        OPT_CREATE, OPT_GET, OPT_SET, OPT_UNSET, OPT_EXISTS, OPT_KEYS, OPT_SIZE, OPT_INFO,
        OPT_MERGE, OPT_WITH, OPT_APPEND, OPT_LAPPEND, OPT_INCR, OPT_REMOVE, OPT_VALUES, OPT_FOR,
        OPT_REPLACE, OPT_UPDATE,
    };

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "subcommand ?arguments ...?");
        return SAWO_ERR;
    }

    if (sawo_GetEnum(interp, argv[1], options, &option, "subcommand", SAWO_ERRMSG) != SAWO_OK) {
        return SAWO_ERR;
    }

    switch (option) {
        case OPT_GET:
            if (argc < 3) {
                sawo_WrongNumArgs(interp, 2, argv, "dictionary ?key ...?");
                return SAWO_ERR;
            }
            if (sawo_DictKeysVector(interp, argv[2], argv + 3, argc - 3, &objPtr,
                    SAWO_ERRMSG) != SAWO_OK) {
                return SAWO_ERR;
            }
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;

        case OPT_SET:
            if (argc < 5) {
                sawo_WrongNumArgs(interp, 2, argv, "varName key ?key ...? value");
                return SAWO_ERR;
            }
            return sawo_SetDictKeysVector(interp, argv[2], argv + 3, argc - 4, argv[argc - 1], SAWO_ERRMSG);

        case OPT_EXISTS:
            if (argc < 4) {
                sawo_WrongNumArgs(interp, 2, argv, "dictionary key ?key ...?");
                return SAWO_ERR;
            }
            else {
                int rc = sawo_DictKeysVector(interp, argv[2], argv + 3, argc - 3, &objPtr, SAWO_ERRMSG);
                if (rc < 0) {
                    return SAWO_ERR;
                }
                sawo_SetResultBool(interp,  rc == SAWO_OK);
                return SAWO_OK;
            }

        case OPT_UNSET:
            if (argc < 4) {
                sawo_WrongNumArgs(interp, 2, argv, "varName key ?key ...?");
                return SAWO_ERR;
            }
            if (sawo_SetDictKeysVector(interp, argv[2], argv + 3, argc - 3, NULL, 0) != SAWO_OK) {
                return SAWO_ERR;
            }
            return SAWO_OK;

        case OPT_KEYS:
            if (argc != 3 && argc != 4) {
                sawo_WrongNumArgs(interp, 2, argv, "dictionary ?pattern?");
                return SAWO_ERR;
            }
            return sawo_DictKeys(interp, argv[2], argc == 4 ? argv[3] : NULL);

        case OPT_SIZE:
            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "dictionary");
                return SAWO_ERR;
            }
            else if (sawo_DictSize(interp, argv[2]) < 0) {
                return SAWO_ERR;
            }
            sawo_SetResultInt(interp, sawo_DictSize(interp, argv[2]));
            return SAWO_OK;

        case OPT_MERGE:
            if (argc == 2) {
                return SAWO_OK;
            }
            if (sawo_DictSize(interp, argv[2]) < 0) {
                return SAWO_ERR;
            }
            
            break;

        case OPT_UPDATE:
            if (argc < 6 || argc % 2) {
                
                argc = 2;
            }
            break;

        case OPT_CREATE:
            if (argc % 2) {
                sawo_WrongNumArgs(interp, 2, argv, "?key value ...?");
                return SAWO_ERR;
            }
            objPtr = sawo_NewDictObj(interp, argv + 2, argc - 2);
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;

        case OPT_INFO:
            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "dictionary");
                return SAWO_ERR;
            }
            return sawo_DictInfo(interp, argv[2]);
    }
    
    return sawo_EvalEnsemble(interp, "dict", options[option], argc - 2, argv + 2);
}


static int sawo_SubstCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    static const char * const options[] = {
        "-nobackslashes", "-nocommands", "-novariables", NULL
    };
    enum
    { OPT_NOBACKSLASHES, OPT_NOCOMMANDS, OPT_NOVARIABLES };
    int i;
    int flags = SAWO_SUBST_FLAG;
    sawo_Obj *objPtr;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "?options? string");
        return SAWO_ERR;
    }
    for (i = 1; i < (argc - 1); i++) {
        int option;

        if (sawo_GetEnum(interp, argv[i], options, &option, NULL,
                SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        switch (option) {
            case OPT_NOBACKSLASHES:
                flags |= SAWO_SUBST_NOESC;
                break;
            case OPT_NOCOMMANDS:
                flags |= SAWO_SUBST_NOCMD;
                break;
            case OPT_NOVARIABLES:
                flags |= SAWO_SUBST_NOVAR;
                break;
        }
    }
    if (sawo_SubstObj(interp, argv[argc - 1], &objPtr, flags) != SAWO_OK) {
        return SAWO_ERR;
    }
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}


static int sawo_ShowCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int cmd;
    sawo_Obj *objPtr;
    int mode = 0;

    static const char * const commands[] = {
        "body", "statics", "commands", "raises", "channels", "exists", "globals", "level", "frame", "locals",
        "vars", "version", "patchlevel", "complete", "args", "hostname",
        "script", "source", "stacktrace", "execname", "returncodes",
        "references", "alias", NULL
    };
    enum
    { SHOW_BODY, SHOW_STATICS, SHOW_COMMANDS, SHOW_RAISES, SHOW_CHANNELS, SHOW_EXISTS, SHOW_GLOBALS, SHOW_LEVEL,
        SHOW_FRAME, SHOW_LOCALS, SHOW_VARS, SHOW_VERSION, SHOW_PATCHLEVEL, SHOW_COMPLETE, SHOW_ARGS,
        SHOW_HOSTNAME, SHOW_SCRIPT, SHOW_SOURCE, SHOW_STACKTRACE, SHOW_EXECNAME,
        SHOW_RETURNCODES, SHOW_REFERENCES, SHOW_ALIAS,
    };

#ifdef sawo_ext_namespace
    int nons = 0;

    if (argc > 2 && sawo_CompareStringImmediate(interp, argv[1], "-nons")) {
        
        argc--;
        argv++;
        nons = 1;
    }
#endif

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "subcommand ?args ...?");
        return SAWO_ERR;
    }
    if (sawo_GetEnum(interp, argv[1], commands, &cmd, "subcommand", SAWO_ERRMSG | SAWO_ENUM_ABBREV)
        != SAWO_OK) {
        return SAWO_ERR;
    }

    
    switch (cmd) {
        case SHOW_EXISTS:
            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "varName");
                return SAWO_ERR;
            }
            sawo_SetResultBool(interp, sawo_GetVariable(interp, argv[2], 0) != NULL);
            break;

        case SHOW_ALIAS:{
            sawo_Cmd *cmdPtr;

            if (argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "command");
                return SAWO_ERR;
            }
            if ((cmdPtr = sawo_GetCommand(interp, argv[2], SAWO_ERRMSG)) == NULL) {
                return SAWO_ERR;
            }
            if (cmdPtr->israise || cmdPtr->u.native.cmdRaise != sawoAliasCmd) {
                sawo_SetResultFormatted(interp, "command \"%#s\" is not an alias", argv[2]);
                return SAWO_ERR;
            }
            sawo_SetResult(interp, (sawo_Obj *)cmdPtr->u.native.privData);
            return SAWO_OK;
        }

        case SHOW_CHANNELS:
            mode++;             
#ifndef sawo_ext_aio
            sawo_SetResultString(interp, "aio not enabled", -1);
            return SAWO_ERR;
#endif
            
        case SHOW_RAISES:
            mode++;             
            
        case SHOW_COMMANDS:
            
            if (argc != 2 && argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "?pattern?");
                return SAWO_ERR;
            }
#ifdef sawo_ext_namespace
            if (!nons) {
                if (sawo_Length(interp->framePtr->nsObj) || (argc == 3 && sawoGlobMatch("::*", sawo_String(argv[2]), 0))) {
                    return sawo_EvalPrefix(interp, "namespace show", argc - 1, argv + 1);
                }
            }
#endif
            sawo_SetResult(interp, sawoCommandsList(interp, (argc == 3) ? argv[2] : NULL, mode));
            break;

        case SHOW_VARS:
            mode++;             
            
        case SHOW_LOCALS:
            mode++;             
            
        case SHOW_GLOBALS:
            
            if (argc != 2 && argc != 3) {
                sawo_WrongNumArgs(interp, 2, argv, "?pattern?");
                return SAWO_ERR;
            }
#ifdef sawo_ext_namespace
            if (!nons) {
                if (sawo_Length(interp->framePtr->nsObj) || (argc == 3 && sawoGlobMatch("::*", sawo_String(argv[2]), 0))) {
                    return sawo_EvalPrefix(interp, "namespace show", argc - 1, argv + 1);
                }
            }
#endif
            sawo_SetResult(interp, sawoVariablesList(interp, argc == 3 ? argv[2] : NULL, mode));
            break;

        case SHOW_SCRIPT:
            if (argc != 2) {
                sawo_WrongNumArgs(interp, 2, argv, "");
                return SAWO_ERR;
            }
            sawo_SetResult(interp, sawoGetScript(interp, interp->currentScriptObj)->fileNameObj);
            break;

        case SHOW_SOURCE:{
                sawo_wide line;
                sawo_Obj *resObjPtr;
                sawo_Obj *fileNameObj;

                if (argc != 3 && argc != 5) {
                    sawo_WrongNumArgs(interp, 2, argv, "source ?filename line?");
                    return SAWO_ERR;
                }
                if (argc == 5) {
                    if (sawo_GetWide(interp, argv[4], &line) != SAWO_OK) {
                        return SAWO_ERR;
                    }
                    resObjPtr = sawo_NewStringObj(interp, sawo_String(argv[2]), sawo_Length(argv[2]));
                    sawoSetSourceInfo(interp, resObjPtr, argv[3], line);
                }
                else {
                    if (argv[2]->typePtr == &sourceObjType) {
                        fileNameObj = argv[2]->internalRep.sourceValue.fileNameObj;
                        line = argv[2]->internalRep.sourceValue.lineNumber;
                    }
                    else if (argv[2]->typePtr == &scriptObjType) {
                        ScriptObj *script = sawoGetScript(interp, argv[2]);
                        fileNameObj = script->fileNameObj;
                        line = script->firstline;
                    }
                    else {
                        fileNameObj = interp->emptyObj;
                        line = 1;
                    }
                    resObjPtr = sawo_NewListObj(interp, NULL, 0);
                    sawo_ListAppendElement(interp, resObjPtr, fileNameObj);
                    sawo_ListAppendElement(interp, resObjPtr, sawo_NewIntObj(interp, line));
                }
                sawo_SetResult(interp, resObjPtr);
                break;
            }

        case SHOW_STACKTRACE:
            sawo_SetResult(interp, interp->stackTrace);
            break;

        case SHOW_LEVEL:
        case SHOW_FRAME:
            switch (argc) {
                case 2:
                    sawo_SetResultInt(interp, interp->framePtr->level);
                    break;

                case 3:
                    if (sawoInfoLevel(interp, argv[2], &objPtr, cmd == SHOW_LEVEL) != SAWO_OK) {
                        return SAWO_ERR;
                    }
                    sawo_SetResult(interp, objPtr);
                    break;

                default:
                    sawo_WrongNumArgs(interp, 2, argv, "?levelNum?");
                    return SAWO_ERR;
            }
            break;

        case SHOW_BODY:
        case SHOW_STATICS:
        case SHOW_ARGS:{
                sawo_Cmd *cmdPtr;

                if (argc != 3) {
                    sawo_WrongNumArgs(interp, 2, argv, "raisename");
                    return SAWO_ERR;
                }
                if ((cmdPtr = sawo_GetCommand(interp, argv[2], SAWO_ERRMSG)) == NULL) {
                    return SAWO_ERR;
                }
                if (!cmdPtr->israise) {
                    sawo_SetResultFormatted(interp, "command \"%#s\" is not a procedure", argv[2]);
                    return SAWO_ERR;
                }
                switch (cmd) {
                    case SHOW_BODY:
                        sawo_SetResult(interp, cmdPtr->u.raise.bodyObjPtr);
                        break;
                    case SHOW_ARGS:
                        sawo_SetResult(interp, cmdPtr->u.raise.argListObjPtr);
                        break;
                    case SHOW_STATICS:
                        if (cmdPtr->u.raise.staticVars) {
                            int mode = SAWO_VARLIST_LOCALS | SAWO_VARLIST_VALUES;
                            sawo_SetResult(interp, sawoHashtablePatternMatch(interp, cmdPtr->u.raise.staticVars,
                                NULL, sawoVariablesMatch, mode));
                        }
                        break;
                }
                break;
            }

        case SHOW_VERSION:
        case SHOW_PATCHLEVEL:{
                char buf[(SAWO_INTEGER_SPACE * 2) + 1];

                sprintf(buf, "%s.%s (%s / %s-%s-%s) -- %s", HYANG_MAJOR, HYANG_MINOR,
                HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY, HYANG_NICK);
                sawo_SetResultString(interp, buf, -1);
                break;
            }

        case SHOW_COMPLETE:
            if (argc != 3 && argc != 4) {
                sawo_WrongNumArgs(interp, 2, argv, "script ?missing?");
                return SAWO_ERR;
            }
            else {
                char missing;

                sawo_SetResultBool(interp, sawo_ScriptIsComplete(interp, argv[2], &missing));
                if (missing != ' ' && argc == 4) {
                    sawo_SetVariable(interp, argv[3], sawo_NewStringObj(interp, &missing, 1));
                }
            }
            break;

        case SHOW_HOSTNAME:
            
            return sawo_Eval(interp, "os.gethostname");

        case SHOW_EXECNAME:
            
            return sawo_Eval(interp, "{show execname}");

        case SHOW_RETURNCODES:
            if (argc == 2) {
                int i;
                sawo_Obj *listObjPtr = sawo_NewListObj(interp, NULL, 0);

                for (i = 0; sawoReturnCodes[i]; i++) {
                    sawo_ListAppendElement(interp, listObjPtr, sawo_NewIntObj(interp, i));
                    sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp,
                            sawoReturnCodes[i], -1));
                }

                sawo_SetResult(interp, listObjPtr);
            }
            else if (argc == 3) {
                long code;
                const char *name;

                if (sawo_GetLong(interp, argv[2], &code) != SAWO_OK) {
                    return SAWO_ERR;
                }
                name = sawo_ReturnCode(code);
                if (*name == '?') {
                    sawo_SetResultInt(interp, code);
                }
                else {
                    sawo_SetResultString(interp, name, -1);
                }
            }
            else {
                sawo_WrongNumArgs(interp, 2, argv, "?code?");
                return SAWO_ERR;
            }
            break;
        case SHOW_REFERENCES:
#ifdef SAWO_REFERENCES
            return sawoInfoReferences(interp, argc, argv);
#else
            sawo_SetResultString(interp, "not supported", -1);
            return SAWO_ERR;
#endif
    }
    return SAWO_OK;
}


static int sawo_ExistsCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;
    int result = 0;

    static const char * const options[] = {
        "-command", "-raise", "-alias", "-var", NULL
    };
    enum
    {
        OPT_COMMAND, OPT_RAISE, OPT_ALIAS, OPT_VAR
    };
    int option;

    if (argc == 2) {
        option = OPT_VAR;
        objPtr = argv[1];
    }
    else if (argc == 3) {
        if (sawo_GetEnum(interp, argv[1], options, &option, NULL, SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        objPtr = argv[2];
    }
    else {
        sawo_WrongNumArgs(interp, 1, argv, "?option? name");
        return SAWO_ERR;
    }

    if (option == OPT_VAR) {
        result = sawo_GetVariable(interp, objPtr, 0) != NULL;
    }
    else {
        
        sawo_Cmd *cmd = sawo_GetCommand(interp, objPtr, SAWO_NONE);

        if (cmd) {
            switch (option) {
            case OPT_COMMAND:
                result = 1;
                break;

            case OPT_ALIAS:
                result = cmd->israise == 0 && cmd->u.native.cmdRaise == sawoAliasCmd;
                break;

            case OPT_RAISE:
                result = cmd->israise;
                break;
            }
        }
    }
    sawo_SetResultBool(interp, result);
    return SAWO_OK;
}


static int sawo_SplitCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *str, *splitChars, *noMatchStart;
    int splitLen, strLen;
    sawo_Obj *resObjPtr;
    int c;
    int len;

    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "string ?splitChars?");
        return SAWO_ERR;
    }

    str = sawo_GetString(argv[1], &len);
    if (len == 0) {
        return SAWO_OK;
    }
    strLen = sawo_Utf8Length(interp, argv[1]);

    
    if (argc == 2) {
        splitChars = " \n\t\r";
        splitLen = 4;
    }
    else {
        splitChars = sawo_String(argv[2]);
        splitLen = sawo_Utf8Length(interp, argv[2]);
    }

    noMatchStart = str;
    resObjPtr = sawo_NewListObj(interp, NULL, 0);

    
    if (splitLen) {
        sawo_Obj *objPtr;
        while (strLen--) {
            const char *sc = splitChars;
            int scLen = splitLen;
            int sl = utf8_tounicode(str, &c);
            while (scLen--) {
                int pc;
                sc += utf8_tounicode(sc, &pc);
                if (c == pc) {
                    objPtr = sawo_NewStringObj(interp, noMatchStart, (str - noMatchStart));
                    sawo_ListAppendElement(interp, resObjPtr, objPtr);
                    noMatchStart = str + sl;
                    break;
                }
            }
            str += sl;
        }
        objPtr = sawo_NewStringObj(interp, noMatchStart, (str - noMatchStart));
        sawo_ListAppendElement(interp, resObjPtr, objPtr);
    }
    else {
        sawo_Obj **commonObj = NULL;
#define NUM_COMMON (128 - 9)
        while (strLen--) {
            int n = utf8_tounicode(str, &c);
#ifdef SAWO_OPTIMIZATION
            if (c >= 9 && c < 128) {
                
                c -= 9;
                if (!commonObj) {
                    commonObj = sawo_Alloc(sizeof(*commonObj) * NUM_COMMON);
                    memset(commonObj, 0, sizeof(*commonObj) * NUM_COMMON);
                }
                if (!commonObj[c]) {
                    commonObj[c] = sawo_NewStringObj(interp, str, 1);
                }
                sawo_ListAppendElement(interp, resObjPtr, commonObj[c]);
                str++;
                continue;
            }
#endif
            sawo_ListAppendElement(interp, resObjPtr, sawo_NewStringObjUtf8(interp, str, 1));
            str += n;
        }
        sawo_Free(commonObj);
    }

    sawo_SetResult(interp, resObjPtr);
    return SAWO_OK;
}


static int sawo_JoinCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *joinStr;
    int joinStrLen;

    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "list ?joinString?");
        return SAWO_ERR;
    }
    
    if (argc == 2) {
        joinStr = " ";
        joinStrLen = 1;
    }
    else {
        joinStr = sawo_GetString(argv[2], &joinStrLen);
    }
    sawo_SetResult(interp, sawo_ListJoin(interp, argv[1], joinStr, joinStrLen));
    return SAWO_OK;
}


static int sawo_FormatCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "formatString ?arg arg ...?");
        return SAWO_ERR;
    }
    objPtr = sawo_FormatString(interp, argv[1], argc - 2, argv + 2);
    if (objPtr == NULL)
        return SAWO_ERR;
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}


static int sawo_ScanCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *listPtr, **outVec;
    int outc, i;

    if (argc < 3) {
        sawo_WrongNumArgs(interp, 1, argv, "string format ?varName varName ...?");
        return SAWO_ERR;
    }
    if (argv[2]->typePtr != &scanFmtStringObjType)
        SetScanFmtFromAny(interp, argv[2]);
    if (FormatGetError(argv[2]) != 0) {
        sawo_SetResultString(interp, FormatGetError(argv[2]), -1);
        return SAWO_ERR;
    }
    if (argc > 3) {
        int maxPos = FormatGetMaxPos(argv[2]);
        int count = FormatGetCnvCount(argv[2]);

        if (maxPos > argc - 3) {
            sawo_SetResultString(interp, "\"%n$\" argument index out of range", -1);
            return SAWO_ERR;
        }
        else if (count > argc - 3) {
            sawo_SetResultString(interp, "different numbers of variable names and "
                "field specifiers", -1);
            return SAWO_ERR;
        }
        else if (count < argc - 3) {
            sawo_SetResultString(interp, "variable is not assigned by any "
                "conversion specifiers", -1);
            return SAWO_ERR;
        }
    }
    listPtr = sawo_ScanString(interp, argv[1], argv[2], SAWO_ERRMSG);
    if (listPtr == 0)
        return SAWO_ERR;
    if (argc > 3) {
        int rc = SAWO_OK;
        int count = 0;

        if (listPtr != 0 && listPtr != (sawo_Obj *)EOF) {
            int len = sawo_ListLength(interp, listPtr);

            if (len != 0) {
                sawoListGetElements(interp, listPtr, &outc, &outVec);
                for (i = 0; i < outc; ++i) {
                    if (sawo_Length(outVec[i]) > 0) {
                        ++count;
                        if (sawo_SetVariable(interp, argv[3 + i], outVec[i]) != SAWO_OK) {
                            rc = SAWO_ERR;
                        }
                    }
                }
            }
            sawo_FreeNewObj(interp, listPtr);
        }
        else {
            count = -1;
        }
        if (rc == SAWO_OK) {
            sawo_SetResultInt(interp, count);
        }
        return rc;
    }
    else {
        if (listPtr == (sawo_Obj *)EOF) {
            sawo_SetResult(interp, sawo_NewListObj(interp, 0, 0));
            return SAWO_OK;
        }
        sawo_SetResult(interp, listPtr);
    }
    return SAWO_OK;
}


static int sawo_ErrorCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    if (argc != 2 && argc != 3) {
        sawo_WrongNumArgs(interp, 1, argv, "message ?stacktrace?");
        return SAWO_ERR;
    }
    sawo_SetResult(interp, argv[1]);
    if (argc == 3) {
        sawoSetStackTrace(interp, argv[2]);
        return SAWO_ERR;
    }
    interp->addStackTrace++;
    return SAWO_ERR;
}


static int sawo_LrangeCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;

    if (argc != 4) {
        sawo_WrongNumArgs(interp, 1, argv, "list first last");
        return SAWO_ERR;
    }
    if ((objPtr = sawo_ListRange(interp, argv[1], argv[2], argv[3])) == NULL)
        return SAWO_ERR;
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}


static int sawo_LrepeatCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;
    long count;

    if (argc < 2 || sawo_GetLong(interp, argv[1], &count) != SAWO_OK || count < 0) {
        sawo_WrongNumArgs(interp, 1, argv, "count ?value ...?");
        return SAWO_ERR;
    }

    if (count == 0 || argc == 2) {
        return SAWO_OK;
    }

    argc -= 2;
    argv += 2;

    objPtr = sawo_NewListObj(interp, argv, argc);
    while (--count) {
        ListInsertElements(objPtr, -1, argc, argv);
    }

    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}

char **sawo_GetEnviron(void)
{
#if defined(HAVE__NSGETENVIRON)
    return *_NSGetEnviron();
#else
    #if !defined(NO_ENVIRON_EXTERN)
    extern char **environ;
    #endif

    return environ;
#endif
}

void sawo_SetEnviron(char **env)
{
#if defined(HAVE__NSGETENVIRON)
    *_NSGetEnviron() = env;
#else
    #if !defined(NO_ENVIRON_EXTERN)
    extern char **environ;
    #endif

    environ = env;
#endif
}


static int sawo_EnvCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *key;
    const char *val;

    if (argc == 1) {
        char **e = sawo_GetEnviron();

        int i;
        sawo_Obj *listObjPtr = sawo_NewListObj(interp, NULL, 0);

        for (i = 0; e[i]; i++) {
            const char *equals = strchr(e[i], '=');

            if (equals) {
                sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp, e[i],
                        equals - e[i]));
                sawo_ListAppendElement(interp, listObjPtr, sawo_NewStringObj(interp, equals + 1, -1));
            }
        }

        sawo_SetResult(interp, listObjPtr);
        return SAWO_OK;
    }

    if (argc < 2) {
        sawo_WrongNumArgs(interp, 1, argv, "varName ?default?");
        return SAWO_ERR;
    }
    key = sawo_String(argv[1]);
    val = getenv(key);
    if (val == NULL) {
        if (argc < 3) {
            sawo_SetResultFormatted(interp, "environment variable \"%#s\" does not exist", argv[1]);
            return SAWO_ERR;
        }
        val = sawo_String(argv[2]);
    }
    sawo_SetResult(interp, sawo_NewStringObj(interp, val, -1));
    return SAWO_OK;
}


static int sawo_SourceCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int retval;

    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "fileName");
        return SAWO_ERR;
    }
    retval = sawo_EvalFile(interp, sawo_String(argv[1]));
    if (retval == SAWO_RETURN)
        return SAWO_OK;
    return retval;
}


static int sawo_LreverseCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *revObjPtr, **ele;
    int len;

    if (argc != 2) {
        sawo_WrongNumArgs(interp, 1, argv, "list");
        return SAWO_ERR;
    }
    sawoListGetElements(interp, argv[1], &len, &ele);
    len--;
    revObjPtr = sawo_NewListObj(interp, NULL, 0);
    while (len >= 0)
        ListAppendElement(revObjPtr, ele[len--]);
    sawo_SetResult(interp, revObjPtr);
    return SAWO_OK;
}

static int sawoRangeLen(sawo_wide start, sawo_wide end, sawo_wide step)
{
    sawo_wide len;

    if (step == 0)
        return -1;
    if (start == end)
        return 0;
    else if (step > 0 && start > end)
        return -1;
    else if (step < 0 && end > start)
        return -1;
    len = end - start;
    if (len < 0)
        len = -len;             
    if (step < 0)
        step = -step;           
    len = 1 + ((len - 1) / step);
    if (len > INT_MAX)
        len = INT_MAX;
    return (int)((len < 0) ? -1 : len);
}


static int sawo_RangeCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_wide start = 0, end, step = 1;
    int len, i;
    sawo_Obj *objPtr;

    if (argc < 2 || argc > 4) {
        sawo_WrongNumArgs(interp, 1, argv, "?start? end ?step?");
        return SAWO_ERR;
    }
    if (argc == 2) {
        if (sawo_GetWide(interp, argv[1], &end) != SAWO_OK)
            return SAWO_ERR;
    }
    else {
        if (sawo_GetWide(interp, argv[1], &start) != SAWO_OK ||
            sawo_GetWide(interp, argv[2], &end) != SAWO_OK)
            return SAWO_ERR;
        if (argc == 4 && sawo_GetWide(interp, argv[3], &step) != SAWO_OK)
            return SAWO_ERR;
    }
    if ((len = sawoRangeLen(start, end, step)) == -1) {
        sawo_SetResultString(interp, "Invalid (infinite?) range specified", -1);
        return SAWO_ERR;
    }
    objPtr = sawo_NewListObj(interp, NULL, 0);
    for (i = 0; i < len; i++)
        ListAppendElement(objPtr, sawo_NewIntObj(interp, start + i * step));
    sawo_SetResult(interp, objPtr);
    return SAWO_OK;
}


static int sawo_RandCoreCommand(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_wide min = 0, max = 0, len, maxMul;

    if (argc < 1 || argc > 3) {
        sawo_WrongNumArgs(interp, 1, argv, "?min? max");
        return SAWO_ERR;
    }
    if (argc == 1) {
        max = SAWO_WIDE_MAX;
    } else if (argc == 2) {
        if (sawo_GetWide(interp, argv[1], &max) != SAWO_OK)
            return SAWO_ERR;
    } else if (argc == 3) {
        if (sawo_GetWide(interp, argv[1], &min) != SAWO_OK ||
            sawo_GetWide(interp, argv[2], &max) != SAWO_OK)
            return SAWO_ERR;
    }
    len = max-min;
    if (len < 0) {
        sawo_SetResultString(interp, "Invalid arguments (max < min)", -1);
        return SAWO_ERR;
    }
    maxMul = SAWO_WIDE_MAX - (len ? (SAWO_WIDE_MAX%len) : 0);
    while (1) {
        sawo_wide r;

        sawoRandomBytes(interp, &r, sizeof(sawo_wide));
        if (r < 0 || r >= maxMul) continue;
        r = (len == 0) ? 0 : r%len;
        sawo_SetResultInt(interp, min+r);
        return SAWO_OK;
    }
}

static const struct {
    const char *name;
    sawo_CmdRaise *cmdRaise;
} sawo_CoreCommandsTable[] = {
    {"alias", sawo_AliasCoreCommand},
    {"set", sawo_SetCoreCommand},
    {"unset", sawo_UnsetCoreCommand},
    {"pin", sawo_PinCoreCommand},
    {"+", sawo_AddCoreCommand},
    {"*", sawo_MulCoreCommand},
    {"-", sawo_SubCoreCommand},
    {"/", sawo_DivCoreCommand},
    {"incr", sawo_IncrCoreCommand},
    {"while", sawo_WhileCoreCommand},
    {"loop", sawo_LoopCoreCommand},
    {"for", sawo_ForCoreCommand},
    {"foreach", sawo_ForeachCoreCommand},
    {"lmap", sawo_LmapCoreCommand},
    {"lassign", sawo_LassignCoreCommand},
    {"if", sawo_IfCoreCommand},
    {"switch", sawo_SwitchCoreCommand},
    {"list", sawo_ListCoreCommand},
    {"lindex", sawo_LindexCoreCommand},
    {"lset", sawo_LsetCoreCommand},
    {"lsearch", sawo_LsearchCoreCommand},
    {"llength", sawo_LlengthCoreCommand},
    {"lappend", sawo_LappendCoreCommand},
    {"linsert", sawo_LinsertCoreCommand},
    {"lreplace", sawo_LreplaceCoreCommand},
    {"lsort", sawo_LsortCoreCommand},
    {"append", sawo_AppendCoreCommand},
    {"debug", sawo_DebugCoreCommand},
    {"eval", sawo_EvalCoreCommand},
    {"uplevel", sawo_UplevelCoreCommand},
    {"expr", sawo_ExprCoreCommand},
    {"break", sawo_BreakCoreCommand},
    {"continue", sawo_ContinueCoreCommand},
    {"raise", sawo_RaiseCoreCommand},
    {"concat", sawo_ConcatCoreCommand},
    {"return", sawo_ReturnCoreCommand},
    {"upvar", sawo_UpvarCoreCommand},
    {"global", sawo_GlobalCoreCommand},
    {"string", sawo_StringCoreCommand},
    {"time", sawo_TimeCoreCommand},
    {"exit", sawo_ExitCoreCommand},
    {"catch", sawo_CatchCoreCommand},
#ifdef SAWO_REFERENCES
    {"ref", sawo_RefCoreCommand},
    {"getref", sawo_GetrefCoreCommand},
    {"setref", sawo_SetrefCoreCommand},
    {"finalize", sawo_FinalizeCoreCommand},
    {"collect", sawo_CollectCoreCommand},
#endif
    {"rename", sawo_RenameCoreCommand},
    {"dict", sawo_DictCoreCommand},
    {"subst", sawo_SubstCoreCommand},
    {"show", sawo_ShowCoreCommand},
    {"exists", sawo_ExistsCoreCommand},
    {"split", sawo_SplitCoreCommand},
    {"join", sawo_JoinCoreCommand},
    {"format", sawo_FormatCoreCommand},
    {"scan", sawo_ScanCoreCommand},
    {"error", sawo_ErrorCoreCommand},
    {"lrange", sawo_LrangeCoreCommand},
    {"lrepeat", sawo_LrepeatCoreCommand},
    {"env", sawo_EnvCoreCommand},
    {"source", sawo_SourceCoreCommand},
    {"lreverse", sawo_LreverseCoreCommand},
    {"range", sawo_RangeCoreCommand},
    {"rand", sawo_RandCoreCommand},
    {"tailcall", sawo_TailcallCoreCommand},
    {"local", sawo_LocalCoreCommand},
    {"upcall", sawo_UpcallCoreCommand},
    {"apply", sawo_ApplyCoreCommand},
    {NULL, NULL},
};

void sawo_RegisterCoreCommands(sawo_Interp *interp)
{
    int i = 0;

    while (sawo_CoreCommandsTable[i].name != NULL) {
        sawo_CreateCommand(interp,
            sawo_CoreCommandsTable[i].name, sawo_CoreCommandsTable[i].cmdRaise, NULL, NULL);
        i++;
    }
}

void sawo_MakeErrorMessage(sawo_Interp *interp)
{
    sawo_Obj *argv[2];

    argv[0] = sawo_NewStringObj(interp, "errorInfo", -1);
    argv[1] = interp->result;

    sawo_EvalObjVector(interp, 2, argv);
}

static void sawoSetFailedEnumResult(sawo_Interp *interp, const char *arg, const char *badtype,
    const char *prefix, const char *const *tablePtr, const char *name)
{
    int count;
    char **tablePtrSorted;
    int i;

    for (count = 0; tablePtr[count]; count++) {
    }

    if (name == NULL) {
        name = "option";
    }

    sawo_SetResultFormatted(interp, "%s%s \"%s\": must be ", badtype, name, arg);
    tablePtrSorted = sawo_Alloc(sizeof(char *) * count);
    memcpy(tablePtrSorted, tablePtr, sizeof(char *) * count);
    qsort(tablePtrSorted, count, sizeof(char *), qsortCompareStringPointers);
    for (i = 0; i < count; i++) {
        if (i + 1 == count && count > 1) {
            sawo_AppendString(interp, sawo_GetResult(interp), "or ", -1);
        }
        sawo_AppendStrings(interp, sawo_GetResult(interp), prefix, tablePtrSorted[i], NULL);
        if (i + 1 != count) {
            sawo_AppendString(interp, sawo_GetResult(interp), ", ", -1);
        }
    }
    sawo_Free(tablePtrSorted);
}

int sawo_GetEnum(sawo_Interp *interp, sawo_Obj *objPtr,
    const char *const *tablePtr, int *indexPtr, const char *name, int flags)
{
    const char *bad = "bad ";
    const char *const *entryPtr = NULL;
    int i;
    int match = -1;
    int arglen;
    const char *arg = sawo_GetString(objPtr, &arglen);

    *indexPtr = -1;

    for (entryPtr = tablePtr, i = 0; *entryPtr != NULL; entryPtr++, i++) {
        if (sawo_CompareStringImmediate(interp, objPtr, *entryPtr)) {
            
            *indexPtr = i;
            return SAWO_OK;
        }
        if (flags & SAWO_ENUM_ABBREV) {
            if (strncmp(arg, *entryPtr, arglen) == 0) {
                if (*arg == '-' && arglen == 1) {
                    break;
                }
                if (match >= 0) {
                    bad = "ambiguous ";
                    goto ambiguous;
                }
                match = i;
            }
        }
    }

    
    if (match >= 0) {
        *indexPtr = match;
        return SAWO_OK;
    }

  ambiguous:
    if (flags & SAWO_ERRMSG) {
        sawoSetFailedEnumResult(interp, arg, bad, "", tablePtr, name);
    }
    return SAWO_ERR;
}

int sawo_FindByName(const char *name, const char * const array[], size_t len)
{
    int i;

    for (i = 0; i < (int)len; i++) {
        if (array[i] && strcmp(array[i], name) == 0) {
            return i;
        }
    }
    return -1;
}

int sawo_IsDict(sawo_Obj *objPtr)
{
    return objPtr->typePtr == &dictObjType;
}

int sawo_IsList(sawo_Obj *objPtr)
{
    return objPtr->typePtr == &listObjType;
}

void sawo_SetResultFormatted(sawo_Interp *interp, const char *format, ...)
{
    
    int len = strlen(format);
    int extra = 0;
    int n = 0;
    const char *params[5];
    char *buf;
    va_list args;
    int i;

    va_start(args, format);

    for (i = 0; i < len && n < 5; i++) {
        int l;

        if (strncmp(format + i, "%s", 2) == 0) {
            params[n] = va_arg(args, char *);

            l = strlen(params[n]);
        }
        else if (strncmp(format + i, "%#s", 3) == 0) {
            sawo_Obj *objPtr = va_arg(args, sawo_Obj *);

            params[n] = sawo_GetString(objPtr, &l);
        }
        else {
            if (format[i] == '%') {
                i++;
            }
            continue;
        }
        n++;
        extra += l;
    }

    len += extra;
    buf = sawo_Alloc(len + 1);
    len = snprintf(buf, len + 1, format, params[0], params[1], params[2], params[3], params[4]);

    va_end(args);

    sawo_SetResult(interp, sawo_NewStringObjNoAlloc(interp, buf, len));
}


#ifndef sawo_ext_package
int sawo_PackageProvide(sawo_Interp *interp, const char *name, const char *ver, int flags)
{
    return SAWO_OK;
}
#endif
#ifndef sawo_ext_aio
FILE *sawo_AioFilehandle(sawo_Interp *interp, sawo_Obj *fhObj)
{
    sawo_SetResultString(interp, "aio not enabled", -1);
    return NULL;
}
#endif


#include <stdio.h>
#include <string.h>


static int subcmd_null(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    
    return SAWO_OK;
}

static const sawo_subcmd_type dummy_subcmd = {
    "dummy", NULL, subcmd_null, 0, 0, SAWO_MODFLAG_HIDDEN
};

static void add_commands(sawo_Interp *interp, const sawo_subcmd_type * ct, const char *sep)
{
    const char *s = "";

    for (; ct->cmd; ct++) {
        if (!(ct->flags & SAWO_MODFLAG_HIDDEN)) {
            sawo_AppendStrings(interp, sawo_GetResult(interp), s, ct->cmd, NULL);
            s = sep;
        }
    }
}

static void bad_subcmd(sawo_Interp *interp, const sawo_subcmd_type * command_table, const char *type,
    sawo_Obj *cmd, sawo_Obj *subcmd)
{
    sawo_SetResult(interp, sawo_NewEmptyStringObj(interp));
    sawo_AppendStrings(interp, sawo_GetResult(interp), sawo_String(cmd), ", ", type,
        " command \"", sawo_String(subcmd), "\": should be ", NULL);
    add_commands(interp, command_table, ", ");
}

static void show_cmd_usage(sawo_Interp *interp, const sawo_subcmd_type * command_table, int argc,
    sawo_Obj *const *argv)
{
    sawo_SetResult(interp, sawo_NewEmptyStringObj(interp));
    sawo_AppendStrings(interp, sawo_GetResult(interp), "Usage: \"", sawo_String(argv[0]),
        " command ... \", where command is one of: ", NULL);
    add_commands(interp, command_table, ", ");
}

static void add_cmd_usage(sawo_Interp *interp, const sawo_subcmd_type * ct, sawo_Obj *cmd)
{
    if (cmd) {
        sawo_AppendStrings(interp, sawo_GetResult(interp), sawo_String(cmd), " ", NULL);
    }
    sawo_AppendStrings(interp, sawo_GetResult(interp), ct->cmd, NULL);
    if (ct->args && *ct->args) {
        sawo_AppendStrings(interp, sawo_GetResult(interp), " ", ct->args, NULL);
    }
}

static void set_wrong_args(sawo_Interp *interp, const sawo_subcmd_type * command_table, sawo_Obj *subcmd)
{
    sawo_SetResultString(interp, "wrong # args: should be \"", -1);
    add_cmd_usage(interp, command_table, subcmd);
    sawo_AppendStrings(interp, sawo_GetResult(interp), "\"", NULL);
}

const sawo_subcmd_type *sawo_ParseSubCmd(sawo_Interp *interp, const sawo_subcmd_type * command_table,
    int argc, sawo_Obj *const *argv)
{
    const sawo_subcmd_type *ct;
    const sawo_subcmd_type *partial = 0;
    int cmdlen;
    sawo_Obj *cmd;
    const char *cmdstr;
    const char *cmdname;
    int help = 0;

    cmdname = sawo_String(argv[0]);

    if (argc < 2) {
        sawo_SetResult(interp, sawo_NewEmptyStringObj(interp));
        sawo_AppendStrings(interp, sawo_GetResult(interp), "wrong # args: should be \"", cmdname,
            " command ...\"\n", NULL);
        sawo_AppendStrings(interp, sawo_GetResult(interp), "Use \"", cmdname, " -help ?command?\" for help", NULL);
        return 0;
    }

    cmd = argv[1];

    
    if (sawo_CompareStringImmediate(interp, cmd, "-help")) {
        if (argc == 2) {
            
            show_cmd_usage(interp, command_table, argc, argv);
            return &dummy_subcmd;
        }
        help = 1;

        
        cmd = argv[2];
    }

    
    if (sawo_CompareStringImmediate(interp, cmd, "-commands")) {
        
        sawo_SetResult(interp, sawo_NewEmptyStringObj(interp));
        add_commands(interp, command_table, " ");
        return &dummy_subcmd;
    }

    cmdstr = sawo_GetString(cmd, &cmdlen);

    for (ct = command_table; ct->cmd; ct++) {
        if (sawo_CompareStringImmediate(interp, cmd, ct->cmd)) {
            
            break;
        }
        if (strncmp(cmdstr, ct->cmd, cmdlen) == 0) {
            if (partial) {
                
                if (help) {
                    
                    show_cmd_usage(interp, command_table, argc, argv);
                    return &dummy_subcmd;
                }
                bad_subcmd(interp, command_table, "ambiguous", argv[0], argv[1 + help]);
                return 0;
            }
            partial = ct;
        }
        continue;
    }

    
    if (partial && !ct->cmd) {
        ct = partial;
    }

    if (!ct->cmd) {
        
        if (help) {
            
            show_cmd_usage(interp, command_table, argc, argv);
            return &dummy_subcmd;
        }
        bad_subcmd(interp, command_table, "unknown", argv[0], argv[1 + help]);
        return 0;
    }

    if (help) {
        sawo_SetResultString(interp, "Usage: ", -1);
        
        add_cmd_usage(interp, ct, argv[0]);
        return &dummy_subcmd;
    }

    
    if (argc - 2 < ct->minargs || (ct->maxargs >= 0 && argc - 2 > ct->maxargs)) {
        sawo_SetResultString(interp, "wrong # args: should be \"", -1);
        
        add_cmd_usage(interp, ct, argv[0]);
        sawo_AppendStrings(interp, sawo_GetResult(interp), "\"", NULL);

        return 0;
    }

    
    return ct;
}

int sawo_CallSubCmd(sawo_Interp *interp, const sawo_subcmd_type * ct, int argc, sawo_Obj *const *argv)
{
    int ret = SAWO_ERR;

    if (ct) {
        if (ct->flags & SAWO_MODFLAG_FULLARGV) {
            ret = ct->function(interp, argc, argv);
        }
        else {
            ret = ct->function(interp, argc - 2, argv + 2);
        }
        if (ret < 0) {
            set_wrong_args(interp, ct, argv[0]);
            ret = SAWO_ERR;
        }
    }
    return ret;
}

int sawo_SubCmdRaise(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const sawo_subcmd_type *ct =
        sawo_ParseSubCmd(interp, (const sawo_subcmd_type *)sawo_CmdPrivData(interp), argc, argv);

    return sawo_CallSubCmd(interp, ct, argc, argv);
}

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <assert.h>


int utf8_fromunicode(char *p, unsigned uc)
{
    if (uc <= 0x7f) {
        *p = uc;
        return 1;
    }
    else if (uc <= 0x7ff) {
        *p++ = 0xc0 | ((uc & 0x7c0) >> 6);
        *p = 0x80 | (uc & 0x3f);
        return 2;
    }
    else if (uc <= 0xffff) {
        *p++ = 0xe0 | ((uc & 0xf000) >> 12);
        *p++ = 0x80 | ((uc & 0xfc0) >> 6);
        *p = 0x80 | (uc & 0x3f);
        return 3;
    }
    
    else {
        *p++ = 0xf0 | ((uc & 0x1c0000) >> 18);
        *p++ = 0x80 | ((uc & 0x3f000) >> 12);
        *p++ = 0x80 | ((uc & 0xfc0) >> 6);
        *p = 0x80 | (uc & 0x3f);
        return 4;
    }
}

#include <ctype.h>
#include <string.h>


#define SAWO_INTEGER_SPACE 24
#define MAX_FLOAT_WIDTH 320

sawo_Obj *sawo_FormatString(sawo_Interp *interp, sawo_Obj *fmtObjPtr, int objc, sawo_Obj *const *objv)
{
    const char *span, *format, *formatEnd, *msg;
    int numBytes = 0, objIndex = 0, gotXpg = 0, gotSequential = 0;
    static const char * const mixedXPG =
            "cannot mix \"%\" and \"%n$\" conversion specifiers";
    static const char * const badIndex[2] = {
        "not enough arguments for all format specifiers",
        "\"%n$\" argument index out of range"
    };
    int formatLen;
    sawo_Obj *resultPtr;

    char *num_buffer = NULL;
    int num_buffer_size = 0;

    span = format = sawo_GetString(fmtObjPtr, &formatLen);
    formatEnd = format + formatLen;
    resultPtr = sawo_NewEmptyStringObj(interp);

    while (format != formatEnd) {
        char *end;
        int gotMinus, sawFlag;
        int gotPrecision, useShort;
        long width, precision;
        int newXpg;
        int ch;
        int step;
        int doubleType;
        char pad = ' ';
        char spec[2*SAWO_INTEGER_SPACE + 12];
        char *p;

        int formatted_chars;
        int formatted_bytes;
        const char *formatted_buf;

        step = utf8_tounicode(format, &ch);
        format += step;
        if (ch != '%') {
            numBytes += step;
            continue;
        }
        if (numBytes) {
            sawo_AppendString(interp, resultPtr, span, numBytes);
            numBytes = 0;
        }


        step = utf8_tounicode(format, &ch);
        if (ch == '%') {
            span = format;
            numBytes = step;
            format += step;
            continue;
        }


        newXpg = 0;
        if (isdigit(ch)) {
            int position = strtoul(format, &end, 10);
            if (*end == '$') {
                newXpg = 1;
                objIndex = position - 1;
                format = end + 1;
                step = utf8_tounicode(format, &ch);
            }
        }
        if (newXpg) {
            if (gotSequential) {
                msg = mixedXPG;
                goto errorMsg;
            }
            gotXpg = 1;
        } else {
            if (gotXpg) {
                msg = mixedXPG;
                goto errorMsg;
            }
            gotSequential = 1;
        }
        if ((objIndex < 0) || (objIndex >= objc)) {
            msg = badIndex[gotXpg];
            goto errorMsg;
        }

        p = spec;
        *p++ = '%';

        gotMinus = 0;
        sawFlag = 1;
        do {
            switch (ch) {
            case '-':
                gotMinus = 1;
                break;
            case '0':
                pad = ch;
                break;
            case ' ':
            case '+':
            case '#':
                break;
            default:
                sawFlag = 0;
                continue;
            }
            *p++ = ch;
            format += step;
            step = utf8_tounicode(format, &ch);
        } while (sawFlag);


        width = 0;
        if (isdigit(ch)) {
            width = strtoul(format, &end, 10);
            format = end;
            step = utf8_tounicode(format, &ch);
        } else if (ch == '*') {
            if (objIndex >= objc - 1) {
                msg = badIndex[gotXpg];
                goto errorMsg;
            }
            if (sawo_GetLong(interp, objv[objIndex], &width) != SAWO_OK) {
                goto error;
            }
            if (width < 0) {
                width = -width;
                if (!gotMinus) {
                    *p++ = '-';
                    gotMinus = 1;
                }
            }
            objIndex++;
            format += step;
            step = utf8_tounicode(format, &ch);
        }


        gotPrecision = precision = 0;
        if (ch == '.') {
            gotPrecision = 1;
            format += step;
            step = utf8_tounicode(format, &ch);
        }
        if (isdigit(ch)) {
            precision = strtoul(format, &end, 10);
            format = end;
            step = utf8_tounicode(format, &ch);
        } else if (ch == '*') {
            if (objIndex >= objc - 1) {
                msg = badIndex[gotXpg];
                goto errorMsg;
            }
            if (sawo_GetLong(interp, objv[objIndex], &precision) != SAWO_OK) {
                goto error;
            }


            if (precision < 0) {
                precision = 0;
            }
            objIndex++;
            format += step;
            step = utf8_tounicode(format, &ch);
        }


        useShort = 0;
        if (ch == 'h') {
            useShort = 1;
            format += step;
            step = utf8_tounicode(format, &ch);
        } else if (ch == 'l') {
            
            format += step;
            step = utf8_tounicode(format, &ch);
            if (ch == 'l') {
                format += step;
                step = utf8_tounicode(format, &ch);
            }
        }

        format += step;
        span = format;


        if (ch == 'i') {
            ch = 'd';
        }

        doubleType = 0;

        switch (ch) {
        case '\0':
            msg = "format string ended in middle of field specifier";
            goto errorMsg;
        case 's': {
            formatted_buf = sawo_GetString(objv[objIndex], &formatted_bytes);
            formatted_chars = sawo_Utf8Length(interp, objv[objIndex]);
            if (gotPrecision && (precision < formatted_chars)) {
                
                formatted_chars = precision;
                formatted_bytes = utf8_index(formatted_buf, precision);
            }
            break;
        }
        case 'c': {
            sawo_wide code;

            if (sawo_GetWide(interp, objv[objIndex], &code) != SAWO_OK) {
                goto error;
            }
            
            formatted_bytes = utf8_getchars(spec, code);
            formatted_buf = spec;
            formatted_chars = 1;
            break;
        }
        case 'b': {
                unsigned sawo_wide w;
                int length;
                int i;
                int j;

                if (sawo_GetWide(interp, objv[objIndex], (sawo_wide *)&w) != SAWO_OK) {
                    goto error;
                }
                length = sizeof(w) * 8;


                
                if (num_buffer_size < length + 1) {
                    num_buffer_size = length + 1;
                    num_buffer = sawo_Realloc(num_buffer, num_buffer_size);
                }

                j = 0;
                for (i = length; i > 0; ) {
                        i--;
                    if (w & ((unsigned sawo_wide)1 << i)) {
                                num_buffer[j++] = '1';
                        }
                        else if (j || i == 0) {
                                num_buffer[j++] = '0';
                        }
                }
                num_buffer[j] = 0;
                formatted_chars = formatted_bytes = j;
                formatted_buf = num_buffer;
                break;
        }

        case 'e':
        case 'E':
        case 'f':
        case 'g':
        case 'G':
            doubleType = 1;
            
        case 'd':
        case 'u':
        case 'o':
        case 'x':
        case 'X': {
            sawo_wide w;
            double d;
            int length;

            
            if (width) {
                p += sprintf(p, "%ld", width);
            }
            if (gotPrecision) {
                p += sprintf(p, ".%ld", precision);
            }

            
            if (doubleType) {
                if (sawo_GetDouble(interp, objv[objIndex], &d) != SAWO_OK) {
                    goto error;
                }
                length = MAX_FLOAT_WIDTH;
            }
            else {
                if (sawo_GetWide(interp, objv[objIndex], &w) != SAWO_OK) {
                    goto error;
                }
                length = SAWO_INTEGER_SPACE;
                if (useShort) {
                    if (ch == 'd') {
                        w = (short)w;
                    }
                    else {
                        w = (unsigned short)w;
                    }
                }
                *p++ = 'l';
#ifdef HAVE_LONG_LONG
                if (sizeof(long long) == sizeof(sawo_wide)) {
                    *p++ = 'l';
                }
#endif
            }

            *p++ = (char) ch;
            *p = '\0';

            
            if (width > length) {
                length = width;
            }
            if (gotPrecision) {
                length += precision;
            }

            
            if (num_buffer_size < length + 1) {
                num_buffer_size = length + 1;
                num_buffer = sawo_Realloc(num_buffer, num_buffer_size);
            }

            if (doubleType) {
                snprintf(num_buffer, length + 1, spec, d);
            }
            else {
                formatted_bytes = snprintf(num_buffer, length + 1, spec, w);
            }
            formatted_chars = formatted_bytes = strlen(num_buffer);
            formatted_buf = num_buffer;
            break;
        }

        default: {
            
            spec[0] = ch;
            spec[1] = '\0';
            sawo_SetResultFormatted(interp, "bad field specifier \"%s\"", spec);
            goto error;
        }
        }

        if (!gotMinus) {
            while (formatted_chars < width) {
                sawo_AppendString(interp, resultPtr, &pad, 1);
                formatted_chars++;
            }
        }

        sawo_AppendString(interp, resultPtr, formatted_buf, formatted_bytes);

        while (formatted_chars < width) {
            sawo_AppendString(interp, resultPtr, &pad, 1);
            formatted_chars++;
        }

        objIndex += gotSequential;
    }
    if (numBytes) {
        sawo_AppendString(interp, resultPtr, span, numBytes);
    }

    sawo_Free(num_buffer);
    return resultPtr;

  errorMsg:
    sawo_SetResultString(interp, msg, -1);
  error:
    sawo_FreeNewObj(interp, resultPtr);
    sawo_Free(num_buffer);
    return NULL;
}


#if defined(SAWO_REGEXP)
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>



#define REG_MAX_PAREN 100



#define	END	0	
#define	BOL	1	
#define	EOL	2	
#define	ANY	3	
#define	ANYOF	4	
#define	ANYBUT	5	
#define	BRANCH	6	
#define	BACK	7	
#define	EXACTLY	8	
#define	NOTHING	9	
#define	REP	10	
#define	REPMIN	11	
#define	REPX	12	
#define	REPXMIN	13	
#define	BOLX	14	
#define	EOLX	15	
#define	WORDA	16	
#define	WORDZ	17	

#define	OPENNC 	1000	
#define	OPEN   	1001	
			



#define	CLOSENC	2000 	
#define	CLOSE	2001 	
#define	CLOSE_END	(CLOSE+REG_MAX_PAREN)

#define	REG_MAGIC	0xFADED00D


#define	OP(preg, p)	(preg->program[p])
#define	NEXT(preg, p)	(preg->program[p + 1])
#define	OPERAND(p)	((p) + 2)




#define	FAIL(R,M)	{ (R)->err = (M); return (M); }
#define	ISMULT(c)	((c) == '*' || (c) == '+' || (c) == '?' || (c) == '{')
#define	META		"^$.[()|?{+*"

#define	HASWIDTH	1	
#define	SIMPLE		2	
#define	SPSTART		4	
#define	WORST		0	

#define MAX_REP_COUNT 1000000

static int reg(regex_t *preg, int paren , int *flagp );
static int regpiece(regex_t *preg, int *flagp );
static int regbranch(regex_t *preg, int *flagp );
static int regatom(regex_t *preg, int *flagp );
static int regnode(regex_t *preg, int op );
static int regnext(regex_t *preg, int p );
static void regc(regex_t *preg, int b );
static int reginsert(regex_t *preg, int op, int size, int opnd );
static void regtail(regex_t *preg, int p, int val);
static void regoptail(regex_t *preg, int p, int val );
static int regopsize(regex_t *preg, int p );

static int reg_range_find(const int *string, int c);
static const char *str_find(const char *string, int c, int nocase);
static int prefix_cmp(const int *prog, int proglen, const char *string, int nocase);


#ifdef DEBUG
static int regnarrate = 0;
static void regdump(regex_t *preg);
static const char *regprop( int op );
#endif


static int str_int_len(const int *seq)
{
	int n = 0;
	while (*seq++) {
		n++;
	}
	return n;
}

int regcomp(regex_t *preg, const char *exp, int cflags)
{
	int scan;
	int longest;
	unsigned len;
	int flags;

#ifdef DEBUG
	fprintf(stderr, "Compiling: '%s'\n", exp);
#endif
	memset(preg, 0, sizeof(*preg));

	if (exp == NULL)
		FAIL(preg, REG_ERR_NULL_ARGUMENT);

	
	preg->cflags = cflags;
	preg->regparse = exp;

	
	preg->proglen = (strlen(exp) + 1) * 5;
	preg->program = malloc(preg->proglen * sizeof(int));
	if (preg->program == NULL)
		FAIL(preg, REG_ERR_NOMEM);

	regc(preg, REG_MAGIC);
	if (reg(preg, 0, &flags) == 0) {
		return preg->err;
	}

	
	if (preg->re_nsub >= REG_MAX_PAREN)		
		FAIL(preg,REG_ERR_TOO_BIG);

	
	preg->regstart = 0;	
	preg->reganch = 0;
	preg->regmust = 0;
	preg->regmlen = 0;
	scan = 1;			
	if (OP(preg, regnext(preg, scan)) == END) {		
		scan = OPERAND(scan);

		
		if (OP(preg, scan) == EXACTLY) {
			preg->regstart = preg->program[OPERAND(scan)];
		}
		else if (OP(preg, scan) == BOL)
			preg->reganch++;

		if (flags&SPSTART) {
			longest = 0;
			len = 0;
			for (; scan != 0; scan = regnext(preg, scan)) {
				if (OP(preg, scan) == EXACTLY) {
					int plen = str_int_len(preg->program + OPERAND(scan));
					if (plen >= len) {
						longest = OPERAND(scan);
						len = plen;
					}
				}
			}
			preg->regmust = longest;
			preg->regmlen = len;
		}
	}

#ifdef DEBUG
	regdump(preg);
#endif

	return 0;
}

static int reg(regex_t *preg, int paren , int *flagp )
{
	int ret;
	int br;
	int ender;
	int parno = 0;
	int flags;

	*flagp = HASWIDTH;	

	
	if (paren) {
		if (preg->regparse[0] == '?' && preg->regparse[1] == ':') {
			
			preg->regparse += 2;
			parno = -1;
		}
		else {
			parno = ++preg->re_nsub;
		}
		ret = regnode(preg, OPEN+parno);
	} else
		ret = 0;

	
	br = regbranch(preg, &flags);
	if (br == 0)
		return 0;
	if (ret != 0)
		regtail(preg, ret, br);	
	else
		ret = br;
	if (!(flags&HASWIDTH))
		*flagp &= ~HASWIDTH;
	*flagp |= flags&SPSTART;
	while (*preg->regparse == '|') {
		preg->regparse++;
		br = regbranch(preg, &flags);
		if (br == 0)
			return 0;
		regtail(preg, ret, br);	
		if (!(flags&HASWIDTH))
			*flagp &= ~HASWIDTH;
		*flagp |= flags&SPSTART;
	}

	
	ender = regnode(preg, (paren) ? CLOSE+parno : END);
	regtail(preg, ret, ender);

	
	for (br = ret; br != 0; br = regnext(preg, br))
		regoptail(preg, br, ender);

	
	if (paren && *preg->regparse++ != ')') {
		preg->err = REG_ERR_UNMATCHED_PAREN;
		return 0;
	} else if (!paren && *preg->regparse != '\0') {
		if (*preg->regparse == ')') {
			preg->err = REG_ERR_UNMATCHED_PAREN;
			return 0;
		} else {
			preg->err = REG_ERR_JUNK_ON_END;
			return 0;
		}
	}

	return(ret);
}

static int regbranch(regex_t *preg, int *flagp )
{
	int ret;
	int chain;
	int latest;
	int flags;

	*flagp = WORST;		

	ret = regnode(preg, BRANCH);
	chain = 0;
	while (*preg->regparse != '\0' && *preg->regparse != ')' &&
	       *preg->regparse != '|') {
		latest = regpiece(preg, &flags);
		if (latest == 0)
			return 0;
		*flagp |= flags&HASWIDTH;
		if (chain == 0) {
			*flagp |= flags&SPSTART;
		}
		else {
			regtail(preg, chain, latest);
		}
		chain = latest;
	}
	if (chain == 0)	
		(void) regnode(preg, NOTHING);

	return(ret);
}

static int regpiece(regex_t *preg, int *flagp)
{
	int ret;
	char op;
	int next;
	int flags;
	int min;
	int max;

	ret = regatom(preg, &flags);
	if (ret == 0)
		return 0;

	op = *preg->regparse;
	if (!ISMULT(op)) {
		*flagp = flags;
		return(ret);
	}

	if (!(flags&HASWIDTH) && op != '?') {
		preg->err = REG_ERR_OPERAND_COULD_BE_EMPTY;
		return 0;
	}

	
	if (op == '{') {
		char *end;

		min = strtoul(preg->regparse + 1, &end, 10);
		if (end == preg->regparse + 1) {
			preg->err = REG_ERR_BAD_COUNT;
			return 0;
		}
		if (*end == '}') {
			max = min;
		}
		else {
			preg->regparse = end;
			max = strtoul(preg->regparse + 1, &end, 10);
			if (*end != '}') {
				preg->err = REG_ERR_UNMATCHED_BRACES;
				return 0;
			}
		}
		if (end == preg->regparse + 1) {
			max = MAX_REP_COUNT;
		}
		else if (max < min || max >= 100) {
			preg->err = REG_ERR_BAD_COUNT;
			return 0;
		}
		if (min >= 100) {
			preg->err = REG_ERR_BAD_COUNT;
			return 0;
		}

		preg->regparse = strchr(preg->regparse, '}');
	}
	else {
		min = (op == '+');
		max = (op == '?' ? 1 : MAX_REP_COUNT);
	}

	if (preg->regparse[1] == '?') {
		preg->regparse++;
		next = reginsert(preg, flags & SIMPLE ? REPMIN : REPXMIN, 5, ret);
	}
	else {
		next = reginsert(preg, flags & SIMPLE ? REP: REPX, 5, ret);
	}
	preg->program[ret + 2] = max;
	preg->program[ret + 3] = min;
	preg->program[ret + 4] = 0;

	*flagp = (min) ? (WORST|HASWIDTH) : (WORST|SPSTART);

	if (!(flags & SIMPLE)) {
		int back = regnode(preg, BACK);
		regtail(preg, back, ret);
		regtail(preg, next, back);
	}

	preg->regparse++;
	if (ISMULT(*preg->regparse)) {
		preg->err = REG_ERR_NESTED_COUNT;
		return 0;
	}

	return ret;
}

static void reg_addrange(regex_t *preg, int lower, int upper)
{
	if (lower > upper) {
		reg_addrange(preg, upper, lower);
	}
	
	regc(preg, upper - lower + 1);
	regc(preg, lower);
}

static void reg_addrange_str(regex_t *preg, const char *str)
{
	while (*str) {
		reg_addrange(preg, *str, *str);
		str++;
	}
}

static int reg_utf8_tounicode_case(const char *s, int *uc, int upper)
{
	int l = utf8_tounicode(s, uc);
	if (upper) {
		*uc = utf8_upper(*uc);
	}
	return l;
}

static int hexdigitval(int c)
{
	if (c >= '0' && c <= '9')
		return c - '0';
	if (c >= 'a' && c <= 'f')
		return c - 'a' + 10;
	if (c >= 'A' && c <= 'F')
		return c - 'A' + 10;
	return -1;
}

static int parse_hex(const char *s, int n, int *uc)
{
	int val = 0;
	int k;

	for (k = 0; k < n; k++) {
		int c = hexdigitval(*s++);
		if (c == -1) {
			break;
		}
		val = (val << 4) | c;
	}
	if (k) {
		*uc = val;
	}
	return k;
}

static int reg_decode_escape(const char *s, int *ch)
{
	int n;
	const char *s0 = s;

	*ch = *s++;

	switch (*ch) {
		case 'b': *ch = '\b'; break;
		case 'e': *ch = 27; break;
		case 'f': *ch = '\f'; break;
		case 'n': *ch = '\n'; break;
		case 'r': *ch = '\r'; break;
		case 't': *ch = '\t'; break;
		case 'v': *ch = '\v'; break;
		case 'u':
			if (*s == '{') {
				
				n = parse_hex(s + 1, 6, ch);
				if (n > 0 && s[n + 1] == '}' && *ch >= 0 && *ch <= 0x1fffff) {
					s += n + 2;
				}
				else {
					
					*ch = 'u';
				}
			}
			else if ((n = parse_hex(s, 4, ch)) > 0) {
				s += n;
			}
			break;
		case 'U':
			if ((n = parse_hex(s, 8, ch)) > 0) {
				s += n;
			}
			break;
		case 'x':
			if ((n = parse_hex(s, 2, ch)) > 0) {
				s += n;
			}
			break;
		case '\0':
			s--;
			*ch = '\\';
			break;
	}
	return s - s0;
}

static int regatom(regex_t *preg, int *flagp)
{
	int ret;
	int flags;
	int nocase = (preg->cflags & REG_ICASE);

	int ch;
	int n = reg_utf8_tounicode_case(preg->regparse, &ch, nocase);

	*flagp = WORST;		

	preg->regparse += n;
	switch (ch) {
	
	case '^':
		ret = regnode(preg, BOL);
		break;
	case '$':
		ret = regnode(preg, EOL);
		break;
	case '.':
		ret = regnode(preg, ANY);
		*flagp |= HASWIDTH|SIMPLE;
		break;
	case '[': {
			const char *pattern = preg->regparse;

			if (*pattern == '^') {	
				ret = regnode(preg, ANYBUT);
				pattern++;
			} else
				ret = regnode(preg, ANYOF);

			
			if (*pattern == ']' || *pattern == '-') {
				reg_addrange(preg, *pattern, *pattern);
				pattern++;
			}

			while (*pattern && *pattern != ']') {
				
				int start;
				int end;

				pattern += reg_utf8_tounicode_case(pattern, &start, nocase);
				if (start == '\\') {
					pattern += reg_decode_escape(pattern, &start);
					if (start == 0) {
						preg->err = REG_ERR_NULL_CHAR;
						return 0;
					}
				}
				if (pattern[0] == '-' && pattern[1] && pattern[1] != ']') {
					
					pattern += utf8_tounicode(pattern, &end);
					pattern += reg_utf8_tounicode_case(pattern, &end, nocase);
					if (end == '\\') {
						pattern += reg_decode_escape(pattern, &end);
						if (end == 0) {
							preg->err = REG_ERR_NULL_CHAR;
							return 0;
						}
					}

					reg_addrange(preg, start, end);
					continue;
				}
				if (start == '[' && pattern[0] == ':') {
					static const char *character_class[] = {
						":alpha:", ":alnum:", ":space:", ":blank:", ":upper:", ":lower:",
						":digit:", ":xdigit:", ":cntrl:", ":graph:", ":print:", ":punct:",
					};
					enum {
						CC_ALPHA, CC_ALNUM, CC_SPACE, CC_BLANK, CC_UPPER, CC_LOWER,
						CC_DIGIT, CC_XDIGIT, CC_CNTRL, CC_GRAPH, CC_PRINT, CC_PUNCT,
						CC_NUM
					};
					int i;

					for (i = 0; i < CC_NUM; i++) {
						int n = strlen(character_class[i]);
						if (strncmp(pattern, character_class[i], n) == 0) {
							
							pattern += n + 1;
							break;
						}
					}
					if (i != CC_NUM) {
						switch (i) {
							case CC_ALNUM:
								reg_addrange(preg, '0', '9');
								
							case CC_ALPHA:
								if ((preg->cflags & REG_ICASE) == 0) {
									reg_addrange(preg, 'a', 'z');
								}
								reg_addrange(preg, 'A', 'Z');
								break;
							case CC_SPACE:
								reg_addrange_str(preg, " \t\r\n\f\v");
								break;
							case CC_BLANK:
								reg_addrange_str(preg, " \t");
								break;
							case CC_UPPER:
								reg_addrange(preg, 'A', 'Z');
								break;
							case CC_LOWER:
								reg_addrange(preg, 'a', 'z');
								break;
							case CC_XDIGIT:
								reg_addrange(preg, 'a', 'f');
								reg_addrange(preg, 'A', 'F');
								
							case CC_DIGIT:
								reg_addrange(preg, '0', '9');
								break;
							case CC_CNTRL:
								reg_addrange(preg, 0, 31);
								reg_addrange(preg, 127, 127);
								break;
							case CC_PRINT:
								reg_addrange(preg, ' ', '~');
								break;
							case CC_GRAPH:
								reg_addrange(preg, '!', '~');
								break;
							case CC_PUNCT:
								reg_addrange(preg, '!', '/');
								reg_addrange(preg, ':', '@');
								reg_addrange(preg, '[', '`');
								reg_addrange(preg, '{', '~');
								break;
						}
						continue;
					}
				}
				
				reg_addrange(preg, start, start);
			}
			regc(preg, '\0');

			if (*pattern) {
				pattern++;
			}
			preg->regparse = pattern;

			*flagp |= HASWIDTH|SIMPLE;
		}
		break;
	case '(':
		ret = reg(preg, 1, &flags);
		if (ret == 0)
			return 0;
		*flagp |= flags&(HASWIDTH|SPSTART);
		break;
	case '\0':
	case '|':
	case ')':
		preg->err = REG_ERR_INTERNAL;
		return 0;	
	case '?':
	case '+':
	case '*':
	case '{':
		preg->err = REG_ERR_COUNT_FOLLOWS_NOTHING;
		return 0;
	case '\\':
		ch = *preg->regparse++;
		switch (ch) {
		case '\0':
			preg->err = REG_ERR_TRAILING_BACKSLASH;
			return 0;
		case 'A':
			ret = regnode(preg, BOLX);
			break;
		case 'Z':
			ret = regnode(preg, EOLX);
			break;
		case '<':
		case 'm':
			ret = regnode(preg, WORDA);
			break;
		case '>':
		case 'M':
			ret = regnode(preg, WORDZ);
			break;
		case 'd':
		case 'D':
			ret = regnode(preg, ch == 'd' ? ANYOF : ANYBUT);
			reg_addrange(preg, '0', '9');
			regc(preg, '\0');
			*flagp |= HASWIDTH|SIMPLE;
			break;
		case 'w':
		case 'W':
			ret = regnode(preg, ch == 'w' ? ANYOF : ANYBUT);
			if ((preg->cflags & REG_ICASE) == 0) {
				reg_addrange(preg, 'a', 'z');
			}
			reg_addrange(preg, 'A', 'Z');
			reg_addrange(preg, '0', '9');
			reg_addrange(preg, '_', '_');
			regc(preg, '\0');
			*flagp |= HASWIDTH|SIMPLE;
			break;
		case 's':
		case 'S':
			ret = regnode(preg, ch == 's' ? ANYOF : ANYBUT);
			reg_addrange_str(preg," \t\r\n\f\v");
			regc(preg, '\0');
			*flagp |= HASWIDTH|SIMPLE;
			break;
		
		default:
			
			
			preg->regparse--;
			goto de_fault;
		}
		break;
	de_fault:
	default: {
			int added = 0;

			
			preg->regparse -= n;

			ret = regnode(preg, EXACTLY);


			
			while (*preg->regparse && strchr(META, *preg->regparse) == NULL) {
				n = reg_utf8_tounicode_case(preg->regparse, &ch, (preg->cflags & REG_ICASE));
				if (ch == '\\' && preg->regparse[n]) {
					if (strchr("<>mMwWdDsSAZ", preg->regparse[n])) {
						
						break;
					}
					n += reg_decode_escape(preg->regparse + n, &ch);
					if (ch == 0) {
						preg->err = REG_ERR_NULL_CHAR;
						return 0;
					}
				}


				if (ISMULT(preg->regparse[n])) {
					
					if (added) {
						
						break;
					}
					
					regc(preg, ch);
					added++;
					preg->regparse += n;
					break;
				}

				
				regc(preg, ch);
				added++;
				preg->regparse += n;
			}
			regc(preg, '\0');

			*flagp |= HASWIDTH;
			if (added == 1)
				*flagp |= SIMPLE;
			break;
		}
		break;
	}

	return(ret);
}

static void reg_grow(regex_t *preg, int n)
{
	if (preg->p + n >= preg->proglen) {
		preg->proglen = (preg->p + n) * 2;
		preg->program = realloc(preg->program, preg->proglen * sizeof(int));
	}
}


static int regnode(regex_t *preg, int op)
{
	reg_grow(preg, 2);

	
	preg->program[preg->p++] = op;
	preg->program[preg->p++] = 0;

	
	return preg->p - 2;
}

static void regc(regex_t *preg, int b )
{
	reg_grow(preg, 1);
	preg->program[preg->p++] = b;
}

static int reginsert(regex_t *preg, int op, int size, int opnd )
{
	reg_grow(preg, size);

	
	memmove(preg->program + opnd + size, preg->program + opnd, sizeof(int) * (preg->p - opnd));
	
	memset(preg->program + opnd, 0, sizeof(int) * size);

	preg->program[opnd] = op;

	preg->p += size;

	return opnd + size;
}

static void regtail(regex_t *preg, int p, int val)
{
	int scan;
	int temp;
	int offset;

	
	scan = p;
	for (;;) {
		temp = regnext(preg, scan);
		if (temp == 0)
			break;
		scan = temp;
	}

	if (OP(preg, scan) == BACK)
		offset = scan - val;
	else
		offset = val - scan;

	preg->program[scan + 1] = offset;
}


static void regoptail(regex_t *preg, int p, int val )
{
	
	if (p != 0 && OP(preg, p) == BRANCH) {
		regtail(preg, OPERAND(p), val);
	}
}


static int regtry(regex_t *preg, const char *string );
static int regmatch(regex_t *preg, int prog);
static int regrepeat(regex_t *preg, int p, int max);

int regexec(regex_t  *preg,  const  char *string, size_t nmatch, regmatch_t pmatch[], int eflags)
{
	const char *s;
	int scan;

	
	if (preg == NULL || preg->program == NULL || string == NULL) {
		return REG_ERR_NULL_ARGUMENT;
	}

	
	if (*preg->program != REG_MAGIC) {
		return REG_ERR_CORRUPTED;
	}

#ifdef DEBUG
	fprintf(stderr, "regexec: %s\n", string);
	regdump(preg);
#endif

	preg->eflags = eflags;
	preg->pmatch = pmatch;
	preg->nmatch = nmatch;
	preg->start = string;	

	
	for (scan = OPERAND(1); scan != 0; scan += regopsize(preg, scan)) {
		int op = OP(preg, scan);
		if (op == END)
			break;
		if (op == REPX || op == REPXMIN)
			preg->program[scan + 4] = 0;
	}

	
	if (preg->regmust != 0) {
		s = string;
		while ((s = str_find(s, preg->program[preg->regmust], preg->cflags & REG_ICASE)) != NULL) {
			if (prefix_cmp(preg->program + preg->regmust, preg->regmlen, s, preg->cflags & REG_ICASE) >= 0) {
				break;
			}
			s++;
		}
		if (s == NULL)	
			return REG_NOMATCH;
	}

	
	preg->regbol = string;

	
	if (preg->reganch) {
		if (eflags & REG_NOTBOL) {
			
			goto nextline;
		}
		while (1) {
			if (regtry(preg, string)) {
				return REG_NOERROR;
			}
			if (*string) {
nextline:
				if (preg->cflags & REG_NEWLINE) {
					
					string = strchr(string, '\n');
					if (string) {
						preg->regbol = ++string;
						continue;
					}
				}
			}
			return REG_NOMATCH;
		}
	}

	
	s = string;
	if (preg->regstart != '\0') {
		
		while ((s = str_find(s, preg->regstart, preg->cflags & REG_ICASE)) != NULL) {
			if (regtry(preg, s))
				return REG_NOERROR;
			s++;
		}
	}
	else
		
		while (1) {
			if (regtry(preg, s))
				return REG_NOERROR;
			if (*s == '\0') {
				break;
			}
			else {
				int c;
				s += utf8_tounicode(s, &c);
			}
		}

	
	return REG_NOMATCH;
}

			
static int regtry( regex_t *preg, const char *string )
{
	int i;

	preg->reginput = string;

	for (i = 0; i < preg->nmatch; i++) {
		preg->pmatch[i].rm_so = -1;
		preg->pmatch[i].rm_eo = -1;
	}
	if (regmatch(preg, 1)) {
		preg->pmatch[0].rm_so = string - preg->start;
		preg->pmatch[0].rm_eo = preg->reginput - preg->start;
		return(1);
	} else
		return(0);
}

static int prefix_cmp(const int *prog, int proglen, const char *string, int nocase)
{
	const char *s = string;
	while (proglen && *s) {
		int ch;
		int n = reg_utf8_tounicode_case(s, &ch, nocase);
		if (ch != *prog) {
			return -1;
		}
		prog++;
		s += n;
		proglen--;
	}
	if (proglen == 0) {
		return s - string;
	}
	return -1;
}

static int reg_range_find(const int *range, int c)
{
	while (*range) {
		
		if (c >= range[1] && c <= (range[0] + range[1] - 1)) {
			return 1;
		}
		range += 2;
	}
	return 0;
}

static const char *str_find(const char *string, int c, int nocase)
{
	if (nocase) {
		
		c = utf8_upper(c);
	}
	while (*string) {
		int ch;
		int n = reg_utf8_tounicode_case(string, &ch, nocase);
		if (c == ch) {
			return string;
		}
		string += n;
	}
	return NULL;
}

static int reg_iseol(regex_t *preg, int ch)
{
	if (preg->cflags & REG_NEWLINE) {
		return ch == '\0' || ch == '\n';
	}
	else {
		return ch == '\0';
	}
}

static int regmatchsimplerepeat(regex_t *preg, int scan, int matchmin)
{
	int nextch = '\0';
	const char *save;
	int no;
	int c;

	int max = preg->program[scan + 2];
	int min = preg->program[scan + 3];
	int next = regnext(preg, scan);

	if (OP(preg, next) == EXACTLY) {
		nextch = preg->program[OPERAND(next)];
	}
	save = preg->reginput;
	no = regrepeat(preg, scan + 5, max);
	if (no < min) {
		return 0;
	}
	if (matchmin) {
		
		max = no;
		no = min;
	}
	
	while (1) {
		if (matchmin) {
			if (no > max) {
				break;
			}
		}
		else {
			if (no < min) {
				break;
			}
		}
		preg->reginput = save + utf8_index(save, no);
		reg_utf8_tounicode_case(preg->reginput, &c, (preg->cflags & REG_ICASE));
		
		if (reg_iseol(preg, nextch) || c == nextch) {
			if (regmatch(preg, next)) {
				return(1);
			}
		}
		if (matchmin) {
			
			no++;
		}
		else {
			
			no--;
		}
	}
	return(0);
}

static int regmatchrepeat(regex_t *preg, int scan, int matchmin)
{
	int *scanpt = preg->program + scan;

	int max = scanpt[2];
	int min = scanpt[3];

	
	if (scanpt[4] < min) {
		
		scanpt[4]++;
		if (regmatch(preg, scan + 5)) {
			return 1;
		}
		scanpt[4]--;
		return 0;
	}
	if (scanpt[4] > max) {
		return 0;
	}

	if (matchmin) {
		
		if (regmatch(preg, regnext(preg, scan))) {
			return 1;
		}
		
		scanpt[4]++;
		if (regmatch(preg, scan + 5)) {
			return 1;
		}
		scanpt[4]--;
		return 0;
	}
	
	if (scanpt[4] < max) {
		scanpt[4]++;
		if (regmatch(preg, scan + 5)) {
			return 1;
		}
		scanpt[4]--;
	}
	
	return regmatch(preg, regnext(preg, scan));
}


static int regmatch(regex_t *preg, int prog)
{
	int scan;	
	int next;		
	const char *save;

	scan = prog;

#ifdef DEBUG
	if (scan != 0 && regnarrate)
		fprintf(stderr, "%s(\n", regprop(scan));
#endif
	while (scan != 0) {
		int n;
		int c;
#ifdef DEBUG
		if (regnarrate) {
			fprintf(stderr, "%3d: %s...\n", scan, regprop(OP(preg, scan)));	
		}
#endif
		next = regnext(preg, scan);
		n = reg_utf8_tounicode_case(preg->reginput, &c, (preg->cflags & REG_ICASE));

		switch (OP(preg, scan)) {
		case BOLX:
			if ((preg->eflags & REG_NOTBOL)) {
				return(0);
			}
			
		case BOL:
			if (preg->reginput != preg->regbol) {
				return(0);
			}
			break;
		case EOLX:
			if (c != 0) {
				
				return 0;
			}
			break;
		case EOL:
			if (!reg_iseol(preg, c)) {
				return(0);
			}
			break;
		case WORDA:
			
			if ((!isalnum(UCHAR(c))) && c != '_')
				return(0);
			
			if (preg->reginput > preg->regbol &&
				(isalnum(UCHAR(preg->reginput[-1])) || preg->reginput[-1] == '_'))
				return(0);
			break;
		case WORDZ:
			
			if (preg->reginput > preg->regbol) {
				
				if (reg_iseol(preg, c) || !isalnum(UCHAR(c)) || c != '_') {
					c = preg->reginput[-1];
					
					if (isalnum(UCHAR(c)) || c == '_') {
						break;
					}
				}
			}
			
			return(0);

		case ANY:
			if (reg_iseol(preg, c))
				return 0;
			preg->reginput += n;
			break;
		case EXACTLY: {
				int opnd;
				int len;
				int slen;

				opnd = OPERAND(scan);
				len = str_int_len(preg->program + opnd);

				slen = prefix_cmp(preg->program + opnd, len, preg->reginput, preg->cflags & REG_ICASE);
				if (slen < 0) {
					return(0);
				}
				preg->reginput += slen;
			}
			break;
		case ANYOF:
			if (reg_iseol(preg, c) || reg_range_find(preg->program + OPERAND(scan), c) == 0) {
				return(0);
			}
			preg->reginput += n;
			break;
		case ANYBUT:
			if (reg_iseol(preg, c) || reg_range_find(preg->program + OPERAND(scan), c) != 0) {
				return(0);
			}
			preg->reginput += n;
			break;
		case NOTHING:
			break;
		case BACK:
			break;
		case BRANCH:
			if (OP(preg, next) != BRANCH)		
				next = OPERAND(scan);	
			else {
				do {
					save = preg->reginput;
					if (regmatch(preg, OPERAND(scan))) {
						return(1);
					}
					preg->reginput = save;
					scan = regnext(preg, scan);
				} while (scan != 0 && OP(preg, scan) == BRANCH);
				return(0);
				
			}
			break;
		case REP:
		case REPMIN:
			return regmatchsimplerepeat(preg, scan, OP(preg, scan) == REPMIN);

		case REPX:
		case REPXMIN:
			return regmatchrepeat(preg, scan, OP(preg, scan) == REPXMIN);

		case END:
			return 1;	

		case OPENNC:
		case CLOSENC:
			return regmatch(preg, next);

		default:
			if (OP(preg, scan) >= OPEN+1 && OP(preg, scan) < CLOSE_END) {
				save = preg->reginput;
				if (regmatch(preg, next)) {
					if (OP(preg, scan) < CLOSE) {
						int no = OP(preg, scan) - OPEN;
						if (no < preg->nmatch && preg->pmatch[no].rm_so == -1) {
							preg->pmatch[no].rm_so = save - preg->start;
						}
					}
					else {
						int no = OP(preg, scan) - CLOSE;
						if (no < preg->nmatch && preg->pmatch[no].rm_eo == -1) {
							preg->pmatch[no].rm_eo = save - preg->start;
						}
					}
					return(1);
				}
				return(0);
			}
			return REG_ERR_INTERNAL;
		}

		scan = next;
	}

	return REG_ERR_INTERNAL;
}

static int regrepeat(regex_t *preg, int p, int max)
{
	int count = 0;
	const char *scan;
	int opnd;
	int ch;
	int n;

	scan = preg->reginput;
	opnd = OPERAND(p);
	switch (OP(preg, p)) {
	case ANY:
		
		while (!reg_iseol(preg, *scan) && count < max) {
			count++;
			scan++;
		}
		break;
	case EXACTLY:
		while (count < max) {
			n = reg_utf8_tounicode_case(scan, &ch, preg->cflags & REG_ICASE);
			if (preg->program[opnd] != ch) {
				break;
			}
			count++;
			scan += n;
		}
		break;
	case ANYOF:
		while (count < max) {
			n = reg_utf8_tounicode_case(scan, &ch, preg->cflags & REG_ICASE);
			if (reg_iseol(preg, ch) || reg_range_find(preg->program + opnd, ch) == 0) {
				break;
			}
			count++;
			scan += n;
		}
		break;
	case ANYBUT:
		while (count < max) {
			n = reg_utf8_tounicode_case(scan, &ch, preg->cflags & REG_ICASE);
			if (reg_iseol(preg, ch) || reg_range_find(preg->program + opnd, ch) != 0) {
				break;
			}
			count++;
			scan += n;
		}
		break;
	default:		
		preg->err = REG_ERR_INTERNAL;
		count = 0;	
		break;
	}
	preg->reginput = scan;

	return(count);
}

static int regnext(regex_t *preg, int p )
{
	int offset;

	offset = NEXT(preg, p);

	if (offset == 0)
		return 0;

	if (OP(preg, p) == BACK)
		return(p-offset);
	else
		return(p+offset);
}

static int regopsize(regex_t *preg, int p )
{
	
	switch (OP(preg, p)) {
		case REP:
		case REPMIN:
		case REPX:
		case REPXMIN:
			return 5;

		case ANYOF:
		case ANYBUT:
		case EXACTLY: {
			int s = p + 2;
			while (preg->program[s++]) {
			}
			return s - p;
		}
	}
	return 2;
}


size_t regerror(int errcode, const regex_t *preg, char *errbuf,  size_t errbuf_size)
{
	static const char *error_strings[] = {
		"success",
		"no match",
		"bad pattern",
		"null argument",
		"unknown error",
		"too big",
		"out of memory",
		"too many ()",
		"parentheses () not balanced",
		"braces {} not balanced",
		"invalid repetition count(s)",
		"extra characters",
		"*+ of empty atom",
		"nested count",
		"internal error",
		"count follows nothing",
		"trailing backslash",
		"corrupted program",
		"contains null char",
	};
	const char *err;

	if (errcode < 0 || errcode >= REG_ERR_NUM) {
		err = "Bad error code";
	}
	else {
		err = error_strings[errcode];
	}

	return snprintf(errbuf, errbuf_size, "%s", err);
}

void regfree(regex_t *preg)
{
	free(preg->program);
}

#endif

#if defined(_WIN32) || defined(WIN32)
#ifndef STRICT
#define STRICT
#endif
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#if defined(HAVE_DLOPEN_COMPAT)
void *dlopen(const char *path, int mode)
{
    SAWO_NOTUSED(mode);

    return (void *)LoadLibraryA(path);
}

int dlclose(void *handle)
{
    FreeLibrary((HANDLE)handle);
    return 0;
}

void *dlsym(void *handle, const char *symbol)
{
    return GetRaiseAddress((HMODULE)handle, symbol);
}

char *dlerror(void)
{
    static char msg[121];
    FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM, NULL, GetLastError(),
                   LANG_NEUTRAL, msg, sizeof(msg) - 1, NULL);
    return msg;
}
#endif

#ifdef _MSC_VER

#include <sys/timeb.h>


int gettimeofday(struct timeval *tv, void *unused)
{
    struct _timeb tb;

    _ftime(&tb);
    tv->tv_sec = tb.time;
    tv->tv_usec = tb.millitm * 1000;

    return 0;
}


DIR *opendir(const char *name)
{
    DIR *dir = 0;

    if (name && name[0]) {
        size_t base_length = strlen(name);
        const char *all =       
            strchr("/\\", name[base_length - 1]) ? "*" : "/*";

        if ((dir = (DIR *) sawo_Alloc(sizeof *dir)) != 0 &&
            (dir->name = (char *)sawo_Alloc(base_length + strlen(all) + 1)) != 0) {
            strcat(strcpy(dir->name, name), all);

            if ((dir->handle = (long)_findfirst(dir->name, &dir->show)) != -1)
                dir->result.d_name = 0;
            else {              
                sawo_Free(dir->name);
                sawo_Free(dir);
                dir = 0;
            }
        }
        else {                  
            sawo_Free(dir);
            dir = 0;
            errno = ENOMEM;
        }
    }
    else {
        errno = EINVAL;
    }
    return dir;
}

int closedir(DIR * dir)
{
    int result = -1;

    if (dir) {
        if (dir->handle != -1)
            result = _findclose(dir->handle);
        sawo_Free(dir->name);
        sawo_Free(dir);
    }
    if (result == -1)           
        errno = EBADF;
    return result;
}

struct dirent *readdir(DIR * dir)
{
    struct dirent *result = 0;

    if (dir && dir->handle != -1) {
        if (!dir->result.d_name || _findnext(dir->handle, &dir->show) != -1) {
            result = &dir->result;
            result->d_name = dir->show.name;
        }
    }
    else {
        errno = EBADF;
    }
    return result;
}
#endif
#endif


/* //////////////////////////////////////////////////////////////////////// */


#ifndef SAWO_BOOTSTRAP_LIB_ONLY
#include <errno.h>
#include <string.h>


#ifdef USE_LINENOISE
#ifdef HAVE_UNISTD_H
    #include <unistd.h>
#endif
#include "linenoise.h"
#else
#define MAX_LINE_LEN 512
#endif

char *sawo_HistoryGetline(const char *prompt)
{
#ifdef USE_LINENOISE
    return linenoise(prompt);
#else
    int len;
    char *line = malloc(MAX_LINE_LEN);

    fputs(prompt, stdout);
    fflush(stdout);

    if (fgets(line, MAX_LINE_LEN, stdin) == NULL) {
        free(line);
        return NULL;
    }
    len = strlen(line);
    if (len && line[len - 1] == '\n') {
        line[len - 1] = '\0';
    }
    return line;
#endif
}

void sawo_HistoryLoad(const char *filename)
{
#ifdef USE_LINENOISE
    linenoiseHistoryLoad(filename);
#endif
}

void sawo_HistoryAdd(const char *line)
{
#ifdef USE_LINENOISE
    linenoiseHistoryAdd(line);
#endif
}

void sawo_HistorySave(const char *filename)
{
#ifdef USE_LINENOISE
    linenoiseHistorySave(filename);
#endif
}

void sawo_HistoryShow(void)
{
#ifdef USE_LINENOISE
    
    int i;
    int len;
    char **history = linenoiseHistory(&len);
    for (i = 0; i < len; i++) {
        printf("%4d %s\n", i + 1, history[i]);
    }
#endif
}

int sawo_InteractivePrompt(sawo_Interp *interp)
{
    int retcode = SAWO_OK;
    char *history_file = NULL;
#ifdef USE_LINENOISE
    const char *home;

    home = getenv("HOME");
    if (home && isatty(STDIN_FILENO)) {
        int history_len = strlen(home) + sizeof("/.sawo_history");
        history_file = sawo_Alloc(history_len);
        snprintf(history_file, history_len, "%s/.sawo_history", home);
        sawo_HistoryLoad(history_file);
    }
#endif

    printf("Hyang scripting version %s.%s (%s / %s-%s-%s) -- %s\n",
        HYANG_MAJOR, HYANG_MINOR, HYANG_HYSCM_CHECKIN, HYANG_YEAR,
        HYANG_MONTH, HYANG_DAY, HYANG_NICK);
    sawo_SetVariableStrWithStr(interp, SAWO_INTERACTIVE, "1");

    while (1) {
        sawo_Obj *scriptObjPtr;
        const char *result;
        int reslen;
        char prompt[20];

        if (retcode != SAWO_OK) {
            const char *retcodestr = sawo_ReturnCode(retcode);

            if (*retcodestr == '?') {
                snprintf(prompt, sizeof(prompt) - 3, "[%d] > ", retcode);
            }
            else {
                snprintf(prompt, sizeof(prompt) - 3, "[%s] > ", retcodestr);
            }
        }
        else {
            strcpy(prompt, "> ");
        }

        scriptObjPtr = sawo_NewStringObj(interp, "", 0);
        sawo_IncrRefCount(scriptObjPtr);
        while (1) {
            char state;
            char *line;

            line = sawo_HistoryGetline(prompt);
            if (line == NULL) {
                if (errno == EINTR) {
                    continue;
                }
                sawo_DecrRefCount(interp, scriptObjPtr);
                retcode = SAWO_OK;
                goto out;
            }
            if (sawo_Length(scriptObjPtr) != 0) {

                sawo_AppendString(interp, scriptObjPtr, "\n", 1);
            }
            sawo_AppendString(interp, scriptObjPtr, line, -1);
            free(line);
            if (sawo_ScriptIsComplete(interp, scriptObjPtr, &state))
                break;

            snprintf(prompt, sizeof(prompt), "%c> ", state);
        }
#ifdef USE_LINENOISE
        if (strcmp(sawo_String(scriptObjPtr), "h") == 0) {

            sawo_HistoryShow();
            sawo_DecrRefCount(interp, scriptObjPtr);
            continue;
        }

        sawo_HistoryAdd(sawo_String(scriptObjPtr));
        if (history_file) {
            sawo_HistorySave(history_file);
        }
#endif
        retcode = sawo_EvalObj(interp, scriptObjPtr);
        sawo_DecrRefCount(interp, scriptObjPtr);

        if (retcode == SAWO_EXIT) {
            break;
        }
        if (retcode == SAWO_ERR) {
            sawo_MakeErrorMessage(interp);
        }
        result = sawo_GetString(sawo_GetResult(interp), &reslen);
        if (reslen) {
            printf("%s\n", result);
        }
    }
  out:
    sawo_Free(history_file);
    return retcode;
}


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


extern int sawo_initsawoInit(sawo_Interp *interp);

static void sawoSetArgv(sawo_Interp *interp, int argc, char *const argv[])
{
    int n;
    sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);


    for (n = 0; n < argc; n++) {
        sawo_Obj *obj = sawo_NewStringObj(interp, argv[n], -1);

        sawo_ListAppendElement(interp, listObj, obj);
    }

    sawo_SetVariableStr(interp, "argv", listObj);
    sawo_SetVariableStr(interp, "argc", sawo_NewIntObj(interp, argc));
}

static void sawoPrintErrorMessage(sawo_Interp *interp)
{
    sawo_MakeErrorMessage(interp);
    fprintf(stderr, "%s\n", sawo_String(sawo_GetResult(interp)));
}

void usage(const char* executable_name)
{
    printf("Hyang scripting version %s.%s (%s / %s-%s-%s) -- %s\n", HYANG_MAJOR,
    HYANG_MINOR, HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY, HYANG_NICK);
    printf("Usage: %s\n", executable_name);
    printf("or   : %s [options] [filename]\n", executable_name);
    printf("\n");
    printf("Without options: Interactive mode\n");
    printf("\n");
    printf("Options:\n");
    printf("         --version  : prints the version string\n");
    printf("         --help     : prints this text\n");
    printf("         -e CMD     : executes command CMD\n");
    printf("                      NOTE: all subsequent options will be passed as arguments to the command\n");
    printf("         [filename] : executes the script contained in the named file\n");
    printf("                      NOTE: all subsequent options will be passed to the script\n\n");
}

int main(int argc, char *const argv[])
{
    int retcode;
    sawo_Interp *interp;
    char *const orig_argv0 = argv[0];

    if (argc > 1 && strcmp(argv[1], "--version") == 0) {
        printf("%s.%s (%s / %s-%s-%s) -- %s\n", HYANG_MAJOR, HYANG_MINOR,
        HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY, HYANG_NICK);
        return 0;
    }
    else if (argc > 1 && strcmp(argv[1], "--help") == 0) {
        usage(argv[0]);
        return 0;
    }

    interp = sawo_CreateInterp();
    sawo_RegisterCoreCommands(interp);

    if (sawo_InitStaticExtensions(interp) != SAWO_OK) {
        sawoPrintErrorMessage(interp);
    }

    sawo_SetVariableStrWithStr(interp, "sawo::argv0", orig_argv0);
    sawo_SetVariableStrWithStr(interp, SAWO_INTERACTIVE, argc == 1 ? "1" : "0");
    retcode = sawo_initsawoInit(interp);

    if (argc == 1) {
        if (retcode == SAWO_ERR) {
            sawoPrintErrorMessage(interp);
        }
        if (retcode != SAWO_EXIT) {
            sawoSetArgv(interp, 0, NULL);
            retcode = sawo_InteractivePrompt(interp);
        }
    }
    else {
        if (argc > 2 && strcmp(argv[1], "-e") == 0) {
            sawoSetArgv(interp, argc - 3, argv + 3);
            retcode = sawo_Eval(interp, argv[2]);
            if (retcode != SAWO_ERR) {
                printf("%s\n", sawo_String(sawo_GetResult(interp)));
            }
        }
        else {
            sawo_SetVariableStr(interp, "argv0", sawo_NewStringObj(interp, argv[1], -1));
            sawoSetArgv(interp, argc - 2, argv + 2);
            retcode = sawo_EvalFile(interp, argv[1]);
        }
        if (retcode == SAWO_ERR) {
            sawoPrintErrorMessage(interp);
        }
    }
    if (retcode == SAWO_EXIT) {
        retcode = sawo_GetExitCode(interp);
    }
    else if (retcode == SAWO_ERR) {
        retcode = 1;
    }
    else {
        retcode = 0;
    }
    sawo_FreeInterp(interp);
    return retcode;
}
#endif

