/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#include <errno.h>
#include <stdio.h>
#include <string.h>

#ifdef HAVE_DIRENT_H
#include <dirent.h>
#endif

int sawo_ReaddirCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    const char *dirPath;
    DIR *dirPtr;
    struct dirent *entryPtr;
    int nocomplain = 0;

    if (argc == 3 && sawo_CompareStringImmediate(interp, argv[1], "-nocomplain")) {
        nocomplain = 1;
    }
    if (argc != 2 && !nocomplain) {
        sawo_WrongNumArgs(interp, 1, argv, "?-nocomplain? dirPath");
        return SAWO_ERR;
    }

    dirPath = sawo_String(argv[1 + nocomplain]);

    dirPtr = opendir(dirPath);
    if (dirPtr == NULL) {
        if (nocomplain) {
            return SAWO_OK;
        }
        sawo_SetResultString(interp, strerror(errno), -1);
        return SAWO_ERR;
    }
    else {
        sawo_Obj *listObj = sawo_NewListObj(interp, NULL, 0);

        while ((entryPtr = readdir(dirPtr)) != NULL) {
            if (entryPtr->d_name[0] == '.') {
                if (entryPtr->d_name[1] == '\0') {
                    continue;
                }
                if ((entryPtr->d_name[1] == '.') && (entryPtr->d_name[2] == '\0'))
                    continue;
            }
            sawo_ListAppendElement(interp, listObj, sawo_NewStringObj(interp, entryPtr->d_name, -1));
        }
        closedir(dirPtr);

        sawo_SetResult(interp, listObj);

        return SAWO_OK;
    }
}

int sawo_readdirInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "readdir", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "readdir", sawo_ReaddirCmd, NULL, NULL);
    return SAWO_OK;
}

#include <stdlib.h>
#include <string.h>

#if defined(SAWO_REGEXP)
#else
    #include <regex.h>
#endif

static void FreeRegexpInternalRep(sawo_Interp *interp, sawo_Obj *objPtr)
{
    regfree(objPtr->internalRep.regexpValue.compre);
    sawo_Free(objPtr->internalRep.regexpValue.compre);
}

static const sawo_ObjType regexpObjType = {
    "regexp",
    FreeRegexpInternalRep,
    NULL,
    NULL,
    SAWO_TYPE_NONE
};

static regex_t *SetRegexpFromAny(sawo_Interp *interp, sawo_Obj *objPtr, unsigned flags)
{
    regex_t *compre;
    const char *pattern;
    int ret;

    if (objPtr->typePtr == &regexpObjType &&
        objPtr->internalRep.regexpValue.compre && objPtr->internalRep.regexpValue.flags == flags) {
        return objPtr->internalRep.regexpValue.compre;
    }

    pattern = sawo_String(objPtr);
    compre = sawo_Alloc(sizeof(regex_t));

    if ((ret = regcomp(compre, pattern, REG_EXTENDED | flags)) != 0) {
        char buf[100];

        regerror(ret, compre, buf, sizeof(buf));
        sawo_SetResultFormatted(interp, "couldn't compile regular expression pattern: %s", buf);
        regfree(compre);
        sawo_Free(compre);
        return NULL;
    }

    sawo_FreeIntRep(interp, objPtr);

    objPtr->typePtr = &regexpObjType;
    objPtr->internalRep.regexpValue.flags = flags;
    objPtr->internalRep.regexpValue.compre = compre;

    return compre;
}

int sawo_RegexpCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int opt_indices = 0;
    int opt_all = 0;
    int opt_inline = 0;
    regex_t *regex;
    int match, i, j;
    int offset = 0;
    regmatch_t *pmatch = NULL;
    int source_len;
    int result = SAWO_OK;
    const char *pattern;
    const char *source_str;
    int num_matches = 0;
    int num_vars;
    sawo_Obj *resultListObj = NULL;
    int regcomp_flags = 0;
    int eflags = 0;
    int option;
    enum {
        OPT_INDICES,  OPT_NOCASE, OPT_LINE, OPT_ALL, OPT_INLINE, OPT_START, OPT_END
    };
    static const char * const options[] = {
        "-indices", "-nocase", "-line", "-all", "-inline", "-start", "--", NULL
    };

    if (argc < 3) {
      wrongNumArgs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-switch ...? exp string ?matchVar? ?subMatchVar ...?");
        return SAWO_ERR;
    }

    for (i = 1; i < argc; i++) {
        const char *opt = sawo_String(argv[i]);

        if (*opt != '-') {
            break;
        }
        if (sawo_GetEnum(interp, argv[i], options, &option, "switch", SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (option == OPT_END) {
            i++;
            break;
        }
        switch (option) {
            case OPT_INDICES:
                opt_indices = 1;
                break;

            case OPT_NOCASE:
                regcomp_flags |= REG_ICASE;
                break;

            case OPT_LINE:
                regcomp_flags |= REG_NEWLINE;
                break;

            case OPT_ALL:
                opt_all = 1;
                break;

            case OPT_INLINE:
                opt_inline = 1;
                break;

            case OPT_START:
                if (++i == argc) {
                    goto wrongNumArgs;
                }
                if (sawo_GetIndex(interp, argv[i], &offset) != SAWO_OK) {
                    return SAWO_ERR;
                }
                break;
        }
    }
    if (argc - i < 2) {
        goto wrongNumArgs;
    }

    regex = SetRegexpFromAny(interp, argv[i], regcomp_flags);
    if (!regex) {
        return SAWO_ERR;
    }

    pattern = sawo_String(argv[i]);
    source_str = sawo_GetString(argv[i + 1], &source_len);

    num_vars = argc - i - 2;

    if (opt_inline) {
        if (num_vars) {
            sawo_SetResultString(interp, "regexp match variables not allowed when using -inline",
                -1);
            result = SAWO_ERR;
            goto done;
        }
        num_vars = regex->re_nsub + 1;
    }

    pmatch = sawo_Alloc((num_vars + 1) * sizeof(*pmatch));

    if (offset) {
        if (offset < 0) {
            offset += source_len + 1;
        }
        if (offset > source_len) {
            source_str += source_len;
        }
        else if (offset > 0) {
            source_str += offset;
        }
        eflags |= REG_NOTBOL;
    }

    if (opt_inline) {
        resultListObj = sawo_NewListObj(interp, NULL, 0);
    }

  next_match:
    match = regexec(regex, source_str, num_vars + 1, pmatch, eflags);
    if (match >= REG_BADPAT) {
        char buf[100];

        regerror(match, regex, buf, sizeof(buf));
        sawo_SetResultFormatted(interp, "error while matching pattern: %s", buf);
        result = SAWO_ERR;
        goto done;
    }

    if (match == REG_NOMATCH) {
        goto done;
    }

    num_matches++;

    if (opt_all && !opt_inline) {
        
        goto try_next_match;
    }


    j = 0;
    for (i += 2; opt_inline ? j < num_vars : i < argc; i++, j++) {
        sawo_Obj *resultObj;

        if (opt_indices) {
            resultObj = sawo_NewListObj(interp, NULL, 0);
        }
        else {
            resultObj = sawo_NewStringObj(interp, "", 0);
        }

        if (pmatch[j].rm_so == -1) {
            if (opt_indices) {
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp, -1));
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp, -1));
            }
        }
        else {
            int len = pmatch[j].rm_eo - pmatch[j].rm_so;

            if (opt_indices) {
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp,
                        offset + pmatch[j].rm_so));
                sawo_ListAppendElement(interp, resultObj, sawo_NewIntObj(interp,
                        offset + pmatch[j].rm_so + len - 1));
            }
            else {
                sawo_AppendString(interp, resultObj, source_str + pmatch[j].rm_so, len);
            }
        }

        if (opt_inline) {
            sawo_ListAppendElement(interp, resultListObj, resultObj);
        }
        else {
            
            result = sawo_SetVariable(interp, argv[i], resultObj);

            if (result != SAWO_OK) {
                sawo_FreeObj(interp, resultObj);
                break;
            }
        }
    }

  try_next_match:
    if (opt_all && (pattern[0] != '^' || (regcomp_flags & REG_NEWLINE)) && *source_str) {
        if (pmatch[0].rm_eo) {
            offset += pmatch[0].rm_eo;
            source_str += pmatch[0].rm_eo;
        }
        else {
            source_str++;
            offset++;
        }
        if (*source_str) {
            eflags = REG_NOTBOL;
            goto next_match;
        }
    }

  done:
    if (result == SAWO_OK) {
        if (opt_inline) {
            sawo_SetResult(interp, resultListObj);
        }
        else {
            sawo_SetResultInt(interp, num_matches);
        }
    }

    sawo_Free(pmatch);
    return result;
}

#define MAX_SUB_MATCHES 50

int sawo_RegsubCmd(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int regcomp_flags = 0;
    int regexec_flags = 0;
    int opt_all = 0;
    int offset = 0;
    regex_t *regex;
    const char *p;
    int result;
    regmatch_t pmatch[MAX_SUB_MATCHES + 1];
    int num_matches = 0;

    int i, j, n;
    sawo_Obj *varname;
    sawo_Obj *resultObj;
    const char *source_str;
    int source_len;
    const char *replace_str;
    int replace_len;
    const char *pattern;
    int option;
    enum {
        OPT_NOCASE, OPT_LINE, OPT_ALL, OPT_START, OPT_END
    };
    static const char * const options[] = {
        "-nocase", "-line", "-all", "-start", "--", NULL
    };

    if (argc < 4) {
      wrongNumArgs:
        sawo_WrongNumArgs(interp, 1, argv,
            "?-switch ...? exp string subSpec ?varName?");
        return SAWO_ERR;
    }

    for (i = 1; i < argc; i++) {
        const char *opt = sawo_String(argv[i]);

        if (*opt != '-') {
            break;
        }
        if (sawo_GetEnum(interp, argv[i], options, &option, "switch", SAWO_ERRMSG | SAWO_ENUM_ABBREV) != SAWO_OK) {
            return SAWO_ERR;
        }
        if (option == OPT_END) {
            i++;
            break;
        }
        switch (option) {
            case OPT_NOCASE:
                regcomp_flags |= REG_ICASE;
                break;

            case OPT_LINE:
                regcomp_flags |= REG_NEWLINE;
                break;

            case OPT_ALL:
                opt_all = 1;
                break;

            case OPT_START:
                if (++i == argc) {
                    goto wrongNumArgs;
                }
                if (sawo_GetIndex(interp, argv[i], &offset) != SAWO_OK) {
                    return SAWO_ERR;
                }
                break;
        }
    }
    if (argc - i != 3 && argc - i != 4) {
        goto wrongNumArgs;
    }

    regex = SetRegexpFromAny(interp, argv[i], regcomp_flags);
    if (!regex) {
        return SAWO_ERR;
    }
    pattern = sawo_String(argv[i]);

    source_str = sawo_GetString(argv[i + 1], &source_len);
    replace_str = sawo_GetString(argv[i + 2], &replace_len);
    varname = argv[i + 3];

    
    resultObj = sawo_NewStringObj(interp, "", 0);

    if (offset) {
        if (offset < 0) {
            offset += source_len + 1;
        }
        if (offset > source_len) {
            offset = source_len;
        }
        else if (offset < 0) {
            offset = 0;
        }
    }

    
    sawo_AppendString(interp, resultObj, source_str, offset);


    n = source_len - offset;
    p = source_str + offset;
    do {
        int match = regexec(regex, p, MAX_SUB_MATCHES, pmatch, regexec_flags);

        if (match >= REG_BADPAT) {
            char buf[100];

            regerror(match, regex, buf, sizeof(buf));
            sawo_SetResultFormatted(interp, "error while matching pattern: %s", buf);
            return SAWO_ERR;
        }
        if (match == REG_NOMATCH) {
            break;
        }

        num_matches++;

        sawo_AppendString(interp, resultObj, p, pmatch[0].rm_so);


        for (j = 0; j < replace_len; j++) {
            int idx;
            int c = replace_str[j];

            if (c == '&') {
                idx = 0;
            }
            else if (c == '\\' && j < replace_len) {
                c = replace_str[++j];
                if ((c >= '0') && (c <= '9')) {
                    idx = c - '0';
                }
                else if ((c == '\\') || (c == '&')) {
                    sawo_AppendString(interp, resultObj, replace_str + j, 1);
                    continue;
                }
                else {
                    sawo_AppendString(interp, resultObj, replace_str + j - 1, (j == replace_len) ? 1 : 2);
                    continue;
                }
            }
            else {
                sawo_AppendString(interp, resultObj, replace_str + j, 1);
                continue;
            }
            if ((idx < MAX_SUB_MATCHES) && pmatch[idx].rm_so != -1 && pmatch[idx].rm_eo != -1) {
                sawo_AppendString(interp, resultObj, p + pmatch[idx].rm_so,
                    pmatch[idx].rm_eo - pmatch[idx].rm_so);
            }
        }

        p += pmatch[0].rm_eo;
        n -= pmatch[0].rm_eo;

        
        if (!opt_all || n == 0) {
            break;
        }

        
        if ((regcomp_flags & REG_NEWLINE) == 0 && pattern[0] == '^') {
            break;
        }

        
        if (pattern[0] == '\0' && n) {
            
            sawo_AppendString(interp, resultObj, p, 1);
            p++;
            n--;
        }

        regexec_flags |= REG_NOTBOL;
    } while (n);

    sawo_AppendString(interp, resultObj, p, -1);

    
    if (argc - i == 4) {
        result = sawo_SetVariable(interp, varname, resultObj);

        if (result == SAWO_OK) {
            sawo_SetResultInt(interp, num_matches);
        }
        else {
            sawo_FreeObj(interp, resultObj);
        }
    }
    else {
        sawo_SetResult(interp, resultObj);
        result = SAWO_OK;
    }

    return result;
}

int sawo_regexpInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "regexp", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "regexp", sawo_RegexpCmd, NULL, NULL);
    sawo_CreateCommand(interp, "regsub", sawo_RegsubCmd, NULL, NULL);
    return SAWO_OK;
}

