/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef SAWO_WIN32COMPAT_H
#define SAWO_WIN32COMPAT_H

#ifdef __cplusplus
extern "C" {
#endif

#if defined(_WIN32) || defined(WIN32)

#define HAVE_DLOPEN
void *dlopen(const char *path, int mode);
int dlclose(void *handle);
void *dlsym(void *handle, const char *symbol);
char *dlerror(void);

#if defined(__MINGW32__)
    #define SAWO_SPRINTF_DOUBLE_NEEDS_FIX
#endif

#ifdef _MSC_VER

#if _MSC_VER >= 1000
	#pragma warning(disable:4146)
#endif

#include <limits.h>
#define sawo_wide _int64
#ifndef LLONG_MAX
	#define LLONG_MAX    9223372036854775807I64
#endif
#ifndef LLONG_MIN
	#define LLONG_MIN    (-LLONG_MAX - 1I64)
#endif
#define SAWO_WIDE_MIN LLONG_MIN
#define SAWO_WIDE_MAX LLONG_MAX
#define SAWO_WIDE_MODIFIER "I64d"
#define strcasecmp _stricmp
#define strtoull _strtoui64
#define snprintf _snprintf

#include <io.h>

struct timeval {
	long tv_sec;
	long tv_usec;
};

int gettimeofday(struct timeval *tv, void *unused);

#define HAVE_OPENDIR
struct dirent {
	char *d_name;
};

typedef struct DIR {
	long                handle;
	struct _finddata_t  show;
	struct dirent       result;
	char                *name;
} DIR;

DIR *opendir(const char *name);
int closedir(DIR *dir);
struct dirent *readdir(DIR *dir);

#elif defined(__MINGW32__)

#include <stdlib.h>
#define strtod __strtod

#endif

#endif

#ifdef __cplusplus
}
#endif

#endif
