/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "sawo.h"
#include "sawoconfig.h"
#include "saworegexp.h"
#include "subcmd.h"
#include "utf8util.h"
#include "win32compat.h"

#include <limits.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <errno.h>

static int array_cmd_exists(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_SetResultInt(interp, sawo_GetVariable(interp, argv[0], 0) != 0);
    return SAWO_OK;
}

static int array_cmd_get(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    sawo_Obj *patternObj;

    if (!objPtr) {
        return SAWO_OK;
    }

    patternObj = (argc == 1) ? NULL : argv[1];

    if (patternObj == NULL || sawo_CompareStringImmediate(interp, patternObj, "*")) {
        if (sawo_IsList(objPtr) && sawo_ListLength(interp, objPtr) % 2 == 0) {
            sawo_SetResult(interp, objPtr);
            return SAWO_OK;
        }
    }

    return sawo_DictValues(interp, objPtr, patternObj);
}

static int array_cmd_names(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);

    if (!objPtr) {
        return SAWO_OK;
    }

    return sawo_DictKeys(interp, objPtr, argc == 1 ? NULL : argv[1]);
}

static int array_cmd_unset(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    int len;
    sawo_Obj *resultObj;
    sawo_Obj *objPtr;
    sawo_Obj **dictValuesObj;

    if (argc == 1 || sawo_CompareStringImmediate(interp, argv[1], "*")) {
        sawo_UnsetVariable(interp, argv[0], SAWO_NONE);
        return SAWO_OK;
    }

    objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);

    if (objPtr == NULL) {
        return SAWO_OK;
    }

    if (sawo_DictPairs(interp, objPtr, &dictValuesObj, &len) != SAWO_OK) {
        return SAWO_ERR;
    }

    resultObj = sawo_NewDictObj(interp, NULL, 0);

    for (i = 0; i < len; i += 2) {
        if (!sawo_StringMatchObj(interp, argv[1], dictValuesObj[i], 0)) {
            sawo_DictAddElement(interp, resultObj, dictValuesObj[i], dictValuesObj[i + 1]);
        }
    }
    sawo_Free(dictValuesObj);

    sawo_SetVariable(interp, argv[0], resultObj);
    return SAWO_OK;
}

static int array_cmd_size(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr;
    int len = 0;

    objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    if (objPtr) {
        len = sawo_DictSize(interp, objPtr);
        if (len < 0) {
            return SAWO_ERR;
        }
    }

    sawo_SetResultInt(interp, len);

    return SAWO_OK;
}

static int array_cmd_stat(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    sawo_Obj *objPtr = sawo_GetVariable(interp, argv[0], SAWO_NONE);
    if (objPtr) {
        return sawo_DictInfo(interp, objPtr);
    }
    sawo_SetResultFormatted(interp, "\"%#s\" isn't an array", argv[0], NULL);
    return SAWO_ERR;
}

static int array_cmd_set(sawo_Interp *interp, int argc, sawo_Obj *const *argv)
{
    int i;
    int len;
    sawo_Obj *listObj = argv[1];
    sawo_Obj *dictObj;

    len = sawo_ListLength(interp, listObj);
    if (len % 2) {
        sawo_SetResultString(interp, "list must have an even number of elements", -1);
        return SAWO_ERR;
    }

    dictObj = sawo_GetVariable(interp, argv[0], SAWO_UNSHARED);
    if (!dictObj) {
        return sawo_SetVariable(interp, argv[0], listObj);
    }
    else if (sawo_DictSize(interp, dictObj) < 0) {
        return SAWO_ERR;
    }

    if (sawo_IsShared(dictObj)) {
        dictObj = sawo_DuplicateObj(interp, dictObj);
    }

    for (i = 0; i < len; i += 2) {
        sawo_Obj *nameObj;
        sawo_Obj *valueObj;

        sawo_ListIndex(interp, listObj, i, &nameObj, SAWO_NONE);
        sawo_ListIndex(interp, listObj, i + 1, &valueObj, SAWO_NONE);

        sawo_DictAddElement(interp, dictObj, nameObj, valueObj);
    }
    return sawo_SetVariable(interp, argv[0], dictObj);
}

static const sawo_subcmd_type array_command_table[] = {
        {       "exists",
                "arrayName",
                array_cmd_exists,
                1,
                1,

        },
        {       "get",
                "arrayName ?pattern?",
                array_cmd_get,
                1,
                2,

        },
        {       "names",
                "arrayName ?pattern?",
                array_cmd_names,
                1,
                2,

        },
        {       "set",
                "arrayName list",
                array_cmd_set,
                2,
                2,

        },
        {       "size",
                "arrayName",
                array_cmd_size,
                1,
                1,

        },
        {       "stat",
                "arrayName",
                array_cmd_stat,
                1,
                1,

        },
        {       "unset",
                "arrayName ?pattern?",
                array_cmd_unset,
                1,
                2,

        },
        {       NULL
        }
};

int sawo_arrayInit(sawo_Interp *interp)
{
    if (sawo_PackageProvide(interp, "array", "1.0", SAWO_ERRMSG))
        return SAWO_ERR;

    sawo_CreateCommand(interp, "array", sawo_SubCmdRaise, (void *)array_command_table, NULL);
    return SAWO_OK;
}


int sawo_InitStaticExtensions(sawo_Interp *interp)
{
extern int sawo_bootstrapInit(sawo_Interp *);
extern int sawo_aioInit(sawo_Interp *);
extern int sawo_readdirInit(sawo_Interp *);
extern int sawo_regexpInit(sawo_Interp *);
extern int sawo_fileInit(sawo_Interp *);
extern int sawo_globInit(sawo_Interp *);
extern int sawo_execInit(sawo_Interp *);
extern int sawo_clockInit(sawo_Interp *);
extern int sawo_arrayInit(sawo_Interp *);
extern int sawo_stdlibInit(sawo_Interp *);
extern int sawo_hyangcompatInit(sawo_Interp *);
sawo_bootstrapInit(interp);
sawo_aioInit(interp);
sawo_readdirInit(interp);
sawo_regexpInit(interp);
sawo_fileInit(interp);
sawo_globInit(interp);
sawo_execInit(interp);
sawo_clockInit(interp);
sawo_arrayInit(interp);
sawo_stdlibInit(interp);
sawo_hyangcompatInit(interp);
return SAWO_OK;
}


#define SAWO_OPTIMIZATION

#include <stdio.h>
#include <stdlib.h>

#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include <limits.h>
#include <assert.h>
#include <errno.h>
#include <time.h>
#include <setjmp.h>


#ifdef HAVE_SYS_TIME_H
#include <sys/time.h>
#endif
#ifdef HAVE_BACKTRACE
#include <execinfo.h>
#endif
#ifdef HAVE_CRT_EXTERNS_H
#include <crt_externs.h>
#endif


#include <math.h>


#ifndef HYANG_LIBRARY
#define HYANG_LIBRARY "."
#endif
#ifndef HYANG_PLATFORM_OS
#define HYANG_PLATFORM_OS "unknown"
#endif
#ifndef HYANG_PLATFORM_PLATFORM
#define HYANG_PLATFORM_PLATFORM "unknown"
#endif
#ifndef HYANG_PLATFORM_PATH_SEPARATOR
#define HYANG_PLATFORM_PATH_SEPARATOR ":"
#endif


#ifdef SAWO_MAINTAINER
#define SAWO_DEBUG_COMMAND
#define SAWO_DEBUG_PANIC
#endif



#define SAWO_INTEGER_SPACE 24

const char *sawo_tt_name(int type);

#ifdef SAWO_DEBUG_PANIC
static void sawoPanicDump(int fail_condition, const char *fmt, ...);
#define sawoPanic(X) sawoPanicDump X
#else
#define sawoPanic(X)
#endif


static char sawoEmptyStringRep[] = "";

static void sawoFreeCallFrame(sawo_Interp *interp, sawo_CallFrame *cf, int action);
static int ListSetIndex(sawo_Interp *interp, sawo_Obj *listPtr, int listindex, sawo_Obj *newObjPtr,
    int flags);
static int sawoDeleteLocalRaises(sawo_Interp *interp, sawo_Stack *localCommands);
static sawo_Obj *sawoExpandDictSugar(sawo_Interp *interp, sawo_Obj *objPtr);
static void SetDictSubstFromAny(sawo_Interp *interp, sawo_Obj *objPtr);
static sawo_Obj **sawoDictPairs(sawo_Obj *dictPtr, int *len);
static void sawoSetFailedEnumResult(sawo_Interp *interp, const char *arg, const char *badtype,
    const char *prefix, const char *const *tablePtr, const char *name);
static int sawoCallProcedure(sawo_Interp *interp, sawo_Cmd *cmd, int argc, sawo_Obj *const *argv);
static int sawoGetWideNoErr(sawo_Interp *interp, sawo_Obj *objPtr, sawo_wide * widePtr);
static int sawoSign(sawo_wide w);
static int sawoValidName(sawo_Interp *interp, const char *type, sawo_Obj *nameObjPtr);
static void sawoPrngSeed(sawo_Interp *interp, unsigned char *seed, int seedLen);
static void sawoRandomBytes(sawo_Interp *interp, void *dest, unsigned int len);

#define sawoWideValue(objPtr) (objPtr)->internalRep.wideValue

#define sawoObjTypeName(O) ((O)->typePtr ? (O)->typePtr->name : "none")

static int utf8_tounicode_case(const char *s, int *uc, int upper)
{
    int l = utf8_tounicode(s, uc);
    if (upper) {
        *uc = utf8_upper(*uc);
    }
    return l;
}

#define SAWO_CHARSET_SCAN 2
#define SAWO_CHARSET_GLOB 0

static const char *sawoCharsetMatch(const char *pattern, int c, int flags)
{
    int not = 0;
    int pchar;
    int match = 0;
    int nocase = 0;

    if (flags & SAWO_NOCASE) {
        nocase++;
        c = utf8_upper(c);
    }

    if (flags & SAWO_CHARSET_SCAN) {
        if (*pattern == '^') {
            not++;
            pattern++;
        }

        if (*pattern == ']') {
            goto first;
        }
    }

    while (*pattern && *pattern != ']') {
        if (pattern[0] == '\\') {
first:
            pattern += utf8_tounicode_case(pattern, &pchar, nocase);
        }
        else {
            int start;
            int end;

            pattern += utf8_tounicode_case(pattern, &start, nocase);
            if (pattern[0] == '-' && pattern[1]) {
                pattern += utf8_tounicode(pattern, &pchar);
                pattern += utf8_tounicode_case(pattern, &end, nocase);
                if ((c >= start && c <= end) || (c >= end && c <= start)) {
                    match = 1;
                }
                continue;
            }
            pchar = start;
        }

        if (pchar == c) {
            match = 1;
        }
    }
    if (not) {
        match = !match;
    }

    return match ? pattern : NULL;
}

static int sawoGlobMatch(const char *pattern, const char *string, int nocase)
{
    int c;
    int pchar;
    while (*pattern) {
        switch (pattern[0]) {
            case '*':
                while (pattern[1] == '*') {
                    pattern++;
                }
                pattern++;
                if (!pattern[0]) {
                    return 1;
                }
                while (*string) {
                    if (sawoGlobMatch(pattern, string, nocase))
                        return 1;
                    string += utf8_tounicode(string, &c);
                }
                return 0;

            case '?':
                string += utf8_tounicode(string, &c);
                break;

            case '[': {
                    string += utf8_tounicode(string, &c);
                    pattern = sawoCharsetMatch(pattern + 1, c, nocase ? SAWO_NOCASE : 0);
                    if (!pattern) {
                        return 0;
                    }
                    if (!*pattern) {
                        continue;
                    }
                    break;
                }
            case '\\':
                if (pattern[1]) {
                    pattern++;
                }

            default:
                string += utf8_tounicode_case(string, &c, nocase);
                utf8_tounicode_case(pattern, &pchar, nocase);
                if (pchar != c) {
                    return 0;
                }
                break;
        }
        pattern += utf8_tounicode_case(pattern, &pchar, nocase);
        if (!*string) {
            while (*pattern == '*') {
                pattern++;
            }
            break;
        }
    }
    if (!*pattern && !*string) {
        return 1;
    }
    return 0;
}

static int sawoStringCompare(const char *s1, int l1, const char *s2, int l2)
{
    if (l1 < l2) {
        return memcmp(s1, s2, l1) <= 0 ? -1 : 1;
    }
    else if (l2 < l1) {
        return memcmp(s1, s2, l2) >= 0 ? 1 : -1;
    }
    else {
        return sawoSign(memcmp(s1, s2, l1));
    }
}

static int sawoStringCompareLen(const char *s1, const char *s2, int maxchars, int nocase)
{
    while (*s1 && *s2 && maxchars) {
        int c1, c2;
        s1 += utf8_tounicode_case(s1, &c1, nocase);
        s2 += utf8_tounicode_case(s2, &c2, nocase);
        if (c1 != c2) {
            return sawoSign(c1 - c2);
        }
        maxchars--;
    }
    if (!maxchars) {
        return 0;
    }
    if (*s1) {
        return 1;
    }
    if (*s2) {
        return -1;
    }
    return 0;
}

static int sawoStringFirst(const char *s1, int l1, const char *s2, int l2, int idx)
{
    int i;
    int l1bytelen;

    if (!l1 || !l2 || l1 > l2) {
        return -1;
    }
    if (idx < 0)
        idx = 0;
    s2 += utf8_index(s2, idx);

    l1bytelen = utf8_index(s1, l1);

    for (i = idx; i <= l2 - l1; i++) {
        int c;
        if (memcmp(s2, s1, l1bytelen) == 0) {
            return i;
        }
        s2 += utf8_tounicode(s2, &c);
    }
    return -1;
}

static int sawoStringLast(const char *s1, int l1, const char *s2, int l2)
{
    const char *p;

    if (!l1 || !l2 || l1 > l2)
        return -1;

    for (p = s2 + l2 - 1; p != s2 - 1; p--) {
        if (*p == *s1 && memcmp(s1, p, l1) == 0) {
            return p - s2;
        }
    }
    return -1;
}

#ifdef SAWO_UTF8
static int sawoStringLastUtf8(const char *s1, int l1, const char *s2, int l2)
{
    int n = sawoStringLast(s1, utf8_index(s1, l1), s2, utf8_index(s2, l2));
    if (n > 0) {
        n = utf8_strlen(s2, n);
    }
    return n;
}
#endif

static int sawoCheckConversion(const char *str, const char *endptr)
{
    if (str[0] == '\0' || str == endptr) {
        return SAWO_ERR;
    }

    if (endptr[0] != '\0') {
        while (*endptr) {
            if (!isspace(UCHAR(*endptr))) {
                return SAWO_ERR;
            }
            endptr++;
        }
    }
    return SAWO_OK;
}

static int sawoNumberBase(const char *str, int *base, int *sign)
{
    int i = 0;

    *base = 10;

    while (isspace(UCHAR(str[i]))) {
        i++;
    }

    if (str[i] == '-') {
        *sign = -1;
        i++;
    }
    else {
        if (str[i] == '+') {
            i++;
        }
        *sign = 1;
    }

    if (str[i] != '0') {
        return 0;
    }

    switch (str[i + 1]) {
        case 'x': case 'X': *base = 16; break;
        case 'o': case 'O': *base = 8; break;
        case 'b': case 'B': *base = 2; break;
        default: return 0;
    }
    i += 2;
    if (str[i] != '-' && str[i] != '+' && !isspace(UCHAR(str[i]))) {
        return i;
    }
    *base = 10;
    return 0;
}

static long sawo_strtol(const char *str, char **endptr)
{
    int sign;
    int base;
    int i = sawoNumberBase(str, &base, &sign);

    if (base != 10) {
        long value = strtol(str + i, endptr, base);
        if (endptr == NULL || *endptr != str + i) {
            return value * sign;
        }
    }

    return strtol(str, endptr, 10);
}


static sawo_wide sawo_strtoull(const char *str, char **endptr)
{
#ifdef HAVE_LONG_LONG
    int sign;
    int base;
    int i = sawoNumberBase(str, &base, &sign);

    if (base != 10) {
        sawo_wide value = strtoull(str + i, endptr, base);
        if (endptr == NULL || *endptr != str + i) {
            return value * sign;
        }
    }

    return strtoull(str, endptr, 10);
#else
    return (unsigned long)sawo_strtol(str, endptr);
#endif
}

int sawo_StringToWide(const char *str, sawo_wide * widePtr, int base)
{
    char *endptr;

    if (base) {
        *widePtr = strtoull(str, &endptr, base);
    }
    else {
        *widePtr = sawo_strtoull(str, &endptr);
    }

    return sawoCheckConversion(str, endptr);
}

int sawo_StringToDouble(const char *str, double *doublePtr)
{
    char *endptr;

    errno = 0;

    *doublePtr = strtod(str, &endptr);

    return sawoCheckConversion(str, endptr);
}

static sawo_wide sawoPowWide(sawo_wide b, sawo_wide e)
{
    sawo_wide i, res = 1;

    if ((b == 0 && e != 0) || (e < 0))
        return 0;
    for (i = 0; i < e; i++) {
        res *= b;
    }
    return res;
}

#ifdef SAWO_DEBUG_PANIC
static void sawoPanicDump(int condition, const char *fmt, ...)
{
    va_list ap;

    if (!condition) {
        return;
    }

    va_start(ap, fmt);

    fprintf(stderr, "\nSAWO INTERPRETER PANIC: ");
    vfprintf(stderr, fmt, ap);
    fprintf(stderr, "\n\n");
    va_end(ap);

#ifdef HAVE_BACKTRACE
    {
        void *array[40];
        int size, i;
        char **strings;

        size = backtrace(array, 40);
        strings = backtrace_symbols(array, size);
        for (i = 0; i < size; i++)
            fprintf(stderr, "[backtrace] %s\n", strings[i]);
        fprintf(stderr, "[backtrace] Include the above lines and the output\n");
        fprintf(stderr, "[backtrace] of 'nm <executable>' in the bug report.\n");
    }
#endif

    exit(1);
}
#endif


void *sawo_Alloc(int size)
{
    return size ? malloc(size) : NULL;
}

void sawo_Free(void *ptr)
{
    free(ptr);
}

void *sawo_Realloc(void *ptr, int size)
{
    return realloc(ptr, size);
}

char *sawo_StrDup(const char *s)
{
    return strdup(s);
}

char *sawo_StrDupLen(const char *s, int l)
{
    char *copy = sawo_Alloc(l + 1);

    memcpy(copy, s, l + 1);
    copy[l] = 0;
    return copy;
}

static sawo_wide sawoClock(void)
{
    struct timeval tv;

    gettimeofday(&tv, NULL);
    return (sawo_wide) tv.tv_sec * 1000000 + tv.tv_usec;
}

static void sawoExpandHashTableIfNeeded(sawo_HashTable *ht);
static unsigned int sawoHashTableNextPower(unsigned int size);
static sawo_HashEntry *sawoInsertHashEntry(sawo_HashTable *ht, const void *key, int replace);

unsigned int sawo_IntHashFunction(unsigned int key)
{
    key += ~(key << 15);
    key ^= (key >> 10);
    key += (key << 3);
    key ^= (key >> 6);
    key += ~(key << 11);
    key ^= (key >> 16);
    return key;
}

unsigned int sawo_GenHashFunction(const unsigned char *buf, int len)
{
    unsigned int h = 0;

    while (len--)
        h += (h << 3) + *buf++;
    return h;
}

static void sawoResetHashTable(sawo_HashTable *ht)
{
    ht->table = NULL;
    ht->size = 0;
    ht->sizemask = 0;
    ht->used = 0;
    ht->collisions = 0;
#ifdef SAWO_RANDOMISE_HASH
    ht->uniq = (rand() ^ time(NULL) ^ clock());
#else
    ht->uniq = 0;
#endif
}

static void sawoInitHashTableIterator(sawo_HashTable *ht, sawo_HashTableIterator *iter)
{
    iter->ht = ht;
    iter->index = -1;
    iter->entry = NULL;
    iter->nextEntry = NULL;
}

int sawo_InitHashTable(sawo_HashTable *ht, const sawo_HashTableType *type, void *privDataPtr)
{
    sawoResetHashTable(ht);
    ht->type = type;
    ht->privdata = privDataPtr;
    return SAWO_OK;
}

void sawo_ResizeHashTable(sawo_HashTable *ht)
{
    int minimal = ht->used;

    if (minimal < SAWO_HT_INITIAL_SIZE)
        minimal = SAWO_HT_INITIAL_SIZE;
    sawo_ExpandHashTable(ht, minimal);
}

void sawo_ExpandHashTable(sawo_HashTable *ht, unsigned int size)
{
    sawo_HashTable n;
    unsigned int realsize = sawoHashTableNextPower(size), i;

     if (size <= ht->used)
        return;

    sawo_InitHashTable(&n, ht->type, ht->privdata);
    n.size = realsize;
    n.sizemask = realsize - 1;
    n.table = sawo_Alloc(realsize * sizeof(sawo_HashEntry *));
    n.uniq = ht->uniq;

    memset(n.table, 0, realsize * sizeof(sawo_HashEntry *));

    n.used = ht->used;
    for (i = 0; ht->used > 0; i++) {
        sawo_HashEntry *he, *nextHe;

        if (ht->table[i] == NULL)
            continue;

        he = ht->table[i];
        while (he) {
            unsigned int h;

            nextHe = he->next;
            h = sawo_HashKey(ht, he->key) & n.sizemask;
            he->next = n.table[h];
            n.table[h] = he;
            ht->used--;
            he = nextHe;
        }
    }
    assert(ht->used == 0);
    sawo_Free(ht->table);

    *ht = n;
}


int sawo_AddHashEntry(sawo_HashTable *ht, const void *key, void *val)
{
    sawo_HashEntry *entry;

    entry = sawoInsertHashEntry(ht, key, 0);
    if (entry == NULL)
        return SAWO_ERR;

    sawo_SetHashKey(ht, entry, key);
    sawo_SetHashVal(ht, entry, val);
    return SAWO_OK;
}

int sawo_ReplaceHashEntry(sawo_HashTable *ht, const void *key, void *val)
{
    int existed;
    sawo_HashEntry *entry;

    entry = sawoInsertHashEntry(ht, key, 1);
    if (entry->key) {
        if (ht->type->valDestructor && ht->type->valDup) {
            void *newval = ht->type->valDup(ht->privdata, val);
            ht->type->valDestructor(ht->privdata, entry->u.val);
            entry->u.val = newval;
        }
        else {
            sawo_FreeEntryVal(ht, entry);
            sawo_SetHashVal(ht, entry, val);
        }
        existed = 1;
    }
    else {
        sawo_SetHashKey(ht, entry, key);
        sawo_SetHashVal(ht, entry, val);
        existed = 0;
    }

    return existed;
}

int sawo_DeleteHashEntry(sawo_HashTable *ht, const void *key)
{
    unsigned int h;
    sawo_HashEntry *he, *prevHe;

    if (ht->used == 0)
        return SAWO_ERR;
    h = sawo_HashKey(ht, key) & ht->sizemask;
    he = ht->table[h];

    prevHe = NULL;
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key)) {
            if (prevHe)
                prevHe->next = he->next;
            else
                ht->table[h] = he->next;
            sawo_FreeEntryKey(ht, he);
            sawo_FreeEntryVal(ht, he);
            sawo_Free(he);
            ht->used--;
            return SAWO_OK;
        }
        prevHe = he;
        he = he->next;
    }
    return SAWO_ERR;
}


int sawo_FreeHashTable(sawo_HashTable *ht)
{
    unsigned int i;

    for (i = 0; ht->used > 0; i++) {
        sawo_HashEntry *he, *nextHe;

        if ((he = ht->table[i]) == NULL)
            continue;
        while (he) {
            nextHe = he->next;
            sawo_FreeEntryKey(ht, he);
            sawo_FreeEntryVal(ht, he);
            sawo_Free(he);
            ht->used--;
            he = nextHe;
        }
    }
    sawo_Free(ht->table);
    sawoResetHashTable(ht);
    return SAWO_OK;
}

sawo_HashEntry *sawo_FindHashEntry(sawo_HashTable *ht, const void *key)
{
    sawo_HashEntry *he;
    unsigned int h;

    if (ht->used == 0)
        return NULL;
    h = sawo_HashKey(ht, key) & ht->sizemask;
    he = ht->table[h];
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key))
            return he;
        he = he->next;
    }
    return NULL;
}

sawo_HashTableIterator *sawo_GetHashTableIterator(sawo_HashTable *ht)
{
    sawo_HashTableIterator *iter = sawo_Alloc(sizeof(*iter));
    sawoInitHashTableIterator(ht, iter);
    return iter;
}

sawo_HashEntry *sawo_NextHashEntry(sawo_HashTableIterator *iter)
{
    while (1) {
        if (iter->entry == NULL) {
            iter->index++;
            if (iter->index >= (signed)iter->ht->size)
                break;
            iter->entry = iter->ht->table[iter->index];
        }
        else {
            iter->entry = iter->nextEntry;
        }
        if (iter->entry) {
            iter->nextEntry = iter->entry->next;
            return iter->entry;
        }
    }
    return NULL;
}

static void sawoExpandHashTableIfNeeded(sawo_HashTable *ht)
{
    if (ht->size == 0)
        sawo_ExpandHashTable(ht, SAWO_HT_INITIAL_SIZE);
    if (ht->size == ht->used)
        sawo_ExpandHashTable(ht, ht->size * 2);
}

static unsigned int sawoHashTableNextPower(unsigned int size)
{
    unsigned int i = SAWO_HT_INITIAL_SIZE;

    if (size >= 2147483648U)
        return 2147483648U;
    while (1) {
        if (i >= size)
            return i;
        i *= 2;
    }
}

static sawo_HashEntry *sawoInsertHashEntry(sawo_HashTable *ht, const void *key, int replace)
{
    unsigned int h;
    sawo_HashEntry *he;

    sawoExpandHashTableIfNeeded(ht);

    h = sawo_HashKey(ht, key) & ht->sizemask;
    he = ht->table[h];
    while (he) {
        if (sawo_CompareHashKeys(ht, key, he->key))
            return replace ? he : NULL;
        he = he->next;
    }

    he = sawo_Alloc(sizeof(*he));
    he->next = ht->table[h];
    ht->table[h] = he;
    ht->used++;
    he->key = NULL;

    return he;
}

static unsigned int sawoStringCopyHTHashFunction(const void *key)
{
    return sawo_GenHashFunction(key, strlen(key));
}

static void *sawoStringCopyHTDup(void *privdata, const void *key)
{
    return sawo_StrDup(key);
}

static int sawoStringCopyHTKeyCompare(void *privdata, const void *key1, const void *key2)
{
    return strcmp(key1, key2) == 0;
}

static void sawoStringCopyHTKeyDestructor(void *privdata, void *key)
{
    sawo_Free(key);
}

static const sawo_HashTableType sawoPackageHashTableType = {
    sawoStringCopyHTHashFunction,
    sawoStringCopyHTDup,
    NULL,
    sawoStringCopyHTKeyCompare,
    sawoStringCopyHTKeyDestructor,
    NULL
};

typedef struct AssocDataValue
{
    sawo_InterpDeleteRaise *delRaise;
    void *data;
} AssocDataValue;

static void sawoAssocDataHashTableValueDestructor(void *privdata, void *data)
{
    AssocDataValue *assocPtr = (AssocDataValue *) data;

    if (assocPtr->delRaise != NULL)
        assocPtr->delRaise((sawo_Interp *)privdata, assocPtr->data);
    sawo_Free(data);
}

static const sawo_HashTableType sawoAssocDataHashTableType = {
    sawoStringCopyHTHashFunction,    
    sawoStringCopyHTDup,             
    NULL,                           
    sawoStringCopyHTKeyCompare,      
    sawoStringCopyHTKeyDestructor,   
    sawoAssocDataHashTableValueDestructor        
};

void sawo_InitStack(sawo_Stack *stack)
{
    stack->len = 0;
    stack->maxlen = 0;
    stack->vector = NULL;
}

void sawo_FreeStack(sawo_Stack *stack)
{
    sawo_Free(stack->vector);
}

int sawo_StackLen(sawo_Stack *stack)
{
    return stack->len;
}

void sawo_StackPush(sawo_Stack *stack, void *element)
{
    int neededLen = stack->len + 1;

    if (neededLen > stack->maxlen) {
        stack->maxlen = neededLen < 20 ? 20 : neededLen * 2;
        stack->vector = sawo_Realloc(stack->vector, sizeof(void *) * stack->maxlen);
    }
    stack->vector[stack->len] = element;
    stack->len++;
}

void *sawo_StackPop(sawo_Stack *stack)
{
    if (stack->len == 0)
        return NULL;
    stack->len--;
    return stack->vector[stack->len];
}

void *sawo_StackPeek(sawo_Stack *stack)
{
    if (stack->len == 0)
        return NULL;
    return stack->vector[stack->len - 1];
}

void sawo_FreeStackElements(sawo_Stack *stack, void (*freeFunc) (void *ptr))
{
    int i;

    for (i = 0; i < stack->len; i++)
        freeFunc(stack->vector[i]);
}

