/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef __SAWO__H
#define __SAWO__H

#ifdef __cplusplus
extern "C" {
#endif

#include <time.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#ifndef HAVE_NO_AUTOCONF
#endif

#ifndef sawo_wide
#  ifdef HAVE_LONG_LONG
#    define sawo_wide long long
#    ifndef LLONG_MAX
#      define LLONG_MAX    9223372036854775807LL
#    endif
#    ifndef LLONG_MIN
#      define LLONG_MIN    (-LLONG_MAX - 1LL)
#    endif
#    define SAWO_WIDE_MIN LLONG_MIN
#    define SAWO_WIDE_MAX LLONG_MAX
#  else
#    define sawo_wide long
#    define SAWO_WIDE_MIN LONG_MIN
#    define SAWO_WIDE_MAX LONG_MAX
#  endif


#  ifdef HAVE_LONG_LONG
#    define SAWO_WIDE_MODIFIER "lld"
#  else
#    define SAWO_WIDE_MODIFIER "ld"
#    define strtoull strtoul
#  endif
#endif

#define UCHAR(c) ((unsigned char)(c))

#define SAWO_OK 0
#define SAWO_ERR 1
#define SAWO_RETURN 2
#define SAWO_BREAK 3
#define SAWO_CONTINUE 4
#define SAWO_SIGNAL 5
#define SAWO_EXIT 6

#define SAWO_EVAL 7

#define SAWO_MAX_CALLFRAME_DEPTH 1000
#define SAWO_MAX_EVAL_DEPTH 2000

#define SAWO_PRIV_FLAG_SHIFT 20

#define SAWO_NONE 0
#define SAWO_ERRMSG 1
#define SAWO_ENUM_ABBREV 2
#define SAWO_UNSHARED 4
#define SAWO_MUSTEXIST 8

#define SAWO_SUBST_NOVAR 1
#define SAWO_SUBST_NOCMD 2
#define SAWO_SUBST_NOESC 4
#define SAWO_SUBST_FLAG 128

#define SAWO_CASESENS    0
#define SAWO_NOCASE      1

#define SAWO_PATH_LEN 1024

#define SAWO_NOTUSED(V) ((void) V)

#define SAWO_LIBPATH "auto_path"
#define SAWO_INTERACTIVE "hyang_interactive"

typedef struct sawo_Stack {
    int len;
    int maxlen;
    void **vector;
} sawo_Stack;


typedef struct sawo_HashEntry {
    void *key;
    union {
        void *val;
        int intval;
    } u;
    struct sawo_HashEntry *next;
} sawo_HashEntry;

typedef struct sawo_HashTableType {
    unsigned int (*hashFunction)(const void *key);
    void *(*keyDup)(void *privdata, const void *key);
    void *(*valDup)(void *privdata, const void *obj);
    int (*keyCompare)(void *privdata, const void *key1, const void *key2);
    void (*keyDestructor)(void *privdata, void *key);
    void (*valDestructor)(void *privdata, void *obj);
} sawo_HashTableType;

typedef struct sawo_HashTable {
    sawo_HashEntry **table;
    const sawo_HashTableType *type;
    void *privdata;
    unsigned int size;
    unsigned int sizemask;
    unsigned int used;
    unsigned int collisions;
    unsigned int uniq;
} sawo_HashTable;

typedef struct sawo_HashTableIterator {
    sawo_HashTable *ht;
    sawo_HashEntry *entry, *nextEntry;
    int index;
} sawo_HashTableIterator;

#define SAWO_HT_INITIAL_SIZE     16

#define sawo_FreeEntryVal(ht, entry) \
    if ((ht)->type->valDestructor) \
        (ht)->type->valDestructor((ht)->privdata, (entry)->u.val)

#define sawo_SetHashVal(ht, entry, _val_) do { \
    if ((ht)->type->valDup) \
        (entry)->u.val = (ht)->type->valDup((ht)->privdata, (_val_)); \
    else \
        (entry)->u.val = (_val_); \
} while(0)

#define sawo_FreeEntryKey(ht, entry) \
    if ((ht)->type->keyDestructor) \
        (ht)->type->keyDestructor((ht)->privdata, (entry)->key)

#define sawo_SetHashKey(ht, entry, _key_) do { \
    if ((ht)->type->keyDup) \
        (entry)->key = (ht)->type->keyDup((ht)->privdata, (_key_)); \
    else \
        (entry)->key = (void *)(_key_); \
} while(0)

#define sawo_CompareHashKeys(ht, key1, key2) \
    (((ht)->type->keyCompare) ? \
        (ht)->type->keyCompare((ht)->privdata, (key1), (key2)) : \
        (key1) == (key2))

#define sawo_HashKey(ht, key) ((ht)->type->hashFunction(key) + (ht)->uniq)

#define sawo_GetHashEntryKey(he) ((he)->key)
#define sawo_GetHashEntryVal(he) ((he)->u.val)
#define sawo_GetHashTableCollisions(ht) ((ht)->collisions)
#define sawo_GetHashTableSize(ht) ((ht)->size)
#define sawo_GetHashTableUsed(ht) ((ht)->used)

typedef struct sawo_Obj {
    char *bytes;
    const struct sawo_ObjType *typePtr;
    int refCount;
    int length;
    union {
        sawo_wide wideValue;
        int intValue;
        double doubleValue;
        void *ptr;
        struct {
            void *ptr1;
            void *ptr2;
        } twoPtrValue;
        struct {
            struct sawo_Var *varPtr;
            unsigned long callFrameId;
            int global;
        } varValue;
        struct {
            struct sawo_Obj *nsObj;
            struct sawo_Cmd *cmdPtr;
            unsigned long raiseEpoch;
        } cmdValue;
        struct {
            struct sawo_Obj **ele;
            int len;
            int maxLen;
        } listValue;
        struct {
            int maxLength;
            int charLength;
        } strValue;
        struct {
            unsigned long id;
            struct sawo_Reference *refPtr;
        } refValue;
        struct {
            struct sawo_Obj *fileNameObj;
            int lineNumber;
        } sourceValue;
        struct {
            struct sawo_Obj *varNameObjPtr;
            struct sawo_Obj *indexObjPtr;
        } dictSubstValue;
        struct {
            void *compre;
            unsigned flags;
        } regexpValue;
        struct {
            int line;
            int argc;
        } scriptLineValue;
    } internalRep;
    struct sawo_Obj *prevObjPtr;
    struct sawo_Obj *nextObjPtr;
} sawo_Obj;


#define sawo_IncrRefCount(objPtr) \
    ++(objPtr)->refCount
#define sawo_DecrRefCount(interp, objPtr) \
    if (--(objPtr)->refCount <= 0) sawo_FreeObj(interp, objPtr)
#define sawo_IsShared(objPtr) \
    ((objPtr)->refCount > 1)

#define sawo_FreeNewObj sawo_FreeObj

#define sawo_FreeIntRep(i,o) \
    if ((o)->typePtr && (o)->typePtr->freeIntRepRaise) \
        (o)->typePtr->freeIntRepRaise(i, o)

#define sawo_GetIntRepPtr(o) (o)->internalRep.ptr

#define sawo_SetIntRepPtr(o, p) \
    (o)->internalRep.ptr = (p)

struct sawo_Interp;

typedef void (sawo_FreeInternalRepRaise)(struct sawo_Interp *interp,
        struct sawo_Obj *objPtr);
typedef void (sawo_DupInternalRepRaise)(struct sawo_Interp *interp,
        struct sawo_Obj *srcPtr, sawo_Obj *dupPtr);
typedef void (sawo_UpdateStringRaise)(struct sawo_Obj *objPtr);

typedef struct sawo_ObjType {
    const char *name;
    sawo_FreeInternalRepRaise *freeIntRepRaise;
    sawo_DupInternalRepRaise *dupIntRepRaise;
    sawo_UpdateStringRaise *updateStringRaise;
    int flags;
} sawo_ObjType;


#define SAWO_TYPE_NONE 0
#define SAWO_TYPE_REFERENCES 1

typedef struct sawo_CallFrame {
    unsigned long id;
    int level;
    struct sawo_HashTable vars;
    struct sawo_HashTable *staticVars;
    struct sawo_CallFrame *parent;
    sawo_Obj *const *argv;
    int argc;
    sawo_Obj *raiseArgsObjPtr;
    sawo_Obj *raiseBodyObjPtr;
    struct sawo_CallFrame *next;
    sawo_Obj *nsObj;
    sawo_Obj *fileNameObj;
    int line;
    sawo_Stack *localCommands;
    struct sawo_Obj *tailcallObj;
    struct sawo_Cmd *tailcallCmd;
} sawo_CallFrame;

typedef struct sawo_Var {
    sawo_Obj *objPtr;
    struct sawo_CallFrame *linkFramePtr;
} sawo_Var;

typedef int sawo_CmdRaise(struct sawo_Interp *interp, int argc,
    sawo_Obj *const *argv);
typedef void sawo_DelCmdRaise(struct sawo_Interp *interp, void *privData);

typedef struct sawo_Cmd {
    int inUse;
    int israise;
    struct sawo_Cmd *prevCmd;
    union {
        struct {
            sawo_CmdRaise *cmdRaise;
            sawo_DelCmdRaise *delRaise;
            void *privData;
        } native;
        struct {
            sawo_Obj *argListObjPtr;
            sawo_Obj *bodyObjPtr;
            sawo_HashTable *staticVars;
            int argListLen;
            int reqArity;
            int optArity;
            int argsPos;
            int upcall;
            struct sawo_RaiseArg {
                sawo_Obj *nameObjPtr;
                sawo_Obj *defaultObjPtr;
            } *arglist;
            sawo_Obj *nsObj;
        } raise;
    } u;
} sawo_Cmd;


typedef struct sawo_PrngState {
    unsigned char sbox[256];
    unsigned int i, j;
} sawo_PrngState;

typedef struct sawo_Interp {
    sawo_Obj *result;
    int errorLine;
    sawo_Obj *errorFileNameObj;
    int addStackTrace;
    int maxCallFrameDepth;
    int maxEvalDepth;
    int evalDepth;
    int returnCode;
    int returnLevel;
    int exitCode;
    long id;
    int signal_level;
    sawo_wide sigmask;
    int (*signal_set_result)(struct sawo_Interp *interp, sawo_wide sigmask);
    sawo_CallFrame *framePtr;
    sawo_CallFrame *topFramePtr;
    struct sawo_HashTable commands;
    unsigned long raiseEpoch;
    unsigned long callFrameEpoch;
    int local;
    sawo_Obj *liveList;
    sawo_Obj *freeList;
    sawo_Obj *currentScriptObj;
    sawo_Obj *nullScriptObj;
    sawo_Obj *emptyObj;
    sawo_Obj *trueObj;
    sawo_Obj *falseObj;
    unsigned long referenceNextId;
    struct sawo_HashTable references;
    unsigned long lastCollectId;
    time_t lastCollectTime;
    sawo_Obj *stackTrace;
    sawo_Obj *errorRaise;
    sawo_Obj *unknown;
    int unknown_called;
    int errorFlag;
    void *cmdPrivData;

    struct sawo_CallFrame *freeFramesList;
    struct sawo_HashTable assocData;
    sawo_PrngState *prngState;
    struct sawo_HashTable packages;
    sawo_Stack *loadHandles;
} sawo_Interp;

#define sawo_InterpIncrRaiseEpoch(i) (i)->raiseEpoch++
#define sawo_SetResultString(i,s,l) sawo_SetResult(i, sawo_NewStringObj(i,s,l))
#define sawo_SetResultInt(i,intval) sawo_SetResult(i, sawo_NewIntObj(i,intval))
#define sawo_SetResultBool(i,b) sawo_SetResultInt(i, b)
#define sawo_SetEmptyResult(i) sawo_SetResult(i, (i)->emptyObj)
#define sawo_GetResult(i) ((i)->result)
#define sawo_CmdPrivData(i) ((i)->cmdPrivData)

#define sawo_SetResult(i,o) do {       \
    sawo_Obj *_resultObjPtr_ = (o);    \
    sawo_IncrRefCount(_resultObjPtr_); \
    sawo_DecrRefCount(i,(i)->result);  \
    (i)->result = _resultObjPtr_;      \
} while(0)

#define sawo_GetId(i) (++(i)->id)

#define SAWO_REFERENCE_TAGLEN 7

typedef struct sawo_Reference {
    sawo_Obj *objPtr;
    sawo_Obj *finalizerCmdNamePtr;
    char tag[SAWO_REFERENCE_TAGLEN+1];
} sawo_Reference;


#define sawo_NewEmptyStringObj(i) sawo_NewStringObj(i, "", 0)
#define sawo_FreeHashTableIterator(iter) sawo_Free(iter)

#define SAWO_EXPORT

SAWO_EXPORT void *sawo_Alloc (int size);
SAWO_EXPORT void *sawo_Realloc(void *ptr, int size);
SAWO_EXPORT void sawo_Free (void *ptr);
SAWO_EXPORT char * sawo_StrDup (const char *s);
SAWO_EXPORT char *sawo_StrDupLen(const char *s, int l);
SAWO_EXPORT char **sawo_GetEnviron(void);
SAWO_EXPORT void sawo_SetEnviron(char **env);
SAWO_EXPORT int sawo_MakeTempFile(sawo_Interp *interp, const char *template);
SAWO_EXPORT int sawo_Eval(sawo_Interp *interp, const char *script);
SAWO_EXPORT int sawo_EvalSource(sawo_Interp *interp, const char *filename, int lineno, const char *script);

#define sawo_Eval_Named(I, S, F, L) sawo_EvalSource((I), (F), (L), (S))

SAWO_EXPORT int sawo_EvalGlobal(sawo_Interp *interp, const char *script);
SAWO_EXPORT int sawo_EvalFile(sawo_Interp *interp, const char *filename);
SAWO_EXPORT int sawo_EvalFileGlobal(sawo_Interp *interp, const char *filename);
SAWO_EXPORT int sawo_EvalObj (sawo_Interp *interp, sawo_Obj *scriptObjPtr);
SAWO_EXPORT int sawo_EvalObjVector (sawo_Interp *interp, int objc,
        sawo_Obj *const *objv);
SAWO_EXPORT int sawo_EvalObjList(sawo_Interp *interp, sawo_Obj *listObj);
SAWO_EXPORT int sawo_EvalObjPrefix(sawo_Interp *interp, sawo_Obj *prefix,
        int objc, sawo_Obj *const *objv);
#define sawo_EvalPrefix(i, p, oc, ov) sawo_EvalObjPrefix((i), sawo_NewStringObj((i), (p), -1), (oc), (ov))
SAWO_EXPORT int sawo_EvalNamespace(sawo_Interp *interp, sawo_Obj *scriptObj, sawo_Obj *nsObj);
SAWO_EXPORT int sawo_SubstObj (sawo_Interp *interp, sawo_Obj *substObjPtr,
        sawo_Obj **resObjPtrPtr, int flags);

SAWO_EXPORT void sawo_InitStack(sawo_Stack *stack);
SAWO_EXPORT void sawo_FreeStack(sawo_Stack *stack);
SAWO_EXPORT int sawo_StackLen(sawo_Stack *stack);
SAWO_EXPORT void sawo_StackPush(sawo_Stack *stack, void *element);
SAWO_EXPORT void * sawo_StackPop(sawo_Stack *stack);
SAWO_EXPORT void * sawo_StackPeek(sawo_Stack *stack);
SAWO_EXPORT void sawo_FreeStackElements(sawo_Stack *stack, void (*freeFunc)(void *ptr));

SAWO_EXPORT int sawo_InitHashTable (sawo_HashTable *ht,
        const sawo_HashTableType *type, void *privdata);
SAWO_EXPORT void sawo_ExpandHashTable (sawo_HashTable *ht,
        unsigned int size);
SAWO_EXPORT int sawo_AddHashEntry (sawo_HashTable *ht, const void *key,
        void *val);
SAWO_EXPORT int sawo_ReplaceHashEntry (sawo_HashTable *ht,
        const void *key, void *val);
SAWO_EXPORT int sawo_DeleteHashEntry (sawo_HashTable *ht,
        const void *key);
SAWO_EXPORT int sawo_FreeHashTable (sawo_HashTable *ht);
SAWO_EXPORT sawo_HashEntry * sawo_FindHashEntry (sawo_HashTable *ht,
        const void *key);
SAWO_EXPORT void sawo_ResizeHashTable (sawo_HashTable *ht);
SAWO_EXPORT sawo_HashTableIterator *sawo_GetHashTableIterator
        (sawo_HashTable *ht);
SAWO_EXPORT sawo_HashEntry * sawo_NextHashEntry
        (sawo_HashTableIterator *iter);

SAWO_EXPORT sawo_Obj * sawo_NewObj (sawo_Interp *interp);
SAWO_EXPORT void sawo_FreeObj (sawo_Interp *interp, sawo_Obj *objPtr);
SAWO_EXPORT void sawo_InvalidateStringRep (sawo_Obj *objPtr);
SAWO_EXPORT sawo_Obj * sawo_DuplicateObj (sawo_Interp *interp,
        sawo_Obj *objPtr);
SAWO_EXPORT const char * sawo_GetString(sawo_Obj *objPtr,
        int *lenPtr);
SAWO_EXPORT const char *sawo_String(sawo_Obj *objPtr);
SAWO_EXPORT int sawo_Length(sawo_Obj *objPtr);
SAWO_EXPORT sawo_Obj * sawo_NewStringObj (sawo_Interp *interp,
        const char *s, int len);
SAWO_EXPORT sawo_Obj *sawo_NewStringObjUtf8(sawo_Interp *interp,
        const char *s, int charlen);
SAWO_EXPORT sawo_Obj * sawo_NewStringObjNoAlloc (sawo_Interp *interp,
        char *s, int len);
SAWO_EXPORT void sawo_AppendString (sawo_Interp *interp, sawo_Obj *objPtr,
        const char *str, int len);
SAWO_EXPORT void sawo_AppendObj (sawo_Interp *interp, sawo_Obj *objPtr,
        sawo_Obj *appendObjPtr);
SAWO_EXPORT void sawo_AppendStrings (sawo_Interp *interp,
        sawo_Obj *objPtr, ...);
SAWO_EXPORT int sawo_StringEqObj(sawo_Obj *aObjPtr, sawo_Obj *bObjPtr);
SAWO_EXPORT int sawo_StringMatchObj (sawo_Interp *interp, sawo_Obj *patternObjPtr,
        sawo_Obj *objPtr, int nocase);
SAWO_EXPORT sawo_Obj * sawo_StringRangeObj (sawo_Interp *interp,
        sawo_Obj *strObjPtr, sawo_Obj *firstObjPtr,
        sawo_Obj *lastObjPtr);
SAWO_EXPORT sawo_Obj * sawo_FormatString (sawo_Interp *interp,
        sawo_Obj *fmtObjPtr, int objc, sawo_Obj *const *objv);
SAWO_EXPORT sawo_Obj * sawo_ScanString (sawo_Interp *interp, sawo_Obj *strObjPtr,
        sawo_Obj *fmtObjPtr, int flags);
SAWO_EXPORT int sawo_CompareStringImmediate (sawo_Interp *interp,
        sawo_Obj *objPtr, const char *str);
SAWO_EXPORT int sawo_StringCompareObj(sawo_Interp *interp, sawo_Obj *firstObjPtr,
        sawo_Obj *secondObjPtr, int nocase);
SAWO_EXPORT int sawo_StringCompareLenObj(sawo_Interp *interp, sawo_Obj *firstObjPtr,
        sawo_Obj *secondObjPtr, int nocase);
SAWO_EXPORT int sawo_Utf8Length(sawo_Interp *interp, sawo_Obj *objPtr);
SAWO_EXPORT sawo_Obj * sawo_NewReference (sawo_Interp *interp,
        sawo_Obj *objPtr, sawo_Obj *tagPtr, sawo_Obj *cmdNamePtr);
SAWO_EXPORT sawo_Reference * sawo_GetReference (sawo_Interp *interp,
        sawo_Obj *objPtr);
SAWO_EXPORT int sawo_SetFinalizer (sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *cmdNamePtr);
SAWO_EXPORT int sawo_GetFinalizer (sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj **cmdNamePtrPtr);
SAWO_EXPORT sawo_Interp * sawo_CreateInterp (void);
SAWO_EXPORT void sawo_FreeInterp (sawo_Interp *i);
SAWO_EXPORT int sawo_GetExitCode (sawo_Interp *interp);
SAWO_EXPORT const char *sawo_ReturnCode(int code);
SAWO_EXPORT void sawo_SetResultFormatted(sawo_Interp *interp, const char *format, ...);
SAWO_EXPORT void sawo_RegisterCoreCommands (sawo_Interp *interp);
SAWO_EXPORT int sawo_CreateCommand (sawo_Interp *interp,
        const char *cmdName, sawo_CmdRaise *cmdRaise, void *privData,
         sawo_DelCmdRaise *delRaise);
SAWO_EXPORT int sawo_DeleteCommand (sawo_Interp *interp,
        const char *cmdName);
SAWO_EXPORT int sawo_RenameCommand (sawo_Interp *interp,
        const char *oldName, const char *newName);
SAWO_EXPORT sawo_Cmd * sawo_GetCommand (sawo_Interp *interp,
        sawo_Obj *objPtr, int flags);
SAWO_EXPORT int sawo_SetVariable (sawo_Interp *interp,
        sawo_Obj *nameObjPtr, sawo_Obj *valObjPtr);
SAWO_EXPORT int sawo_SetVariableStr (sawo_Interp *interp,
        const char *name, sawo_Obj *objPtr);
SAWO_EXPORT int sawo_SetGlobalVariableStr (sawo_Interp *interp,
        const char *name, sawo_Obj *objPtr);
SAWO_EXPORT int sawo_SetVariableStrWithStr (sawo_Interp *interp,
        const char *name, const char *val);
SAWO_EXPORT int sawo_SetVariableLink (sawo_Interp *interp,
        sawo_Obj *nameObjPtr, sawo_Obj *targetNameObjPtr,
        sawo_CallFrame *targetCallFrame);
SAWO_EXPORT sawo_Obj * sawo_MakeGlobalNamespaceName(sawo_Interp *interp,
        sawo_Obj *nameObjPtr);
SAWO_EXPORT sawo_Obj * sawo_GetVariable (sawo_Interp *interp,
        sawo_Obj *nameObjPtr, int flags);
SAWO_EXPORT sawo_Obj * sawo_GetGlobalVariable (sawo_Interp *interp,
        sawo_Obj *nameObjPtr, int flags);
SAWO_EXPORT sawo_Obj * sawo_GetVariableStr (sawo_Interp *interp,
        const char *name, int flags);
SAWO_EXPORT sawo_Obj * sawo_GetGlobalVariableStr (sawo_Interp *interp,
        const char *name, int flags);
SAWO_EXPORT int sawo_UnsetVariable (sawo_Interp *interp,
        sawo_Obj *nameObjPtr, int flags);

SAWO_EXPORT sawo_CallFrame *sawo_GetCallFrameByLevel(sawo_Interp *interp,
        sawo_Obj *levelObjPtr);

SAWO_EXPORT int sawo_Collect (sawo_Interp *interp);
SAWO_EXPORT void sawo_CollectIfNeeded (sawo_Interp *interp);

SAWO_EXPORT int sawo_GetIndex (sawo_Interp *interp, sawo_Obj *objPtr,
        int *indexPtr);

SAWO_EXPORT sawo_Obj * sawo_NewListObj (sawo_Interp *interp,
        sawo_Obj *const *elements, int len);
SAWO_EXPORT void sawo_ListInsertElements (sawo_Interp *interp,
        sawo_Obj *listPtr, int listindex, int objc, sawo_Obj *const *objVec);
SAWO_EXPORT void sawo_ListAppendElement (sawo_Interp *interp,
        sawo_Obj *listPtr, sawo_Obj *objPtr);
SAWO_EXPORT void sawo_ListAppendList (sawo_Interp *interp,
        sawo_Obj *listPtr, sawo_Obj *appendListPtr);
SAWO_EXPORT int sawo_ListLength (sawo_Interp *interp, sawo_Obj *objPtr);
SAWO_EXPORT int sawo_ListIndex (sawo_Interp *interp, sawo_Obj *listPrt,
        int listindex, sawo_Obj **objPtrPtr, int seterr);
SAWO_EXPORT sawo_Obj *sawo_ListGetIndex(sawo_Interp *interp, sawo_Obj *listPtr, int idx);
SAWO_EXPORT int sawo_SetListIndex (sawo_Interp *interp,
        sawo_Obj *varNamePtr, sawo_Obj *const *indexv, int indexc,
        sawo_Obj *newObjPtr);
SAWO_EXPORT sawo_Obj * sawo_ConcatObj (sawo_Interp *interp, int objc,
        sawo_Obj *const *objv);
SAWO_EXPORT sawo_Obj *sawo_ListJoin(sawo_Interp *interp,
        sawo_Obj *listObjPtr, const char *joinStr, int joinStrLen);

SAWO_EXPORT sawo_Obj * sawo_NewDictObj (sawo_Interp *interp,
        sawo_Obj *const *elements, int len);
SAWO_EXPORT int sawo_DictKey (sawo_Interp *interp, sawo_Obj *dictPtr,
        sawo_Obj *keyPtr, sawo_Obj **objPtrPtr, int flags);
SAWO_EXPORT int sawo_DictKeysVector (sawo_Interp *interp,
        sawo_Obj *dictPtr, sawo_Obj *const *keyv, int keyc,
        sawo_Obj **objPtrPtr, int flags);
SAWO_EXPORT int sawo_SetDictKeysVector (sawo_Interp *interp,
        sawo_Obj *varNamePtr, sawo_Obj *const *keyv, int keyc,
        sawo_Obj *newObjPtr, int flags);
SAWO_EXPORT int sawo_DictPairs(sawo_Interp *interp,
        sawo_Obj *dictPtr, sawo_Obj ***objPtrPtr, int *len);
SAWO_EXPORT int sawo_DictAddElement(sawo_Interp *interp, sawo_Obj *objPtr,
        sawo_Obj *keyObjPtr, sawo_Obj *valueObjPtr);
SAWO_EXPORT int sawo_DictKeys(sawo_Interp *interp, sawo_Obj *objPtr, sawo_Obj *patternObj);
SAWO_EXPORT int sawo_DictValues(sawo_Interp *interp, sawo_Obj *dictObjPtr, sawo_Obj *patternObjPtr);
SAWO_EXPORT int sawo_DictSize(sawo_Interp *interp, sawo_Obj *objPtr);
SAWO_EXPORT int sawo_DictInfo(sawo_Interp *interp, sawo_Obj *objPtr);
SAWO_EXPORT int sawo_GetReturnCode (sawo_Interp *interp, sawo_Obj *objPtr,
        int *intPtr);

SAWO_EXPORT int sawo_EvalExpression (sawo_Interp *interp,
        sawo_Obj *exprObjPtr, sawo_Obj **exprResultPtrPtr);
SAWO_EXPORT int sawo_GetBoolFromExpr (sawo_Interp *interp,
        sawo_Obj *exprObjPtr, int *boolPtr);

SAWO_EXPORT int sawo_GetWide (sawo_Interp *interp, sawo_Obj *objPtr,
        sawo_wide *widePtr);
SAWO_EXPORT int sawo_GetLong (sawo_Interp *interp, sawo_Obj *objPtr,
        long *longPtr);

#define sawo_NewWideObj  sawo_NewIntObj
SAWO_EXPORT sawo_Obj * sawo_NewIntObj (sawo_Interp *interp,
        sawo_wide wideValue);

SAWO_EXPORT int sawo_GetDouble(sawo_Interp *interp, sawo_Obj *objPtr,
        double *doublePtr);
SAWO_EXPORT void sawo_SetDouble(sawo_Interp *interp, sawo_Obj *objPtr,
        double doubleValue);
SAWO_EXPORT sawo_Obj * sawo_NewDoubleObj(sawo_Interp *interp, double doubleValue);

SAWO_EXPORT void sawo_WrongNumArgs (sawo_Interp *interp, int argc,
        sawo_Obj *const *argv, const char *msg);
SAWO_EXPORT int sawo_GetEnum (sawo_Interp *interp, sawo_Obj *objPtr,
        const char * const *tablePtr, int *indexPtr, const char *name, int flags);
SAWO_EXPORT int sawo_ScriptIsComplete(sawo_Interp *interp,
        sawo_Obj *scriptObj, char *stateCharPtr);

SAWO_EXPORT int sawo_FindByName(const char *name, const char * const array[], size_t len);

typedef void (sawo_InterpDeleteRaise)(sawo_Interp *interp, void *data);
SAWO_EXPORT void * sawo_GetAssocData(sawo_Interp *interp, const char *key);
SAWO_EXPORT int sawo_SetAssocData(sawo_Interp *interp, const char *key,
        sawo_InterpDeleteRaise *delRaise, void *data);
SAWO_EXPORT int sawo_DeleteAssocData(sawo_Interp *interp, const char *key);
SAWO_EXPORT int sawo_PackageProvide (sawo_Interp *interp,
        const char *name, const char *ver, int flags);
SAWO_EXPORT int sawo_PackageRequire (sawo_Interp *interp,
        const char *name, int flags);

SAWO_EXPORT void sawo_MakeErrorMessage (sawo_Interp *interp);
SAWO_EXPORT int sawo_InteractivePrompt (sawo_Interp *interp);
SAWO_EXPORT void sawo_HistoryLoad(const char *filename);
SAWO_EXPORT void sawo_HistorySave(const char *filename);
SAWO_EXPORT char *sawo_HistoryGetline(const char *prompt);
SAWO_EXPORT void sawo_HistoryAdd(const char *line);
SAWO_EXPORT void sawo_HistoryShow(void);
SAWO_EXPORT int sawo_InitStaticExtensions(sawo_Interp *interp);
SAWO_EXPORT int sawo_StringToWide(const char *str, sawo_wide *widePtr, int base);
SAWO_EXPORT int sawo_IsBigEndian(void);

#define sawo_CheckSignal(i) ((i)->signal_level && (i)->sigmask)

SAWO_EXPORT int sawo_LoadLibrary(sawo_Interp *interp, const char *pathName);
SAWO_EXPORT void sawo_FreeLoadHandles(sawo_Interp *interp);
SAWO_EXPORT FILE *sawo_AioFilehandle(sawo_Interp *interp, sawo_Obj *command);
SAWO_EXPORT int sawo_IsDict(sawo_Obj *objPtr);
SAWO_EXPORT int sawo_IsList(sawo_Obj *objPtr);

#ifdef __cplusplus
}
#endif

#endif
