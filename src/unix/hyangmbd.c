/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#include <hyangdefn.h>

int hyangly_init_hyang(int ac, char **av);
void hyang_MainloopSetup(void);
void fpu_setup(hyangboolean start);
extern void hyang_CleanTempDir(void);

int hyangly_initEmbed(int argc, char **argv)
{
    hyangly_init_hyang(argc, argv);
    hyang_Interactive = TRUE;
    hyang_MainloopSetup();
    return(1);
}

void hyangly_endEmbed(int fatal)
{
    hyang_RunExitFinalizers();
    CleanEd();
    if(!fatal) KillAllDevices();
    hyang_CleanTempDir();
    if(!fatal && hyang_CollectWarnings)
	PrintWarnings();
    fpu_setup(FALSE);
}
