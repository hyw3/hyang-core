/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#define HYANG_USE_SIGNALS 1
#include <hyangdefn.h>

#include <locale.h>

#ifndef FD_SET
# ifdef HAVE_SYS_TIME_H
#  include <sys/time.h>
# endif
#endif

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#include <errno.h>

#include "hyangfio.h"

#define __SYSTEM__
#define HYANG_INTFC_PTRS 1
#include <hyangintfc.h>
#undef __SYSTEM__

#include "hyangunix.h"

attribute_hidden FILE *ifp = NULL;

attribute_hidden
hyangboolean UsingReadline = TRUE;

void hyang_Suicide(const char *s) { hyang_ptrSuicide(s); }
void hyang_ShowMessage(const char *s) { hyang_ptrShowMsg(s); }
int hyang_ReadConsole(const char *prompt, unsigned char *buf, int len, int addtohistory)
{ return hyang_ptrReadConsole(prompt, buf, len, addtohistory); }
void hyang_WriteConsole(const char *buf, int len) {if (hyang_ptrWriteConsole) hyang_ptrWriteConsole(buf, len); else hyang_ptrWriteConsoleEx(buf, len, 0); }
void hyang_WriteConsoleEx(const char *buf, int len, int otype) {if (hyang_ptrWriteConsole) hyang_ptrWriteConsole(buf, len); else hyang_ptrWriteConsoleEx(buf, len, otype); }
void hyang_ResetConsole(void) { hyang_ptrResetConsole(); }
#ifndef HAVE_AQUA
void hyang_FlushConsole(void) { hyang_ptrFlushConsole(); }
#endif
void hyang_ClearerrConsole(void) { hyang_ptrClearerrConsole(); }
void hyang_Busy(int which) { hyang_ptrBusy(which); }
void hyang_CleanUp(SA_TYPE saveact, int status, int runLast)
{ hyang_ptrCleanUp(saveact, status, runLast); }

attribute_hidden
int hyang_ShowFiles(int nfile, const char **file, const char **headers,
		const char *wtitle, hyangboolean del, const char *pager)
{ return hyang_ptrShowFiles(nfile, file, headers, wtitle, del, pager); }

attribute_hidden
int hyang_ChooseFile(int _new,  char *buf, int len)
{ return hyang_ptrChooseFile(_new, buf, len); }

void hyang_setStartTime(void);

#ifdef HAVE_AQUA
hyangboolean useaqua = FALSE;

#include <hyangexts/hyangdyl.h>
DL_FUNC ptr_do_flushconsole;
void hyang_FlushConsole(void) {
    if (hyang_ptrFlushConsole) hyang_ptrFlushConsole();
    else if (ptr_do_flushconsole) ptr_do_flushconsole();
}
#endif


void hyang_setupHistory()
{
    int value, ierr;
    char *p;

    if ((hyang_HistoryFile = getenv("HYANG_HISTFILE")) == NULL)
	hyang_HistoryFile = ".hyanghistory";
    hyang_HistorySize = 512;
    if ((p = getenv("HYANG_HISTSIZE"))) {
	value = (int) hyang_Decode2Long(p, &ierr);
	if (ierr != 0 || value < 0)
	    hyang_ShowMessage("WARNING: invalid HYANG_HISTSIZE ignored;");
	else
	    hyang_HistorySize = value;
    }
}

#if defined(HAVE_SYS_RESOURCE_H) && defined(HAVE_GETRLIMIT)
# ifdef HAVE_SYS_TIME_H
#  include <sys/time.h>
# endif
# include <sys/resource.h>
# ifdef HAVE_LIBC_STACK_END
extern void * __libc_stack_end;
# endif
# ifdef HAVE_KERN_USRSTACK
#  include <unistd.h>
#  include <sys/types.h>
#  include <sys/sysctl.h>
# endif
#endif

int hyang_running_as_main_program = 0;

extern uintptr_t dummy_ii(void);

static int num_initialized = 0;

static char* unescape_arg(char *p, char* avp) {
    char *q;
    for(q = avp; *q; q++) {
	if(*q == '~' && *(q+1) == '+' && *(q+2) == '~') {
	    q += 2;
	    *p++ = ' ';
	} else *p++ = *q;
    }
    return p;
}

#if defined(HAVE_THREAD_H)
# include <thread.h>
#endif
#include <signal.h>

int hyangly_init_hyang(int ac, char **av)
{
    int i, ioff = 1, j;
    hyangboolean useX11 = TRUE, useTk = FALSE;
    char *p, msg[1024], cmdlines[10000], **avv;
    structHyangStart rstart;
    hyangstart Rp = &rstart;
    hyangboolean force_interactive = FALSE;

    if (num_initialized++) {
	fprintf(stderr, "%s", "Hyang is already initialized\n");
	exit(1);
    }


#if defined(HAVE_SYS_RESOURCE_H) && defined(HAVE_GETRLIMIT)
{
    struct rlimit rlim;

    {
	uintptr_t ii = dummy_ii();

	hyang_CStackDir = ((uintptr_t)&i > ii) ? 1 : -1;
    }

    if(getrlimit(RLIMIT_STACK, &rlim) == 0) {
	rlim_t lim = rlim.rlim_cur;
#if defined(RLIM_SAVED_CUR) && defined(RLIM_SAVED_MAX)
	if (lim == RLIM_SAVED_CUR || lim == RLIM_SAVED_MAX)
	    lim = RLIM_INFINITY;
#endif
	if (lim != RLIM_INFINITY) hyang_CStackLim = (uintptr_t) lim;
    }
#if defined(HAVE_LIBC_STACK_END)
    {
	hyang_CStackStart = (uintptr_t) __libc_stack_end;
	FILE *f;
	f = fopen("/proc/self/maps", "r");
	if (f) {
	    for(;;) {
		int c;
		unsigned long start, end;

		if (fscanf(f, "%lx-%lx", &start, &end) == 2 &&
		    hyang_CStackStart >= (uintptr_t)start &&
		    hyang_CStackStart < (uintptr_t)end) {
		    hyang_CStackStart = (uintptr_t) ((hyang_CStackDir == 1) ? end : start);
		    break;
		}
		for(c = getc(f); c != '\n' && c != EOF; c = getc(f));
		if (c == EOF) {
		    fprintf(stderr, "WARNING: Error parsing /proc/self/maps!\n");
		    break;
		}
	    }
	    fclose(f);
	}
    }
#elif defined(HAVE_KERN_USRSTACK)
    {
	int nm[2] = {CTL_KERN, KERN_USRSTACK};
	void * base;
	size_t len = sizeof(void *);
	(void) sysctl(nm, 2, &base, &len, NULL, 0);
	hyang_CStackStart = (uintptr_t) base;
    }
#elif defined(HAVE_THR_STKSEGMENT)
    {
	stack_t stack;
	if (thr_stksegment(&stack))
	    hyang_Suicide("Cannot obtain stack information (thr_stksegment).");
	hyang_CStackStart = (uintptr_t) stack.ss_sp;
    }
#else
    if(hyang_running_as_main_program) {
	hyang_CStackStart = (uintptr_t) &i + (6000 * hyang_CStackDir);
    }
#endif
    if(hyang_CStackStart == (uintptr_t)(-1)) hyang_CStackLim = (uintptr_t)(-1);
}
#endif

    hyang_ptrSuicide = hyangStd_Suicide;
    hyang_ptrShowMsg = hyangStd_ShowMessage;
    hyang_ptrReadConsole = hyangStd_ReadConsole;
    hyang_ptrWriteConsole = hyangStd_WriteConsole;
    hyang_ptrResetConsole = hyangStd_ResetConsole;
    hyang_ptrFlushConsole = hyangStd_FlushConsole;
    hyang_ptrClearerrConsole = hyangStd_ClearerrConsole;
    hyang_ptrBusy = hyangStd_Busy;
    hyang_ptrCleanUp = hyangStd_CleanUp;
    hyang_ptrShowFiles = hyangStd_ShowFiles;
    hyang_ptrChooseFile = hyangStd_ChooseFile;
    hyang_ptrloadhistory = hyangStd_loadhistory;
    hyang_ptrsavehistory = hyangStd_savehistory;
    hyang_ptraddhistory = hyangStd_addhistory;
    hyang_ptrEditFile = NULL;
    hyang_timeoutHandler = NULL;
    hyang_timeoutVal = 0;

    hyang_GlobalCtx = NULL;

    if((hyang_Home = hyang_HomeDir()) == NULL)
	hyang_Suicide("Hyang home directory is not defined");
    BindDomain(hyang_Home);

    process_system_hyangenviron();

    hyang_setStartTime();
    hyang_DefParams(Rp);
    hyang_setCmdLineArgs(ac, av);
    cmdlines[0] = '\0';

    for(i = 0, avv = av; i < ac; i++, avv++) {
	if (!strcmp(*avv, "--args"))
	    break;
	if(!strncmp(*avv, "--gui", 5) || !strncmp(*avv, "-g", 2)) {
	    if(!strncmp(*avv, "--gui", 5) && strlen(*avv) >= 7)
		p = &(*avv)[6];
	    else {
		if(i+1 < ac) {
		    avv++; p = *avv; ioff++;
		} else {
		    snprintf(msg, 1024,
			    _("WARNING: --gui or -g without value ignored"));
		    hyang_ShowMessage(msg);
		    p = "X11";
		}
	    }
	    if(!strcmp(p, "none"))
		useX11 = FALSE;
#ifdef HAVE_AQUA
	    else if(!strcmp(p, "aqua"))
		useaqua = TRUE;
#endif
	    else if(!strcmp(p, "X11") || !strcmp(p, "x11"))
		useX11 = TRUE;
	    else if(!strcmp(p, "Tk") || !strcmp(p, "tk"))
		useTk = TRUE;
	    else {
#ifdef HAVE_X11
		snprintf(msg, 1024,
			 _("WARNING: unknown gui '%s', using X11\n"), p);
#else
		snprintf(msg, 1024,
			 _("WARNING: unknown gui '%s', using none\n"), p);
#endif
		hyang_ShowMessage(msg);
	    }
	    for(j = i; j < ac - ioff; j++)
		av[j] = av[j + ioff];
	    ac -= ioff;
	    break;
	}
    }

#ifdef HAVE_X11
    if(useX11) hyang_GUIType = "X11";
#endif

#ifdef HAVE_AQUA
    if(useaqua) hyang_GUIType = "AQUA";
#endif

#ifdef HAVE_TCLTK
    if(useTk) hyang_GUIType = "Tk";
#endif

    hyang_common_command_line(&ac, av, Rp);
    while (--ac) {
	if (**++av == '-') {
	    if(!strcmp(*av, "--no-readline")) {
		UsingReadline = FALSE;
	    } else if(!strcmp(*av, "-f")) {
		ac--; av++;
#define HYANG_INIT_TREAT_F(_AV_)				        \
		Rp->hyang_Interactive = FALSE;				\
		if(strcmp(_AV_, "-")) {					\
		    char path[PATH_MAX], *p = path;			\
		    p = unescape_arg(p, _AV_);				\
		    *p = '\0';						\
		    ifp = hyang_fopen(path, "r");			\
		    if(!ifp) {						\
			snprintf(msg, 1024,				\
				 _("cannot open file '%s': %s"),	\
				 path, strerror(errno));		\
			hyang_Suicide(msg);				\
		    }							\
		}
		HYANG_INIT_TREAT_F(*av);

	    } else if(!strncmp(*av, "--file=", 7)) {

		HYANG_INIT_TREAT_F((*av)+7);

	    } else if(!strcmp(*av, "-e")) {
		ac--; av++;
		Rp->hyang_Interactive = FALSE;
		if(strlen(cmdlines) + strlen(*av) + 2 <= 10000) {
		    char *p = cmdlines+strlen(cmdlines);
		    p = unescape_arg(p, *av);
		    *p++ = '\n'; *p = '\0';
		} else {
		    snprintf(msg, 1024, _("WARNING: '-e %s' omitted as input is too long\n"), *av);
		    hyang_ShowMessage(msg);
		}
	    } else if(!strcmp(*av, "--args")) {
		break;
	    } else if(!strcmp(*av, "--interactive")) {
		force_interactive = TRUE;
		break;
	    } else {
#ifdef HAVE_AQUA
		if(!strncmp(*av, "-psn", 4)) break; else
#endif
		snprintf(msg, 1024, _("WARNING: unknown option '%s'\n"), *av);
		hyang_ShowMessage(msg);
	    }
	} else {
	    snprintf(msg, 1024, _("ARGUMENT '%s' __ignored__\n"), *av);
	    hyang_ShowMessage(msg);
	}
    }

    if(strlen(cmdlines)) {
	size_t res;
	if(ifp) hyang_Suicide(_("cannot use -e with -f or --file"));
	ifp = tmpfile();
	if(!ifp) hyang_Suicide(_("creating temporary file for '-e' failed"));
	res = fwrite(cmdlines, strlen(cmdlines)+1, 1, ifp);
	if(res != 1) error("fwrite error in initialize_R");
	fflush(ifp);
	rewind(ifp);
    }
    if (ifp && Rp->SaveAction != SA_SAVE) Rp->SaveAction = SA_NOSAVE;

    hyang_SetParams(Rp);

    if(!Rp->Nohyangenviron) {
	process_site_hyangenviron();
	process_user_hyangenviron();

	hyang_SizeFromEnv(Rp);
	hyang_SetParams(Rp);
    }

#ifdef HAVE_AQUA
    if(useaqua)
	hyang_Interactive = useaqua;
    else
#endif
	hyang_Interactive = hyang_Interactive && (force_interactive || isatty(0));

#ifdef HAVE_AQUA
    if(useaqua ||
       (hyang_Interactive && getenv("TERM") && strcmp(getenv("TERM"), "dumb"))) {
	hyang_Outputfile = NULL;
	hyang_Consolefile = NULL;
	hyang_ptrWriteConsoleEx = hyangStd_WriteConsoleEx;
	hyang_ptrWriteConsole = NULL;
    } else {
#endif
	hyang_Outputfile = stdout;
	hyang_Consolefile = stderr;
#ifdef HAVE_AQUA
    }
#endif

    if (!hyang_Interactive && Rp->SaveAction != SA_SAVE &&
	Rp->SaveAction != SA_NOSAVE)
	hyang_Suicide(_("you must specify '--save', '--no-save' or '--vanilla'"));

    hyang_setupHistory();
    if (hyang_RestoreHistory)
	hyangStd_read_history(hyang_HistoryFile);
    fpu_setup(1);

    return(0);
}

int hyang_EditFiles(int nfile, const char **file, const char **title,
		const char *editor)
{
    char  buf[1024];

    if (hyang_ptrEditFiles) return(hyang_ptrEditFiles(nfile, file, title, editor));

    if (nfile > 0) {
	if (nfile > 1)
	    hyang_ShowMessage(_("WARNING: Only editing the first in the list of files"));

	if (hyang_ptrEditFile) hyang_ptrEditFile((char *) file[0]);
	else {
	    if (editor[0] != '"' && hyangly_strchr(editor, ' '))
		snprintf(buf, 1024, "\"%s\" \"%s\"", editor, file[0]);
	    else
		snprintf(buf, 1024, "%s \"%s\"", editor, file[0]);
	    if (hyang_system(buf) == 127)
		warningcall(hyang_AbsurdValue, _("error in running command"));
	}
	return 0;
    }
    return 1;
}

int hyang_GetFDLimit() {

#if defined(HAVE_SYS_RESOURCE_H) && defined(HAVE_GETRLIMIT)
    struct rlimit rlim;
    if (getrlimit(RLIMIT_NOFILE, &rlim) == 0) {
	rlim_t lim = rlim.rlim_cur;
#if defined(RLIM_SAVED_CUR) && defined(RLIM_SAVED_MAX)
	if (lim == RLIM_SAVED_CUR || lim == RLIM_SAVED_MAX)
	    lim = RLIM_INFINITY;
#endif
	return (int)((lim > INT_MAX) ? INT_MAX : lim);
    }
#endif
    return -1;
}

int hyang_EnsureFDLimit(int desired) {

#if defined(HAVE_SYS_RESOURCE_H) && defined(HAVE_SETRLIMIT) && defined(HAVE_GETRLIMIT)
    struct rlimit rlim;
    if (getrlimit(RLIMIT_NOFILE, &rlim))
	return -1;
    rlim_t lim = rlim.rlim_cur;
#if defined(RLIM_SAVED_CUR) && defined(RLIM_SAVED_MAX)
    if (lim == RLIM_SAVED_CUR || lim == RLIM_SAVED_MAX)
	lim = RLIM_INFINITY;
#endif
    if (lim == RLIM_INFINITY || lim >= desired)
	return desired;

    rlim_t hlim = rlim.rlim_max;
#if defined(RLIM_SAVED_CUR) && defined(RLIM_SAVED_MAX)
    if (hlim == RLIM_SAVED_CUR || hlim == RLIM_SAVED_MAX)
	hlim = RLIM_INFINITY;
#endif
    if (hlim == RLIM_INFINITY || hlim >= desired)
	rlim.rlim_cur = (rlim_t) desired;
    else
	rlim.rlim_cur = hlim;
    if (setrlimit(RLIMIT_NOFILE, &rlim))
	return (int) lim;
    return (int) rlim.rlim_cur;
#else
    return -1;
#endif
}
