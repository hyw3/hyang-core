/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyangexts/hyangextstu.h>

void fpu_setup(hyangboolean);
void hyangStd_read_history(const char *s);
void hyangStd_Suicide(const char *s);
void hyangStd_ShowMessage(const char *s);
int  hyangStd_ReadConsole(const char *prompt, unsigned char *buf, int len,
		          int addtohistory);
void hyangStd_WriteConsole(const char *buf, int len);
void hyangStd_WriteConsoleEx(const char *buf, int len, int otype);
void hyangStd_ResetConsole(void);
void hyangStd_FlushConsole(void);
void hyangStd_ClearerrConsole(void);
void hyangStd_Busy(int which);
void NORET hyangStd_CleanUp(SA_TYPE saveact, int status, int runLast);
int  hyangStd_ShowFiles(int nfile, const char **file, const char **headers,
		        const char *wtitle, hyangboolean del, const char *pager);
int  hyangStd_ChooseFile(int _new, char *buf, int len);
void hyangStd_loadhistory(SEXP call, SEXP op, SEXP args, SEXP env);
void hyangStd_savehistory(SEXP call, SEXP op, SEXP args, SEXP env);
void hyangStd_addhistory(SEXP call, SEXP op, SEXP args, SEXP env);
void hyang_load_X11_shlib(void);
