/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#define HYANG_USE_SIGNALS 1
#include <hyangdefn.h>
#include <hyangintl.h>

#ifdef HAVE_STRINGS_H
  #include <strings.h>
#endif

#include "hyangfio.h"
#include "hyangunix.h"
#include "hyangstu.h"
#include <hyangexts/hyangiconv.h>
#include <hyangexts/hyangextprint.h>

#define __SYSTEM__
#include <hyangexts/hyangloop.h>
#undef __SYSTEM__

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

extern SA_TYPE SaveAction;
extern hyangboolean UsingReadline;
extern FILE* ifp;

void attribute_hidden hyangStd_Suicide(const char *s)
{
    REprintf("Fatal error: %s\n", s);
    hyang_CleanUp(SA_SUICIDE, 2, 0);
}

static SIGJMP_BUF seljmpbuf;

static RETSIGTYPE (*oldSigintHandler)(int) = SIG_DFL;

typedef void (*sel_intr_handler_t)(void);

static RETSIGTYPE NORET handleSelectInterrupt(int dummy)
{
    signal(SIGINT, oldSigintHandler);
    SIGLONGJMP(seljmpbuf, 1);
}

int hyang_SelectEx(int  n,  fd_set  *readfds,  fd_set  *writefds,
	       fd_set *exceptfds, struct timeval *timeout,
	       void (*intr)(void))
{
    if (timeout != NULL && timeout->tv_sec == 0 && timeout->tv_usec == 0)
	return select(n, readfds, writefds, exceptfds, timeout);
    else {
	volatile sel_intr_handler_t myintr = intr != NULL ?
	    intr : onintrNoResume;
	volatile int old_interrupts_suspended = hyang_interrupts_suspended;
	if (SIGSETJMP(seljmpbuf, 1)) {
	    myintr();
	    hyang_interrupts_suspended = old_interrupts_suspended;
	    error(_("interrupt handler must not return"));
	    return 0;
	}
	else {
	    int val;

	    hyang_interrupts_suspended = FALSE;

	    oldSigintHandler = signal(SIGINT, handleSelectInterrupt);

	    if (hyang_interrupts_pending)
		myintr();

	    val = select(n, readfds, writefds, exceptfds, timeout);
	    signal(SIGINT, oldSigintHandler);
	    hyang_interrupts_suspended = old_interrupts_suspended;
	    return val;
	}
    }
}

static InputHandler BasicInputHandler = {StdinActivity, -1, NULL};

InputHandler *hyang_InputHandlers = &BasicInputHandler;

InputHandler * initStdinHandler(void)
{
    InputHandler *inputs;

    inputs = addInputHandler(hyang_InputHandlers, fileno(stdin), NULL,
			     StdinActivity);
    return(inputs);
}

InputHandler *
addInputHandler(InputHandler *handlers, int fd, InputHandlerProc handler,
		int activity)
{
    InputHandler *input, *tmp;
    input = (InputHandler*) calloc(1, sizeof(InputHandler));

    input->activity = activity;
    input->fileDescriptor = fd;
    input->handler = handler;

    tmp = handlers;

    if(handlers == NULL) {
	hyang_InputHandlers = input;
	return(input);
    }

    while(tmp->next != NULL) {
	tmp = tmp->next;
    }
    tmp->next = input;

    return(input);
}

int
removeInputHandler(InputHandler **handlers, InputHandler *it)
{
    InputHandler *tmp;

    if (it == NULL) return(0);

    if(*handlers == it) {
	*handlers = (*handlers)->next;
	free(it);
	return(1);
    }

    tmp = *handlers;

    while(tmp) {
	if(tmp->next == it) {
	    tmp->next = it->next;
	    free(it);
	    return(1);
	}
	tmp = tmp->next;
    }

    return(0);
}


InputHandler *
getInputHandler(InputHandler *handlers, int fd)
{
    InputHandler *tmp;
    tmp = handlers;

    while(tmp != NULL) {
	if(tmp->fileDescriptor == fd)
	    return(tmp);
	tmp = tmp->next;
    }

    return(tmp);
}

static void nop(void){}

void (* hyang_PolledEvents)(void) = nop;
int hyang_waitUsec = 0; /* 0 means no timeout */

void (* hyang_gPolledEvents)(void) = nop;
int hyang_gWaitUsec = 0;

static int setSelectMask(InputHandler *, fd_set *);

fd_set *hyang_checkActivityEx(int usec, int ignore_stdin, void (*intr)(void))
{
    int maxfd;
    struct timeval tv;
    static fd_set readMask;

    if (hyang_interrupts_pending) {
	if (intr != NULL) intr();
	else onintr();
    }

    tv.tv_sec = usec/1000000;
    tv.tv_usec = usec % 1000000;
    maxfd = setSelectMask(hyang_InputHandlers, &readMask);
    if (ignore_stdin)
	FD_CLR(fileno(stdin), &readMask);
    if (hyang_SelectEx(maxfd+1, &readMask, NULL, NULL,
		   (usec >= 0) ? &tv : NULL, intr) > 0)
	return(&readMask);
    else
	return(NULL);
}

fd_set *hyang_checkActivity(int usec, int ignore_stdin)
{
    return hyang_checkActivityEx(usec, ignore_stdin, NULL);
}

static int
setSelectMask(InputHandler *handlers, fd_set *readMask)
{
    int maxfd = -1;
    InputHandler *tmp = handlers;
    FD_ZERO(readMask);

    if(handlers == &BasicInputHandler)
	handlers->fileDescriptor = fileno(stdin);

    while(tmp) {
	FD_SET(tmp->fileDescriptor, readMask);
	maxfd = maxfd < tmp->fileDescriptor ? tmp->fileDescriptor : maxfd;
	tmp = tmp->next;
    }

    return(maxfd);
}

void hyang_runHandlers(InputHandler *handlers, fd_set *readMask)
{
    InputHandler *tmp = handlers, *next;

    if (readMask == NULL) {
	hyang_gPolledEvents();
	hyang_PolledEvents();
    } else
	while(tmp) {
	    next = tmp->next;
	    if(FD_ISSET(tmp->fileDescriptor, readMask)
	       && tmp->handler != NULL)
		tmp->handler((void*) tmp->userData);
	    tmp = next;
	}
}

InputHandler *
getSelectedHandler(InputHandler *handlers, fd_set *readMask)
{
    InputHandler *tmp = handlers;

    if(handlers == &BasicInputHandler && handlers->next)
	tmp = handlers->next;

    while(tmp) {
	if(FD_ISSET(tmp->fileDescriptor, readMask))
	    return(tmp);
	tmp = tmp->next;
    }

    if(FD_ISSET(handlers->fileDescriptor, readMask))
	return(handlers);

    return((InputHandler*) NULL);
}


#ifdef HAVE_LIBREADLINE

# include <readline/readline.h>

# if !defined (_RL_FUNCTION_TYPEDEF)
typedef void rl_vcpfunc_t (char *);
# endif

# if defined(RL_READLINE_VERSION) && RL_READLINE_VERSION >= 0x0603
#  define NEED_INT_HANDLER
# endif

attribute_hidden
char *hyang_ExpandFileName_readline(const char *s, char *buff)
{
#if defined(__APPLE__)
    char *s2 = tilde_expand((char *)s);
#else
    char *s2 = tilde_expand(s);
#endif

    strncpy(buff, s2, PATH_MAX);
    if(strlen(s2) >= PATH_MAX) buff[PATH_MAX-1] = '\0';
    free(s2);
    return buff;
}


# ifdef HAVE_READLINE_HISTORY_H
#  include <readline/history.h>
# endif

typedef struct _HYANG_ReadlineData hyang_ReadlineData;

struct _HYANG_ReadlineData {

 int readline_gotaline;
 int readline_addtohistory;
 int readline_len;
 int readline_eof;
 unsigned char *readline_buf;
 hyang_ReadlineData *prev;

};

static hyang_ReadlineData *rl_top = NULL;

#define MAX_READLINE_NESTING 10

static struct {
  int current;
  int max;
  rl_vcpfunc_t *fun[MAX_READLINE_NESTING];
} ReadlineStack = {-1, MAX_READLINE_NESTING - 1};

#ifdef NEED_INT_HANDLER
static volatile hyangboolean caught_sigwinch = FALSE;

static RETSIGTYPE
hyang_readline_sigwinch_handler(int sig)
{
    caught_sigwinch = TRUE;
}
#endif

static void
pushReadline(const char *prompt, rl_vcpfunc_t f)
{
   if(ReadlineStack.current >= ReadlineStack.max) {
     warning(_("An unusual circumstance has arisen in the nesting of readline input. Please report using bug.report()"));
   } else
     ReadlineStack.fun[++ReadlineStack.current] = f;

   rl_callback_handler_install(prompt, f);

#ifdef NEED_INT_HANDLER
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = &hyang_readline_sigwinch_handler;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGWINCH, &sa, NULL);
#endif

   fflush(stdout);
}

#if defined(RL_READLINE_VERSION) && RL_READLINE_VERSION >= 0x0600
static void resetReadline(void)
{
    rl_free_line_state();
    rl_cleanup_after_signal();
    RL_UNSETSTATE(RL_STATE_ISEARCH | RL_STATE_NSEARCH | RL_STATE_VIMOTION |
		  RL_STATE_NUMERICARG | RL_STATE_MULTIKEY);
    rl_line_buffer[rl_point = rl_end = rl_mark = 0] = 0;
    rl_done = 1;
}
#endif

static void popReadline(void)
{
  if(ReadlineStack.current > -1) {
#if defined(RL_READLINE_VERSION) && RL_READLINE_VERSION >= 0x0600
     resetReadline();
#endif
     rl_callback_handler_remove();
     ReadlineStack.fun[ReadlineStack.current--] = NULL;
     if(ReadlineStack.current > -1 && ReadlineStack.fun[ReadlineStack.current])
	rl_callback_handler_install("", ReadlineStack.fun[ReadlineStack.current]);
  }
}

static void readline_handler(char *line)
{
    hyang_size_t buflen = rl_top->readline_len;

    popReadline();

    if ((rl_top->readline_eof = !line))
	return;
    if (line[0]) {
# ifdef HAVE_READLINE_HISTORY_H
	if (strlen(line) && rl_top->readline_addtohistory)
	    add_history(line);
# endif
	strncpy((char *)rl_top->readline_buf, line, buflen);
	size_t l = strlen(line);
	if(l < buflen - 1) {
	    rl_top->readline_buf[l] = '\n';
	    rl_top->readline_buf[l+1] = '\0';
	}
    }
    else {
	rl_top->readline_buf[0] = '\n';
	rl_top->readline_buf[1] = '\0';
    }
    free(line);
    rl_top->readline_gotaline = 1;
}

static void
handleInterrupt(void)
{
    popReadline();
    onintrNoResume();
}

#ifdef HAVE_RL_COMPLETION_MATCHES

static char **hyang_custom_completion(const char *text, int start, int end);
static char *hyang_completion_generator(const char *text, int state);

static SEXP
    hyangCompl_assignBuffSym,
    hyangCompl_assignStartSym,
    hyangCompl_assignEndSym,
    hyangCompl_assignTokenSym,
    hyangCompl_completeTokenSym,
    hyangCompl_getFileComplSym,
    hyangCompl_retrieveComplsSym;

attribute_hidden
void set_rl_word_breaks(const char *str)
{
    static char p1[201], p2[203];
    strncpy(p1, str, 200); p1[200]= '\0';
    strncpy(p2, p1, 200); p2[200] = '\0';
    strcat(p2, "[]");
    rl_basic_word_break_characters = p2;
    rl_completer_word_break_characters = p1;
}

static int rcompgen_active = -1;
static SEXP rcompgen_rho;

#include <hyangexts/hyangextparse.h>
static void initialize_rlcompletion(void)
{
    if(rcompgen_active >= 0) return;

    if(rcompgen_active < 0) {
	char *p = getenv("HYANG_COMPLETION");
	if(p && streql(p, "FALSE")) {
	    rcompgen_active = 0;
	    return;
	}
	if(findVarInFrame(hyang_NamespaceRegistry, install("utils"))
	   != hyang_UnboundValue) rcompgen_active = 1;
	else {
	    SEXP cmdSexp, cmdexpr;
	    ParseStatus status;
	    int i;
	    char *p = "try(loadNamespace('hyangcompgen'), silent=TRUE)";

	    PROTECT(cmdSexp = mkString(p));
	    cmdexpr = PROTECT(hyang_ParseVect(cmdSexp, -1, &status, hyang_AbsurdValue));
	    if(status == PARSE_OK) {
		for(i = 0; i < length(cmdexpr); i++)
		    eval(VECTOR_ELT(cmdexpr, i), hyang_GlobalEnv);
	    }
	    UNPROTECT(2);
	    if(findVarInFrame(hyang_NamespaceRegistry, install("utils"))
	       != hyang_UnboundValue) rcompgen_active = 1;
	    else {
		rcompgen_active = 0;
		return;
	    }
	}
    }

    rcompgen_rho = hyang_FindNamespace(mkString("utils"));

    hyangCompl_assignBuffSym  = install(".assignLinebuffer");
    hyangCompl_assignStartSym   = install(".assignStart");
    hyangCompl_assignEndSym     = install(".assignEnd");
    hyangCompl_assignTokenSym   = install(".assignToken");
    hyangCompl_completeTokenSym = install(".completeToken");
    hyangCompl_getFileComplSym   = install(".getFileComp");
    hyangCompl_retrieveComplsSym = install(".retrieveCompletions");

    rl_attempted_completion_function = hyang_custom_completion;

#ifdef HAVE_RL_SORT_COMPLETION_MATCHES
    rl_sort_completion_matches = 0;
#endif

    return;
}

static char **
hyang_custom_completion(const char *text, int start, int end)
{
    char **matches = (char **)NULL;
    SEXP infile,
	linebufferCall = PROTECT(lang2(hyangCompl_assignBuffSym,
				       mkString(rl_line_buffer))),
	startCall = PROTECT(lang2(hyangCompl_assignStartSym, ScalarInteger(start))),
	endCall = PROTECT(lang2(hyangCompl_assignEndSym,ScalarInteger(end)));
    SEXP filecompCall;

    rl_completion_append_character = '\0';

    eval(linebufferCall, rcompgen_rho);
    eval(startCall, rcompgen_rho);
    eval(endCall, rcompgen_rho);
    UNPROTECT(3);
    matches = rl_completion_matches(text, hyang_completion_generator);
    filecompCall = PROTECT(lang1(hyangCompl_getFileComplSym));
    infile = PROTECT(eval(filecompCall, rcompgen_rho));
    if (!asLogical(infile)) rl_attempted_completion_over = 1;
    UNPROTECT(2);
    return matches;
}

static char *hyang_completion_generator(const char *text, int state)
{
    static int list_index, ncomp;
    static char **compstrings;

    if (!state) {
	int i;
	SEXP completions,
	    assignCall = PROTECT(lang2(hyangCompl_assignTokenSym, mkString(text))),
	    completionCall = PROTECT(lang1(hyangCompl_completeTokenSym)),
	    retrieveCall = PROTECT(lang1(hyangCompl_retrieveComplsSym));
	const void *vmax = vmaxget();

	eval(assignCall, rcompgen_rho);
	eval(completionCall, rcompgen_rho);
	PROTECT(completions = eval(retrieveCall, rcompgen_rho));
	list_index = 0;
	ncomp = length(completions);
	if (ncomp > 0) {
	    compstrings = (char **) malloc(ncomp * sizeof(char*));
	    if (!compstrings) {
		UNPROTECT(4);
		return (char *)NULL;
	    }
	    for (i = 0; i < ncomp; i++)
		compstrings[i] = strdup(translateChar(STRING_ELT(completions, i)));
	}
	UNPROTECT(4);
	vmaxset(vmax);
    }

    if (list_index < ncomp)
	return compstrings[list_index++];
    else {
	if (ncomp > 0) free(compstrings);
    }
    return (char *)NULL;
}

#else
attribute_hidden
void set_rl_word_breaks(const char *str)
{
}
#endif

#else
static void
handleInterrupt(void)
{
    onintrNoResume();
}
#endif

static void *cd = NULL;

int attribute_hidden
hyangStd_ReadConsole(const char *prompt, unsigned char *buf, int len,
		 int addtohistory)
{
    if(!hyang_Interactive) {
	size_t ll;
	int err = 0;
	if (!hyang_Slave) {
	    fputs(prompt, stdout);
	    fflush(stdout);
	}
	if (fgets((char *)buf, len, ifp ? ifp: stdin) == NULL)
	    return 0;
	ll = strlen((char *)buf);
	if (ll >= 2 && buf[ll - 1] == '\n' && buf[ll - 2] == '\r') {
	    buf[ll - 2] = '\n';
	    buf[--ll] = '\0';
	}
	if(strlen(hyang_StdinEnc) && strcmp(hyang_StdinEnc, "native.enc")) {
	    size_t res, inb = strlen((char *)buf), onb = len;
	    char obuf[CONSOLE_BUFFER_SIZE+1];
	    const char *ib = (const char *)buf;
	    char *ob = obuf;
	    if(!cd) {
		cd = hyangiconv_open("", hyang_StdinEnc);
		if(cd == (void *)-1) error(_("encoding '%s' is not recognised"), hyang_StdinEnc);
	    }
	    res = hyangiconv(cd, &ib, &inb, &ob, &onb);
	    *ob = '\0';
	    err = res == (size_t)(-1);
	    if(err) printf(_("<ERROR: re-encoding failure from encoding '%s'>\n"),
			   hyang_StdinEnc);
	    strncpy((char *)buf, obuf, len);
	}
	if ((err || feof(ifp ? ifp : stdin))
	    && (ll == 0 || buf[ll - 1] != '\n') && ll < (size_t)len) {
	    buf[ll++] = '\n'; buf[ll] = '\0';
	}
	if (!hyang_Slave) {
	    fputs((char *)buf, stdout);
	    fflush(stdout);
	}
	return 1;
    }
    else {
#ifdef HAVE_LIBREADLINE
	hyang_ReadlineData rl_data;
	if (UsingReadline) {
	    rl_data.readline_gotaline = 0;
	    rl_data.readline_buf = buf;
	    rl_data.readline_addtohistory = addtohistory;
	    rl_data.readline_len = len;
	    rl_data.readline_eof = 0;
	    rl_data.prev = rl_top;
	    rl_top = &rl_data;
	    rl_readline_name = "hyang";
	    pushReadline(prompt, readline_handler);
#ifdef HAVE_RL_COMPLETION_MATCHES
	    initialize_rlcompletion();
#endif
	}
	else
#endif
	{
	    fputs(prompt, stdout);
	    fflush(stdout);
	}

	if(hyang_InputHandlers == NULL)
	    initStdinHandler();

	for (;;) {
	    fd_set *what;

	    int wt = -1;
	    if (hyang_waitUsec > 0) wt = hyang_waitUsec;
	    if (hyang_gWaitUsec > 0 && (wt < 0 || wt > hyang_gWaitUsec))
		wt = hyang_gWaitUsec;
	    what = hyang_checkActivityEx(wt, 0, handleInterrupt);
#ifdef NEED_INT_HANDLER
            if (UsingReadline && caught_sigwinch) {
		caught_sigwinch = FALSE;
#ifdef HAVE_RL_RESIZE_TERMINAL
		rl_resize_terminal();
		static int oldwidth;
		int height, width;
		rl_get_screen_size(&height,&width);
		if (oldwidth >= 0 && oldwidth != width) {
		    static SEXP opsym = NULL;
		    if (! opsym)
			opsym = install("setWidthOnResize");
		    hyangboolean setOK = asLogical(GetOption1(opsym));
		    oldwidth = width;
		    if (setOK != NA_LOGICAL && setOK)
			hyang_SetOptWidth(width);
		}
#endif
            }
#endif


	    hyang_runHandlers(hyang_InputHandlers, what);
	    if (what == NULL)
		continue;
	    if (FD_ISSET(fileno(stdin), what)) {
#ifdef HAVE_LIBREADLINE
		if (UsingReadline) {
		    rl_callback_read_char();
		    if(rl_data.readline_eof || rl_data.readline_gotaline) {
			rl_top = rl_data.prev;
			return(rl_data.readline_eof ? 0 : 1);
		    }
		}
		else
#endif
		{
		    if(fgets((char *)buf, len, stdin) == NULL)
			return 0;
		    else
			return 1;
		}
	    }
	}
    }
}

void attribute_hidden hyangStd_WriteConsole(const char *buf, int len)
{
    printf("%s", buf);
    fflush(stdout);
}

void attribute_hidden hyangStd_WriteConsoleEx(const char *buf, int len, int otype)
{
    if (otype)
      printf("\033[1m%s\033[0m", buf);
    else
      printf("%s", buf);
    fflush(stdout);
}

void attribute_hidden hyangStd_ResetConsole()
{
}

void attribute_hidden hyangStd_FlushConsole()
{
}

void attribute_hidden hyangStd_ClearerrConsole()
{
    clearerr(stdin);
}

void attribute_hidden hyangStd_Busy(int which)
{
}

void hyang_CleanTempDir(void)
{
    char buf[1024];

    if((Sys_TempDir)) {
#if defined(__sun) || defined(sun)
	chdir(hyang_HomeDir());
#endif
	snprintf(buf, 1024, "rm -Rf %s", Sys_TempDir);
	buf[1023] = '\0';
	hyang_system(buf);
    }
}


void attribute_hidden NORET hyangStd_CleanUp(SA_TYPE saveact, int status, int runLast)
{
    if(saveact == SA_DEFAULT)
	saveact = SaveAction;

    if(saveact == SA_SAVEASK) {
	if(hyang_Interactive) {
	    unsigned char buf[1024];
	qask:

	    hyang_ClearerrConsole();
	    hyang_FlushConsole();
	    int res = hyang_ReadConsole("Save workspace image? [y/n/c]: ",
				    buf, 128, 0);
	    if(res) {
		switch (buf[0]) {
		case 'y':
		case 'Y':
		    saveact = SA_SAVE;
		    break;
		case 'n':
		case 'N':
		    saveact = SA_NOSAVE;
		    break;
		case 'c':
		case 'C':
		    jump_to_toplevel();
		    break;
		default:
		    goto qask;
		}
	    } else saveact = SA_NOSAVE;
	} else
	    saveact = SaveAction;
    }
    switch (saveact) {
    case SA_SAVE:
	if(runLast) hyang_dotLast();
	if(hyang_DirtyImage) hyang_SaveGlobalEnv();
#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY_H)
	if(hyang_Interactive && UsingReadline) {
	    int err;
	    hyang_setupHistory();
	    stifle_history(hyang_HistorySize);
	    err = write_history(hyang_HistoryFile);
	    if(err) warning(_("problem in saving the history file '%s'"),
			    hyang_HistoryFile);
	}
#endif
	break;
    case SA_NOSAVE:
	if(runLast) hyang_dotLast();
	break;
    case SA_SUICIDE:
    default:
	break;
    }
    hyang_RunExitFinalizers();
    CleanEd();
    if(saveact != SA_SUICIDE) KillAllDevices();
    hyang_CleanTempDir();
    if(saveact != SA_SUICIDE && hyang_CollectWarnings)
	PrintWarnings();
    if(ifp) {
	fclose(ifp);
	ifp = NULL;
    }
    fpu_setup(FALSE);

    exit(status);
}

# include <errno.h>

int attribute_hidden
hyangStd_ShowFiles(int nfile,
	       const char **file,
	       const char **headers,
	       const char *wtitle,
	       hyangboolean del,
	       const char *pager)

{
    int c, i, res;
    char *filename;
    FILE *fp, *tfp;
    char buf[1024];

    if (nfile > 0) {
	if (pager == NULL || strlen(pager) == 0) pager = "more";
	filename = hyang_tmpnam(NULL, hyang_TempDir);
	if ((tfp = hyang_fopen(filename, "w")) != NULL) {
	    for(i = 0; i < nfile; i++) {
		if (headers[i] && *headers[i])
		    fprintf(tfp, "%s\n\n", headers[i]);
		errno = 0;
		if ((fp = hyang_fopen(hyang_ExpandFileName(file[i]), "r"))
		    != NULL) {
		    while ((c = fgetc(fp)) != EOF)
			fputc(c, tfp);
		    fprintf(tfp, "\n");
		    fclose(fp);
		    if(del)
			unlink(hyang_ExpandFileName(file[i]));
		}
		else
		    fprintf(tfp, _("Cannot open file '%s': %s\n\n"),
			    file[i], strerror(errno));
	    }
	    fclose(tfp);
	}
	snprintf(buf, 1024, "'%s' < '%s'", pager, filename);
	res = hyang_system(buf);
	if (res == 127)
	    warningcall(hyang_AbsurdValue, _("error in running command"));
	unlink(filename);
	free(filename);
	return (res != 0);
    }
    return 1;
}

int attribute_hidden hyangStd_ChooseFile(int _new, char *buf, int len)
{
    size_t namelen;
    char *bufp;
    hyang_ReadConsole("Enter file name: ", (unsigned char *)buf, len, 0);
    namelen = strlen(buf);
    bufp = &buf[namelen - 1];
    while (bufp >= buf && isspace((int)*bufp))
	*bufp-- = '\0';
    return (int) strlen(buf);
}


void attribute_hidden hyangStd_ShowMessage(const char *s)
{
    REprintf("%s\n", s);
}


void attribute_hidden hyangStd_read_history(const char *s)
{
#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY_H)
    if(hyang_Interactive && UsingReadline) {
	read_history(s);
    }
#endif
}

void attribute_hidden hyangStd_loadhistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP sfile;
    char file[PATH_MAX];
    const char *p;

    sfile = CAR(args);
    if (!isString(sfile) || LENGTH(sfile) < 1)
	errorcall(call, _("invalid '%s' argument"), "file");
    p = hyang_ExpandFileName(translateChar(STRING_ELT(sfile, 0)));
    if(strlen(p) > PATH_MAX - 1)
	errorcall(call, _("'file' argument is too long"));
    strcpy(file, p);
#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY_H)
    if(hyang_Interactive && UsingReadline) {
	clear_history();
	read_history(file);
    } else errorcall(call, _("no history mechanism available"));
#else
    errorcall(call, _("no history mechanism available"));
#endif
}

void attribute_hidden hyangStd_savehistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP sfile;
    char file[PATH_MAX];
    const char *p;

    sfile = CAR(args);
    if (!isString(sfile) || LENGTH(sfile) < 1)
	errorcall(call, _("invalid '%s' argument"), "file");
    p = hyang_ExpandFileName(translateChar(STRING_ELT(sfile, 0)));
    if(strlen(p) > PATH_MAX - 1)
	errorcall(call, _("'file' argument is too long"));
    strcpy(file, p);
#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY_H)
    if(hyang_Interactive && UsingReadline) {
	int err;
	err = write_history(file);
	if(err) error(_("problem in saving the history file '%s'"), file);
#ifdef HAVE_HISTORY_TRUNCATE_FILE
	hyang_setupHistory();
	err = history_truncate_file(file, hyang_HistorySize);
	if(err) warning(_("problem in truncating the history file"));
#endif
    } else errorcall(call, _("no history available to save"));
#else
    errorcall(call, _("no history available to save"));
#endif
}

void attribute_hidden hyangStd_addhistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP stamp;
    int i;

    checkArity(op, args);
    stamp = CAR(args);
    if (!isString(stamp))
	errorcall(call, _("invalid timestamp"));
#if defined(HAVE_LIBREADLINE) && defined(HAVE_READLINE_HISTORY_H)
    if(hyang_Interactive && UsingReadline)
	for (i = 0; i < LENGTH(stamp); i++)
	    add_history(CHAR(STRING_ELT(stamp, i)));
# endif
}


#define HYANG_MIN(a, b) ((a) < (b) ? (a) : (b))

void hyangsleep(double timeint)
{
    double tm = timeint * 1e6, start = currentTime(), elapsed;
    for (;;) {
	fd_set *what;
	tm = HYANG_MIN(tm, 2e9);
	int wt = -1;
	if (hyang_waitUsec > 0) wt = hyang_waitUsec;
	if (hyang_gWaitUsec > 0 && (wt < 0 || wt > hyang_gWaitUsec))
	    wt = hyang_gWaitUsec;
	int Timeout = (int) (wt > 0 ? HYANG_MIN(tm, wt) : tm);
	what = hyang_checkActivity(Timeout, 1);
	hyang_CheckUsrInterrupt();
	elapsed = currentTime() - start;
	if(elapsed >= timeint) break;

	hyang_runHandlers(hyang_InputHandlers, what);

	elapsed = currentTime() - start;
	if(elapsed >= timeint) break;

	tm = 1e6*(timeint - elapsed);
    }
}
