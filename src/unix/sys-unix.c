/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#define HYANG_USE_SIGNALS 1
#include <hyangdefn.h>
#include <hyangintl.h>
#include <hyangfio.h>
#include <hyangmath.h>
#include "hyangunix.h"

#ifdef HAVE_UNISTD_H
# include <unistd.h>
#endif

#ifndef HAVE_GETRUSAGE
# ifdef HAVE_SYS_TIME_H
#  include <sys/times.h>
# endif
#endif

#ifdef HAVE_FCNTL_H
# include <fcntl.h>
#endif

#if defined(HAVE_SYS_RESOURCE_H) && defined(HAVE_GETRUSAGE)
# include <sys/time.h>
# include <sys/resource.h>
#endif

#include <errno.h>

attribute_hidden
FILE *hyang_OpenInitFile(void)
{
    char buf[PATH_MAX], *home, *p = getenv("HYANG_PROFILE_USER");
    FILE *fp;

    fp = NULL;
    if (LoadInitFile) {
	if(p) {
	    if(!*p) return NULL;
	    return hyang_fopen(hyang_ExpandFileName(p), "r");
	}
	if((fp = hyang_fopen(".hyangprofile", "r")))
	    return fp;
	if((home = getenv("HOME")) == NULL)
	    return NULL;
	snprintf(buf, PATH_MAX, "%s/.hyangprofile", home);
	if((fp = hyang_fopen(buf, "r")))
	    return fp;
    }
    return fp;
}

char *hyang_ExpandFileName_readline(const char *s, char *buff);

static char newFileName[PATH_MAX];
static int HaveHOME=-1;
static char UserHOME[PATH_MAX];

static const char *hyang_ExpandFileName_unix(const char *s, char *buff)
{
    char *p;

    if(s[0] != '~') return s;
    if(strlen(s) > 1 && s[1] != '/') return s;
    if(HaveHOME < 0) {
	p = getenv("HOME");
	if(p && *p && (strlen(p) < PATH_MAX)) {
	    strcpy(UserHOME, p);
	    HaveHOME = 1;
	} else
	    HaveHOME = 0;
    }
    if(HaveHOME > 0 && (strlen(UserHOME) + strlen(s+1) < PATH_MAX)) {
	strcpy(buff, UserHOME);
	strcat(buff, s+1);
	return buff;
    } else return s;
}


extern hyangboolean UsingReadline;

const char *hyang_ExpandFileName(const char *s)
{
#ifdef HAVE_LIBREADLINE
    if(UsingReadline) {
	const char * c = hyang_ExpandFileName_readline(s, newFileName);
	if (!c || c[0]!='~' || (c[1]!='\0' && c[1]!='/'))
	    return c;
    }
#endif
    return hyang_ExpandFileName_unix(s, newFileName);
}

SEXP attribute_hidden do_machine(SEXP call, SEXP op, SEXP args, SEXP env)
{
    checkArity(op, args);
    return mkString("Unix");
}

# ifdef HAVE_SYS_TIMES_H
#  include <sys/times.h>
# endif

static double clk_tck, StartTime;

void hyang_setStartTime(void)
{
#ifdef HAVE_SYSCONF
    clk_tck = (double) sysconf(_SC_CLK_TCK);
#else
# ifndef CLK_TCK
#  ifdef HZ
#   define CLK_TCK HZ
#  else
#   define CLK_TCK 60
#  endif
# endif
    clk_tck = (double) CLK_TCK;
#endif
    StartTime = currentTime();
}

attribute_hidden
void hyang_getProcTime(double *data)
{
    double et = currentTime() - StartTime;
    data[2] = 1e-3 * rint(1000*et);
#ifdef HAVE_GETRUSAGE
    struct rusage self, children;
    getrusage(RUSAGE_SELF, &self);
    getrusage(RUSAGE_CHILDREN, &children);
    data[0] = (double) self.ru_utime.tv_sec +
	1e-3 * (self.ru_utime.tv_usec/1000);
    data[1] = (double) self.ru_stime.tv_sec +
	1e-3 * (self.ru_stime.tv_usec/1000);
    data[3] = (double) children.ru_utime.tv_sec +
	1e-3 * (children.ru_utime.tv_usec/1000);
    data[4] = (double) children.ru_stime.tv_sec +
	1e-3 * (children.ru_stime.tv_usec/1000);
#else
    struct tms timeinfo;
    times(&timeinfo);
    data[0] = fround(timeinfo.tms_utime / clk_tck, 3);
    data[1] = fround(timeinfo.tms_stime / clk_tck, 3);
    data[3] = fround(timeinfo.tms_cutime / clk_tck, 3);
    data[4] = fround(timeinfo.tms_cstime / clk_tck, 3);
#endif
}

attribute_hidden
double hyang_getClockIncrement(void)
{
    return 1.0 / clk_tck;
}


#ifdef HAVE_SYS_WAIT_H
# include <sys/wait.h>
#endif

#define KILL_SIGNAL1 SIGINT
#define KILL_SIGNAL2 SIGTERM
#define KILL_SIGNAL3 SIGKILL
#define EMERGENCY_TIMEOUT 20

int kill_signals[] = { KILL_SIGNAL1, KILL_SIGNAL2, KILL_SIGNAL3 };
static struct {
    pid_t child_pid;
    int timedout;
    int kill_attempts;
    sigset_t oldset;
    struct sigaction oldalrm, oldint, oldquit, oldhup, oldterm, oldttin,
                     oldttou, oldchld;
    HYANGCTX cntxt;
    FILE *fp;
} tost;

static void timeout_handler(int sig);
static void timeout_init()
{
    tost.child_pid = 0;
    tost.timedout = 0;
    tost.kill_attempts = 0;
    sigprocmask(0, NULL, &tost.oldset);
    sigaction(SIGALRM, NULL, &tost.oldalrm);
    sigaction(SIGINT, NULL, &tost.oldint);
    sigaction(SIGQUIT, NULL, &tost.oldquit);
    sigaction(SIGHUP, NULL, &tost.oldhup);
    sigaction(SIGTERM, NULL, &tost.oldterm);
    sigaction(SIGTTIN, NULL, &tost.oldttin);
    sigaction(SIGTTOU, NULL, &tost.oldttou);
    sigaction(SIGCHLD, NULL, &tost.oldchld);
    tost.fp = NULL;

    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_handler = &timeout_handler;
    sa.sa_flags = SA_RESTART;
    sigaction(SIGALRM, &sa, NULL);
    sigaction(SIGINT, &sa, NULL);
    sigaction(SIGQUIT, &sa, NULL);
    sigaction(SIGHUP, &sa, NULL);
    sigaction(SIGTERM, &sa, NULL);
    sigaction(SIGCHLD, &sa, NULL);
}

static void timeout_cleanup_set(sigset_t *ss)
{
    sigemptyset(ss);
    sigaddset(ss, SIGALRM);
    sigaddset(ss, SIGINT);
    sigaddset(ss, SIGQUIT);
    sigaddset(ss, SIGHUP);
    sigaddset(ss, SIGTERM);
    sigaddset(ss, SIGTTIN);
    sigaddset(ss, SIGTTOU);
    sigaddset(ss, SIGCHLD);
}

static void timeout_cleanup()
{
    sigset_t ss;
    timeout_cleanup_set(&ss);
    sigprocmask(SIG_BLOCK, &ss, NULL);
    alarm(0);

    sigaction(SIGALRM, &tost.oldalrm, NULL);
    sigaction(SIGINT, &tost.oldint, NULL);
    sigaction(SIGQUIT, &tost.oldquit, NULL);
    sigaction(SIGHUP, &tost.oldhup, NULL);
    sigaction(SIGTERM, &tost.oldterm, NULL);
    sigaction(SIGTTIN, &tost.oldttin, NULL);
    sigaction(SIGTTOU, &tost.oldttou, NULL);
    sigaction(SIGCHLD, &tost.oldchld, NULL);

    sigprocmask(SIG_SETMASK, &tost.oldset, NULL);
}

static void timeout_handler(int sig)
{
    if (sig == SIGCHLD)
	return;
    if (tost.child_pid > 0 && sig == SIGALRM) {
	tost.timedout = 1;
	if (tost.kill_attempts < 3) {
	    sig = kill_signals[tost.kill_attempts];
	    if (tost.kill_attempts < 2) {
		int saveerrno = errno;
		alarm(EMERGENCY_TIMEOUT);
		errno = saveerrno;
	    }
	    tost.kill_attempts++;
	} else
	    sig = KILL_SIGNAL1;
    }
    if (tost.child_pid > 0) {

	kill(tost.child_pid, sig);
	int saveerrno = errno;
	killpg(tost.child_pid, sig);
	errno = saveerrno;
	if (sig != SIGKILL && sig != SIGCONT) {
	    kill(tost.child_pid, SIGCONT);
	    saveerrno = errno;
	    killpg(tost.child_pid, SIGCONT);
	    errno = saveerrno;
	}
    } else if (tost.child_pid == 0) {
	_exit(128 + sig);
    }
}

static pid_t timeout_wait(int *wstatus)
{
    pid_t wres;

    sigset_t ss;
    timeout_cleanup_set(&ss);
    sigset_t unblocked_ss;
    sigprocmask(SIG_BLOCK, &ss, &unblocked_ss);

    int saveerrno = errno;
    while((wres = waitpid(tost.child_pid, wstatus, WNOHANG)) == 0)
	sigsuspend(&unblocked_ss);

    if (errno == EINTR)
	errno = saveerrno;
    if (wres == tost.child_pid)
	tost.child_pid = -1;
    timeout_cleanup();
    return wres;
}

static void timeout_cend(void *data)
{
    if (tost.child_pid > 0) {
	timeout_handler(SIGALRM);
	timeout_wait(NULL);
    }
    timeout_cleanup();
}

static void timeout_fork()
{
    sigset_t css;
    sigemptyset(&css);
    sigaddset(&css, SIGCHLD);
    sigprocmask(SIG_BLOCK, &css, NULL);
    tost.child_pid = fork();
    sigprocmask(SIG_UNBLOCK, &css, NULL);
}

static FILE *hyang_popen_timeout(const char *cmd, const char *type, int timeout)
{
    if (!type || type[1] ||  (type[0] != 'r' && type[0] != 'w')) {
	errno = EINVAL;
	return NULL;
    }
    int doread = (type[0] == 'r');
    int pipefd[2];
    int parent_end, child_end;
    if (pipe(pipefd) < 0)
	return NULL;
    if (doread) {
	parent_end = pipefd[0];
	child_end = pipefd[1];
    } else {
	parent_end = pipefd[1];
	child_end = pipefd[0];
    }

    timeout_init();

    begincontext(&tost.cntxt, CTXT_CCODE, hyang_AbsurdValue, hyang_BaseEnv, hyang_BaseEnv,
                 hyang_AbsurdValue, hyang_AbsurdValue);
    tost.cntxt.cenddata = NULL;
    tost.cntxt.cend = &timeout_cend;

    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    timeout_fork();

    if (tost.child_pid == 0) {
	setpgid(0, 0);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);
	dup2(child_end, doread ? 1 : 0);
	close(child_end);
	close(parent_end);
	close(doread ? 0 : 1);
	if (open("/dev/null", O_RDONLY) < 0) {
	    perror("Cannot open /dev/null for reading:");
	    _exit(127);
	}
	execl("/bin/sh", "sh", "-c", cmd, (char *)NULL);
	_exit(127);
    } else if (tost.child_pid > 0) {
	close(child_end);
        tost.fp = fdopen(parent_end, type);
	if (!tost.fp) {
	    close(parent_end);
	    return NULL;
	}

	sigset_t ss;
	sigemptyset(&ss);
	sigaddset(&ss, SIGALRM);
	sigprocmask(SIG_UNBLOCK, &ss, NULL);
	alarm(timeout);

	return tost.fp;
    } else {
	close(parent_end);
	return NULL;
    }
}

int hyang_pclose_timeout(FILE *fp)
{
    if (fp != tost.fp)
	error("Invalid file pointer in pclose");

    int fd = fileno(fp);
    if (fd >= 0)
	close(fd);

    pid_t wres;
    int wstatus;

    wres = timeout_wait(&wstatus);
    endcontext(&tost.cntxt);

    if (wres < 0)
	return -1;
    return wstatus;
}

static int hyang_system_timeout(const char *cmd, int timeout)
{
    if (!cmd)
	return hyang_system(cmd);


    timeout_init();
    signal(SIGTTIN, SIG_IGN);
    signal(SIGTTOU, SIG_IGN);
    timeout_fork();

    if (tost.child_pid == 0) {
	close(0);
	if (open("/dev/null", O_RDONLY) < 0) {
	    perror("Cannot open /dev/null for reading:");
	    _exit(127);
	}
	setpgid(0, 0);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGTTOU, SIG_DFL);

	execl("/bin/sh", "sh", "-c", cmd, (char *)NULL);
	_exit(127);
    } else if (tost.child_pid > 0) {
	sigset_t ss;
	sigemptyset(&ss);
	sigaddset(&ss, SIGALRM);
	sigprocmask(SIG_UNBLOCK, &ss, NULL);
	alarm(timeout);

	int wstatus;
	timeout_wait(&wstatus);
	if (tost.child_pid != -1)
	    return -1;
#ifdef HAVE_SYS_WAIT_H
	if (WIFEXITED(wstatus)) wstatus = WEXITSTATUS(wstatus);
#else
	if ((wstatus % 256) == 0) wstatus = wstatus/256;
#endif
        if (wstatus == -1) {
	    warning(_("system call failed: %s"), strerror(errno));
	    wstatus = 127;
	}
	return wstatus;
    } else
	return -1;
}

static void warn_status(const char *cmd, int res)
{
    if (!res)
	return;

    if (errno)
	warning(_("running command '%s' had status %d and error message '%s'"),
		cmd, res, strerror(errno));
    else
	warning(_("running command '%s' had status %d"), cmd, res);
}

#define INTERN_BUFSIZE 8096
SEXP attribute_hidden do_system(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    SEXP tlist = hyang_AbsurdValue;
    int intern = 0;
    int timeout = 0;

    checkArity(op, args);
    if (!isValidStringF(CAR(args)))
	error(_("non-empty character argument expected"));
    intern = asLogical(CADR(args));
    if (intern == NA_INTEGER)
	error(_("'intern' must be logical and not NA"));
    timeout = asInteger(CADDR(args));
    if (timeout == NA_INTEGER || timeout < 0)
	error(_("invalid '%s' argument"), "timeout");
    const char *cmd = translateChar(STRING_ELT(CAR(args), 0));
    if (timeout > 0) {
	const void *vmax = vmaxget();
	const char *c = translateCharUTF8(STRING_ELT(CAR(args), 0));
	int last_is_amp = 0;
	int len = 0;
	for(;*c; c += len) {
	    len = utf8clen(*c);
	    if (len == 1) {
		if (*c == '&')
		    last_is_amp = 1;
		else if (*c != ' ' && *c != '\t' && *c != '\r' && *c != '\n')
		    last_is_amp = 0;
	    } else
		last_is_amp = 0;
	}
	if (last_is_amp)
	    error("Timeout with background running processes is not supported.");
	vmaxset(vmax);
    }
    if (intern) {
	FILE *fp;
	char *x = "r",
#ifdef HAVE_GETLINE
	    *buf = NULL;
	size_t buf_len = 0;
#else
	    buf[INTERN_BUFSIZE];
#endif
	int i, j, res;
	SEXP tchar, rval;

	PROTECT(tlist);
	errno = 0;
	if (timeout == 0)
	    fp = hyang_popen(cmd, x);
	else
	    fp = hyang_popen_timeout(cmd, x, timeout);
	if(!fp)
	    error(_("cannot popen '%s', probable reason '%s'"),
		  cmd, strerror(errno));
#ifdef HAVE_GETLINE
        size_t read;
        for(i = 0; (read = getline(&buf, &buf_len, fp)) != (size_t)-1; i++) {
	    if (buf[read - 1] == '\n')
#else
	for (i = 0; fgets(buf, INTERN_BUFSIZE, fp); i++) {
	    size_t read = strlen(buf);
	    if(read >= INTERN_BUFSIZE - 1)
		warning(_("line %d may be truncated in call to system(, intern = TRUE)"), i + 1);
	    if (read > 0 && buf[read-1] == '\n')
#endif
		buf[read - 1] = '\0';
	    tchar = mkChar(buf);
	    UNPROTECT(1);
	    PROTECT(tlist = CONS(tchar, tlist));
	}
#ifdef HAVE_GETLINE
        if (buf != NULL)
          free(buf);
#endif
	if (timeout == 0)
	    res = pclose(fp);
	else
	    res = hyang_pclose_timeout(fp);

#ifdef HAVE_SYS_WAIT_H
	if (WIFEXITED(res)) res = WEXITSTATUS(res);
	else res = 0;
#else
	if ((res % 256) == 0) res = res/256;
#endif
	if ((res & 0xff)  == 127) {
	    if (errno)
		error(_("error in running command: '%s'"), strerror(errno));
	    else
		error(_("error in running command"));
	}

	if (timeout && tost.timedout) {
	    res = 124;
	    warning(_("command '%s' timed out after %ds"), cmd, timeout);
	} else
	    warn_status(cmd, res);

	rval = PROTECT(allocVector(STRSXP, i));
	for (j = (i - 1); j >= 0; j--) {
	    SET_STRING_ELT(rval, j, CAR(tlist));
	    tlist = CDR(tlist);
	}
	if(res) {
	    SEXP lsym = install("status");
	    setAttrib(rval, lsym, ScalarInteger(res));
	    if(errno) {
		lsym = install("errmsg");
		setAttrib(rval, lsym, mkString(strerror(errno)));
	    }
	}
	UNPROTECT(2);
	return rval;
    }
    else {
#ifdef HAVE_AQUA
	hyang_Busy(1);
#endif
	tlist = PROTECT(allocVector(INTSXP, 1));
	fflush(stdout);
	int res;
	if (timeout == 0)
	    res = hyang_system(cmd);
	else
	    res = hyang_system_timeout(cmd, timeout);
	if (res == 127)
	    warning(_("error in running command"));
	if (timeout && tost.timedout) {
	    res = 124;
	    warning(_("command '%s' timed out after %ds"), cmd, timeout);
	}
	INTEGER(tlist)[0] = res;
#ifdef HAVE_AQUA
	hyang_Busy(0);
#endif
	UNPROTECT(1);
	hyang_Visible = 0;
	return tlist;
    }
}

#ifdef HAVE_SYS_UTSNAME_H
# include <sys/utsname.h>

# ifdef HAVE_UNISTD_H
#  include <unistd.h>
# endif

# ifdef HAVE_PWD_H
#  include <pwd.h>
# endif

SEXP attribute_hidden do_sysinfo(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    SEXP ans, ansnames;
    struct utsname name;
    char *login;

    checkArity(op, args);
    PROTECT(ans = allocVector(STRSXP, 8));
    if(uname(&name) == -1) {
	UNPROTECT(1);
	return hyang_AbsurdValue;
    }
    SET_STRING_ELT(ans, 0, mkChar(name.sysname));
    SET_STRING_ELT(ans, 1, mkChar(name.release));
    SET_STRING_ELT(ans, 2, mkChar(name.version));
    SET_STRING_ELT(ans, 3, mkChar(name.nodename));
    SET_STRING_ELT(ans, 4, mkChar(name.machine));
    login = getlogin();
    SET_STRING_ELT(ans, 5, login ? mkChar(login) : mkChar("unknown"));
#if defined(HAVE_PWD_H) && defined(HAVE_GETPWUID) && defined(HAVE_GETUID)
    {
	struct passwd *stpwd;
	stpwd = getpwuid(getuid());
	SET_STRING_ELT(ans, 6, stpwd ? mkChar(stpwd->pw_name) : mkChar("unknown"));
    }
#else
    SET_STRING_ELT(ans, 6, mkChar("unknown"));
#endif
#if defined(HAVE_PWD_H) && defined(HAVE_GETPWUID) && defined(HAVE_GETEUID)
    {
	struct passwd *stpwd;
	stpwd = getpwuid(geteuid());
	SET_STRING_ELT(ans, 7, stpwd ? mkChar(stpwd->pw_name) : mkChar("unknown"));
    }
#else
    SET_STRING_ELT(ans, 7, mkChar("unknown"));
#endif
    PROTECT(ansnames = allocVector(STRSXP, 8));
    SET_STRING_ELT(ansnames, 0, mkChar("sysname"));
    SET_STRING_ELT(ansnames, 1, mkChar("release"));
    SET_STRING_ELT(ansnames, 2, mkChar("version"));
    SET_STRING_ELT(ansnames, 3, mkChar("nodename"));
    SET_STRING_ELT(ansnames, 4, mkChar("machine"));
    SET_STRING_ELT(ansnames, 5, mkChar("login"));
    SET_STRING_ELT(ansnames, 6, mkChar("user"));
    SET_STRING_ELT(ansnames, 7, mkChar("effective_user"));
    setAttrib(ans, hyang_NamesSym, ansnames);
    UNPROTECT(2);
    return ans;
}
#else
SEXP do_sysinfo(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    warning(_("Sys.info() is not implemented on this system"));
    return hyang_AbsurdValue;
}
#endif

#include <hyangexts/hyangloop.h>
#include <hyangexts/hyangdyl.h>
DL_FUNC hyang_ptrProcessEvents;
void hyang_ProcessEvents(void)
{
#ifdef HAVE_AQUA
    if (hyang_ptrProcessEvents && !hyang_isForkedChild) hyang_ptrProcessEvents();
#else
    if (hyang_ptrProcessEvents) hyang_ptrProcessEvents();
#endif
    hyang_PolledEvents();
    if (cpuLimit > 0.0 || elapsedLimit > 0.0) {
	double cpu, data[5];
	hyang_getProcTime(data);
	cpu = data[0] + data[1] + data[3] + data[4];
	if (elapsedLimit > 0.0 && data[2] > elapsedLimit) {
	    cpuLimit = elapsedLimit = -1;
	    if (elapsedLimit2 > 0.0 && data[2] > elapsedLimit2) {
		elapsedLimit2 = -1.0;
		error(_("reached session elapsed time limit"));
	    } else
		error(_("reached elapsed time limit"));
	}
	if (cpuLimit > 0.0 && cpu > cpuLimit) {
	    cpuLimit = elapsedLimit = -1;
	    if (cpuLimit2 > 0.0 && cpu > cpuLimit2) {
		cpuLimit2 = -1.0;
		error(_("reached session CPU time limit"));
	    } else
		error(_("reached CPU time limit"));
	}
    }
}

#ifdef __FreeBSD__
# ifdef HAVE_FLOATINGPOINT_H
#  include <floatingpoint.h>
# endif
#endif

#if (defined(__i386) || defined(__x86_64)) && defined(__INTEL_COMPILER) && __INTEL_COMPILER > 800
#include <xmmintrin.h>
#include <pmmintrin.h>
#endif

void fpu_setup(hyangboolean start)
{
    if (start) {
#ifdef __FreeBSD__
    fpsetmask(0);
#endif

#if (defined(__i386) || defined(__x86_64)) && defined(__INTEL_COMPILER) && __INTEL_COMPILER > 800
    _MM_SET_FLUSH_ZERO_MODE(_MM_FLUSH_ZERO_OFF);
    _MM_SET_DENORMALS_ZERO_MODE(_MM_DENORMALS_ZERO_OFF);
#endif
    } else {
#ifdef __FreeBSD__
    fpsetmask(~0);
#endif
    }
}
