/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangintl.h>

#include <hyangconn.h>
#include <hyangdyp.h>

#ifdef HAVE_X11

#include <hyangextslib/hyangmodX11.h>

static hyang_X11Routines routines, *ptr = &routines;

static int initialized = 0;

hyang_X11Routines * hyang_setX11Routines(hyang_X11Routines *routines)
{
    hyang_X11Routines *tmp;
    tmp = ptr;
    ptr = routines;
    return tmp;
}

int attribute_hidden hyang_X11_Init(void)
{
    int res;

    if(initialized) return initialized;

    initialized = -1;
    if(strcmp(hyang_GUIType, "none") == 0) {
	warning(_("X11 module is not available under this GUI"));
	return initialized;
    }
    res = hyang_moduleCdyl("HYANG_X11", 1, 1);
    if(!res) return initialized;
    if(!ptr->access)
	error(_("X11 routines cannot be accessed in module"));
    initialized = 1;
    return initialized;
}

hyangboolean attribute_hidden hyang_access_X11(void)
{
    hyang_X11_Init();
    return (initialized > 0) ? (*ptr->access)() > 0 : FALSE;
}

SEXP do_X11(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_X11_Init();
    if(initialized > 0)
	return (*ptr->X11)(call, op, args, rho);
    else {
	error(_("X11 module cannot be loaded"));
	return hyang_AbsurdValue;
    }
}

SEXP do_saveplot(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_X11_Init();
    if(initialized > 0)
	return (*ptr->saveplot)(call, op, args, rho);
    else {
	error(_("X11 module cannot be loaded"));
	return hyang_AbsurdValue;
    }
}

hyangboolean hyang_GetX11Image(int d, void *pximage, int *pwidth, int *pheight)
{
    hyang_X11_Init();
    if(initialized > 0)
	return (*ptr->image)(d, pximage, pwidth, pheight);
    else {
	error(_("X11 module cannot be loaded"));
	return FALSE;
    }
}

hyangboolean attribute_hidden hyang_ReadClipboard(hyangclpconn clpcon, char *type)
{
    hyang_X11_Init();
    if(initialized > 0)
	return (*ptr->readclp)(clpcon, type);
    else {
	error(_("X11 module cannot be loaded"));
	return FALSE;
    }
}

SEXP do_bmVersion(void)
{
   SEXP ans = PROTECT(allocVector(STRSXP, 3)),
	nms = PROTECT(allocVector(STRSXP, 3));
    setAttrib(ans, hyang_NamesSym, nms);
    SET_STRING_ELT(nms, 0, mkChar("libpng"));
    SET_STRING_ELT(nms, 1, mkChar("jpeg"));
    SET_STRING_ELT(nms, 2, mkChar("libtiff"));
    hyang_X11_Init();
    if(initialized > 0) {
	SET_STRING_ELT(ans, 0, mkChar((*ptr->hyang_pngVersion)()));
	SET_STRING_ELT(ans, 1, mkChar((*ptr->hyang_jpegVersion)()));
	SET_STRING_ELT(ans, 2, mkChar((*ptr->hyang_tiffVersion)()));
    }
    UNPROTECT(2);
    return ans;
}
#else

hyangboolean attribute_hidden hyang_access_X11(void)
{
    return FALSE;
}

SEXP do_X11(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    error(_("X11 is not available"));
    return hyang_AbsurdValue;
}

SEXP do_saveplot(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    error(_("X11 is not available"));
    return hyang_AbsurdValue;
}

hyangboolean hyang_GetX11Image(int d, void *pximage, int *pwidth, int *pheight)
{
    error(_("X11 is not available"));
    return FALSE;
}

hyangboolean attribute_hidden hyang_ReadClipboard(hyangclpconn con, char *type)
{
    error(_("X11 is not available"));
    return FALSE;
}

SEXP do_bmVersion(void)
{
    SEXP ans = PROTECT(allocVector(STRSXP, 3)),
	nms = PROTECT(allocVector(STRSXP, 3));
    setAttrib(ans, hyang_NamesSym, nms);
    SET_STRING_ELT(nms, 0, mkChar("libpng"));
    SET_STRING_ELT(nms, 1, mkChar("jpeg"));
    SET_STRING_ELT(nms, 2, mkChar("libtiff"));
    UNPROTECT(2);
    return ans;
}
#endif
