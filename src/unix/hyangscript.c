/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Used in Hyang scripts like:
 *
 * #!/path/to/hyangscript --vanilla
 * commandArgs(TRUE)
 * q(status=7)
 *
 * This invokes Hyang with a command line like
 * hyang --slave --no-restore --vanilla --file=foo [script_args]
 *
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#ifdef _WIN32
#include <psignal.h>
#endif

#include <stdio.h>
#include <limits.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

#include <hyangversion.h>

#if !defined(PATH_MAX)
# if defined(HAVE_SYS_PARAM_H)
#  include <sys/param.h>
# endif
# if !defined(PATH_MAX)
#  if defined(MAXPATHLEN)
#    define PATH_MAX MAXPATHLEN
#  elif defined(Win32)
#    define PATH_MAX 260
#  else
#    define PATH_MAX 5000
#  endif
# endif
#endif

#ifndef _WIN32
#ifndef HYANG_ARCH
#define HYANG_ARCH ""
#endif

static char hyanghome[] = HYANG_HOME;
static char hyangarch[] = HYANG_ARCH;
#else
# ifndef BINDIR
#  define BINDIR "bin"
# endif
# define FOR_hyangscript
# include "hyangterm.c"
#endif

#ifdef HAVE_EXECV
static int verbose = 0;
#endif

void usage(void)
{
    fprintf(stderr, "Usage: /path/to/hyangscript [--options] [-e expr [-e expr2 ...] | file] [args]\n\n");
    fprintf(stderr, "--options accepted are\n");
    fprintf(stderr, "  --help              Print usage and exit\n");
    fprintf(stderr, "  --version           Print version and exit\n");
    fprintf(stderr, "  --verbose           Print information on progress\n");
    fprintf(stderr, "  --default-packages=list\n");
    fprintf(stderr, "                      Where 'list' is a comma-separated set\n");
    fprintf(stderr, "                      of package names, or 'NULL'\n");
    fprintf(stderr, "or options to Hyang, in addition to --slave --no-restore, such as\n");
    fprintf(stderr, "  --save              Do save workspace at the end of the session\n");
    fprintf(stderr, "  --no-environ        Don't read the site and user environment files\n");
    fprintf(stderr, "  --no-site-file      Don't read the site-wide hyangprofile\n");
    fprintf(stderr, "  --no-init-file      Don't read the user Hyang profile\n");
    fprintf(stderr, "  --restore           Do restore previously saved objects at startup\n");
    fprintf(stderr, "  --vanilla           Combine --no-save, --no-restore, --no-site-file\n");
    fprintf(stderr, "                      --no-init-file and --no-environ\n");
    fprintf(stderr, "\n'file' may contain spaces but not shell metacharacters\n");
    fprintf(stderr, "Expressions (one or more '-e <expr>') may be used *instead* of 'file'\n");
    fprintf(stderr, "See also  ?hyangscript  from within Hyang\n");
}


int main(int argc_, char *argv_[])
{
#ifdef HAVE_EXECV
    char cmd[PATH_MAX+1], buf[PATH_MAX+8], buf2[1100], *p;
    int i, i0 = 0, ac = 0, res = 0, e_mode = 0, set_dp = 0;
    char **av;
    int have_cmdarg_default_packages = 0;

    if(argc_ <= 1) {
	usage();
	exit(1);
    }

    char *s = argv_[1];
    int njoined = 0;
    size_t j;
    if (strncmp(s, "--", 2) == 0)
	for(j = 0; s[j] != 0; j++)
	    if (s[j] != ' ' && s[j] != '\t' &&
		    (j == 0 || s[j-1] == ' ' || s[j-1] == '\t'))
		njoined++;

    int argc;
    char **argv;

    if (njoined > 1) {
	argc = argc_ - 1 + njoined;
	argv = (char **) malloc((size_t) (argc+1)*sizeof(char *));
	if (!argv) {
	    fprintf(stderr, "malloc failure\n");
	    exit(1);
	}
	argv[0] = argv_[0];

	size_t len = strlen(s);
	char *buf = (char *)malloc((size_t) (len+1)*sizeof(char *));
	if (!buf) {
	    fprintf(stderr, "malloc failure\n");
	    exit(1);
	}
	strcpy(buf, s);

	i = 1;
	for(j = 0; s[j] != 0; j++)
	    if (s[j] == ' ' || s[j] == '\t')
		buf[j] = 0;
	    else if (j == 0 || s[j-1] == ' ' || s[j-1] == '\t')
		argv[i++] = buf + j;

	for(i = 2; i < argc_; i++)
	    argv[i-1+njoined] = argv_[i];
	argv[argc] = 0;

    } else {
	argc = argc_;
	argv = argv_;
    }

    av = (char **) malloc((size_t) (argc+4)*sizeof(char *));
    if(!av) {
	fprintf(stderr, "malloc failure\n");
	exit(1);
    }

    p = getenv("HYANGHOME");
#ifdef _WIN32
    if(p && *p)
	snprintf(cmd, PATH_MAX+1, "%s\\%s\\HyangTerm.exe",  p, BINDIR);
    else {
	char hyanghome[MAX_PATH];
	GetModuleFileName(NULL, hyanghome, MAX_PATH);
	p = strrchr(hyanghome,'\\');
	if(!p) {fprintf(stderr, "installation problem\n"); exit(1);}
	*p = '\0';
	snprintf(cmd, PATH_MAX+1, "%s\\HyangTerm.exe",  hyanghome);
    }
#else
    if(!(p && *p)) p = hyanghome;
    if(strlen(p) + 6 > PATH_MAX) {
	fprintf(stderr, "impossibly long path for HYANGHOME\n");
	exit(1);
    }
    snprintf(cmd, PATH_MAX+1, "%s/bin/hyang", p);
#endif
    av[ac++] = cmd;
    av[ac++] = "--slave";
    av[ac++] = "--no-restore";

    if(argc == 2) {
	if(strcmp(argv[1], "--help") == 0) {
	    usage();
	    exit(0);
	}
	if(strcmp(argv[1], "--version") == 0) {
	    if(strlen(HYANG_STATUS) == 0)
		fprintf(stderr, "Hyang scripting front-end version %s.%s (%s-%s-%s)\n",
			HYANG_MAJOR, HYANG_MINOR, HYANG_YEAR, HYANG_MONTH, HYANG_DAY);
	    else
		fprintf(stderr, "Hyang scripting front-end version %s.%s %s (%s-%s-%s %s)\n",
			HYANG_MAJOR, HYANG_MINOR, HYANG_STATUS, HYANG_YEAR, HYANG_MONTH, HYANG_DAY,
                        HYANG_HYSCM_CHECKIN);
	    exit(0);
	}
    }

    for(i = 1; i < argc; i++) {
	if(strcmp(argv[i], "-e") == 0) {
	    e_mode = 1;
	    av[ac++] = argv[i];
	    if(!argv[++i]) {
		fprintf(stderr, "-e not followed by an expression\n");
		exit(1);
	    }
	    av[ac++] = argv[i];
	    i0 = i;
	    continue;
	}
	if(strncmp(argv[i], "--", 2) != 0) break;
	if(strcmp(argv[i], "--verbose") == 0) {
	    verbose = 1;
	    i0 = i;
	    continue;
	}
	if(strncmp(argv[i], "--default-packages=", 18) == 0) {
	    set_dp = 1;
	    if(strlen(argv[i]) > 1000) {
		fprintf(stderr, "unable to set HYANG_DEFAULT_PKGS\n");
		exit(1);
	    }
	    snprintf(buf2, 1100, "HYANG_DEFAULT_PKGS=%s", argv[i]+19);
	    if(verbose)
		fprintf(stderr, "setting '%s'\n", buf2);
#ifdef HAVE_PUTENV
	    if(putenv(buf2))
#endif
	    {
		fprintf(stderr, "unable to set HYANG_DEFAULT_PKGS\n");
		exit(1);
	    }
	    else have_cmdarg_default_packages = 1;
	    i0 = i;
	    continue;
	}
	av[ac++] = argv[i];
	i0 = i;
    }

    if(!e_mode) {
	if(++i0 >= argc) {
	    fprintf(stderr, "file name is missing\n");
	    exit(1);
	}
	if(strlen(argv[i0]) > PATH_MAX) {
	    fprintf(stderr, "file name is too long\n");
	    exit(1);
	}
	snprintf(buf, PATH_MAX+8, "--file=%s", argv[i0]);
	av[ac++] = buf;
    }
    i = i0+1;
    if (i < argc) {
	av[ac++] = "--args";
	for(; i < argc; i++)
	    av[ac++] = argv[i];
    }
    av[ac] = (char *) NULL;
#ifdef HAVE_PUTENV
    if (! have_cmdarg_default_packages) {
	char *rdpvar = "HYANG_DEFAULT_PKGS";
	char *rsdp = getenv("HYANG_SCRIPT_DEFAULT_PKGS");
	if (rsdp && strlen(rdpvar) + strlen(rsdp) + 1 < sizeof(buf2)) {
	    snprintf(buf2, sizeof(buf2), "%s=%s", rdpvar, rsdp);
	    putenv(buf2);
	}
    }

    p = getenv("HYANG_SCRIPT_LEGACY");
    int legacy = (p && (strcmp(p, "yes") == 0)) ? 1 : 0;
    if(legacy && !set_dp && !getenv("HYANG_DEFAULT_PKGS"))
	putenv("HYANG_DEFAULT_PKGS=datasets,utils,grade,graphics,datasci");

#ifndef _WIN32
    if (!getenv("HYANG_ARCH") && *hyangarch) {
	if (strlen(hyangarch) + 9 > sizeof(buf2)) {
	    fprintf(stderr, "impossibly long string for HYANG_ARCH\n");
	    exit(1);
	}
	strcpy(buf2, "HYANG_ARCH=/");
	strcat(buf2, hyangarch);
	putenv(buf2);
    }
#endif
#endif
    if(verbose) {
	fprintf(stderr, "running\n  '%s", cmd);
	for(i = 1; i < ac; i++) fprintf(stderr, " %s", av[i]);
	fprintf(stderr, "'\n\n");
    }
#ifndef _WIN32
    res = execv(cmd, av);
    perror("hyangscript execution error");
#else
    AppMain(ac, av);
#endif
    return res;
#else
    fprintf(stderr, "hyangscript is not supported on this system");
    exit(1);
#endif
}

