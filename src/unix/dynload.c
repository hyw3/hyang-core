/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangdyp.h>

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

# ifdef __hpux
#  ifdef HAVE_DL_H
#   include "hpdlfcn.c"
#   define HAVE_DYNAMIC_LOADING
#  endif
# else
#  ifdef HAVE_DLFCN_H
#   include <dlfcn.h>
#   define HAVE_DYNAMIC_LOADING
#  endif
# endif

#ifdef HAVE_DYNAMIC_LOADING

static void *loadLibrary(const char *path, int asLocal, int now,
			 const char *search);
static void closeLibrary(void *handle);
static void deleteCachedSymbols(DllInfo *);
static DL_FUNC hyang_local_dlsym(DllInfo *info, char const *name);
static void getFullDLLPath(SEXP call, char *buf, const char *path);
static void getSystemError(char *buf, int len);

static int computeDLOpenFlag(int asLocal, int now);

void attribute_hidden InitFunctionHashing()
{
    hyang_osDynSym->loadLibrary = loadLibrary;
    hyang_osDynSym->dlsym = hyang_local_dlsym;
    hyang_osDynSym->closeLibrary = closeLibrary;
    hyang_osDynSym->getError = getSystemError;

    hyang_osDynSym->deleteCachedSymbols = deleteCachedSymbols;
    hyang_osDynSym->lookupCachedSymbol = hyangly_lookupCachedSym;

    hyang_osDynSym->getFullDLLPath = getFullDLLPath;
}

static void getSystemError(char *buf, int len)
{
    strcpy(buf, dlerror());
}

static void *loadLibrary(const char *path, int asLocal, int now,
			 const char *search)
{
    void *handle;
    int openFlag = 0;

    openFlag = computeDLOpenFlag(asLocal, now);
    handle = (void *) dlopen(path,openFlag);

    return(handle);
}

static void closeLibrary(HINSTANCE handle)
{
    dlclose(handle);
}

static void deleteCachedSymbols(DllInfo *dll)
{
#ifdef CACHE_DLL_SYM
    int i;
    for(i = nCPFun - 1; i >= 0; i--)
	if(!strcmp(CPFun[i].pkg, dll->name)) {
	    if(i < nCPFun - 1) {
		strcpy(CPFun[i].name, CPFun[--nCPFun].name);
		strcpy(CPFun[i].pkg, CPFun[nCPFun].pkg);
		CPFun[i].func = CPFun[nCPFun].func;
	    } else nCPFun--;
	}
#endif
}

static int computeDLOpenFlag(int asLocal, int now)
{
#if !defined(RTLD_LOCAL) || !defined(RTLD_GLOBAL) || !defined(RTLD_NOW) || !defined(RTLD_LAZY)
    static char *warningMessages[] = {
	N_("Explicit local dynamic loading not supported on this platform. Using default."),
	N_("Explicit global dynamic loading not supported on this platform. Using default."),
	N_("Explicit non-lazy dynamic loading not supported on this platform. Using default."),
	N_("Explicit lazy dynamic loading not supported on this platform. Using default.")
    };

# define DL_WARN(i) \
    if(asInteger(GetOption1(install("warn"))) == 1 || \
       asInteger(GetOption1(install("verbose"))) > 0) \
	warning(_(warningMessages[i]))
#endif

    int openFlag = 0;

    if(asLocal != 0) {
#ifndef RTLD_LOCAL
	DL_WARN(0);
#else
	openFlag = RTLD_LOCAL;
#endif
    } else {
#ifndef RTLD_GLOBAL
	DL_WARN(1);
#else
	openFlag = RTLD_GLOBAL;
#endif
    }

    if(now != 0) {
#ifndef RTLD_NOW
	DL_WARN(2);
#else
	openFlag |= RTLD_NOW;
#endif
    } else {
#ifndef RTLD_LAZY
	DL_WARN(3);
#else
	openFlag |= RTLD_LAZY;
#endif
    }

    return(openFlag);
}

typedef union {void *p; DL_FUNC fn;} fn_ptr;
static DL_FUNC hyang_local_dlsym(DllInfo *info, char const *name)
{
    fn_ptr tmp;
    tmp.p = dlsym(info->handle, name);
    return tmp.fn;
}

static void getFullDLLPath(SEXP call, char *buf, const char *path)
{
    if(path[0] == '~')
	strcpy(buf, hyang_ExpandFileName(path));
    else if(path[0] != '/') {
#ifdef HAVE_GETCWD
	if(!getcwd(buf, PATH_MAX))
#endif
	    errorcall(call, _("cannot get working directory!"));
	strcat(buf, "/");
	strcat(buf, path);
    }
    else strcpy(buf, path);
}

#endif
