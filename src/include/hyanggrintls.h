/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGGRINTLS_H_
#define HYANGGRINTLS_H_

#include <hyangexts/hyangboolean.h>

#include <hyangexts/hyanggrengine.h>

#define HYANG_GRAPHICS 1
#include <hyanggraph.h>

#define MAX_LAYOUT_ROWS 200
#define MAX_LAYOUT_COLS 200
#define MAX_LAYOUT_CELLS 10007

typedef struct {
	double ax;
	double bx;
	double ay;
	double by;
} GTrans;

typedef struct {
    int	state;
    hyangboolean valid;
    double adj;
    hyangboolean ann;
    hyangcolor bg;
    char bty;
    double cex;
    double lheight;
    hyangcolor col;
    double crt;
    double din[2];
    int	err;
    hyangcolor fg;
    char family[201];
    int	font;
    double gamma;
    int	lab[3];
    int	las;
    int	lty;
    double lwd;
    hyang_GE_lineend lend;
    hyang_GE_linejoin ljoin;
    double lmitre;
    double mgp[3];
    double mkh;
    int	pch;
    double ps;
    int	smo;
    double srt;
    double tck;
    double tcl;
    double xaxp[3];
    char xaxs;		/* X Axis style */
    char xaxt;		/* X Axis type */
    hyangboolean xlog;	/* Log Axis for X */
    int	xpd;		/* Clip to plot region indicator */
    int	oldxpd;
    double yaxp[3];	/* Y Axis annotation */
    char yaxs;		/* Y Axis style */
    char yaxt;		/* Y Axis type */
    hyangboolean ylog;	/* Log Axis for Y */

    double cexbase;	/* Base character size */
    double cexmain;	/* Main title size */
    double cexlab;	/* xlab and ylab size */
    double cexsub;	/* Sub title size */
    double cexaxis;	/* Axis label size */

    int	fontmain;	/* Main title font */
    int	fontlab;	/* Xlab and ylab font */
    int	fontsub;	/* Subtitle font */
    int	fontaxis;	/* Axis label fonts */

    hyangcolor colmain;	/* Main title color */
    hyangcolor collab;	/* Xlab and ylab color */
    hyangcolor colsub;	/* Subtitle color */
    hyangcolor colaxis;	/* Axis label color */

    hyangboolean layout;

    int	numrows;
    int	numcols;
    int	currentFigure;
    int	lastFigure;
    double heights[MAX_LAYOUT_ROWS];
    double widths[MAX_LAYOUT_COLS];
    int	cmHeights[MAX_LAYOUT_ROWS];
    int	cmWidths[MAX_LAYOUT_COLS];
    unsigned short order[MAX_LAYOUT_CELLS];
    int	rspct;
    unsigned char respect[MAX_LAYOUT_CELLS];

    int	mfind;

    double fig[4];	/* (current) Figure size (proportion) */
			/* [0] = left, [1] = right */
			/* [2] = bottom, [3] = top */
    double fin[2];	/* (current) Figure size (inches) */
			/* [0] = width, [1] = height */
    GUnit fUnits;	/* (current) figure size units */
    double plt[4];	/* (current) Plot size (proportions) */
			/* [0] = left, [1] = right */
			/* [2] = bottom, [3] = top */
    double pin[2];	/* (current) plot size (inches) */
			/* [0] = width, [1] = height */
    GUnit pUnits;	/* (current) plot size units */
    hyangboolean defaultFigure;
    hyangboolean defaultPlot;

    double mar[4];	/* Plot margins in lines */
    double mai[4];	/* Plot margins in inches */
			/* [0] = bottom, [1] = left */
			/* [2] = top, [3] = right */
    GUnit mUnits;	/* plot margin units */
    double mex;		/* Margin expansion factor */
    double oma[4];	/* Outer margins in lines */
    double omi[4];	/* outer margins in inches */
    double omd[4];	/* outer margins in NDC */
			/* [0] = bottom, [1] = left */
			/* [2] = top, [3] = right */
    GUnit oUnits;	/* outer margin units */
    char pty;		/* Plot type */

    double usr[4];	/* Graphics window */
			/* [0] = xmin, [1] = xmax */
			/* [2] = ymin, [3] = ymax */

    double logusr[4];

    hyangboolean new;	/* Clean plot ? */
    int	devmode;	/* creating new image or adding to existing one */

    double xNDCPerChar;	/* Nominal character width (NDC) */
    double yNDCPerChar;	/* Nominal character height (NDC) */
    double xNDCPerLine;	/* Nominal line width (NDC) */
    double yNDCPerLine;	/* Nominal line height (NDC) */
    double xNDCPerInch;	/* xNDC -> Inches */
    double yNDCPerInch;	/* yNDC -> Inches */

    GTrans fig2dev;	/* Figure to device */

    GTrans inner2dev;	/* Inner region to device */

    GTrans ndc2dev;	/* NDC to raw device */

    GTrans win2fig;	/* Window to figure mapping */

    double scale;
} GPar;

#define copyGPar		hyangly_copyGPar
#define FixupCol		hyangly_FixupCol
#define FixupLty		hyangly_FixupLty
#define FixupLwd		hyangly_FixupLwd
#define FixupVFont		hyangly_FixupVFont
#define GInit			hyangly_GInit
#define labelformat		hyangly_labelformat
#define ProcessInlinePars	hyangly_ProcessInlinePars
#define recordGraphicOperation	hyangly_recordGraphOpr

hyangboolean GRecording(SEXP, pGEDevDesc);

void GInit(GPar*);

void copyGPar(GPar *, GPar *);

double hyang_Log10(double);

void ProcessInlinePars(SEXP, pGEDevDesc);

void recordGraphicOperation(SEXP, SEXP, pGEDevDesc);

SEXP FixupCol(SEXP, unsigned int);
SEXP FixupLty(SEXP, int);
SEXP FixupLwd(SEXP, double);
SEXP FixupVFont(SEXP);
SEXP labelformat(SEXP);

void gcontextFromGP(pGEcontext gc, pGEDevDesc dd);

#define gpptr hyangly_gpptr
#define dpptr hyangly_dpptr
GPar* hyangly_gpptr(pGEDevDesc dd);
GPar* hyangly_dpptr(pGEDevDesc dd);

#endif /* HYANGGRINTLS_H_ */
