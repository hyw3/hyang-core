/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_INTLS_H_
#define HYANG_INTLS_H_

#ifdef __cplusplus
# include <cstdio>
# include <climits>
# include <cstddef>
extern "C" {
#else
# include <stdio.h>
# include <limits.h>
# include <stddef.h>
#endif

#include <hyangexts/hyangarith.h>
#include <hyangexts/hyangboolean.h>
#include <hyangexts/hyangcomplex.h>
#include <hyangexts/hyangerrhand.h>
#include <hyangexts/hyangmem.h>
#include <hyangexts/hyangutls.h>
#include <hyangexts/hyangextprint.h>
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangextlib.h>

typedef unsigned char hyangbyte;

typedef int hyang_len_t;
#define HYANG_LEN_T_MAX INT_MAX

#ifndef HYANG_CONFIG_H
# include <hyangconfig.h>
#endif

#if ( SIZEOF_SIZE_T > 4 )
# define LONG_VECTOR_SUPPORT
#endif

#ifdef LONG_VECTOR_SUPPORT
    typedef ptrdiff_t hyang_xlen_t;
# define HYANG_XLEN_T_MAX 4503599627370496
# define HYANG_SHORT_LEN_MAX 2147483647
#else
    typedef int hyang_xlen_t;
# define HYANG_XLEN_T_MAX HYANG_LEN_T_MAX
#endif

#ifndef TESTING_WRITE_BARRIER
# define INLINE_PROTECT
#endif

#define HYANG_INTLS_UUID ""

#ifndef enum_SEXPTYPE
typedef unsigned int SEXPTYPE;

#define ABSURDSXP	     0	  /* nil = NULL */
#define SYMSXP	     1	  /* symbols */
#define LISTSXP	     2	  /* lists of dotted pairs */
#define CLOSXP	     3	  /* closures */
#define ENVSXP	     4	  /* environments */
#define PROMSXP	     5	  /* promises: [un]evaluated closure arguments */
#define LANGSXP	     6	  /* language constructs (special lists) */
#define SPECIALSXP   7	  /* special forms */
#define BUILTINSXP   8	  /* builtin non-special forms */
#define CHARSXP	     9	  /* "scalar" string type (internal only)*/
#define LGLSXP	    10	  /* logical vectors */
#define INTSXP	    13	  /* integer vectors */
#define REALSXP	    14	  /* real variables */
#define CPLXSXP	    15	  /* complex variables */
#define STRSXP	    16	  /* string vectors */
#define DOTSXP	    17	  /* dot-dot-dot object */
#define ANYSXP	    18	  /* make "any" args work.
			     Used in specifying types for symbol
			     registration to mean anything is okay  */
#define VECSXP	    19	  /* generic vectors */
#define EXPRSXP	    20	  /* expressions vectors */
#define BCODESXP    21    /* byte code */
#define EXTPTRSXP   22    /* external pointer */
#define WEAKREFSXP  23    /* weak reference */
#define RAWSXP      24    /* raw bytes */
#define S4SXP       25    /* S4, non-vector */

#define NEWSXP      30    /* fresh node created in new page */
#define FREESXP     31    /* node released by GC */

#define FUNSXP      99    /* Closure or Builtin or Special */


#else

typedef enum {
    ABSURDSXP	= 0,	/* nil = NULL */
    SYMSXP	= 1,	/* symbols */
    LISTSXP	= 2,	/* lists of dotted pairs */
    CLOSXP	= 3,	/* closures */
    ENVSXP	= 4,	/* environments */
    PROMSXP	= 5,	/* promises: [un]evaluated closure arguments */
    LANGSXP	= 6,	/* language constructs (special lists) */
    SPECIALSXP	= 7,	/* special forms */
    BUILTINSXP	= 8,	/* builtin non-special forms */
    CHARSXP	= 9,	/* "scalar" string type (internal only)*/
    LGLSXP	= 10,	/* logical vectors */
    INTSXP	= 13,	/* integer vectors */
    REALSXP	= 14,	/* real variables */
    CPLXSXP	= 15,	/* complex variables */
    STRSXP	= 16,	/* string vectors */
    DOTSXP	= 17,	/* dot-dot-dot object */
    ANYSXP	= 18,	/* make "any" args work */
    VECSXP	= 19,	/* generic vectors */
    EXPRSXP	= 20,	/* expressions vectors */
    BCODESXP	= 21,	/* byte code */
    EXTPTRSXP	= 22,	/* external pointer */
    WEAKREFSXP	= 23,	/* weak reference */
    RAWSXP	= 24,	/* raw bytes */
    S4SXP	= 25,	/* S4 non-vector */

    NEWSXP      = 30,   /* fresh node creaed in new page */
    FREESXP     = 31,   /* node released by GC */

    FUNSXP	= 99	/* Closure or Builtin */
} SEXPTYPE;
#endif

#define TYPE_BITS 5
#define MAX_NUM_SEXPTYPE (1<<TYPE_BITS)

typedef struct SEXPREC *SEXP;

#ifdef USE_HYANGINTLS

#define NAMED_BITS 16

struct sxpinfo_struct {
    SEXPTYPE type      :  TYPE_BITS;
    unsigned int scalar:  1;
    unsigned int obj   :  1;
    unsigned int alt   :  1;
    unsigned int gp    : 16;
    unsigned int mark  :  1;
    unsigned int debug :  1;
    unsigned int trace :  1;  /* functions and memory tracing */
    unsigned int spare :  1;  /* used on closures and when REFCNT is defined */
    unsigned int pbcgen :  1;  /* old generation number */
    unsigned int pbccls :  3;  /* node class */
    unsigned int named : NAMED_BITS;
    unsigned int extra : 32 - NAMED_BITS;
};

struct vecsxp_struct {
    hyang_xlen_t	length;
    hyang_xlen_t	truelength;
};

struct primsxp_struct {
    int offset;
};

struct symsxp_struct {
    struct SEXPREC *pname;
    struct SEXPREC *value;
    struct SEXPREC *internal;
};

struct listsxp_struct {
    struct SEXPREC *carval;
    struct SEXPREC *cdrval;
    struct SEXPREC *tagval;
};

struct envsxp_struct {
    struct SEXPREC *frame;
    struct SEXPREC *enclos;
    struct SEXPREC *hashtab;
};

struct closxp_struct {
    struct SEXPREC *formals;
    struct SEXPREC *body;
    struct SEXPREC *env;
};

struct promsxp_struct {
    struct SEXPREC *value;
    struct SEXPREC *expr;
    struct SEXPREC *env;
};

//#define SWITCH_TO_REFCNT

#if defined(SWITCH_TO_REFCNT) && ! defined(COMPUTE_REFCNT_VALUES)
# define COMPUTE_REFCNT_VALUES
#endif
#if defined(SWITCH_TO_REFCNT) && ! defined(ADJUST_ENVIR_REFCNTS)
# define ADJUST_ENVIR_REFCNTS
#endif
#define REFCNTMAX ((1 << NAMED_BITS) - 1)

#define SEXPREC_HEADER \
    struct sxpinfo_struct sxpinfo; \
    struct SEXPREC *attrib; \
    struct SEXPREC *genpbc_next_node, *genpbc_prev_node

typedef struct SEXPREC {
    SEXPREC_HEADER;
    union {
	struct primsxp_struct primsxp;
	struct symsxp_struct symsxp;
	struct listsxp_struct listsxp;
	struct envsxp_struct envsxp;
	struct closxp_struct closxp;
	struct promsxp_struct promsxp;
    } u;
} SEXPREC;

typedef struct VECTOR_SEXPREC {
    SEXPREC_HEADER;
    struct vecsxp_struct vecsxp;
} VECTOR_SEXPREC, *VECSEXP;

typedef union { VECTOR_SEXPREC s; double align; } SEXPREC_ALIGN;

#define ATTRIB(x)	((x)->attrib)
#define OBJECT(x)	((x)->sxpinfo.obj)
#define MARK(x)		((x)->sxpinfo.mark)
#define TYPEOF(x)	((x)->sxpinfo.type)
#define NAMED(x)	((x)->sxpinfo.named)
#define RTRACE(x)	((x)->sxpinfo.trace)
#define LEVELS(x)	((x)->sxpinfo.gp)
#define SET_OBJECT(x,v)	(((x)->sxpinfo.obj)=(v))
#define SET_TYPEOF(x,v)	(((x)->sxpinfo.type)=(v))
#define SET_NAMED(x,v)	(((x)->sxpinfo.named)=(v))
#define SET_RTRACE(x,v)	(((x)->sxpinfo.trace)=(v))
#define SETLEVELS(x,v)	(((x)->sxpinfo.gp)=((unsigned short)v))
#define ALTREP(x)       ((x)->sxpinfo.alt)
#define SETALTREP(x, v) (((x)->sxpinfo.alt) = (v))
#define SETSCALAR(x, v) (((x)->sxpinfo.scalar) = (v))

#if defined(COMPUTE_REFCNT_VALUES)
# define REFCNT(x) ((x)->sxpinfo.named)
# define TRACKREFS(x) (TYPEOF(x) == CLOSXP ? TRUE : ! (x)->sxpinfo.spare)
#else
# define REFCNT(x) 0
# define TRACKREFS(x) FALSE
#endif

#ifdef SWITCH_TO_REFCNT
# undef NAMED
# undef SET_NAMED
# define NAMED(x) REFCNT(x)
# define SET_NAMED(x, v) do {} while (0)
#endif

#define ENSURE_NAMEDMAX(v) do {			\
	SEXP __enm_v__ = (v);			\
	if (NAMED(__enm_v__) < NAMEDMAX)	\
	    SET_NAMED( __enm_v__, NAMEDMAX);	\
    } while (0)
#define ENSURE_NAMED(v) do { if (NAMED(v) == 0) SET_NAMED(v, 1); } while (0)
#define SETTER_CLEAR_NAMED(x) do {				\
	SEXP __x__ = (x);				\
	if (NAMED(__x__) == 1) SET_NAMED(__x__, 0);	\
    } while (0)
#define RAISE_NAMED(x, n) do {			\
	SEXP __x__ = (x);			\
	int __n__ = (n);			\
	if (NAMED(__x__) < __n__)		\
	    SET_NAMED(__x__, __n__);		\
    } while (0)

/* S4 object bit, set by hyangto_NewObj for all new() calls */
#define S4_OBJECT_MASK ((unsigned short)(1<<4))
#define IS_S4_OBJECT(x) ((x)->sxpinfo.gp & S4_OBJECT_MASK)
#define SET_S4_OBJECT(x) (((x)->sxpinfo.gp) |= S4_OBJECT_MASK)
#define UNSET_S4_OBJECT(x) (((x)->sxpinfo.gp) &= ~S4_OBJECT_MASK)

/* JIT optimization support */
#define NOJIT_MASK ((unsigned short)(1<<5))
#define NOJIT(x) ((x)->sxpinfo.gp & NOJIT_MASK)
#define SET_NOJIT(x) (((x)->sxpinfo.gp) |= NOJIT_MASK)
#define MAYBEJIT_MASK ((unsigned short)(1<<6))
#define MAYBEJIT(x) ((x)->sxpinfo.gp & MAYBEJIT_MASK)
#define SET_MAYBEJIT(x) (((x)->sxpinfo.gp) |= MAYBEJIT_MASK)
#define UNSET_MAYBEJIT(x) (((x)->sxpinfo.gp) &= ~MAYBEJIT_MASK)

/* Growable vector support */
#define GROWABLE_MASK ((unsigned short)(1<<5))
#define GROWABLE_BIT_SET(x) ((x)->sxpinfo.gp & GROWABLE_MASK)
#define SET_GROWABLE_BIT(x) (((x)->sxpinfo.gp) |= GROWABLE_MASK)
#define IS_GROWABLE(x) (GROWABLE_BIT_SET(x) && XLENGTH(x) < XTRUELENGTH(x))

/* Vector Access Macros */
#ifdef LONG_VECTOR_SUPPORT
# define IS_LONG_VEC(x) (XLENGTH(x) > HYANG_SHORT_LEN_MAX)
#else
# define IS_LONG_VEC(x) 0
#endif
#define STDVEC_LENGTH(x) (((VECSEXP) (x))->vecsxp.length)
#define STDVEC_TRUELENGTH(x) (((VECSEXP) (x))->vecsxp.truelength)
#define SET_STDVEC_TRUELENGTH(x, v) (STDVEC_TRUELENGTH(x)=(v))
#define SET_TRUELENGTH(x,v) do {				\
	SEXP sl__x__ = (x);					\
	hyang_xlen_t sl__v__ = (v);				\
	if (ALTREP(x)) error("can't set ALTREP truelength");	\
	SET_STDVEC_TRUELENGTH(sl__x__, sl__v__);	\
    } while (0)

#define IS_SCALAR(x, t) (((x)->sxpinfo.type == (t)) && (x)->sxpinfo.scalar)
#define LENGTH(x) LENGTH_EX(x, __FILE__, __LINE__)
#define TRUELENGTH(x) XTRUELENGTH(x)

#define XLENGTH(x) XLENGTH_EX(x)

#define SET_STDVEC_LENGTH(x,v) do {		\
	SEXP __x__ = (x);			\
	hyang_xlen_t __v__ = (v);		\
	STDVEC_LENGTH(__x__) = __v__;		\
	SETSCALAR(__x__, __v__ == 1 ? 1 : 0);	\
    } while (0)

#define STDVEC_DATAPTR(x) ((void *) (((SEXPREC_ALIGN *) (x)) + 1))
#define CHAR(x)		((const char *) STDVEC_DATAPTR(x))
#define LOGICAL(x)	((int *) DATAPTR(x))
#define INTEGER(x)	((int *) DATAPTR(x))
#define RAW(x)		((hyangbyte *) DATAPTR(x))
#define COMPLEX(x)	((hyangcomplex *) DATAPTR(x))
#define REAL(x)		((double *) DATAPTR(x))
#define VECTOR_ELT(x,i)	((SEXP *) DATAPTR(x))[i]
#define STRING_PTR(x)	((SEXP *) DATAPTR(x))
#define VECTOR_PTR(x)	((SEXP *) DATAPTR(x))
#define LOGICAL_RO(x)	((const int *) DATAPTR_RO(x))
#define INTEGER_RO(x)	((const int *) DATAPTR_RO(x))
#define RAW_RO(x)	((const hyangbyte *) DATAPTR_RO(x))
#define COMPLEX_RO(x)	((const hyangcomplex *) DATAPTR_RO(x))
#define REAL_RO(x)	((const double *) DATAPTR_RO(x))
#define STRING_PTR_RO(x)((const SEXP *) DATAPTR_RO(x))

#define LISTVAL(x)	((x)->u.listsxp)
#define TAG(e)		((e)->u.listsxp.tagval)
#define CAR(e)		((e)->u.listsxp.carval)
#define CDR(e)		((e)->u.listsxp.cdrval)
#define CAAR(e)		CAR(CAR(e))
#define CDAR(e)		CDR(CAR(e))
#define CADR(e)		CAR(CDR(e))
#define CDDR(e)		CDR(CDR(e))
#define CDDDR(e)	CDR(CDR(CDR(e)))
#define CADDR(e)	CAR(CDR(CDR(e)))
#define CADDDR(e)	CAR(CDR(CDR(CDR(e))))
#define CAD4R(e)	CAR(CDR(CDR(CDR(CDR(e)))))
#define MISSING_MASK	15
#define MISSING(x)	((x)->sxpinfo.gp & MISSING_MASK)
#define SET_MISSING(x,v) do { \
  SEXP __x__ = (x); \
  int __v__ = (v); \
  int __other_flags__ = __x__->sxpinfo.gp & ~MISSING_MASK; \
  __x__->sxpinfo.gp = __other_flags__ | __v__; \
} while (0)

#define FORMALS(x)	((x)->u.closxp.formals)
#define BODY(x)		((x)->u.closxp.body)
#define CLOENV(x)	((x)->u.closxp.env)
#define RDEBUG(x)	((x)->sxpinfo.debug)
#define SET_RDEBUG(x,v)	(((x)->sxpinfo.debug)=(v))
#define RSTEP(x)	((x)->sxpinfo.spare)
#define SET_RSTEP(x,v)	(((x)->sxpinfo.spare)=(v))

#define PRINTNAME(x)	((x)->u.symsxp.pname)
#define SYMVALUE(x)	((x)->u.symsxp.value)
#define INTERNAL(x)	((x)->u.symsxp.internal)
#define DDVAL_MASK	1
#define DDVAL(x)	((x)->sxpinfo.gp & DDVAL_MASK) /* for ..1, ..2 etc */
#define SET_DDVAL_BIT(x) (((x)->sxpinfo.gp) |= DDVAL_MASK)
#define UNSET_DDVAL_BIT(x) (((x)->sxpinfo.gp) &= ~DDVAL_MASK)
#define SET_DDVAL(x,v) ((v) ? SET_DDVAL_BIT(x) : UNSET_DDVAL_BIT(x)) /* for ..1, ..2 etc */

#define FRAME(x)	((x)->u.envsxp.frame)
#define ENCLOS(x)	((x)->u.envsxp.enclos)
#define HASHTAB(x)	((x)->u.envsxp.hashtab)
#define ENVFLAGS(x)	((x)->sxpinfo.gp)	/* for environments */
#define SET_ENVFLAGS(x,v)	(((x)->sxpinfo.gp)=(v))

#else /* not USE_HYANGINTLS */
// ======================= not USE_HYANGINTLS section

#define CHAR(x)		HYANG_CHAR(x)
const char *(HYANG_CHAR)(SEXP x);

hyangboolean (hyangly_isNull)(SEXP s);
hyangboolean (hyangly_isSym)(SEXP s);
hyangboolean (hyangly_isLogical)(SEXP s);
hyangboolean (hyangly_isReal)(SEXP s);
hyangboolean (hyangly_isComplex)(SEXP s);
hyangboolean (hyangly_isExpr)(SEXP s);
hyangboolean (hyangly_isEnv)(SEXP s);
hyangboolean (hyangly_isString)(SEXP s);
hyangboolean (hyangly_isObj)(SEXP s);

#endif /* USE_HYANGINTLS */

#define IS_SIMPLE_SCALAR(x, type) \
    (IS_SCALAR(x, type) && ATTRIB(x) == hyang_AbsurdValue)

#define NAMEDMAX 3
#define INCREMENT_NAMED(x) do {				\
	SEXP __x__ = (x);				\
	if (NAMED(__x__) != NAMEDMAX)			\
	    SET_NAMED(__x__, NAMED(__x__) + 1);		\
    } while (0)
#define DECREMENT_NAMED(x) do {				    \
	SEXP __x__ = (x);				    \
	int __n__ = NAMED(__x__);			    \
	if (__n__ > 0 && __n__ < NAMEDMAX)		    \
	    SET_NAMED(__x__, __n__ - 1);		    \
    } while (0)

#define INCREMENT_LINKS(x) do {			\
	SEXP il__x__ = (x);			\
	INCREMENT_NAMED(il__x__);		\
	INCREMENT_REFCNT(il__x__);		\
    } while (0)
#define DECREMENT_LINKS(x) do {			\
	SEXP dl__x__ = (x);			\
	DECREMENT_NAMED(dl__x__);		\
	DECREMENT_REFCNT(dl__x__);		\
    } while (0)

#if defined(COMPUTE_REFCNT_VALUES)
# define SET_REFCNT(x,v) (REFCNT(x) = (v))
# if defined(EXTRA_REFCNT_FIELDS)
#  define SET_TRACKREFS(x,v) (TRACKREFS(x) = (v))
# else
#  define SET_TRACKREFS(x,v) ((x)->sxpinfo.spare = ! (v))
# endif
# define DECREMENT_REFCNT(x) do {					\
	SEXP drc__x__ = (x);						\
	if (REFCNT(drc__x__) > 0 && REFCNT(drc__x__) < REFCNTMAX)	\
	    SET_REFCNT(drc__x__, REFCNT(drc__x__) - 1);			\
    } while (0)
# define INCREMENT_REFCNT(x) do {			      \
	SEXP irc__x__ = (x);				      \
	if (REFCNT(irc__x__) < REFCNTMAX)		      \
	    SET_REFCNT(irc__x__, REFCNT(irc__x__) + 1);	      \
    } while (0)
#else
# define SET_REFCNT(x,v) do {} while(0)
# define SET_TRACKREFS(x,v) do {} while(0)
# define DECREMENT_REFCNT(x) do {} while(0)
# define INCREMENT_REFCNT(x) do {} while(0)
#endif

#define ENABLE_REFCNT(x) SET_TRACKREFS(x, TRUE)
#define DISABLE_REFCNT(x) SET_TRACKREFS(x, FALSE)

/* Macros for some common idioms. */
#ifdef SWITCH_TO_REFCNT
# define MAYBE_SHARED(x) (REFCNT(x) > 1)
# define NO_REFERENCES(x) (REFCNT(x) == 0)
# define MARK_NOT_MUTABLE(x) SET_REFCNT(x, REFCNTMAX)
#else
# define MAYBE_SHARED(x) (NAMED(x) > 1)
# define NO_REFERENCES(x) (NAMED(x) == 0)
# define MARK_NOT_MUTABLE(x) SET_NAMED(x, NAMEDMAX)
#endif
#define MAYBE_REFERENCED(x) (! NO_REFERENCES(x))
#define NOT_SHARED(x) (! MAYBE_SHARED(x))

/* ALTREP sorting support */
enum {SORTED_DECR_NA_1ST = -2,
      SORTED_DECR = -1,
      UNKNOWN_SORTEDNESS = INT_MIN, /*INT_MIN is NA_INTEGER! */
      SORTED_INCR = 1,
      SORTED_INCR_NA_1ST = 2,
      KNOWN_UNSORTED = 0};
#define KNOWN_SORTED(sorted) (sorted == SORTED_DECR ||			\
			      sorted == SORTED_INCR ||			\
			      sorted == SORTED_DECR_NA_1ST ||		\
			      sorted == SORTED_INCR_NA_1ST)

#define KNOWN_NA_1ST(sorted) (sorted == SORTED_INCR_NA_1ST ||	\
			      sorted == SORTED_DECR_NA_1ST)

#define KNOWN_INCR(sorted) (sorted == SORTED_INCR ||		\
			    sorted == SORTED_INCR_NA_1ST)

#define KNOWN_DECR(sorted) (sorted == SORTED_DECR ||	\
			    sorted == SORTED_DECR_NA_1ST)

/* Complex assignment support */
/* temporary definition that will need to be refined to distinguish
   getter from setter calls */
#define IS_GETTER_CALL(call) (CADR(call) == hyang_TmpValSym)

/* Accessor functions.  Many are declared using () to avoid the macro
   definitions in the USE_HYANGINTLS section.
   The function STRING_ELT is used as an argument to arrayAssign even
   if the macro version is in use.
*/

/* General Cons Cell Attributes */
SEXP (ATTRIB)(SEXP x);
int  (OBJECT)(SEXP x);
int  (MARK)(SEXP x);
int  (TYPEOF)(SEXP x);
int  (NAMED)(SEXP x);
int  (REFCNT)(SEXP x);
int  (TRACKREFS)(SEXP x);
void (SET_OBJECT)(SEXP x, int v);
void (SET_TYPEOF)(SEXP x, int v);
void (SET_NAMED)(SEXP x, int v);
void SET_ATTRIB(SEXP x, SEXP v);
void DUPLICATE_ATTRIB(SEXP to, SEXP from);
void SHALLOW_DUPLICATE_ATTRIB(SEXP to, SEXP from);
void (ENSURE_NAMEDMAX)(SEXP x);
void (ENSURE_NAMED)(SEXP x);
void (SETTER_CLEAR_NAMED)(SEXP x);
void (RAISE_NAMED)(SEXP x, int n);

/* S4 object testing */
int (IS_S4_OBJECT)(SEXP x);
void (SET_S4_OBJECT)(SEXP x);
void (UNSET_S4_OBJECT)(SEXP x);

/* JIT optimization support */
int (NOJIT)(SEXP x);
int (MAYBEJIT)(SEXP x);
void (SET_NOJIT)(SEXP x);
void (SET_MAYBEJIT)(SEXP x);
void (UNSET_MAYBEJIT)(SEXP x);

/* Growable vector support */
int (IS_GROWABLE)(SEXP x);
void (SET_GROWABLE_BIT)(SEXP x);

/* Vector Access Functions */
int  (LENGTH)(SEXP x);
hyang_xlen_t (XLENGTH)(SEXP x);
hyang_xlen_t  (TRUELENGTH)(SEXP x);
void (SETLENGTH)(SEXP x, hyang_xlen_t v);
void (SET_TRUELENGTH)(SEXP x, hyang_xlen_t v);
int  (IS_LONG_VEC)(SEXP x);
int  (LEVELS)(SEXP x);
int  (SETLEVELS)(SEXP x, int v);
#ifdef TESTING_WRITE_BARRIER
hyang_xlen_t (STDVEC_LENGTH)(SEXP);
hyang_xlen_t (STDVEC_TRUELENGTH)(SEXP);
void (SETALTREP)(SEXP, int);
#endif

int  *(LOGICAL)(SEXP x);
int  *(INTEGER)(SEXP x);
hyangbyte *(RAW)(SEXP x);
double *(REAL)(SEXP x);
hyangcomplex *(COMPLEX)(SEXP x);
const int  *(LOGICAL_RO)(SEXP x);
const int  *(INTEGER_RO)(SEXP x);
const hyangbyte *(RAW_RO)(SEXP x);
const double *(REAL_RO)(SEXP x);
const hyangcomplex *(COMPLEX_RO)(SEXP x);
//SEXP (STRING_ELT)(SEXP x, hyang_xlen_t i);
SEXP (VECTOR_ELT)(SEXP x, hyang_xlen_t i);
void SET_STRING_ELT(SEXP x, hyang_xlen_t i, SEXP v);
SEXP SET_VECTOR_ELT(SEXP x, hyang_xlen_t i, SEXP v);
SEXP *(STRING_PTR)(SEXP x);
const SEXP *(STRING_PTR_RO)(SEXP x);
SEXP * NORET (VECTOR_PTR)(SEXP x);

/* ALTREP support */
void *(STDVEC_DATAPTR)(SEXP x);
int (IS_SCALAR)(SEXP x, int type);
int (ALTREP)(SEXP x);
SEXP ALTREP_DUPLICATE_EX(SEXP x, hyangboolean deep);
SEXP ALTREP_COERCE(SEXP x, int type);
hyangboolean ALTREP_INSPECT(SEXP, int, int, int, void (*)(SEXP, int, int, int));
SEXP ALTREP_SERIALIZED_CLASS(SEXP);
SEXP ALTREP_SERIALIZED_STATE(SEXP);
SEXP ALTREP_UNSERIALIZE_EX(SEXP, SEXP, SEXP, int, int);
hyang_xlen_t ALTREP_LENGTH(SEXP x);
hyang_xlen_t ALTREP_TRUELENGTH(SEXP x);
void *ALTVEC_DATAPTR(SEXP x);
const void *ALTVEC_DATAPTR_RO(SEXP x);
const void *ALTVEC_DATAPTR_OR_NULL(SEXP x);
SEXP ALTVEC_EXTRACT_SUBSET(SEXP x, SEXP indx, SEXP call);
int ALTINTEGER_ELT(SEXP x, hyang_xlen_t i);
void ALTINTEGER_SET_ELT(SEXP x, hyang_xlen_t i, int v);
int ALTLOGICAL_ELT(SEXP x, hyang_xlen_t i);
void ALTLOGICAL_SET_ELT(SEXP x, hyang_xlen_t i, int v);
double ALTREAL_ELT(SEXP x, hyang_xlen_t i);
void ALTREAL_SET_ELT(SEXP x, hyang_xlen_t i, double v);
SEXP ALTSTRING_ELT(SEXP, hyang_xlen_t);
void ALTSTRING_SET_ELT(SEXP, hyang_xlen_t, SEXP);
hyangcomplex ALTCOMPLEX_ELT(SEXP x, hyang_xlen_t i);
void ALTCOMPLEX_SET_ELT(SEXP x, hyang_xlen_t i, hyangcomplex v);
hyangbyte ALTRAW_ELT(SEXP x, hyang_xlen_t i);
void ALTRAW_SET_ELT(SEXP x, hyang_xlen_t i, int v);
hyang_xlen_t INTEGER_GET_REGION(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, int *buf);
int INTEGER_IS_SORTED(SEXP x);
int INTEGER_NO_NA(SEXP x);
SEXP ALTINTEGER_SUM(SEXP x, hyangboolean narm);
SEXP ALTREAL_SUM(SEXP x, hyangboolean narm);
SEXP ALTINTEGER_MIN(SEXP x, hyangboolean narm);
SEXP ALTINTEGER_MAX(SEXP x, hyangboolean narm);
SEXP ALTREAL_MIN(SEXP x, hyangboolean narm);
SEXP ALTREAL_MAX(SEXP x, hyangboolean narm);
SEXP INTEGER_MATCH(SEXP, SEXP, int, SEXP, SEXP, hyangboolean);
SEXP INTEGER_IS_NA(SEXP x);
SEXP REAL_MATCH(SEXP, SEXP, int, SEXP, SEXP, hyangboolean);

hyang_xlen_t REAL_GET_REGION(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, double *buf);
int REAL_IS_SORTED(SEXP x);
int REAL_NO_NA(SEXP x);
SEXP REAL_IS_NA(SEXP x);
int STRING_IS_SORTED(SEXP x);
int STRING_NO_NA(SEXP x);
SEXP hyang_compact_intrange(hyang_xlen_t n1, hyang_xlen_t n2);
SEXP hyang_deferred_coerceToStr(SEXP v, SEXP sp);
SEXP hyang_virtrep_vec(SEXP, SEXP);

#ifdef LONG_VECTOR_SUPPORT
    hyang_len_t NORET hyang_BadLongVector(SEXP, const char *, int);
#endif

#define CONS(a, b)	cons((a), (b))		/* data lists */
#define LCONS(a, b)	lcons((a), (b))		/* language lists */
SEXP (TAG)(SEXP e);
SEXP (CAR)(SEXP e);
SEXP (CDR)(SEXP e);
SEXP (CAAR)(SEXP e);
SEXP (CDAR)(SEXP e);
SEXP (CADR)(SEXP e);
SEXP (CDDR)(SEXP e);
SEXP (CDDDR)(SEXP e);
SEXP (CADDR)(SEXP e);
SEXP (CADDDR)(SEXP e);
SEXP (CAD4R)(SEXP e);
int  (MISSING)(SEXP x);
void (SET_MISSING)(SEXP x, int v);
void SET_TAG(SEXP x, SEXP y);
SEXP SETCAR(SEXP x, SEXP y);
SEXP SETCDR(SEXP x, SEXP y);
SEXP SETCADR(SEXP x, SEXP y);
SEXP SETCADDR(SEXP x, SEXP y);
SEXP SETCADDDR(SEXP x, SEXP y);
SEXP SETCAD4R(SEXP e, SEXP y);

SEXP CONS_NR(SEXP a, SEXP b);

SEXP (FORMALS)(SEXP x);
SEXP (BODY)(SEXP x);
SEXP (CLOENV)(SEXP x);
int  (RDEBUG)(SEXP x);
int  (RSTEP)(SEXP x);
int  (RTRACE)(SEXP x);
void (SET_RDEBUG)(SEXP x, int v);
void (SET_RSTEP)(SEXP x, int v);
void (SET_RTRACE)(SEXP x, int v);
void SET_FORMALS(SEXP x, SEXP v);
void SET_BODY(SEXP x, SEXP v);
void SET_CLOENV(SEXP x, SEXP v);

/* Symbol Access Functions */
SEXP (PRINTNAME)(SEXP x);
SEXP (SYMVALUE)(SEXP x);
SEXP (INTERNAL)(SEXP x);
int  (DDVAL)(SEXP x);
void (SET_DDVAL)(SEXP x, int v);
void SET_PRINTNAME(SEXP x, SEXP v);
void SET_SYMVALUE(SEXP x, SEXP v);
void SET_INTERNAL(SEXP x, SEXP v);

/* Environment Access Functions */
SEXP (FRAME)(SEXP x);
SEXP (ENCLOS)(SEXP x);
SEXP (HASHTAB)(SEXP x);
int  (ENVFLAGS)(SEXP x);
void (SET_ENVFLAGS)(SEXP x, int v);
void SET_FRAME(SEXP x, SEXP v);
void SET_ENCLOS(SEXP x, SEXP v);
void SET_HASHTAB(SEXP x, SEXP v);

/* Promise Access Functions */
/* First five have macro versions in hyangdefn.h */
SEXP (PRCODE)(SEXP x);
SEXP (PRENV)(SEXP x);
SEXP (PRVALUE)(SEXP x);
int  (PRSEEN)(SEXP x);
void (SET_PRSEEN)(SEXP x, int v);
void SET_PRENV(SEXP x, SEXP v);
void SET_PRVALUE(SEXP x, SEXP v);
void SET_PRCODE(SEXP x, SEXP v);
void SET_PRSEEN(SEXP x, int v);

/* Hashing Functions */
/* There are macro versions in hyangdefn.h */
int  (HASHASH)(SEXP x);
int  (HASHVALUE)(SEXP x);
void (SET_HASHASH)(SEXP x, int v);
void (SET_HASHVALUE)(SEXP x, int v);


/* External pointer access macros */
#define EXTPTR_PTR(x)	CAR(x)
#define EXTPTR_PROT(x)	CDR(x)
#define EXTPTR_TAG(x)	TAG(x)

/* Bytecode access macros */
#define BCODE_CODE(x)	CAR(x)
#define BCODE_CONSTS(x) CDR(x)
#define BCODE_EXPR(x)	TAG(x)
#define isByteCode(x)	(TYPEOF(x)==BCODESXP)

/* Pointer Protection and Unprotection */
#define PROTECT(s)	hyangly_protect(s)
#define UNPROTECT(n)	hyangly_unprotect(n)
#define UNPROTECT_PTR(s)	hyangly_ptrUnprotect(s)

typedef int PROTECT_INDEX;
#define PROTECT_WITH_INDEX(x,i) hyang_ProtectWithIndex(x,i)
#define REPROTECT(x,i) hyang_Reprotect(x,i)

/* Evaluation Environment */
LibExtern SEXP	hyang_GlobalEnv;	    /* The "global" environment */

LibExtern SEXP  hyang_EmptyEnv;	    /* An empty environment at the root of the
				    	environment tree */
LibExtern SEXP  hyang_BaseEnv;	    /* The base environment; formerly hyang_AbsurdValue */
LibExtern SEXP	hyang_BaseNamespace;    /* The (fake) namespace for base */
LibExtern SEXP	hyang_NamespaceRegistry;/* Registry for registered namespaces */

LibExtern SEXP	hyang_Srcref;           /* Current srcref, for debuggers */

/* Special Values */
LibExtern SEXP	hyang_AbsurdValue;	    /* The nil object */
LibExtern SEXP	hyang_UnboundValue;	    /* Unbound marker */
LibExtern SEXP	hyang_MissingArg;	    /* Missing argument marker */
LibExtern SEXP	hyang_InAbah;  /* To be found in BC interp. state
				       (marker) */
LibExtern SEXP	hyang_CurrentExpr; /* Use current expression (marker) */
#ifdef __MAIN__
attribute_hidden
#else
extern
#endif
SEXP	hyang_RestartToken;     /* Marker for restarted function calls */

/* Symbol Table Shortcuts */
LibExtern SEXP	hyang_AsCharSym;/* "as.character" */
LibExtern SEXP	hyang_baseSym; // <-- backcompatible version of:
LibExtern SEXP	hyang_BaseSym;	// "base"
LibExtern SEXP	hyang_BraceSym;	    /* "{" */
LibExtern SEXP	hyang_Bracket2Sym;   /* "[[" */
LibExtern SEXP	hyang_BracketSym;    /* "[" */
LibExtern SEXP	hyang_ClassSym;	    /* "class" */
LibExtern SEXP	hyang_DeviceSym;	    /* ".Device" */
LibExtern SEXP	hyang_DimNamesSym;   /* "dimnames" */
LibExtern SEXP	hyang_DimSym;	    /* "dim" */
LibExtern SEXP	hyang_DollarSym;	    /* "$" */
LibExtern SEXP	hyang_DotsSym;	    /* "..." */
LibExtern SEXP	hyang_DoubleColonSym;// "::"
LibExtern SEXP	hyang_DropSym;	    /* "drop" */
LibExtern SEXP	hyang_LastvalueSym;  /* ".Last.value" */
LibExtern SEXP	hyang_LevelsSym;	    /* "levels" */
LibExtern SEXP	hyang_ModeSym;	    /* "mode" */
LibExtern SEXP	hyang_NaRmSym;	    /* "na.rm" */
LibExtern SEXP	hyang_NameSym;	    /* "name" */
LibExtern SEXP	hyang_NamesSym;	    /* "names" */
LibExtern SEXP	hyang_NamespaceEnvSym;// ".__NAMESPACE__."
LibExtern SEXP	hyang_PkgSym;    /* "package" */
LibExtern SEXP	hyang_PreviousSym;   /* "previous" */
LibExtern SEXP	hyang_QuoteSym;	    /* "quote" */
LibExtern SEXP	hyang_RowNamesSym;   /* "row.names" */
LibExtern SEXP	hyang_SeedsSym;	    /* ".Random.seed" */
LibExtern SEXP	hyang_SortListSym;   /* "sort.list" */
LibExtern SEXP	hyang_SourceSym;	    /* "source" */
LibExtern SEXP	hyang_SpecSym;	// "spec"
LibExtern SEXP	hyang_TripleColonSym;// ":::"
LibExtern SEXP	hyang_TspSym;	    /* "tsp" */

LibExtern SEXP  hyang_dot_defined;      /* ".defined" */
LibExtern SEXP  hyang_dot_Method;       /* ".Method" */
LibExtern SEXP	hyang_dot_pkgName;// ".packageName"
LibExtern SEXP  hyang_dot_target;       /* ".target" */
LibExtern SEXP  hyang_dot_Gen;      /* ".Generic" */

/* Missing Values - others from hyangarith.h */
#define NA_STRING	hyang_NaString
LibExtern SEXP	hyang_NaString;	    /* NA_STRING as a CHARSXP */
LibExtern SEXP	hyang_BlankString;	    /* "" as a CHARSXP */
LibExtern SEXP	hyang_BlankScalarStr;	    /* "" as a STRSXP */

/* srcref related functions */
SEXP hyang_GetCurrentSrcref(int);
SEXP hyang_GetSrcFilename(SEXP);
SEXP hyangly_asChar(SEXP);
SEXP hyangly_coeVect(SEXP, SEXPTYPE);
SEXP hyangly_ptvl(SEXP x);
SEXP hyangly_vtpl(SEXP x);
SEXP hyangly_asCharf(SEXP x);
int hyangly_asLogical(SEXP x);
int hyangly_asInteger(SEXP x);
double hyangly_asReal(SEXP x);
hyangcomplex hyangly_asComplex(SEXP x);


#ifndef HYANG_ALLOCATOR_TYPE
#define HYANG_ALLOCATOR_TYPE
typedef struct hyang_allocator hyang_allocator_t;
#endif

char * hyangly_cpstr(const char *);
void hyangly_addMVarsToNewEnv(SEXP, SEXP);
SEXP hyangly_alloc3DArray(SEXPTYPE, int, int, int);
SEXP hyangly_allocArray(SEXPTYPE, SEXP);
SEXP hyangly_allocFL2(SEXP sym1, SEXP sym2);
SEXP hyangly_allocFL3(SEXP sym1, SEXP sym2, SEXP sym3);
SEXP hyangly_allocFL4(SEXP sym1, SEXP sym2, SEXP sym3, SEXP sym4);
SEXP hyangly_allocFL5(SEXP sym1, SEXP sym2, SEXP sym3, SEXP sym4, SEXP sym5);
SEXP hyangly_allocFL6(SEXP sym1, SEXP sym2, SEXP sym3, SEXP sym4, SEXP sym5, SEXP sym6);
SEXP hyangly_allocMatrix(SEXPTYPE, int, int);
SEXP hyangly_allocList(int);
SEXP hyangly_allocS4Obj(void);
SEXP hyangly_allocSExp(SEXPTYPE);
SEXP hyangly_allocVect3(SEXPTYPE, hyang_xlen_t, hyang_allocator_t*);
hyang_xlen_t hyangly_anyDupl(SEXP x, hyangboolean from_last);
hyang_xlen_t hyangly_anyDupl3(SEXP x, SEXP incomp, hyangboolean from_last);
SEXP hyangly_applyClosure(SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP hyangly_arraySubscript(int, SEXP, SEXP, SEXP (*)(SEXP,SEXP),
                       SEXP (*)(SEXP, int), SEXP);
SEXP hyangly_classGets(SEXP, SEXP);
SEXP hyangly_cons(SEXP, SEXP);
SEXP hyangly_fixSubset3Args(SEXP, SEXP, SEXP, SEXP*);
void hyangly_copyMatrix(SEXP, SEXP, hyangboolean);
void hyangly_copyListMatrix(SEXP, SEXP, hyangboolean);
void hyangly_copyMostAttrib(SEXP, SEXP);
void hyangly_copyVect(SEXP, SEXP);
int hyangly_countCtxs(int, int);
SEXP hyangly_CreateTag(SEXP);
void hyangly_defineVar(SEXP, SEXP, SEXP);
SEXP hyangly_dimgets(SEXP, SEXP);
SEXP hyangly_dimNamesGets(SEXP, SEXP);
SEXP hyangly_DropDims(SEXP);
SEXP hyangly_duplicate(SEXP);
SEXP hyangly_shallowDupl(SEXP);
SEXP hyangly_econDupl(SEXP);
SEXP hyangly_Dupld(SEXP, hyangboolean);
hyangboolean hyang_envHasNoSpecialSyms(SEXP);
SEXP hyangly_eval(SEXP, SEXP);
SEXP hyangly_ExtractSubset(SEXP, SEXP, SEXP);
SEXP hyangly_findFun(SEXP, SEXP);
SEXP hyangly_findFun3(SEXP, SEXP, SEXP);
void hyangly_findFuncForBody(SEXP);
SEXP hyangly_findVar(SEXP, SEXP);
SEXP hyangly_findVarInFrame(SEXP, SEXP);
SEXP hyangly_findVarInFrame3(SEXP, SEXP, hyangboolean);
SEXP hyangly_getAttr(SEXP, SEXP);
SEXP hyangly_GetArrayDimNames(SEXP);
SEXP hyangly_getColNames(SEXP);
void hyangly_GetMatrixDimNames(SEXP, SEXP*, SEXP*, const char**, const char**);
SEXP hyangly_GetOpt(SEXP, SEXP);
SEXP hyangly_GetOpt1(SEXP);
int hyangly_GetOptDigits(void);
int hyangly_GetOptWidth(void);
SEXP hyangly_getRowNames(SEXP);
void hyangly_gsetVar(SEXP, SEXP, SEXP);
SEXP hyangly_inst(const char *);
SEXP hyangly_instChar(SEXP);
SEXP hyangly_instNoTrChar(SEXP);
SEXP hyangly_installDDVAL(int i);
SEXP hyangly_instS3Signature(const char *, const char *);
hyangboolean hyangly_isFree(SEXP);
hyangboolean hyangly_isOrdered(SEXP);
hyangboolean hyangly_isUnmodifSpecSym(SEXP sym, SEXP env);
hyangboolean hyangly_isUnordered(SEXP);
hyangboolean hyangly_isUnsorted(SEXP, hyangboolean);
SEXP hyangly_getLength(SEXP, hyang_len_t);
SEXP hyangly_xlengthGets(SEXP, hyang_xlen_t);
SEXP hyang_lsInternal(SEXP, hyangboolean);
SEXP hyang_lsInternal3(SEXP, hyangboolean, hyangboolean);
SEXP hyangly_match(SEXP, SEXP, int);
SEXP hyangly_matchE(SEXP, SEXP, int, SEXP);
SEXP hyangly_namesGets(SEXP, SEXP);
SEXP hyangly_mkChar(const char *);
SEXP hyangly_mkCharLen(const char *, int);
hyangboolean hyangly_NonNullStrMatch(SEXP, SEXP);
int hyangly_ncols(SEXP);
int hyangly_nrows(SEXP);
SEXP hyangly_nthcdr(SEXP, int);

typedef enum {Bytes, Chars, Width} nchar_type;
int hyang_nchar(SEXP string, nchar_type type_,
	    hyangboolean allowNA, hyangboolean keepNA, const char* msg_name);

hyangboolean hyangly_pmatch(SEXP, SEXP, hyangboolean);
hyangboolean hyangly_psMatch(const char *, const char *, hyangboolean);
SEXP hyang_ParseEvalStr(const char *, SEXP);
void hyangly_PrtValue(SEXP);
#ifndef INLINE_PROTECT
SEXP hyangly_protect(SEXP);
#endif
void hyangly_readS3VarsFromFrame(SEXP, SEXP*, SEXP*, SEXP*, SEXP*, SEXP*, SEXP*);
SEXP hyangly_setAttr(SEXP, SEXP, SEXP);
void hyangly_setSVect(SEXP*, int, SEXP);
void hyangly_setVar(SEXP, SEXP, SEXP);
SEXP hyangly_strSuffix(SEXP, int);
SEXPTYPE hyangly_str2type(const char *);
hyangboolean hyangly_StrBlank(SEXP);
SEXP hyangly_substitute(SEXP,SEXP);
SEXP hyangly_topEnv(SEXP, SEXP);
const char * hyangly_trnsltChar(SEXP);
const char * hyangly_trnsltChar0(SEXP);
const char * hyangly_trnsltCharUTF8(SEXP);
const char * hyangly_type2char(SEXPTYPE);
SEXP hyangly_type2hystr(SEXPTYPE);
SEXP hyangly_type2str(SEXPTYPE);
SEXP hyangly_type2strNoWarn(SEXPTYPE);
#ifndef INLINE_PROTECT
void hyangly_unprotect(int);
#endif
void hyangly_ptrUnprotect(SEXP);

void NORET hyang_signal_protect_error(void);
void NORET hyang_signal_unprotect_error(void);
void NORET hyang_signal_reprotect_error(PROTECT_INDEX i);

#ifndef INLINE_PROTECT
void hyang_ProtectWithIndex(SEXP, PROTECT_INDEX *);
void hyang_Reprotect(SEXP, PROTECT_INDEX);
#endif
SEXP hyang_tryEval(SEXP, SEXP, int *);
SEXP hyang_tryEvalSilent(SEXP, SEXP, int *);
const char *hyang_curErrBuf();

hyangboolean hyangly_isS4(SEXP);
SEXP hyangly_asS4(SEXP, hyangboolean, int);
SEXP hyangly_S3Class(SEXP);
int hyangly_isBasicClass(const char *);

hyangboolean hyang_cycle_detected(SEXP s, SEXP child);

typedef enum {
    CE_NATIVE = 0,
    CE_UTF8   = 1,
    CE_LATIN1 = 2,
    CE_BYTES  = 3,
    CE_SYMBOL = 5,
    CE_ANY    =99
} cetype_t;

cetype_t hyangly_getCharCE(SEXP);
SEXP hyangly_mkCharCE(const char *, cetype_t);
SEXP hyangly_mkCharLenCE(const char *, int, cetype_t);
const char *hyangly_reEnc(const char *x, cetype_t ce_in, cetype_t ce_out, int subst);

				/* match(.) NOT reached : for -Wall */
#define error_return(msg)	{ hyangly_error(msg);	   return hyang_AbsurdValue; }
#define errorcall_return(cl,msg){ hyangly_errCall(cl, msg);   return hyang_AbsurdValue; }

#ifdef __MAIN__
#undef extern
#undef LibExtern
#endif

SEXP hyang_forceAndCall(SEXP e, int n, SEXP rho);

SEXP hyang_MakeExtPtr(void *p, SEXP tag, SEXP prot);
void *hyang_ExtPtrAddr(SEXP s);
SEXP hyang_ExtPtrTag(SEXP s);
SEXP hyang_ExternalPtrProtected(SEXP s);
void hyang_ClearExternalPtr(SEXP s);
void hyang_SetExternalPtrAddr(SEXP s, void *p);
void hyang_SetExternalPtrTag(SEXP s, SEXP tag);
void hyang_SetExternalPtrProtected(SEXP s, SEXP p);
SEXP hyang_MakeExtPtrFn(DL_FUNC p, SEXP tag, SEXP prot);
DL_FUNC hyang_ExtPtrAddrFn(SEXP s);

typedef void (*hyang_CFinalizer_t)(SEXP);
void hyang_RegisterFinalizer(SEXP s, SEXP fun);
void hyang_RegCFinalizer(SEXP s, hyang_CFinalizer_t fun);
void hyang_RegisterFinalizerEx(SEXP s, SEXP fun, hyangboolean onexit);
void hyang_RegCFinalizerEx(SEXP s, hyang_CFinalizer_t fun, hyangboolean onexit);
void hyang_RunPendingFinalizers(void);

SEXP hyang_MakeWeakRef(SEXP key, SEXP val, SEXP fin, hyangboolean onexit);
SEXP hyang_MakeWeakRefC(SEXP key, SEXP val, hyang_CFinalizer_t fin, hyangboolean onexit);
SEXP hyang_WeakRefKey(SEXP w);
SEXP hyang_WeakRefValue(SEXP w);
void hyang_RunWeakRefFinalizer(SEXP w);

SEXP hyang_PromExpr(SEXP);
SEXP hyang_ClosureExpr(SEXP);
SEXP hyang_AbahExpr(SEXP e);
void hyang_AbahInitBC(void);
SEXP hyang_AbahEncode(SEXP);
SEXP hyang_AbahDecode(SEXP);
void hyang_AbahRegister(SEXP, SEXP);
hyangboolean hyang_checkConstants(hyangboolean);
hyangboolean hyang_AbahVersionOK(SEXP);
#define PREXPR(e) hyang_PromExpr(e)
#define BODY_EXPR(e) hyang_ClosureExpr(e)

void hyang_init_altrep();
void hyang_reinit_altrep_classes(DllInfo *);

hyangboolean hyang_ToplevelExec(void (*fun)(void *), void *data);
SEXP hyang_ExecWithCleanup(SEXP (*fun)(void *), void *data,
		       void (*cleanfun)(void *), void *cleandata);
SEXP hyang_tryCatch(SEXP (*)(void *), void *,       /* body closure*/
		SEXP,                           /* condition classes (STRSXP) */
		SEXP (*)(SEXP, void *), void *, /* handler closure */
		void (*)(void *), void *);      /* finally closure */
SEXP hyang_tryCatchError(SEXP (*)(void *), void *,        /* body closure*/
		     SEXP (*)(SEXP, void *), void *); /* handler closure */
SEXP hyang_MakeUnwindCont();
void NORET hyang_ContinueUnwind(SEXP cont);
SEXP hyang_UnwindProtect(SEXP (*fun)(void *data), void *data,
                     void (*cleanfun)(void *data, hyangboolean jump),
                     void *cleandata, SEXP cont);

void hyang_RestoreHashCount(SEXP rho);
hyangboolean hyang_IsPackageEnv(SEXP rho);
SEXP hyang_PackageEnvName(SEXP rho);
SEXP hyang_FindPackageEnv(SEXP info);
hyangboolean hyang_IsNamespaceEnv(SEXP rho);
SEXP hyang_NamespaceEnvSpec(SEXP rho);
SEXP hyang_FindNamespace(SEXP info);
void hyang_LockEnvironment(SEXP env, hyangboolean bindings);
hyangboolean hyang_EnvironmentIsLocked(SEXP env);
void hyang_LockBinding(SEXP sym, SEXP env);
void hyang_unLockBinding(SEXP sym, SEXP env);
void hyang_MakeActiveBinding(SEXP sym, SEXP fun, SEXP env);
hyangboolean hyang_BindingIsLocked(SEXP sym, SEXP env);
hyangboolean hyang_BindingIsActive(SEXP sym, SEXP env);
hyangboolean hyang_HasFancyBindings(SEXP rho);

#if defined(__GNUC__) && __GNUC__ >= 3
void hyangly_errCall(SEXP, const char *, ...) __attribute__((noreturn));
#else
void hyangly_errCall(SEXP, const char *, ...);
#endif
void hyangly_warningcall(SEXP, const char *, ...);
void hyangly_warningCallImmediate(SEXP, const char *, ...);

#define HYANG_XDR_DOUBLE_SIZE 8
#define HYANG_XDR_INTEGER_SIZE 4

void hyang_XDREncodeDouble(double d, void *buf);
double hyang_XDRDecodeDouble(void *buf);
void hyang_XDREncodeInt(int i, void *buf);
int hyang_XDRDecodeInt(void *buf);

typedef void *hyang_pstream_data_t;

typedef enum {
    hyang_pstream_any_format,
    hyang_pstream_ascii_format,
    hyang_pstream_binary_format,
    hyang_pstream_xdr_format,
    hyang_pstream_asciihex_format
} hyang_pstream_format_t;

typedef struct hyang_outpstream_st *hyang_outpstream_t;
struct hyang_outpstream_st {
    hyang_pstream_data_t data;
    hyang_pstream_format_t type;
    int version;
    void (*OutChar)(hyang_outpstream_t, int);
    void (*OutBytes)(hyang_outpstream_t, void *, int);
    SEXP (*OutPersistHookFunc)(SEXP, SEXP);
    SEXP OutPersistHookData;
};

typedef struct hyang_inpstream_st *hyang_inpstream_t;
#define HYANG_CODESET_MAX 63
struct hyang_inpstream_st {
    hyang_pstream_data_t data;
    hyang_pstream_format_t type;
    int (*InChar)(hyang_inpstream_t);
    void (*InBytes)(hyang_inpstream_t, void *, int);
    SEXP (*InPersistHookFunc)(SEXP, SEXP);
    SEXP InPersistHookData;
    char native_encoding[HYANG_CODESET_MAX + 1];
    void *nat2nat_obj;
    void *nat2utf8_obj;
};

void hyang_InitInPStream(hyang_inpstream_t stream, hyang_pstream_data_t data,
		     hyang_pstream_format_t type,
		     int (*inchar)(hyang_inpstream_t),
		     void (*inbytes)(hyang_inpstream_t, void *, int),
		     SEXP (*phook)(SEXP, SEXP), SEXP pdata);
void hyang_InitOutPStream(hyang_outpstream_t stream, hyang_pstream_data_t data,
		      hyang_pstream_format_t type, int version,
		      void (*outchar)(hyang_outpstream_t, int),
		      void (*outbytes)(hyang_outpstream_t, void *, int),
		      SEXP (*phook)(SEXP, SEXP), SEXP pdata);

#ifdef __cplusplus
void hyang_InitFileInPStream(hyang_inpstream_t stream, std::FILE *fp,
			 hyang_pstream_format_t type,
			 SEXP (*phook)(SEXP, SEXP), SEXP pdata);
void hyang_InitFileOutPStream(hyang_outpstream_t stream, std::FILE *fp,
			  hyang_pstream_format_t type, int version,
			  SEXP (*phook)(SEXP, SEXP), SEXP pdata);
#else
void hyang_InitFileInPStream(hyang_inpstream_t stream, FILE *fp,
			 hyang_pstream_format_t type,
			 SEXP (*phook)(SEXP, SEXP), SEXP pdata);
void hyang_InitFileOutPStream(hyang_outpstream_t stream, FILE *fp,
			  hyang_pstream_format_t type, int version,
			  SEXP (*phook)(SEXP, SEXP), SEXP pdata);
#endif

#ifdef NEED_CONNECTION_PSTREAMS
#ifndef HAVE_HYANGCONNECTION_TYPEDEF
typedef struct HyangConn  *hyangconn;
#define HAVE_HYANGCONNECTION_TYPEDEF
#endif
void hyang_InitConnOutPStream(hyang_outpstream_t stream, hyangconn con,
			  hyang_pstream_format_t type, int version,
			  SEXP (*phook)(SEXP, SEXP), SEXP pdata);
void hyang_InitConnInPStream(hyang_inpstream_t stream,  hyangconn con,
			 hyang_pstream_format_t type,
			 SEXP (*phook)(SEXP, SEXP), SEXP pdata);
#endif

void hyang_Serialize(SEXP s, hyang_outpstream_t ops);
SEXP hyang_Unserialize(hyang_inpstream_t ips);
SEXP hyang_SerializeInfo(hyang_inpstream_t ips);
SEXP hyangto_slot(SEXP obj, SEXP name);
SEXP hyangto_slotAssign(SEXP obj, SEXP name, SEXP value);
int hyang_has_slot(SEXP obj, SEXP name);
SEXP hyang_S4_extends(SEXP klass, SEXP useTable);

SEXP hyangto_MAKE_CLASS(const char *what);
SEXP hyang_getClassDef  (const char *what);
SEXP hyang_getClassDef_hyang(SEXP what);
hyangboolean hyang_has_methods_attached(void);
hyangboolean hyang_isVirtualClass(SEXP class_def, SEXP env);
hyangboolean hyang_extends  (SEXP class1, SEXP class2, SEXP env);
SEXP hyangto_NewObj(SEXP class_def);

int hyang_check_class_and_super(SEXP x, const char **valid, SEXP rho);
int hyang_check_class_etc      (SEXP x, const char **valid);

void hyang_PreserveObj(SEXP);
void hyang_ReleaseObj(SEXP);

void hyang_dotLast(void);		/* in main.c */
void hyang_RunExitFinalizers(void);	/* in memory.c */

#ifdef HAVE_POPEN
# ifdef __cplusplus
std::FILE *hyang_popen(const char *, const char *);
# else
FILE *hyang_popen(const char *, const char *);
# endif
#endif
int hyang_system(const char *);

hyangboolean hyang_compute_identical(SEXP, SEXP, int);

SEXP hyang_body_no_src(SEXP x);

void hyang_orderVect (int *indx, int n, SEXP arglist, hyangboolean nalast, hyangboolean decreasing);
void hyang_orderVect1(int *indx, int n, SEXP x,       hyangboolean nalast, hyangboolean decreasing);

#ifndef HYANG_NOREMAP
#define acopy_string		hyangly_cpstr
#define addMissingVarsToNewEnv	hyangly_addMVarsToNewEnv
#define alloc3DArray            hyangly_alloc3DArray
#define allocArray		hyangly_allocArray
#define allocFormalsList2	hyangly_allocFL2
#define allocFormalsList3	hyangly_allocFL3
#define allocFormalsList4	hyangly_allocFL4
#define allocFormalsList5	hyangly_allocFL5
#define allocFormalsList6	hyangly_allocFL6
#define allocList		hyangly_allocList
#define allocMatrix		hyangly_allocMatrix
#define allocS4Object		hyangly_allocS4Obj
#define allocSExp		hyangly_allocSExp
#define allocVector		hyangly_allocVect
#define allocVector3		hyangly_allocVect3
#define any_duplicated		hyangly_anyDupl
#define any_duplicated3		hyangly_anyDupl3
#define applyClosure		hyangly_applyClosure
#define arraySubscript		hyangly_arraySubscript
#define asChar			hyangly_asChar
#define asCharacterFactor	hyangly_asCharf
#define asComplex		hyangly_asComplex
#define asInteger		hyangly_asInteger
#define asLogical		hyangly_asLogical
#define asReal			hyangly_asReal
#define asS4			hyangly_asS4
#define classgets		hyangly_classGets
#define coerceVector		hyangly_coeVect
#define conformable		hyangly_conformable
#define cons			hyangly_cons
#define fixSubset3Args		hyangly_fixSubset3Args
#define copyListMatrix		hyangly_copyListMatrix
#define copyMatrix		hyangly_copyMatrix
#define copyMostAttrib		hyangly_copyMostAttrib
#define copyVector		hyangly_copyVect
#define countContexts		hyangly_countCtxs
#define CreateTag		hyangly_CreateTag
#define defineVar		hyangly_defineVar
#define dimgets			hyangly_dimgets
#define dimnamesgets		hyangly_dimNamesGets
#define DropDims                hyangly_DropDims
#define duplicate		hyangly_duplicate
#define duplicated		hyangly_Dupld
#define elt			hyangly_elt
#define errorcall		hyangly_errCall
#define eval			hyangly_eval
#define ExtractSubset		hyangly_ExtractSubset
#define findFun			hyangly_findFun
#define findFun3		hyangly_findFun3
#define findFunctionForBody	hyangly_findFuncForBody
#define findVar			hyangly_findVar
#define findVarInFrame		hyangly_findVarInFrame
#define findVarInFrame3		hyangly_findVarInFrame3
#define GetArrayDimnames	hyangly_GetArrayDimNames
#define getAttrib		hyangly_getAttr
#define getCharCE		hyangly_getCharCE
#define GetColNames		hyangly_getColNames
#define GetMatrixDimnames	hyangly_GetMatrixDimNames
#define GetOption1		hyangly_GetOpt1
#define GetOptionDigits		hyangly_GetOptDigits
#define GetOptionWidth		hyangly_GetOptWidth
#define GetOption		hyangly_GetOpt
#define GetRowNames		hyangly_getRowNames
#define gsetVar			hyangly_gsetVar
#define inherits		hyangly_inherits
#define install			hyangly_inst
#define installChar		hyangly_instChar
#define installNoTrChar		hyangly_instNoTrChar
#define installDDVAL		hyangly_installDDVAL
#define installS3Signature	hyangly_instS3Signature
#define isArray			hyangly_isArray
#define isBasicClass            hyangly_isBasicClass
#define isComplex		hyangly_isComplex
#define isEnvironment		hyangly_isEnv
#define isExpression		hyangly_isExpr
#define isFactor		hyangly_isFactor
#define isFrame			hyangly_isFrame
#define isFree			hyangly_isFree
#define isFunction		hyangly_isFunction
#define isInteger		hyangly_isInteger
#define isLanguage		hyangly_isLanguage
#define isList			hyangly_isList
#define isLogical		hyangly_isLogical
#define isSymbol		hyangly_isSym
#define isMatrix		hyangly_isMatrix
#define isNewList		hyangly_isNewList
#define isNull			hyangly_isNull
#define isNumeric		hyangly_isNumeric
#define isNumber		hyangly_isNumber
#define isObject		hyangly_isObj
#define isOrdered		hyangly_isOrdered
#define isPairList		hyangly_isPairList
#define isPrimitive		hyangly_isPrimitive
#define isReal			hyangly_isReal
#define isS4			hyangly_isS4
#define isString		hyangly_isString
#define isTs			hyangly_isTs
#define isUnmodifiedSpecSym	hyangly_isUnmodifSpecSym
#define isUnordered		hyangly_isUnordered
#define isUnsorted		hyangly_isUnsorted
#define isUserBinop		hyangly_isUsrBinop
#define isValidString		hyangly_isValidStr
#define isValidStringF		hyangly_isValidStrF
#define isVector		hyangly_isVect
#define isVectorAtomic		hyangly_isVectAtomic
#define isVectorizable		hyangly_isVectorizable
#define isVectorList		hyangly_isVectList
#define lang1			hyangly_lang1
#define lang2			hyangly_lang2
#define lang3			hyangly_lang3
#define lang4			hyangly_lang4
#define lang5			hyangly_lang5
#define lang6			hyangly_lang6
#define lastElt			hyangly_lastElt
#define lazy_duplicate		hyangly_econDupl
#define lcons			hyangly_lcons
#define length(x)		hyangly_length(x)
#define lengthgets		hyangly_getLength
#define list1			hyangly_list1
#define list2			hyangly_list2
#define list3			hyangly_list3
#define list4			hyangly_list4
#define list5			hyangly_list5
#define list6			hyangly_list6
#define listAppend		hyangly_listAppend
#define match			hyangly_match
#define matchE			hyangly_matchE
#define mkChar			hyangly_mkChar
#define mkCharCE		hyangly_mkCharCE
#define mkCharLen		hyangly_mkCharLen
#define mkCharLenCE		hyangly_mkCharLenCE
#define mkNamed			hyangly_mkNamed
#define mkString		hyangly_mkStr
#define namesgets		hyangly_namesGets
#define ncols			hyangly_ncols
#define nlevels			hyangly_nlevels
#define NonNullStringMatch	hyangly_NonNullStrMatch
#define nrows			hyangly_nrows
#define nthcdr			hyangly_nthcdr
#define PairToVectorList	hyangly_ptvl
#define pmatch			hyangly_pmatch
#define psmatch			hyangly_psMatch
#define PrintValue		hyangly_PrtValue
#define protect			hyangly_protect
#define readS3VarsFromFrame	hyangly_readS3VarsFromFrame
#define reEnc			hyangly_reEnc
#define rownamesgets		hyangly_rowNamesGets
#define S3Class                 hyangly_S3Class
#define ScalarComplex		hyangly_ScalarCmplx
#define ScalarInteger		hyangly_ScalarInt
#define ScalarLogical		hyangly_ScalarLogical
#define ScalarReal		hyangly_ScalarReal
#define ScalarString		hyangly_ScalarStr
#define ScalarRaw		hyangly_ScalarRaw
#define setAttrib		hyangly_setAttr
#define setSVector		hyangly_setSVect
#define setVar			hyangly_setVar
#define shallow_duplicate	hyangly_shallowDupl
#define str2type		hyangly_str2type
#define stringSuffix		hyangly_strSuffix
#define stringPositionTr	hyangly_strPositionTr
#define StringBlank		hyangly_StrBlank
#define substitute		hyangly_substitute
#define topenv		        hyangly_topEnv
#define translateChar		hyangly_trnsltChar
#define translateChar0		hyangly_trnsltChar0
#define translateCharUTF8      	hyangly_trnsltCharUTF8
#define type2char		hyangly_type2char
#define type2rstr		hyangly_type2hystr
#define type2str		hyangly_type2str
#define type2str_nowarn		hyangly_type2strNoWarn
#define unprotect		hyangly_unprotect
#define unprotect_ptr		hyangly_ptrUnprotect
#define VectorToPairList	hyangly_vtpl
#define warningcall		hyangly_warningcall
#define warningcall_immediate	hyangly_warningCallImmediate
#define xlength(x)		hyangly_xlength(x)
#define xlengthgets		hyangly_xlengthGets

#endif

#if defined(CALLED_FROM_DEFN_H) && !defined(__MAIN__) && (defined(COMPILING_HYANG) || ( __GNUC__ && !defined(__INTEL_COMPILER) )) && (defined(COMPILING_HYANG) || !defined(NO_RINLINEDFUNS))
#include "hyanginldf.h"
#else

SEXP     hyangly_allocVect(SEXPTYPE, hyang_xlen_t);
hyangboolean hyangly_conformable(SEXP, SEXP);
SEXP	 hyangly_elt(SEXP, int);
hyangboolean hyangly_inherits(SEXP, const char *);
hyangboolean hyangly_isArray(SEXP);
hyangboolean hyangly_isFactor(SEXP);
hyangboolean hyangly_isFrame(SEXP);
hyangboolean hyangly_isFunction(SEXP);
hyangboolean hyangly_isInteger(SEXP);
hyangboolean hyangly_isLanguage(SEXP);
hyangboolean hyangly_isList(SEXP);
hyangboolean hyangly_isMatrix(SEXP);
hyangboolean hyangly_isNewList(SEXP);
hyangboolean hyangly_isNumber(SEXP);
hyangboolean hyangly_isNumeric(SEXP);
hyangboolean hyangly_isPairList(SEXP);
hyangboolean hyangly_isPrimitive(SEXP);
hyangboolean hyangly_isTs(SEXP);
hyangboolean hyangly_isUsrBinop(SEXP);
hyangboolean hyangly_isValidStr(SEXP);
hyangboolean hyangly_isValidStrF(SEXP);
hyangboolean hyangly_isVect(SEXP);
hyangboolean hyangly_isVectAtomic(SEXP);
hyangboolean hyangly_isVectList(SEXP);
hyangboolean hyangly_isVectorizable(SEXP);
SEXP	 hyangly_lang1(SEXP);
SEXP	 hyangly_lang2(SEXP, SEXP);
SEXP	 hyangly_lang3(SEXP, SEXP, SEXP);
SEXP	 hyangly_lang4(SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_lang5(SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_lang6(SEXP, SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_lastElt(SEXP);
SEXP	 hyangly_lcons(SEXP, SEXP);
hyang_len_t  hyangly_length(SEXP);
SEXP	 hyangly_list1(SEXP);
SEXP	 hyangly_list2(SEXP, SEXP);
SEXP	 hyangly_list3(SEXP, SEXP, SEXP);
SEXP	 hyangly_list4(SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_list5(SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_list6(SEXP, SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP	 hyangly_listAppend(SEXP, SEXP);
SEXP	 hyangly_mkNamed(SEXPTYPE, const char **);
SEXP	 hyangly_mkStr(const char *);
int	 hyangly_nlevels(SEXP);
int	 hyangly_strPositionTr(SEXP, const char *);
SEXP	 hyangly_ScalarCmplx(hyangcomplex);
SEXP	 hyangly_ScalarInt(int);
SEXP	 hyangly_ScalarLogical(int);
SEXP	 hyangly_ScalarRaw(hyangbyte);
SEXP	 hyangly_ScalarReal(double);
SEXP	 hyangly_ScalarStr(SEXP);
hyang_xlen_t  hyangly_xlength(SEXP);
hyang_xlen_t  (XLENGTH)(SEXP x);
hyang_xlen_t  (XTRUELENGTH)(SEXP x);
int LENGTH_EX(SEXP x, const char *file, int line);
hyang_xlen_t XLENGTH_EX(SEXP x);
# ifdef INLINE_PROTECT
SEXP hyangly_protect(SEXP);
void hyangly_unprotect(int);
void hyang_ProtectWithIndex(SEXP, PROTECT_INDEX *);
void hyang_Reprotect(SEXP, PROTECT_INDEX);
# endif
SEXP hyang_FixupRHS(SEXP x, SEXP y);
void *(DATAPTR)(SEXP x);
const void *(DATAPTR_RO)(SEXP x);
const void *(DATAPTR_OR_NULL)(SEXP x);
const int *(LOGICAL_OR_NULL)(SEXP x);
const int *(INTEGER_OR_NULL)(SEXP x);
const double *(REAL_OR_NULL)(SEXP x);
const hyangcomplex *(COMPLEX_OR_NULL)(SEXP x);
const hyangbyte *(RAW_OR_NULL)(SEXP x);
void *(STDVEC_DATAPTR)(SEXP x);
int (INTEGER_ELT)(SEXP x, hyang_xlen_t i);
double (REAL_ELT)(SEXP x, hyang_xlen_t i);
int (LOGICAL_ELT)(SEXP x, hyang_xlen_t i);
hyangcomplex (COMPLEX_ELT)(SEXP x, hyang_xlen_t i);
hyangbyte (RAW_ELT)(SEXP x, hyang_xlen_t i);
SEXP (STRING_ELT)(SEXP x, hyang_xlen_t i);
double SCALAR_DVAL(SEXP x);
int SCALAR_LVAL(SEXP x);
int SCALAR_IVAL(SEXP x);
void SET_SCALAR_DVAL(SEXP x, double v);
void SET_SCALAR_LVAL(SEXP x, int v);
void SET_SCALAR_IVAL(SEXP x, int v);
void SET_SCALAR_CVAL(SEXP x, hyangcomplex v);
void SET_SCALAR_BVAL(SEXP x, hyangbyte v);
SEXP hyang_altrep_data1(SEXP x);
SEXP hyang_altrep_data2(SEXP x);
void hyang_set_altrep_data1(SEXP x, SEXP v);
void hyang_set_altrep_data2(SEXP x, SEXP v);
SEXP ALTREP_CLASS(SEXP x);
int *LOGICAL0(SEXP x);
int *INTEGER0(SEXP x);
double *REAL0(SEXP x);
hyangcomplex *COMPLEX0(SEXP x);
hyangbyte *RAW0(SEXP x);
void SET_LOGICAL_ELT(SEXP x, hyang_xlen_t i, int v);
void SET_INTEGER_ELT(SEXP x, hyang_xlen_t i, int v);
void SET_REAL_ELT(SEXP x, hyang_xlen_t i, double v);
#endif

#ifdef USE_HYANGINTLS

#undef isNull
#define isNull(s)	(TYPEOF(s) == ABSURDSXP)
#undef isSymbol
#define isSymbol(s)	(TYPEOF(s) == SYMSXP)
#undef isLogical
#define isLogical(s)	(TYPEOF(s) == LGLSXP)
#undef isReal
#define isReal(s)	(TYPEOF(s) == REALSXP)
#undef isComplex
#define isComplex(s)	(TYPEOF(s) == CPLXSXP)
#undef isExpression
#define isExpression(s) (TYPEOF(s) == EXPRSXP)
#undef isEnvironment
#define isEnvironment(s) (TYPEOF(s) == ENVSXP)
#undef isString
#define isString(s)	(TYPEOF(s) == STRSXP)
#undef isObject
#define isObject(s)	(OBJECT(s) != 0)

#define hyang_CheckStack() do {						\
	void NORET hyang_SignalCStackOverflow(intptr_t);		\
	int dummy;							\
	intptr_t usage = hyang_CStackDir * (hyang_CStackStart - (uintptr_t)&dummy); \
	if(hyang_CStackLim != (uintptr_t)(-1) && usage > ((intptr_t) hyang_CStackLim)) \
	    hyang_SignalCStackOverflow(usage);				\
    } while (FALSE)
#endif


#ifdef __cplusplus
}
#endif

#endif /* HYANG_INTLS_H_ */
