/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
 
 
 

/*
 *  These functions are not part of the API.
 */
#ifndef PRTUTIL_H_
#define PRTUTIL_H_

#include <hyangintls.h> // for hyang_xlen_t
#include <hyangexts/hyangcomplex.h>

#define formatLogical      hyangly_formatLogical
#define formatInteger      hyangly_formatInt
#define formatReal         hyangly_formatReal
#define formatComplex      hyangly_formatCmplx
#define EncodeLogical      hyangly_EncodeLogical
#define EncodeInteger      hyangly_EncodeInt
#define EncodeReal         hyangly_EncodeReal
#define EncodeReal0        hyangly_EncodeReal0
#define EncodeComplex      hyangly_EncodeCmplx
#define vectorIndex        hyangly_vectIndex
#define printIntegerVector hyangly_prtIntVect
#define printRealVector    hyangly_prtRealVect
#define printComplexVector hyangly_prtCmplxVect

#ifdef  __cplusplus
extern "C" {
#endif

/* Computation of printing formats */
void formatLogical(const int *, hyang_xlen_t, int *);
void formatInteger(const int *, hyang_xlen_t, int *);
void formatReal(const double *, hyang_xlen_t, int *, int *, int *, int);
void formatComplex(const hyangcomplex *, hyang_xlen_t, int *, int *, int *, int *, int *, int *, int);

/* Formating of values */
const char *EncodeLogical(int, int);
const char *EncodeInteger(int, int);
const char *EncodeReal0(double, int, int, int, const char *);
const char *EncodeComplex(hyangcomplex, int, int, int, int, int, int, const char *);

/* Legacy, misused by packages RGtk2 and qtbase */
const char *EncodeReal(double, int, int, int, char);


/* Printing */
int	IndexWidth(hyang_xlen_t);
void VectorIndex(hyang_xlen_t, int);

//void printLogicalVector(int *, hyang_xlen_t, int);
void printIntegerVector(const int *, hyang_xlen_t, int);
void printRealVector   (const double *, hyang_xlen_t, int);
void printComplexVector(const hyangcomplex *, hyang_xlen_t, int);

#ifdef  __cplusplus
}
#endif

#endif /* PRTUTIL_H_ */
