/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */






/* Included by hyang.h: API */

#ifndef HYANG_ARITH_H_
#define HYANG_ARITH_H_

/* 
   This used to define _BSD_SOURCE to make declarations of isfinite
   and isnan visible in glibc.  But that was deprecated in glibc 2.20,
   and --std=c99 suffices nowadays.
*/

#include <hyangexts/hyangextlib.h>
#ifdef  __cplusplus
extern "C" {
#else
/* needed for isnan and isfinite, neither of which are used under C++ */
# include <math.h>
#endif

/* implementation of these : ../../main/arithmetic.c */
LibExtern double hyang_NaN;		/* IEEE NaN */
LibExtern double hyang_PosInf;	/* IEEE Inf */
LibExtern double hyang_NegInf;	/* IEEE -Inf */
LibExtern double hyang_NaReal;	/* NA_REAL: IEEE */
LibExtern int	 hyang_NaInt;	/* NA_INTEGER:= INT_MIN currently */
#ifdef __MAIN__
#undef extern
#undef LibExtern
#endif

#define NA_LOGICAL	hyang_NaInt
#define NA_INTEGER	hyang_NaInt
/* #define NA_FACTOR	hyang_NaInt  unused */
#define NA_REAL		hyang_NaReal
/* NA_STRING is a SEXP, so defined in hyangintls.h */

int hyang_IsNA(double);		/* True for Hyang's NA only */
int hyang_IsNaN(double);		/* True for special NaN, *not* for NA */
int hyang_finite(double);		/* True if none of NA, NaN, +/-Inf */
#define ISNA(x)	       hyang_IsNA(x)

/* ISNAN(): True for *both* NA and NaN.
   NOTE: some systems do not return 1 for TRUE.
   Also note that C++ math headers specifically undefine
   isnan if it is a macro (it is on macOS and in C99),
   hence the workaround.  This code also appears in hyangmath.h
*/
#ifdef __cplusplus
  int hyang_isnancpp(double); /* in arithmetic.c */
#  define ISNAN(x)     hyang_isnancpp(x)
#else
#  define ISNAN(x)     (isnan(x)!=0)
#endif

/* The following is only defined inside Hyang */
#ifdef HAVE_WORKING_ISFINITE
/* isfinite is defined in <math.h> according to C99 */
# define HYANG_FINITE(x)    isfinite(x)
#else
# define HYANG_FINITE(x)    hyang_finite(x)
#endif

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_ARITH_H_ */
