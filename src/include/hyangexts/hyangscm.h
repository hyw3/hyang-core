/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/* Included by hyang.h: API */



#ifndef HYANG_HYANGSCM_H
#define HYANG_HYANGSCM_H

#if defined(__cplusplus) && !defined(DO_NOT_USE_CXX_HEADERS)
# include <cstring>
# include <cstddef>
# define HYANG_SIZE_T std::size_t
#else
# include <string.h>		/* for memcpy, memset */
# include <stddef.h> /* for size_t */
# define HYANG_SIZE_T size_t
#endif

#include <hyangconfig.h>		/* for F77_APPEND_UNDERSCORE */

#ifdef  __cplusplus
extern "C" {
#endif

/* S Like Error Handling */

#include <hyangexts/hyangerrhand.h>	/* for error and warning */

#ifndef STRICT_HYANG_HEADERS

#define HYANG_PROBLEM_BUFSIZE	4096
/* Parentheses added for FC4 with gcc4 and -D_FORTIFY_SOURCE=2 */
#define PROBLEM			{char hyang_problem_buf[HYANG_PROBLEM_BUFSIZE];(sprintf)(hyang_problem_buf,
#define MESSAGE                 {char hyang_problem_buf[HYANG_PROBLEM_BUFSIZE];(sprintf)(hyang_problem_buf,
#define ERROR			),error(hyang_problem_buf);}
#define RECOVER(x)		),error(hyang_problem_buf);}
#define WARNING(x)		),warning(hyang_problem_buf);}
#define LOCAL_EVALUATOR		/**/
#define NULL_ENTRY		/**/
#define WARN			WARNING(NULL)

#endif

/* S Like Memory Management */

extern void *hyang_chk_calloc(HYANG_SIZE_T, HYANG_SIZE_T);
extern void *hyang_chk_realloc(void *, HYANG_SIZE_T);
extern void hyang_chk_free(void *);

#ifndef STRICT_HYANG_HEADERS
/* S-PLUS 3.x but not 5.x NULLs the pointer in the following */
#define Calloc(n, t)   (t *) hyang_chk_calloc( (HYANG_SIZE_T) (n), sizeof(t) )
#define Realloc(p,n,t) (t *) hyang_chk_realloc( (void *)(p), (HYANG_SIZE_T)((n) * sizeof(t)) )
#define Free(p)        (hyang_chk_free( (void *)(p) ), (p) = NULL)
#endif
#define hyang_Calloc(n, t)   (t *) hyang_chk_calloc( (HYANG_SIZE_T) (n), sizeof(t) )
#define hyang_Realloc(p,n,t) (t *) hyang_chk_realloc( (void *)(p), (HYANG_SIZE_T)((n) * sizeof(t)) )
#define hyang_Free(p)      (hyang_chk_free( (void *)(p) ), (p) = NULL)

#define Memcpy(p,q,n)  memcpy( p, q, (HYANG_SIZE_T)(n) * sizeof(*p) )

/* added for 3.0.0 */
#define Memzero(p,n)  memset(p, 0, (HYANG_SIZE_T)(n) * sizeof(*p))

#define CallocCharBuf(n) (char *) hyang_chk_calloc((HYANG_SIZE_T) ((n)+1), sizeof(char))

/* S Like Fortran Interface */
/* These may not be adequate everywhere. Convex had _ prepending common
   blocks, and some compilers may need to specify Fortran linkage */

#ifdef HAVE_F77_UNDERSCORE
# define F77_CALL(x)	x ## _
#else
# define F77_CALL(x)	x
#endif
#define F77_NAME(x)    F77_CALL(x)
#define F77_SUB(x)     F77_CALL(x)
#define F77_COM(x)     F77_CALL(x)
#define F77_COMDECL(x) F77_CALL(x)

#ifndef NO_CALL_HYANG
void	call_Hyang(char*, long, void**, char**, long*, char**, long, char**);
#endif

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_HYANGSCM_H */
