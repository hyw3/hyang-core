/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* 
   Not part of the API, subject to change at any time.
*/

#ifndef HYANG_CALLBACKS_H
#define HYANG_CALLBACKS_H

/**
  These structures are for C (and Hyang function) top-level task handlers.
  Such routines are called at the end of every (successful) top-level task
  in the regular REPL. 
 */

#include <hyangintls.h>
/**
  The signature of the C routine that a callback must implement.
  expr - the expression for the top-level task that was evaluated.
  value - the result of the top-level task, i.e. evaluating expr.
  succeeded - a logical value indicating whether the task completed propertly.
  visible - a logical value indicating whether the result was printed to the Hyang ``console''/stdout.
  data - user-level data passed to the registration routine.
 */
typedef hyangboolean (*hyang_ToplevelCallback)(SEXP expr, SEXP value, hyangboolean succeeded, hyangboolean visible, void *);

typedef struct _ToplevelCallback  hyang_ToplevelCallbackEl;
/** 
 Linked list element for storing the top-level task callbacks.
 */
struct _ToplevelCallback {
    hyang_ToplevelCallback cb; /* the C routine to call. */
    void *data;            /* the user-level data to pass to the call to cb() */
    void (*finalizer)(void *data); /* Called when the callback is removed. */

    char *name;  /* a name by which to identify this element. */ 

    hyang_ToplevelCallbackEl *next; /* the next element in the linked list. */
};

#ifdef __cplusplus
extern "C" {
#endif

hyangboolean hyangly_removeTaskCallbackByIndex(int id);
hyangboolean hyangly_removeTaskCallbackByName(const char *name);
SEXP hyang_removeTaskCallback(SEXP which);
hyang_ToplevelCallbackEl* hyangly_addTaskCallback(hyang_ToplevelCallback cb, void *data, void (*finalizer)(void *), const char *name, int *pos);



/*
  The following definitions are for callbacks to Hyang functions and
  methods related to user-level tables.  This was implemented in a
  separate package on Omegahat and these declarations allow the package
  to interface to the internal Hyang code.
  
  
  
*/

typedef struct  _hyang_ObjTbl hyang_ObjTbl;

/* Do we actually need the exists() since it is never called but Hyang
   uses get to see if the symbol is bound to anything? */
typedef hyangboolean (*hyangdb_exists)(const char * const name, hyangboolean *canCache, hyang_ObjTbl *);
typedef SEXP     (*hyangdb_get)(const char * const name, hyangboolean *canCache, hyang_ObjTbl *);
typedef int      (*hyangdb_remove)(const char * const name, hyang_ObjTbl *);
typedef SEXP     (*hyangdb_assign)(const char * const name, SEXP value, hyang_ObjTbl *);
typedef SEXP     (*hyangdb_objs)(hyang_ObjTbl *);
typedef hyangboolean (*hyangdb_canCache)(const char * const name, hyang_ObjTbl *);

typedef void     (*hyangdb_onDetach)(hyang_ObjTbl *);
typedef void     (*hyangdb_onAttach)(hyang_ObjTbl *);

struct  _hyang_ObjTbl{
  int       type;
  char    **cachedNames;
  hyangboolean  active;

  hyangdb_exists   exists;
  hyangdb_get      get;
  hyangdb_remove   remove;
  hyangdb_assign   assign;
  hyangdb_objs  objects;
  hyangdb_canCache canCache;

  hyangdb_onDetach onDetach;
  hyangdb_onAttach onAttach;
  
  void     *privateData;
};


#ifdef __cplusplus
}
#endif

#endif /* HYANG_CALLBACKS_H */
