/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/*
  Experimental: included by src/library/datasci/src/distance.c

  Note that only uses hyang_NumOfMathThreads: it is not clear
  hyang_NumOfMathThreads should be exposed at all.

  This is not used currently on Windows, where hyang_NumOfMathThreads
  used not to be exposed.
*/

#ifndef HYANG_MATHJUNCTURE_H_
#define HYANG_MATHJUNCTURE_H_

#ifdef  __cplusplus
extern "C" {
#endif

#include <hyangexts/hyangextlib.h>
LibExtern int hyang_NumOfMathThreads;
LibExtern int hyang_max_NumOfMathThreads;

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_MATHJUNCTURE_H_ */
