/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
  C functions to be called from alternative front-ends.

  Part of the API for such front-ends but not for packages.
*/

#ifndef HYANG_EXT_STARTUP_H_
#define HYANG_EXT_STARTUP_H_

#if defined(__cplusplus) && !defined(DO_NOT_USE_CXX_HEADERS)
# include <cstddef>
# define HYANG_SIZE_T std::size_t
#else
# include <stddef.h> /* for size_t */
# define HYANG_SIZE_T size_t
#endif

#include <hyangexts/hyangboolean.h>	/* TRUE/FALSE */

#ifdef __cplusplus
extern "C" {
#endif

#ifdef Win32
typedef int (*blah1) (const char *, char *, int, int);
typedef void (*blah2) (const char *, int);
typedef void (*blah3) (void);
typedef void (*blah4) (const char *);
/* Return value here is expected to be 1 for Yes, -1 for No and 0 for Cancel:
   symbolic constants in graphapp.h */
typedef int (*blah5) (const char *);
typedef void (*blah6) (int);
typedef void (*blah7) (const char *, int, int);
typedef enum {hyangGui, hyangTerm, LinkDLL} UImode;
#endif

/* Startup Actions */
typedef enum {
    SA_NORESTORE,/* = 0 */
    SA_RESTORE,
    SA_DEFAULT,/* was === SA_RESTORE */
    SA_NOSAVE,
    SA_SAVE,
    SA_SAVEASK,
    SA_SUICIDE
} SA_TYPE;

typedef struct
{
    hyangboolean hyang_Quiet;
    hyangboolean hyang_Slave;
    hyangboolean hyang_Interactive;
    hyangboolean hyang_Verbose;
    hyangboolean LoadSiteFile;
    hyangboolean LoadInitFile;
    hyangboolean DebugInitFile;
    SA_TYPE RestoreAction;
    SA_TYPE SaveAction;
    HYANG_SIZE_T vsize;
    HYANG_SIZE_T nsize;
    HYANG_SIZE_T max_vsize;
    HYANG_SIZE_T max_nsize;
    HYANG_SIZE_T ppsize;
    int Nohyangenviron;

#ifdef Win32
    char *hyanghome;               /* HYANG_HOME */
    char *home;                /* HOME  */
    blah1 ReadConsole;
    blah2 WriteConsole;
    blah3 CallBack;
    blah4 ShowMessage;
    blah5 YesNoCancel;
    blah6 Busy;
    UImode CharacterMode;
    blah7 WriteConsoleEx; /* used only if WriteConsole is NULL */
#endif
} structHyangStart;

typedef structHyangStart *hyangstart;

void hyang_DefParams(hyangstart);
void hyang_SetParams(hyangstart);
void hyang_SetWin32(hyangstart);
void hyang_SizeFromEnv(hyangstart);
void hyang_common_command_line(int *, char **, hyangstart);

void hyang_setCmdLineArgs(int argc, char **argv);

void hyang_MainloopSetup(void); // also in hyangmbd.h

#ifdef __cplusplus
}
#endif

#endif
