/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* 
   For use by alternative front-ends and packages which need to share
   the Hyang event loop (on Unix-alikes).

   Not part of the API and subject to change without notice.

   NB: HAVE_SYS_SELECT_H should be checked and defined before this is
   included.
 */

#ifndef HYANG_EXT_EVENTLOOP_H
#define HYANG_EXT_EVENTLOOP_H

#ifdef HAVE_SYS_SELECT_H
# include <sys/select.h> /* for fd_set, select according to POSIX 2004 */
#endif
#ifdef HAVE_SYS_TIME_H
# include <sys/time.h>	 /* ... according to earlier POSIX and perhaps HP-UX */
#endif
/* NOTE: At one time needed on FreeBSD so that fd_set is defined. */
#include <sys/types.h>

#ifdef  __cplusplus
extern "C" {
#endif

#define XActivity 1
#define StdinActivity 2

typedef void (*InputHandlerProc)(void *userData); 

typedef struct _InputHandler {

  int activity;
  int fileDescriptor;
  InputHandlerProc handler;

  struct _InputHandler *next;

    /* Whether we should be listening to this file descriptor or not. */
  int active;

    /* Data that can be passed to the routine as its only argument.
       This might be a user-level function or closure when we implement
       a callback to Hyang mechanism. 
     */
  void *userData;

} InputHandler;


extern InputHandler *initStdinHandler(void);
extern void consoleInputHandler(unsigned char *buf, int len);

extern InputHandler *addInputHandler(InputHandler *handlers, int fd, InputHandlerProc handler, int activity);
extern InputHandler *getInputHandler(InputHandler *handlers, int fd);
extern int           removeInputHandler(InputHandler **handlers, InputHandler *it);
extern InputHandler *getSelectedHandler(InputHandler *handlers, fd_set *mask);
extern fd_set *hyang_checkActivity(int usec, int ignore_stdin);
extern fd_set *hyang_checkActivityEx(int usec, int ignore_stdin, void (*intr)(void));
extern void hyang_runHandlers(InputHandler *handlers, fd_set *mask);

extern int hyang_SelectEx(int  n,  fd_set  *readfds,  fd_set  *writefds,
		      fd_set *exceptfds, struct timeval *timeout,
		      void (*intr)(void));

#ifdef __SYSTEM__
#ifndef __cplusplus   /* Would get duplicate conflicting symbols*/
InputHandler *hyang_InputHandlers;
#endif
#else
extern InputHandler *hyang_InputHandlers;
#endif

extern void (* hyang_PolledEvents)(void);
extern int hyang_waitUsec;

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_EXT_EVENTLOOP_H */
