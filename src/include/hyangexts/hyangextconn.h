/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* This header is not part of the Hyang API (see 'Writing Hyang Extensions') */

#ifndef HYANG_EXTCONN_H_
#define HYANG_EXTCONN_H_

#include <hyangexts/hyangboolean.h>

#if defined(__cplusplus) && !defined(DO_NOT_USE_CXX_HEADERS)
# include <cstddef>
# include <cstdarg>
#else
# include <stddef.h> /* for size_t */
# include <stdarg.h> /* for va_list */
#endif

/* IMPORTANT: we do not expect future connection APIs to be
   backward-compatible so if you use this, you *must* check the
   version and proceed only if it matches what you expect.

   We explicitly reserve the right to change the connection
   implementation without a compatibility layer.
 */
#define HYANG_CONNECTIONS_VERSION 1

/* this allows the opaque pointer definition to be made available 
   in hyangintls.h */
#ifndef HAVE_HYANGCONNECTION_TYPEDEF
typedef struct HyangConn  *hyangconn;
#endif
struct HyangConn {
    char* class;
    char* description;
    int enc; /* the encoding of 'description' */
    char mode[5];
    hyangboolean text, isopen, incomplete, canread, canwrite, canseek, blocking, 
	isGzcon;
    hyangboolean (*open)(struct HyangConn *);
    void (*close)(struct HyangConn *); /* routine closing after auto open */
    void (*destroy)(struct HyangConn *); /* when closing connection */
    int (*vfprintf)(struct HyangConn *, const char *, va_list);
    int (*fgetc)(struct HyangConn *);
    int (*fgetc_internal)(struct HyangConn *);
    double (*seek)(struct HyangConn *, double, int, int);
    void (*truncate)(struct HyangConn *);
    int (*fflush)(struct HyangConn *);
    size_t (*read)(void *, size_t, size_t, struct HyangConn *);
    size_t (*write)(const void *, size_t, size_t, struct HyangConn *);
    int nPushBack, posPushBack; /* number of lines, position on top line */
    char **PushBack;
    int save, save2;
    char encname[101];
    /* will be iconv_t, which is a pointer. NULL if not in use */
    void *inconv, *outconv;
    /* The idea here is that no MBCS char will ever not fit */
    char iconvbuff[25], oconvbuff[50], *next, init_out[25];
    short navail, inavail;
    hyangboolean EOF_signalled;
    hyangboolean UTF8out;
    void *id;
    void *ex_ptr;
    void *private;
    int status; /* for pipes etc */
    unsigned char *buff;
    size_t buff_len, buff_stored_len, buff_pos;
};

#ifdef  __cplusplus
extern "C" {
#endif

SEXP   hyang_NewCustomConn(const char *description, const char *mode, const char *class_name, hyangconn *ptr);
size_t hyang_ReadConn(hyangconn con, void *buf, size_t n);
size_t hyang_WriteConn(hyangconn con, void *buf, size_t n);
hyangconn hyang_GetConn(SEXP sConn);

#ifdef  __cplusplus
}
#endif

#endif
