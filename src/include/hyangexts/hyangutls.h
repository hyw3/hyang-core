/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* Generally useful  UTILITIES  *NOT* relying on Hyang internals (from hyangdefn.h)
 */

/* Included by hyang.h: API */

#ifndef HYANG_EXT_UTILS_H_
#define HYANG_EXT_UTILS_H_

#include <hyangexts/hyangboolean.h>
#include <hyangexts/hyangcomplex.h>

#if defined(__cplusplus) && !defined(DO_NOT_USE_CXX_HEADERS)
# include <cstddef>
# define HYANG_SIZE_T std::size_t
#else
# include <stddef.h>
# define HYANG_SIZE_T size_t
#endif

#define revsort       hyangly_revsort
#define iPsort        hyangly_iPsort
#define hyangPsort        hyangly_rPsort
#define cPsort        hyangly_cPsort
#define IndexWidth    hyangly_IndexWidth
#define setIVector    hyangly_setIVect
#define setRVector    hyangly_setRVect
#define StringFalse   hyangly_StrFalse
#define StringTrue    hyangly_StrTrue
#define isBlankString hyangly_isBlankString

#ifdef  __cplusplus
extern "C" {
#endif

/* ../../main/sort.c : */
void	hyang_isort(int*, int);
void	hyang_hyangsort(double*, int);
void	hyang_csort(hyangcomplex*, int);
void    rsort_with_index(double *, int *, int);
void	revsort(double*, int*, int);/* reverse; sort i[] alongside */
void	iPsort(int*,    int, int);
void	hyangPsort(double*, int, int);
void	cPsort(hyangcomplex*, int, int);

/* ../../main/qsort.c : */
/* dummy renamed to II to avoid problems with g++ on Solaris */
void hyang_qsort    (double *v,         HYANG_SIZE_T i, HYANG_SIZE_T j);
void hyang_qsort_I  (double *v, int *II, int i, int j);
void hyang_qsort_int  (int *iv,         HYANG_SIZE_T i, HYANG_SIZE_T j);
void hyang_qsort_int_I(int *iv, int *II, int i, int j);
#ifdef HYANG_HYANGSCM_H
void F77_NAME(qsort4)(double *v, int *indx, int *ii, int *jj);
void F77_NAME(qsort3)(double *v,            int *ii, int *jj);
#endif

/* ../../main/util.c  and others : */
const char *hyang_ExpandFileName(const char *);
#ifdef Win32
const char *hyang_ExpandFileNameUTF8(const char *);
#endif
void	setIVector(int*, int, int);
void	setRVector(double*, int, double);
hyangboolean StringFalse(const char *);
hyangboolean StringTrue(const char *);
hyangboolean isBlankString(const char *);

/* These two are guaranteed to use '.' as the decimal point,
   and to accept "NA".
 */
double hyang_atof(const char *str);
double hyang_strtod(const char *c, char **end);

char *hyang_tmpnam(const char *prefix, const char *tempdir);
char *hyang_tmpnam2(const char *prefix, const char *tempdir, const char *fileext);

void hyang_CheckUsrInterrupt(void);
void hyang_CheckStack(void);
void hyang_CheckStack2(HYANG_SIZE_T);


/* ../../appl/interv.c: also in hyangapproutine.h */
int findInterval(double *xt, int n, double x,
		 hyangboolean rightmost_closed,  hyangboolean all_inside, int ilo,
		 int *mflag);
int findInterval2(double *xt, int n, double x,
		  hyangboolean rightmost_closed,  hyangboolean all_inside, hyangboolean left_open,
		  int ilo, int *mflag);
#ifdef HYANG_HYANGSCM_H
int F77_SUB(interv)(double *xt, int *n, double *x,
		    hyangboolean *rightmost_closed, hyangboolean *all_inside,
		    int *ilo, int *mflag);
#endif
void find_interv_vec(double *xt, int *n,	double *x,   int *nx,
		     int *rightmost_closed, int *all_inside, int *indx);

/* ../../appl/maxcol.c: also in hyangapproutine.h */
void hyang_max_col(double *matrix, int *nr, int *nc, int *maxes, int *ties_meth);

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_EXT_UTILS_H_ */
