/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/*
   C declarations of BLAS Fortran subroutines always available in Hyang.

   Part of the API.

   Hyang packages that use these should have PKG_LIBS in src/Makevars include
   $(BLAS_LIBS) $(FLIBS)
 */


/* Part of the API */


#ifndef HYANG_BLAS_H
#define HYANG_BLAS_H

#include <hyangexts/hyangscm.h>		/* for F77_... */
#include <hyangexts/hyangcomplex.h>	/* for hyangcomplex */

#ifdef  __cplusplus
extern "C" {
#endif

// never defined in Hyang itself.
#ifndef BLAS_extern
#define BLAS_extern extern
#endif

/* Double Precision Level 1 BLAS */

BLAS_extern double /* DASUM - sum of absolute values of a one-dimensional array */
F77_NAME(dasum)(const int *n, const double *dx, const int *incx);
BLAS_extern void   /* DAXPY - replace y by alpha*x + y */
F77_NAME(daxpy)(const int *n, const double *alpha,
		const double *dx, const int *incx,
		double *dy, const int *incy);
BLAS_extern void   /* DCOPY - copy x to y */
F77_NAME(dcopy)(const int *n, const double *dx, const int *incx,
		double *dy, const int *incy);
BLAS_extern double /* DDOT - inner product of x and y */
F77_NAME(ddot)(const int *n, const double *dx, const int *incx,
	       const double *dy, const int *incy);
BLAS_extern double /* DNRM2 - 2-norm of a vector */
F77_NAME(dnrm2)(const int *n, const double *dx, const int *incx);
BLAS_extern void   /* DROT - apply a Given's rotation */
F77_NAME(drot)(const int *n, double *dx, const int *incx,
	       double *dy, const int *incy, const double *c, const double *s);
BLAS_extern void   /* DROTG - generate a Given's rotation */
F77_NAME(drotg)(const double *a, const double *b, double *c, double *s);
BLAS_extern void   /* DROTM - apply a modified Given's rotation */
F77_NAME(drotm)(const int *n, double *dx, const int *incx,
		double *dy, const int *incy, const double *dparam);
BLAS_extern void   /* DROTMG - generate a modified Given's rotation */
F77_NAME(drotmg)(const double *dd1, const double *dd2, const double *dx1,
		 const double *dy1, double *param);
BLAS_extern void   /* DSCAL - scale a one-dimensional array */
F77_NAME(dscal)(const int *n, const double *alpha, double *dx, const int *incx);
BLAS_extern void   /* DSWAP - interchange one-dimensional arrays */
F77_NAME(dswap)(const int *n, double *dx, const int *incx,
		double *dy, const int *incy);
BLAS_extern int    /* IDAMAX - return the index of the element with max abs value */
F77_NAME(idamax)(const int *n, const double *dx, const int *incx);

/* Double Precision Level 2 BLAS */

/* DGBMV - perform one of the matrix-vector operations */
/* y := alpha*A*x + beta*y, or y := alpha*A'*x + beta*y, */
BLAS_extern void
F77_NAME(dgbmv)(const char *trans, const int *m, const int *n,
		const int *kl,const int *ku,
		const double *alpha, const double *a, const int *lda,
		const double *x, const int *incx,
		const double *beta, double *y, const int *incy);
/* DGEMV - perform one of the matrix-vector operations */
/* y := alpha*A*x + beta*y, or y := alpha*A'*x + beta*y,  */
BLAS_extern void
F77_NAME(dgemv)(const char *trans, const int *m, const int *n,
		const double *alpha, const double *a, const int *lda,
		const double *x, const int *incx, const double *beta,
		double *y, const int *incy);
/* DSBMV - perform the matrix-vector operation */
/* y := alpha*A*x + beta*y, */
BLAS_extern void
F77_NAME(dsbmv)(const char *uplo, const int *n, const int *k,
		const double *alpha, const double *a, const int *lda,
		const double *x, const int *incx,
		const double *beta, double *y, const int *incy);
/* DSPMV - perform the matrix-vector operation */
/* y := alpha*A*x + beta*y, */
BLAS_extern void
F77_NAME(dspmv)(const char *uplo, const int *n,
		const double *alpha, const double *ap,
		const double *x, const int *incx,
		const double *beta, double *y, const int *incy);

/* DSYMV - perform the matrix-vector operation */
/*  y := alpha*A*x + beta*y, */
BLAS_extern void
F77_NAME(dsymv)(const char *uplo, const int *n, const double *alpha,
		const double *a, const int *lda,
		const double *x, const int *incx,
		const double *beta, double *y, const int *incy);
/* DTBMV - perform one of the matrix-vector operations */
/* x := A*x, or x := A'*x, */
BLAS_extern void
F77_NAME(dtbmv)(const char *uplo, const char *trans,
		const char *diag, const int *n, const int *k,
		const double *a, const int *lda,
		double *x, const int *incx);
/* DTPMV - perform one of the matrix-vector operations */
/* x := A*x, or x := A'*x, */
BLAS_extern void
F77_NAME(dtpmv)(const char *uplo, const char *trans, const char *diag,
		const int *n, const double *ap,
		double *x, const int *incx);
/* DTRMV - perform one of the matrix-vector operations  */
/* x := A*x, or x := A'*x, */
BLAS_extern void
F77_NAME(dtrmv)(const char *uplo, const char *trans, const char *diag,
		const int *n, const double *a, const int *lda,
		double *x, const int *incx);
/* DTBSV - solve one of the systems of equations */
/* A*x = b, or A'*x = b, */
BLAS_extern void
F77_NAME(dtbsv)(const char *uplo, const char *trans,
		const char *diag, const int *n, const int *k,
		const double *a, const int *lda,
		double *x, const int *incx);
/* DTPSV - solve one of the systems of equations */
/* A*x = b, or A'*x = b, */
BLAS_extern void
F77_NAME(dtpsv)(const char *uplo, const char *trans,
		const char *diag, const int *n,
		const double *ap, double *x, const int *incx);
/* DTRSV - solve one of the systems of equations */
/* A*x = b, or A'*x = b, */
BLAS_extern void
F77_NAME(dtrsv)(const char *uplo, const char *trans,
		const char *diag, const int *n,
		const double *a, const int *lda,
		double *x, const int *incx);
/* DGER - perform the rank 1 operation   A := alpha*x*y' + A */
BLAS_extern void
F77_NAME(dger)(const int *m, const int *n, const double *alpha,
	       const double *x, const int *incx,
	       const double *y, const int *incy,
	       double *a, const int *lda);
/* DSYR - perform the symmetric rank 1 operation A := alpha*x*x' + A */
BLAS_extern void
F77_NAME(dsyr)(const char *uplo, const int *n, const double *alpha,
	       const double *x, const int *incx,
	       double *a, const int *lda);
/* DSPR - perform the symmetric rank 1 operation A := alpha*x*x' + A */
BLAS_extern void
F77_NAME(dspr)(const char *uplo, const int *n, const double *alpha,
	       const double *x, const int *incx, double *ap);
/* DSYR2 - perform the symmetric rank 2 operation */
/* A := alpha*x*y' + alpha*y*x' + A, */
BLAS_extern void
F77_NAME(dsyr2)(const char *uplo, const int *n, const double *alpha,
		const double *x, const int *incx,
		const double *y, const int *incy,
		double *a, const int *lda);
/* DSPR2 - perform the symmetric rank 2 operation */
/* A := alpha*x*y' + alpha*y*x' + A,  */
BLAS_extern void
F77_NAME(dspr2)(const char *uplo, const int *n, const double *alpha,
		const double *x, const int *incx,
		const double *y, const int *incy, double *ap);

/* Double Precision Level 3 BLAS */

/* DGEMM - perform one of the matrix-matrix operations    */
/* C := alpha*op( A )*op( B ) + beta*C */
BLAS_extern void
F77_NAME(dgemm)(const char *transa, const char *transb, const int *m,
		const int *n, const int *k, const double *alpha,
		const double *a, const int *lda,
		const double *b, const int *ldb,
		const double *beta, double *c, const int *ldc);
/* DTRSM - solve one of the matrix equations  */
/* op(A)*X = alpha*B, or  X*op(A) = alpha*B  */
BLAS_extern void
F77_NAME(dtrsm)(const char *side, const char *uplo,
		const char *transa, const char *diag,
		const int *m, const int *n, const double *alpha,
		const double *a, const int *lda,
		double *b, const int *ldb);
/* DTRMM - perform one of the matrix-matrix operations */
/* B := alpha*op( A )*B, or B := alpha*B*op( A ) */
BLAS_extern void
F77_NAME(dtrmm)(const char *side, const char *uplo, const char *transa,
		const char *diag, const int *m, const int *n,
		const double *alpha, const double *a, const int *lda,
		double *b, const int *ldb);
/* DSYMM - perform one of the matrix-matrix operations   */
/*  C := alpha*A*B + beta*C, */
BLAS_extern void
F77_NAME(dsymm)(const char *side, const char *uplo, const int *m,
		const int *n, const double *alpha,
		const double *a, const int *lda,
		const double *b, const int *ldb,
		const double *beta, double *c, const int *ldc);
/* DSYRK - perform one of the symmetric rank k operations */
/* C := alpha*A*A' + beta*C or C := alpha*A'*A + beta*C */
BLAS_extern void
F77_NAME(dsyrk)(const char *uplo, const char *trans,
		const int *n, const int *k,
		const double *alpha, const double *a, const int *lda,
		const double *beta, double *c, const int *ldc);
/* DSYR2K - perform one of the symmetric rank 2k operations */
/* C := alpha*A*B' + alpha*B*A' + beta*C or */
/* C := alpha*A'*B + alpha*B'*A + beta*C */
BLAS_extern void
F77_NAME(dsyr2k)(const char *uplo, const char *trans,
		 const int *n, const int *k,
		 const double *alpha, const double *a, const int *lda,
		 const double *b, const int *ldb,
		 const double *beta, double *c, const int *ldc);
/*
  LSAME is a LAPACK support routine, not part of BLAS
*/

/* Double complex BLAS routines added for 2.3.0 */
/* #ifdef HAVE_FORTRAN_DOUBLE_COMPLEX */
    BLAS_extern double
    F77_NAME(dcabs1)(const double *z);
    BLAS_extern double
    F77_NAME(dzasum)(const int *n, const hyangcomplex *zx, const int *incx);
    BLAS_extern double
    F77_NAME(dznrm2)(const int *n, const hyangcomplex *x, const int *incx);
    BLAS_extern int
    F77_NAME(izamax)(const int *n, const hyangcomplex *zx, const int *incx);
    BLAS_extern void
    F77_NAME(zaxpy)(const int *n, const hyangcomplex *za, const hyangcomplex *zx,
		    const int *incx, const hyangcomplex *zy, const int *incy);
    BLAS_extern void
    F77_NAME(zcopy)(const int *n, const hyangcomplex *zx, const int *incx,
		    const hyangcomplex *zy, const int *incy);

    /* WARNING!  The next two return a value that may not be
       compatible between C and Fortran, and even if it is, this might
       not be the right translation to C.  Only use after
       configure-testing with your compilers.
     */
    BLAS_extern hyangcomplex
    F77_NAME(zdotc)(const int *n,
		    const hyangcomplex *zx, const int *incx, 
		    const hyangcomplex *zy, const int *incy);
    BLAS_extern hyangcomplex
    F77_NAME(zdotu)(const int *n,
		    const hyangcomplex *zx, const int *incx,
		    const hyangcomplex *zy, const int *incy);

    BLAS_extern void
    F77_NAME(zdrot)(const int *n, 
		    const hyangcomplex *zx, const int *incx, 
		    hyangcomplex *zy, const int *incy, 
		    const double *c, const double *s);
    BLAS_extern void
    F77_NAME(zdscal)(const int *n, const double *da, 
		     hyangcomplex *zx, const int *incx);
    BLAS_extern void
    F77_NAME(zgbmv)(const char *trans, int *m, int *n, int *kl,
		    int *ku, hyangcomplex *alpha, hyangcomplex *a, int *lda,
		    hyangcomplex *x, int *incx, hyangcomplex *beta, hyangcomplex *y,
		    int *incy);
    BLAS_extern void
    F77_NAME(zgemm)(const char *transa, const char *transb, const int *m,
		    const int *n, const int *k, const hyangcomplex *alpha,
		    const hyangcomplex *a, const int *lda,
		    const hyangcomplex *b, const int *ldb,
		    const hyangcomplex *beta, hyangcomplex *c, const int *ldc);
    BLAS_extern void
    F77_NAME(zgemv)(const char *trans, const int *m, const int *n,
		    const hyangcomplex *alpha, const hyangcomplex *a, const int *lda,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *beta,
		    hyangcomplex *y, const int *incy);
    BLAS_extern void
    F77_NAME(zgerc)(const int *m, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *y,
		    const int *incy, hyangcomplex *a, const int *lda);
    BLAS_extern void
    F77_NAME(zgeru)(const int *m, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *y,
		    const int *incy, hyangcomplex *a, const int *lda);
    BLAS_extern void
    F77_NAME(zhbmv)(const char *uplo, const int *n, const int *k,
		    const hyangcomplex *alpha, const hyangcomplex *a, const int *lda,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *beta,
		    hyangcomplex *y, const int *incy);
    BLAS_extern void
    F77_NAME(zhemm)(const char *side, const char *uplo, const int *m,
		    const int *n, const hyangcomplex *alpha, const hyangcomplex *a,
		    const int *lda, const hyangcomplex *b, const int *ldb,
		    const hyangcomplex *beta, hyangcomplex *c, const int *ldc);
    BLAS_extern void
    F77_NAME(zhemv)(const char *uplo, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *a, const int *lda, const hyangcomplex *x,
		    const int *incx, const hyangcomplex *beta,
		    hyangcomplex *y, const int *incy);
    BLAS_extern void
    F77_NAME(zher)(const char *uplo, const int *n, const double *alpha,
		   const hyangcomplex *x, const int *incx, hyangcomplex *a,
		   const int *lda);
    BLAS_extern void
    F77_NAME(zher2)(const char *uplo, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *y,
		    const int *incy, hyangcomplex *a, const int *lda);
    BLAS_extern void
    F77_NAME(zher2k)(const char *uplo, const char *trans, const int *n,
		     const int *k, const hyangcomplex *alpha, const hyangcomplex *a,
		     const int *lda, const hyangcomplex *b, const  int *ldb,
		     const double *beta, hyangcomplex *c, const int *ldc);
    BLAS_extern void
    F77_NAME(zherk)(const char *uplo, const char *trans, const int *n,
		    const int *k, const double *alpha, const hyangcomplex *a,
		    const int *lda, const double *beta, hyangcomplex *c,
		    const int *ldc);
    BLAS_extern void
    F77_NAME(zhpmv)(const char *uplo, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *ap, const hyangcomplex *x, const int *incx,
		    const hyangcomplex * beta, hyangcomplex *y, const int *incy);
    BLAS_extern void
    F77_NAME(zhpr)(const char *uplo, const int *n, const double *alpha,
		   const hyangcomplex *x, const int *incx, hyangcomplex *ap);
    BLAS_extern void
    F77_NAME(zhpr2)(const char *uplo, const int *n, const hyangcomplex *alpha,
		    const hyangcomplex *x, const int *incx, const hyangcomplex *y,
		    const int *incy, hyangcomplex *ap);
    BLAS_extern void
    F77_NAME(zrotg)(const hyangcomplex *ca, const hyangcomplex *cb, 
		    double *c, hyangcomplex *s);
    BLAS_extern void
    F77_NAME(zscal)(const int *n, const hyangcomplex *za, hyangcomplex *zx,
		    const int *incx);
    BLAS_extern void
    F77_NAME(zswap)(const int *n, hyangcomplex *zx, const int *incx,
		    hyangcomplex *zy, const int *incy);
    BLAS_extern void
    F77_NAME(zsymm)(const char *side, const char *uplo, const int *m,
		    const int *n, const hyangcomplex *alpha, const hyangcomplex *a,
		    const int *lda, const hyangcomplex *b, const int *ldb,
		    const hyangcomplex *beta, hyangcomplex *c, const int *ldc);
    BLAS_extern void
    F77_NAME(zsyr2k)(const char *uplo, const char *trans, int *n, int *k,
		     hyangcomplex *alpha, hyangcomplex *a, int *lda, hyangcomplex *b,
		     int *ldb, hyangcomplex *beta, hyangcomplex *c, int *ldc);
    BLAS_extern void
    F77_NAME(zsyrk)(const char *uplo, const char *trans, const  int *n,
		    const int *k, const hyangcomplex *alpha, const hyangcomplex *a,
		    const int *lda, const hyangcomplex *beta, hyangcomplex *c,
		    const int *ldc);
    BLAS_extern void
    F77_NAME(ztbmv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const int *k, const hyangcomplex *a,
		    const int *lda, hyangcomplex *x, const int *incx);
    BLAS_extern void
    F77_NAME(ztbsv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const int *k, const hyangcomplex *a,
		    const int *lda, hyangcomplex *x, const int *incx);
    BLAS_extern void
    F77_NAME(ztpmv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const hyangcomplex *ap, hyangcomplex *x,
		    const int *incx);
    BLAS_extern void
    F77_NAME(ztpsv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const hyangcomplex *ap, hyangcomplex *x,
		    const int *incx);
    BLAS_extern void
    F77_NAME(ztrmm)(const char *side, const char *uplo, const char *transa,
		    const char *diag, const int *m, const int *n,
		    const hyangcomplex *alpha, const hyangcomplex *a,
		    const int *lda, hyangcomplex *b, const int *ldb);
    BLAS_extern void
    F77_NAME(ztrmv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const hyangcomplex *a, const int *lda,
		    hyangcomplex *x, const int *incx);
    BLAS_extern void
    F77_NAME(ztrsm)(const char *side, const char *uplo, const char *transa,
		    const char *diag, int *m, int *n, hyangcomplex *alpha,
		    hyangcomplex *a, int *lda, hyangcomplex *b, int *ldb);
    BLAS_extern void
    F77_NAME(ztrsv)(const char *uplo, const char *trans, const char *diag,
		    const int *n, const hyangcomplex *a, const int *lda,
		    hyangcomplex *x, const int *incx);
/* #endif */

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_BLAS_H */
