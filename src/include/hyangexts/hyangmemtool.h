/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/*  Definition of the hyang_allocator_t structure for custom allocators
 *  to be used with allocVector3()
 */

#ifndef HYANG_MEMTOOL_H_
#define HYANG_MEMTOOL_H_

#if defined(__cplusplus) && !defined(DO_NOT_USE_CXX_HEADERS)
# include <cstddef>
#else
# include <stddef.h> /* for size_t */
#endif

/* hyang_allocator_t typedef is also declared in hyangintls.h 
   so we guard against random inclusion order */
#ifndef HYANG_ALLOCATOR_TYPE
#define HYANG_ALLOCATOR_TYPE
typedef struct hyang_allocator hyang_allocator_t;
#endif

typedef void *(*custom_alloc_t)(hyang_allocator_t *allocator, size_t);
typedef void  (*custom_free_t)(hyang_allocator_t *allocator, void *);

struct hyang_allocator {
    custom_alloc_t mem_alloc; /* malloc equivalent */
    custom_free_t  mem_free;  /* free equivalent */
    void *res;                /* reserved (maybe for copy) - must be NULL */
    void *data;               /* custom data for the allocator implementation */
};

#endif /* HYANG_MEMTOOL_H_ */
