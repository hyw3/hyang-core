/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
 
#ifndef HYANG_EXTALTREP_H_
#define HYANG_EXTALTREP_H_

#define STRUCT_SUBTYPES
#ifdef STRUCT_SUBTYPES
# define HYANG_SEXP(x) (x).ptr
# define HYANG_SUBTYPE_INIT(x) { x }
  typedef struct { SEXP ptr; } hyang_altrep_class_t;
#else
# define HYANG_SEXP(x) ((SEXP) (x))
# define HYANG_SUBTYPE_INIT(x) (void *) (x)
  typedef struct hyang_altcls *hyang_altrep_class_t;
#endif

SEXP
hyang_new_altrep(hyang_altrep_class_t class, SEXP data1, SEXP data2);

hyang_altrep_class_t
hyang_make_altstring_class(const char *cname, const char *pname, DllInfo *info);
hyang_altrep_class_t
hyang_make_altinteger_class(const char *cname, const char *pname, DllInfo *info);
hyang_altrep_class_t
hyang_make_altreal_class(const char *cname, const char *pname, DllInfo *info);
hyangboolean hyang_altrep_inherits(SEXP x, hyang_altrep_class_t);

typedef SEXP (*hyang_altrep_UnserializeEX_method_t)(SEXP, SEXP, SEXP, int, int);
typedef SEXP (*hyang_altrep_Unserialize_method_t)(SEXP, SEXP);
typedef SEXP (*hyang_altrep_Serialized_state_method_t)(SEXP);
typedef SEXP (*hyang_altrep_DuplicateEX_method_t)(SEXP, hyangboolean);
typedef SEXP (*hyang_altrep_Duplicate_method_t)(SEXP, hyangboolean);
typedef SEXP (*hyang_altrep_Coerce_method_t)(SEXP, int);
typedef hyangboolean (*hyang_altrep_Inspect_method_t)(SEXP, int, int, int,
					      void (*)(SEXP, int, int, int));
typedef hyang_xlen_t (*hyang_altrep_Length_method_t)(SEXP);

typedef void *(*hyang_altvec_Dataptr_method_t)(SEXP, hyangboolean);
typedef const void *(*hyang_altvec_Dataptr_or_null_method_t)(SEXP);
typedef SEXP (*hyang_altvec_Extract_subset_method_t)(SEXP, SEXP, SEXP);

typedef int (*hyang_altinteger_Elt_method_t)(SEXP, hyang_xlen_t);
typedef hyang_xlen_t
(*hyang_altinteger_Get_region_method_t)(SEXP, hyang_xlen_t, hyang_xlen_t, int *);
typedef int (*hyang_altinteger_Is_sorted_method_t)(SEXP);
typedef int (*hyang_altinteger_No_NA_method_t)(SEXP);
typedef SEXP (*hyang_altinteger_Sum_method_t)(SEXP, hyangboolean); 
typedef SEXP (*hyang_altinteger_Min_method_t)(SEXP, hyangboolean);
typedef SEXP (*hyang_altinteger_Max_method_t)(SEXP, hyangboolean);

typedef double (*hyang_altreal_Elt_method_t)(SEXP, hyang_xlen_t);
typedef hyang_xlen_t
(*hyang_altreal_Get_region_method_t)(SEXP, hyang_xlen_t, hyang_xlen_t, double *);
typedef int (*hyang_altreal_Is_sorted_method_t)(SEXP);
typedef int (*hyang_altreal_No_NA_method_t)(SEXP);
typedef SEXP (*hyang_altreal_Sum_method_t)(SEXP, hyangboolean); 
typedef SEXP (*hyang_altreal_Min_method_t)(SEXP, hyangboolean);
typedef SEXP (*hyang_altreal_Max_method_t)(SEXP, hyangboolean);

typedef SEXP (*hyang_altstring_Elt_method_t)(SEXP, hyang_xlen_t);
typedef void (*hyang_altstring_Set_elt_method_t)(SEXP, hyang_xlen_t, SEXP);
typedef int (*hyang_altstring_Is_sorted_method_t)(SEXP);
typedef int (*hyang_altstring_No_NA_method_t)(SEXP);

#define DECLARE_METHOD_SETTER(CNAME, MNAME)				\
    void								\
    hyang_set_##CNAME##_##MNAME##_method(hyang_altrep_class_t cls,		\
				     hyang_##CNAME##_##MNAME##_method_t fun);

DECLARE_METHOD_SETTER(altrep, UnserializeEX)
DECLARE_METHOD_SETTER(altrep, Unserialize)
DECLARE_METHOD_SETTER(altrep, Serialized_state)
DECLARE_METHOD_SETTER(altrep, DuplicateEX)
DECLARE_METHOD_SETTER(altrep, Duplicate)
DECLARE_METHOD_SETTER(altrep, Coerce)
DECLARE_METHOD_SETTER(altrep, Inspect)
DECLARE_METHOD_SETTER(altrep, Length)

DECLARE_METHOD_SETTER(altvec, Dataptr)
DECLARE_METHOD_SETTER(altvec, Dataptr_or_null)
DECLARE_METHOD_SETTER(altvec, Extract_subset)

DECLARE_METHOD_SETTER(altinteger, Elt)
DECLARE_METHOD_SETTER(altinteger, Get_region)
DECLARE_METHOD_SETTER(altinteger, Is_sorted)
DECLARE_METHOD_SETTER(altinteger, No_NA)
DECLARE_METHOD_SETTER(altinteger, Sum)
DECLARE_METHOD_SETTER(altinteger, Min)
DECLARE_METHOD_SETTER(altinteger, Max)

DECLARE_METHOD_SETTER(altreal, Elt)
DECLARE_METHOD_SETTER(altreal, Get_region)
DECLARE_METHOD_SETTER(altreal, Is_sorted)
DECLARE_METHOD_SETTER(altreal, No_NA)
DECLARE_METHOD_SETTER(altreal, Sum)
DECLARE_METHOD_SETTER(altreal, Min)
DECLARE_METHOD_SETTER(altreal, Max)

DECLARE_METHOD_SETTER(altstring, Elt)
DECLARE_METHOD_SETTER(altstring, Set_elt)
DECLARE_METHOD_SETTER(altstring, Is_sorted)
DECLARE_METHOD_SETTER(altstring, No_NA)

#endif /* HYANG_EXTALTREP_H_ */
