/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* Included by hyang.h: API */

#ifndef HYANG_ERRHANDLING_H_
#define HYANG_ERRHANDLING_H_

#ifdef  __cplusplus
extern "C" {
#endif

#if defined(__GNUC__) && __GNUC__ >= 3
#define NORET __attribute__((noreturn))
#else
#define NORET
#endif

void NORET hyangly_error(const char *, ...);
void NORET UNIMPLEMENTED(const char *);
void NORET WrongArgCount(const char *);

void	hyangly_warning(const char *, ...);
void 	hyang_ShowMessage(const char *s);
    

#ifdef  __cplusplus
}
#endif

#ifndef HYANG_NOREMAP
#define error hyangly_error
#define warning hyangly_warning
#endif


#endif /* HYANG_ERRHANDLING_H_ */
