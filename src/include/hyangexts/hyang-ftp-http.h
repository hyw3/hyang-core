/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* Advertized entry points, for that part of libxml included in
 * the internet module.
 */

#ifndef HYANG_FTP_HTTP_H_
#define HYANG_FTP_HTTP_H_

/*
  allow for 'large' files (>= 2GB) on 32-bit systems, where supported.
*/
/* required by C99/C11 */
#ifdef __cplusplus
# include <cstdint>
#else
# include <stdint.h>
#endif

typedef int_fast64_t DLsize_t; // used for download lengths and sizes

#ifdef __cplusplus
extern "C" {
#endif

void *hyang_HTTPOpen(const char *url);
int   hyang_HTTPRead(void *ctx, char *dest, int len);
void  hyang_HTTPClose(void *ctx);

void *hyang_FTPOpen(const char *url);
int   hyang_FTPRead(void *ctx, char *dest, int len);
void  hyang_FTPClose(void *ctx);

void *	hyangXmlNanoHTTPOpen(const char *URL, char **contentType, const char *headers, int cacheOK);
int	hyangXmlNanoHTTPRead(void *ctx, void *dest, int len);
void	hyangXmlNanoHTTPClose(void *ctx);
int 	hyangXmlNanoHTTPReturnCode(void *ctx);
char * 	hyangXmlNanoHTTPStatusMsg(void *ctx);
DLsize_t hyangXmlNanoHTTPContentLength(void *ctx);
char *	hyangXmlNanoHTTPContentType(void *ctx);
void	hyangXmlNanoHTTPTimeout(int delay);

void *	hyangXmlNanoFTPOpen(const char *URL);
int	hyangXmlNanoFTPRead(void *ctx, void *dest, int len);
int	hyangXmlNanoFTPClose(void *ctx);
void	hyangXmlNanoFTPTimeout(int delay);
DLsize_t hyangXmlNanoFTPContentLength(void *ctx);

void    hyangXmlMessage(int level, const char *format, ...);

/* not currently used */

void hyangXmlNanoFTPCleanup(void);
void hyangXmlNanoHTTPCleanup(void);

#ifdef __cplusplus
}
#endif

#endif /* HYANG_FTP_HTTP_H_ */
