/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/*
  C functions used to register compiled code in packages.

  Those needed for that purpose are part of the API.

  Some changes require recompilation of packages.
 */

#ifndef  HYANG_EXT_DYNLOAD_H_
#define  HYANG_EXT_DYNLOAD_H_

#include <hyangexts/hyangboolean.h>

/* called with a variable argument set */
typedef void * (*DL_FUNC)();

typedef unsigned int hyang_NativePrimitiveArgType;

/* For interfaces to objects created with as.single */
#define SINGLESXP 302

/*
 These are very similar to those in hyangdyp.h,
 but we maintain them separately to give us more freedom to do
 some computations on the internal versions that are derived from
 these definitions.
*/
typedef struct {
    const char *name;
    DL_FUNC     fun;
    int         numArgs;
    hyang_NativePrimitiveArgType *types;
} hyang_CMethodDef;

typedef hyang_CMethodDef hyang_FortranMethodDef;


typedef struct {
    const char *name;
    DL_FUNC     fun;
    int         numArgs;
} hyang_CallMethodDef;

typedef hyang_CallMethodDef hyang_ExternalMethodDef;


typedef struct _DllInfo DllInfo;

/*
  Currently ignore the graphics routines, accessible via .Nexus.graphics()
  and .Hil.graphics().
 */
#ifdef __cplusplus
extern "C" {
#endif
int hyang_regRoutines(DllInfo *info, const hyang_CMethodDef * const croutines,
		       const hyang_CallMethodDef * const callRoutines,
		       const hyang_FortranMethodDef * const fortranRoutines,
		       const hyang_ExternalMethodDef * const externalRoutines);

hyangboolean hyang_useDynSyms(DllInfo *info, hyangboolean value);
hyangboolean hyang_forceSyms(DllInfo *info, hyangboolean value);

DllInfo *hyang_getDllInfo(const char *name);

/* To be used by applications embedding Hyang to register their symbols
   that are not related to any dynamic module */
DllInfo *hyang_getEmbeddingDllInfo(void);

typedef struct hyangly_RegNativeSym hyang_RegNativeSym;
typedef enum {HYANG_ANY_SYM=0, HYANG_C_SYM, HYANG_CALL_SYM, HYANG_FORTRAN_SYM, HYANG_EXT_SYM} NativeSymbolType;


DL_FUNC hyang_FindSym(char const *, char const *,
		       hyang_RegNativeSym *symbol);


/* Interface for exporting and importing functions from one package
   for use from C code in a package.  The registration part probably
   ought to be integrated with the other registrations.  The naming of
   these routines may be less than ideal.
*/

void hyang_RegisterCCallable(const char *package, const char *name, DL_FUNC fptr);
DL_FUNC hyang_GetCCallable(const char *package, const char *name);

#ifdef __cplusplus
}
#endif

#endif /* HYANG_EXT_DYNLOAD_H_ */
