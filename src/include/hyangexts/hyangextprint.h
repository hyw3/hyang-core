/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */





/* Included by hyang.h: API */

#ifndef HYANG_EXT_PRINT_H_
#define HYANG_EXT_PRINT_H_

#ifdef  __cplusplus
/* If the vprintf interface is defined at all in C++ it may only be
   defined in namespace std.  It is part of the C++11 standard. */
# ifdef HYANG_USE_C99_IN_CXX
#  include <cstdarg>
#  define HYANG_VA_LIST std::va_list
# endif
extern "C" {
#else
# include <stdarg.h>
# define HYANG_VA_LIST va_list
#endif

void hyangprtf(const char *, ...);
void REprintf(const char *, ...);
#if !defined(__cplusplus) || defined HYANG_USE_C99_IN_CXX
void Rvprintf(const char *, HYANG_VA_LIST);
void REvprintf(const char *, HYANG_VA_LIST);
#endif

#ifdef  __cplusplus
}
#endif

#endif /* HYANG_EXT_PRINT_H_ */
