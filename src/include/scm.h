/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYANG_SCM_H
#define HYANG_SCM_H

#ifndef USING_HYANG
# define USING_HYANG
# define longint int
#endif

#ifdef __cplusplus
# error scm.h can not be used from C++ code: use hyang.h instead
#endif

#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <float.h>
#include <math.h>

#include <hyangconfig.h>
#include <hyangexts/hyangconstants.h>
#include <hyangexts/hyangmem.h>

extern void seed_in(long *);
extern void seed_out(long *);
extern double unif_rand(void);
extern double norm_rand(void);

#include <hyangexts/hyangscm.h>

#define Salloc(n,t) (t*)S_alloc(n, sizeof(t))
#define Srealloc(p,n,old,t) (t*)S_realloc(p,n,old,sizeof(t))

typedef struct {
	double re;
	double im;
} S_complex;

#ifdef S_OLD_COMPLEX
# define complex S_complex
#endif

#ifndef NO_CALL_HYANG
#define call_S call_Hyang
#endif

#endif
