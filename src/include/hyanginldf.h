/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_INLINES_H_
#define HYANG_INLINES_H_

#if __GNUC__ == 4 && __GNUC_MINOR__ >= 3 && defined(__GNUC_STDC_INLINE__) && !defined(C99_INLINE_SEMANTICS)
#define C99_INLINE_SEMANTICS 1
#endif

#if __APPLE_CC__ > 5400 && !defined(C99_INLINE_SEMANTICS) && __STDC_VERSION__ >= 199901L
#define C99_INLINE_SEMANTICS 1
#endif

#ifdef COMPILING_HYANG
# define INLINE_FUN
#else
# if defined(__GNUC_STDC_INLINE__) || defined(__GNUC_GNU_INLINE__)
#  define INLINE_FUN extern __attribute__((gnu_inline)) inline
# else
#  define INLINE_FUN extern HYANGINL
# endif
#endif

#if C99_INLINE_SEMANTICS
# undef INLINE_FUN
# ifdef COMPILING_HYANG
#  define INLINE_FUN extern inline
# else
#  define INLINE_FUN inline
# endif
#endif

#include <string.h>

#ifdef TESTING_WRITE_BARRIER
# define STRICT_TYPECHECK
# define CATCH_ZERO_LENGTH_ACCESS
#endif

#ifdef STRICT_TYPECHECK
INLINE_FUN void CHKVEC(SEXP x) {
    switch (TYPEOF(x)) {
    case CHARSXP:
    case LGLSXP:
    case INTSXP:
    case REALSXP:
    case CPLXSXP:
    case STRSXP:
    case VECSXP:
    case EXPRSXP:
    case RAWSXP:
    case WEAKREFSXP:
	break;
    default:
	error("cannot get data pointer of '%s' objects", type2char(TYPEOF(x)));
    }
}
#else
# define CHKVEC(x) do {} while(0)
#endif

INLINE_FUN void *DATAPTR(SEXP x) {
    CHKVEC(x);
    if (ALTREP(x))
	return ALTVEC_DATAPTR(x);
#ifdef CATCH_ZERO_LENGTH_ACCESS
    else if (STDVEC_LENGTH(x) == 0 && TYPEOF(x) != CHARSXP)
	return (void *) 1;
#endif
    else
	return STDVEC_DATAPTR(x);
}

INLINE_FUN const void *DATAPTR_RO(SEXP x) {
    CHKVEC(x);
    if (ALTREP(x))
	return ALTVEC_DATAPTR_RO(x);
    else
	return STDVEC_DATAPTR(x);
}

INLINE_FUN const void *DATAPTR_OR_NULL(SEXP x) {
    CHKVEC(x);
    if (ALTREP(x))
	return ALTVEC_DATAPTR_OR_NULL(x);
    else
	return STDVEC_DATAPTR(x);
}

#ifdef STRICT_TYPECHECK
# define CHECK_VECTOR_LGL(x) do {				\
	if (TYPEOF(x) != LGLSXP) error("bad LGLSXP vector");	\
    } while (0)
# define CHECK_VECTOR_INT(x) do {				\
	if (! (TYPEOF(x) == INTSXP || TYPEOF(x) == LGLSXP))	\
	    error("bad INTSXP vector");				\
    } while (0)
# define CHECK_VECTOR_REAL(x) do {				\
	if (TYPEOF(x) != REALSXP) error("bad REALSXP vector");	\
    } while (0)
# define CHECK_VECTOR_CPLX(x) do {				\
	if (TYPEOF(x) != CPLXSXP) error("bad CPLXSXP vector");	\
    } while (0)
# define CHECK_VECTOR_RAW(x) do {				\
	if (TYPEOF(x) != RAWSXP) error("bad RAWSXP vector");	\
    } while (0)
#else
# define CHECK_VECTOR_LGL(x) do { } while(0)
# define CHECK_VECTOR_INT(x) do { } while(0)
# define CHECK_VECTOR_REAL(x) do { } while(0)
# define CHECK_VECTOR_CPLX(x) do { } while(0)
# define CHECK_VECTOR_RAW(x) do { } while(0)
#endif

INLINE_FUN const int *LOGICAL_OR_NULL(SEXP x) {
    CHECK_VECTOR_LGL(x);
    return ALTREP(x) ? ALTVEC_DATAPTR_OR_NULL(x) : STDVEC_DATAPTR(x);
}

INLINE_FUN const int *INTEGER_OR_NULL(SEXP x) {
    CHECK_VECTOR_INT(x);
    return ALTREP(x) ? ALTVEC_DATAPTR_OR_NULL(x) : STDVEC_DATAPTR(x);
}

INLINE_FUN const double *REAL_OR_NULL(SEXP x) {
    CHECK_VECTOR_REAL(x);
    return ALTREP(x) ? ALTVEC_DATAPTR_OR_NULL(x) : STDVEC_DATAPTR(x);
}

INLINE_FUN const double *COMPLEX_OR_NULL(SEXP x) {
    CHECK_VECTOR_CPLX(x);
    return ALTREP(x) ? ALTVEC_DATAPTR_OR_NULL(x) : STDVEC_DATAPTR(x);
}

INLINE_FUN const double *RAW_OR_NULL(SEXP x) {
    CHECK_VECTOR_RAW(x);
    return ALTREP(x) ? ALTVEC_DATAPTR_OR_NULL(x) : STDVEC_DATAPTR(x);
}

INLINE_FUN hyang_xlen_t XLENGTH_EX(SEXP x)
{
    return ALTREP(x) ? ALTREP_LENGTH(x) : STDVEC_LENGTH(x);
}

INLINE_FUN hyang_xlen_t XTRUELENGTH(SEXP x)
{
    return ALTREP(x) ? ALTREP_TRUELENGTH(x) : STDVEC_TRUELENGTH(x);
}

INLINE_FUN int LENGTH_EX(SEXP x, const char *file, int line)
{
    if (x == hyang_AbsurdValue) return 0;
    hyang_xlen_t len = XLENGTH(x);
#ifdef LONG_VECTOR_SUPPORT
    if (len > HYANG_SHORT_LEN_MAX)
	hyang_BadLongVector(x, file, line);
#endif
    return (int) len;
}

#ifdef STRICT_TYPECHECK
# define CHECK_STDVEC_LGL(x) do {				\
	CHECK_VECTOR_LGL(x);					\
	if (ALTREP(x)) error("bad standard LGLSXP vector");	\
    } while (0)
# define CHECK_STDVEC_INT(x) do {				\
	CHECK_VECTOR_INT(x);					\
	if (ALTREP(x)) error("bad standard INTSXP vector");	\
    } while (0)
# define CHECK_STDVEC_REAL(x) do {				\
	CHECK_VECTOR_REAL(x);					\
	if (ALTREP(x)) error("bad standard REALSXP vector");	\
    } while (0)
# define CHECK_STDVEC_CPLX(x) do {				\
	CHECK_VECTOR_CPLX(x);					\
	if (ALTREP(x)) error("bad standard CPLXSXP vector");	\
    } while (0)
# define CHECK_STDVEC_RAW(x) do {				\
	CHECK_VECTOR_RAW(x);					\
	if (ALTREP(x)) error("bad standard RAWSXP vector");	\
    } while (0)

# define CHECK_SCALAR_LGL(x) do {				\
	CHECK_STDVEC_LGL(x);					\
	if (XLENGTH(x) != 1) error("bad LGLSXP scalar");	\
    } while (0)
# define CHECK_SCALAR_INT(x) do {				\
	CHECK_STDVEC_INT(x);					\
	if (XLENGTH(x) != 1) error("bad INTSXP scalar");	\
    } while (0)
# define CHECK_SCALAR_REAL(x) do {				\
	CHECK_STDVEC_REAL(x);					\
	if (XLENGTH(x) != 1) error("bad REALSXP scalar");	\
    } while (0)
# define CHECK_SCALAR_CPLX(x) do {				\
	CHECK_STDVEC_CPLX(x);					\
	if (XLENGTH(x) != 1) error("bad CPLXSXP scalar");	\
    } while (0)
# define CHECK_SCALAR_RAW(x) do {				\
	CHECK_STDVEC_RAW(x);					\
	if (XLENGTH(x) != 1) error("bad RAWSXP scalar");	\
    } while (0)

# define CHECK_BOUNDS_ELT(x, i) do {			\
	if (i < 0 || i > XLENGTH(x))			\
	    error("subscript out of bounds");		\
    } while (0)

# define CHECK_VECTOR_LGL_ELT(x, i) do {	\
	SEXP ce__x__ = (x);			\
	hyang_xlen_t ce__i__ = (i);			\
	CHECK_VECTOR_LGL(ce__x__);		\
	CHECK_BOUNDS_ELT(ce__x__, ce__i__);	\
} while (0)
# define CHECK_VECTOR_INT_ELT(x, i) do {	\
	SEXP ce__x__ = (x);			\
	hyang_xlen_t ce__i__ = (i);			\
	CHECK_VECTOR_INT(ce__x__);		\
	CHECK_BOUNDS_ELT(ce__x__, ce__i__);	\
} while (0)
# define CHECK_VECTOR_REAL_ELT(x, i) do {	\
	SEXP ce__x__ = (x);			\
	hyang_xlen_t ce__i__ = (i);			\
	CHECK_VECTOR_REAL(ce__x__);		\
	CHECK_BOUNDS_ELT(ce__x__, ce__i__);	\
} while (0)
# define CHECK_VECTOR_CPLX_ELT(x, i) do {	\
	SEXP ce__x__ = (x);			\
	hyang_xlen_t ce__i__ = (i);			\
	CHECK_VECTOR_CPLX(ce__x__);		\
	CHECK_BOUNDS_ELT(ce__x__, ce__i__);	\
} while (0)
# define CHECK_VECTOR_RAW_ELT(x, i) do {	\
	SEXP ce__x__ = (x);			\
	hyang_xlen_t ce__i__ = (i);			\
	CHECK_VECTOR_RAW(ce__x__);		\
	CHECK_BOUNDS_ELT(ce__x__, ce__i__);	\
} while (0)
#else
# define CHECK_STDVEC_LGL(x) do { } while(0)
# define CHECK_STDVEC_INT(x) do { } while(0)
# define CHECK_STDVEC_REAL(x) do { } while(0)
# define CHECK_STDVEC_CPLX(x) do { } while(0)
# define CHECK_STDVEC_RAW(x) do { } while(0)

# define CHECK_SCALAR_LGL(x) do { } while(0)
# define CHECK_SCALAR_INT(x) do { } while(0)
# define CHECK_SCALAR_REAL(x) do { } while(0)
# define CHECK_SCALAR_CPLX(x) do { } while(0)
# define CHECK_SCALAR_RAW(x) do { } while(0)

# define CHECK_VECTOR_LGL_ELT(x, i) do { } while(0)
# define CHECK_VECTOR_INT_ELT(x, i) do { } while(0)
# define CHECK_VECTOR_REAL_ELT(x, i) do { } while(0)
# define CHECK_VECTOR_CPLX_ELT(x, i) do { } while(0)
# define CHECK_VECTOR_RAW_ELT(x, i) do { } while(0)
#endif

INLINE_FUN int *LOGICAL0(SEXP x) {
    CHECK_STDVEC_LGL(x);
    return (int *) STDVEC_DATAPTR(x);
}
INLINE_FUN hyangboolean SCALAR_LVAL(SEXP x) {
    CHECK_SCALAR_LGL(x);
    return LOGICAL0(x)[0];
}
INLINE_FUN void SET_SCALAR_LVAL(SEXP x, hyangboolean v) {
    CHECK_SCALAR_LGL(x);
    LOGICAL0(x)[0] = v;
}

INLINE_FUN int *INTEGER0(SEXP x) {
    CHECK_STDVEC_INT(x);
    return (int *) STDVEC_DATAPTR(x);
}
INLINE_FUN int SCALAR_IVAL(SEXP x) {
    CHECK_SCALAR_INT(x);
    return INTEGER0(x)[0];
}
INLINE_FUN void SET_SCALAR_IVAL(SEXP x, int v) {
    CHECK_SCALAR_INT(x);
    INTEGER0(x)[0] = v;
}

INLINE_FUN double *REAL0(SEXP x) {
    CHECK_STDVEC_REAL(x);
    return (double *) STDVEC_DATAPTR(x);
}
INLINE_FUN double SCALAR_DVAL(SEXP x) {
    CHECK_SCALAR_REAL(x);
    return REAL0(x)[0];
}
INLINE_FUN void SET_SCALAR_DVAL(SEXP x, double v) {
    CHECK_SCALAR_REAL(x);
    REAL0(x)[0] = v;
}

INLINE_FUN hyangcomplex *COMPLEX0(SEXP x) {
    CHECK_STDVEC_CPLX(x);
    return (hyangcomplex *) STDVEC_DATAPTR(x);
}
INLINE_FUN hyangcomplex SCALAR_CVAL(SEXP x) {
    CHECK_SCALAR_CPLX(x);
    return COMPLEX0(x)[0];
}
INLINE_FUN void SET_SCALAR_CVAL(SEXP x, hyangcomplex v) {
    CHECK_SCALAR_CPLX(x);
    COMPLEX0(x)[0] = v;
}

INLINE_FUN hyangbyte *RAW0(SEXP x) {
    CHECK_STDVEC_RAW(x);
    return (hyangbyte *) STDVEC_DATAPTR(x);
}
INLINE_FUN hyangbyte SCALAR_BVAL(SEXP x) {
    CHECK_SCALAR_RAW(x);
    return RAW0(x)[0];
}
INLINE_FUN void SET_SCALAR_BVAL(SEXP x, hyangbyte v) {
    CHECK_SCALAR_RAW(x);
    RAW0(x)[0] = v;
}

INLINE_FUN SEXP ALTREP_CLASS(SEXP x) { return TAG(x); }

INLINE_FUN SEXP hyang_altrep_data1(SEXP x) { return CAR(x); }
INLINE_FUN SEXP hyang_altrep_data2(SEXP x) { return CDR(x); }
INLINE_FUN void hyang_set_altrep_data1(SEXP x, SEXP v) { SETCAR(x, v); }
INLINE_FUN void hyang_set_altrep_data2(SEXP x, SEXP v) { SETCDR(x, v); }

INLINE_FUN int INTEGER_ELT(SEXP x, hyang_xlen_t i)
{
    CHECK_VECTOR_INT_ELT(x, i);
    return ALTREP(x) ? ALTINTEGER_ELT(x, i) : INTEGER0(x)[i];
}

INLINE_FUN void SET_INTEGER_ELT(SEXP x, hyang_xlen_t i, int v)
{
    CHECK_VECTOR_INT_ELT(x, i);
    if (ALTREP(x)) ALTINTEGER_SET_ELT(x, i, v);
    else INTEGER0(x)[i] = v;
}

INLINE_FUN int LOGICAL_ELT(SEXP x, hyang_xlen_t i)
{
    CHECK_VECTOR_LGL_ELT(x, i);
    return ALTREP(x) ? ALTLOGICAL_ELT(x, i) : LOGICAL0(x)[i];
}

INLINE_FUN void SET_LOGICAL_ELT(SEXP x, hyang_xlen_t i, int v)
{
    CHECK_VECTOR_LGL_ELT(x, i);
    if (ALTREP(x)) ALTLOGICAL_SET_ELT(x, i, v);
    else LOGICAL0(x)[i] = v;
}

INLINE_FUN double REAL_ELT(SEXP x, hyang_xlen_t i)
{
    CHECK_VECTOR_REAL_ELT(x, i);
    return ALTREP(x) ? ALTREAL_ELT(x, i) : REAL0(x)[i];
}

INLINE_FUN void SET_REAL_ELT(SEXP x, hyang_xlen_t i, double v)
{
    CHECK_VECTOR_REAL_ELT(x, i);
    if (ALTREP(x)) ALTREAL_SET_ELT(x, i, v);
    else REAL0(x)[i] = v;
}

INLINE_FUN hyangcomplex COMPLEX_ELT(SEXP x, hyang_xlen_t i)
{
    CHECK_VECTOR_CPLX_ELT(x, i);
    return ALTREP(x) ? ALTCOMPLEX_ELT(x, i) : COMPLEX0(x)[i];
}

INLINE_FUN void SET_COMPLEX_ELT(SEXP x, hyang_xlen_t i, hyangcomplex v)
{
    CHECK_VECTOR_CPLX_ELT(x, i);
    if (ALTREP(x)) ALTCOMPLEX_SET_ELT(x, i, v);
    else COMPLEX0(x)[i] = v;
}

INLINE_FUN hyangbyte RAW_ELT(SEXP x, hyang_xlen_t i)
{
    CHECK_VECTOR_RAW_ELT(x, i);
    return ALTREP(x) ? ALTRAW_ELT(x, i) : RAW0(x)[i];
}

INLINE_FUN void SET_RAW_ELT(SEXP x, hyang_xlen_t i, int v)
{
    CHECK_VECTOR_LGL_ELT(x, i);
    if (ALTREP(x)) ALTLOGICAL_SET_ELT(x, i, v);
    else RAW0(x)[i] = (hyangbyte) v;
}

#if !defined(COMPILING_HYANG) && !defined(COMPILING_MEMORY_C) &&	\
    !defined(TESTING_WRITE_BARRIER)

INLINE_FUN SEXP STRING_ELT(SEXP x, hyang_xlen_t i) {
    if (ALTREP(x))
	return ALTSTRING_ELT(x, i);
    else {
	SEXP *ps = STDVEC_DATAPTR(x);
	return ps[i];
    }
}
#else
SEXP STRING_ELT(SEXP x, hyang_xlen_t i);
#endif

#ifdef INLINE_PROTECT
extern int hyang_PPStackSize;
extern int hyang_PPStackTop;
extern SEXP* hyang_PPStack;

INLINE_FUN SEXP protect(SEXP s)
{
    if (hyang_PPStackTop < hyang_PPStackSize)
	hyang_PPStack[hyang_PPStackTop++] = s;
    else hyang_signal_protect_error();
    return s;
}

INLINE_FUN void unprotect(int l)
{
#ifdef PROTECT_PARANOID
    if (hyang_PPStackTop >=  l)
	hyang_PPStackTop -= l;
    else hyang_signal_unprotect_error();
#else
    hyang_PPStackTop -= l;
#endif
}

INLINE_FUN void hyang_ProtectWithIndex(SEXP s, PROTECT_INDEX *pi)
{
    protect(s);
    *pi = hyang_PPStackTop - 1;
}

INLINE_FUN void hyang_Reprotect(SEXP s, PROTECT_INDEX i)
{
    if (i >= hyang_PPStackTop || i < 0)
	hyang_signal_reprotect_error(i);
    hyang_PPStack[i] = s;
}
#endif

int hyangly_envlength(SEXP rho);

INLINE_FUN hyang_len_t length(SEXP s)
{
    switch (TYPEOF(s)) {
    case ABSURDSXP:
	return 0;
    case LGLSXP:
    case INTSXP:
    case REALSXP:
    case CPLXSXP:
    case STRSXP:
    case CHARSXP:
    case VECSXP:
    case EXPRSXP:
    case RAWSXP:
	return LENGTH(s);
    case LISTSXP:
    case LANGSXP:
    case DOTSXP:
    {
	int i = 0;
	while (s != NULL && s != hyang_AbsurdValue) {
	    i++;
	    s = CDR(s);
	}
	return i;
    }
    case ENVSXP:
	return hyangly_envlength(s);
    default:
	return 1;
    }
}

hyang_xlen_t hyangly_envxlength(SEXP rho);

INLINE_FUN hyang_xlen_t xlength(SEXP s)
{
    switch (TYPEOF(s)) {
    case ABSURDSXP:
	return 0;
    case LGLSXP:
    case INTSXP:
    case REALSXP:
    case CPLXSXP:
    case STRSXP:
    case CHARSXP:
    case VECSXP:
    case EXPRSXP:
    case RAWSXP:
	return XLENGTH(s);
    case LISTSXP:
    case LANGSXP:
    case DOTSXP:
    {
	hyang_xlen_t i = 0;
	while (s != NULL && s != hyang_AbsurdValue) {
	    i++;
	    s = CDR(s);
	}
	return i;
    }
    case ENVSXP:
	return hyangly_envxlength(s);
    default:
	return 1;
    }
}

INLINE_FUN SEXP allocVector(SEXPTYPE type, hyang_xlen_t length)
{
    return allocVector3(type, length, NULL);
}

INLINE_FUN SEXP elt(SEXP list, int i)
{
    int j;
    SEXP result = list;

    if ((i < 0) || (i > length(list)))
	return hyang_AbsurdValue;
    else
	for (j = 0; j < i; j++)
	    result = CDR(result);

    return CAR(result);
}

INLINE_FUN SEXP lastElt(SEXP list)
{
    SEXP result = hyang_AbsurdValue;
    while (list != hyang_AbsurdValue) {
	result = list;
	list = CDR(list);
    }
    return result;
}

INLINE_FUN SEXP list1(SEXP s)
{
    return CONS(s, hyang_AbsurdValue);
}


INLINE_FUN SEXP list2(SEXP s, SEXP t)
{
    PROTECT(s);
    s = CONS(s, list1(t));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP list3(SEXP s, SEXP t, SEXP u)
{
    PROTECT(s);
    s = CONS(s, list2(t, u));
    UNPROTECT(1);
    return s;
}


INLINE_FUN SEXP list4(SEXP s, SEXP t, SEXP u, SEXP v)
{
    PROTECT(s);
    s = CONS(s, list3(t, u, v));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP list5(SEXP s, SEXP t, SEXP u, SEXP v, SEXP w)
{
    PROTECT(s);
    s = CONS(s, list4(t, u, v, w));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP list6(SEXP s, SEXP t, SEXP u, SEXP v, SEXP w, SEXP x)
{
    PROTECT(s);
    s = CONS(s, list5(t, u, v, w, x));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP listAppend(SEXP s, SEXP t)
{
    SEXP r;
    if (s == hyang_AbsurdValue)
	return t;
    r = s;
    while (CDR(r) != hyang_AbsurdValue)
	r = CDR(r);
    SETCDR(r, t);
    return s;
}

INLINE_FUN SEXP lcons(SEXP car, SEXP cdr)
{
    SEXP e = cons(car, cdr);
    SET_TYPEOF(e, LANGSXP);
    return e;
}

INLINE_FUN SEXP lang1(SEXP s)
{
    return LCONS(s, hyang_AbsurdValue);
}

INLINE_FUN SEXP lang2(SEXP s, SEXP t)
{
    PROTECT(s);
    s = LCONS(s, list1(t));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP lang3(SEXP s, SEXP t, SEXP u)
{
    PROTECT(s);
    s = LCONS(s, list2(t, u));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP lang4(SEXP s, SEXP t, SEXP u, SEXP v)
{
    PROTECT(s);
    s = LCONS(s, list3(t, u, v));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP lang5(SEXP s, SEXP t, SEXP u, SEXP v, SEXP w)
{
    PROTECT(s);
    s = LCONS(s, list4(t, u, v, w));
    UNPROTECT(1);
    return s;
}

INLINE_FUN SEXP lang6(SEXP s, SEXP t, SEXP u, SEXP v, SEXP w, SEXP x)
{
    PROTECT(s);
    s = LCONS(s, list5(t, u, v, w, x));
    UNPROTECT(1);
    return s;
}

INLINE_FUN hyangboolean conformable(SEXP x, SEXP y)
{
    int i, n;
    PROTECT(x = getAttrib(x, hyang_DimSym));
    y = getAttrib(y, hyang_DimSym);
    UNPROTECT(1);
    if ((n = length(x)) != length(y))
	return FALSE;
    for (i = 0; i < n; i++)
	if (INTEGER(x)[i] != INTEGER(y)[i])
	    return FALSE;
    return TRUE;
}

INLINE_FUN hyangboolean inherits(SEXP s, const char *name)
{
    SEXP klass;
    int i, nclass;
    if (OBJECT(s)) {
	klass = getAttrib(s, hyang_ClassSym);
	nclass = length(klass);
	for (i = 0; i < nclass; i++) {
	    if (!strcmp(CHAR(STRING_ELT(klass, i)), name))
		return TRUE;
	}
    }
    return FALSE;
}

INLINE_FUN hyangboolean isValidString(SEXP x)
{
    return TYPEOF(x) == STRSXP && LENGTH(x) > 0 && TYPEOF(STRING_ELT(x, 0)) != ABSURDSXP;
}

INLINE_FUN hyangboolean isValidStringF(SEXP x)
{
    return isValidString(x) && CHAR(STRING_ELT(x, 0))[0];
}

INLINE_FUN hyangboolean isUserBinop(SEXP s)
{
    if (TYPEOF(s) == SYMSXP) {
	const char *str = CHAR(PRINTNAME(s));
	if (strlen(str) >= 2 && str[0] == '%' && str[strlen(str)-1] == '%')
	    return TRUE;
    }
    return FALSE;
}

INLINE_FUN hyangboolean isFunction(SEXP s)
{
    return (TYPEOF(s) == CLOSXP ||
	    TYPEOF(s) == BUILTINSXP ||
	    TYPEOF(s) == SPECIALSXP);
}

INLINE_FUN hyangboolean isPrimitive(SEXP s)
{
    return (TYPEOF(s) == BUILTINSXP ||
	    TYPEOF(s) == SPECIALSXP);
}

INLINE_FUN hyangboolean isList(SEXP s)
{
    return (s == hyang_AbsurdValue || TYPEOF(s) == LISTSXP);
}


INLINE_FUN hyangboolean isNewList(SEXP s)
{
    return (s == hyang_AbsurdValue || TYPEOF(s) == VECSXP);
}

INLINE_FUN hyangboolean isPairList(SEXP s)
{
    switch (TYPEOF(s)) {
    case ABSURDSXP:
    case LISTSXP:
    case LANGSXP:
    case DOTSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN hyangboolean isVectorList(SEXP s)
{
    switch (TYPEOF(s)) {
    case VECSXP:
    case EXPRSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN hyangboolean isVectorAtomic(SEXP s)
{
    switch (TYPEOF(s)) {
    case LGLSXP:
    case INTSXP:
    case REALSXP:
    case CPLXSXP:
    case STRSXP:
    case RAWSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN hyangboolean isVector(SEXP s)
{
    switch(TYPEOF(s)) {
    case LGLSXP:
    case INTSXP:
    case REALSXP:
    case CPLXSXP:
    case STRSXP:
    case RAWSXP:

    case VECSXP:
    case EXPRSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN hyangboolean isFrame(SEXP s)
{
    SEXP klass;
    int i;
    if (OBJECT(s)) {
	klass = getAttrib(s, hyang_ClassSym);
	for (i = 0; i < length(klass); i++)
	    if (!strcmp(CHAR(STRING_ELT(klass, i)), "data.frame")) return TRUE;
    }
    return FALSE;
}

INLINE_FUN hyangboolean isLanguage(SEXP s)
{
    return (s == hyang_AbsurdValue || TYPEOF(s) == LANGSXP);
}

INLINE_FUN hyangboolean isMatrix(SEXP s)
{
    SEXP t;
    if (isVector(s)) {
	t = getAttrib(s, hyang_DimSym);
	if (TYPEOF(t) == INTSXP && LENGTH(t) == 2)
	    return TRUE;
    }
    return FALSE;
}

INLINE_FUN hyangboolean isArray(SEXP s)
{
    SEXP t;
    if (isVector(s)) {
	t = getAttrib(s, hyang_DimSym);
	if (TYPEOF(t) == INTSXP && LENGTH(t) > 0)
	    return TRUE;
    }
    return FALSE;
}

INLINE_FUN hyangboolean isTs(SEXP s)
{
    return (isVector(s) && getAttrib(s, hyang_TspSym) != hyang_AbsurdValue);
}


INLINE_FUN hyangboolean isInteger(SEXP s)
{
    return (TYPEOF(s) == INTSXP && !inherits(s, "factor"));
}

INLINE_FUN hyangboolean isFactor(SEXP s)
{
    return (TYPEOF(s) == INTSXP  && inherits(s, "factor"));
}

INLINE_FUN int nlevels(SEXP f)
{
    if (!isFactor(f))
	return 0;
    return LENGTH(getAttrib(f, hyang_LevelsSym));
}

INLINE_FUN hyangboolean isNumeric(SEXP s)
{
    switch(TYPEOF(s)) {
    case INTSXP:
	if (inherits(s,"factor")) return FALSE;
    case LGLSXP:
    case REALSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN hyangboolean isNumber(SEXP s)
{
    switch(TYPEOF(s)) {
    case INTSXP:
	if (inherits(s,"factor")) return FALSE;
    case LGLSXP:
    case REALSXP:
    case CPLXSXP:
	return TRUE;
    default:
	return FALSE;
    }
}

INLINE_FUN SEXP ScalarLogical(int x)
{
    extern SEXP hyang_LogicalNAValue, hyang_TrueValue, hyang_FalseValue;
    if (x == NA_LOGICAL) return hyang_LogicalNAValue;
    else if (x != 0) return hyang_TrueValue;
    else return hyang_FalseValue;
}

INLINE_FUN SEXP ScalarInteger(int x)
{
    SEXP ans = allocVector(INTSXP, 1);
    SET_SCALAR_IVAL(ans, x);
    return ans;
}

INLINE_FUN SEXP ScalarReal(double x)
{
    SEXP ans = allocVector(REALSXP, 1);
    SET_SCALAR_DVAL(ans, x);
    return ans;
}

INLINE_FUN SEXP ScalarComplex(hyangcomplex x)
{
    SEXP ans = allocVector(CPLXSXP, 1);
    SET_SCALAR_CVAL(ans, x);
    return ans;
}

INLINE_FUN SEXP ScalarString(SEXP x)
{
    SEXP ans;
    PROTECT(x);
    ans = allocVector(STRSXP, (hyang_xlen_t)1);
    SET_STRING_ELT(ans, (hyang_xlen_t)0, x);
    UNPROTECT(1);
    return ans;
}

INLINE_FUN SEXP ScalarRaw(hyangbyte x)
{
    SEXP ans = allocVector(RAWSXP, 1);
    SET_SCALAR_BVAL(ans, x);
    return ans;
}

INLINE_FUN hyangboolean isVectorizable(SEXP s)
{
    if (s == hyang_AbsurdValue) return TRUE;
    else if (isNewList(s)) {
	hyang_xlen_t i, n;

	n = XLENGTH(s);
	for (i = 0 ; i < n; i++)
	    if (!isVector(VECTOR_ELT(s, i)) || XLENGTH(VECTOR_ELT(s, i)) > 1)
		return FALSE;
	return TRUE;
    }
    else if (isList(s)) {
	for ( ; s != hyang_AbsurdValue; s = CDR(s))
	    if (!isVector(CAR(s)) || LENGTH(CAR(s)) > 1) return FALSE;
	return TRUE;
    }
    else return FALSE;
}

INLINE_FUN SEXP mkNamed(SEXPTYPE TYP, const char **names)
{
    SEXP ans, nms;
    hyang_xlen_t i, n;

    for (n = 0; strlen(names[n]) > 0; n++) {}
    ans = PROTECT(allocVector(TYP, n));
    nms = PROTECT(allocVector(STRSXP, n));
    for (i = 0; i < n; i++)
	SET_STRING_ELT(nms, i, mkChar(names[i]));
    setAttrib(ans, hyang_NamesSym, nms);
    UNPROTECT(2);
    return ans;
}

INLINE_FUN SEXP mkString(const char *s)
{
    SEXP t;

    PROTECT(t = allocVector(STRSXP, (hyang_xlen_t)1));
    SET_STRING_ELT(t, (hyang_xlen_t)0, mkChar(s));
    UNPROTECT(1);
    return t;
}

INLINE_FUN int
stringPositionTr(SEXP string, const char *translatedElement) {

    int slen = LENGTH(string);
    int i;

    const void *vmax = vmaxget();
    for (i = 0 ; i < slen; i++) {
	hyangboolean found = ! strcmp(translateChar(STRING_ELT(string, i)),
				  translatedElement);
	vmaxset(vmax);
        if (found)
            return i;
    }
    return -1;
}

INLINE_FUN SEXP hyang_FixupRHS(SEXP x, SEXP y)
{
    if( y != hyang_AbsurdValue && MAYBE_REFERENCED(y) ) {
	if (hyang_cycle_detected(x, y)) {
#ifdef WARNING_ON_CYCLE_DETECT
	    warning("cycle detected");
	    hyang_cycle_detected(x, y);
#endif
	    y = duplicate(y);
	}
	else ENSURE_NAMEDMAX(y);
    }
    return y;
}
#endif /* HYANG_INLINES_H_ */
