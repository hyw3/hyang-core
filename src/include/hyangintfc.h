/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGINTFC_H_
#define HYANGINTFC_H_

#include <hyangexts/hyangboolean.h>

#ifdef __cplusplus
# include <cstdio>
extern "C" {
#else
# include <stdio.h>
#endif

#if defined(__GNUC__) && __GNUC__ >= 3
# define NORET __attribute__((noreturn))
#else
# define NORET
#endif

extern hyangboolean hyang_Interactive;
extern hyangboolean	hyang_Slave;
extern void hyang_RestoreGlobalEnv(void);
extern void hyang_RestoreGlobalEnvFromFile(const char *, hyangboolean);
extern void hyang_SaveGlobalEnv(void);
extern void hyang_SaveGlobalEnvToFile(const char *);
extern void hyang_FlushConsole(void);
extern void hyang_ClearerrConsole(void);
extern void hyang_Suicide(const char *);
extern char *hyang_HomeDir(void);
extern int hyang_DirtyImage;
extern char *hyang_GUIType;
extern void hyang_setupHistory(void);
extern char *hyang_HistoryFile;
extern int hyang_HistorySize;
extern int hyang_RestoreHistory;
extern char *hyang_Home;

# define jump_to_toplevel	hyangly_jump_to_toplevel
# define mainloop		hyangly_mainloop
# define onintr			hyangly_onintr
# define onintrNoResume		hyangly_onintrNoResume
void NORET jump_to_toplevel(void);
void mainloop(void);
void onintr(void);
void onintrNoResume(void);
#ifndef HYANGDEFN_H_
extern void* hyang_GlobalCtx;
#endif

void process_site_hyangenviron(void);
void process_system_hyangenviron(void);
void process_user_hyangenviron(void);

#ifdef __cplusplus
extern std::FILE * hyang_Consolefile;
extern std::FILE * hyang_Outputfile;
#else
extern FILE * hyang_Consolefile;
extern FILE * hyang_Outputfile;
#endif

void hyang_setStartTime(void);
void fpu_setup(hyangboolean);

extern int hyang_running_as_main_program;

#ifdef CSTACK_DEFNS
#if !defined(HAVE_UINTPTR_T) && !defined(uintptr_t)
 typedef unsigned long uintptr_t;
#else
# ifndef __cplusplus
#  include <stdint.h>
# elif __cplusplus >= 201103L
#  include <cstdint>
# endif
#endif

extern uintptr_t hyang_CStackLim;
extern uintptr_t hyang_CStackStart;
#endif

#ifdef HYANG_INTFC_PTRS
#include <hyangintls.h>
#include <hyangexts/hyangextstu.h>

#ifdef __SYSTEM__
# define extern
#endif

extern void (*hyang_ptrSuicide)(const char *);
extern void (*hyang_ptrShowMsg)(const char *);
extern int  (*hyang_ptrReadConsole)(const char *, unsigned char *, int, int);
extern void (*hyang_ptrWriteConsole)(const char *, int);
extern void (*hyang_ptrWriteConsoleEx)(const char *, int, int);
extern void (*hyang_ptrResetConsole)(void);
extern void (*hyang_ptrFlushConsole)(void);
extern void (*hyang_ptrClearerrConsole)(void);
extern void (*hyang_ptrBusy)(int);
extern void (*hyang_ptrCleanUp)(SA_TYPE, int, int);
extern int  (*hyang_ptrShowFiles)(int, const char **, const char **,
			       const char *, hyangboolean, const char *);
extern int  (*hyang_ptrChooseFile)(int, char *, int);
extern int  (*hyang_ptrEditFile)(const char *);
extern void (*hyang_ptrloadhistory)(SEXP, SEXP, SEXP, SEXP);
extern void (*hyang_ptrsavehistory)(SEXP, SEXP, SEXP, SEXP);
extern void (*hyang_ptraddhistory)(SEXP, SEXP, SEXP, SEXP);

extern int  (*hyang_ptrEditFiles)(int, const char **, const char **, const char *);
extern SEXP (*ptr_do_selectlist)(SEXP, SEXP, SEXP, SEXP);
extern SEXP (*ptr_do_dataentry)(SEXP, SEXP, SEXP, SEXP);
extern SEXP (*ptr_do_dataviewer)(SEXP, SEXP, SEXP, SEXP);
extern void (*hyang_ptrProcessEvents)();

extern int  (*hyang_timeoutHandler)(void);
extern long hyang_timeoutVal;

#endif /* HYANG_INTFC_PTRS */

#ifdef __SYSTEM__
# undef extern
#endif

extern int hyang_SignalHandlers;

#ifdef __cplusplus
}
#endif

#endif /* HYANGINTFC_H_ */
