/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYANG_DYP_H
#define HYANG_DYP_H

#ifdef Win32
#include <windows.h>
#define CACHE_DLL_SYM 1
#else
typedef void *HINSTANCE;
#endif

#include <hyangdefn.h>
#include <hyangexts/hyangdyl.h>
int hyang_moduleCdyl(const char *module, int local, int now);

typedef struct {
    char *name;
    DL_FUNC func;
} CFunTabEntry;

typedef struct {
    char       *name;
    DL_FUNC     fun;
    int         numArgs;

    hyang_NativePrimitiveArgType *types;
} hyangly_DotCSym;

typedef hyangly_DotCSym hyangly_DotFortranSym;

typedef struct {
    char       *name;
    DL_FUNC     fun;
    int         numArgs;
} hyangly_DotCallSym;

typedef hyangly_DotCallSym hyangly_DotExtSym;

struct _DllInfo {
    char  *path;
    char  *name;
    HINSTANCE handle;
    hyangboolean useDynamicLookup;

    int numCSymbols;
    hyangly_DotCSym *CSymbols;

    int numCallSymbols;
    hyangly_DotCallSym *CallSymbols;

    int numFortranSymbols;
    hyangly_DotFortranSym *FortranSymbols;

    int numExternalSymbols;
    hyangly_DotExtSym *ExternalSymbols;

    hyangboolean forceSymbols;
};

struct hyangly_RegNativeSym {
    NativeSymbolType type;
    union {
	hyangly_DotCSym        *c;
	hyangly_DotCallSym     *call;
	hyangly_DotFortranSym  *fortran;
	hyangly_DotExtSym *external;
    } symbol;
    DllInfo *dll;
};

typedef struct {
    HINSTANCE (*loadLibrary)(const char *path, int asLocal, int now,
			     char const *search);
    DL_FUNC (*dlsym)(DllInfo *info, char const *name);
    void (*closeLibrary)(HINSTANCE handle);
    void (*getError)(char *buf, int len);

    void (*deleteCachedSymbols)(DllInfo *dll);
    DL_FUNC (*lookupCachedSymbol)(const char *name, const char *pkg, int all);

    void  (*fixPath)(char *path);
    void  (*getFullDLLPath)(SEXP call, char *buf, const char * const path);

} OSDynSymbol;

extern OSDynSymbol hyangly_osDynSym, *hyang_osDynSym;

#ifdef CACHE_DLL_SYM

typedef struct {
    char pkg[21];
    char name[41];
    DL_FUNC func;
} hyang_CPFun;

extern hyang_CPFun CPFun[];
extern int nCPFun;

#endif

DL_FUNC hyangly_lookupCachedSym(const char *name, const char *pkg, int all);

DL_FUNC hyang_dlsym(DllInfo *info, char const *name,
		hyang_RegNativeSym *symbol);

DL_FUNC hyang_dotCallFn(SEXP, SEXP, int);
SEXP hyangto_DotCall(DL_FUNC, int, SEXP *, SEXP);

#endif
