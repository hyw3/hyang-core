/* Hyang Programming Language
 *
 * Copyright (C) 2017 Ei-ji Nakama
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_LOCALE_H
#define HYANG_LOCALE_H

#include <wchar.h>
#include <ctype.h>
#include <wctype.h>

#ifdef Win32
typedef unsigned int hyangwchar_t;
#else
typedef wchar_t hyangwchar_t;
#endif

extern int hyangi18n_wcwidth(hyangwchar_t);
extern int hyangi18n_wcswidth (const wchar_t *, size_t);

extern wctype_t hyangi18n_wctype(const char *);
extern int      hyangi18n_iswctype(wint_t, wctype_t);

#ifndef IN_RLOCALE_C
#undef iswupper
#undef iswlower
#undef iswalpha
#undef iswdigit
#undef iswxdigit
#undef iswspace
#undef iswprint
#undef iswgraph
#undef iswblank
#undef iswcntrl
#undef iswpunct
#undef iswalnum
#undef wctype
#undef iswctype

#define iswupper(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("upper"))
#define iswlower(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("lower"))
#define iswalpha(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("alpha"))
#define iswdigit(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("digit"))
#define iswxdigit(__x)    hyangi18n_iswctype(__x, hyangi18n_wctype("xdigit"))
#define iswspace(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("space"))
#define iswprint(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("print"))
#define iswgraph(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("graph"))
#define iswblank(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("blank"))
#define iswcntrl(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("cntrl"))
#define iswpunct(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("punct"))
#define iswalnum(__x)     hyangi18n_iswctype(__x, hyangi18n_wctype("alnum"))
#define wctype(__x)       hyangi18n_wctype(__x)
#define iswctype(__x,__y) hyangi18n_iswctype(__x,__y)
#endif

#define HIGH_SURROGATE_START 0xd800
#define HIGH_SURROGATE_END 0xdbff
#define LOW_SURROGATE_START 0xdc00
#define LOW_SURROGATE_END 0xdfff
#define IS_HIGH_SURROGATE(wch) (((wch) >= HIGH_SURROGATE_START) && ((wch) <= HIGH_SURROGATE_END))
#define IS_LOW_SURROGATE(wch) (((wch) >= LOW_SURROGATE_START) && ((wch) <= LOW_SURROGATE_END))
#define IS_SURROGATE_PAIR(hs, ls) (IS_HIGH_SURROGATE (hs) && IS_LOW_SURROGATE (ls))

# define utf8toucs32		hyangly_utf8toucs32
hyangwchar_t utf8toucs32(wchar_t high, const char *s);

#endif /* HYANG_LOCALE_H */
