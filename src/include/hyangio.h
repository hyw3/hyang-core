/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGIO_H
#define HYANGIO_H

#include <hyangdefn.h>
#include <stdio.h>

#define IOBSIZE 4096

typedef struct BufferListItem {
	unsigned char			buf[IOBSIZE];
	struct BufferListItem	*next;
} BufferListItem;

typedef struct IoBuffer {
	BufferListItem	*start_buf;		/* First buffer item */
	BufferListItem	*write_buf;		/* Write pointer location */
	unsigned char	*write_ptr;		/* Write pointer location */
	int		 write_offset;		/* Write pointer location */
	BufferListItem	*read_buf;		/* Read pointer location */
	unsigned  char	*read_ptr;		/* Read pointer location */
	int		 read_offset;		/* Read pointer location */
} IoBuffer;


typedef struct TextBuffer {
	void	*vmax;				/* Memory stack top */
	unsigned char	*buf;			/* Line buffer */
	unsigned char	*bufp;			/* Line buffer location */
	SEXP	text;				/* String Vector */
	int	ntext;				/* Vector length */
	int	offset;				/* Offset within vector */
} TextBuffer;

#ifndef __MAIN__
extern
#else
attribute_hidden
#endif
IoBuffer hyang_ConsoleIob;

int hyang_IoBuffInit(IoBuffer*);
int hyang_IoBuffFree(IoBuffer*);
int hyang_IoBuffReadReset(IoBuffer*);
int hyang_IoBuffWriteReset(IoBuffer*);
int hyang_IoBuffGetC(IoBuffer*);
int hyang_IoBuffPutC(int, IoBuffer*);
int hyang_IoBuffPutS(char*, IoBuffer*);
int hyang_IoBuffReadOffset(IoBuffer*);

int hyang_TxtBuffInit(TextBuffer*, SEXP);
int hyang_TxtBuffFree(TextBuffer*);
int hyang_TxtBuffGetC(TextBuffer*);

#endif /* not HYANGIO_H */
