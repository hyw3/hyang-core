/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGPRINT_H_
#define HYANGPRINT_H_

#include "hyangdefn.h"
#include <hyangexts/hyangprtutl.h>
#include <hyangexts/hyangextprint.h>

#define formatRaw           hyangly_formatRaw
#define formatString        hyangly_formatStr
#define EncodeElement       hyangly_EncodeElement
#define EncodeElement0      hyangly_EncodeElement0
#define EncodeEnvironment   hyangly_EncodeEnv
#define printArray          hyangly_prtArray
#define printMatrix         hyangly_prtMatrix
#define printNamedVector    hyangly_prtNmdVect
#define printVector         hyangly_prtVect

typedef struct {
    int width;
    int na_width;
    int na_width_noquote;
    int digits;
    int scipen;
    int gap;
    int quote;
    int right;
    int max;
    SEXP na_string;
    SEXP na_string_noquote;
    int useSource;
    int cutoff;
} hyang_print_par_t;
extern hyang_print_par_t hyang_print;

void formatRaw(const hyangbyte *, hyang_xlen_t, int *);
void formatString(const SEXP*, hyang_xlen_t, int*, int);

const char *EncodeElement0(SEXP, int, int, const char *);
const char *EncodeEnvironment(SEXP);
const char *EncodeElement(SEXP, int, int, char);

void printArray(SEXP, SEXP, int, int, SEXP);
void printMatrix(SEXP, int, SEXP, int, int, SEXP, SEXP,
		 const char*, const char*);
void printNamedVector(SEXP, SEXP, int, const char*);
void printVector(SEXP, int, int);

int F77_SYMBOL(dblepr0)(const char *, int *, double *, int *);
int F77_SYMBOL(intpr0) (const char *, int *, int *, int *);
int F77_SYMBOL(realpr0)(const char *, int *, float *, int *);
void hyang_PV(SEXP s);

#define HYANG_MIN_LBLOFF        2
#define HYANG_MIN_WIDTH_OPT	10
#define HYANG_MAX_WIDTH_OPT	10000
#define HYANG_MIN_DIGITS_OPT	0
#define HYANG_MAX_DIGITS_OPT	22

#endif
