/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGPARSE_H
#define HYANGPARSE_H

#include <hyangexts/hyangextparse.h>
#include <hyangio.h>

typedef struct SrcRefState SrcRefState;

struct SrcRefState {
    hyangboolean keepSrcRefs;
    hyangboolean didAttach;
    SEXP SrcFile;
    SEXP Original;
    PROTECT_INDEX SrcFileProt;
    PROTECT_INDEX OriginalProt;
    SEXP data;
    SEXP text;
    SEXP ids;
    int data_count;
    int xxlineno;
    int xxcolno;
    int xxbyteno;
    int xxparseno;

    SrcRefState* prevState;
};

void InitParser(void);

void hyang_InitSrcRefState(void);
void hyang_FinSrcRefState(void);

SEXP hyang_Parse1Buff(IoBuffer*, int, ParseStatus *);
SEXP hyang_ParseBuff(IoBuffer*, int, ParseStatus *, SEXP, SEXP);
SEXP hyang_Parse1File(FILE*, int, ParseStatus *);
SEXP hyang_ParseFile(FILE*, int, ParseStatus *, SEXP);

#ifndef HAVE_HYANGCONNECTION_TYPEDEF
typedef struct HyangConn  *hyangconn;
#define HAVE_HYANGCONNECTION_TYPEDEF
#endif
SEXP hyang_ParseConn(hyangconn con, int n, ParseStatus *status, SEXP srcfile);

void NORET parseError(SEXP call, int linenum);

#endif
