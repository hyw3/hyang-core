/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYANGGRAPH_H_
#define HYANGGRAPH_H_

typedef enum {
 DEVICE	= 0,	/* native device coordinates (rasters) */
 NDC	= 1,	/* normalised device coordinates x=(0,1), y=(0,1) */
 INCHES = 13,	/* inches x=(0,width), y=(0,height) */
 NIC	= 6,	/* normalised inner region coordinates (0,1) */
 OMA1	= 2,	/* outer margin 1 (bottom) x=NIC, y=LINES */
 OMA2	= 3,	/* outer margin 2 (left) */
 OMA3	= 4,	/* outer margin 3 (top) */
 OMA4	= 5,	/* outer margin 4 (right) */
 NFC	= 7,	/* normalised figure region coordinates (0,1) */
 NPC	= 16,	/* normalised plot region coordinates (0,1) */
 USER	= 12,	/* user/data/world coordinates;
		 * x,=(xmin,xmax), y=(ymin,ymax) */
 MAR1	= 8,	/* figure margin 1 (bottom) x=USER(x), y=LINES */
 MAR2	= 9,	/* figure margin 2 (left)   x=USER(y), y=LINES */
 MAR3	= 10,	/* figure margin 3 (top)    x=USER(x), y=LINES */
 MAR4	= 11,	/* figure margin 4 (right)  x=USER(y), y=LINES */
 LINES = 14,	/* multiples of a line in the margin (mex) */
 CHARS = 15	/* multiples of text height (cex) */
} GUnit;


#define currentFigureLocation	hyangly_currentFigLoc
#define GArrow			hyangly_GrArrow
#define GBox			hyangly_GrBox
#define GCheckState		hyangly_GrCheckState
#define GCircle			hyangly_GrCircle
#define GClip			hyangly_GrClip
#define GClipPolygon		hyangly_GrClipPolygon
#define GConvert		hyangly_GrConvert
#define GConvertX		hyangly_GrConvertX
#define GConvertXUnits		hyangly_GrConvertXUnits
#define GConvertY		hyangly_GrConvertY
#define GConvertYUnits		hyangly_GrConvertYUnits
#define GExpressionHeight	hyangly_GrExpressionHeight
#define GExpressionWidth	hyangly_GrExpressionWidth
#define GForceClip		hyangly_GrForceClip
#define GLine			hyangly_GrLine
#define GLocator		hyangly_GrLocator
#define GMapUnits		hyangly_GrMapUnits
#define GMapWin2Fig		hyangly_GrMapWin2Fig
#define GMathText		hyangly_GrMathText
#define GMetricInfo		hyangly_GrMetricInfo
#define GMMathText		hyangly_GrMMathText
#define GMode			hyangly_GrMode
#define GMtext			hyangly_GrMtext
#define GNewPlot		hyangly_GrNewPlot
#define GPath   		hyangly_GrPath
#define GPolygon		hyangly_GrPolygon
#define GPolyline		hyangly_GrPolyline
#define GPretty			hyangly_GPretty
#define GRect			hyangly_GrRect
#define GRaster			hyangly_GrRaster
#define GReset			hyangly_GrReset
#define GRestore		hyangly_GrRestore
#define GRestorePars	        hyangly_GrRestorePars
#define GSavePars		hyangly_GrSavePars
#define GScale			hyangly_GrScale
#define GSetState		hyangly_GrSetState
#define GSetupAxis		hyangly_GrSetupAxis
#define GStrHeight		hyangly_GrStrHeight
#define GStrWidth		hyangly_GrStrWidth
#define GSymbol			hyangly_GSym
#define GText			hyangly_GrText
#define GVStrHeight		hyangly_GrVStrHeight
#define GVStrWidth		hyangly_GrVStrWidth
#define GVText			hyangly_GrVText

#define xDevtoNDC		hyangly_xDevtoNDC
#define xDevtoNFC		hyangly_xDevtoNFC
#define xDevtoNPC		hyangly_xDevtoNPC
#define xDevtoUsr		hyangly_xDevtoUsr
#define xNPCtoUsr		hyangly_xNPCtoUsr
#define yDevtoNDC		hyangly_yDevtoNDC
#define yDevtoNFC		hyangly_yDevtoNFC
#define yDevtoNPC		hyangly_yDevtoNPC
#define yDevtoUsr		hyangly_yDevtoUsr
#define yNPCtoUsr		hyangly_yNPCtoUsr

void GRestore(pGEDevDesc);
void GSavePars(pGEDevDesc);
void GRestorePars(pGEDevDesc);

void GCheckState(pGEDevDesc);
void GSetState(int, pGEDevDesc);

void GCircle(double, double, int, double, int, int, pGEDevDesc);
void GClip(pGEDevDesc);
int GClipPolygon(double *, double *, int, int, int,
		 double *, double *, pGEDevDesc);
void GForceClip(pGEDevDesc);
void GLine(double, double, double, double, int, pGEDevDesc);
hyangboolean GLocator(double*, double*, int, pGEDevDesc);
void GMetricInfo(int, double*, double*, double*, GUnit, pGEDevDesc);
void GMode(int, pGEDevDesc);
void GPath(double*, double*, int, int*, hyangboolean, int, int, pGEDevDesc);
void GPolygon(int, double*, double*, int, int, int, pGEDevDesc);
void GPolyline(int, double*, double*, int, pGEDevDesc);
void GRect(double, double, double, double, int, int, int, pGEDevDesc);
void GRaster(unsigned int*, int, int,
             double, double, double, double,
             double, hyangboolean, pGEDevDesc);
double GStrHeight(const char *, cetype_t, GUnit, pGEDevDesc);
double GStrWidth(const char *, cetype_t, GUnit, pGEDevDesc);
void GText(double, double, int, const char *, cetype_t, double, double, double,
	   pGEDevDesc);

void GMathText(double, double, int, SEXP, double, double, double, pGEDevDesc);
void GMMathText(SEXP, int, double, int, double, int, double, pGEDevDesc);
void GArrow(double, double, double, double, int, double, double, int, pGEDevDesc);
void GBox(int, pGEDevDesc);
void GPretty(double*, double*, int*);
void GMtext(const char *, cetype_t, int, double, int, double, int, double, pGEDevDesc);
void GSymbol(double, double, int, int, pGEDevDesc);
double GExpressionHeight(SEXP, GUnit, pGEDevDesc);
double GExpressionWidth(SEXP, GUnit, pGEDevDesc);

GUnit GMapUnits(int);
void GConvert(double*, double*, GUnit, GUnit, pGEDevDesc);
double GConvertX(double, GUnit, GUnit, pGEDevDesc);
double GConvertY(double, GUnit, GUnit, pGEDevDesc);
double GConvertXUnits(double, GUnit, GUnit, pGEDevDesc);
double GConvertYUnits(double, GUnit, GUnit, pGEDevDesc);

void GReset(pGEDevDesc);

void GMapWin2Fig(pGEDevDesc);
pGEDevDesc GNewPlot(hyangboolean);
void GScale(double, double, int, pGEDevDesc);
void GSetupAxis(int, pGEDevDesc);
void currentFigureLocation(int*, int*, pGEDevDesc);

double xDevtoNDC(double, pGEDevDesc);
double yDevtoNDC(double, pGEDevDesc);
double xDevtoNFC(double, pGEDevDesc);
double yDevtoNFC(double, pGEDevDesc);
double xDevtoNPC(double, pGEDevDesc);
double yDevtoNPC(double, pGEDevDesc);
double xDevtoUsr(double, pGEDevDesc);
double yDevtoUsr(double, pGEDevDesc);
double xNPCtoUsr(double, pGEDevDesc);
double yNPCtoUsr(double, pGEDevDesc);

#endif /* HYANGGRAPH_H_ */
