/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYANGWARNS_H
#define HYANGWARNS_H

#define hyang_MSG_subs_o_b _("subscript out of bounds")
#define hyang_MSG_ob_nonsub _("object of type '%s' is not subsettable")

typedef enum {
    ERROR_NUMARGS = 1,
    ERROR_ARGTYPE = 2,
    ERROR_INCOMPAT_ARGS = 3,
    ERROR_TSVEC_MISMATCH = 100,
    ERROR_UNIMPLEMENTED	= 9998,
    ERROR_UNKNOWN = 9999
} HYANG_ERROR;

typedef enum {
    WARNING_coerce_NA	= 101,
    WARNING_coerce_INACC= 102,
    WARNING_coerce_IMAG = 103,
    WARNING_UNKNOWN = 9999
} HYANG_WARNING;

#endif

