/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef HYANG_DEFINES_H
#define HYANG_DEFINES_H

#if !defined(HYANG_HYANG_H) && !defined(HYANG_SCM_H)
# include <hyangexts/hyangmem.h>
# include <hyangexts/hyangscm.h>
#endif

#include <hyangintls.h>

#define NULL_USER_OBJECT	hyang_AbsurdValue

#define AS_LOGICAL(x)		hyangly_coeVect(x,LGLSXP)
#define AS_INTEGER(x)		hyangly_coeVect(x,INTSXP)
#define AS_NUMERIC(x)		hyangly_coeVect(x,REALSXP)
#define AS_CHARACTER(x)		hyangly_coeVect(x,STRSXP)
#define AS_COMPLEX(x)		hyangly_coeVect(x,CPLXSXP)
#define AS_VECTOR(x)		hyangly_coeVect(x,VECSXP)
#define AS_LIST(x)		hyangly_coeVect(x,VECSXP)
#define AS_RAW(x)		hyangly_coeVect(x,RAWSXP)

#ifdef USE_HYANGINTLS
# define IS_LOGICAL(x)		isLogical(x)
# define IS_INTEGER(x)		isInteger(x)
# define IS_NUMERIC(x)		isReal(x)
# define IS_CHARACTER(x)	isString(x)
# define IS_COMPLEX(x)		isComplex(x)
#else
# define IS_LOGICAL(x)		hyangly_isLogical(x)
# define IS_INTEGER(x)		hyangly_isInteger(x)
# define IS_NUMERIC(x)		hyangly_isReal(x)
# define IS_CHARACTER(x)	hyangly_isString(x)
# define IS_COMPLEX(x)		hyangly_isComplex(x)
#endif
#define IS_VECTOR(x)		hyangly_isVect(x)
#define IS_LIST(x)		IS_VECTOR(x)
#define IS_RAW(x)		(TYPEOF(x) == RAWSXP)
#define NEW_LOGICAL(n)		hyangly_allocVect(LGLSXP,n)
#define NEW_INTEGER(n)		hyangly_allocVect(INTSXP,n)
#define NEW_NUMERIC(n)		hyangly_allocVect(REALSXP,n)
#define NEW_CHARACTER(n)	hyangly_allocVect(STRSXP,n)
#define NEW_COMPLEX(n)		hyangly_allocVect(CPLXSXP,n)
#define NEW_LIST(n)		hyangly_allocVect(VECSXP,n)
#define NEW_STRING(n)		NEW_CHARACTER(n)
#define NEW_RAW(n)		hyangly_allocVect(RAWSXP,n)
#define LOGICAL_POINTER(x)	LOGICAL(x)
#define INTEGER_POINTER(x)	INTEGER(x)
#define NUMERIC_POINTER(x)	REAL(x)
#define CHARACTER_POINTER(x)	STRING_PTR(x)
#define COMPLEX_POINTER(x)	COMPLEX(x)
#define LIST_POINTER(x)		VECTOR_PTR(x)
#define RAW_POINTER(x)		RAW(x)
#define LOGICAL_DATA(x)		(LOGICAL(x))
#define INTEGER_DATA(x)		(INTEGER(x))
#define DOUBLE_DATA(x)		(REAL(x))
#define NUMERIC_DATA(x)		(REAL(x))
#define CHARACTER_DATA(x)	(STRING_PTR(x))
#define COMPLEX_DATA(x)		(COMPLEX(x))
#define RECURSIVE_DATA(x)	(VECTOR_PTR(x))
#define VECTOR_DATA(x)		(VECTOR_PTR(x))
#define LOGICAL_VALUE(x)	hyangly_asLogical(x)
#define INTEGER_VALUE(x)	hyangly_asInteger(x)
#define NUMERIC_VALUE(x)	hyangly_asReal(x)
#define CHARACTER_VALUE(x)	CHAR(hyangly_asChar(x))
#define STRING_VALUE(x)		CHAR(hyangly_asChar(x))
#define LIST_VALUE(x)		hyangly_error("the 'value' of a list object is not defined")
#define RAW_VALUE(x)		hyangly_error("the 'value' of a raw object is not defined")
#define SET_ELEMENT(x, i, val)	SET_VECTOR_ELT(x, i, val)
#define GET_ATTR(x,what)       	hyangly_getAttr(x, what)
#define GET_CLASS(x)       	hyangly_getAttr(x, hyang_ClassSym)
#define GET_DIM(x)       	hyangly_getAttr(x, hyang_DimSym)
#define GET_DIMNAMES(x)       	hyangly_getAttr(x, hyang_DimNamesSym)
#define GET_COLNAMES(x)       	hyangly_getColNames(x)
#define GET_ROWNAMES(x)       	hyangly_getRowNames(x)
#define GET_LEVELS(x)       	hyangly_getAttr(x, hyang_LevelsSym)
#define GET_TSP(x)       	hyangly_getAttr(x, hyang_TspSym)
#define GET_NAMES(x)		hyangly_getAttr(x, hyang_NamesSym)
#define SET_ATTR(x, what, n)    hyangly_setAttr(x, what, n)
#define SET_CLASS(x, n)     	hyangly_setAttr(x, hyang_ClassSym, n)
#define SET_DIM(x, n)     	hyangly_setAttr(x, hyang_DimSym, n)
#define SET_DIMNAMES(x, n)     	hyangly_setAttr(x, hyang_DimNamesSym, n)
#define SET_LEVELS(x, l)       	hyangly_setAttr(x, hyang_LevelsSym, l)
#define SET_NAMES(x, n)		hyangly_setAttr(x, hyang_NamesSym, n)
#define GET_LENGTH(x)		hyangly_length(x)
#define SET_LENGTH(x, n)	(x = hyangly_getLength(x, n))
#define GET_SLOT(x, what)       hyangto_slot(x, what)
#define SET_SLOT(x, what, value)  hyangto_slotAssign(x, what, value)
#define MAKE_CLASS(what)	hyangto_MAKE_CLASS(what)
#define NEW_OBJECT(class_def)	hyangto_NewObj(class_def)
#define NEW(class_def)		hyangto_NewObj(class_def)
#define s_object                SEXPREC
#define S_EVALUATOR

#ifdef __cplusplus
# ifndef HYANG_EXT_BOOLEAN_H_
#  ifndef TRUE
#   define TRUE 1
#  endif
#  ifndef FALSE
#   define FALSE 0
#  endif
# endif
#else
#  ifndef TRUE
#   define TRUE 1
#  endif
#  ifndef FALSE
#   define FALSE 0
#  endif
#endif

#define COPY_TO_USER_STRING(x)	mkChar(x)
#define CREATE_STRING_VECTOR(x)	mkChar(x)

#define CREATE_FUNCTION_CALL(name, argList) createFunctionCall(name, argList)

#define EVAL(x)			eval(x,hyang_GlobalEnv)

#endif
