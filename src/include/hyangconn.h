/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_CONN_H_
#define HYANG_CONN_H_

#include <hyangexts/hyangextconn.h>

typedef enum {HTTPsh, FTPsh, HTTPSsh, FTPSsh} UrlScheme;

typedef struct urlconn {
    void *ctxt;
    UrlScheme type;
} *Rurlconn;

typedef struct sockconn {
    int port;
    int server;
    int fd;
    int timeout;
    char *host;
    char inbuf[4096], *pstart, *pend;
} *hyangsockconn;

typedef struct clpconn {
    char *buff;
    int pos, len, last, sizeKB;
    hyangboolean warned;
} *hyangclpconn;

#define init_con	hyangly_init_con
#define con_pushback	hyangly_con_pushback

int hyangconn_fgetc(hyangconn con);
int hyangconn_ungetc(int c, hyangconn con);
size_t hyangconn_getline(hyangconn con, char *buf, size_t bufsize);
int hyangconn_printf(hyangconn con, const char *format, ...);
hyangconn getConnection(int n);
hyangconn getConnection_no_err(int n);
hyangboolean switch_stdout(int icon, int closeOnExit);
void init_con(hyangconn new, const char *description, int enc,
	      const char * const mode);
hyangconn hyang_newurl(const char *description, const char * const mode, int type);
hyangconn hyang_newsock(const char *host, int port, int server, const char * const mode, int timeout);
hyangconn in_hyang_newsock(const char *host, int port, int server, const char *const mode, int timeout);
hyangconn hyang_newunz(const char *description, const char * const mode);
int dummy_fgetc(hyangconn con);
int dummy_vfprintf(hyangconn con, const char *format, va_list ap);
int getActiveSink(int n);
void con_pushback(hyangconn con, hyangboolean newLine, char *line);

int hyangsockselect(int nsock, int *insockfd, int *ready, int *write, double timeout);

#define set_iconv hyangly_set_iconv
void set_iconv(hyangconn con);
#endif

