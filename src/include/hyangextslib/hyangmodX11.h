/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Definitions for the X11 module.  Not intended for end-user use */

#ifndef HYANG_X11_MODULE_H
#define HYANG_X11_MODULE_H

#include <hyangintls.h>
#include <hyangconn.h>

typedef SEXP (*hyangto_X11)(SEXP call, SEXP op, SEXP args, SEXP rho);
typedef SEXP (*hyang_X11DataEntryRoutine)(SEXP call, SEXP op, SEXP args, SEXP rho);
typedef SEXP (*hyang_X11DataViewer)(SEXP call, SEXP op, SEXP args, SEXP rho);
typedef hyangboolean (*hyang_GetX11ImageRoutine)(int d, void *pximage, 
					 int *pwidth, int *pheight);
typedef int (*hyang_X11_access)(void);

typedef hyangboolean (*hyang_X11clp)(hyangclpconn, char*);

typedef const char * (*hyang_version_t)(void);


typedef struct {
    hyangto_X11 X11;
    hyangto_X11 saveplot;
    hyang_GetX11ImageRoutine  image;
    hyang_X11_access access;
    hyang_X11clp readclp;
    hyang_version_t hyang_pngVersion, hyang_jpegVersion, hyang_tiffVersion;
} hyang_X11Routines;

typedef struct {
    hyang_X11DataEntryRoutine de;
    hyang_X11DataViewer dv;
} hyang_deRoutines;

#ifdef __cplusplus
extern "C" {
#endif
hyang_X11Routines *hyang_setX11Routines(hyang_X11Routines *routines);
hyang_deRoutines *hyang_setdeRoutines(hyang_deRoutines *routines);
#ifdef __cplusplus
}
#endif

#endif /* HYANG_X11_MODULE_H */
