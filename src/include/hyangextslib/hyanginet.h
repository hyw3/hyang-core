/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_INTERNET_MODULE_H
#define HYANG_INTERNET_MODULE_H

#include <hyangintls.h>

typedef SEXP (*hyang_DownloadRoutine)(SEXP args);
typedef hyangconn (*hyang_NewUrlRoutine)(const char *description, const char * const mode, int method);
typedef hyangconn (*hyang_NewSockRoutine)(const char *host, int port, int server, const char *const mode, int timeout);

typedef void * (*hyang_HTTPOpenRoutine)(const char *url, const char *headers, const int cacheOK);
typedef int    (*hyang_HTTPReadRoutine)(void *ctx, char *dest, int len);
typedef void   (*hyang_HTTPCloseRoutine)(void *ctx);

typedef void * (*hyang_FTPOpenRoutine)(const char *url);
typedef int    (*hyang_FTPReadRoutine)(void *ctx, char *dest, int len);
typedef void   (*hyang_FTPCloseRoutine)(void *ctx);

typedef void   (*hyang_SockOpenRoutine)(int *port);
typedef void   (*hyang_SockListenRoutine)(int *sockp, char **buf, int *len);
typedef void   (*hyang_SockConnectRoutine)(int *port, char **host);
typedef void   (*hyang_SockCloseRoutine)(int *sockp);

typedef void   (*hyang_SockReadRoutine)(int *sockp, char **buf, int *maxlen);
typedef void   (*hyang_SockWriteRoutine)(int *sockp, char **buf, int *start, int *end, int *len);
typedef int    (*hyang_SockSelectRoutine)(int nsock, int *insockfd, int *ready, int *write, double timeout);

typedef int    (*hyang_WWHYCreateRoutine)(const char *ip, int port);
typedef void   (*hyang_WWHYStopRoutine)();

typedef SEXP (*hyang_CurlRoutine)(SEXP call, SEXP op, SEXP args, SEXP rho);

typedef struct {
    hyang_DownloadRoutine     download;
    hyang_NewUrlRoutine       newurl;
    hyang_NewSockRoutine      newsock;

    hyang_HTTPOpenRoutine     HTTPOpen;
    hyang_HTTPReadRoutine     HTTPRead;
    hyang_HTTPCloseRoutine    HTTPClose;

    hyang_FTPOpenRoutine      FTPOpen;
    hyang_FTPReadRoutine      FTPRead;
    hyang_FTPCloseRoutine     FTPClose;

    hyang_SockOpenRoutine     sockopen;
    hyang_SockListenRoutine   socklisten;
    hyang_SockConnectRoutine  sockconnect;
    hyang_SockCloseRoutine    sockclose;

    hyang_SockReadRoutine     sockread;
    hyang_SockWriteRoutine    sockwrite;
    hyang_SockSelectRoutine   sockselect;

    hyang_WWHYCreateRoutine   WWHYCreate;
    hyang_WWHYStopRoutine     WWHYStop;

    hyang_CurlRoutine         curlVersion;
    hyang_CurlRoutine         curlGetHeaders;
    hyang_CurlRoutine         curlDownload;
    hyang_NewUrlRoutine       newcurlurl;
} hyang_inetRoutines;

hyang_inetRoutines *hyang_setInetRoutines(hyang_inetRoutines *routines);

#endif /* ifndef HYANG_INTERNET_MODULE_H */
