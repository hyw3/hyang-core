/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGDEFN_H_
#define HYANGDEFN_H_

#define COUNTING

#define BYTECODE

#define NEW_CONDITION_HANDLING

#ifndef TESTING_WRITE_BARRIER
# define USE_HYANGINTLS
#endif

#ifdef HAVE_VISIBILITY_ATTRIBUTE
# define attribute_visible __attribute__ ((visibility ("default")))
# define attribute_hidden __attribute__ ((visibility ("hidden")))
#else
# define attribute_visible
# define attribute_hidden
#endif

#ifdef __MAIN__
# define extern0 attribute_hidden
#else
# define extern0 extern
#endif

#define MAXELTSIZE 8192

#include <hyangexts/hyangcomplex.h>
void hyangly_CoercWarning(int);/* warning code */
int hyangly_LogicalFromInt(int, int*);
int hyangly_LogicalFromReal(double, int*);
int hyangly_LogicalFromCmplx(hyangcomplex, int*);
int hyangly_IntFromLogical(int, int*);
int hyangly_IntFromReal(double, int*);
int hyangly_IntFromCmplx(hyangcomplex, int*);
double hyangly_RealFromLogical(int, int*);
double hyangly_RealFromInt(int, int*);
double hyangly_RealFromCmplx(hyangcomplex, int*);
hyangcomplex hyangly_CmplxFromLogical(int, int*);
hyangcomplex hyangly_CmplxFromInt(int, int*);
hyangcomplex hyangly_CmplxFromReal(double, int*);

#define CALLED_FROM_DEFN_H 1
#include <hyangintls.h>
#undef CALLED_FROM_DEFN_H
extern0 SEXP	hyang_CommentSym;       /* "comment" */
extern0 SEXP	hyang_DotEnvSym;        /* ".Environment" */
extern0 SEXP	hyang_ExactSym;	        /* "exact" */
extern0 SEXP	hyang_RecursiveSym;     /* "recursive" */
extern0 SEXP	hyang_WholeSrcRefSym;   /* "wholeSrcref" */
extern0 SEXP	hyang_TmpValSym;        /* "*tmp*" */
extern0 SEXP	hyang_UseNamesSym;      /* "use.names" */
extern0 SEXP	hyang_ColonSym;         /* ":" */
//extern0 SEXP	hyang_DoubleColonSym;   /* "::" */
//extern0 SEXP	hyang_TripleColonSym;   /* ":::" */
extern0 SEXP    hyang_ConnIdSym;        /* "conn_id" */
extern0 SEXP    hyang_DevicesSym;       /* ".Devices" */

extern0 SEXP    hyang_dot_Methods;      /* ".Methods" */
extern0 SEXP    hyang_dot_Group;        /* ".Group" */
extern0 SEXP    hyang_dot_Class;        /* ".Class" */
extern0 SEXP    hyang_dot_GenCallEnv;   /* ".GenericCallEnv" */
extern0 SEXP    hyang_dot_GenDefEnv;    /* ".GenericDefEnv" */

extern0 SEXP	hyang_StrHash;           /* Global hash of CHARSXPs */

#define CHAR_RW(x)	((char *) CHAR(x))

#define BYTES_MASK (1<<1)
#define LATIN1_MASK (1<<2)
#define UTF8_MASK (1<<3)
#define CACHED_MASK (1<<5)
#define ASCII_MASK (1<<6)
#define HASHASH_MASK 1

#ifdef USE_HYANGINTLS
# define IS_BYTES(x) ((x)->sxpinfo.gp & BYTES_MASK)
# define SET_BYTES(x) (((x)->sxpinfo.gp) |= BYTES_MASK)
# define IS_LATIN1(x) ((x)->sxpinfo.gp & LATIN1_MASK)
# define SET_LATIN1(x) (((x)->sxpinfo.gp) |= LATIN1_MASK)
# define IS_ASCII(x) ((x)->sxpinfo.gp & ASCII_MASK)
# define SET_ASCII(x) (((x)->sxpinfo.gp) |= ASCII_MASK)
# define IS_UTF8(x) ((x)->sxpinfo.gp & UTF8_MASK)
# define SET_UTF8(x) (((x)->sxpinfo.gp) |= UTF8_MASK)
# define ENC_KNOWN(x) ((x)->sxpinfo.gp & (LATIN1_MASK | UTF8_MASK))
# define SET_CACHED(x) (((x)->sxpinfo.gp) |= CACHED_MASK)
# define IS_CACHED(x) (((x)->sxpinfo.gp) & CACHED_MASK)
#else

int IS_BYTES(SEXP x);
void SET_BYTES(SEXP x);
int IS_LATIN1(SEXP x);
void SET_LATIN1(SEXP x);
int IS_ASCII(SEXP x);
void SET_ASCII(SEXP x);
int IS_UTF8(SEXP x);
void SET_UTF8(SEXP x);
int ENC_KNOWN(SEXP x);
int SET_CACHED(SEXP x);
int IS_CACHED(SEXP x);
#endif

# define CXHEAD(x) (x)
# define CXTAIL(x) ATTRIB(x)
SEXP (SET_CXTAIL)(SEXP x, SEXP y);

#include "hyangwarns.h"

extern void hyang_ProcessEvents(void);
#ifdef Win32
extern void hyang_WaitEvent(void);
#endif

#ifdef HYANG_USE_SIGNALS
#ifdef Win32
# include <psignal.h>
#else
# include <signal.h>
# include <setjmp.h>
#endif
#endif

#ifdef Unix
# define OSTYPE      "unix"
# define FILESEP     "/"
#endif /* Unix */

#ifdef Win32
# define OSTYPE      "windows"
# define FILESEP     "/"
#endif /* Win32 */

#ifdef HAVE_F77_UNDERSCORE
# define F77_SYMBOL(x)	x ## _
# define F77_QSYMBOL(x)	#x "_"
#else
# define F77_SYMBOL(x)	x
# define F77_QSYMBOL(x) #x
#endif

#ifdef HAVE_INTTYPES_H
# include <inttypes.h>
#endif

#ifdef HAVE_STDINT_H
# include <stdint.h>
#endif
#ifdef HAVE_LIMITS_H
# include <limits.h>
#endif

#if defined HAVE_DECL_SIZE_MAX && HAVE_DECL_SIZE_MAX
  typedef size_t hyang_size_t;
# define HYANG_SIZE_T_MAX SIZE_MAX
#else
# error SIZE_MAX is required for C99
#endif


#define Mega 1048576.
#define Giga 1073741824.

#ifndef HYANG_PPSSIZE
#define	HYANG_PPSSIZE	50000L
#endif
#ifndef HYANG_NSIZE
#define	HYANG_NSIZE		350000L
#endif
#ifndef HYANG_VSIZE
#define	HYANG_VSIZE		67108864L
#endif

#include <math.h>
#include <stdlib.h>
#include <string.h>

#if !defined(strdup) && defined(HAVE_DECL_STRDUP) && !HAVE_DECL_STRDUP
extern char *strdup(const char *s1);
#endif
#if !defined(strncascmp) && defined(HAVE_DECL_STRNCASECMP) && !HAVE_DECL_STRNCASECMP
extern int strncasecmp(const char *s1, const char *s2, size_t n);
#endif

#if defined(HAVE_PUTENV) && !defined(putenv) && defined(HAVE_DECL_PUTENV) && !HAVE_DECL_PUTENV
extern int putenv(char *string);
#endif


#if !defined(PATH_MAX)
# if defined(HAVE_SYS_PARAM_H)
#  include <sys/param.h>
# endif
# if !defined(PATH_MAX)
#  if defined(MAXPATHLEN)
#    define PATH_MAX MAXPATHLEN
#  elif defined(Win32)
#    define PATH_MAX 260
#  else
#    define PATH_MAX 5000
#  endif
# endif
#endif

#ifdef HYANG_USE_SIGNALS
#ifdef HAVE_POSIX_SETJMP
# define SIGJMP_BUF sigjmp_buf
# define SIGSETJMP(x,s) sigsetjmp(x,s)
# define SIGLONGJMP(x,i) siglongjmp(x,i)
# define JMP_BUF sigjmp_buf
# define SETJMP(x) sigsetjmp(x,0)
# define LONGJMP(x,i) siglongjmp(x,i)
#else
# define SIGJMP_BUF jmp_buf
# define SIGSETJMP(x,s) setjmp(x)
# define SIGLONGJMP(x,i) longjmp(x,i)
# define JMP_BUF jmp_buf
# define SETJMP(x) setjmp(x)
# define LONGJMP(x,i) longjmp(x,i)
#endif
#endif

#define HSIZE	  49157
#define MAXIDSIZE 10000

typedef SEXP (*CCODE)(SEXP, SEXP, SEXP, SEXP);

typedef enum {
    PP_INVALID  =  0,
    PP_ASSIGN   =  1,
    PP_ASSIGN2  =  2,
    PP_BINARY   =  3,
    PP_BINARY2  =  4,
    PP_BREAK    =  5,
    PP_CURLY    =  6,
    PP_FOR      =  7,
    PP_FUNCALL  =  8,
    PP_FUNCTION =  9,
    PP_IF 	= 10,
    PP_NEXT 	= 11,
    PP_PAREN    = 12,
    PP_RETURN   = 13,
    PP_SUBASS   = 14,
    PP_SUBSET   = 15,
    PP_WHILE 	= 16,
    PP_UNARY 	= 17,
    PP_DOLLAR 	= 18,
    PP_FOREIGN 	= 19,
    PP_REPEAT 	= 20
} PPkind;

typedef enum {
    PREC_FN	 = 0,
    PREC_EQ	 = 1,
    PREC_LEFT    = 2,
    PREC_RIGHT	 = 3,
    PREC_TILDE	 = 4,
    PREC_OR	 = 5,
    PREC_AND	 = 6,
    PREC_NOT	 = 7,
    PREC_COMPARE = 8,
    PREC_SUM	 = 9,
    PREC_PROD	 = 10,
    PREC_PERCENT = 11,
    PREC_COLON	 = 12,
    PREC_SIGN	 = 13,
    PREC_POWER	 = 14,
    PREC_SUBSET  = 15,
    PREC_DOLLAR	 = 16,
    PREC_NS	 = 17
} PPprec;

typedef struct {
	PPkind kind;
	PPprec precedence;
	unsigned int rightassoc;
} PPinfo;

typedef struct {
    char   *name;    /* print name */
    CCODE  cfun;     /* c-code address */
    int	   code;     /* offset within c-code */
    int	   eval;     /* evaluate args? */
    int	   arity;    /* function arity */
    PPinfo gram;     /* pretty-print info */
} FUNTAB;

#ifdef USE_HYANGINTLS

#define PRIMOFFSET(x)	((x)->u.primsxp.offset)
#define SET_PRIMOFFSET(x,v)	(((x)->u.primsxp.offset)=(v))
#define PRIMFUN(x)	(hyang_FunTab[(x)->u.primsxp.offset].cfun)
#define PRIMNAME(x)	(hyang_FunTab[(x)->u.primsxp.offset].name)
#define PRIMVAL(x)	(hyang_FunTab[(x)->u.primsxp.offset].code)
#define PRIMARITY(x)	(hyang_FunTab[(x)->u.primsxp.offset].arity)
#define PPINFO(x)	(hyang_FunTab[(x)->u.primsxp.offset].gram)
#define PRIMPRINT(x)	(((hyang_FunTab[(x)->u.primsxp.offset].eval)/100)%10)
#define PRIMINTERNAL(x)	(((hyang_FunTab[(x)->u.primsxp.offset].eval)%100)/10)

#define PRCODE(x)	((x)->u.promsxp.expr)
#define PRENV(x)	((x)->u.promsxp.env)
#define PRVALUE(x)	((x)->u.promsxp.value)
#define PRSEEN(x)	((x)->sxpinfo.gp)
#define SET_PRSEEN(x,v)	(((x)->sxpinfo.gp)=(v))

#define HASHASH(x)      ((x)->sxpinfo.gp & HASHASH_MASK)
#define HASHVALUE(x)    ((int) TRUELENGTH(x))
#define SET_HASHASH(x,v) ((v) ? (((x)->sxpinfo.gp) |= HASHASH_MASK) : \
			  (((x)->sxpinfo.gp) &= (~HASHASH_MASK)))
#define SET_HASHVALUE(x,v) SET_TRUELENGTH(x, ((int) (v)))

typedef struct {
	union {
		SEXP		backpointer;
		double		align;
	} u;
} VECREC, *VECP;

#define BACKPOINTER(v)	((v).u.backpointer)
#define BYTE2VEC(n)	(((n)>0)?(((n)-1)/sizeof(VECREC)+1):0)
#define INT2VEC(n)	(((n)>0)?(((n)*sizeof(int)-1)/sizeof(VECREC)+1):0)
#define FLOAT2VEC(n)	(((n)>0)?(((n)*sizeof(double)-1)/sizeof(VECREC)+1):0)
#define COMPLEX2VEC(n)	(((n)>0)?(((n)*sizeof(hyangcomplex)-1)/sizeof(VECREC)+1):0)
#define PTR2VEC(n)	(((n)>0)?(((n)*sizeof(SEXP)-1)/sizeof(VECREC)+1):0)

#define ACTIVE_BINDING_MASK (1<<15)
#define BINDING_LOCK_MASK (1<<14)
#define SPECIAL_BINDING_MASK (ACTIVE_BINDING_MASK | BINDING_LOCK_MASK)
#define IS_ACTIVE_BINDING(b) ((b)->sxpinfo.gp & ACTIVE_BINDING_MASK)
#define BINDING_IS_LOCKED(b) ((b)->sxpinfo.gp & BINDING_LOCK_MASK)
#define SET_ACTIVE_BINDING_BIT(b) ((b)->sxpinfo.gp |= ACTIVE_BINDING_MASK)
#define LOCK_BINDING(b) ((b)->sxpinfo.gp |= BINDING_LOCK_MASK)
#define UNLOCK_BINDING(b) ((b)->sxpinfo.gp &= (~BINDING_LOCK_MASK))

#define BASE_SYM_CACHED_MASK (1<<13)
#define SET_BASE_SYM_CACHED(b) ((b)->sxpinfo.gp |= BASE_SYM_CACHED_MASK)
#define UNSET_BASE_SYM_CACHED(b) ((b)->sxpinfo.gp &= (~BASE_SYM_CACHED_MASK))
#define BASE_SYM_CACHED(b) ((b)->sxpinfo.gp & BASE_SYM_CACHED_MASK)

#define SPECIAL_SYMBOL_MASK (1<<12)
#define SET_SPECIAL_SYMBOL(b) ((b)->sxpinfo.gp |= SPECIAL_SYMBOL_MASK)
#define UNSET_SPECIAL_SYMBOL(b) ((b)->sxpinfo.gp &= (~SPECIAL_SYMBOL_MASK))
#define IS_SPECIAL_SYMBOL(b) ((b)->sxpinfo.gp & SPECIAL_SYMBOL_MASK)
#define SET_NO_SPECIAL_SYMBOLS(b) ((b)->sxpinfo.gp |= SPECIAL_SYMBOL_MASK)
#define UNSET_NO_SPECIAL_SYMBOLS(b) ((b)->sxpinfo.gp &= (~SPECIAL_SYMBOL_MASK))
#define NO_SPECIAL_SYMBOLS(b) ((b)->sxpinfo.gp & SPECIAL_SYMBOL_MASK)

#else /* USE_HYANGINTLS */

typedef struct VECREC *VECP;
int (PRIMOFFSET)(SEXP x);
void (SET_PRIMOFFSET)(SEXP x, int v);

#define PRIMFUN(x)	(hyang_FunTab[PRIMOFFSET(x)].cfun)
#define PRIMNAME(x)	(hyang_FunTab[PRIMOFFSET(x)].name)
#define PRIMVAL(x)	(hyang_FunTab[PRIMOFFSET(x)].code)
#define PRIMARITY(x)	(hyang_FunTab[PRIMOFFSET(x)].arity)
#define PPINFO(x)	(hyang_FunTab[PRIMOFFSET(x)].gram)
#define PRIMPRINT(x)	(((hyang_FunTab[PRIMOFFSET(x)].eval)/100)%10)
#define PRIMINTERNAL(x) (((hyang_FunTab[PRIMOFFSET(x)].eval)%100)/10)

hyangboolean (IS_ACTIVE_BINDING)(SEXP b);
hyangboolean (BINDING_IS_LOCKED)(SEXP b);
void (SET_ACTIVE_BINDING_BIT)(SEXP b);
void (LOCK_BINDING)(SEXP b);
void (UNLOCK_BINDING)(SEXP b);

void (SET_BASE_SYM_CACHED)(SEXP b);
void (UNSET_BASE_SYM_CACHED)(SEXP b);
hyangboolean (BASE_SYM_CACHED)(SEXP b);

void (SET_SPECIAL_SYMBOL)(SEXP b);
void (UNSET_SPECIAL_SYMBOL)(SEXP b);
hyangboolean (IS_SPECIAL_SYMBOL)(SEXP b);
void (SET_NO_SPECIAL_SYMBOLS)(SEXP b);
void (UNSET_NO_SPECIAL_SYMBOLS)(SEXP b);
hyangboolean (NO_SPECIAL_SYMBOLS)(SEXP b);

#endif /* USE_HYANGINTLS */

#define TYPED_STACK
#ifdef TYPED_STACK

typedef struct {
    int tag;
    union {
	int ival;
	double dval;
	SEXP sxpval;
    } u;
} Abah_stack_t;
# define PARTIALSXP_MASK (~255)
# define IS_PARTIAL_SXP_TAG(x) ((x) & PARTIALSXP_MASK)
# define RAWMEM_TAG 254
#else
typedef SEXP Abah_stack_t;
#endif
#ifdef BC_INT_STACK
typedef union { void *p; int i; } IStackval;
#endif

#ifdef HYANG_USE_SIGNALS

typedef struct RPRSTACK {
    SEXP promise;
    struct RPRSTACK *next;
} RPRSTACK;

typedef struct HYANGCTX {
    struct HYANGCTX *nextcontext;
    int callflag;		/* The context "type" */
    JMP_BUF cjmpbuf;		/* C stack and register information */
    int cstacktop;		/* Top of the pointer protection stack */
    int evaldepth;	        /* evaluation depth at inception */
    SEXP promargs;		/* Promises supplied to closure */
    SEXP callfun;		/* The closure called */
    SEXP sysparent;		/* environment the closure was called from */
    SEXP call;			/* The call that effected this context*/
    SEXP cloenv;		/* The environment */
    SEXP conexit;		/* Interpreted "on.exit" code */
    void (*cend)(void *);	/* C "on.exit" thunk */
    void *cenddata;		/* data for C "on.exit" thunk */
    void *vmax;		        /* top of hyang_alloc stack */
    int intsusp;                /* interrupts are suspended */
    int gcenabled;		/* hyang_PBCEnabled value */
    int bcintactive;            /* Abah_IntActive value */
    SEXP bcbody;                /* Abah_body value */
    void* bcpc;                 /* Abah_pc value */
    SEXP handlerstack;          /* condition handler stack */
    SEXP restartstack;          /* stack of available restarts */
    struct RPRSTACK *prstack;   /* stack of pending promises */
    Abah_stack_t *nodestack;
#ifdef BC_INT_STACK
    IStackval *intstack;
#endif
    SEXP srcref;	        /* The source line in effect */
    int browserfinish;          /* should browser finish this context without
                                   stopping */
    SEXP returnValue;           /* only set during on.exit calls */
    struct HYANGCTX *jumptarget;
    int jumpmask;               /* associated LONGJMP argument */
} HYANGCTX, *context;

enum {
    CTXT_TOPLEVEL = 0,
    CTXT_NEXT	  = 1,
    CTXT_BREAK	  = 2,
    CTXT_LOOP	  = 3,
    CTXT_FUNCTION = 4,
    CTXT_CCODE	  = 8,
    CTXT_RETURN	  = 12,
    CTXT_BROWSER  = 16,
    CTXT_GENERIC  = 20,
    CTXT_RESTART  = 32,
    CTXT_BUILTIN  = 64,
    CTXT_UNWIND   = 128
};

/*
TOP   0 0 0 0 0 0  = 0
NEX   1 0 0 0 0 0  = 1
BRE   0 1 0 0 0 0  = 2
LOO   1 1 0 0 0 0  = 3
FUN   0 0 1 0 0 0  = 4
CCO   0 0 0 1 0 0  = 8
BRO   0 0 0 0 1 0  = 16
RET   0 0 1 1 0 0  = 12
GEN   0 0 1 0 1 0  = 20
RES   0 0 0 0 0 0 1 = 32
BUI   0 0 0 0 0 0 0 1 = 64
*/

#define IS_RESTART_BIT_SET(flags) ((flags) & CTXT_RESTART)
#define SET_RESTART_BIT_ON(flags) (flags |= CTXT_RESTART)
#define SET_RESTART_BIT_OFF(flags) (flags &= ~CTXT_RESTART)
#endif

#define streql(s, t)	(!strcmp((s), (t)))

typedef enum {
    PLUSOP = 1,
    MINUSOP,
    TIMESOP,
    DIVOP,
    POWOP,
    MODOP,
    IDIVOP
} ARITHOP_TYPE;

typedef enum {
    EQOP = 1,
    NEOP,
    LTOP,
    LEOP,
    GEOP,
    GTOP
} RELOP_TYPE;

typedef enum {
    MATPROD_DEFAULT = 1,
    MATPROD_INTERNAL,
    MATPROD_BLAS,
    MATPROD_DEFAULT_SIMD
} MATPROD_TYPE;

/*
#define HYANG_EOF	65535
*/
#define HYANG_EOF	-1

#ifndef __hyang_Names__
extern
#endif
FUNTAB	hyang_FunTab[];

#include <hyangexts/hyangextlib.h>

#ifdef __MAIN__
# define INI_as(v) = v
#define extern0 attribute_hidden
#else
# define INI_as(v)
#define extern0 extern
#endif

LibExtern SEXP  hyang_SrcfileSym;    /* "srcfile" */
LibExtern SEXP  hyang_SrcRefSym;     /* "srcref" */


LibExtern hyangboolean hyang_interrupts_suspended INI_as(FALSE);
LibExtern int hyang_interrupts_pending INI_as(0);

LibExtern char *hyang_Home;

extern0 hyang_size_t hyang_NSize  INI_as(HYANG_NSIZE);/* Size of cons cell heap */
extern0 hyang_size_t hyang_VSize  INI_as(HYANG_VSIZE);/* Size of the vector heap */
extern0 int	hyang_PBCEnabled INI_as(1);
extern0 int	hyang_in_pbc INI_as(0);
extern0 int	Abah_IntActive INI_as(0); /* bcEval called more recently than
                                            eval */
extern0 void*	Abah_pc INI_as(NULL);/* current byte code instruction */
extern0 SEXP	Abah_body INI_as(NULL); /* current byte code object */
extern0 SEXP	hyang_NHeap;	    /* Start of the cons cell heap */
extern0 SEXP	hyang_FreeSEXP;	    /* Cons cell free list */
extern0 hyang_size_t hyang_Collected;	    /* Number of free cons cells (after gc) */
extern0 int	hyang_Is_Running;	    /* for Windows memory manager */

LibExtern int	hyang_PPStackSize	INI_as(HYANG_PPSSIZE); /* The stack size (elements) */
LibExtern int	hyang_PPStackTop;	    /* The top of the stack */
LibExtern SEXP*	hyang_PPStack;	    /* The pointer protection stack */

extern0 SEXP	hyang_CurrentExpr;	    /* Currently evaluating expression */
extern0 SEXP	hyang_ReturnedValue;    /* Slot for return-ing values */
extern0 SEXP*	hyang_SymTable;	    /* The symbol table */
#ifdef HYANG_USE_SIGNALS
extern0 HYANGCTX hyang_TopLevel;	      /* Storage for the toplevel context */
extern0 HYANGCTX* hyang_TopLevelCtx;  /* The toplevel context */
LibExtern HYANGCTX* hyang_GlobalCtx;    /* The global context */
extern0 HYANGCTX* hyang_SessionCtx;   /* The session toplevel context */
extern0 HYANGCTX* hyang_ExitCtx;      /* The active context for on.exit processing */
#endif
extern hyangboolean hyang_Visible;	    /* Value visibility flag */
extern0 int	hyang_EvalDepth	INI_as(0);	/* Evaluation recursion depth */
extern0 int	hyang_BrowseLines	INI_as(0);	/* lines/per call in browser :
						 * options(deparse.max.lines) */
extern0 int	hyang_Exprs	INI_as(5000);	/* options(expressions) */
extern0 int	hyang_Exprs_keep INI_as(5000);/* options(expressions) */
extern0 hyangboolean hyang_KeepSrc	INI_as(FALSE);	/* options(keep.source) */
extern0 hyangboolean hyang_CBoundsCheck	INI_as(FALSE);	/* options(CBoundsCheck) */
extern0 MATPROD_TYPE hyang_Matprod	INI_as(MATPROD_DEFAULT);  /* options(matprod) */
extern0 int	hyang_WarnLength	INI_as(1000);	/* Error/warning max length */
extern0 int	hyang_nwarnings	INI_as(50);
extern uintptr_t hyang_CStackLim	INI_as((uintptr_t)-1);	/* C stack limit */
extern uintptr_t hyang_OldCStackLim INI_as((uintptr_t)0); /* Old value while
							   handling overflow */
extern uintptr_t hyang_CStackStart	INI_as((uintptr_t)-1);	/* Initial stack address */
extern int	hyang_CStackDir	INI_as(1);	/* C stack direction */

#ifdef HYANG_USE_SIGNALS
extern0 struct RPRSTACK *hyang_PendingProms INI_as(NULL); /* Pending promise stack */
#endif

LibExtern hyangboolean hyang_Interactive INI_as(TRUE);	/* TRUE during interactive use*/
extern0 hyangboolean hyang_Quiet	INI_as(FALSE);	/* Be as quiet as possible */
extern hyangboolean  hyang_Slave	INI_as(FALSE);	/* Run as a slave process */
extern0 hyangboolean hyang_Verbose	INI_as(FALSE);	/* Be verbose */
extern FILE*	hyang_Consolefile	INI_as(NULL);	/* Console output file */
extern FILE*	hyang_Outputfile	INI_as(NULL);	/* Output file */
extern0 int	hyang_ErrCon	INI_as(2);	/* Error connection */
LibExtern char *hyang_TempDir	INI_as(NULL);	/* Name of per-session dir */
extern0 char   *Sys_TempDir	INI_as(NULL);	/* Name of per-session dir
						   if set by Hyang itself */
extern0 char	hyang_StdinEnc[31]  INI_as("");	/* Encoding assumed for stdin */

LibExtern int	hyang_errParse	INI_as(0); /* Line where parse error occurred */
extern0 int	hyang_ParseErrCol;    /* Column of start of token where parse error occurred */
extern0 SEXP	hyang_ParseErrFile;   /* Source file where parse error was seen.  Either a
				       STRSXP or (when keeping srcrefs) a SrcFile ENVSXP */
#define PARSE_ERROR_SIZE 256	    /* Parse error messages saved here */
LibExtern char	hyang_ParseErrMsg[PARSE_ERROR_SIZE] INI_as("");
#define PARSE_CONTEXT_SIZE 256	    /* Recent parse context kept in a circular buffer */
LibExtern char	hyang_ParseContext[PARSE_CONTEXT_SIZE] INI_as("");
LibExtern int	hyang_ParseContextLast INI_as(0); /* last character in context buffer */
LibExtern int	hyang_ParseContextLine; /* Line in file of the above */

extern int	hyang_DirtyImage	INI_as(0);	/* Current image dirty */

LibExtern char *hyang_HistoryFile;	/* Name of the history file */
LibExtern int	hyang_HistorySize;	/* Size of the history file */
LibExtern int	hyang_RestoreHistory;	/* restore the history file? */
extern void 	hyang_setupHistory(void);

extern0 int	hyang_CollectWarnings INI_as(0);	/* the number of warnings */
extern0 SEXP	hyang_Warnings;	    /* the warnings and their calls */
extern0 int	hyang_ShowErrMsg INI_as(1);	/* show error messages? */
extern0 SEXP	hyang_HandlerStack;	/* Condition handler stack */
extern0 SEXP	hyang_RestartStack;	/* Stack of available restarts */
extern0 hyangboolean hyang_WarnPartialMatch_args   INI_as(FALSE);
extern0 hyangboolean hyang_WarnPartialMatch_dollar INI_as(FALSE);
extern0 hyangboolean hyang_WarnPartialMatch_attr INI_as(FALSE);
extern0 hyangboolean hyang_ShowWarnCalls INI_as(FALSE);
extern0 hyangboolean hyang_ShowErrCalls INI_as(FALSE);
extern0 int	hyang_NShowCalls INI_as(50);

LibExtern hyangboolean utf8locale  INI_as(FALSE);  /* is this a UTF-8 locale? */
LibExtern hyangboolean mbcslocale  INI_as(FALSE);  /* is this a MBCS locale? */
extern0   hyangboolean latin1locale INI_as(FALSE); /* is this a Latin-1 locale? */
#ifdef Win32
LibExtern unsigned int localeCP  INI_as(1252); /* the locale's codepage */
extern0   hyangboolean WinUTF8out  INI_as(FALSE);  /* Use UTF-8 for output */
extern0   void WinCheckUTF8(void);
#endif

extern char* OutDec	INI_as(".");  /* decimal point used for output */
extern0 hyangboolean hyang_DisableNLinBrowser	INI_as(FALSE);
extern0 char hyang_BrowserLastCmd	INI_as('n');

extern int hyangly_initEmbed(int argc, char **argv);

extern char	*hyang_GUIType	INI_as("unknown");
extern hyangboolean hyang_isForkedChild		INI_as(FALSE); /* was this forked? */

extern0 double cpuLimit			INI_as(-1.0);
extern0 double cpuLimit2	       	INI_as(-1.0);
extern0 double cpuLimitValue		INI_as(-1.0);
extern0 double elapsedLimit		INI_as(-1.0);
extern0 double elapsedLimit2		INI_as(-1.0);
extern0 double elapsedLimitValue       	INI_as(-1.0);

void resetTimeLimits(void);

#define ABAH_NODESTACKSIZE 200000
extern0 Abah_stack_t *Abah_NodeStackBase, *Abah_NodeStackTop, *Abah_NodeStackEnd;
#ifdef BC_INT_STACK
# define ABAH_INTSTACKSIZE 10000
extern0 IStackval *Abah_IntStackBase, *Abah_IntStackTop, *Abah_IntStackEnd;
#endif
extern0 int jit_enabled INI_as(0); /* has to be 0 during Hyang startup */
extern0 int hyang_compile_pkgs INI_as(0);
extern0 int hyang_check_constants INI_as(0);
extern0 int jit_disabled INI_as(0);
extern SEXP hyang_abahfun1(SEXP); /* unconditional fresh compilation */
extern void jit_enabled_init(void);
extern void hyang_initAssignSym(void);
#ifdef HYANG_USE_SIGNALS
extern SEXP Abah_InterpreterSrcRef(HYANGCTX*);
#endif
extern SEXP Abah_getCurrentSrcRef();
extern SEXP Abah_getInterpreterExpr();

LibExtern SEXP hyang_CachedScalarReal INI_as(NULL);
LibExtern SEXP hyang_CachedScalarInt INI_as(NULL);

LibExtern int hyang_NumOfMathThreads INI_as(1);
LibExtern int hyang_max_NumOfMathThreads INI_as(1);

typedef SEXP (*hyang_stdGen_ptr_t)(SEXP, SEXP, SEXP); /* typedef */
hyang_stdGen_ptr_t hyang_setStdGen_ptr(hyang_stdGen_ptr_t, SEXP); /* set method */
LibExtern SEXP hyang_MethodsNamespace;
SEXP hyang_deferred_default_method(void);
SEXP hyang_set_prim_method(SEXP fname, SEXP op, SEXP code_vec, SEXP fundef,
		       SEXP mlist);
SEXP do_set_prim_method(SEXP op, const char *code_string, SEXP fundef,
			SEXP mlist);
void hyang_set_quick_method_check(hyang_stdGen_ptr_t);
SEXP hyang_primitive_methods(SEXP op);
SEXP hyang_primitive_generic(SEXP op);

extern0 int hyang_decMinExp		INI_as(-308);

typedef struct {
    int ibeta, it, irnd, ngrd, machep, negep, iexp, minexp, maxexp;
    double eps, epsneg, xmin, xmax;
} AccuracyInfo;

LibExtern AccuracyInfo hyang_AccuracyInfo;

extern unsigned int max_contour_segments INI_as(25000);

extern hyangboolean known_to_be_latin1 INI_as(FALSE);
extern0 hyangboolean known_to_be_utf8 INI_as(FALSE);

LibExtern SEXP hyang_TrueValue INI_as(NULL);
LibExtern SEXP hyang_FalseValue INI_as(NULL);
LibExtern SEXP hyang_LogicalNAValue INI_as(NULL);

extern0 hyangboolean hyang_PCRE_use_JIT INI_as(TRUE);
extern0 int hyang_PCRE_study INI_as(10);
extern0 int hyang_PCRE_limit_recursion;


#ifdef __MAIN__
# undef extern
# undef extern0
# undef LibExtern
#endif
#undef INI_as

#define checkArity(a,b) hyangly_checkArityCall(a,b,call)

# define allocCharsxp		hyangly_allocCharsxp
# define asVecSize		hyangly_asVectSize
# define begincontext		hyangly_beginCtx
# define BindDomain		hyangly_BindDomain
# define check_stack_balance	hyangly_checkStackBalance
# define check1arg		hyangly_check1arg
# define CheckFormals		hyangly_CheckFormals
# define CleanEd		hyangly_CleanEd
# define CoercionWarning       	hyangly_CoercWarning
# define ComplexFromInteger	hyangly_CmplxFromInt
# define ComplexFromLogical	hyangly_CmplxFromLogical
# define ComplexFromReal	hyangly_CmplxFromReal
# define ComplexFromString	hyangly_CmplxFromStr
# define copyMostAttribNoTs	hyangly_copyMostAttribNoTs
# define createS3Vars		hyangly_createS3Vars
# define currentTime		hyangly_currentTime
# define CustomPrintValue	hyangly_CustomPrtValue
# define DataFrameClass		hyangly_DataFrameClass
# define ddfindVar		hyangly_ddfindVar
# define deparse1		hyangly_deparse1
# define deparse1m		hyangly_deparse1m
# define deparse1w		hyangly_deparse1w
# define deparse1line		hyangly_deparse1line
# define deparse1s		hyangly_deparse1s
# define DispatchGroup		hyangly_DispatchGroup
# define DispatchOrEval		hyangly_DispatchOrEval
# define DispatchAnyOrEval      hyangly_DispatchAnyOrEval
# define dynamicfindVar		hyangly_dynamicfindVar
# define EncodeChar             hyangly_EncodeChar
# define EncodeRaw              hyangly_EncodeRaw
# define EncodeReal2            hyangly_EncodeReal2
# define EncodeString           hyangly_EncodeStr
# define EnsureString 		hyangly_EnsureStr
# define endcontext		hyangly_endCtx
# define errorcall_cpy		hyangly_errCallCpy
# define ErrorMessage		hyangly_ErrMsg
# define evalList		hyangly_evalList
# define evalListKeepMissing	hyangly_evalListKeepMissing
# define factorsConform		hyangly_factorsConform
# define findcontext		hyangly_findCtx
# define findVar1		hyangly_findVar1
# define FrameClassFix		hyangly_FrameClassFix
# define framedepth		hyangly_framedepth
# define frameSubscript		hyangly_frameSubscript
# define get1index		hyangly_get1index
# define GetOptionCutoff       	hyangly_GetOptCutoff
# define getVar			hyangly_getVar
# define getVarInFrame		hyangly_getVarInFrame
# define InitArithmetic		hyangly_InitArith
# define InitConnections	hyangly_InitConn
# define InitEd			hyangly_InitEd
# define InitFunctionHashing	hyangly_InitFuncHashing
# define InitBaseEnv		hyangly_InitBaseEnv
# define InitGlobalEnv		hyangly_InitGlobalEnv
# define InitGraphics		hyangly_InitGraph
# define InitMemory		hyangly_InitMem
# define InitNames		hyangly_InitNames
# define InitOptions		hyangly_InitOpts
# define InitStringHash		hyangly_InitStrHash
# define InitS3DefaultTypes	hyangly_InitS3DefaultTypes
# define InitTempDir		hyangly_InitTempDir
# define InitTypeTables		hyangly_InitTypeTables
# define initStack		hyangly_initStack
# define IntegerFromComplex	hyangly_IntFromCmplx
# define IntegerFromLogical	hyangly_IntFromLogical
# define IntegerFromReal	hyangly_IntFromReal
# define IntegerFromString	hyangly_IntFromStr
# define internalTypeCheck	hyangly_intlTypeCheck
# define isValidName		hyangly_isValidName
# define installTrChar		hyangly_installTrChar
# define ItemName		hyangly_ItemName
# define jump_to_toplevel	hyangly_jump_to_toplevel
# define KillAllDevices		hyangly_StopAllDevcs
# define levelsgets		hyangly_levelsgets
# define LogicalFromComplex	hyangly_LogicalFromCmplx
# define LogicalFromInteger	hyangly_LogicalFromInt
# define LogicalFromReal	hyangly_LogicalFromReal
# define LogicalFromString	hyangly_LogicalFromStr
# define mainloop		hyangly_mainloop
# define makeSubscript		hyangly_makeSubscript
# define markKnown		hyangly_markKnown
# define mat2indsub		hyangly_mat2indsub
# define matchArg		hyangly_matchArg
# define matchArgExact		hyangly_matchArgExact
# define matchArgs		hyangly_matchArgs
# define matchArgs_RC		hyangly_matchArgs_RC
# define matchPar		hyangly_matchPar
# define Mbrtowc		hyangly_mbrtowc
# define mbtoucs		hyangly_mbtoucs
# define mbcsToUcs2		hyangly_mbcsToUcs2
# define memtrace_report	hyangly_memtrace_report
# define mkCLOSXP		hyangly_mkCLOSXP
# define mkFalse		hyangly_mkFalse
# define mkPROMISE		hyangly_mkPROM
# define mkQUOTE		hyangly_mkQUOTE
# define mkSYMSXP		hyangly_mkSYMSXP
# define mkTrue			hyangly_mkTrue
# define NewEnvironment		hyangly_NewEnv
# define OneIndex		hyangly_OneIndex
# define onintr			hyangly_onintr
# define onintrNoResume		hyangly_onintrNoResume
# define onsigusr1              hyangly_onsigusr1
# define onsigusr2              hyangly_onsigusr2
# define parse			hyangly_parse
# define patchArgsByActuals	hyangly_patchArgsByActuals
# define PrintDefaults		hyangly_PrtDefaults
# define PrintGreeting		hyangly_PrtGreeting
# define PrintValueEnv		hyangly_PrtValueEnv
# define PrintValueRec		hyangly_PrtValueRec
# define PrintVersion		hyangly_PrtVersion
# define PrintVersion_part_1	hyangly_PrtVersion_part_1
# define PrintVersionString    	hyangly_PrtVersionStr
# define PrintWarnings		hyangly_PrtWarnings
# define promiseArgs		hyangly_promArgs
# define RealFromComplex	hyangly_RealFromCmplx
# define RealFromInteger	hyangly_RealFromInt
# define RealFromLogical	hyangly_RealFromLogical
# define RealFromString		hyangly_RealFromStr
# define Seql			hyangly_Seql
# define sexptype2char		hyangly_sexptype2char
# define Scollate		hyangly_Scollate
# define sortVector		hyangly_sortVect
# define SrcrefPrompt		hyangly_SrcrefPrompt
# define ssort			hyangly_ssort
# define StringFromComplex	hyangly_StrFromCmplx
# define StringFromInteger	hyangly_StrFromInt
# define StringFromLogical	hyangly_StrFromLogical
# define StringFromReal		hyangly_StrFromReal
# define strIsASCII		hyangly_strIsASCII
# define StrToInternal		hyangly_StrToIntl
# define strmat2intmat		hyangly_strmat2intmat
# define substituteList		hyangly_substituteList
# define TimeToSeed		hyangly_TimeToSeed
# define tspgets		hyangly_tspGets
# define type2symbol		hyangly_type2symbol
# define unbindVar		hyangly_unbindVar
# define usemethod		hyangly_useMethod
# define ucstomb		hyangly_ucstomb
# define ucstoutf8		hyangly_ucstoutf8
#ifdef ADJUST_ENVIR_REFCNTS
# define unpromiseArgs		hyangly_unPromArgs
#endif
# define utf8toucs		hyangly_utf8toucs
# define utf8towcs		hyangly_utf8towcs
# define vectorIndex		hyangly_vectIndex
# define warningcall		hyangly_warningcall
# define WarningMessage		hyangly_warnMsg
# define wcstoutf8		hyangly_wcstoutf8
# define wtransChar		hyangly_wtransChar
# define yychar			hyangly_yychar
# define yylval			hyangly_yylval
# define yynerrs		hyangly_yynerrs
# define yyparse		hyangly_yyparse

#define	HYANG_CONSOLE	1
#define	HYANG_FILE	2
#define HYANG_TEXT	3

#define CONSOLE_BUFFER_SIZE 4096
int	hyang_ReadConsole(const char *, unsigned char *, int, int);
void	hyang_WriteConsole(const char *, int);
void	hyang_WriteConsoleEx(const char *, int, int);
void	hyang_ResetConsole(void);
void	hyang_FlushConsole(void);
void	hyang_ClearerrConsole(void);
void	hyang_Busy(int);
int	hyang_ShowFiles(int, const char **, const char **, const char *,
		    hyangboolean, const char *);
int     hyang_EditFiles(int, const char **, const char **, const char *);
int	hyang_ChooseFile(int, char *, int);
char	*hyang_HomeDir(void);
hyangboolean hyang_FileExists(const char *);
hyangboolean hyang_HiddenFile(const char *);
double	hyang_FileMtime(const char *);
int	hyang_GetFDLimit();
int	hyang_EnsureFDLimit(int);

typedef struct { SEXP cell; } hyang_varloc_t;
#define HYANG_VARLOC_IS_NULL(loc) ((loc).cell == NULL)
hyang_varloc_t hyang_findVarLocInFrame(SEXP, SEXP);
SEXP hyang_GetVarLocVal(hyang_varloc_t);
SEXP hyang_GetVarLocSym(hyang_varloc_t);
hyangboolean hyang_GetVarLocMISSING(hyang_varloc_t);
void hyang_SetVarLocVal(hyang_varloc_t, SEXP);

#define KEEPINTEGER 		1
#define QUOTEEXPRESSIONS 	2
#define SHOWATTRIBUTES 		4
#define USESOURCE 		8
#define WARNINCOMPLETE 		16
#define DELAYPROMISES 		32
#define KEEPNA			64
#define S_COMPAT       		128
#define HEXNUMERIC             	256
#define DIGITS16             	512
#define NICE_NAMES             	1024
#define SIMPLEDEPARSE		0
#define DEFAULTDEPARSE		1089
#define FORSOURCING		95

int hyangly_LogicalFromStr(SEXP, int*);
int hyangly_IntFromStr(SEXP, int*);
double hyangly_RealFromStr(SEXP, int*);
hyangcomplex hyangly_CmplxFromStr(SEXP, int*);
SEXP hyangly_StrFromLogical(int, int*);
SEXP hyangly_StrFromInt(int, int*);
SEXP hyangly_StrFromReal(double, int*);
SEXP hyangly_StrFromCmplx(hyangcomplex, int*);
SEXP hyangly_EnsureStr(SEXP);
SEXP hyangly_allocCharsxp(hyang_len_t);
SEXP hyangly_append(SEXP, SEXP);
hyang_xlen_t asVecSize(SEXP x);
void check1arg(SEXP, SEXP, const char *);
void hyangly_checkArityCall(SEXP, SEXP, SEXP);
void CheckFormals(SEXP);
void hyang_check_locale(void);
void check_stack_balance(SEXP op, int save);
void CleanEd(void);
void copyMostAttribNoTs(SEXP, SEXP);
SEXP createS3Vars(SEXP, SEXP, SEXP, SEXP, SEXP, SEXP);
void CustomPrintValue(SEXP, SEXP);
double currentTime(void);
void DataFrameClass(SEXP);
SEXP ddfindVar(SEXP, SEXP);
SEXP deparse1(SEXP,hyangboolean,int);
SEXP deparse1m(SEXP call, hyangboolean abbrev, int opts);
SEXP deparse1w(SEXP,hyangboolean,int);
SEXP deparse1line (SEXP, hyangboolean);
SEXP deparse1line_(SEXP, hyangboolean, int);
SEXP deparse1s(SEXP call);
int DispatchAnyOrEval(SEXP, SEXP, const char *, SEXP, SEXP, SEXP*, int, int);
int DispatchOrEval(SEXP, SEXP, const char *, SEXP, SEXP, SEXP*, int, int);
int DispatchGroup(const char *, SEXP,SEXP,SEXP,SEXP,SEXP*);
hyang_xlen_t dispatch_xlength(SEXP, SEXP, SEXP);
hyang_len_t dispatch_length(SEXP, SEXP, SEXP);
SEXP dispatch_subset2(SEXP, hyang_xlen_t, SEXP, SEXP);
SEXP duplicated(SEXP, hyangboolean);
hyang_xlen_t any_duplicated(SEXP, hyangboolean);
hyang_xlen_t any_duplicated3(SEXP, SEXP, hyangboolean);
SEXP evalList(SEXP, SEXP, SEXP, int);
SEXP evalListKeepMissing(SEXP, SEXP);
int factorsConform(SEXP, SEXP);
void NORET findcontext(int, SEXP, SEXP);
SEXP findVar1(SEXP, SEXP, SEXPTYPE, int);
void FrameClassFix(SEXP);
SEXP frameSubscript(int, SEXP, SEXP);
hyang_xlen_t get1index(SEXP, SEXP, hyang_xlen_t, int, int, SEXP);
int GetOptionCutoff(void);
SEXP getVar(SEXP, SEXP);
SEXP getVarInFrame(SEXP, SEXP);
void InitArithmetic(void);
void InitConnections(void);
void InitEd(void);
void InitFunctionHashing(void);
void InitBaseEnv(void);
void InitGlobalEnv(void);
hyangboolean hyang_current_trace_state(void);
hyangboolean hyang_current_debug_state(void);
hyangboolean hyang_has_methods(SEXP);
void hyang_InitialData(void);
SEXP hyang_possDispatch(SEXP, SEXP, SEXP, SEXP, hyangboolean);
hyangboolean inherits2(SEXP, const char *);
void InitGraphics(void);
void InitMemory(void);
void InitNames(void);
void InitOptions(void);
void InitStringHash(void);
void Init_hyang_Vars(SEXP);
void InitTempDir(void);
void hyang_reInitTempDir(int);
void InitTypeTables(void);
void initStack(void);
void InitS3DefaultTypes(void);
void internalTypeCheck(SEXP, SEXP, SEXPTYPE);
hyangboolean isMethodsDispatchOn(void);
int isValidName(const char *);
void NORET jump_to_toplevel(void);
void KillAllDevices(void);
SEXP levelsgets(SEXP, SEXP);
void mainloop(void);
SEXP makeSubscript(SEXP, SEXP, hyang_xlen_t *, SEXP);
SEXP markKnown(const char *, SEXP);
SEXP mat2indsub(SEXP, SEXP, SEXP);
SEXP matchArg(SEXP, SEXP*);
SEXP matchArgExact(SEXP, SEXP*);
SEXP matchArgs(SEXP, SEXP, SEXP);
SEXP matchArgs_RC(SEXP, SEXP, SEXP);
SEXP matchPar(const char *, SEXP*);
void memtrace_report(void *, void *);
SEXP mkCLOSXP(SEXP, SEXP, SEXP);
SEXP mkFalse(void);
SEXP mkPRIMSXP (int, int);
SEXP mkPROMISE(SEXP, SEXP);
SEXP hyang_mkEVPROM(SEXP, SEXP);
SEXP hyang_mkEVPROM_NR(SEXP, SEXP);
SEXP mkQUOTE(SEXP);
SEXP mkSYMSXP(SEXP, SEXP);
SEXP mkTrue(void);
const char *hyang_nativeEncoding(void);
SEXP NewEnvironment(SEXP, SEXP, SEXP);
void onintr(void);
void onintrNoResume(void);
RETSIGTYPE onsigusr1(int);
RETSIGTYPE onsigusr2(int);
hyang_xlen_t OneIndex(SEXP, SEXP, hyang_xlen_t, int, SEXP*, int, SEXP);
SEXP parse(FILE*, int);
SEXP patchArgsByActuals(SEXP, SEXP, SEXP);
void PrintDefaults(void);
void PrintGreeting(void);
void PrintValueEnv(SEXP, SEXP);
void PrintValueRec(SEXP, SEXP);
void PrintVersion(char *, size_t len);
void PrintVersion_part_1(char *, size_t len);
void PrintVersionString(char *, size_t len);
void PrintWarnings(void);
void process_site_hyangenviron(void);
void process_system_hyangenviron(void);
void process_user_hyangenviron(void);
SEXP promiseArgs(SEXP, SEXP);
void hyangcons_vprintf(const char *, va_list);
SEXP hyang_data_class(SEXP , hyangboolean);
SEXP hyang_data_class2(SEXP);
char *hyang_LibFileName(const char *, char *, size_t);
SEXP hyang_LoadFromFile(FILE*, int);
SEXP hyang_NewHashedEnv(SEXP, SEXP);
extern int hyang_Newhashpjw(const char *);
FILE* hyang_OpenLibFile(const char *);
SEXP hyang_Primitive(const char *);
void hyang_RestoreGlobalEnv(void);
void hyang_RestoreGlobalEnvFromFile(const char *, hyangboolean);
void hyang_SaveGlobalEnv(void);
void hyang_SaveGlobalEnvToFile(const char *);
void hyang_SaveToFile(SEXP, FILE*, int);
void hyang_SaveToFileV(SEXP, FILE*, int, int);
hyangboolean hyang_seemsOldStyleS4Object(SEXP object);
int hyang_SetOptWarn(int);
int hyang_SetOptWidth(int);
void hyang_Suicide(const char *);
void hyang_getProcTime(double *data);
int hyang_isMissing(SEXP symbol, SEXP rho);
const char *sexptype2char(SEXPTYPE type);
void sortVector(SEXP, hyangboolean);
void SrcrefPrompt(const char *, SEXP);
void ssort(SEXP*,int);
int StrToInternal(const char *);
SEXP strmat2intmat(SEXP, SEXP, SEXP);
SEXP substituteList(SEXP, SEXP);
unsigned int TimeToSeed(void);
SEXP tspgets(SEXP, SEXP);
SEXP type2symbol(SEXPTYPE);
void unbindVar(SEXP, SEXP);
#ifdef ALLOW_OLD_SAVE
void unmarkPhase(void);
#endif
#ifdef ADJUST_ENVIR_REFCNTS
void unpromiseArgs(SEXP);
#endif
SEXP hyang_LookupMethod(SEXP, SEXP, SEXP, SEXP);
int usemethod(const char *, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP, SEXP*);
SEXP vectorIndex(SEXP, SEXP, int, int, int, SEXP, hyangboolean);

#ifdef HYANG_USE_SIGNALS
void begincontext(HYANGCTX*, int, SEXP, SEXP, SEXP, SEXP, SEXP);
SEXP dynamicfindVar(SEXP, HYANGCTX*);
void endcontext(HYANGCTX*);
int framedepth(HYANGCTX*);
void hyang_InsertRestartHandlers(HYANGCTX *, const char *);
void NORET hyang_JumpToCtx(HYANGCTX *, int, SEXP);
SEXP hyang_syscall(int,HYANGCTX*);
int hyang_sysparent(int,HYANGCTX*);
SEXP hyang_sysframe(int,HYANGCTX*);
SEXP hyang_sysfunct(int,HYANGCTX*);

void hyang_run_onexits(HYANGCTX *);
void NORET hyang_jumpctxt(HYANGCTX *, int, SEXP);
#endif

SEXP ItemName(SEXP, hyang_xlen_t);

void NORET errorcall_cpy(SEXP, const char *, ...);
void NORET ErrorMessage(SEXP, int, ...);
void WarningMessage(SEXP, HYANG_WARNING, ...);
SEXP hyang_GetTraceback(int);

hyang_size_t hyang_GetMaxVSize(void);
void hyang_SetMaxVSize(hyang_size_t);
hyang_size_t hyang_GetMaxNSize(void);
void hyang_SetMaxNSize(hyang_size_t);
hyang_size_t hyang_Decode2Long(char *p, int *ierr);
void hyang_SetPPSize(hyang_size_t);

#define hyang_MaxDevices 64

typedef enum {
    hyangprt_adj_left = 0,
    hyangprt_adj_right = 1,
    hyangprt_adj_centre = 2,
    hyangprt_adj_none = 3
} hyangprt_adj;

int	hyangstrlen(SEXP, int);
const char *EncodeRaw(hyangbyte, const char *);
const char *EncodeString(SEXP, int, int, hyangprt_adj);
const char *EncodeReal2(double, int, int, int);
const char *EncodeChar(SEXP);

void orderVector1(int *indx, int n, SEXP key, hyangboolean nalast,
		  hyangboolean decreasing, SEXP rho);

SEXP hyang_subset3_dflt(SEXP, SEXP, SEXP);

SEXP hyang_subassign3_dflt(SEXP, SEXP, SEXP, SEXP);

#include <wchar.h>

void NORET UNIMPLEMENTED_TYPE(const char *s, SEXP x);
void NORET UNIMPLEMENTED_TYPEt(const char *s, SEXPTYPE t);
hyangboolean hyangly_strIsASCII(const char *str);
int utf8clen(char c);
int hyangly_AdobeSym2ucs2(int n);
double hyang_strtod5(const char *str, char **endptr, char dec,
		 hyangboolean NA, int exact);

typedef unsigned short ucs2_t;
size_t mbcsToUcs2(const char *in, ucs2_t *out, int nout, int enc);
/* size_t mbcsMblen(char *in);
size_t ucs2ToMbcs(ucs2_t *in, char *out);
size_t ucs2Mblen(ucs2_t *in); */
size_t utf8toucs(wchar_t *wc, const char *s);
size_t utf8towcs(wchar_t *wc, const char *s, size_t n);
size_t ucstomb(char *s, const unsigned int wc);
size_t ucstoutf8(char *s, const unsigned int wc);
size_t mbtoucs(unsigned int *wc, const char *s, size_t n);
size_t wcstoutf8(char *s, const wchar_t *wc, size_t n);

SEXP hyangly_installTrChar(SEXP);

const wchar_t *wtransChar(SEXP x); /* from sysutils.c */

#define mbs_init(x) memset(x, 0, sizeof(mbstate_t))
size_t Mbrtowc(wchar_t *wc, const char *s, size_t n, mbstate_t *ps);
hyangboolean mbcsValid(const char *str);
hyangboolean utf8Valid(const char *str);
char *hyangly_strchr(const char *s, int c);
char *hyangly_strrchr(const char *s, int c);

SEXP fixup_NaRm(SEXP args); /* summary.c */
void invalidate_cached_recodings(void);  /* from sysutils.c */
void resetICUcollator(void); /* from util.c */
void dt_invalidate_locale(); /* from hyangstrptime.h */
extern int hyang_OutputCon; /* from connections.c */
extern int hyang_InitReadItemDepth, hyang_ReadItemDepth; /* from serialize.c */
void get_current_mem(size_t *,size_t *,size_t *); /* from memory.c */
unsigned long get_duplicate_counter(void);  /* from duplicate.c */
void reset_duplicate_counter(void);  /* from duplicate.c */
void BindDomain(char *); /* from main.c */
extern hyangboolean LoadInitFile;  /* from startup.c */

double hyang_getClockIncrement(void);
void hyang_getProcTime(double *data);
void InitDyl(void);
void hyang_CleanTempDir(void);

#ifdef Win32
void hyang_fixSlash(char *s);
void hyang_fixbackslash(char *s);
wchar_t *filenameToWchar(const SEXP fn, const hyangboolean expand);

#if defined(SUPPORT_UTF8_WIN32)
#define mbrtowc(a,b,c,d) Rmbrtowc(a,b)
#define wcrtomb(a,b,c) Rwcrtomb(a,b)
#define mbstowcs(a,b,c) Rmbstowcs(a,b,c)
#define wcstombs(a,b,c) Rwcstombs(a,b,c)
size_t Rmbrtowc(wchar_t *wc, const char *s);
size_t Rwcrtomb(char *s, const wchar_t wc);
size_t Rmbstowcs(wchar_t *wc, const char *s, size_t n);
size_t Rwcstombs(char *s, const wchar_t *wc, size_t n);
#endif
#endif

FILE *RC_fopen(const SEXP fn, const char *mode, const hyangboolean expand);
int Seql(SEXP a, SEXP b);
int Scollate(SEXP a, SEXP b);

double hyang_strtod4(const char *str, char **endptr, char dec, hyangboolean NA);
double hyang_strtod(const char *str, char **endptr);
double hyang_atof(const char *str);

void set_rl_word_breaks(const char *str);

extern const char *locale2charset(const char *);

#ifndef NO_NLS
# ifdef ENABLE_NLS
#  include <libintl.h>
#  ifdef Win32
#   define _(String) libintl_gettext (String)
#   undef gettext
#  else
#   define _(String) gettext (String)
#  endif
#  define gettext_noop(String) String
#  define N_(String) gettext_noop (String)
#  else
#  define _(String) (String)
#  define N_(String) String
#  define ngettext(String, StringP, N) (N > 1 ? StringP: String)
# endif
#endif

#define BEGIN_SUSPEND_INTERRUPTS do { \
    hyangboolean __oldsusp__ = hyang_interrupts_suspended; \
    hyang_interrupts_suspended = TRUE;
#define END_SUSPEND_INTERRUPTS hyang_interrupts_suspended = __oldsusp__; \
    if (hyang_interrupts_pending && ! hyang_interrupts_suspended) \
        onintr(); \
} while(0)

#ifdef __GNUC__
# undef alloca
# define alloca(x) __builtin_alloca((x))
#else
# ifdef HAVE_ALLOCA_H
#  include <alloca.h>
# endif
# if !HAVE_DECL_ALLOCA
#  include <stddef.h>
extern void *alloca(size_t);
# endif
#endif

#ifdef HAVE_LONG_DOUBLE
# define LDOUBLE long double
#else
# define LDOUBLE double
#endif

#ifdef HAVE_INT64_T
# define LONG_INT int64_t
# define LONG_INT_MAX INT64_MAX
#elif defined(HAVE_INT_FAST64_T)
# define LONG_INT int_fast64_t
# define LONG_INT_MAX INT_FAST64_MAX
#endif

#define hyangExp10(x) pow(10.0, x)

#endif /* HYANGDEFN_H_ */
/*
 *- Local Variables:
 *- page-delimiter: "^/\\*---"
 *- End:
 */
