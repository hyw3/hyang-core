/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANGMBD_H_
#define HYANGMBD_H_

#include <hyangexts/hyangboolean.h>

#ifdef __cplusplus
extern "C" {
#endif

extern int hyangly_initEmbed(int argc, char *argv[]);
extern void hyangly_endEmbed(int fatal);

#ifndef LibExtern
# define LibExtern extern
#endif

int hyangly_init_hyang(int ac, char **av);
void hyang_MainloopSetup(void);
extern void hyang_ReplDLLinit(void);
extern int hyang_ReplDLLdo1(void);

void hyang_setStartTime(void);
extern void hyang_RunExitFinalizers(void);
extern void CleanEd(void);
extern void hyangly_StopAllDevcs(void);
LibExtern int hyang_DirtyImage;
extern void hyang_CleanTempDir(void);
LibExtern char *hyang_TempDir;
extern void hyang_SaveGlobalEnv(void);

#ifdef _WIN32
extern char *getDLLVersion(void), *getHyangUsr(void), *get_hyang_HOME(void);
extern void setup_term_ui(void);
LibExtern int UserBreak;
extern hyangboolean AllDevicesKilled;
extern void editorcleanall(void);
extern int GA_initapp(int, char **);
extern void GA_appcleanup(void);
extern void readconsolecfg(void);
#else
void fpu_setup(hyangboolean start);
#endif

#ifdef __cplusplus
}
#endif

#endif /* HYANGMBD_H_ */
