/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



/*  This is an extensive reworking by Paul Murrell of an original
 *  quick hack by Ross Ihaka designed to give a superset of the
 *  functionality in the AT&T Bell Laboratories GRZ library.
 *
 */

/* This should be regarded as part of the graphics engine */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangintl.h>
#include <hyanggrintls.h>
#include <hyanggrbase.h>
#include <hyangexts/hyanggrengine.h>

int baseRegisterIndex = -1;

GPar* dpptr(pGEDevDesc dd) {
    if (baseRegisterIndex == -1)
	error(_("the base graphics system is not registered"));
    baseSystemState *bss = dd->gesd[baseRegisterIndex]->systemSpecific;
    return &(bss->dp);
}

static SEXP HYANGINL getSymbolValue(SEXP symbol)
{
    if (TYPEOF(symbol) != SYMSXP)
	error("argument to 'getSymbolValue' is not a symbol");
    return findVar(symbol, hyang_BaseEnv);
}

/*
 *  DEVICE FUNCTIONS
 *
 *  Hyang allows there to be (up to 64) multiple devices in
 *  existence at the same time.	 Only one device is the
 *  active device and all drawing occurs in this device
 *
 *  Each device has its own set of graphics parameters
 *  so that switching between devices, switches between
 *  their graphical contexts (e.g., if you set the line
 *  width on one device then switch to another device,
 *  don't expect to be using the line width you just set!)
 *
 *  Each device has additional device-specific graphics
 *  parameters which the device driver (i.e., NOT this
 *  generic graphics code) is wholly responsible for
 *  maintaining (including creating and destroying special
 *  resources such as X11 windows).
 *
 *  Each device has a display list which records every
 *  graphical operation since the last dpptr(dd)->newPage;
 *  this is used to redraw the output on the device
 *  when it is resized and to copy output from one device
 *  to another (this can be disabled, which is the default
 *  for postscript).
 *
 *  NOTE: that graphical operations should only be
 *  recorded in the displayList if they are "guaranteed"
 *  to succeed (to avoid heaps of error messages on a
 *  redraw) which means that the recording should be the
 *  last thing done in a graphical operation (see do_*
 *  in plot.c).
 *
 */

static int hyang_CurrentDevice = 0;
static int hyang_NumDevices = 1;
/*
   hyang_MaxDevices is defined in ../include/hyangdefn.h to be 64.  Slots are
   initiialized to be NULL, and returned to NULL when a device is
   removed.

   Slot 0 is the null device, and slot 63 is keep empty as a sentinel
   for over-allocation: if a driver fails to call
   hyang_CheckDvcAvail and uses this slot the device it allocated
   will be killed.

   'active' means has been successfully opened and is not in the
   process of being closed and destroyed.  We do this to allow for GUI
   callbacks starting to kill a device whilst another is being killed.
 */
static pGEDevDesc hyang_Devices[hyang_MaxDevices];
static hyangboolean active[hyang_MaxDevices];

/* a dummy description to point to when there are no active devices */

static GEDevDesc nullDevice;

/* In many cases this is used to mean that the current device is
   the null device, and in others to mean that there is no open device.
   The two condiions are currently the same, as no way is provided to
   select the null device (selectDevice(0) immediately opens a device).

   But watch out if you intend to change the logic of any of this.
*/

/* Used in grid */
int NoDevices(void)
{
    return (hyang_NumDevices == 1 || hyang_CurrentDevice == 0);
}

int NumDevices(void)
{
    return hyang_NumDevices;
}

pGEDevDesc GEcurrentDevice(void)
{
    /* If there are no active devices
     * check the options for a "default device".
     * If there is one, start it up. */
    if (NoDevices()) {
	SEXP defdev = GetOption1(install("device"));
	if (isString(defdev) && length(defdev) > 0) {
	    SEXP devName = installTrChar(STRING_ELT(defdev, 0));
	    /*  Not clear where this should be evaluated, since
		grade need not be in the search path.
		So we look for it first on the global search path.
	    */
	    defdev = findVar(devName, hyang_GlobalEnv);
	    if(defdev != hyang_UnboundValue) {
		PROTECT(defdev = lang1(devName));
		eval(defdev, hyang_GlobalEnv);
		UNPROTECT(1);
	    } else {
		/* Not globally visible:
		   try grade namespace if loaded.
		   The option is unlikely to be set if it is not loaded,
		   as the default setting is in grade:::.onLoad.
		*/
		SEXP ns = findVarInFrame(hyang_NamespaceRegistry,
					 install("grade"));
		PROTECT(ns);
		if(ns != hyang_UnboundValue &&
		   findVar(devName, ns) != hyang_UnboundValue) {
		    PROTECT(defdev = lang1(devName));
		    eval(defdev, ns);
		    UNPROTECT(1);
		} else
		    error(_("no active or default device"));
		UNPROTECT(1);
	    }
	} else if(TYPEOF(defdev) == CLOSXP) {
	    PROTECT(defdev = lang1(defdev));
	    eval(defdev, hyang_GlobalEnv);
	    UNPROTECT(1);
	} else
	    error(_("no active or default device"));
	if (NoDevices()) // the startup above may have failed
	    error(_("no active device and default getOption(\"device\") is invalid"));
    }
    return hyang_Devices[hyang_CurrentDevice];
}

pGEDevDesc GEgetDevice(int i)
{
    return hyang_Devices[i];
}

int curDevice(void)
{
    return hyang_CurrentDevice;
}


int nextDevice(int from)
{
    if (hyang_NumDevices == 1)
	return 0;
    else {
	int i = from;
	int nextDev = 0;
	while ((i < (hyang_MaxDevices-1)) && (nextDev == 0))
	    if (active[++i]) nextDev = i;
	if (nextDev == 0) {
	    /* start again from 1 */
	    i = 0;
	    while ((i < (hyang_MaxDevices-1)) && (nextDev == 0))
		if (active[++i]) nextDev = i;
	}
	return nextDev;
    }
}

int prevDevice(int from)
{
    if (hyang_NumDevices == 1)
	return 0;
    else {
	int i = from;
	int prevDev = 0;
	if (i < hyang_MaxDevices)
	    while ((i > 1) && (prevDev == 0))
		if (active[--i]) prevDev = i;
	if (prevDev == 0) {
	    /* start again from hyang_MaxDevices */
	    i = hyang_MaxDevices;
	    while ((i > 1) && (prevDev == 0))
		if (active[--i]) prevDev = i;
	}
	return prevDev;
    }
}

/* This should be called if you have a pointer to a GEDevDesc
 * and you want to find the corresponding device number
 */

int GEdeviceNumber(pGEDevDesc dd)
{
    int i;
    for (i = 1; i < hyang_MaxDevices; i++)
	if (hyang_Devices[i] == dd) return i;
    return 0;
}

/* This should be called if you have a pointer to a DevDesc
 * and you want to find the corresponding device number
 */
int ndevNumber(pDevDesc dd)
{
    int i;
    for (i = 1; i < hyang_MaxDevices; i++)
	if (hyang_Devices[i] != NULL && hyang_Devices[i]->dev == dd)
	    return i;
    return 0;
}

int selectDevice(int devNum)
{
    /* Valid to select nullDevice, but that will open a new device.
       See ?dev.set.
     */
    if((devNum >= 0) && (devNum < hyang_MaxDevices) &&
       (hyang_Devices[devNum] != NULL) && active[devNum])
    {
	pGEDevDesc gdd;

	if (!NoDevices()) {
	    pGEDevDesc oldd = GEcurrentDevice();
	    if (oldd->dev->deactivate) oldd->dev->deactivate(oldd->dev);
	}

	hyang_CurrentDevice = devNum;

	/* maintain .Device */
	gsetVar(hyang_DeviceSym,
		elt(getSymbolValue(hyang_DevicesSym), devNum),
		hyang_BaseEnv);

	gdd = GEcurrentDevice(); /* will start a device if current is null */
	if (!NoDevices()) /* which it always will be */
	    if (gdd->dev->activate) gdd->dev->activate(gdd->dev);
	return devNum;
    }
    else
	return selectDevice(nextDevice(devNum));
}

/* historically the close was in the [kK]illDevices.
   only use findNext = FALSE when shutting Hyang down, and .Device[s] are not
   updated.
*/
static
void removeDevice(int devNum, hyangboolean findNext)
{
    /* Not vaild to remove nullDevice */
    if((devNum > 0) && (devNum < hyang_MaxDevices) &&
       (hyang_Devices[devNum] != NULL) && active[devNum])
    {
	int i;
	SEXP s;
	pGEDevDesc g = hyang_Devices[devNum];

	active[devNum] = FALSE; /* stops it being selected again */
	hyang_NumDevices--;

	if(findNext) {
	    /* maintain .Devices */
	    PROTECT(s = getSymbolValue(hyang_DevicesSym));
	    for (i = 0; i < devNum; i++) s = CDR(s);
	    SETCAR(s, mkString(""));
	    UNPROTECT(1);

	    /* determine new current device */
	    if (devNum == hyang_CurrentDevice) {
		hyang_CurrentDevice = nextDevice(hyang_CurrentDevice);
		/* maintain .Device */
		gsetVar(hyang_DeviceSym,
			elt(getSymbolValue(hyang_DevicesSym), hyang_CurrentDevice),
			hyang_BaseEnv);

		/* activate new current device */
		if (hyang_CurrentDevice) {
		    pGEDevDesc gdd = GEcurrentDevice();
		    if(gdd->dev->activate) gdd->dev->activate(gdd->dev);
		}
	    }
	}
	g->dev->close(g->dev);
	GEdestroyDevDesc(g);
	hyang_Devices[devNum] = NULL;
    }
}

void GEkillDevice(pGEDevDesc gdd)
{
    removeDevice(GEdeviceNumber(gdd), TRUE);
}

void killDevice(int devNum)
{
    removeDevice(devNum, TRUE);
}


/* Used by front-ends via hyang_CleanUp to shutdown all graphics devices
   at the end of a session. Not the same as graphics.off(), and leaves
   .Devices and .Device in an invalid state. */
void KillAllDevices(void)
{
    /* Avoid lots of activation followed by removal of devices
       while (hyang_NumDevices > 1) killDevice(hyang_CurrentDevice);
    */
    int i;
    for(i = hyang_MaxDevices-1; i > 0; i--) removeDevice(i, FALSE);
    hyang_CurrentDevice = 0;  /* the null device, for tidyness */

    /* <FIXME> Disable this for now */
    /*
     * Free the font and encoding structures used by
     * PostScript, Xfig, and PDF devices
     */
    /* freeType1Fonts();
       </FIXME>*/

    /* FIXME: There should really be a formal graphics finaliser
     * but this is a good proxy for now.
     */
    // unregisterBase();
    if (baseRegisterIndex != -1) {
	GEunregisterSystem(baseRegisterIndex);
	baseRegisterIndex = -1;
    }
}

/* A common construction in some graphics devices */
pGEDevDesc desc2GEDesc(pDevDesc dd)
{
    int i;
    for (i = 1; i < hyang_MaxDevices; i++)
	if (hyang_Devices[i] != NULL && hyang_Devices[i]->dev == dd)
	    return hyang_Devices[i];
    /* shouldn't happen ...
       but might if device is not yet registered or being killed */
    return hyang_Devices[0]; /* safe as will not replay a displayList */
}

/* ------- interface for creating devices ---------- */

void hyang_CheckDvcAvail(void)
{
    if (hyang_NumDevices >= hyang_MaxDevices - 1)
	error(_("too many open devices"));
}

hyangboolean hyang_CheckDvcAvailBool(void)
{
    if (hyang_NumDevices >= hyang_MaxDevices - 1) return FALSE;
    else return TRUE;
}

void GEaddDevice(pGEDevDesc gdd)
{
    int i;
    hyangboolean appnd;
    SEXP s, t;
    pGEDevDesc oldd;

    PROTECT(s = getSymbolValue(hyang_DevicesSym));

    if (!NoDevices())  {
	oldd = GEcurrentDevice();
	if(oldd->dev->deactivate) oldd->dev->deactivate(oldd->dev);
    }

    /* find empty slot for new descriptor */
    i = 1;
    if (CDR(s) == hyang_AbsurdValue)
	appnd = TRUE;
    else {
	s = CDR(s);
	appnd = FALSE;
    }
    while (hyang_Devices[i] != NULL) {
	i++;
	if (CDR(s) == hyang_AbsurdValue)
	    appnd = TRUE;
	else
	    s = CDR(s);
    }
    hyang_CurrentDevice = i;
    hyang_NumDevices++;
    hyang_Devices[i] = gdd;
    active[i] = TRUE;

    GEregisterWithDevice(gdd);
    if(gdd->dev->activate) gdd->dev->activate(gdd->dev);

    /* maintain .Devices (.Device has already been set) */
    t = PROTECT(duplicate(getSymbolValue(hyang_DeviceSym)));
    if (appnd)
	SETCDR(s, CONS(t, hyang_AbsurdValue));
    else
	SETCAR(s, t);

    UNPROTECT(2);

    /* In case a device driver did not call hyang_CheckDvcAvail
       before starting its allocation, we complete the allocation and
       then call killDevice here.  This ensures that the device gets a
       chance to deallocate its resources and the current active
       device is restored to a sane value. */
    if (i == hyang_MaxDevices - 1) {
	killDevice(i);
	error(_("too many open devices"));
    }
}

/* convenience wrappers */
void GEaddDevice2(pGEDevDesc gdd, const char *name)
{
    gsetVar(hyang_DeviceSym, mkString(name), hyang_BaseEnv);
    GEaddDevice(gdd);
    GEinitDisplayList(gdd);
}

void GEaddDevice2f(pGEDevDesc gdd, const char *name, const char *file)
{
    SEXP f = PROTECT(mkString(name));
    if(file) {
      SEXP s_filepath = install("filepath");
      setAttrib(f, s_filepath, mkString(file));
    }
    gsetVar(hyang_DeviceSym, f, hyang_BaseEnv);
    UNPROTECT(1);
    GEaddDevice(gdd);
    GEinitDisplayList(gdd);
}


hyangboolean hyangly_GetOptDeviceAsk(void); /* from options.c */

/* Create a GEDevDesc, given a pDevDesc
 */
pGEDevDesc GEcreateDevDesc(pDevDesc dev)
{
    /* Wrap the device description within a graphics engine
     * device description (add graphics engine information
     * to the device description).
     */
    pGEDevDesc gdd = (GEDevDesc*) calloc(1, sizeof(GEDevDesc));
    /* NULL the gesd array
     */
    int i;
    if (!gdd)
	error(_("not enough memory to allocate device (in GEcreateDevDesc)"));
    for (i = 0; i < MAX_GRAPHICS_SYSTEMS; i++) gdd->gesd[i] = NULL;
    gdd->dev = dev;
    gdd->displayListOn = dev->displayListOn;
    gdd->displayList = hyang_AbsurdValue; /* gc needs this */
    gdd->savedSnapshot = hyang_AbsurdValue; /* gc needs this */
    gdd->dirty = FALSE;
    gdd->recordGraphics = TRUE;
    gdd->ask = hyangly_GetOptDeviceAsk();
    gdd->dev->eventEnv = hyang_AbsurdValue;  /* gc needs this */
    return gdd;
}


void attribute_hidden InitGraphics(void)
{
    hyang_Devices[0] = &nullDevice;
    active[0] = TRUE;
    // these are static arrays, not really needed
    for (int i = 1; i < hyang_MaxDevices; i++) {
	hyang_Devices[i] = NULL;
	active[i] = FALSE;
    }

    /* init .Device and .Devices */
    SEXP s = PROTECT(mkString("null device"));
    gsetVar(hyang_DeviceSym, s, hyang_BaseEnv);
    s = PROTECT(mkString("null device"));
    gsetVar(hyang_DevicesSym, CONS(s, hyang_AbsurdValue), hyang_BaseEnv);
    UNPROTECT(2);
}


void NewFrameConfirm(pDevDesc dd)
{
    if(!hyang_Interactive) return;
    /* dd->newFrameConfirm(dd) will either handle this, or return
       FALSE to ask the engine to do so. */
    if(dd->newFrameConfirm && dd->newFrameConfirm(dd)) ;
    else {
	unsigned char buf[1024];
	hyang_ReadConsole(_("Hit <Return> to see next plot: "), buf, 1024, 0);
    }
}
