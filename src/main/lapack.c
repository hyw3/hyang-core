/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangdyp.h>
#include <hyangextslib/hyanglapack.h>

static hyang_LapackRoutines *ptr;


static int initialized = 0;

static void La_Init(void)
{
    int res = hyang_moduleCdyl("lapack", 1, 1);
    initialized = -1;
    if(!res) return;
    if(!ptr->do_lapack)
	error(_("LAPACK routines cannot be accessed in module"));
    initialized = 1;
    return;
}


SEXP attribute_hidden
do_lapack(SEXP call, SEXP op, SEXP args, SEXP env)
{
    checkArity(op, args);
    if(!initialized) La_Init();
    if(initialized > 0)
	return (*ptr->do_lapack)(call, op, args, env);
    else {
	error(_("LAPACK routines cannot be loaded"));
	return hyang_AbsurdValue;
    }
}


hyang_LapackRoutines *
hyang_setLapackRoutines(hyang_LapackRoutines *routines)
{
    hyang_LapackRoutines *tmp;
    tmp = ptr;
    ptr = routines;
    return(tmp);
}
