/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 *  This file replaces the previously used ROUTINES file and is used to
 *  explicitly register native routines that are located in the Hyang
 *  executable (e.g. hyang.bin, hyangGUI.exe) but which are intended to be
 *  accessible to S code via .C(), .Fortran(), .Hil(), .Nexus().
 *  The approach we use here is the regular registration mechanism that
 *  packages can use to explicitly list the symbols to be exported.
 *  For .C() and .Hil() routines, we give the number of arguments
 *  expected.
 *  For .C() routines, we also specify the types of the arguments.
 *  For .Fortran() and .Nexus() routines, we specify only the name
 *  and symbol.

 *  To add an entry, first determine by which interface the routine will
 *  be accessed:
 *   .C, .Hil, .Nexus or .Fortran
 *  Then add an entry to
 *    cMethods, callMethods, externalMethods, or fortranMethods
 *  respectively
 *
 *  DTL 14-Dec-2002
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangapproutine.h>
#include <hyangexts/hyanglpkg.h>


/*  These get the declarations of some routines referenced here but
    not explicitly declared.    This is necessary when we link with
    a C++ compiler because the linkage changes as the declarations
    are (currently) within extern "C" blocks.
*/
#include <hyangexts/hyangcallbacks.h>
#include <hyangdyp.h>

#include "basedecl.h"



#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}

static hyang_CallMethodDef callMethods [] = {
    /* Top-level task callbacks: .Hil as .Internal does not work */
    CALLDEF(hyang_addTaskCallback, 4),
    CALLDEF(hyang_getTaskCallbackNames, 0),
    CALLDEF(hyang_removeTaskCallback, 1),

    {NULL, NULL, 0}
};


#define FDEF(name, n)  {#name, (DL_FUNC) &F77_SYMBOL(name), n, NULL}
static hyang_FortranMethodDef fortranMethods[] = {
    /* LINPACK */
    FDEF(dqrcf, 8), // qr and auxiliaries
    FDEF(dqrdc2, 9),
    FDEF(dqrqty, 7),
    FDEF(dqrqy, 7),
    FDEF(dqrrsd, 7),
    FDEF(dqrxb, 7),
    FDEF(dtrco, 6), // .kappa_tri

    {NULL, NULL, 0}
};


void attribute_hidden
hyang_init_base(DllInfo *dll)
{
    hyang_regRoutines(dll, NULL, callMethods, fortranMethods, NULL);
    hyang_useDynSyms(dll, FALSE);
}
