/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/*
  See ../unix/system.txt for a description of some of these functions
*/

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "hyangdefn.h"
#include "hyangfio.h" /* for hyang_fopen */
#include "hyangstu.h"

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

/* These are used in ../hyangwin32/system.c, ../unix/sys-std.c */
SA_TYPE SaveAction = SA_SAVEASK;
SA_TYPE	RestoreAction = SA_RESTORE;
static hyangboolean LoadSiteFile = TRUE;
attribute_hidden hyangboolean LoadInitFile = TRUE;  /* Used in hyang_OpenInitFile */
static hyangboolean DebugInitFile = FALSE;

/*
 *  INITIALIZATION AND TERMINATION ACTIONS
 */

void attribute_hidden hyang_InitialData(void)
{
    hyang_RestoreGlobalEnv();
}


attribute_hidden
FILE *hyang_OpenLibFile(const char *file)
{
    char buf[PATH_MAX];
    FILE *fp;

    snprintf(buf, PATH_MAX, "%s/library/base/hyang/%s", hyang_Home, file);
    fp = hyang_fopen(buf, "r");
    return fp;
}

attribute_hidden
char *hyang_LibFileName(const char *file, char *buf, size_t bsize)
{
    if (snprintf(buf, bsize, "%s/library/base/hyang/%s", hyang_Home, file) < 0)
	error(_("hyang_LibFileName: buffer too small"));
    return buf;
}

attribute_hidden
FILE *hyang_OpenSysInitFile(void)
{
    char buf[PATH_MAX];
    FILE *fp;

    snprintf(buf, PATH_MAX, "%s/library/base/hyang/hyangprofile", hyang_Home);
    fp = hyang_fopen(buf, "r");
    return fp;
}

attribute_hidden
FILE *hyang_OpenSiteFile(void)
{
    char buf[PATH_MAX];
    FILE *fp;

    fp = NULL;
    if (LoadSiteFile) {
	char *p = getenv("HYANG_PROFILE");
	if (p) {
	    if (*p) return hyang_fopen(hyang_ExpandFileName(p), "r");
	    else return NULL;
	}
#ifdef HYANG_ARCH
	snprintf(buf, PATH_MAX, "%s/etc/%s/hyangprofile.site", hyang_Home, HYANG_ARCH);
	if ((fp = hyang_fopen(buf, "r"))) return fp;
#endif
	snprintf(buf, PATH_MAX, "%s/etc/hyangprofile.site", hyang_Home);
	if ((fp = hyang_fopen(buf, "r"))) return fp;
    }
    return fp;
}

	/* Saving and Restoring the Global Environment */

#ifndef Win32
static char workspace_name[1000] = ".hyData";

/*
  set_workspace_name is in src/hyangwin32/system.c and used to implement
  drag-and-drop on Windows.
 */
#else
static char workspace_name[PATH_MAX] = ".hyData";

attribute_hidden
void set_workspace_name(const char *fn)
{
    strncpy(workspace_name, fn, PATH_MAX);
    workspace_name[PATH_MAX - 1] = '\0';
}
#endif

attribute_hidden
const char* get_workspace_name()
{
    return workspace_name;
}

void hyang_RestoreGlobalEnv(void)
{
    if(RestoreAction == SA_RESTORE) {
	hyang_RestoreGlobalEnvFromFile(workspace_name, hyang_Quiet);
    }
}

void hyang_SaveGlobalEnv(void)
{
    hyang_SaveGlobalEnvToFile(".hyData");
}


/*
 *  INITIALIZATION HELPER CODE
 */

void hyang_DefParams(hyangstart Rp)
{
    Rp->hyang_Quiet = FALSE;
    Rp->hyang_Slave = FALSE;
    Rp->hyang_Interactive = TRUE;
    Rp->hyang_Verbose = FALSE;
    Rp->RestoreAction = SA_RESTORE;
    Rp->SaveAction = SA_SAVEASK;
    Rp->LoadSiteFile = TRUE;
    Rp->LoadInitFile = TRUE;
    Rp->DebugInitFile = FALSE;
    Rp->vsize = HYANG_VSIZE;
    Rp->nsize = HYANG_NSIZE;
    Rp->max_vsize = HYANG_SIZE_T_MAX;
    Rp->max_nsize = HYANG_SIZE_T_MAX;
    Rp->ppsize = HYANG_PPSSIZE;
    Rp->Nohyangenviron = FALSE;
    hyang_SizeFromEnv(Rp);
}

#define Max_Nsize 50000000	/* about 1.4Gb 32-bit, 2.8Gb 64-bit */
#define Max_Vsize HYANG_SIZE_T_MAX	/* unlimited */

// small values ok for HYANG_DEFAULT_PKGS=NULL (= 'base' only)
#define Min_Nsize 50000
#define Min_Vsize 262144 // = (Mega/4)

void hyang_SizeFromEnv(hyangstart Rp)
{
    int ierr;
    hyang_size_t value;
    char *p, msg[256];

    if ((p = getenv("HYANG_MAX_VSIZE"))) {
	value = hyang_Decode2Long(p, &ierr);
	if(ierr != 0 || value > Max_Vsize)
	    hyang_ShowMessage("WARNING: invalid HYANG_MAX_VSIZE ignored\n");
	else if(value < Min_Vsize) {
	    snprintf(msg, 256,
		     "WARNING: HYANG_MAX_VSIZE smaller than Min_Vsize = %lu is ignored\n",
		     (unsigned long) Min_Vsize);
	    hyang_ShowMessage(msg);
	}
	else
	    Rp->max_vsize = value;
    }
#if defined(__APPLE__) && defined(_SC_PHYS_PAGES) && defined(_SC_PAGE_SIZE)
    /* For now only on macOS place a default limit on the vector heap
       size to avoid having Hyang killed due to memory overcommit.
       Setting the limit at the maximum of 16Gb and available physical
       memory seems reasonable, but there may be better options. LT */
    else {
	hyang_size_t pages = sysconf(_SC_PHYS_PAGES);
	hyang_size_t page_size = sysconf(_SC_PAGE_SIZE);
	hyang_size_t sysmem = pages * page_size;
	hyang_size_t MinMaxVSize = 17179869184; /* 16 Gb */
	Rp->max_vsize = sysmem > MinMaxVSize ? sysmem : MinMaxVSize;
    }
#endif
    if((p = getenv("HYANG_VSIZE"))) {
	value = hyang_Decode2Long(p, &ierr);
	if(ierr != 0 || value > Max_Vsize)
	    hyang_ShowMessage("WARNING: invalid HYANG_VSIZE ignored\n");
	else if(value < Min_Vsize) {
	    snprintf(msg, 256,
		     "WARNING: HYANG_VSIZE smaller than Min_Vsize = %lu is ignored\n",
		     (unsigned long) Min_Vsize);
	    hyang_ShowMessage(msg);
	}
	else
	    Rp->vsize = value;
    }
    if((p = getenv("HYANG_NSIZE"))) {
	value = hyang_Decode2Long(p, &ierr);
	if(ierr != 0 || value > Max_Nsize)
	    hyang_ShowMessage("WARNING: invalid HYANG_NSIZE ignored\n");
	else if(value < Min_Nsize) {
	    snprintf(msg, 256,
		     "WARNING: HYANG_NSIZE smaller than Min_Nsize = %lu is ignored\n",
		     (unsigned long) Min_Nsize);
	    hyang_ShowMessage(msg);
	}
	else
	    Rp->nsize = value;
    }
}

static void SetSize(hyang_size_t vsize, hyang_size_t nsize)
{
    char msg[1024];
    hyangboolean sml;
    /* vsize > 0 to catch long->int overflow */
    if (vsize < 1000 && vsize > 0) {
	hyang_ShowMessage("WARNING: vsize ridiculously low, Megabytes assumed\n");
	vsize *= (hyang_size_t) Mega;
    }
    if((sml = vsize < Min_Vsize) || vsize > Max_Vsize) {
	snprintf(msg, 1024,
		 "WARNING: %s v(ector heap)size '%lu' ignored,"
		 " using default = %gM\n",
		 sml ? "too small" : "too large",
		 (unsigned long) vsize, HYANG_VSIZE / Mega);
	hyang_ShowMessage(msg);
	hyang_VSize = HYANG_VSIZE;
    } else
	hyang_VSize = vsize;
    if((sml = nsize < Min_Nsize) || nsize > Max_Nsize) {
	snprintf(msg, 1024,
		 "WARNING: %s language heap (n)size '%lu' ignored,"
		 " using default = %ld\n",
		 sml ? "too small" : "too large", (unsigned long) nsize, HYANG_NSIZE);
	hyang_ShowMessage(msg);
	hyang_NSize = HYANG_NSIZE;
    } else
	hyang_NSize = nsize;
}


void hyang_SetParams(hyangstart Rp)
{
    hyang_Quiet = Rp->hyang_Quiet;
    hyang_Slave = Rp->hyang_Slave;
    hyang_Interactive = Rp->hyang_Interactive;
    hyang_Verbose = Rp->hyang_Verbose;
    RestoreAction = Rp->RestoreAction;
    SaveAction = Rp->SaveAction;
    LoadSiteFile = Rp->LoadSiteFile;
    LoadInitFile = Rp->LoadInitFile;
    DebugInitFile = Rp->DebugInitFile;
    SetSize(Rp->vsize, Rp->nsize);
    hyang_SetMaxNSize(Rp->max_nsize);
    hyang_SetMaxVSize(Rp->max_vsize);
    hyang_SetPPSize(Rp->ppsize);
#ifdef Win32
    hyang_SetWin32(Rp);
#endif
}
