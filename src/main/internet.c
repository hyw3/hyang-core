/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangintl.h>

#include <hyangconn.h>
#include <hyangdyp.h>
#include <hyangexts/hyang-ftp-http.h>
#include <hyangextslib/hyanginet.h>

static hyang_inetRoutines routines, *ptr = &routines;


/*
SEXP hyangdownload(SEXP args);
hyangconn hyang_newurl(char *description, char *mode);
hyangconn hyang_newsock(char *host, int port, int server, char *mode, int timeout);


Next 6 are for use by libxml, only

void *hyang_HTTPOpen(const char *url);
int   hyang_HTTPRead(void *ctx, char *dest, int len);
void  hyang_HTTPClose(void *ctx);

void *hyang_FTPOpen(const char *url);
int   hyang_FTPRead(void *ctx, char *dest, int len);
void  hyang_FTPClose(void *ctx);

int hyangsockselect(int nsock, int *insockfd, int *ready, int *write,
		double timeout)

int hyang_WWHYCreate(const char *ip, int port);
void hyang_WWHYStop(void);
 */

static int initialized = 0;

hyang_inetRoutines *
hyang_setInetRoutines(hyang_inetRoutines *routines)
{
    hyang_inetRoutines *tmp;
    tmp = ptr;
    ptr = routines;
    return(tmp);
}

static void internet_Init(void)
{
    int res;
    res = hyang_moduleCdyl("server", 1, 1);
    initialized = -1;
    if(!res) return;
    if(!ptr->download)
	error(_("internet routines cannot be accessed in internet_Init()"));
    initialized = 1;
    return;
}

SEXP hyangdownload(SEXP args)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->download)(args);
    else {
	error(_("internet routines cannot be loaded for hyangdownload"));
	return hyang_AbsurdValue;
    }
}

hyangconn attribute_hidden
hyang_newurl(const char *description, const char * const mode, int type)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->newurl)(description, mode, type);
    else {
	error(_("internet routines cannot be loaded for hyang_newurl"));
	return (hyangconn)0;
    }
}

hyangconn attribute_hidden
hyang_newsock(const char *host, int port, int server, const char * const mode,
	  int timeout)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->newsock)(host, port, server, mode, timeout);
    else {
	error(_("internet routines cannot be loaded for hyang_newsock"));
	return (hyangconn)0;
    }
}

void *hyang_HTTPOpen(const char *url)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->HTTPOpen)(url, NULL, 0);
    else {
	error(_("internet routines cannot be loaded for hyang_HTTPOPen"));
	return NULL;
    }
}

int   hyang_HTTPRead(void *ctx, char *dest, int len)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->HTTPRead)(ctx, dest, len);
    else {
	error(_("internet routines cannot be loaded for hyang_HTTPRead"));
	return 0;
    }
}

void  hyang_HTTPClose(void *ctx)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->HTTPClose)(ctx);
    else
	error(_("internet routines cannot be loaded for hyang_HTTPRead"));
}

void *hyang_FTPOpen(const char *url)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->FTPOpen)(url);
    else {
	error(_("internet routines cannot be loaded for hyang_FTPOpen"));
	return NULL;
    }
}

int   hyang_FTPRead(void *ctx, char *dest, int len)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->FTPRead)(ctx, dest, len);
    else {
	error(_("internet routines cannot be loaded for hyang_FTPRead"));
	return 0;
    }
}

void  hyang_FTPClose(void *ctx)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->FTPClose)(ctx);
    else
	error(_("internet routines cannot be loaded for hyang_FTPClose"));
}

int hyang_ext_WWHYCreate(const char *ip, int port)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->WWHYCreate)(ip, port);
    else
	error(_("internet routines cannot be loaded for hyang_ext_WWHYCreate"));
    return -1;
}

void hyang_ext_WWHYStop(void)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->WWHYStop)();
    else
	error(_("internet routines cannot be loaded for hyang_ext_WWHYStop"));
}


SEXP hyangSockConn(SEXP sport, SEXP shost)
{
    if (length(sport) != 1) error("invalid 'socket' argument");
    int port = asInteger(sport);
    char *host[1];
    host[0] = (char *) translateChar(STRING_ELT(shost, 0));
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->sockconnect)(&port, host);
    else
	error(_("socket routines cannot be loaded"));
    return ScalarInteger(port); // The socket number
}

SEXP hyangSockRead(SEXP ssock, SEXP smaxlen)
{
    if (length(ssock) != 1) error("invalid 'socket' argument");
    int sock = asInteger(ssock), maxlen = asInteger(smaxlen);
    char buf[maxlen+1], *abuf[1];
    abuf[0] = buf;
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->sockread)(&sock, abuf, &maxlen);
    else
	error(_("socket routines cannot be loaded"));
    if (maxlen < 0) // presumably -1, error from recv
	error("Error reading data in hyangSockRead");
    SEXP ans = PROTECT(allocVector(STRSXP, 1));
    SET_STRING_ELT(ans, 0, mkCharLen(buf, maxlen));
    UNPROTECT(1);
    return ans;
}

SEXP hyangSockClose(SEXP ssock)
{
    if (length(ssock) != 1) error("invalid 'socket' argument");
    int sock = asInteger(ssock);
    if (sock <= 0) error(_("attempt to close invalid socket"));
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->sockclose)(&sock);
    else
	error(_("socket routines cannot be loaded"));
    return ScalarLogical(sock);
}

SEXP hyangSockOpen(SEXP sport)
{
    if (length(sport) != 1) error("invalid 'port' argument");
    int port = asInteger(sport);
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->sockopen)(&port);
    else
	error(_("socket routines cannot be loaded"));
    return ScalarInteger(port); // The socket number
}

SEXP hyangSockListen(SEXP ssock)
{
    if (length(ssock) != 1) error("invalid 'socket' argument");
    int sock = asInteger(ssock), len = 256;
    char buf[257], *abuf[1];
    abuf[0] = buf;
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->socklisten)(&sock, abuf, &len);
    else
	error(_("socket routines cannot be loaded"));
    SEXP ans = PROTECT(ScalarInteger(sock)); // The socket being listened on
    SEXP host = PROTECT(allocVector(STRSXP, 1));
    SET_STRING_ELT(host, 0, mkChar(buf));
    setAttrib(ans, install("host"), host);
    UNPROTECT(2);
    return ans;
}

SEXP hyangSockWrite(SEXP ssock, SEXP sstring)
{
    if (length(ssock) != 1) error("invalid 'socket' argument");
    int sock = asInteger(ssock), start = 0, end, len;
    char *buf = (char *) translateChar(STRING_ELT(sstring, 0)), *abuf[1];
    end = len = (int) strlen(buf);
    abuf[0] = buf;
    if(!initialized) internet_Init();
    if(initialized > 0)
	(*ptr->sockwrite)(&sock, abuf, &start, &end, &len);
    else
	error(_("socket routines cannot be loaded"));
    return ScalarInteger(len);
}


attribute_hidden
int hyangsockselect(int nsock, int *insockfd, int *ready, int *write,
		double timeout)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->sockselect)(nsock, insockfd, ready, write, timeout);
    else {
	error(_("socket routines cannot be loaded"));
	return 0;
    }
}

SEXP attribute_hidden do_curlVersion(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    checkArity(op, args);
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->curlVersion)(call, op, args, rho);
    else {
	error(_("internet routines cannot be loaded for checkArity in curlVersion"));
	return hyang_AbsurdValue;
    }
}

SEXP attribute_hidden do_curlGetHeaders(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    checkArity(op, args);
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->curlGetHeaders)(call, op, args, rho);
    else {
	error(_("internet routines cannot be loaded for checkArity in curlGetHeaders"));
	return hyang_AbsurdValue;
    }
}

SEXP attribute_hidden do_curlDownload(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    checkArity(op, args);
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->curlDownload)(call, op, args, rho);
    else {
	error(_("internet routines cannot be loaded for checkArity in curlDownload"));
	return hyang_AbsurdValue;
    }
}

hyangconn attribute_hidden
hyang_newCurlUrl(const char *description, const char * const mode, int type)
{
    if(!initialized) internet_Init();
    if(initialized > 0)
	return (*ptr->newcurlurl)(description, mode, type);
    else {
	error(_("internet routines cannot be loaded for hyang_newCurlUrl"));
	return (hyangconn)0;
    }
    return (hyangconn)0;
}

