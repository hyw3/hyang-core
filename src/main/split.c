/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "hyangdefn.h"
#include <hyangintl.h>
#include <hyangexts/hyangmacro.h>

SEXP attribute_hidden do_split(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP x, f, counts, vec, nm, nmj;
    hyangboolean have_names;

    checkArity(op, args);

    x = CAR(args);
    f = CADR(args);
    if (!isVector(x))
	error(_("first argument must be a vector"));
    if (!isFactor(f))
	error(_("second argument must be a factor"));
    int nlevs = nlevels(f);
    hyang_xlen_t nfac = XLENGTH(CADR(args));
    hyang_xlen_t nobs = XLENGTH(CAR(args));
    if (nfac <= 0 && nobs > 0)
	error(_("group length is 0 but data length > 0"));
    if (nfac > 0 && (nobs % nfac) != 0)
	warning(_("data length is not a multiple of split variable"));
    nm = getAttrib(x, hyang_NamesSym);
    have_names = nm != hyang_AbsurdValue;

#ifdef LONG_VECTOR_SUPPORT
    if (IS_LONG_VEC(x))
# define _L_INTSXP_ REALSXP
# define _L_INTEG_  REAL
# define _L_int_    hyang_xlen_t
# include "split-incl.c"

# undef _L_INTSXP_
# undef _L_INTEG_
# undef _L_int_
    else
#endif

# define _L_INTSXP_ INTSXP
# define _L_INTEG_  INTEGER
# define _L_int_    int
# include "split-incl.c"

# undef _L_INTSXP_
# undef _L_INTEG_
# undef _L_int_

    setAttrib(vec, hyang_NamesSym, getAttrib(f, hyang_LevelsSym));
    UNPROTECT(2);
    return vec;
}
