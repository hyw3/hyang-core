/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


# include <stdint.h>
// C99/C11 require this to be implemented.
typedef int_fast64_t hyang_time_t;

struct Rtm
{
  int tm_sec;
  int tm_min;
  int tm_hour;
  int tm_mday;
  int tm_mon;
  int tm_year;
  int tm_wday;
  int tm_yday;
  int tm_isdst;
  long tm_gmtoff;
  const char *tm_zone;
};

typedef struct Rtm stm;

#define time_t hyang_time_t
#define gmtime hyang_gmtime
#define gmtime_r hyang_gmtime_r
#define localtime hyang_localtime
#define localtime_r hyang_localtime_r
#define mktime hyang_mktime
#define tzset hyang_tzset
extern stm* Rgmtime (const hyang_time_t*);
extern stm* hyang_gmtime_r (const hyang_time_t*, stm*);
extern stm* hyang_localtime (const hyang_time_t*);
extern stm* hyang_localtime_r(const hyang_time_t*, stm*);
extern hyang_time_t hyang_mktime (stm*);
extern void hyang_tzset(void);
extern void hyang_tzsetwall(void);
extern char *hyang_tzname[2];
extern int_fast64_t hyang_timegm(stm*);

extern size_t
hyang_strftime(char * const s, const size_t maxsize, const char *const format,
	   const stm *const t);
