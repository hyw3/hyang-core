/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_BUFFER_UTILS
#define HYANG_BUFFER_UTILS

/* used in bind.c character.c deparse.c, printutils.c, saveload.c
   scan.c seq.c sprintf.c sysutils.c */

typedef struct {
 char *data;
 size_t bufsize;
 size_t defaultSize;
} hyang_StringBuffer;

/* code in ./memory.c : */
/* Note that hyang_StringBuffer *buf needs to be initialized before call */
void *hyang_AllocStringBuffer(size_t blen, hyang_StringBuffer *buf);
void hyang_FreeStrBuff(hyang_StringBuffer *buf);
void hyang_FreeStrBuffL(hyang_StringBuffer *buf);

#endif
