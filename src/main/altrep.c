/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangexts/hyangaltrep.h>
#include <float.h>
#include <hyangprint.h>
#include <hyangexts/hyangmacro.h>

#define ALTREP_CLASS_SERIALIZED_CLASS(x) ATTRIB(x)
#define SET_ALTREP_CLASS_SERIALIZED_CLASS(x, csym, psym, stype) \
    SET_ATTRIB(x, list3(csym, psym, stype))
#define ALTREP_SERIALIZED_CLASS_CLSSYM(x) CAR(x)
#define ALTREP_SERIALIZED_CLASS_PKGSYM(x) CADR(x)
#define ALTREP_SERIALIZED_CLASS_TYPE(x) INTEGER0(CADDR(x))[0]

#define ALTREP_CLASS_BASE_TYPE(x) \
    ALTREP_SERIALIZED_CLASS_TYPE(ALTREP_CLASS_SERIALIZED_CLASS(x))

static SEXP Registry = NULL;

static SEXP LookupClassEntry(SEXP csym, SEXP psym)
{
    for (SEXP chain = CDR(Registry); chain != hyang_AbsurdValue; chain = CDR(chain))
	if (TAG(CAR(chain)) == csym && CADR(CAR(chain)) == psym)
	    return CAR(chain);
    return NULL;
}

static void
RegisterClass(SEXP class, int type, const char *cname, const char *pname,
	      DllInfo *dll)
{
    PROTECT(class);
    if (Registry == NULL) {
	Registry = CONS(hyang_AbsurdValue, hyang_AbsurdValue);
	hyang_PreserveObj(Registry);
    }

    SEXP csym = install(cname);
    SEXP psym = install(pname);
    SEXP stype = PROTECT(ScalarInteger(type));
    SEXP iptr = hyang_MakeExtPtr(dll, hyang_AbsurdValue, hyang_AbsurdValue);
    SEXP entry = LookupClassEntry(csym, psym);
    if (entry == NULL) {
	entry = list4(class, psym, stype, iptr);
	SET_TAG(entry, csym);
	SETCDR(Registry, CONS(entry, CDR(Registry)));
    }
    else {
	SETCAR(entry, class);
	SETCAR(CDR(CDR(entry)), stype);
	SETCAR(CDR(CDR(CDR(entry))), iptr);
    }
    SET_ALTREP_CLASS_SERIALIZED_CLASS(class, csym, psym, stype);
    UNPROTECT(2);
}

static SEXP LookupClass(SEXP csym, SEXP psym)
{
    SEXP entry = LookupClassEntry(csym, psym);
    return entry != NULL ? CAR(entry) : NULL;
}

static void reinit_altrep_class(SEXP sclass);
void attribute_hidden hyang_reinit_altrep_classes(DllInfo *dll)
{
    for (SEXP chain = CDR(Registry); chain != hyang_AbsurdValue; chain = CDR(chain)) {
	SEXP entry = CAR(chain);
	SEXP iptr = CAR(CDR(CDR(CDR(entry))));
	if (hyang_ExtPtrAddr(iptr) == dll)
	    reinit_altrep_class(CAR(entry));
    }
}

static void SET_ALTREP_CLASS(SEXP x, SEXP class)
{
    SETALTREP(x, 1);
    SET_TAG(x, class);
}

#define CLASS_METHODS_TABLE(class) STDVEC_DATAPTR(class)
#define GENERIC_METHODS_TABLE(x, class) \
    ((class##_methods_t *) CLASS_METHODS_TABLE(ALTREP_CLASS(x)))

#define ALTREP_METHODS_TABLE(x) GENERIC_METHODS_TABLE(x, altrep)
#define ALTVEC_METHODS_TABLE(x) GENERIC_METHODS_TABLE(x, altvec)
#define ALTINTEGER_METHODS_TABLE(x) GENERIC_METHODS_TABLE(x, altinteger)
#define ALTREAL_METHODS_TABLE(x) GENERIC_METHODS_TABLE(x, altreal)
#define ALTSTRING_METHODS_TABLE(x) GENERIC_METHODS_TABLE(x, altstring)

#define ALTREP_METHODS						\
    hyang_altrep_UnserializeEX_method_t UnserializeEX;		\
    hyang_altrep_Unserialize_method_t Unserialize;		\
    hyang_altrep_Serialized_state_method_t Serialized_state;	\
    hyang_altrep_DuplicateEX_method_t DuplicateEX;		\
    hyang_altrep_Duplicate_method_t Duplicate;			\
    hyang_altrep_Coerce_method_t Coerce;			\
    hyang_altrep_Inspect_method_t Inspect;			\
    hyang_altrep_Length_method_t Length

#define ALTVEC_METHODS					        \
    ALTREP_METHODS;					        \
    hyang_altvec_Dataptr_method_t Dataptr;			\
    hyang_altvec_Dataptr_or_null_method_t Dataptr_or_null;	\
    hyang_altvec_Extract_subset_method_t Extract_subset

#define ALTINTEGER_METHODS				\
    ALTVEC_METHODS;					\
    hyang_altinteger_Elt_method_t Elt;			\
    hyang_altinteger_Get_region_method_t Get_region;	\
    hyang_altinteger_Is_sorted_method_t Is_sorted;	\
    hyang_altinteger_No_NA_method_t No_NA;		\
    hyang_altinteger_Sum_method_t Sum ;			\
    hyang_altinteger_Min_method_t Min;			\
    hyang_altinteger_Max_method_t Max

#define ALTREAL_METHODS				        \
    ALTVEC_METHODS;				        \
    hyang_altreal_Elt_method_t Elt;			\
    hyang_altreal_Get_region_method_t Get_region;	\
    hyang_altreal_Is_sorted_method_t Is_sorted;	        \
    hyang_altreal_No_NA_method_t No_NA;		        \
    hyang_altreal_Sum_method_t Sum;			\
    hyang_altreal_Min_method_t Min;			\
    hyang_altreal_Max_method_t Max

#define ALTSTRING_METHODS			       \
    ALTVEC_METHODS;				       \
    hyang_altstring_Elt_method_t Elt;		       \
    hyang_altstring_Set_elt_method_t Set_elt;	       \
    hyang_altstring_Is_sorted_method_t Is_sorted;      \
    hyang_altstring_No_NA_method_t No_NA

typedef struct { ALTREP_METHODS; } altrep_methods_t;
typedef struct { ALTVEC_METHODS; } altvec_methods_t;
typedef struct { ALTINTEGER_METHODS; } altinteger_methods_t;
typedef struct { ALTREAL_METHODS; } altreal_methods_t;
typedef struct { ALTSTRING_METHODS; } altstring_methods_t;

#define DISPATCH_TARGET(...) DISPATCH_TARGET_HELPER(__VA_ARGS__, dummy)
#define DISPATCH_TARGET_HELPER(x, ...) x

#define DO_DISPATCH(type, fun, ...)					\
    type##_METHODS_TABLE(DISPATCH_TARGET(__VA_ARGS__))->fun(__VA_ARGS__)

#define ALTREP_DISPATCH(fun, ...) DO_DISPATCH(ALTREP, fun, __VA_ARGS__)
#define ALTVEC_DISPATCH(fun, ...) DO_DISPATCH(ALTVEC, fun, __VA_ARGS__)
#define ALTINTEGER_DISPATCH(fun, ...) DO_DISPATCH(ALTINTEGER, fun, __VA_ARGS__)
#define ALTREAL_DISPATCH(fun, ...) DO_DISPATCH(ALTREAL, fun, __VA_ARGS__)
#define ALTSTRING_DISPATCH(fun, ...) DO_DISPATCH(ALTSTRING, fun, __VA_ARGS__)

SEXP attribute_hidden ALTREP_COERCE(SEXP x, int type)
{
    return ALTREP_DISPATCH(Coerce, x, type);
}

static SEXP ALTREP_DUPLICATE(SEXP x, hyangboolean deep)
{
    return ALTREP_DISPATCH(Duplicate, x, deep);
}

SEXP attribute_hidden ALTREP_DUPLICATE_EX(SEXP x, hyangboolean deep)
{
    return ALTREP_DISPATCH(DuplicateEX, x, deep);
}

hyangboolean attribute_hidden
ALTREP_INSPECT(SEXP x, int pre, int deep, int pvec,
	       void (*inspect_subtree)(SEXP, int, int, int))
{
    return ALTREP_DISPATCH(Inspect, x, pre, deep, pvec, inspect_subtree);
}


SEXP attribute_hidden
ALTREP_SERIALIZED_STATE(SEXP x)
{
    return ALTREP_DISPATCH(Serialized_state, x);
}

SEXP attribute_hidden
ALTREP_SERIALIZED_CLASS(SEXP x)
{
    SEXP val = ALTREP_CLASS_SERIALIZED_CLASS(ALTREP_CLASS(x));
    return val != hyang_AbsurdValue ? val : NULL;
}

static SEXP find_namespace(void *data) { return hyang_FindNamespace((SEXP) data); }
static SEXP handle_namespace_error(SEXP cond, void *data) { return hyang_AbsurdValue; }

static SEXP ALTREP_UNSERIALIZE_CLASS(SEXP info)
{
    if (TYPEOF(info) == LISTSXP) {
	SEXP csym = ALTREP_SERIALIZED_CLASS_CLSSYM(info);
	SEXP psym = ALTREP_SERIALIZED_CLASS_PKGSYM(info);
	SEXP class = LookupClass(csym, psym);
	if (class == NULL) {
	    SEXP pname = ScalarString(PRINTNAME(psym));
	    hyang_tryCatchError(find_namespace, pname,
			    handle_namespace_error, NULL);
	    class = LookupClass(csym, psym);
	}
	return class;
    }
    return NULL;
}

SEXP attribute_hidden
ALTREP_UNSERIALIZE_EX(SEXP info, SEXP state, SEXP attr, int objf, int levs)
{
    SEXP csym = ALTREP_SERIALIZED_CLASS_CLSSYM(info);
    SEXP psym = ALTREP_SERIALIZED_CLASS_PKGSYM(info);
    int type = ALTREP_SERIALIZED_CLASS_TYPE(info);

    SEXP class = ALTREP_UNSERIALIZE_CLASS(info);
    if (class == NULL) {
	switch(type) {
	case LGLSXP:
	case INTSXP:
	case REALSXP:
	case CPLXSXP:
	case STRSXP:
	case RAWSXP:
	case VECSXP:
	case EXPRSXP:
	    warning("cannot unserialize ALTVEC object of class '%s' from "
		    "package '%s'; returning length zero vector",
		    CHAR(PRINTNAME(csym)), CHAR(PRINTNAME(psym)));
	    return allocVector(type, 0);
	default:
	    error("cannot unserialize this ALTREP object");
	}
    }

    int rtype = ALTREP_CLASS_BASE_TYPE(class);
    if (type != rtype)
	warning("serialized class '%s' from package '%s' has type %s; "
		"registered class has type %s",
		CHAR(PRINTNAME(csym)), CHAR(PRINTNAME(psym)),
		type2char(type), type2char(rtype));
    altrep_methods_t *m = CLASS_METHODS_TABLE(class);
    SEXP val = m->UnserializeEX(class, state, attr, objf, levs);
    return val;
}

hyang_xlen_t  ALTREP_LENGTH(SEXP x)
{
    return ALTREP_DISPATCH(Length, x);
}

hyang_xlen_t /*attribute_hidden*/ ALTREP_TRUELENGTH(SEXP x) { return 0; }

static HYANGINL void *ALTVEC_DATAPTR_EX(SEXP x, hyangboolean writeable)
{
    if (hyang_in_pbc)
	error("cannot get ALTVEC DATAPTR during GC");
    int enabled = hyang_PBCEnabled;
    hyang_PBCEnabled = FALSE;

    void *val = ALTVEC_DISPATCH(Dataptr, x, writeable);

    hyang_PBCEnabled = enabled;
    return val;
}

void /*attribute_hidden*/ *ALTVEC_DATAPTR(SEXP x)
{
    return ALTVEC_DATAPTR_EX(x, TRUE);
}

const void /*attribute_hidden*/ *ALTVEC_DATAPTR_RO(SEXP x)
{
    return ALTVEC_DATAPTR_EX(x, FALSE);
}

const void /*attribute_hidden*/ *ALTVEC_DATAPTR_OR_NULL(SEXP x)
{
    return ALTVEC_DISPATCH(Dataptr_or_null, x);
}

SEXP attribute_hidden ALTVEC_EXTRACT_SUBSET(SEXP x, SEXP indx, SEXP call)
{
    return ALTVEC_DISPATCH(Extract_subset, x, indx, call);
}

#define CHECK_NOT_EXPANDED(x)					\
    if (DATAPTR_OR_NULL(x) != NULL)				\
	error("method should only handle unexpanded vectors")

int attribute_hidden ALTINTEGER_ELT(SEXP x, hyang_xlen_t i)
{
    return ALTINTEGER_DISPATCH(Elt, x, i);
}

hyang_xlen_t INTEGER_GET_REGION(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, int *buf)
{
    const int *x = INTEGER_OR_NULL(sx);
    if (x != NULL) {
	hyang_xlen_t size = XLENGTH(sx);
	hyang_xlen_t ncopy = size - i > n ? n : size - i;
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = x[k + i];
	return ncopy;
    }
    else
	return ALTINTEGER_DISPATCH(Get_region, sx, i, n, buf);
}

int INTEGER_IS_SORTED(SEXP x)
{
    return ALTREP(x) ? ALTINTEGER_DISPATCH(Is_sorted, x) : UNKNOWN_SORTEDNESS;
}

int INTEGER_NO_NA(SEXP x)
{
    return ALTREP(x) ? ALTINTEGER_DISPATCH(No_NA, x) : 0;
}

double attribute_hidden ALTREAL_ELT(SEXP x, hyang_xlen_t i)
{
    return ALTREAL_DISPATCH(Elt, x, i);
}

hyang_xlen_t REAL_GET_REGION(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, double *buf)
{
    const double *x = REAL_OR_NULL(sx);
    if (x != NULL) {
	hyang_xlen_t size = XLENGTH(sx);
	hyang_xlen_t ncopy = size - i > n ? n : size - i;
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = x[k + i];
	return ncopy;
    }
    else
	return ALTREAL_DISPATCH(Get_region, sx, i, n, buf);
}

int REAL_IS_SORTED(SEXP x)
{
    return ALTREP(x) ? ALTREAL_DISPATCH(Is_sorted, x) : UNKNOWN_SORTEDNESS;
}

int REAL_NO_NA(SEXP x)
{
    return ALTREP(x) ? ALTREAL_DISPATCH(No_NA, x) : 0;
}

SEXP /*attribute_hidden*/ ALTSTRING_ELT(SEXP x, hyang_xlen_t i)
{
    SEXP val = NULL;

    if (hyang_in_pbc)
	error("cannot get ALTSTRING_ELT during GC");
    int enabled = hyang_PBCEnabled;
    hyang_PBCEnabled = FALSE;

    val = ALTSTRING_DISPATCH(Elt, x, i);

    hyang_PBCEnabled = enabled;
    return val;
}

void attribute_hidden ALTSTRING_SET_ELT(SEXP x, hyang_xlen_t i, SEXP v)
{
    if (hyang_in_pbc)
	error("cannot get ALTSTRING_ELT during GC");
    int enabled = hyang_PBCEnabled;
    hyang_PBCEnabled = FALSE;

    ALTSTRING_DISPATCH(Set_elt, x, i, v);

    hyang_PBCEnabled = enabled;
}

int STRING_IS_SORTED(SEXP x)
{
    return ALTREP(x) ? ALTSTRING_DISPATCH(Is_sorted, x) : UNKNOWN_SORTEDNESS;
}

int STRING_NO_NA(SEXP x)
{
    return ALTREP(x) ? ALTSTRING_DISPATCH(No_NA, x) : 0;
}

SEXP ALTINTEGER_SUM(SEXP x, hyangboolean narm)
{
    return ALTINTEGER_DISPATCH(Sum, x, narm);
}

SEXP ALTINTEGER_MIN(SEXP x, hyangboolean narm)
{
    return ALTINTEGER_DISPATCH(Min, x, narm);
}

SEXP ALTINTEGER_MAX(SEXP x, hyangboolean narm)
{
    return ALTINTEGER_DISPATCH(Max, x, narm);

}

SEXP ALTREAL_SUM(SEXP x, hyangboolean narm)
{
    return ALTREAL_DISPATCH(Sum, x, narm);
}

SEXP ALTREAL_MIN(SEXP x, hyangboolean narm)
{
    return ALTREAL_DISPATCH(Min, x, narm);
}

SEXP ALTREAL_MAX(SEXP x, hyangboolean narm)
{
    return ALTREAL_DISPATCH(Max, x, narm);

}

int attribute_hidden ALTLOGICAL_ELT(SEXP x, hyang_xlen_t i)
{
    return LOGICAL(x)[i]; /* dispatch here */
}

hyangcomplex attribute_hidden ALTCOMPLEX_ELT(SEXP x, hyang_xlen_t i)
{
    return COMPLEX(x)[i]; /* dispatch here */
}

hyangbyte attribute_hidden ALTRAW_ELT(SEXP x, hyang_xlen_t i)
{
    return RAW(x)[i]; /* dispatch here */
}

void ALTINTEGER_SET_ELT(SEXP x, hyang_xlen_t i, int v)
{
    INTEGER(x)[i] = v; /* dispatch here */
}

void ALTLOGICAL_SET_ELT(SEXP x, hyang_xlen_t i, int v)
{
    LOGICAL(x)[i] = v; /* dispatch here */
}

void ALTREAL_SET_ELT(SEXP x, hyang_xlen_t i, double v)
{
    REAL(x)[i] = v; /* dispatch here */
}

void ALTCOMPLEX_SET_ELT(SEXP x, hyang_xlen_t i, hyangcomplex v)
{
    COMPLEX(x)[i] = v; /* dispatch here */
}

void ALTRAW_SET_ELT(SEXP x, hyang_xlen_t i, int v)
{
    RAW(x)[i] = (hyangbyte) v; /* dispatch here */
}

static SEXP altrep_UnserializeEX_default(SEXP class, SEXP state, SEXP attr,
					 int objf, int levs)
{
    altrep_methods_t *m = CLASS_METHODS_TABLE(class);
    SEXP val = m->Unserialize(class, state);
    SET_ATTRIB(val, attr);
    SET_OBJECT(val, objf);
    SETLEVELS(val, levs);
    return val;
}

static SEXP altrep_Serialized_state_default(SEXP x) { return NULL; }

static SEXP altrep_Unserialize_default(SEXP class, SEXP state)
{
    error("cannot unserialize this ALTREP object yet");
}

static SEXP altrep_Coerce_default(SEXP x, int type) { return NULL; }

static SEXP altrep_Duplicate_default(SEXP x, hyangboolean deep)
{
    return NULL;
}

static SEXP altrep_DuplicateEX_default(SEXP x, hyangboolean deep)
{
    SEXP ans = ALTREP_DUPLICATE(x, deep);

    if (ans != NULL &&
	ans != x) {
	SEXP attr = ATTRIB(x);
	if (attr != hyang_AbsurdValue) {
	    PROTECT(ans);
	    SET_ATTRIB(ans, deep ? duplicate(attr) : shallow_duplicate(attr));
	    SET_OBJECT(ans, OBJECT(x));
	    IS_S4_OBJECT(x) ? SET_S4_OBJECT(ans) : UNSET_S4_OBJECT(ans);
	    UNPROTECT(1);
	}
    }
    return ans;
}

static
hyangboolean altrep_Inspect_default(SEXP x, int pre, int deep, int pvec,
				void (*inspect_subtree)(SEXP, int, int, int))
{
    return FALSE;
}

static hyang_xlen_t altrep_Length_default(SEXP x)
{
    error("no Length method defined");
}

static void *altvec_Dataptr_default(SEXP x, hyangboolean writeable)
{
    error("cannot access data pointer for this ALTVEC object");
}

static const void *altvec_Dataptr_or_null_default(SEXP x)
{
    return NULL;
}

static SEXP altvec_Extract_subset_default(SEXP x, SEXP indx, SEXP call)
{
    return NULL;
}

static int altinteger_Elt_default(SEXP x, hyang_xlen_t i) { return INTEGER(x)[i]; }

static hyang_xlen_t
altinteger_Get_region_default(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, int *buf)
{
    hyang_xlen_t size = XLENGTH(sx);
    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    for (hyang_xlen_t k = 0; k < ncopy; k++)
	buf[k] = INTEGER_ELT(sx, k + i);
    return ncopy;
}

static int altinteger_Is_sorted_default(SEXP x) { return UNKNOWN_SORTEDNESS; }
static int altinteger_No_NA_default(SEXP x) { return 0; }

static SEXP altinteger_Sum_default(SEXP x, hyangboolean narm) { return NULL; }
static SEXP altinteger_Min_default(SEXP x, hyangboolean narm) { return NULL; }
static SEXP altinteger_Max_default(SEXP x, hyangboolean narm) { return NULL; }

static double altreal_Elt_default(SEXP x, hyang_xlen_t i) { return REAL(x)[i]; }

static hyang_xlen_t
altreal_Get_region_default(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, double *buf)
{
    hyang_xlen_t size = XLENGTH(sx);
    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    for (hyang_xlen_t k = 0; k < ncopy; k++)
	buf[k] = REAL_ELT(sx, k + i);
    return ncopy;
}

static int altreal_Is_sorted_default(SEXP x) { return UNKNOWN_SORTEDNESS; }
static int altreal_No_NA_default(SEXP x) { return 0; }

static SEXP altreal_Sum_default(SEXP x, hyangboolean narm) { return NULL; }
static SEXP altreal_Min_default(SEXP x, hyangboolean narm) { return NULL; }
static SEXP altreal_Max_default(SEXP x, hyangboolean narm) { return NULL; }

static SEXP altstring_Elt_default(SEXP x, hyang_xlen_t i)
{
    error("ALTSTRING classes must provide an Elt method");
}

static void altstring_Set_elt_default(SEXP x, hyang_xlen_t i, SEXP v)
{
    error("ALTSTRING classes must provide a Set_elt method");
}

static int altstring_Is_sorted_default(SEXP x) { return UNKNOWN_SORTEDNESS; }
static int altstring_No_NA_default(SEXP x) { return 0; }

static altinteger_methods_t altinteger_default_methods = {
    .UnserializeEX = altrep_UnserializeEX_default,
    .Unserialize = altrep_Unserialize_default,
    .Serialized_state = altrep_Serialized_state_default,
    .DuplicateEX = altrep_DuplicateEX_default,
    .Duplicate = altrep_Duplicate_default,
    .Coerce = altrep_Coerce_default,
    .Inspect = altrep_Inspect_default,
    .Length = altrep_Length_default,
    .Dataptr = altvec_Dataptr_default,
    .Dataptr_or_null = altvec_Dataptr_or_null_default,
    .Extract_subset = altvec_Extract_subset_default,
    .Elt = altinteger_Elt_default,
    .Get_region = altinteger_Get_region_default,
    .Is_sorted = altinteger_Is_sorted_default,
    .No_NA = altinteger_No_NA_default,
    .Sum = altinteger_Sum_default,
    .Min = altinteger_Min_default,
    .Max = altinteger_Max_default
};

static altreal_methods_t altreal_default_methods = {
    .UnserializeEX = altrep_UnserializeEX_default,
    .Unserialize = altrep_Unserialize_default,
    .Serialized_state = altrep_Serialized_state_default,
    .DuplicateEX = altrep_DuplicateEX_default,
    .Duplicate = altrep_Duplicate_default,
    .Coerce = altrep_Coerce_default,
    .Inspect = altrep_Inspect_default,
    .Length = altrep_Length_default,
    .Dataptr = altvec_Dataptr_default,
    .Dataptr_or_null = altvec_Dataptr_or_null_default,
    .Extract_subset = altvec_Extract_subset_default,
    .Elt = altreal_Elt_default,
    .Get_region = altreal_Get_region_default,
    .Is_sorted = altreal_Is_sorted_default,
    .No_NA = altreal_No_NA_default,
    .Sum = altreal_Sum_default,
    .Min = altreal_Min_default,
    .Max = altreal_Max_default
};


static altstring_methods_t altstring_default_methods = {
    .UnserializeEX = altrep_UnserializeEX_default,
    .Unserialize = altrep_Unserialize_default,
    .Serialized_state = altrep_Serialized_state_default,
    .DuplicateEX = altrep_DuplicateEX_default,
    .Duplicate = altrep_Duplicate_default,
    .Coerce = altrep_Coerce_default,
    .Inspect = altrep_Inspect_default,
    .Length = altrep_Length_default,
    .Dataptr = altvec_Dataptr_default,
    .Dataptr_or_null = altvec_Dataptr_or_null_default,
    .Extract_subset = altvec_Extract_subset_default,
    .Elt = altstring_Elt_default,
    .Set_elt = altstring_Set_elt_default,
    .Is_sorted = altstring_Is_sorted_default,
    .No_NA = altstring_No_NA_default
};

#define INIT_CLASS(cls, type) do {				\
	*((type##_methods_t *) (CLASS_METHODS_TABLE(cls))) =	\
	    type##_default_methods;				\
    } while (FALSE)

#define MAKE_CLASS(var, type) do {				\
	var = allocVector(RAWSXP, sizeof(type##_methods_t));	\
	hyang_PreserveObj(var);					\
	INIT_CLASS(var, type);					\
    } while (FALSE)

static HYANGINL hyang_altrep_class_t hyang_cast_altrep_class(SEXP x)
{
    hyang_altrep_class_t val = HYANG_SUBTYPE_INIT(x);
    return val;
}

static hyang_altrep_class_t
make_altrep_class(int type, const char *cname, const char *pname, DllInfo *dll)
{
    SEXP class;
    switch(type) {
    case INTSXP:  MAKE_CLASS(class, altinteger); break;
    case REALSXP: MAKE_CLASS(class, altreal);    break;
    case STRSXP:  MAKE_CLASS(class, altstring);  break;
    default: error("unsupported ALTREP class");
    }
    RegisterClass(class, type, cname, pname, dll);
    return hyang_cast_altrep_class(class);
}

#define DEFINE_CLASS_CONSTRUCTOR(cls, type)			        \
    hyang_altrep_class_t hyang_make_##cls##_class(const char *cname,	\
					  const char *pname,	        \
					  DllInfo *dll)		        \
    {								        \
	return  make_altrep_class(type, cname, pname, dll);	        \
    }

DEFINE_CLASS_CONSTRUCTOR(altstring, STRSXP)
DEFINE_CLASS_CONSTRUCTOR(altinteger, INTSXP)
DEFINE_CLASS_CONSTRUCTOR(altreal, REALSXP)

static void reinit_altrep_class(SEXP class)
{
    switch (ALTREP_CLASS_BASE_TYPE(class)) {
    case INTSXP: INIT_CLASS(class, altinteger); break;
    case REALSXP: INIT_CLASS(class, altreal); break;
    case STRSXP: INIT_CLASS(class, altstring); break;
    default: error("unsupported ALTREP class");
    }
}

#define DEFINE_METHOD_SETTER(CNAME, MNAME)				          \
    void hyang_set_##CNAME##_##MNAME##_method(hyang_altrep_class_t cls,		  \
					  hyang_##CNAME##_##MNAME##_method_t fun) \
    {									          \
	CNAME##_methods_t *m = CLASS_METHODS_TABLE(HYANG_SEXP(cls));	          \
	m->MNAME = fun;							          \
    }

DEFINE_METHOD_SETTER(altrep, UnserializeEX)
DEFINE_METHOD_SETTER(altrep, Unserialize)
DEFINE_METHOD_SETTER(altrep, Serialized_state)
DEFINE_METHOD_SETTER(altrep, DuplicateEX)
DEFINE_METHOD_SETTER(altrep, Duplicate)
DEFINE_METHOD_SETTER(altrep, Coerce)
DEFINE_METHOD_SETTER(altrep, Inspect)
DEFINE_METHOD_SETTER(altrep, Length)

DEFINE_METHOD_SETTER(altvec, Dataptr)
DEFINE_METHOD_SETTER(altvec, Dataptr_or_null)
DEFINE_METHOD_SETTER(altvec, Extract_subset)

DEFINE_METHOD_SETTER(altinteger, Elt)
DEFINE_METHOD_SETTER(altinteger, Get_region)
DEFINE_METHOD_SETTER(altinteger, Is_sorted)
DEFINE_METHOD_SETTER(altinteger, No_NA)
DEFINE_METHOD_SETTER(altinteger, Sum)
DEFINE_METHOD_SETTER(altinteger, Min)
DEFINE_METHOD_SETTER(altinteger, Max)

DEFINE_METHOD_SETTER(altreal, Elt)
DEFINE_METHOD_SETTER(altreal, Get_region)
DEFINE_METHOD_SETTER(altreal, Is_sorted)
DEFINE_METHOD_SETTER(altreal, No_NA)
DEFINE_METHOD_SETTER(altreal, Sum)
DEFINE_METHOD_SETTER(altreal, Min)
DEFINE_METHOD_SETTER(altreal, Max)

DEFINE_METHOD_SETTER(altstring, Elt)
DEFINE_METHOD_SETTER(altstring, Set_elt)
DEFINE_METHOD_SETTER(altstring, Is_sorted)
DEFINE_METHOD_SETTER(altstring, No_NA)

SEXP hyang_new_altrep(hyang_altrep_class_t class, SEXP data1, SEXP data2)
{
    SEXP sclass = HYANG_SEXP(class);
    int type = ALTREP_CLASS_BASE_TYPE(sclass);
    SEXP ans = CONS(data1, data2);
    SET_TYPEOF(ans, type);
    SET_ALTREP_CLASS(ans, sclass);
    return ans;
}

hyangboolean hyang_altrep_inherits(SEXP x, hyang_altrep_class_t class)
{
    return ALTREP(x) && ALTREP_CLASS(x) == HYANG_SEXP(class);
}

#define COMPACT_SEQ_INFO(x) hyang_altrep_data1(x)
#define COMPACT_SEQ_EXPANDED(x) hyang_altrep_data2(x)
#define SET_COMPACT_SEQ_EXPANDED(x, v) hyang_set_altrep_data2(x, v)

#define COMPACT_INTSEQ_SERIALIZED_STATE_LENGTH(info) \
    (TYPEOF(info) == INTSXP ? INTEGER0(info)[0] : (hyang_xlen_t) REAL0(info)[0])
#define COMPACT_INTSEQ_SERIALIZED_STATE_FIRST(info) \
    (TYPEOF(info) == INTSXP ? INTEGER0(info)[1] : (int) REAL0(info)[1])
#define COMPACT_INTSEQ_SERIALIZED_STATE_INCR(info) \
    (TYPEOF(info) == INTSXP ? INTEGER0(info)[2] : (int) REAL0(info)[2])

#define COMPACT_INTSEQ_INFO_LENGTH(info) ((hyang_xlen_t) REAL0(info)[0])
#define COMPACT_INTSEQ_INFO_FIRST(info) ((int) REAL0(info)[1])
#define COMPACT_INTSEQ_INFO_INCR(info) ((int) REAL0(info)[2])

static SEXP compact_intseq_Serialized_state(SEXP x)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return NULL;
#endif
    return COMPACT_SEQ_INFO(x);
}

static SEXP new_compact_intseq(hyang_xlen_t, int, int);
static SEXP new_compact_realseq(hyang_xlen_t, double, double);

static SEXP compact_intseq_Unserialize(SEXP class, SEXP state)
{
    hyang_xlen_t n = COMPACT_INTSEQ_SERIALIZED_STATE_LENGTH(state);
    int n1 = COMPACT_INTSEQ_SERIALIZED_STATE_FIRST(state);
    int inc = COMPACT_INTSEQ_SERIALIZED_STATE_INCR(state);

    if (inc == 1)
	return new_compact_intseq(n, n1,  1);
    else if (inc == -1)
	return new_compact_intseq(n, n1,  -1);
    else
	error("compact sequences with increment %d not supported yet", inc);
}

static SEXP compact_intseq_Coerce(SEXP x, int type)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return NULL;
#endif
    if (type == REALSXP) {
	SEXP info = COMPACT_SEQ_INFO(x);
	hyang_xlen_t n = COMPACT_INTSEQ_INFO_LENGTH(info);
	int n1 = COMPACT_INTSEQ_INFO_FIRST(info);
	int inc = COMPACT_INTSEQ_INFO_INCR(info);
	return new_compact_realseq(n, n1, inc);
    }
    else return NULL;
}

static SEXP compact_intseq_Duplicate(SEXP x, hyangboolean deep)
{
    hyang_xlen_t n = XLENGTH(x);
    SEXP val = allocVector(INTSXP, n);
    INTEGER_GET_REGION(x, 0, n, INTEGER0(val));
    return val;
}

static
hyangboolean compact_intseq_Inspect(SEXP x, int pre, int deep, int pvec,
				void (*inspect_subtree)(SEXP, int, int, int))
{
    int inc = COMPACT_INTSEQ_INFO_INCR(COMPACT_SEQ_INFO(x));
    if (inc != 1 && inc != -1)
	error("compact sequences with increment %d not supported yet", inc);

#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue) {
	hyangprtf("  <expanded compact integer sequence>\n");
	inspect_subtree(COMPACT_SEQ_EXPANDED(x), pre, deep, pvec);
	return TRUE;
    }
#endif

    int n = LENGTH(x);
    int n1 = INTEGER_ELT(x, 0);
    int n2 = inc == 1 ? n1 + n - 1 : n1 - n + 1;
    hyangprtf(" %d : %d (%s)", n1, n2,
	    COMPACT_SEQ_EXPANDED(x) == hyang_AbsurdValue ? "compact" : "expanded");
    hyangprtf("\n");
    return TRUE;
}

static HYANGINL hyang_xlen_t compact_intseq_Length(SEXP x)
{
    SEXP info = COMPACT_SEQ_INFO(x);
    return COMPACT_INTSEQ_INFO_LENGTH(info);
}

static void *compact_intseq_Dataptr(SEXP x, hyangboolean writeable)
{
    if (COMPACT_SEQ_EXPANDED(x) == hyang_AbsurdValue) {
	PROTECT(x);
	SEXP info = COMPACT_SEQ_INFO(x);
	hyang_xlen_t n = COMPACT_INTSEQ_INFO_LENGTH(info);
	int n1 = COMPACT_INTSEQ_INFO_FIRST(info);
	int inc = COMPACT_INTSEQ_INFO_INCR(info);
	SEXP val = allocVector(INTSXP, n);
	int *data = INTEGER(val);

	if (inc == 1) {
	    for (int i = 0; i < n; i++)
		data[i] = n1 + i;
	}
	else if (inc == -1) {
	    for (int i = 0; i < n; i++)
		data[i] = n1 - i;
	}
	else
	    error("compact sequences with increment %d not supported yet", inc);

	SET_COMPACT_SEQ_EXPANDED(x, val);
	UNPROTECT(1);
    }
    return DATAPTR(COMPACT_SEQ_EXPANDED(x));
}

static const void *compact_intseq_Dataptr_or_null(SEXP x)
{
    SEXP val = COMPACT_SEQ_EXPANDED(x);
    return val == hyang_AbsurdValue ? NULL : DATAPTR(val);
}

static int compact_intseq_Elt(SEXP x, hyang_xlen_t i)
{
    SEXP ex = COMPACT_SEQ_EXPANDED(x);
    if (ex != hyang_AbsurdValue)
	return INTEGER0(ex)[i];
    else {
	SEXP info = COMPACT_SEQ_INFO(x);
	int n1 = COMPACT_INTSEQ_INFO_FIRST(info);
	int inc = COMPACT_INTSEQ_INFO_INCR(info);
	return (int) (n1 + inc * i);
    }
}

static hyang_xlen_t
compact_intseq_Get_region(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, int *buf)
{
    CHECK_NOT_EXPANDED(sx);

    SEXP info = COMPACT_SEQ_INFO(sx);
    hyang_xlen_t size = COMPACT_INTSEQ_INFO_LENGTH(info);
    hyang_xlen_t n1 = COMPACT_INTSEQ_INFO_FIRST(info);
    int inc = COMPACT_INTSEQ_INFO_INCR(info);

    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    if (inc == 1) {
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = (int) (n1 + k + i);
	return ncopy;
    }
    else if (inc == -1) {
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = (int) (n1 - k - i);
	return ncopy;
    }
    else
	error("compact sequences with increment %d not supported yet", inc);
}

static int compact_intseq_Is_sorted(SEXP x)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return UNKNOWN_SORTEDNESS;
#endif
    int inc = COMPACT_INTSEQ_INFO_INCR(COMPACT_SEQ_INFO(x));
    return inc < 0 ? SORTED_DECR : SORTED_INCR;
}

static int compact_intseq_No_NA(SEXP x)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return FALSE;
#endif
    return TRUE;
}

#define HYANG_INT_MIN (1 + INT_MIN)

static SEXP compact_intseq_Sum(SEXP x, hyangboolean narm)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return NULL;
#endif
    double tmp;
    SEXP info = COMPACT_SEQ_INFO(x);
    hyang_xlen_t size = COMPACT_INTSEQ_INFO_LENGTH(info);
    hyang_xlen_t n1 = COMPACT_INTSEQ_INFO_FIRST(info);
    int inc = COMPACT_INTSEQ_INFO_INCR(info);
    tmp = (size / 2.0) * (n1 + n1 + inc * (size - 1));
    if(tmp > INT_MAX || tmp < HYANG_INT_MIN)
	return ScalarReal(tmp);
    else
	return ScalarInteger((int) tmp);
}

hyang_altrep_class_t hyang_compact_intseq_class;

static void InitCompactIntegerClass()
{
    hyang_altrep_class_t cls = hyang_make_altinteger_class("compact_intseq", "base",
						   NULL);
    hyang_compact_intseq_class = cls;

    hyang_set_altrep_Unserialize_method(cls, compact_intseq_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, compact_intseq_Serialized_state);
    hyang_set_altrep_Duplicate_method(cls, compact_intseq_Duplicate);
    hyang_set_altrep_Coerce_method(cls, compact_intseq_Coerce);
    hyang_set_altrep_Inspect_method(cls, compact_intseq_Inspect);
    hyang_set_altrep_Length_method(cls, compact_intseq_Length);

    hyang_set_altvec_Dataptr_method(cls, compact_intseq_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, compact_intseq_Dataptr_or_null);

    hyang_set_altinteger_Elt_method(cls, compact_intseq_Elt);
    hyang_set_altinteger_Get_region_method(cls, compact_intseq_Get_region);
    hyang_set_altinteger_Is_sorted_method(cls, compact_intseq_Is_sorted);
    hyang_set_altinteger_No_NA_method(cls, compact_intseq_No_NA);
    hyang_set_altinteger_Sum_method(cls, compact_intseq_Sum);
}

static SEXP new_compact_intseq(hyang_xlen_t n, int n1, int inc)
{
    if (n == 1) return ScalarInteger(n1);

    if (inc != 1 && inc != -1)
	error("compact sequences with increment %d not supported yet", inc);

    SEXP info = allocVector(REALSXP, 3);
    REAL0(info)[0] = (double) n;
    REAL0(info)[1] = (double) n1;
    REAL0(info)[2] = (double) inc;

    SEXP ans = hyang_new_altrep(hyang_compact_intseq_class, info, hyang_AbsurdValue);
#ifndef COMPACT_INTSEQ_MUTABLE
    MARK_NOT_MUTABLE(ans);
#endif

    return ans;
}

#define COMPACT_REALSEQ_INFO_LENGTH(info) ((hyang_xlen_t) REAL0(info)[0])
#define COMPACT_REALSEQ_INFO_FIRST(info) REAL0(info)[1]
#define COMPACT_REALSEQ_INFO_INCR(info) REAL0(info)[2]

static SEXP compact_realseq_Serialized_state(SEXP x)
{
    return COMPACT_SEQ_INFO(x);
}

static SEXP compact_realseq_Unserialize(SEXP class, SEXP state)
{
    double inc = COMPACT_REALSEQ_INFO_INCR(state);
    hyang_xlen_t len = COMPACT_REALSEQ_INFO_LENGTH(state);
    double n1 = COMPACT_REALSEQ_INFO_FIRST(state);

    if (inc == 1)
	return new_compact_realseq(len, n1,  1);
    else if (inc == -1)
	return new_compact_realseq(len, n1, -1);
    else
	error("compact sequences with increment %f not supported yet", inc);
}

static SEXP compact_realseq_Duplicate(SEXP x, hyangboolean deep)
{
    hyang_xlen_t n = XLENGTH(x);
    SEXP val = allocVector(REALSXP, n);
    REAL_GET_REGION(x, 0, n, REAL0(val));
    return val;
}

static
hyangboolean compact_realseq_Inspect(SEXP x, int pre, int deep, int pvec,
				 void (*inspect_subtree)(SEXP, int, int, int))
{
    double inc = COMPACT_REALSEQ_INFO_INCR(COMPACT_SEQ_INFO(x));
    if (inc != 1 && inc != -1)
	error("compact sequences with increment %f not supported yet", inc);

    hyang_xlen_t n = XLENGTH(x);
    hyang_xlen_t n1 = (hyang_xlen_t) REAL_ELT(x, 0);
    hyang_xlen_t n2 = inc == 1 ? n1 + n - 1 : n1 - n + 1;
    hyangprtf(" %ld : %ld (%s)", n1, n2,
	    COMPACT_SEQ_EXPANDED(x) == hyang_AbsurdValue ? "compact" : "expanded");
    hyangprtf("\n");
    return TRUE;
}

static HYANGINL hyang_xlen_t compact_realseq_Length(SEXP x)
{
    return (hyang_xlen_t) REAL0(COMPACT_SEQ_INFO(x))[0];
}

static void *compact_realseq_Dataptr(SEXP x, hyangboolean writeable)
{
    if (COMPACT_SEQ_EXPANDED(x) == hyang_AbsurdValue) {
	PROTECT(x);
	SEXP info = COMPACT_SEQ_INFO(x);
	hyang_xlen_t n = COMPACT_REALSEQ_INFO_LENGTH(info);
	double n1 = COMPACT_REALSEQ_INFO_FIRST(info);
	double inc = COMPACT_REALSEQ_INFO_INCR(info);
	SEXP val = allocVector(REALSXP, (hyang_xlen_t) n);
	double *data = REAL(val);

	if (inc == 1) {
	    for (hyang_xlen_t i = 0; i < n; i++)
		data[i] = n1 + i;
	}
	else if (inc == -1) {
	    for (hyang_xlen_t i = 0; i < n; i++)
		data[i] = n1 - i;
	}
	else
	    error("compact sequences with increment %f not supported yet", inc);

	SET_COMPACT_SEQ_EXPANDED(x, val);
	UNPROTECT(1);
    }
    return DATAPTR(COMPACT_SEQ_EXPANDED(x));
}

static const void *compact_realseq_Dataptr_or_null(SEXP x)
{
    SEXP val = COMPACT_SEQ_EXPANDED(x);
    return val == hyang_AbsurdValue ? NULL : DATAPTR(val);
}

static double compact_realseq_Elt(SEXP x, hyang_xlen_t i)
{
    SEXP ex = COMPACT_SEQ_EXPANDED(x);
    if (ex != hyang_AbsurdValue)
	return REAL0(ex)[i];
    else {
	SEXP info = COMPACT_SEQ_INFO(x);
	double n1 = COMPACT_REALSEQ_INFO_FIRST(info);
	double inc = COMPACT_REALSEQ_INFO_INCR(info);
	return n1 + inc * i;
    }
}

static hyang_xlen_t
compact_realseq_Get_region(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, double *buf)
{
    CHECK_NOT_EXPANDED(sx);

    SEXP info = COMPACT_SEQ_INFO(sx);
    hyang_xlen_t size = COMPACT_REALSEQ_INFO_LENGTH(info);
    double n1 = COMPACT_REALSEQ_INFO_FIRST(info);
    double inc = COMPACT_REALSEQ_INFO_INCR(info);

    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    if (inc == 1) {
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = n1 + k + i;
	return ncopy;
    }
    else if (inc == -1) {
	for (hyang_xlen_t k = 0; k < ncopy; k++)
	    buf[k] = n1 - k - i;
	return ncopy;
    }
    else
	error("compact sequences with increment %f not supported yet", inc);
}

static int compact_realseq_Is_sorted(SEXP x)
{
#ifdef COMPACT_REALSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return UNKNOWN_SORTEDNESS;
#endif
    double inc = COMPACT_REALSEQ_INFO_INCR(COMPACT_SEQ_INFO(x));
    return inc < 0 ? SORTED_DECR : SORTED_INCR;
}

static int compact_realseq_No_NA(SEXP x)
{
#ifdef COMPACT_REALSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return FALSE;
#endif
    return TRUE;
}

static SEXP compact_realseq_Sum(SEXP x, hyangboolean narm)
{
#ifdef COMPACT_INTSEQ_MUTABLE
    if (COMPACT_SEQ_EXPANDED(x) != hyang_AbsurdValue)
	return NULL;
#endif
    SEXP info = COMPACT_SEQ_INFO(x);
    double size = (double) COMPACT_REALSEQ_INFO_LENGTH(info);
    double n1 = COMPACT_REALSEQ_INFO_FIRST(info);
    double inc = COMPACT_REALSEQ_INFO_INCR(info);
    return ScalarReal((size / 2.0) *(n1 + n1 + inc * (size - 1)));
}

hyang_altrep_class_t hyang_compact_realseq_class;

static void InitCompactRealClass()
{
    hyang_altrep_class_t cls = hyang_make_altreal_class("compact_realseq", "base",
						NULL);
    hyang_compact_realseq_class = cls;

    hyang_set_altrep_Unserialize_method(cls, compact_realseq_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, compact_realseq_Serialized_state);
    hyang_set_altrep_Duplicate_method(cls, compact_realseq_Duplicate);
    hyang_set_altrep_Inspect_method(cls, compact_realseq_Inspect);
    hyang_set_altrep_Length_method(cls, compact_realseq_Length);

    hyang_set_altvec_Dataptr_method(cls, compact_realseq_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, compact_realseq_Dataptr_or_null);

    hyang_set_altreal_Elt_method(cls, compact_realseq_Elt);
    hyang_set_altreal_Get_region_method(cls, compact_realseq_Get_region);
    hyang_set_altreal_Is_sorted_method(cls, compact_realseq_Is_sorted);
    hyang_set_altreal_No_NA_method(cls, compact_realseq_No_NA);
    hyang_set_altreal_Sum_method(cls, compact_realseq_Sum);
}

static SEXP new_compact_realseq(hyang_xlen_t n, double n1, double inc)
{
    if (n == 1) return ScalarReal(n1);

    if (inc != 1 && inc != -1)
	error("compact sequences with increment %f not supported yet", inc);

    SEXP info = allocVector(REALSXP, 3);
    REAL(info)[0] = n;
    REAL(info)[1] = n1;
    REAL(info)[2] = inc;

    SEXP ans = hyang_new_altrep(hyang_compact_realseq_class, info, hyang_AbsurdValue);
    MARK_NOT_MUTABLE(ans);
    return ans;
}

SEXP attribute_hidden hyang_compact_intrange(hyang_xlen_t n1, hyang_xlen_t n2)
{
    hyang_xlen_t n = n1 <= n2 ? n2 - n1 + 1 : n1 - n2 + 1;

    if (n >= HYANG_XLEN_T_MAX)
	error("result would be too long a vector");

    if (n1 <= INT_MIN || n1 > INT_MAX || n2 <= INT_MIN || n2 > INT_MAX)
	return new_compact_realseq(n, n1, n1 <= n2 ? 1 : -1);
    else
	return new_compact_intseq(n, (int) n1, n1 <= n2 ? 1 : -1);
}

#define DEFERRED_STRING_STATE(x) hyang_altrep_data1(x)
#define	CLEAR_DEFERRED_STRING_STATE(x) hyang_set_altrep_data1(x, hyang_AbsurdValue)
#define DEFERRED_STRING_EXPANDED(x) hyang_altrep_data2(x)
#define SET_DEFERRED_STRING_EXPANDED(x, v) hyang_set_altrep_data2(x, v)

#define MAKE_DEFERRED_STRING_STATE(v, sp) CONS(v, sp)
#define DEFERRED_STRING_STATE_ARG(s) CAR(s)
#define DEFERRED_STRING_STATE_SCIPEN(s) CDR(s)

#define DEFERRED_STRING_ARG(x) \
    DEFERRED_STRING_STATE_ARG(DEFERRED_STRING_STATE(x))
#define DEFERRED_STRING_SCIPEN(x) \
    DEFERRED_STRING_STATE_SCIPEN(DEFERRED_STRING_STATE(x))

static SEXP deferred_string_Serialized_state(SEXP x)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    return state != hyang_AbsurdValue ? state : NULL;
}

static SEXP deferred_string_Unserialize(SEXP class, SEXP state)
{
    SEXP arg = DEFERRED_STRING_STATE_ARG(state);
    SEXP sp = DEFERRED_STRING_STATE_SCIPEN(state);
    return hyang_deferred_coerceToStr(arg, sp);
}

static
hyangboolean deferred_string_Inspect(SEXP x, int pre, int deep, int pvec,
				 void (*inspect_subtree)(SEXP, int, int, int))
{
    SEXP state = DEFERRED_STRING_STATE(x);
    if (state != hyang_AbsurdValue) {
	SEXP arg = DEFERRED_STRING_STATE_ARG(state);
	hyangprtf("  <deferred string conversion>\n");
	inspect_subtree(arg, pre, deep, pvec);
    }
    else {
	hyangprtf("  <expanded string conversion>\n");
	inspect_subtree(DEFERRED_STRING_EXPANDED(x), pre, deep, pvec);
    }
    return TRUE;
}

static HYANGINL hyang_xlen_t deferred_string_Length(SEXP x)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    return state == hyang_AbsurdValue ?
	XLENGTH(DEFERRED_STRING_EXPANDED(x)) :
	XLENGTH(DEFERRED_STRING_STATE_ARG(state));
}

static HYANGINL SEXP ExpandDeferredStringElt(SEXP x, hyang_xlen_t i)
{
    SEXP val = DEFERRED_STRING_EXPANDED(x);
    if (val == hyang_AbsurdValue) {
	hyang_xlen_t n = XLENGTH(x);
	val = allocVector(STRSXP, n);
	memset(STDVEC_DATAPTR(val), 0, n * sizeof(SEXP));
	SET_DEFERRED_STRING_EXPANDED(x, val);
    }

    SEXP elt = STRING_ELT(val, i);
    if (elt == NULL) {
	int warn;
	int savedigits, savescipen;
	SEXP data = DEFERRED_STRING_ARG(x);
	switch(TYPEOF(data)) {
	case INTSXP:
	    elt = StringFromInteger(INTEGER_ELT(data, i), &warn);
	    break;
	case REALSXP:
	    savedigits = hyang_print.digits;
	    savescipen = hyang_print.scipen;
	    hyang_print.digits = DBL_DIG;
	    hyang_print.scipen = INTEGER0(DEFERRED_STRING_SCIPEN(x))[0];
	    elt = StringFromReal(REAL_ELT(data, i), &warn);
	    hyang_print.digits = savedigits;
	    hyang_print.scipen = savescipen;
	    break;
	default:
	    error("unsupported type for deferred string coercion");
	}
	SET_STRING_ELT(val, i, elt);
    }
    return elt;
}

static HYANGINL void expand_deferred_string(SEXP x)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    if (state != hyang_AbsurdValue) {
	PROTECT(x);
	hyang_xlen_t n = XLENGTH(x), i;
	if (n == 0)
	    SET_DEFERRED_STRING_EXPANDED(x, allocVector(STRSXP, 0));
	else
	    for (i = 0; i < n; i++)
		ExpandDeferredStringElt(x, i);
	CLEAR_DEFERRED_STRING_STATE(x);
	UNPROTECT(1);
    }
}

static void *deferred_string_Dataptr(SEXP x, hyangboolean writeable)
{
    expand_deferred_string(x);
    return DATAPTR(DEFERRED_STRING_EXPANDED(x));
}

static const void *deferred_string_Dataptr_or_null(SEXP x)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    return state != hyang_AbsurdValue ? NULL : DATAPTR(DEFERRED_STRING_EXPANDED(x));
}

static SEXP deferred_string_Elt(SEXP x, hyang_xlen_t i)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    if (state == hyang_AbsurdValue)
	return STRING_ELT(DEFERRED_STRING_EXPANDED(x), i);
    else {
	PROTECT(x);
	SEXP elt = ExpandDeferredStringElt(x, i);
	UNPROTECT(1);
	return elt;
    }
}

static void deferred_string_Set_elt(SEXP x, hyang_xlen_t i, SEXP v)
{
    expand_deferred_string(x);
    SET_STRING_ELT(DEFERRED_STRING_EXPANDED(x), i, v);
}

static int deferred_string_Is_sorted(SEXP x)
{
    return UNKNOWN_SORTEDNESS;
}

static int deferred_string_No_NA(SEXP x)
{
    SEXP state = DEFERRED_STRING_STATE(x);
    if (state == hyang_AbsurdValue)
	return FALSE;
    else {
	SEXP arg = DEFERRED_STRING_STATE_ARG(state);
	switch(TYPEOF(arg)) {
	case INTSXP: return INTEGER_NO_NA(arg);
	case REALSXP: return REAL_NO_NA(arg);
	default: return FALSE;
	}
    }
}

static SEXP deferred_string_Extract_subset(SEXP x, SEXP indx, SEXP call)
{
    SEXP result = NULL;

    if (! OBJECT(x) && ATTRIB(x) == hyang_AbsurdValue &&
	DEFERRED_STRING_STATE(x) != hyang_AbsurdValue) {
	SEXP data = DEFERRED_STRING_ARG(x);
	SEXP sp = DEFERRED_STRING_SCIPEN(x);
	PROTECT(result = ExtractSubset(data, indx, call));
	result = hyang_deferred_coerceToStr(result, sp);
	UNPROTECT(1);
	return result;
    }

    return result;
}

static hyang_altrep_class_t hyang_deferred_string_class;

static void InitDefferredStringClass()
{
    hyang_altrep_class_t cls = hyang_make_altstring_class("deferred_string", "base",
						  NULL);
    hyang_deferred_string_class = cls;

    hyang_set_altrep_Unserialize_method(cls, deferred_string_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, deferred_string_Serialized_state);
    hyang_set_altrep_Inspect_method(cls, deferred_string_Inspect);
    hyang_set_altrep_Length_method(cls, deferred_string_Length);

    hyang_set_altvec_Dataptr_method(cls, deferred_string_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, deferred_string_Dataptr_or_null);
    hyang_set_altvec_Extract_subset_method(cls, deferred_string_Extract_subset);

    hyang_set_altstring_Elt_method(cls, deferred_string_Elt);
    hyang_set_altstring_Set_elt_method(cls, deferred_string_Set_elt);
    hyang_set_altstring_Is_sorted_method(cls, deferred_string_Is_sorted);
    hyang_set_altstring_No_NA_method(cls, deferred_string_No_NA);
}

SEXP attribute_hidden hyang_deferred_coerceToStr(SEXP v, SEXP sp)
{
    SEXP ans = hyang_AbsurdValue;
    switch (TYPEOF(v)) {
    case INTSXP:
    case REALSXP:
	PROTECT(v);
	if (sp == NULL) {
	    PrintDefaults();
	    sp = ScalarInteger(hyang_print.scipen);
	}
	MARK_NOT_MUTABLE(v);
	ans = PROTECT(MAKE_DEFERRED_STRING_STATE(v, sp));
	ans = hyang_new_altrep(hyang_deferred_string_class, ans, hyang_AbsurdValue);
	UNPROTECT(2);
	break;
    default:
	error("unsupported type for deferred string coercion");
    }
    return ans;
}

static SEXP make_mmap_state(SEXP file, size_t size, int type,
			    hyangboolean ptrOK, hyangboolean wrtOK, hyangboolean serOK)
{
    SEXP sizes = PROTECT(allocVector(REALSXP, 2));
    double *dsizes = REAL(sizes);
    dsizes[0] = size;
    switch(type) {
    case INTSXP: dsizes[1] = size / sizeof(int); break;
    case REALSXP: dsizes[1] = size / sizeof(double); break;
    default: error("mmap for %s not supported yet", type2char(type));
    }

    SEXP info = PROTECT(allocVector(INTSXP, 4));
    INTEGER(info)[0] = type;
    INTEGER(info)[1] = ptrOK;
    INTEGER(info)[2] = wrtOK;
    INTEGER(info)[3] = serOK;

    SEXP state = list3(file, sizes, info);

    UNPROTECT(2);
    return state;
}

#define MMAP_STATE_FILE(x) CAR(x)
#define MMAP_STATE_SIZE(x) ((size_t) REAL_ELT(CADR(x), 0))
#define MMAP_STATE_LENGTH(x) ((size_t) REAL_ELT(CADR(x), 1))
#define MMAP_STATE_TYPE(x) INTEGER(CADDR(x))[0]
#define MMAP_STATE_PTROK(x) INTEGER(CADDR(x))[1]
#define MMAP_STATE_WRTOK(x) INTEGER(CADDR(x))[2]
#define MMAP_STATE_SEROK(x) INTEGER(CADDR(x))[3]

static hyang_altrep_class_t mmap_integer_class;
static hyang_altrep_class_t mmap_real_class;

static void register_mmap_eptr(SEXP eptr);
static SEXP make_mmap(void *p, SEXP file, size_t size, int type,
		      hyangboolean ptrOK, hyangboolean wrtOK, hyangboolean serOK)
{
    SEXP state = PROTECT(make_mmap_state(file, size,
					 type, ptrOK, wrtOK, serOK));
    SEXP eptr = PROTECT(hyang_MakeExtPtr(p, hyang_AbsurdValue, state));
    register_mmap_eptr(eptr);

    hyang_altrep_class_t class;
    switch(type) {
    case INTSXP:
	class = mmap_integer_class;
	break;
    case REALSXP:
	class = mmap_real_class;
	break;
    default: error("mmap for %s not supported yet", type2char(type));
    }

    SEXP ans = hyang_new_altrep(class, eptr, state);
    if (ptrOK && ! wrtOK)
	MARK_NOT_MUTABLE(ans);

    UNPROTECT(2);
    return ans;
}

#define MMAP_EPTR(x) hyang_altrep_data1(x)
#define MMAP_STATE(x) hyang_altrep_data2(x)
#define MMAP_LENGTH(x) MMAP_STATE_LENGTH(MMAP_STATE(x))
#define MMAP_PTROK(x) MMAP_STATE_PTROK(MMAP_STATE(x))
#define MMAP_WRTOK(x) MMAP_STATE_WRTOK(MMAP_STATE(x))
#define MMAP_SEROK(x) MMAP_STATE_SEROK(MMAP_STATE(x))

#define MMAP_EPTR_STATE(x) hyang_ExternalPtrProtected(x)

static HYANGINL void *MMAP_ADDR(SEXP x)
{
    SEXP eptr = MMAP_EPTR(x);
    void *addr = hyang_ExtPtrAddr(eptr);

    if (addr == NULL)
	error("object has been unmapped");
    return addr;
}

static SEXP mmap_list = NULL;

#define MAXCOUNT 10

static void mmap_finalize(SEXP eptr);
static void register_mmap_eptr(SEXP eptr)
{
    if (mmap_list == NULL) {
	mmap_list = CONS(hyang_AbsurdValue, hyang_AbsurdValue);
	hyang_PreserveObj(mmap_list);
    }

    static int cleancount = MAXCOUNT;
    if (--cleancount <= 0) {
	cleancount = MAXCOUNT;
	for (SEXP last = mmap_list, next = CDR(mmap_list);
	     next != hyang_AbsurdValue;
	     next = CDR(next))
	    if (hyang_WeakRefKey(CAR(next)) == hyang_AbsurdValue)
		SETCDR(last, CDR(next));
	    else
		last = next;
    }

    SETCDR(mmap_list,
	   CONS(hyang_MakeWeakRefC(eptr, hyang_AbsurdValue, mmap_finalize, TRUE),
		CDR(mmap_list)));

    hyang_SetExternalPtrTag(eptr, CAR(CDR(mmap_list)));
}

#ifdef SIMPLEMMAP
static void finalize_mmap_objects()
{
    if (mmap_list == NULL)
	return;
    for (SEXP next = CDR(mmap_list); next != hyang_AbsurdValue; next = CDR(next))
	hyang_RunWeakRefFinalizer(CAR(next));
    hyang_ReleaseObj(mmap_list);
}
#endif

static SEXP mmap_Serialized_state(SEXP x)
{
    if (MMAP_SEROK(x))
	return MMAP_STATE(x);
    else
	return NULL;
}

static SEXP mmap_file(SEXP, int, hyangboolean, hyangboolean, hyangboolean, hyangboolean);

static SEXP mmap_Unserialize(SEXP class, SEXP state)
{
    SEXP file = MMAP_STATE_FILE(state);
    int type = MMAP_STATE_TYPE(state);
    hyangboolean ptrOK = MMAP_STATE_PTROK(state);
    hyangboolean wrtOK = MMAP_STATE_WRTOK(state);
    hyangboolean serOK = MMAP_STATE_SEROK(state);

    SEXP val = mmap_file(file, type, ptrOK, wrtOK, serOK, TRUE);
    if (val == NULL) {
	warning("memory mapping failed; returning vector of length zero");
	return allocVector(type, 0);
    }
    return val;
}

hyangboolean mmap_Inspect(SEXP x, int pre, int deep, int pvec,
		      void (*inspect_subtree)(SEXP, int, int, int))
{
    hyangboolean ptrOK = MMAP_PTROK(x);
    hyangboolean wrtOK = MMAP_WRTOK(x);
    hyangboolean serOK = MMAP_SEROK(x);
    hyangprtf(" mmaped %s", type2char(TYPEOF(x)));
    hyangprtf(" [ptr=%d,wrt=%d,ser=%d]\n", ptrOK, wrtOK, serOK);
    return TRUE;
}

static hyang_xlen_t mmap_Length(SEXP x)
{
    return MMAP_LENGTH(x);
}

static void *mmap_Dataptr(SEXP x, hyangboolean writeable)
{
    void *addr = MMAP_ADDR(x);

    if (MMAP_PTROK(x))
	return addr;
    else
	error("cannot access data pointer for this mmaped vector");
}

static const void *mmap_Dataptr_or_null(SEXP x)
{
    return MMAP_PTROK(x) ? MMAP_ADDR(x) : NULL;
}

static int mmap_integer_Elt(SEXP x, hyang_xlen_t i)
{
    int *p = MMAP_ADDR(x);
    return p[i];
}

static
hyang_xlen_t mmap_integer_Get_region(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, int *buf)
{
    int *x = MMAP_ADDR(sx);
    hyang_xlen_t size = XLENGTH(sx);
    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    for (hyang_xlen_t k = 0; k < ncopy; k++)
	buf[k] = x[k + i];
    return ncopy;
}

static double mmap_real_Elt(SEXP x, hyang_xlen_t i)
{
    double *p = MMAP_ADDR(x);
    return p[i];
}

static
hyang_xlen_t mmap_real_Get_region(SEXP sx, hyang_xlen_t i, hyang_xlen_t n, double *buf)
{
    double *x = MMAP_ADDR(sx);
    hyang_xlen_t size = XLENGTH(sx);
    hyang_xlen_t ncopy = size - i > n ? n : size - i;
    for (hyang_xlen_t k = 0; k < ncopy; k++)
	buf[k] = x[k + i];
    return ncopy;
}

#ifdef SIMPLEMMAP
# define MMAPPKG "simplemmap"
#else
# define MMAPPKG "base"
#endif

static void InitMmapIntegerClass(DllInfo *dll)
{
    hyang_altrep_class_t cls =
	hyang_make_altinteger_class("mmap_integer", MMAPPKG, dll);
    mmap_integer_class = cls;
    hyang_set_altrep_Unserialize_method(cls, mmap_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, mmap_Serialized_state);
    hyang_set_altrep_Inspect_method(cls, mmap_Inspect);
    hyang_set_altrep_Length_method(cls, mmap_Length);

    hyang_set_altvec_Dataptr_method(cls, mmap_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, mmap_Dataptr_or_null);

    hyang_set_altinteger_Elt_method(cls, mmap_integer_Elt);
    hyang_set_altinteger_Get_region_method(cls, mmap_integer_Get_region);
}

static void InitMmapRealClass(DllInfo *dll)
{
    hyang_altrep_class_t cls =
	hyang_make_altreal_class("mmap_real", MMAPPKG, dll);
    mmap_real_class = cls;

    hyang_set_altrep_Unserialize_method(cls, mmap_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, mmap_Serialized_state);
    hyang_set_altrep_Inspect_method(cls, mmap_Inspect);
    hyang_set_altrep_Length_method(cls, mmap_Length);

    hyang_set_altvec_Dataptr_method(cls, mmap_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, mmap_Dataptr_or_null);

    hyang_set_altreal_Elt_method(cls, mmap_real_Elt);
    hyang_set_altreal_Get_region_method(cls, mmap_real_Get_region);
}

#ifdef Win32
static void mmap_finalize(SEXP eptr)
{
    error("mmop objects not supported on Windows yet");
}

static SEXP mmap_file(SEXP file, int type, hyangboolean ptrOK, hyangboolean wrtOK,
		      hyangboolean serOK, hyangboolean warn)
{
    error("mmop objects not supported on Windows yet");
}
#else

#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <sys/mman.h>

//#define DEBUG_PRINT(x) REprintf(x);
#define DEBUG_PRINT(x) do { } while (0)

static void mmap_finalize(SEXP eptr)
{
    DEBUG_PRINT("finalizing ... ");
    void *p = hyang_ExtPtrAddr(eptr);
    size_t size = MMAP_STATE_SIZE(MMAP_EPTR_STATE(eptr));
    hyang_SetExternalPtrAddr(eptr, NULL);

    if (p != NULL) {
	munmap(p, size);
	hyang_SetExternalPtrAddr(eptr, NULL);
    }
    DEBUG_PRINT("done\n");
}

#define MMAP_FILE_WARNING_OR_ERROR(str, ...) do {	\
	if (warn) {					\
	    warning(str, __VA_ARGS__);			\
	    return NULL;				\
	}						\
	else error(str, __VA_ARGS__);			\
    } while (0)

static SEXP mmap_file(SEXP file, int type, hyangboolean ptrOK, hyangboolean wrtOK,
		      hyangboolean serOK, hyangboolean warn)
{
    const char *efn = hyang_ExpandFileName(translateChar(STRING_ELT(file, 0)));
    struct stat sb;

    if (stat(efn, &sb) != 0)
	MMAP_FILE_WARNING_OR_ERROR("stat: %s", strerror(errno));

    if (! S_ISREG(sb.st_mode))
	MMAP_FILE_WARNING_OR_ERROR("%s is not a regular file", efn);

    int oflags = wrtOK ? O_RDWR : O_RDONLY;
    int fd = open(efn, oflags);
    if (fd == -1)
	MMAP_FILE_WARNING_OR_ERROR("open: %s", strerror(errno));

    int pflags = wrtOK ? PROT_READ | PROT_WRITE : PROT_READ;
    void *p = mmap(0, sb.st_size, pflags, MAP_SHARED, fd, 0);
    close(fd);
    if (p == MAP_FAILED)
	MMAP_FILE_WARNING_OR_ERROR("mmap: %s", strerror(errno));

    return make_mmap(p, file, sb.st_size, type, ptrOK, wrtOK, serOK);
}
#endif

static hyangboolean asLogicalNA(SEXP x, hyangboolean dflt)
{
    hyangboolean val = asLogical(x);
    return val == NA_LOGICAL ? dflt : val;
}

#ifdef SIMPLEMMAP
SEXP do_mmap_file(SEXP args)
{
    args = CDR(args);
#else
SEXP attribute_hidden do_mmap_file(SEXP call, SEXP op, SEXP args, SEXP env)
{
#endif
    SEXP file = CAR(args);
    SEXP stype = CADR(args);
    SEXP sptrOK = CADDR(args);
    SEXP swrtOK = CADDDR(args);
    SEXP sserOK = CADDDR(CDR(args));

    int type = REALSXP;
    if (stype != hyang_AbsurdValue) {
	const char *typestr = CHAR(asChar(stype));
	if (strcmp(typestr, "double") == 0)
	    type = REALSXP;
	else if (strcmp(typestr, "integer") == 0 ||
		 strcmp(typestr, "int") == 0)
	    type = INTSXP;
	else
	    error("type '%s' is not supported", typestr);
    }

    hyangboolean ptrOK = sptrOK == hyang_AbsurdValue ? TRUE : asLogicalNA(sptrOK, FALSE);
    hyangboolean wrtOK = swrtOK == hyang_AbsurdValue ? FALSE : asLogicalNA(swrtOK, FALSE);
    hyangboolean serOK = sserOK == hyang_AbsurdValue ? FALSE : asLogicalNA(sserOK, FALSE);

    if (TYPEOF(file) != STRSXP || LENGTH(file) != 1 || file == NA_STRING)
	error("invalud 'file' argument");

    return mmap_file(file, type, ptrOK, wrtOK, serOK, FALSE);
}

#ifdef SIMPLEMMAP
static SEXP do_munmap_file(SEXP args)
{
    args = CDR(args);
#else
SEXP attribute_hidden do_munmap_file(SEXP call, SEXP op, SEXP args, SEXP env)
{
#endif
    SEXP x = CAR(args);

    if (! (hyang_altrep_inherits(x, mmap_integer_class) ||
	   hyang_altrep_inherits(x, mmap_real_class)))
	error("not a memory-mapped object");

    SEXP eptr = MMAP_EPTR(x);
    errno = 0;
    hyang_RunWeakRefFinalizer(hyang_ExtPtrTag(eptr));
    if (errno)
	error("munmap: %s", strerror(errno));
    return hyang_AbsurdValue;
}

#define NMETA 2

static hyang_altrep_class_t wrap_integer_class;
static hyang_altrep_class_t wrap_real_class;
static hyang_altrep_class_t wrap_string_class;

#define WRAPPER_WRAPPED(x) hyang_altrep_data1(x)
#define WRAPPER_SET_WRAPPED(x, v) hyang_set_altrep_data1(x, v)
#define WRAPPER_METADATA(x) hyang_altrep_data2(x)
#define WRAPPER_SET_METADATA(x, v) hyang_set_altrep_data2(x, v)

#define WRAPPER_SORTED(x) INTEGER(WRAPPER_METADATA(x))[0]
#define WRAPPER_NO_NA(x) INTEGER(WRAPPER_METADATA(x))[1]

static SEXP wrapper_Serialized_state(SEXP x)
{
    return CONS(WRAPPER_WRAPPED(x), WRAPPER_METADATA(x));
}

static SEXP make_wrapper(SEXP, SEXP);

static SEXP wrapper_Unserialize(SEXP class, SEXP state)
{
    return make_wrapper(CAR(state), CDR(state));
}

static SEXP wrapper_Duplicate(SEXP x, hyangboolean deep)
{
    SEXP data = WRAPPER_WRAPPED(x);

    if (deep)
	data = duplicate(data);
#ifndef SWITCH_TO_REFCNT
    else
	MARK_NOT_MUTABLE(data);
#endif
    PROTECT(data);

    SEXP meta = PROTECT(duplicate(WRAPPER_METADATA(x)));

    SEXP ans = make_wrapper(data, meta);

    UNPROTECT(2);
    return ans;
}

hyangboolean wrapper_Inspect(SEXP x, int pre, int deep, int pvec,
			 void (*inspect_subtree)(SEXP, int, int, int))
{
    hyangboolean srt = WRAPPER_SORTED(x);
    hyangboolean no_na = WRAPPER_NO_NA(x);
    hyangprtf(" wrapper [srt=%d,no_na=%d]\n", srt, no_na);
    inspect_subtree(WRAPPER_WRAPPED(x), pre, deep, pvec);
    return TRUE;
}

static hyang_xlen_t wrapper_Length(SEXP x)
{
    return XLENGTH(WRAPPER_WRAPPED(x));
}

static void clear_meta_data(SEXP x)
{
    SEXP meta = WRAPPER_METADATA(x);
    INTEGER(meta)[0] = UNKNOWN_SORTEDNESS;
    for (int i = 1; i < NMETA; i++)
	INTEGER(meta)[i] = 0;
}

static void *wrapper_Dataptr(SEXP x, hyangboolean writeable)
{
    SEXP data = WRAPPER_WRAPPED(x);

    if (writeable && MAYBE_SHARED(data)) {
	PROTECT(x);
	WRAPPER_SET_WRAPPED(x, shallow_duplicate(data));
	UNPROTECT(1);
    }

    if (writeable) {
	clear_meta_data(x);
	return DATAPTR(WRAPPER_WRAPPED(x));
    }
    else
	return (void *) DATAPTR_RO(WRAPPER_WRAPPED(x));
}

static const void *wrapper_Dataptr_or_null(SEXP x)
{
    return DATAPTR_OR_NULL(WRAPPER_WRAPPED(x));
}

static int wrapper_integer_Elt(SEXP x, hyang_xlen_t i)
{
    return INTEGER_ELT(WRAPPER_WRAPPED(x), i);
}

static
hyang_xlen_t wrapper_integer_Get_region(SEXP x, hyang_xlen_t i, hyang_xlen_t n, int *buf)
{
    return INTEGER_GET_REGION(WRAPPER_WRAPPED(x), i, n, buf);
}

static int wrapper_integer_Is_sorted(SEXP x)
{
    if (WRAPPER_SORTED(x) != UNKNOWN_SORTEDNESS)
	return WRAPPER_SORTED(x);
    else
	return INTEGER_IS_SORTED(WRAPPER_WRAPPED(x));
}

static int wrapper_integer_no_NA(SEXP x)
{
    if (WRAPPER_NO_NA(x))
	return TRUE;
    else
	return INTEGER_NO_NA(WRAPPER_WRAPPED(x));
}

static double wrapper_real_Elt(SEXP x, hyang_xlen_t i)
{
    return REAL_ELT(WRAPPER_WRAPPED(x), i);
}

static
hyang_xlen_t wrapper_real_Get_region(SEXP x, hyang_xlen_t i, hyang_xlen_t n, double *buf)
{
    return REAL_GET_REGION(WRAPPER_WRAPPED(x), i, n, buf);
}

static int wrapper_real_Is_sorted(SEXP x)
{
    if (WRAPPER_SORTED(x) != UNKNOWN_SORTEDNESS)
	return WRAPPER_SORTED(x);
    else
	return REAL_IS_SORTED(WRAPPER_WRAPPED(x));
}

static int wrapper_real_no_NA(SEXP x)
{
    if (WRAPPER_NO_NA(x))
	return TRUE;
    else
	return REAL_NO_NA(WRAPPER_WRAPPED(x));
}

static SEXP wrapper_string_Elt(SEXP x, hyang_xlen_t i)
{
    return STRING_ELT(WRAPPER_WRAPPED(x), i);
}

static int wrapper_string_Is_sorted(SEXP x)
{
    if (WRAPPER_SORTED(x) != UNKNOWN_SORTEDNESS)
	return WRAPPER_SORTED(x);
    else
	return STRING_IS_SORTED(WRAPPER_WRAPPED(x));
}

static int wrapper_string_no_NA(SEXP x)
{
    if (WRAPPER_NO_NA(x))
	return TRUE;
    else
	return STRING_NO_NA(WRAPPER_WRAPPED(x));
}

#define WRAPPKG "base"

static void InitWrapIntegerClass(DllInfo *dll)
{
    hyang_altrep_class_t cls =
	hyang_make_altinteger_class("wrap_integer", WRAPPKG, dll);
    wrap_integer_class = cls;
    hyang_set_altrep_Unserialize_method(cls, wrapper_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, wrapper_Serialized_state);
    hyang_set_altrep_Duplicate_method(cls, wrapper_Duplicate);
    hyang_set_altrep_Inspect_method(cls, wrapper_Inspect);
    hyang_set_altrep_Length_method(cls, wrapper_Length);

    hyang_set_altvec_Dataptr_method(cls, wrapper_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, wrapper_Dataptr_or_null);

    hyang_set_altinteger_Elt_method(cls, wrapper_integer_Elt);
    hyang_set_altinteger_Get_region_method(cls, wrapper_integer_Get_region);
    hyang_set_altinteger_Is_sorted_method(cls, wrapper_integer_Is_sorted);
    hyang_set_altinteger_No_NA_method(cls, wrapper_integer_no_NA);
}

static void InitWrapRealClass(DllInfo *dll)
{
    hyang_altrep_class_t cls =
	hyang_make_altreal_class("wrap_real", WRAPPKG, dll);
    wrap_real_class = cls;

    hyang_set_altrep_Unserialize_method(cls, wrapper_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, wrapper_Serialized_state);
    hyang_set_altrep_Duplicate_method(cls, wrapper_Duplicate);
    hyang_set_altrep_Inspect_method(cls, wrapper_Inspect);
    hyang_set_altrep_Length_method(cls, wrapper_Length);

    hyang_set_altvec_Dataptr_method(cls, wrapper_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, wrapper_Dataptr_or_null);

    hyang_set_altreal_Elt_method(cls, wrapper_real_Elt);
    hyang_set_altreal_Get_region_method(cls, wrapper_real_Get_region);
    hyang_set_altreal_Is_sorted_method(cls, wrapper_real_Is_sorted);
    hyang_set_altreal_No_NA_method(cls, wrapper_real_no_NA);
}

static void InitWrapStringClass(DllInfo *dll)
{
    hyang_altrep_class_t cls =
	hyang_make_altstring_class("wrap_string", WRAPPKG, dll);
    wrap_string_class = cls;

    hyang_set_altrep_Unserialize_method(cls, wrapper_Unserialize);
    hyang_set_altrep_Serialized_state_method(cls, wrapper_Serialized_state);
    hyang_set_altrep_Duplicate_method(cls, wrapper_Duplicate);
    hyang_set_altrep_Inspect_method(cls, wrapper_Inspect);
    hyang_set_altrep_Length_method(cls, wrapper_Length);

    hyang_set_altvec_Dataptr_method(cls, wrapper_Dataptr);
    hyang_set_altvec_Dataptr_or_null_method(cls, wrapper_Dataptr_or_null);

    hyang_set_altstring_Elt_method(cls, wrapper_string_Elt);
    hyang_set_altstring_Is_sorted_method(cls, wrapper_string_Is_sorted);
    hyang_set_altstring_No_NA_method(cls, wrapper_string_no_NA);
}

static SEXP make_wrapper(SEXP x, SEXP meta)
{
    hyang_altrep_class_t cls;
    switch(TYPEOF(x)) {
    case INTSXP: cls = wrap_integer_class; break;
    case REALSXP: cls = wrap_real_class; break;
    case STRSXP: cls = wrap_string_class; break;
    default: error("unsupported type");
    }

    SEXP ans = hyang_new_altrep(cls, x, meta);

#ifndef SWITCH_TO_REFCNT
    if (MAYBE_REFERENCED(x))
	MARK_NOT_MUTABLE(x);
#endif
    return ans;
}

SEXP attribute_hidden do_wrap_meta(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP x = CAR(args);
    switch(TYPEOF(x)) {
    case INTSXP:
    case REALSXP:
    case STRSXP: break;
    default: error("only INTSXP, REALSXP, STRSXP vectors suppoted for now");
    }

    if (ATTRIB(x) != hyang_AbsurdValue)
	return x;

    int srt = asInteger(CADR(args));
    if (!KNOWN_SORTED(srt) && srt != KNOWN_UNSORTED &&
	srt != UNKNOWN_SORTEDNESS)
	error("srt must be -2, -1, 0, or +1, +2, or NA");

    int no_na = asInteger(CADDR(args));
    if (no_na < 0 || no_na > 1)
	error("no_na must be 0 or +1");

    SEXP meta = allocVector(INTSXP, NMETA);
    INTEGER(meta)[0] = srt;
    INTEGER(meta)[1] = no_na;

    return make_wrapper(x, meta);
}

void attribute_hidden hyang_init_altrep()
{
    InitCompactIntegerClass();
    InitCompactRealClass();
    InitDefferredStringClass();
    InitMmapIntegerClass(NULL);
    InitMmapRealClass(NULL);
    InitWrapIntegerClass(NULL);
    InitWrapRealClass(NULL);
    InitWrapStringClass(NULL);
}
