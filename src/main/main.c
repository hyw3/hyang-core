/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <math.h>
#include <float.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define __MAIN__
#define HYANG_USE_SIGNALS 1
#include "hyangdefn.h"
#include <hyangintl.h>
#include "hyangintfc.h"
#include "hyangio.h"
#include "hyangfio.h"
#include "hyangparse.h"
#include "hyangstu.h"

#include <locale.h>
#include <hyangexts/hyangextprint.h>

#ifdef ENABLE_NLS
void attribute_hidden nl_hyangdummy(void)
{
    dgettext("hyang", "dummy - do not translate");
}
#endif

void hyangly_callToplevelHandlers(SEXP expr, SEXP value, hyangboolean succeeded,
			     hyangboolean visible);

static int ParseBrowser(SEXP, SEXP);


static void hyang_ReplFile(FILE *fp, SEXP rho)
{
    ParseStatus status;
    int count=0;
    int savestack;

    hyang_InitSrcRefState();
    savestack = hyang_PPStackTop;
    for(;;) {
	hyang_PPStackTop = savestack;
	hyang_CurrentExpr = hyang_Parse1File(fp, 1, &status);
	switch (status) {
	case PARSE_NULL:
	    break;
	case PARSE_OK:
	    hyang_Visible = FALSE;
	    hyang_EvalDepth = 0;
	    resetTimeLimits();
	    count++;
	    PROTECT(hyang_CurrentExpr);
	    hyang_CurrentExpr = eval(hyang_CurrentExpr, rho);
	    SET_SYMVALUE(hyang_LastvalueSym, hyang_CurrentExpr);
	    UNPROTECT(1);
	    if (hyang_Visible)
		PrintValueEnv(hyang_CurrentExpr, rho);
	    if( hyang_CollectWarnings )
		PrintWarnings();
	    break;
	case PARSE_ERROR:
	    hyang_FinSrcRefState();
	    parseError(hyang_AbsurdValue, hyang_errParse);
	    break;
	case PARSE_EOF:
	    hyang_FinSrcRefState();
	    return;
	    break;
	case PARSE_INCOMPLETE:
	    break;
	}
    }
}

static int prompt_type;
static char BrowsePrompt[20];
static const char *hyang_PromptStr(int browselevel, int type)
{
    if (hyang_Slave) {
	BrowsePrompt[0] = '\0';
	return BrowsePrompt;
    }
    else {
	if(type == 1) {
	    if(browselevel) {
		snprintf(BrowsePrompt, 20, "Browse[%d]> ", browselevel);
		return BrowsePrompt;
	    }
	    return CHAR(STRING_ELT(GetOption1(install("prompt")), 0));
	}
	else {
	    return CHAR(STRING_ELT(GetOption1(install("continue")), 0));
	}
    }
}

typedef struct {
  ParseStatus    status;
  int            prompt_type;
  int            browselevel;
  unsigned char  buf[CONSOLE_BUFFER_SIZE+1];
  unsigned char *bufp;
} hyang_ReplState;

int
hyangly_ReplIteration(SEXP rho, int savestack, int browselevel, hyang_ReplState *state)
{
    int c, browsevalue;
    SEXP value, thisExpr;
    hyangboolean wasDisplayed = FALSE;

    if(!*state->bufp) {
	    hyang_Busy(0);
	    if (hyang_ReadConsole(hyang_PromptStr(browselevel, state->prompt_type),
			      state->buf, CONSOLE_BUFFER_SIZE, 1) == 0)
		return(-1);
	    state->bufp = state->buf;
    }
#ifdef SHELL_ESCAPE
    if (*state->bufp == '!') {
	    hyang_system(&(state->buf[1]));
	    state->buf[0] = '\0';
	    return(0);
    }
#endif
    while((c = *state->bufp++)) {
	    hyang_IoBuffPutC(c, &hyang_ConsoleIob);
	    if(c == ';' || c == '\n') break;
    }

    hyang_PPStackTop = savestack;
    hyang_CurrentExpr = hyang_Parse1Buff(&hyang_ConsoleIob, 0, &state->status);

    switch(state->status) {

    case PARSE_NULL:

	if (browselevel && !hyang_DisableNLinBrowser
	    && !strcmp((char *) state->buf, "\n")) return -1;
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	state->prompt_type = 1;
	return 1;

    case PARSE_OK:

	hyang_IoBuffReadReset(&hyang_ConsoleIob);
	hyang_CurrentExpr = hyang_Parse1Buff(&hyang_ConsoleIob, 1, &state->status);
	if (browselevel) {
	    browsevalue = ParseBrowser(hyang_CurrentExpr, rho);
	    if(browsevalue == 1) return -1;
	    if(browsevalue == 2) {
		hyang_IoBuffWriteReset(&hyang_ConsoleIob);
		return 0;
	    }
	    if (hyang_BrowserLastCmd == 's') hyang_BrowserLastCmd = 'S';
	}
	hyang_Visible = FALSE;
	hyang_EvalDepth = 0;
	resetTimeLimits();
	PROTECT(thisExpr = hyang_CurrentExpr);
	hyang_Busy(1);
	PROTECT(value = eval(thisExpr, rho));
	SET_SYMVALUE(hyang_LastvalueSym, value);
	wasDisplayed = hyang_Visible;
	if (hyang_Visible)
	    PrintValueEnv(value, rho);
	if (hyang_CollectWarnings)
	    PrintWarnings();
	hyangly_callToplevelHandlers(thisExpr, value, TRUE, wasDisplayed);
	hyang_CurrentExpr = value;
	UNPROTECT(2);
	if (hyang_BrowserLastCmd == 'S') hyang_BrowserLastCmd = 's';
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	state->prompt_type = 1;
	return(1);

    case PARSE_ERROR:

	state->prompt_type = 1;
	parseError(hyang_AbsurdValue, 0);
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	return(1);

    case PARSE_INCOMPLETE:

	hyang_IoBuffReadReset(&hyang_ConsoleIob);
	state->prompt_type = 2;
	return(2);

    case PARSE_EOF:

	return(-1);
	break;
    }

    return(0);
}

static void hyang_ReplConsole(SEXP rho, int savestack, int browselevel)
{
    int status;
    hyang_ReplState state = { PARSE_NULL, 1, 0, "", NULL};

    hyang_IoBuffWriteReset(&hyang_ConsoleIob);
    state.buf[0] = '\0';
    state.buf[CONSOLE_BUFFER_SIZE] = '\0';
    state.bufp = state.buf;
    if(hyang_Verbose)
	REprintf(" >hyang_ReplConsole(): before \"for(;;)\" {main.c}\n");
    for(;;) {
	status = hyangly_ReplIteration(rho, savestack, browselevel, &state);
	if(status < 0) {
	  if (state.status == PARSE_INCOMPLETE)
	    error(_("unexpected end of input"));
	  return;
	}
    }
}


static unsigned char DLLbuf[CONSOLE_BUFFER_SIZE+1], *DLLbufp;

static void check_session_exit()
{
    if (! hyang_Interactive) {
	static hyangboolean exiting = FALSE;
	if (exiting)
	    hyang_Suicide(_("error during cleanup\n"));
	else {
	    exiting = TRUE;
	    if (GetOption1(install("error")) != hyang_AbsurdValue) {
		exiting = FALSE;
		return;
	    }
	    REprintf(_("Execution halted\n"));
	    hyang_CleanUp(SA_NOSAVE, 1, 0);
	}
    }
}

void hyang_ReplDLLinit(void)
{
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    hyang_IoBuffWriteReset(&hyang_ConsoleIob);
    prompt_type = 1;
    DLLbuf[0] = DLLbuf[CONSOLE_BUFFER_SIZE] = '\0';
    DLLbufp = DLLbuf;
}

int hyang_ReplDLLdo1(void)
{
    int c;
    ParseStatus status;
    SEXP rho = hyang_GlobalEnv, lastExpr;
    hyangboolean wasDisplayed = FALSE;

    if(!*DLLbufp) {
	hyang_Busy(0);
	if (hyang_ReadConsole(hyang_PromptStr(0, prompt_type), DLLbuf,
			  CONSOLE_BUFFER_SIZE, 1) == 0)
	    return -1;
	DLLbufp = DLLbuf;
    }
    while((c = *DLLbufp++)) {
	hyang_IoBuffPutC(c, &hyang_ConsoleIob);
	if(c == ';' || c == '\n') break;
    }
    hyang_PPStackTop = 0;
    hyang_CurrentExpr = hyang_Parse1Buff(&hyang_ConsoleIob, 0, &status);

    switch(status) {
    case PARSE_NULL:
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	prompt_type = 1;
	break;
    case PARSE_OK:
	hyang_IoBuffReadReset(&hyang_ConsoleIob);
	hyang_CurrentExpr = hyang_Parse1Buff(&hyang_ConsoleIob, 1, &status);
	hyang_Visible = FALSE;
	hyang_EvalDepth = 0;
	resetTimeLimits();
	PROTECT(hyang_CurrentExpr);
	hyang_Busy(1);
	lastExpr = hyang_CurrentExpr;
	hyang_CurrentExpr = eval(hyang_CurrentExpr, rho);
	SET_SYMVALUE(hyang_LastvalueSym, hyang_CurrentExpr);
	wasDisplayed = hyang_Visible;
	if (hyang_Visible)
	    PrintValueEnv(hyang_CurrentExpr, rho);
	if (hyang_CollectWarnings)
	    PrintWarnings();
	hyangly_callToplevelHandlers(lastExpr, hyang_CurrentExpr, TRUE, wasDisplayed);
	UNPROTECT(1);
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	hyang_Busy(0);
	prompt_type = 1;
	break;
    case PARSE_ERROR:
	parseError(hyang_AbsurdValue, 0);
	hyang_IoBuffWriteReset(&hyang_ConsoleIob);
	prompt_type = 1;
	break;
    case PARSE_INCOMPLETE:
	hyang_IoBuffReadReset(&hyang_ConsoleIob);
	prompt_type = 2;
	break;
    case PARSE_EOF:
	return -1;
	break;
    }
    return prompt_type;
}

static RETSIGTYPE handleInterrupt(int dummy)
{
    hyang_interrupts_pending = 1;
    signal(SIGINT, handleInterrupt);
}

#ifndef Win32
int hyang_ignore_SIGPIPE = 0;

static RETSIGTYPE handlePipe(int dummy)
{
    signal(SIGPIPE, handlePipe);
    if (!hyang_ignore_SIGPIPE) error("ignoring SIGPIPE signal");
}
#endif


#ifdef Win32
static int num_caught = 0;

static void win32_segv(int signum)
{
    {
	SEXP trace, p, q;
	int line = 1, i;
	PROTECT(trace = hyang_GetTraceback(0));
	if(trace != hyang_AbsurdValue) {
	    REprintf("\nTraceback:\n");
	    for(p = trace; p != hyang_AbsurdValue; p = CDR(p), line++) {
		q = CAR(p);
		REprintf("%2d: ", line);
		for(i = 0; i < LENGTH(q); i++)
		    REprintf("%s", CHAR(STRING_ELT(q, i)));
		REprintf("\n");
	    }
	    UNPROTECT(1);
	}
    }
    num_caught++;
    if(num_caught < 10) signal(signum, win32_segv);
    if(signum == SIGILL)
	error("caught access violation - continue with care");
    else
	error("caught access violation - continue with care");
}
#endif

#if defined(HAVE_SIGALTSTACK) && defined(HAVE_SIGACTION) && defined(HAVE_WORKING_SIGACTION) && defined(HAVE_SIGEMPTYSET)

static unsigned char ConsoleBuf[CONSOLE_BUFFER_SIZE];

static void sigactionSegv(int signum, siginfo_t *ip, void *context)
{
    char *s;

    if(signum == SIGSEGV && (ip != (siginfo_t *)0) &&
       (intptr_t) hyang_CStackStart != -1) {
	uintptr_t addr = (uintptr_t) ip->si_addr;
	intptr_t diff = (hyang_CStackDir > 0) ? hyang_CStackStart - addr:
	    addr - hyang_CStackStart;
	uintptr_t upper = 0x1000000;  /* 16Mb */
	if((intptr_t) hyang_CStackLim != -1) upper += hyang_CStackLim;
	if(diff > 0 && diff < upper) {
	    REprintf(_("Error: segfault from C stack overflow\n"));
	    jump_to_toplevel();
	}
    }

    hyang_CStackLim = (uintptr_t)-1;

    REprintf("\n *** caught %s ***\n",
	     signum == SIGILL ? "illegal operation" :
	     signum == SIGBUS ? "bus error" : "segfault");
    if(ip != (siginfo_t *)0) {
	if(signum == SIGILL) {

	    switch(ip->si_code) {
#ifdef ILL_ILLOPC
	    case ILL_ILLOPC:
		s = "illegal opcode";
		break;
#endif
#ifdef ILL_ILLOPN
	    case ILL_ILLOPN:
		s = "illegal operand";
		break;
#endif
#ifdef ILL_ILLADR
	    case ILL_ILLADR:
		s = "illegal addressing mode";
		break;
#endif
#ifdef ILL_ILLTRP
	    case ILL_ILLTRP:
		s = "illegal trap";
		break;
#endif
#ifdef ILL_COPROC
	    case ILL_COPROC:
		s = "coprocessor error";
		break;
#endif
	    default:
		s = "unknown";
		break;
	    }
	} else if(signum == SIGBUS)
	    switch(ip->si_code) {
#ifdef BUS_ADRALN
	    case BUS_ADRALN:
		s = "invalid alignment";
		break;
#endif
#ifdef BUS_ADRERR
	    case BUS_ADRERR:
		s = "non-existent physical address";
		break;
#endif
#ifdef BUS_OBJERR
	    case BUS_OBJERR:
		s = "object specific hardware error";
		break;
#endif
	    default:
		s = "unknown";
		break;
	    }
	else
	    switch(ip->si_code) {
#ifdef SEGV_MAPERR
	    case SEGV_MAPERR:
		s = "memory not mapped";
		break;
#endif
#ifdef SEGV_ACCERR
	    case SEGV_ACCERR:
		s = "invalid permissions";
		break;
#endif
	    default:
		s = "unknown";
		break;
	    }
	REprintf("address %p, cause '%s'\n", ip->si_addr, s);
    }
    {
	SEXP trace, p, q;
	int line = 1, i;
	PROTECT(trace = hyang_GetTraceback(0));
	if(trace != hyang_AbsurdValue) {
	    REprintf("\nTraceback:\n");
	    for(p = trace; p != hyang_AbsurdValue; p = CDR(p), line++) {
		q = CAR(p); /* a character vector */
		REprintf("%2d: ", line);
		for(i = 0; i < LENGTH(q); i++)
		    REprintf("%s", CHAR(STRING_ELT(q, i)));
		REprintf("\n");
	    }
	    UNPROTECT(1);
	}
    }
    if(hyang_Interactive) {
	REprintf("\nPossible actions:\n1: %s\n2: %s\n3: %s\n4: %s\n",
		 "abort (with core dump, if enabled)",
		 "normal Hyang exit",
		 "exit Hyang without saving workspace",
		 "exit Hyang saving workspace");
	while(1) {
	    if(hyang_ReadConsole("Selection: ", ConsoleBuf, CONSOLE_BUFFER_SIZE,
			     0) > 0) {
		if(ConsoleBuf[0] == '1') break;
		if(ConsoleBuf[0] == '2') hyang_CleanUp(SA_DEFAULT, 0, 1);
		if(ConsoleBuf[0] == '3') hyang_CleanUp(SA_NOSAVE, 70, 0);
		if(ConsoleBuf[0] == '4') hyang_CleanUp(SA_SAVE, 71, 0);
	    }
	}
	REprintf("Hyang is aborting now ...\n");
    }
    else // non-interactively :
	REprintf("An irrecoverable exception occurred. Hyang is aborting now ...\n");
    hyang_CleanTempDir();
    signal(signum, SIG_DFL);
    raise(signum);
}

#ifndef SIGSTKSZ
# define SIGSTKSZ 8192
#endif

#ifdef HAVE_STACK_T
static stack_t sigstk;
#else
static struct sigaltstack sigstk;
#endif
static void *signal_stack;

#define HYANG_USAGE 100000
static void init_signal_handlers(void)
{
    struct sigaction sa;
    signal_stack = malloc(SIGSTKSZ + HYANG_USAGE);
    if (signal_stack != NULL) {
	sigstk.ss_sp = signal_stack;
	sigstk.ss_size = SIGSTKSZ + HYANG_USAGE;
	sigstk.ss_flags = 0;
	if(sigaltstack(&sigstk, NULL) < 0)
	    warning("failed to set alternate signal stack");
    } else
	warning("failed to allocate alternate signal stack");
    sa.sa_sigaction = sigactionSegv;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_ONSTACK | SA_SIGINFO;
    sigaction(SIGSEGV, &sa, NULL);
    sigaction(SIGILL, &sa, NULL);
#ifdef SIGBUS
    sigaction(SIGBUS, &sa, NULL);
#endif

    signal(SIGINT,  handleInterrupt);
    signal(SIGUSR1, onsigusr1);
    signal(SIGUSR2, onsigusr2);
    signal(SIGPIPE, handlePipe);
}

#else
static void init_signal_handlers(void)
{
    signal(SIGINT,  handleInterrupt);
    signal(SIGUSR1, onsigusr1);
    signal(SIGUSR2, onsigusr2);
#ifndef Win32
    signal(SIGPIPE, handlePipe);
#else
    signal(SIGSEGV, win32_segv);
    signal(SIGILL, win32_segv);
#endif
}
#endif


static void hyang_LoadProfile(FILE *fparg, SEXP env)
{
    FILE * volatile fp = fparg; /* is this needed? */
    if (fp != NULL) {
	if (SETJMP(hyang_TopLevel.cjmpbuf))
	    check_session_exit();
	else {
	    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
	    hyang_ReplFile(fp, env);
	}
	fclose(fp);
    }
}


int hyang_SignalHandlers = 1;  /* Exposed in hyangintfc.h */

const char* get_workspace_name();  /* from startup.c */

void attribute_hidden BindDomain(char *hyang_Home)
{
#ifdef ENABLE_NLS
    char localedir[PATH_MAX+20];
    setlocale(LC_MESSAGES,"");
    textdomain(PACKAGE);
    char *p = getenv("HYANG_TRANSLT");
    if (p) snprintf(localedir, PATH_MAX+20, "%s", p);
    else snprintf(localedir, PATH_MAX+20, "%s/library/translations", hyang_Home);
    bindtextdomain(PACKAGE, localedir); // PACKAGE = DOMAIN = "hyang"
    bindtextdomain("hyang-base", localedir);
# ifdef _WIN32
    bindtextdomain("hyangGui", localedir);
# endif
#endif
}

void hyang_MainloopSetup(void)
{
    volatile int doneit;
    volatile SEXP baseEnv;
    SEXP cmd;
    char deferred_warnings[11][250];
    volatile int ndeferred_warnings = 0;

#if 0
    printf("stack limit %ld, start %lx dir %d \n",
	(unsigned long) hyang_CStackLim,
        (unsigned long) hyang_CStackStart,
	hyang_CStackDir);
    uintptr_t firstb = hyang_CStackStart - hyang_CStackDir;
    printf("first accessible byte %lx\n", (unsigned long) firstb);
    if (hyang_CStackLim != (uintptr_t)(-1)) {
        uintptr_t lastb = hyang_CStackStart - hyang_CStackDir * hyang_CStackLim;
	printf("last accessible byte %lx\n", (unsigned long) lastb);
    }
    printf("accessing first byte...\n");
    volatile char dummy = *(char *)firstb;
    if (hyang_CStackLim != (uintptr_t)(-1)) {
	printf("accessing all bytes...\n");
	for(uintptr_t o = 0; o < hyang_CStackLim; o++)
	    dummy = *((char *)firstb - hyang_CStackDir * o);
    }
#endif

    if(hyang_CStackLim > 100000000U)
	hyang_CStackLim = (uintptr_t)-1;
    if(hyang_CStackLim != -1)
	hyang_CStackLim = (uintptr_t)(0.95 * hyang_CStackLim);

    InitConnections(); /* needed to get any output at all */

#ifdef HAVE_LOCALE_H
#ifdef Win32
    {
	char *p, Rlocale[1000]; /* Windows' locales can be very long */
	p = getenv("LC_ALL");
	strncpy(Rlocale, p ? p : "", 1000);
	Rlocale[1000 - 1] = '\0';
	if(!(p = getenv("LC_CTYPE"))) p = Rlocale;
	if(!setlocale(LC_CTYPE, p))
	    snprintf(deferred_warnings[ndeferred_warnings++], 250,
		     "Setting LC_CTYPE=%s failed\n", p);
	if((p = getenv("LC_COLLATE"))) {
	    if(!setlocale(LC_COLLATE, p))
		snprintf(deferred_warnings[ndeferred_warnings++], 250,
			 "Setting LC_COLLATE=%s failed\n", p);
	} else setlocale(LC_COLLATE, Rlocale);
	if((p = getenv("LC_TIME"))) {
	    if(!setlocale(LC_TIME, p))
		snprintf(deferred_warnings[ndeferred_warnings++], 250,
			 "Setting LC_TIME=%s failed\n", p);
	} else setlocale(LC_TIME, Rlocale);
	if((p = getenv("LC_MONETARY"))) {
	    if(!setlocale(LC_MONETARY, p))
		snprintf(deferred_warnings[ndeferred_warnings++], 250,
			 "Setting LC_MONETARY=%s failed\n", p);
	} else setlocale(LC_MONETARY, Rlocale);

	char Rarch[30];
	strcpy(Rarch, "HYANG_ARCH=/");
	strcat(Rarch, HYANG_ARCH);
	putenv(Rarch);
    }
#else /* not Win32 */
    if(!setlocale(LC_CTYPE, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_CTYPE failed, using \"C\"\n");
    if(!setlocale(LC_COLLATE, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_COLLATE failed, using \"C\"\n");
    if(!setlocale(LC_TIME, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_TIME failed, using \"C\"\n");
#ifdef ENABLE_NLS
    if(!setlocale(LC_MESSAGES, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_MESSAGES failed, using \"C\"\n");
#endif
#ifdef LC_MONETARY
    if(!setlocale(LC_MONETARY, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_MONETARY failed, using \"C\"\n");
#endif
#ifdef LC_PAPER
    if(!setlocale(LC_PAPER, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_PAPER failed, using \"C\"\n");
#endif
#ifdef LC_MEASUREMENT
    if(!setlocale(LC_MEASUREMENT, ""))
	snprintf(deferred_warnings[ndeferred_warnings++], 250,
		 "Setting LC_MEASUREMENT failed, using \"C\"\n");
#endif
#endif /* not Win32 */
#endif

    srand(TimeToSeed());
    InitArithmetic();
    InitParser();
    InitTempDir();
    InitMemory();
    InitStringHash();
    InitBaseEnv();
    InitNames(); /* must be after InitBaseEnv to use hyang_EmptyEnv */
    InitGlobalEnv();
    InitDyl();
    InitOptions();
    InitEd();
    InitGraphics();
    InitTypeTables();
    InitS3DefaultTypes();
    PrintDefaults();

    hyang_Is_Running = 1;
    hyang_check_locale();

    hyang_TopLevel.nextcontext = NULL;
    hyang_TopLevel.callflag = CTXT_TOPLEVEL;
    hyang_TopLevel.cstacktop = 0;
    hyang_TopLevel.gcenabled = hyang_PBCEnabled;
    hyang_TopLevel.promargs = hyang_AbsurdValue;
    hyang_TopLevel.callfun = hyang_AbsurdValue;
    hyang_TopLevel.call = hyang_AbsurdValue;
    hyang_TopLevel.cloenv = hyang_BaseEnv;
    hyang_TopLevel.sysparent = hyang_BaseEnv;
    hyang_TopLevel.conexit = hyang_AbsurdValue;
    hyang_TopLevel.vmax = NULL;
    hyang_TopLevel.nodestack = Abah_NodeStackTop;
#ifdef BC_INT_STACK
    hyang_TopLevel.intstack = Abah_IntStackTop;
#endif
    hyang_TopLevel.cend = NULL;
    hyang_TopLevel.cenddata = NULL;
    hyang_TopLevel.intsusp = FALSE;
    hyang_TopLevel.handlerstack = hyang_HandlerStack;
    hyang_TopLevel.restartstack = hyang_RestartStack;
    hyang_TopLevel.srcref = hyang_AbsurdValue;
    hyang_TopLevel.prstack = NULL;
    hyang_TopLevel.returnValue = NULL;
    hyang_TopLevel.evaldepth = 0;
    hyang_TopLevel.browserfinish = 0;
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    hyang_ExitCtx = NULL;

    hyang_Warnings = hyang_AbsurdValue;

    baseEnv = hyang_BaseNamespace;

    Init_hyang_Vars(baseEnv);

#ifdef RMIN_ONLY
    if (hyang_SignalHandlers) init_signal_handlers();
#else
    FILE *fp = hyang_OpenLibFile("base");
    if (fp == NULL)
	hyang_Suicide(_("unable to open the base package\n"));

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (hyang_SignalHandlers) init_signal_handlers();
    if (!doneit) {
	doneit = 1;
	hyang_ReplFile(fp, baseEnv);
    }
    fclose(fp);
#endif

    hyang_IoBuffInit(&hyang_ConsoleIob);
    hyang_LoadProfile(hyang_OpenSysInitFile(), baseEnv);
    hyang_LockEnvironment(hyang_BaseNamespace, TRUE);
#ifdef NOTYET
    hyang_LockEnvironment(hyang_BaseEnv, TRUE);
#endif
    hyang_unLockBinding(hyang_DeviceSym, hyang_BaseEnv);
    hyang_unLockBinding(hyang_DevicesSym, hyang_BaseEnv);
    hyang_unLockBinding(install(".Library.site"), hyang_BaseEnv);

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (!doneit) {
	doneit = 1;
	PROTECT(cmd = install(".OptRequireMethods"));
	hyang_CurrentExpr = findVar(cmd, hyang_GlobalEnv);
	if (hyang_CurrentExpr != hyang_UnboundValue &&
	    TYPEOF(hyang_CurrentExpr) == CLOSXP) {
		PROTECT(hyang_CurrentExpr = lang1(cmd));
		hyang_CurrentExpr = eval(hyang_CurrentExpr, hyang_GlobalEnv);
		UNPROTECT(1);
	}
	UNPROTECT(1);
    }

    if (strcmp(hyang_GUIType, "Tk") == 0) {
	char buf[PATH_MAX];

	snprintf(buf, PATH_MAX, "%s/library/tcltk/exec/Tk-frontend.hyang", hyang_Home);
	hyang_LoadProfile(hyang_fopen(buf, "r"), hyang_GlobalEnv);
    }

    if(!hyang_Quiet) PrintGreeting();

    hyang_LoadProfile(hyang_OpenSiteFile(), baseEnv);
    hyang_LockBinding(install(".Library.site"), hyang_BaseEnv);
    hyang_LoadProfile(hyang_OpenInitFile(), hyang_GlobalEnv);

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (!doneit) {
	doneit = 1;
	hyang_InitialData();
    }
    else {
	if (SETJMP(hyang_TopLevel.cjmpbuf))
	    check_session_exit();
	else {
    	    warning(_("unable to restore saved data in %s\n"), get_workspace_name());
	}
    }

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (!doneit) {
	doneit = 1;
	PROTECT(cmd = install(".First"));
	hyang_CurrentExpr = findVar(cmd, hyang_GlobalEnv);
	if (hyang_CurrentExpr != hyang_UnboundValue &&
	    TYPEOF(hyang_CurrentExpr) == CLOSXP) {
		PROTECT(hyang_CurrentExpr = lang1(cmd));
		hyang_CurrentExpr = eval(hyang_CurrentExpr, hyang_GlobalEnv);
		UNPROTECT(1);
	}
	UNPROTECT(1);
    }

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (!doneit) {
	doneit = 1;
	PROTECT(cmd = install(".First.sys"));
	hyang_CurrentExpr = findVar(cmd, baseEnv);
	if (hyang_CurrentExpr != hyang_UnboundValue &&
	    TYPEOF(hyang_CurrentExpr) == CLOSXP) {
		PROTECT(hyang_CurrentExpr = lang1(cmd));
		hyang_CurrentExpr = eval(hyang_CurrentExpr, hyang_GlobalEnv);
		UNPROTECT(1);
	}
	UNPROTECT(1);
    }
    {
	int i;
	for(i = 0 ; i < ndeferred_warnings; i++)
	    warning(deferred_warnings[i]);
    }
    if (hyang_CollectWarnings) {
	REprintf(_("During startup - "));
	PrintWarnings();
    }
    if(hyang_Verbose)
	REprintf(" ending hyang_MainloopSetup(): hyang_Interactive = %d {main.c}\n",
		 hyang_Interactive);

    doneit = 0;
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    if (!doneit) {
	doneit = 1;
	jit_enabled_init();
    } else
	hyang_Suicide(_("unable to initialize the JITabah\n"));
    hyang_Is_Running = 2;
}

extern SA_TYPE SaveAction; /* from src/main/startup.c */

static void end_Rmainloop(void)
{
    if (!hyang_Slave)
	hyangprtf("\n");
    hyang_CleanUp(SA_DEFAULT, 0, 1);
}

void hyang_MainloopRun(void)
{
    if (SETJMP(hyang_TopLevel.cjmpbuf))
	check_session_exit();
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    hyang_ReplConsole(hyang_GlobalEnv, 0, 0);
    end_Rmainloop(); /* must go here */
}

void mainloop(void)
{
    hyang_MainloopSetup();
    hyang_MainloopRun();
}

static void printwhere(void)
{
  HYANGCTX *cptr;
  int lct = 1;

  for (cptr = hyang_GlobalCtx; cptr; cptr = cptr->nextcontext) {
    if ((cptr->callflag & (CTXT_FUNCTION | CTXT_BUILTIN)) &&
	(TYPEOF(cptr->call) == LANGSXP)) {
	hyangprtf("where %d", lct++);
	SEXP sref;
	if (cptr->srcref == hyang_InAbah)
	    sref = Abah_InterpreterSrcRef(cptr);
	else
	    sref = cptr->srcref;
	SrcrefPrompt("", sref);
	PrintValue(cptr->call);
    }
  }
  hyangprtf("\n");
}

static void printBrowserHelp(void)
{
    hyangprtf("n          next\n");
    hyangprtf("s          step into\n");
    hyangprtf("f          finish\n");
    hyangprtf("c or cont  continue\n");
    hyangprtf("Q          quit\n");
    hyangprtf("where      show stack\n");
    hyangprtf("help       show help\n");
    hyangprtf("<expr>     evaluate expression\n");
}

static int ParseBrowser(SEXP CExpr, SEXP rho)
{
    int rval = 0;
    if (isSymbol(CExpr)) {
	const char *expr = CHAR(PRINTNAME(CExpr));
	if (!strcmp(expr, "c") || !strcmp(expr, "cont")) {
	    rval = 1;
	    SET_RDEBUG(rho, 0);
	} else if (!strcmp(expr, "f")) {
	    rval = 1;
	    HYANGCTX *cntxt = hyang_GlobalCtx;
	    while (cntxt != hyang_TopLevelCtx
		      && !(cntxt->callflag & (CTXT_RETURN | CTXT_LOOP))) {
		cntxt = cntxt->nextcontext;
	    }
	    cntxt->browserfinish = 1;
	    SET_RDEBUG(rho, 1);
	    hyang_BrowserLastCmd = 'f';
	} else if (!strcmp(expr, "help")) {
	    rval = 2;
	    printBrowserHelp();
	} else if (!strcmp(expr, "n")) {
	    rval = 1;
	    SET_RDEBUG(rho, 1);
	    hyang_BrowserLastCmd = 'n';
	} else if (!strcmp(expr, "Q")) {

	    SET_RDEBUG(rho, 0);

	    jump_to_toplevel();
	} else if (!strcmp(expr, "s")) {
	    rval = 1;
	    SET_RDEBUG(rho, 1);
	    hyang_BrowserLastCmd = 's';
	} else if (!strcmp(expr, "where")) {
	    rval = 2;
	    printwhere();
	} else if (!strcmp(expr, "r")) {
	    SEXP hooksym = install(".tryResumeInterrupt");
	    if (SYMVALUE(hooksym) != hyang_UnboundValue) {
		SEXP hcall;
		hyang_Busy(1);
		PROTECT(hcall = LCONS(hooksym, hyang_AbsurdValue));
		eval(hcall, hyang_GlobalEnv);
		UNPROTECT(1);
	    }
	}
    }

    return rval;
}

static void PrintCall(SEXP call, SEXP rho)
{
    int old_bl = hyang_BrowseLines,
	blines = asInteger(GetOption1(install("deparse.max.lines")));
    if(blines != NA_INTEGER && blines > 0)
	hyang_BrowseLines = blines;
    PrintValueRec(call, rho);
    hyang_BrowseLines = old_bl;
}

SEXP attribute_hidden do_browser(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    HYANGCTX *saveToplevelContext;
    HYANGCTX *saveGlobalContext;
    HYANGCTX thiscontext, returncontext, *cptr;
    int savestack, browselevel;
    SEXP ap, topExp, argList;

    PROTECT(ap = list4(hyang_AbsurdValue, hyang_AbsurdValue, hyang_AbsurdValue, hyang_AbsurdValue));
    SET_TAG(ap,  install("text"));
    SET_TAG(CDR(ap), install("condition"));
    SET_TAG(CDDR(ap), install("expr"));
    SET_TAG(CDDDR(ap), install("skipCalls"));
    argList = matchArgs_RC(ap, args, call);
    UNPROTECT(1);
    PROTECT(argList);
    if(CAR(argList) == hyang_MissingArg)
	SETCAR(argList, mkString(""));
    if(CADR(argList) == hyang_MissingArg)
	SETCAR(CDR(argList), hyang_AbsurdValue);
    if(CADDR(argList) == hyang_MissingArg)
	SETCAR(CDDR(argList), ScalarLogical(1));
    if(CADDDR(argList) == hyang_MissingArg)
	SETCAR(CDDDR(argList), ScalarInteger(0));

    if( !asLogical(CADDR(argList)) ) {
	UNPROTECT(1);
	return hyang_AbsurdValue;
    }

    browselevel = countContexts(CTXT_BROWSER, 1);
    savestack = hyang_PPStackTop;
    PROTECT(topExp = hyang_CurrentExpr);
    saveToplevelContext = hyang_TopLevelCtx;
    saveGlobalContext = hyang_GlobalCtx;

    if (!RDEBUG(rho)) {
	int skipCalls = asInteger(CADDDR(argList));
	cptr = hyang_GlobalCtx;
	while ( ( !(cptr->callflag & CTXT_FUNCTION) || skipCalls--)
		&& cptr->callflag )
	    cptr = cptr->nextcontext;
	hyangprtf("Called from: ");
	if( cptr != hyang_TopLevelCtx ) {
	    PrintCall(cptr->call, rho);
	    SET_RDEBUG(cptr->cloenv, 1);
	} else
	    hyangprtf("top level \n");

	hyang_BrowseLines = 0;
    }

    hyang_ReturnedValue = hyang_AbsurdValue;

    begincontext(&returncontext, CTXT_BROWSER, call, rho,
		 hyang_BaseEnv, argList, hyang_AbsurdValue);
    if (!SETJMP(returncontext.cjmpbuf)) {
	begincontext(&thiscontext, CTXT_RESTART, hyang_AbsurdValue, rho,
		     hyang_BaseEnv, hyang_AbsurdValue, hyang_AbsurdValue);
	if (SETJMP(thiscontext.cjmpbuf)) {
	    SET_RESTART_BIT_ON(thiscontext.callflag);
	    hyang_ReturnedValue = hyang_AbsurdValue;
	    hyang_Visible = FALSE;
	}
	hyang_GlobalCtx = &thiscontext;
	hyang_InsertRestartHandlers(&thiscontext, "browser");
	hyang_ReplConsole(rho, savestack, browselevel+1);
	endcontext(&thiscontext);
    }
    endcontext(&returncontext);

    hyang_CurrentExpr = topExp;
    UNPROTECT(1);
    hyang_PPStackTop = savestack;
    UNPROTECT(1);
    hyang_CurrentExpr = topExp;
    hyang_TopLevelCtx = saveToplevelContext;
    hyang_GlobalCtx = saveGlobalContext;
    return hyang_ReturnedValue;
}

void hyang_dotLast(void)
{
    SEXP cmd;
    hyang_GlobalCtx = hyang_TopLevelCtx = hyang_SessionCtx = &hyang_TopLevel;
    PROTECT(cmd = install(".Last"));
    hyang_CurrentExpr = findVar(cmd, hyang_GlobalEnv);
    if (hyang_CurrentExpr != hyang_UnboundValue && TYPEOF(hyang_CurrentExpr) == CLOSXP) {
	PROTECT(hyang_CurrentExpr = lang1(cmd));
	hyang_CurrentExpr = eval(hyang_CurrentExpr, hyang_GlobalEnv);
	UNPROTECT(1);
    }
    UNPROTECT(1);
    PROTECT(cmd = install(".Last.sys"));
    hyang_CurrentExpr = findVar(cmd, hyang_BaseNamespace);
    if (hyang_CurrentExpr != hyang_UnboundValue && TYPEOF(hyang_CurrentExpr) == CLOSXP) {
	PROTECT(hyang_CurrentExpr = lang1(cmd));
	hyang_CurrentExpr = eval(hyang_CurrentExpr, hyang_GlobalEnv);
	UNPROTECT(1);
    }
    UNPROTECT(1);
}

SEXP attribute_hidden do_quit(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    const char *tmp;
    SA_TYPE ask=SA_DEFAULT;
    int status, runLast;

    checkArity(op, args);
    if(countContexts(CTXT_BROWSER, 1)) {
	warning(_("cannot quit from browser"));
	return hyang_AbsurdValue;
    }
    if( !isString(CAR(args)) )
	error(_("one of \"yes\", \"no\", \"ask\" or \"default\" expected."));
    tmp = CHAR(STRING_ELT(CAR(args), 0)); /* ASCII */
    if( !strcmp(tmp, "ask") ) {
	ask = SA_SAVEASK;
	if(!hyang_Interactive)
	    warning(_("save=\"ask\" in non-interactive use: command-line default will be used"));
    } else if( !strcmp(tmp, "no") )
	ask = SA_NOSAVE;
    else if( !strcmp(tmp, "yes") )
	ask = SA_SAVE;
    else if( !strcmp(tmp, "default") )
	ask = SA_DEFAULT;
    else
	error(_("unrecognized value of 'save'"));
    status = asInteger(CADR(args));
    if (status == NA_INTEGER) {
	warning(_("invalid 'status', 0 assumed"));
	status = 0;
    }
    runLast = asLogical(CADDR(args));
    if (runLast == NA_LOGICAL) {
	warning(_("invalid 'runLast', FALSE assumed"));
	runLast = 0;
    }
    hyang_CleanUp(ask, status, runLast);
    exit(0);
}

#include <hyangexts/hyangcallbacks.h>

static hyang_ToplevelCallbackEl *hyangly_ToplevelTaskHandlers = NULL;

hyang_ToplevelCallbackEl *
hyangly_addTaskCallback(hyang_ToplevelCallback cb, void *data,
		   void (*finalizer)(void *), const char *name, int *pos)
{
    int which;
    hyang_ToplevelCallbackEl *el;
    el = (hyang_ToplevelCallbackEl *) malloc(sizeof(hyang_ToplevelCallbackEl));
    if(!el)
	error(_("cannot allocate space for toplevel callback element"));

    el->data = data;
    el->cb = cb;
    el->next = NULL;
    el->finalizer = finalizer;

    if(hyangly_ToplevelTaskHandlers == NULL) {
	hyangly_ToplevelTaskHandlers = el;
	which = 0;
    } else {
	hyang_ToplevelCallbackEl *tmp;
	tmp = hyangly_ToplevelTaskHandlers;
	which = 1;
	while(tmp->next) {
	    which++;
	    tmp = tmp->next;
	}
	tmp->next = el;
    }

    if(!name) {
	char buf[20];
	snprintf(buf, 20, "%d", which+1);
	el->name = strdup(buf);
    } else
	el->name = strdup(name);

    if(pos)
	*pos = which;

    return(el);
}

hyangboolean
hyangly_removeTaskCallbackByName(const char *name)
{
    hyang_ToplevelCallbackEl *el = hyangly_ToplevelTaskHandlers, *prev = NULL;
    hyangboolean status = TRUE;

    if(!hyangly_ToplevelTaskHandlers) {
	return(FALSE); /* error("there are no task callbacks registered"); */
    }

    while(el) {
	if(strcmp(el->name, name) == 0) {
	    if(prev == NULL) {
		hyangly_ToplevelTaskHandlers = el->next;
	    } else {
		prev->next = el->next;
	    }
	    break;
	}
	prev = el;
	el = el->next;
    }
    if(el) {
	if(el->finalizer)
	    el->finalizer(el->data);
	free(el->name);
	free(el);
    } else {
	status = FALSE;
    }
    return(status);
}

hyangboolean
hyangly_removeTaskCallbackByIndex(int id)
{
    hyang_ToplevelCallbackEl *el = hyangly_ToplevelTaskHandlers, *tmp = NULL;
    hyangboolean status = TRUE;

    if(id < 0)
	error(_("negative index passed to hyang_removeTaskCallbackByIndex"));

    if(hyangly_ToplevelTaskHandlers) {
	if(id == 0) {
	    tmp = hyangly_ToplevelTaskHandlers;
	    hyangly_ToplevelTaskHandlers = hyangly_ToplevelTaskHandlers->next;
	} else {
	    int i = 0;
	    while(el && i < (id-1)) {
		el = el->next;
		i++;
	    }

	    if(i == (id -1) && el) {
		tmp = el->next;
		el->next = (tmp ? tmp->next : NULL);
	    }
	}
    }
    if(tmp) {
	if(tmp->finalizer)
	    tmp->finalizer(tmp->data);
	free(tmp->name);
	free(tmp);
    } else {
	status = FALSE;
    }

    return(status);
}

SEXP
hyang_removeTaskCallback(SEXP which)
{
    int id;
    hyangboolean val;

    if(TYPEOF(which) == STRSXP) {
	if (LENGTH(which) == 0)
	    val = FALSE;
	else
	    val = hyangly_removeTaskCallbackByName(CHAR(STRING_ELT(which, 0)));
    } else {
	id = asInteger(which);
	if (id != NA_INTEGER) val = hyangly_removeTaskCallbackByIndex(id - 1);
	else val = FALSE;
    }
    return ScalarLogical(val);
}

SEXP
hyang_getTaskCallbackNames(void)
{
    SEXP ans;
    hyang_ToplevelCallbackEl *el;
    int n = 0;

    el = hyangly_ToplevelTaskHandlers;
    while(el) {
	n++;
	el = el->next;
    }
    PROTECT(ans = allocVector(STRSXP, n));
    n = 0;
    el = hyangly_ToplevelTaskHandlers;
    while(el) {
	SET_STRING_ELT(ans, n, mkChar(el->name));
	n++;
	el = el->next;
    }
    UNPROTECT(1);
    return(ans);
}

static hyangboolean hyangly_RunningToplevelHandlers = FALSE;

void
hyangly_callToplevelHandlers(SEXP expr, SEXP value, hyangboolean succeeded,
			hyangboolean visible)
{
    hyang_ToplevelCallbackEl *h, *prev = NULL;
    hyangboolean again;

    if(hyangly_RunningToplevelHandlers == TRUE)
	return;

    h = hyangly_ToplevelTaskHandlers;
    hyangly_RunningToplevelHandlers = TRUE;
    while(h) {
	again = (h->cb)(expr, value, succeeded, visible, h->data);
	if(hyang_CollectWarnings) {
	    REprintf(_("warning messages from top-level task callback '%s'\n"),
		     h->name);
	    PrintWarnings();
	}
	if(again) {
	    prev = h;
	    h = h->next;
	} else {
	    hyang_ToplevelCallbackEl *tmp;
	    tmp = h;
	    if(prev)
		prev->next = h->next;
	    h = h->next;
	    if(tmp == hyangly_ToplevelTaskHandlers)
		hyangly_ToplevelTaskHandlers = h;
	    if(tmp->finalizer)
		tmp->finalizer(tmp->data);
	    free(tmp);
	}
    }

    hyangly_RunningToplevelHandlers = FALSE;
}


hyangboolean
hyang_taskCallbackRoutine(SEXP expr, SEXP value, hyangboolean succeeded,
		      hyangboolean visible, void *userData)
{
    SEXP f = (SEXP) userData;
    SEXP e, tmp, val, cur;
    int errorOccurred;
    hyangboolean again, useData = LOGICAL(VECTOR_ELT(f, 2))[0];

    PROTECT(e = allocVector(LANGSXP, 5 + useData));
    SETCAR(e, VECTOR_ELT(f, 0));
    cur = CDR(e);
    SETCAR(cur, tmp = allocVector(LANGSXP, 2));
	SETCAR(tmp, hyang_QuoteSym);
	SETCAR(CDR(tmp), expr);
    cur = CDR(cur);
    SETCAR(cur, value);
    cur = CDR(cur);
    SETCAR(cur, ScalarLogical(succeeded));
    cur = CDR(cur);
    SETCAR(cur, tmp = ScalarLogical(visible));
    if(useData) {
	cur = CDR(cur);
	SETCAR(cur, VECTOR_ELT(f, 1));
    }

    val = hyang_tryEval(e, NULL, &errorOccurred);
    UNPROTECT(1); /* e */
    if(!errorOccurred) {
	PROTECT(val);
	if(TYPEOF(val) != LGLSXP) {
	    warning(_("top-level task callback did not return a logical value"));
	}
	again = asLogical(val);
	UNPROTECT(1);
    } else {
	again = FALSE;
    }
    return(again);
}

SEXP
hyang_addTaskCallback(SEXP f, SEXP data, SEXP useData, SEXP name)
{
    SEXP internalData;
    SEXP index;
    hyang_ToplevelCallbackEl *el;
    const char *tmpName = NULL;

    internalData = allocVector(VECSXP, 3);
    hyang_PreserveObj(internalData);
    SET_VECTOR_ELT(internalData, 0, f);
    SET_VECTOR_ELT(internalData, 1, data);
    SET_VECTOR_ELT(internalData, 2, useData);

    if(length(name))
	tmpName = CHAR(STRING_ELT(name, 0));

    PROTECT(index = allocVector(INTSXP, 1));
    el = hyangly_addTaskCallback(hyang_taskCallbackRoutine,  internalData,
			    (void (*)(void*)) hyang_ReleaseObj, tmpName,
			    INTEGER(index));

    if(length(name) == 0) {
	PROTECT(name = mkString(el->name));
	setAttrib(index, hyang_NamesSym, name);
	UNPROTECT(1);
    } else {
	setAttrib(index, hyang_NamesSym, name);
    }

    UNPROTECT(1);
    return(index);
}

#undef __MAIN__

#ifndef Win32
#include <hyangexts/hyangscm.h>
void F77_SYMBOL(intpr) (const char *, int *, int *, int *);
void attribute_hidden dummy12345(void)
{
    int i = 0;
    F77_CALL(intpr)("dummy", &i, &i, &i);
}

uintptr_t dummy_ii(void)
{
    int ii;
    volatile uintptr_t ii_addr = (uintptr_t) &ii;
    return ii_addr;
}
#endif

