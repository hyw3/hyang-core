/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* This should be regarded as part of the graphics engine:
   it is now a stub for code in grade */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>
#include <hyangexts/hyanggrengine.h>

typedef unsigned int (*F1)(SEXP x, int i, unsigned int bg);
typedef const char * (*F2)(unsigned int col);
typedef unsigned int (*F3)(const char *s);
typedef void (*F4)(hyangboolean save);

static F1 ptr_RGBpar3;
static F2 ptr_col2name;
static F3 hyang_ptrGE_str2col;
static F4 ptr_savePalette;

void hyang_gSetColPtrs(F1 f1, F2 f2, F3 f3, F4 f4)
{
    ptr_RGBpar3 = f1;
    ptr_col2name = f2;
    hyang_ptrGE_str2col = f3;
    ptr_savePalette = f4;
}

/* used in grid/src/gpar.c with bg = HYANG_TRANWHITE,
   in packages Cairo, canvas and jpeg */
/* in hyanggrengine.h */
unsigned int RGBpar3(SEXP x, int i, unsigned int bg)
{
    if (!ptr_RGBpar3) error("package grade must be loaded");
    return (ptr_RGBpar3)(x, i, bg);
}

/* in hyanggrengine.h, used by devices */
unsigned int RGBpar(SEXP x, int i)
{
    return RGBpar3(x, i, HYANG_TRANWHITE);
}

/* used in grid */
/* in hyanggrengine.h */
const char *col2name(unsigned int col)
{
    if (!ptr_col2name) error("package grade must be loaded");
    return (ptr_col2name)(col);
}

/* used in grade for fg and bg of devices */
/* in hyanggrengine.h */
unsigned int hyang_GEstr2col(const char *s)
{
    if (!hyang_ptrGE_str2col) error("package grade must be loaded");
    return (hyang_ptrGE_str2col)(s);
}

/* used in engine.c */
attribute_hidden
void savePalette(hyangboolean save)
{
    if (!ptr_savePalette) error("package grade must be loaded");
    (ptr_savePalette)(save);
}
