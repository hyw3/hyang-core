/* Hyang Programming Language
 *
 * Copyright (C) 2017-2020 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2020 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

#include "hyangdefn.h"
#include <hyangintl.h>
#include <hyangversion.h>

void attribute_hidden PrintGreeting(void)
{
    char buf[384];

    hyangprtf("\n");
    hyangprtf(_("=============================================================\n"));
    PrintVersion_part_1(buf, 384);
    hyangprtf("%s\n", buf);

    hyangprtf(_("Hyang is free software and comes with ABSOLUTELY NO WARRANTY.\n\
You are welcome to use or distribute Hyang under the terms of\n\
GNU GPL version 3.\n\n"));
    hyangprtf(_("Type 'donate()' for information on how to donate Hyang.\n\n"));
    hyangprtf(_("Hyang is an open source project with many contributors. Type\n\
'contributors()' for the list of known contributors and type\n\
'refer()' on how to cite Hyang in publication.\n\n"));
    hyangprtf(_("Type 'alisabana.fun()' for Alisabana's Constant, 'hyanghelp()'\n\
for the helps, and 'q()' or 'quit()' to quit Hyang.\n"));
    hyangprtf(_("=============================================================\n\n"));
}

SEXP attribute_hidden do_version(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP value, names;
    char buf[128];

    checkArity(op, args);
    PROTECT(value = allocVector(VECSXP,14));
    PROTECT(names = allocVector(STRSXP,14));

    SET_STRING_ELT(names, 0, mkChar("platform"));
    SET_VECTOR_ELT(value, 0, mkString(HYANG_PLAT));
    SET_STRING_ELT(names, 1, mkChar("arch"));
    SET_VECTOR_ELT(value, 1, mkString(HYANG_CPU));
    SET_STRING_ELT(names, 2, mkChar("os"));
    SET_VECTOR_ELT(value, 2, mkString(HYANG_OS));

    snprintf(buf, 128, "%s, %s", HYANG_CPU, HYANG_OS);
    SET_STRING_ELT(names, 3, mkChar("system"));
    SET_VECTOR_ELT(value, 3, mkString(buf));

    SET_STRING_ELT(names, 4, mkChar("status"));
    SET_VECTOR_ELT(value, 4, mkString(HYANG_STATUS));
    SET_STRING_ELT(names, 5, mkChar("major"));
    SET_VECTOR_ELT(value, 5, mkString(HYANG_MAJOR));
    SET_STRING_ELT(names, 6, mkChar("minor"));
    SET_VECTOR_ELT(value, 6, mkString(HYANG_MINOR));
    SET_STRING_ELT(names, 7, mkChar("year"));
    SET_VECTOR_ELT(value, 7, mkString(HYANG_YEAR));
    SET_STRING_ELT(names, 8, mkChar("month"));
    SET_VECTOR_ELT(value, 8, mkString(HYANG_MONTH));
    SET_STRING_ELT(names, 9, mkChar("day"));
    SET_VECTOR_ELT(value, 9, mkString(HYANG_DAY));
    SET_STRING_ELT(names, 10, mkChar("hyscm check-in"));

    snprintf(buf, 128, "%s", HYANG_HYSCM_CHECKIN);
    SET_VECTOR_ELT(value, 10, mkString(buf));
    SET_STRING_ELT(names, 11, mkChar("language"));
    SET_VECTOR_ELT(value, 11, mkString("hyang"));

    PrintVersionString(buf, 128);
    SET_STRING_ELT(names, 12, mkChar("version.string"));
    SET_VECTOR_ELT(value, 12, mkString(buf));
    SET_STRING_ELT(names, 13, mkChar("nickname"));
    SET_VECTOR_ELT(value, 13, mkString(HYANG_NICK));

    setAttrib(value, hyang_NamesSym, names);
    UNPROTECT(2);
    return value;
}

void attribute_hidden PrintVersion(char *s, size_t len)
{
    PrintVersion_part_1(s, len);

    strcat(s, "\n"
	   "Hyang is free software and comes with ABSOLUTELY NO WARRANTY.\n"
	   "You are welcome to redistribute it under the terms of the\n"
	   "GNU General Public License versions 2 or 3.\n"
	   "For more information about these matters see\n"
	   "http://www.gnu.org/licenses/.\n");
}

void attribute_hidden PrintVersionString(char *s, size_t len)
{
    if(HYANG_HYSCM_CHECKIN == 0) {
	snprintf(s, len, "Hyang version %s.%s %s (%s-%s-%s)",
		HYANG_MAJOR, HYANG_MINOR, HYANG_STATUS, HYANG_YEAR, HYANG_MONTH, HYANG_DAY);
    } else if(strlen(HYANG_STATUS) == 0) {
	snprintf(s, len, "Hyang version %s.%s (%s / %s-%s-%s)",
		HYANG_MAJOR, HYANG_MINOR, HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY);
    } else if(strcmp(HYANG_STATUS, "Under Development (Unstable)") == 0) {
	snprintf(s, len, "Hyang %s (%s / %s-%s-%s)",
		HYANG_STATUS, HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY);
    } else {
	snprintf(s, len, "Hyang version %s.%s %s (%s / %s-%s-%s)",
		HYANG_MAJOR, HYANG_MINOR, HYANG_STATUS, HYANG_HYSCM_CHECKIN, HYANG_YEAR, HYANG_MONTH, HYANG_DAY);
    }
}

void attribute_hidden PrintVersion_part_1(char *s, size_t len)
{
#define SPRINTF_2(_FMT, _OBJ) snprintf(tmp, 128, _FMT, _OBJ); strcat(s, tmp)
    char tmp[128];

    PrintVersionString(s, len);
    if(strlen(HYANG_NICK) != 0) {
	char nick[128];
	snprintf(nick, 128, " -- \"%s\"", HYANG_NICK);
	strcat(s, nick);
    }
    SPRINTF_2("\n\nCopyright (C) 2017-%s The Hyang Language Foundation\n",
	      HYANG_YEAR);

    SPRINTF_2("Platform: %s", HYANG_PLAT);
    if(strlen(HYANG_ARCH)) { SPRINTF_2("/%s", HYANG_ARCH); }
    SPRINTF_2(" (%d-bit)\n", 8*(int)sizeof(void *));
}

SEXP attribute_hidden do_internalsID(SEXP call, SEXP op, SEXP args, SEXP env)
{
    return mkString(HYANG_INTLS_UUID);
}

