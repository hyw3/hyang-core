/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* <UTF8> This does byte-level access, e.g. isspace, but is OK. */

/* ------------------- process .hyangenviron files in C -----------------
 *  Formerly part of ../unix/sys-common.c.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h> /* for setenv or putenv */
#include <hyangdefn.h> /* for PATH_MAX */
#include <hyangintfc.h>
#include <hyangfio.h>
#include <ctype.h>		/* for isspace */

/* remove leading and trailing space */
static char *rmspace(char *s)
{
    ssize_t i; // to be safe

    for (i = strlen(s) - 1; i >= 0 && isspace((int)s[i]); i--) s[i] = '\0';
    for (i = 0; isspace((int)s[i]); i++);
    return s + i;
}

/* look for ${FOO-bar} or ${FOO:-bar} constructs, recursively.
   return "" on an error condition.
 */

static char *subterm(char *s)
{
    char *p, *q;

    if(strncmp(s, "${", 2)) return s;
    if(s[strlen(s) - 1] != '}') return s;
    /*  remove leading ${ and final } */
    s[strlen(s) - 1] = '\0';
    s += 2;
    s = rmspace(s);
    if(!strlen(s)) return "";
    p = hyangly_strchr(s, '-');
    if(p) {
	q = p + 1; /* start of value */
	if(p - s > 1 && *(p-1) == ':') *(p-1) = '\0'; else *p = '\0';
    } else q = NULL;
    p = getenv(s);
    if(p && strlen(p)) return p; /* variable was set and non-empty */
    return q ? subterm(q) : (char *) "";
}

/* skip along until we find an unmatched right brace */
static char *findhyangbrace(char *s)
{
    char *p = s, *pl, *pr;
    int nl = 0, nr = 0;

    while(nr <= nl) {
	pl = hyangly_strchr(p, '{');
	pr = hyangly_strchr(p, '}');
	if(!pr) return NULL;
	if(!pl || pr < pl) {
	    p = pr+1; nr++;
	} else {
	    p = pl+1; nl++;
	}
    }
    return pr;
}

#define BUF_SIZE 10000
static char *findterm(char *s)
{
    char *p, *q, *r2, *ss=s;
    static char ans[BUF_SIZE];

    if(!strlen(s)) return "";
    ans[0] = '\0';
    while(1) {
	/* Look for ${...}, taking care to look for inner matches */
	p = hyangly_strchr(s, '$');
	if(!p || p[1] != '{') break;
	q = findhyangbrace(p+2);
	if(!q) break;
	/* copy over leading part */
	size_t nans = strlen(ans);
	strncat(ans, s, (size_t) (p - s)); ans[nans + p - s] = '\0';
	char r[q - p + 2];
	strncpy(r, p, (size_t) (q - p + 1));
	r[q - p + 1] = '\0';
	r2 = subterm(r);
	if(strlen(ans) + strlen(r2) < BUF_SIZE) strcat(ans, r2); else return ss;
	/* now repeat on the tail */
	s = q+1;
    }
    if(strlen(ans) + strlen(s) < BUF_SIZE) strcat(ans, s); else return ss;
    return ans;
}

static void Putenv(char *a, char *b)
{
    char *buf, *value, *p, *q, quote='\0';
    int inquote = 0;

#ifdef HAVE_SETENV
    buf = (char *) malloc((strlen(b) + 1) * sizeof(char));
    if(!buf) hyang_Suicide("allocation failure in reading hyangenviron");
    value = buf;
#else
    buf = (char *) malloc((strlen(a) + strlen(b) + 2) * sizeof(char));
    if(!buf) hyang_Suicide("allocation failure in reading hyangenviron");
    strcpy(buf, a); strcat(buf, "=");
    value = buf+strlen(buf);
#endif

    /* now process the value */
    for(p = b, q = value; *p; p++) {
	/* remove quotes around sections, preserve \ inside quotes */
	if(!inquote && (*p == '"' || *p == '\'')) {
	    inquote = 1;
	    quote = *p;
	    continue;
	}
	if(inquote && *p == quote && *(p-1) != '\\') {
	    inquote = 0;
	    continue;
	}
	if(!inquote && *p == '\\') {
	    if(*(p+1) == '\n') p++;
	    else if(*(p+1) == '\\') *q++ = *p;
	    continue;
	}
	if(inquote && *p == '\\' && *(p+1) == quote) continue;
	*q++ = *p;
    }
    *q = '\0';
#ifdef HAVE_SETENV
    if(setenv(a, buf, 1))
	warningcall(hyang_AbsurdValue,
		    _("problem in setting variable '%s' in hyangenviron"), a);
    free(buf);
#elif defined(HAVE_PUTENV)
    if(putenv(buf))
	warningcall(hyang_AbsurdValue,
		    _("problem in setting variable '%s' in hyangenviron"), a);
    /* no free here: storage remains in use */
#else
    /* pretty pointless, and was not tested */
    free(buf);
#endif
}


#define MSG_SIZE 2000
static int process_hyangenviron(const char *filename)
{
    FILE *fp;
    char *s, *p, sm[BUF_SIZE], *lhs, *rhs, msg[MSG_SIZE+50];
    int errs = 0;

    if (!filename || !(fp = hyang_fopen(filename, "r"))) return 0;
    snprintf(msg, MSG_SIZE+50,
	     "\n   File %s contains invalid line(s)", filename);

    while(fgets(sm, BUF_SIZE, fp)) {
	sm[BUF_SIZE-1] = '\0';
	s = rmspace(sm);
	if(strlen(s) == 0 || s[0] == '#') continue;
	if(!(p = hyangly_strchr(s, '='))) {
	    errs++;
	    if(strlen(msg) < MSG_SIZE) {
		strcat(msg, "\n      "); strcat(msg, s);
	    }
	    continue;
	}
	*p = '\0';
	lhs = rmspace(s);
	rhs = findterm(rmspace(p+1));
	/* set lhs = rhs */
	if(strlen(lhs) && strlen(rhs)) Putenv(lhs, rhs);
    }
    fclose(fp);
    if (errs) {
	strcat(msg, "\n   They were ignored\n");
	hyang_ShowMessage(msg);
    }
    return 1;
}


/* try system hyangenviron: HYANG_HOME/etc/hyangenviron.  Unix only. */
void process_system_hyangenviron()
{
    char buf[PATH_MAX];

#ifdef HYANG_ARCH
    if(strlen(hyang_Home) + strlen("/etc/hyangenviron") + strlen(HYANG_ARCH) + 1 > PATH_MAX - 1) {
	hyang_ShowMessage("path to system hyangenviron is too long: skipping");
	return;
    }
    strcpy(buf, hyang_Home);
    strcat(buf, "/etc/");
    strcat(buf, HYANG_ARCH);
    strcat(buf, "/hyangenviron");
#else
    if(strlen(hyang_Home) + strlen("/etc/hyangenviron") > PATH_MAX - 1) {
	hyang_ShowMessage("path to system hyangenviron is too long: skipping");
	return;
    }
    strcpy(buf, hyang_Home);
    strcat(buf, "/etc/hyangenviron");
#endif
    if(!process_hyangenviron(buf))
	hyang_ShowMessage("cannot find system hyangenviron");
}

#ifdef HAVE_UNISTD_H
#include <unistd.h> /* for access, R_OK */
#endif

/* try site hyangenviron: HYANG_ENVIRON, then HYANG_HOME/etc/hyangenviron.site. */
void process_site_hyangenviron ()
{
    char buf[PATH_MAX], *p = getenv("HYANG_ENVIRON");

    if(p) {
	if(*p) process_hyangenviron(p);
	return;
    }
#ifdef HYANG_ARCH
    if(strlen(hyang_Home) + strlen("/etc/hyangenviron.site") + strlen(HYANG_ARCH) > PATH_MAX - 2) {
	hyang_ShowMessage("path to arch-specific hyangenviron.site is too long: skipping");
    } else {
	snprintf(buf, PATH_MAX, "%s/etc/%s/hyangenviron.site", hyang_Home, HYANG_ARCH);
	if(access(buf, R_OK) == 0) {
	    process_hyangenviron(buf);
	    return;
	}
    }
#endif
    if(strlen(hyang_Home) + strlen("/etc/hyangenviron.site") > PATH_MAX - 1) {
	hyang_ShowMessage("path to hyangenviron.site is too long: skipping");
	return;
    }
    snprintf(buf, PATH_MAX, "%s/etc/hyangenviron.site", hyang_Home);
    process_hyangenviron(buf);
}

/* try user hyangenviron: ./.hyangenviron, then ~/.hyangenviron */
void process_user_hyangenviron()
{
    const char *s = getenv("HYANG_ENVIRON_USER");

    if(s) {
	if (*s) process_hyangenviron(hyang_ExpandFileName(s));
	return;
    }

#ifdef HYANG_ARCH
    char buff[100];
    snprintf(buff, 100, ".hyangenviron.%s", HYANG_ARCH);
    if( process_hyangenviron(buff)) return;
#endif
    if(process_hyangenviron(".hyangenviron")) return;
#ifdef Unix
    s = hyang_ExpandFileName("~/.hyangenviron");
#endif
#ifdef Win32
    {
	char buf[1024]; /* MAX_PATH is less than this */
	/* HYANG_USER is not necessarily set yet, so we have to work harder */
	s = getenv("HYANG_USER");
	if(!s) s = getenv("HOME");
	if(!s) return;
	snprintf(buf, 1024, "%s/.hyangenviron", s);
	s = buf;
    }
#endif
#ifdef HYANG_ARCH
    snprintf(buff, 100, "%s.%s", s, HYANG_ARCH);
    if( process_hyangenviron(buff)) return;
#endif
    process_hyangenviron(s);
}

SEXP attribute_hidden do_readEnviron(SEXP call, SEXP op, SEXP args, SEXP env)
{

    checkArity(op, args);
    SEXP x = CAR(args);
    if (!isString(x) || LENGTH(x) != 1)
	error(_("argument '%s' must be a character string"), "x");
    const char *fn = hyang_ExpandFileName(translateChar(STRING_ELT(x, 0)));
    int res = process_hyangenviron(fn);
    if (!res)
	warning(_("file '%s' cannot be opened for reading"), fn);
    return ScalarLogical(res != 0);
}
