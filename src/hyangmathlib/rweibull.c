/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  DESCRIPTION
 *
 *    Random variates from the Weibull distribution.
 */

#include "hyangmathlib.h"

double rweibull(double shape, double scale)
{
    if (!HYANG_FINITE(shape) || !HYANG_FINITE(scale) || shape <= 0. || scale <= 0.) {
	if(scale == 0.) return 0.;
	/* else */
	ML_ERR_return_NAN;
    }

    return scale * pow(-log(unif_rand()), 1.0 / shape);
}
