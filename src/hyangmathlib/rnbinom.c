/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  SYNOPSIS
 *
 *    #include <hyangmath.h>
 *    double rnbinom(double n, double p)
 *
 *  DESCRIPTION
 *
 *    Random variates from the negative binomial distribution.
 *
 *  NOTES
 *
 *    x = the number of failures before the n-th success
 *
 *  REFERENCE
 *
 *    Devroye, L. (1986).
 *    Non-Uniform Random Variate Generation.
 *    New York:Springer-Verlag.  Pages 488 and 543.
 *
 *  METHOD
 *
 *    Generate lambda as gamma with shape parameter n and scale
 *    parameter p/(1-p).  Return a Poisson deviate with mean lambda.
 */

#include "hyangmathlib.h"

double rnbinom(double size, double prob)
{
    if(!HYANG_FINITE(prob) || ISNAN(size) || size <= 0 || prob <= 0 || prob > 1)
	
	ML_ERR_return_NAN;
    if(!HYANG_FINITE(size)) size = DBL_MAX / 2.; // '/2' to prevent rgamma() returning Inf
    return (prob == 1) ? 0 : rpois(rgamma(size, (1 - prob) / prob));
}

double rnbinom_mu(double size, double mu)
{
    if(!HYANG_FINITE(mu) || ISNAN(size) || size <= 0 || mu < 0)
	ML_ERR_return_NAN;
    if(!HYANG_FINITE(size)) size = DBL_MAX / 2.;
    return (mu == 0) ? 0 : rpois(rgamma(size, mu / size));
}
