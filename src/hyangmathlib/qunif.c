/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  DESCRIPTION
 *
 *    The quantile function of the uniform distribution.
 */

#include "hyangmathlib.h"
#include "dpq.h"

double qunif(double p, double a, double b, int lower_tail, int log_p)
{
#ifdef IEEE_754
    if (ISNAN(p) || ISNAN(a) || ISNAN(b))
	return p + a + b;
#endif
    R_Q_P01_check(p);
    if (!HYANG_FINITE(a) || !HYANG_FINITE(b)) ML_ERR_return_NAN;
    if (b < a) ML_ERR_return_NAN;
    if (b == a) return a;

    return a + R_DT_qIv(p) * (b - a);
}




