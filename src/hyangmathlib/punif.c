/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*  DESCRIPTION
 *
 *    The distribution function of the uniform distribution.
 */

#include "hyangmathlib.h"
#include "dpq.h"

double punif(double x, double a, double b, int lower_tail, int log_p)
{
#ifdef IEEE_754
    if (ISNAN(x) || ISNAN(a) || ISNAN(b))
	return x + a + b;
#endif
    if (b < a) ML_ERR_return_NAN;
    if (!HYANG_FINITE(a) || !HYANG_FINITE(b)) ML_ERR_return_NAN;

    if (x >= b)
	return R_DT_1;
    if (x <= a)
	return R_DT_0;
    if (lower_tail) return R_D_val((x - a) / (b - a));
    else return R_D_val((b - x) / (b - a));
}
