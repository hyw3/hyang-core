/* Hyang Programming Language
 * 
 * Copyright (C) 1998-2014 Ross Ihaka
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/*  DESCRIPTION
 *
 *    The density of the chi-squared distribution.
 */

#include "hyangmathlib.h"
#include "dpq.h"

double dchisq(double x, double df, int give_log)
{
    return dgamma(x, df / 2., 2., give_log);
}
