/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


/* Code to register grid functions with Hyang
 */
#include <hyangexts/hyangvis.h>
#include <hyangexts/hyangdyl.h>
#include "grid.h"

#define LCALLDEF(name, n)  {#name, (DL_FUNC) &L_##name, n}

static const hyang_CallMethodDef callMethods[] = {
    LCALLDEF(initGrid, 1),
    LCALLDEF(killGrid, 0),
    LCALLDEF(gridDirty, 0),
    LCALLDEF(currentViewport, 0),
    LCALLDEF(setviewport, 2), 
    LCALLDEF(downviewport, 2), 
    LCALLDEF(downvppath, 3), 
    LCALLDEF(unsetviewport, 1), 
    LCALLDEF(upviewport, 1), 
    LCALLDEF(getDisplayList, 0),
    LCALLDEF(setDisplayList, 1),
    LCALLDEF(getDLelt, 1),
    LCALLDEF(setDLelt, 1),
    LCALLDEF(getDLindex, 0), 
    LCALLDEF(setDLindex, 1),
    LCALLDEF(getDLon, 0),
    LCALLDEF(setDLon, 1),
    LCALLDEF(getEngineDLon, 0),
    LCALLDEF(setEngineDLon, 1),
    LCALLDEF(getCurrentGrob, 0),
    LCALLDEF(setCurrentGrob, 1),
    LCALLDEF(getEngineRecording, 0),
    LCALLDEF(setEngineRecording, 1),
    LCALLDEF(currentGPar, 0),
    LCALLDEF(newpagerecording, 0),
    LCALLDEF(newpage, 0),
    LCALLDEF(initGPar, 0),
    LCALLDEF(initViewportStack, 0),
    LCALLDEF(initDisplayList, 0),
    LCALLDEF(moveTo, 2),
    LCALLDEF(lineTo, 3), 
    LCALLDEF(lines, 4), 
    LCALLDEF(segments, 5), 
    LCALLDEF(arrows, 12), 
    LCALLDEF(path, 4),
    LCALLDEF(polygon, 3),
    LCALLDEF(xspline, 7),
    LCALLDEF(circle, 3),
    LCALLDEF(rect, 6),
    LCALLDEF(raster, 8),
    LCALLDEF(cap, 0),
    LCALLDEF(text, 7),
    LCALLDEF(points, 4),
    LCALLDEF(clip, 6),
    LCALLDEF(pretty, 1),
    LCALLDEF(locator, 0),
    LCALLDEF(convert, 4),
    LCALLDEF(layoutRegion, 2),
    LCALLDEF(getGPar, 0),
    LCALLDEF(setGPar, 1),
    LCALLDEF(circleBounds, 4),
    LCALLDEF(locnBounds, 3),
    LCALLDEF(rectBounds, 7),
    LCALLDEF(textBounds, 7),
    LCALLDEF(xsplineBounds, 8),
    LCALLDEF(xsplinePoints, 8),
    LCALLDEF(stringMetric, 1),
    {"validUnits", (DL_FUNC) &validUnits, 1},
    { NULL, NULL, 0 }
};


void attribute_visible hyang_init_grid(DllInfo *dll) 
{
    /* No .C, .Fortran, or .Nexus routines => NULL
     */
    hyang_regRoutines(dll, NULL, callMethods, NULL, NULL);
    hyang_useDynSyms(dll, FALSE);
    hyang_forceSyms(dll, FALSE);
}
