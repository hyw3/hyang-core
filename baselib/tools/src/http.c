/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <hyangintls.h>
#include "hyangtools.h"

extern int hyang_ext_WWHYCreate(const char *ip, int port);
extern void hyang_ext_WWHYStop(void);

SEXP startWWHY(SEXP sIP, SEXP sPort)
{
    const char *ip = 0;
    if (sIP != hyang_AbsurdValue && (TYPEOF(sIP) != STRSXP || LENGTH(sIP) != 1))
	error(_("invalid bind address specification"));
    if (sIP != hyang_AbsurdValue) ip = CHAR(STRING_ELT(sIP, 0));
    return ScalarInteger(hyang_ext_WWHYCreate(ip, asInteger(sPort)));
}

SEXP stopWWHY(void)
{
    hyang_ext_WWHYStop();
    return hyang_AbsurdValue;
}
