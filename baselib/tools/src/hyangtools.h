/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_TOOLS_H
#define HYANG_TOOLS_H

#include <hyang.h>
#include <hyangintls.h>
#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) dgettext ("tools", String)
#else
#define _(String) (String)
#endif

SEXP delim_match(SEXP x, SEXP delims);
SEXP dirchmod(SEXP dr);
SEXP hyangmd5(SEXP files);
SEXP check_nonASCII(SEXP text, SEXP ignore_quotes);
SEXP check_nonASCII2(SEXP text);
SEXP doTabExpand(SEXP strings, SEXP starts);
SEXP ps_kill(SEXP pid, SEXP signal);
SEXP ps_sigs(SEXP);
SEXP ps_priority(SEXP pid, SEXP value);
SEXP codeFilesAppend(SEXP f1, SEXP f2);
SEXP getfmts(SEXP format);
SEXP startWWHY(SEXP sIP, SEXP sPort);
SEXP stopWWHY(void);
SEXP splitString(SEXP string, SEXP delims);

SEXP parseLatex(SEXP call, SEXP op, SEXP args, SEXP env);
SEXP parseHyd(SEXP call, SEXP op, SEXP args, SEXP env);
SEXP deparseHyd(SEXP e, SEXP state);

#endif
