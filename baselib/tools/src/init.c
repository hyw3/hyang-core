/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hyangtools.h"
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangvis.h>

#ifdef UNUSED

void Renctest(char **x)
{
    hyangprtf("'%s', nbytes = %d\n", x[0], strlen(x[0]));
}

static const hyang_CMethodDef CEntries[]  = {
    {"Renctest", (DL_FUNC) &Renctest, 1},
    {NULL, NULL, 0}
};
#endif

#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_CallMethodDef CallEntries[] = {
    CALLDEF(codeFilesAppend, 2),
    CALLDEF(delim_match, 2),
    CALLDEF(dirchmod, 2),
    CALLDEF(getfmts, 1),
    CALLDEF(hyangmd5, 1),
    CALLDEF(check_nonASCII, 2),
    CALLDEF(check_nonASCII2, 1),
    CALLDEF(doTabExpand, 2),
    CALLDEF(ps_kill, 2),
    CALLDEF(ps_sigs, 1),
    CALLDEF(ps_priority, 2),
    CALLDEF(startWWHY, 2),
    CALLDEF(stopWWHY, 0),
    CALLDEF(deparseHyd, 2),
    CALLDEF(splitString, 2),

    {NULL, NULL, 0}
};

#define EXTDEF(name, n)  {#name, (DL_FUNC) &name, n}
static const hyang_ExternalMethodDef ExtEntries[] = {
    EXTDEF(parseLatex, 4),
    EXTDEF(parseHyd, 9),

    {NULL, NULL, 0}
};


void attribute_visible
hyang_init_tools(DllInfo *dll)
{
    hyang_regRoutines(dll, NULL, CallEntries, NULL, ExtEntries);
    hyang_useDynSyms(dll, FALSE);
    hyang_forceSyms(dll, FALSE);
}

