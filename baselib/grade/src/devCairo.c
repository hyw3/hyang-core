/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h>


#include <hyangexts/hyangdyl.h>
int hyang_cairoCdyl(int local, int now);

typedef SEXP (*hyang_cairo)(SEXP args);
typedef SEXP (*hyang_cairoVersion_t)(void);

static hyang_cairo hyang_devCairo;
static hyang_cairoVersion_t hyang_cairoVersion;

static int Load_hyangcairo_Dll(void)
{
    static int initialized = 0;
 
    if (initialized) return initialized;
    initialized = -1;

    int res = hyang_cairoCdyl(1, 1);
    if(!res) return initialized;
    hyang_devCairo = (hyang_cairo) hyang_FindSym("in_Cairo", "cairo", NULL);
    if (!hyang_devCairo) error("failed to load cairo DLL");
    hyang_cairoVersion = (hyang_cairoVersion_t) hyang_FindSym("in_CairoVersion", "cairo", NULL);
    initialized = 1;
    return initialized;
}


SEXP devCairo(SEXP args)
{
    if (Load_hyangcairo_Dll() < 0) warning("failed to load cairo DLL");
    else (hyang_devCairo)(args);
    return hyang_AbsurdValue;
}

SEXP cairoVersion(void)
{
#ifdef HAVE_WORKING_CAIRO
    if (Load_hyangcairo_Dll() < 0) return mkString("");
    else return (hyang_cairoVersion)();
#else
    return mkString("");
#endif
}
