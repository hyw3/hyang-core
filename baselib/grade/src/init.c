/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyang.h>
#include <hyangintls.h>
#include <hyangexts/hyanggrengine.h>

#include "grade.h"
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangvis.h>

#ifndef _WIN32
/* This really belongs with the X11 module, but it is about devices */
static SEXP cairoProps(SEXP in)
{
    int which = asInteger(in);
    if(which == 1)
	return ScalarLogical(
#ifdef HAVE_WORKING_CAIRO
	    1
#else
	    0
#endif
	    );
    else if(which == 2)
	return ScalarLogical(
#ifdef HAVE_PANGOCAIRO
	    1
#else
	    0
#endif
	    );
    return hyang_AbsurdValue;
}
#endif

#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_CallMethodDef CallEntries[] = {
    CALLDEF(Type1FontInUse, 2),
    CALLDEF(CIDFontInUse, 2),
    CALLDEF(hyang_CreateAtVector, 4),
    CALLDEF(hyang_GAxisPars, 3),
    CALLDEF(chull, 1),
    CALLDEF(gray, 2),
    CALLDEF(RGB2hsv, 1),
    CALLDEF(rgb, 6),
    CALLDEF(hsv, 4),
    CALLDEF(hcl, 5),
    CALLDEF(col2rgb, 2),
    CALLDEF(colors, 0),
    CALLDEF(palette, 1),
    CALLDEF(palette2, 1),
    CALLDEF(cairoVersion, 0),
    CALLDEF(bmVersion, 0),

#ifndef _WIN32
    CALLDEF(makeQuartzDefault, 0),
    CALLDEF(cairoProps, 1),
#else
    CALLDEF(bringToTop, 2),
    CALLDEF(msgWindow, 2),
#endif
    {NULL, NULL, 0}
};

#define EXTDEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_ExternalMethodDef ExtEntries[] = {
    EXTDEF(PicTeX, 6),
    EXTDEF(PostScript, 19),
    EXTDEF(XFig, 14),
    EXTDEF(PDF, 20),
    EXTDEF(devCairo, 11),
    EXTDEF(devcap, 0),
    EXTDEF(devcapture, 1),
    EXTDEF(devcontrol, 1),
    EXTDEF(devcopy, 1),
    EXTDEF(devcur, 0),
    EXTDEF(devdisplaylist, 0),
    EXTDEF(devholdflush, 1),
    EXTDEF(devnext, 1),
    EXTDEF(devoff, 1),
    EXTDEF(devprev, 1),
    EXTDEF(devset, 1),
    EXTDEF(devsize, 0),
    EXTDEF(contourLines, 4),
    EXTDEF(getSnapshot, 0),
    EXTDEF(playSnapshot, 1),
    EXTDEF(getGraphicsEvent, 1),
    EXTDEF(getGraphicsEventEnv, 1),
    EXTDEF(setGraphicsEventEnv, 2),
    EXTDEF(devAskNewPage, 1),

#ifdef _WIN32
    EXTDEF(savePlot, 4),
    EXTDEF(devga, 21),
#else
    EXTDEF(savePlot, 3),
    EXTDEF(Quartz, 11),
    EXTDEF(X11, 17),
#endif
    {NULL, NULL, 0}
};

#ifdef HAVE_AQUA
extern void setup_hyangdotApp(void);
extern hyangboolean useaqua;
#endif

void attribute_visible hyang_init_grade(DllInfo *dll)
{
    initPalette();
    hyang_regRoutines(dll, NULL, CallEntries, NULL, ExtEntries);
    hyang_useDynSyms(dll, FALSE);
    hyang_forceSyms(dll, TRUE);

#ifdef HAVE_AQUA
/* hyang.app will run event loop, so if we are running under that we don't
   need to run one here */
    if(useaqua) setup_hyangdotApp();
#endif
}
