/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyang.h>
#include <hyangintls.h>
#include <hyangexts/hyanggrengine.h>

#include "grade.h"

SEXP hyang_CreateAtVector(SEXP axp, SEXP usr, SEXP nint, SEXP is_log)
{
    int nint_v = asInteger(nint);
    hyangboolean logflag = asLogical(is_log);

    PROTECT(axp = coerceVector(axp, REALSXP));
    PROTECT(usr = coerceVector(usr, REALSXP));
    if(LENGTH(axp) != 3) error(_("'%s' must be numeric of length %d"), "axp", 3);
    if(LENGTH(usr) != 2) error(_("'%s' must be numeric of length %d"), "usr", 2);

    SEXP res = CreateAtVector(REAL(axp), REAL(usr), nint_v, logflag);
    // -> ../../../main/plot.c
    UNPROTECT(2);
    return res;
}

SEXP hyang_GAxisPars(SEXP usr, SEXP is_log, SEXP nintLog)
{
    hyangboolean logflag = asLogical(is_log);
    int n = asInteger(nintLog);// will be changed on output ..
    double min, max;
    const char *nms[] = {"axp", "n", ""};
    SEXP axp, ans;

    usr = coerceVector(usr, REALSXP);
    if(LENGTH(usr) != 2) error(_("'%s' must be numeric of length %d"), "usr", 2);
    min = REAL(usr)[0];
    max = REAL(usr)[1];

    GAxisPars(&min, &max, &n, logflag, 0);// axis = 0 :<==> do not warn.. [TODO!]
    // -> ../../../main/graphics.c

    PROTECT(ans = mkNamed(VECSXP, nms));
    SET_VECTOR_ELT(ans, 0, (axp = allocVector(REALSXP, 2)));// protected
    SET_VECTOR_ELT(ans, 1, ScalarInteger(n));
    REAL(axp)[0] = min;
    REAL(axp)[1] = max;

    UNPROTECT(1);
    return ans;
}
