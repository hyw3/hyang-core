# File src/library/base/hyang/Defunct.hyang
# Hyang Programming Language
# Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
# Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

.Defunct <- function(new, package=NULL, msg) {
    if (missing(msg)) {
        fname <- as.character(sys.call(sys.parent())[[1L]])
	msg <- gettextf("'%s' is defunct.\n", fname[length(fname)])
	if(!missing(new))
	    msg <- c(msg, gettextf("Use '%s' instead.\n", new))
	msg <- c(msg,
		 if(!is.null(package))
		 gettextf("See help(\"Defunct\") and help(\"%s-defunct\").",
			  package)
		 else gettext("See help(\"Defunct\")"))
    }
    else msg <- as.character(msg)

    stop(paste(msg, collapse=""), call. = FALSE, domain = NA)
}

mem.limits <- function(nsize=NA, vsize=NA) .Defunct("gc")

.readhyds <- function(...) .Defunct("readhyds")
.savehyds <- function(...) .Defunct("savehyds")
.find.package <- function(...).Defunct("find.package")
.path.package <- function(...).Defunct("path.package")
