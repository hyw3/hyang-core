#  File src/library/base/hyang/debug.hyang
#  Part of the Hyang package, https://www.hyang.org
#
#  Copyright (C) 2017-2019 The Hyang Language Foundation
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

debug <- function(fun, text = "", condition = NULL, signature = NULL) {
    if(is.null(signature))
        .Internal(debug(fun, text, condition))
    else if(requireNamespace("methods"))
        methods::.debugMethod(fun, text, condition, signature, once = FALSE)
    else stop("failed to load the methods package for debugging by signature")
}

debugonce <- function(fun, text = "", condition = NULL, signature = NULL) {
    if(is.null(signature))
        .Internal(debugonce(fun, text, condition))
    else if(requireNamespace("methods"))
        methods::.debugMethod(fun, text, condition, signature, once = TRUE)
    else stop("failed to load the methods package for debugging by signature")
}

undebug <- function(fun, signature = NULL) {
    if(is.null(signature))
        .Internal(undebug(fun))
    else if(requireNamespace("methods"))
        methods::.undebugMethod(fun, signature = signature)
    else stop("failed to load methods package for undebugging by signature")
}

isdebugged <- function(fun, signature = NULL) {
    if(is.null(signature))
        .Internal(isdebugged(fun))
    else if(requireNamespace("methods"))
        methods::.isMethodDebugged(fun, signature)
    else stop("failed to load methods package for handling signature")
}

browserText <- function(n=1L) .Internal(browserText(n))
browserCondition <- function(n=1L) .Internal(browserCondition(n))
browserSetDebug <- function(n=1L) .Internal(browserSetDebug(n))

debuggingState <- function(on = NULL) .Internal(debugOnOff(on))
