#  File src/library/base/hyang/license.hyang
#  Part of the Hyang package, https://www.hyang.org
#
#  Copyright (C) 2017-2019 The Hyang Language Foundation
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.

licence <- license <- function() {
    cat("\nHyang software package is distributed under the terms of GNU\n")
    cat("General Public License, either Version 2 or Version 3.\n")
    cat("The terms of version 3 of the license are in a file called COPYING\n")
	cat("which you should have received with this software package\n")
    cat("and which can be displayed using hyangShowDoc(\"COPYING\").\n")
    cat("Version 2 can be displayed using hyangShowDoc(\"GPL-2\").\n")
    cat("\n")
    cat("You should have received copies of the licenses along with this\n")
    cat("software package. If not, see <http://www.gnu.org/licenses/>.\n")
    cat("\n")
    cat("A small number of files (the API header files listed in the\n")
    cat("HYANG_DOC_DIR/COPYRIGHTS) are distributed under the terms of\n")
    cat("LESSER GNU GENERAL PUBLIC LICENSE, version 2.1 or later.\n")
    cat("This can be displayed using hyangShowDoc(\"LGPL-2.1\").\n")
    cat("Version 3 of LGPL can be displayed using hyangShowDoc(\"LGPL-3\").\n")
    cat("\n")
    cat("'Thank you for using Hyang, and enjoy coding!'\n\n")
}
