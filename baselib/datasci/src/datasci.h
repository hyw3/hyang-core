/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_DATASCI_H
#define HYANG_DATASCI_H

/* definitions not involving SEXPs, plus _() */

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) dgettext ("datasci", String)
#else
#define _(String) (String)
#endif

#include <hyangexts/hyangscm.h>
void
F77_NAME(hclust)(int *n, int *len, int *iopt, int *ia, int *ib,
		 double *crit, double *membr, int *nn,
		 double *disnn, int *flag, double *diss);

void
F77_NAME(hcass2)(int *n, int *ia, int *ib, int *iorder, int *iia, int *iib);

void
F77_NAME(kmns)(double *a, int *m, int *n, double *c, int *k,
	       int *ic1, int *ic2, int *nc, double * an1, double *an2,
	       int *ncp, double *d, int *itran,
	       int *live, int *iter, double *wss, int *ifault);


void rcont2(int *nrow, int *ncol, int *nrowt, int *ncolt, int *ntotal,
	    double *fact, int *jwork, int *matrix);

double hyang_zeroin2(double ax, double bx, double fa, double fb, 
		 double (*f)(double x, void *info), void *info, 
		 double *Tol, int *Maxit);


#endif
