#  File src/library/datasci/hyang/nlsFunc.hyang
#  Part of the Hyang package, https://www.hyang.org
#
#  Copyright (C) 2017-2019 The Hyang Language Foundation
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.


###
###            Utility functions used with nls
###

###
### asOneSidedFormula is extracted from the GaussianEffect-1.3 library
###

asOneSidedFormula <-
  ## Converts an expression or a name or a character string
  ## to a one-sided formula
  function(object)
{
    if ((mode(object) == "call") && (object[[1L]] == "~")) {
        object <- eval(object)
    }
    if (inherits(object, "formula")) {
        if (length(object) != 2L) {
            stop(gettextf("formula '%s' must be of the form '~expr'",
                          deparse(as.vector(object))), domain = NA)
        }
        return(object)
    }
    do.call("~",
            list(switch(mode(object),
                        name = ,
                        numeric = ,
                        call = object,
                        character = as.name(object),
                        expression = object[[1L]],
                        stop(gettextf("'%s' cannot be of mode '%s'",
                                      substitute(object), mode(object)),
                             domain = NA)
                        ))
            )
}

## "FIXME": move to 'base' and make .Internal or even .Primitive
setNames <- function(object = nm, nm)
{
    names(object) <- nm
    object
}
