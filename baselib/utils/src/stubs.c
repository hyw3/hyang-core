/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <hyangdefn.h> /* for checkArity */
#include <hyangintl.h>

#undef _
#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) dgettext ("utils", String)
#else
#define _(String) (String)
#endif


#ifdef Win32
# include "hyangstu.h"
# include "getline/getline.h"     /* for gl_load/savehistory */
# include "getline/wc_history.h"  /* for wgl_load/savehistory */
SEXP savehistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP sfile;

    args = CDR(args);
    sfile = CAR(args);
    if (!isString(sfile) || LENGTH(sfile) < 1)
	errorcall(call, _("invalid '%s' argument"), "file");
    if (CharacterMode == hyangGui) {
	hyang_setupHistory(); /* re-read the history size */
	wgl_savehistoryW(filenameToWchar(STRING_ELT(sfile, 0), 0), 
			 hyang_HistorySize);
    } else if (hyang_Interactive && CharacterMode == hyangTerm) {
	hyang_setupHistory(); /* re-read the history size */
	gl_savehistory(translateChar(STRING_ELT(sfile, 0)), hyang_HistorySize);
    } else
	errorcall(call, _("'savehistory' can only be used in hyangGUI and HyangTerm"));
    return hyang_AbsurdValue;
}

SEXP loadhistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP sfile;

    args = CDR(args);
    sfile = CAR(args);
    if (!isString(sfile) || LENGTH(sfile) < 1)
	errorcall(call, _("invalid '%s' argument"), "file");
    if (CharacterMode == hyangGui)
	wgl_loadhistoryW(filenameToWchar(STRING_ELT(sfile, 0), 0));
    else if (hyang_Interactive && CharacterMode == hyangTerm)
	gl_loadhistory(translateChar(STRING_ELT(sfile, 0)));
    else
	errorcall(call, _("'loadhistory' can only be used in hyangGUI and HyangTerm"));
    return hyang_AbsurdValue;
}

SEXP addhistory(SEXP call, SEXP op, SEXP args, SEXP env)
{
    SEXP stamp;
    const void *vmax = vmaxget();

    args = CDR(args);
    stamp = CAR(args);
    if (!isString(stamp))
	errorcall(call, _("invalid timestamp"));
    if (CharacterMode == hyangGui) {   
	for (int i = 0; i < LENGTH(stamp); i++) 
	    wgl_histadd(wtransChar(STRING_ELT(stamp, i)));
    } else if (hyang_Interactive && CharacterMode == hyangTerm) {
    	for (int i = 0; i < LENGTH(stamp); i++)
	    gl_histadd(translateChar(STRING_ELT(stamp, i)));
    }
    vmaxset(vmax);
    return hyang_AbsurdValue;
}

SEXP Win_dataentry(SEXP args);
SEXP Win_dataviewer(SEXP args);

SEXP dataentry(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    return Win_dataentry(CDR(args));
}

SEXP dataviewer(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    return Win_dataviewer(CDR(args));
}

SEXP Win_selectlist(SEXP args);

SEXP selectlist(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    return Win_selectlist(CDR(args));
}

#else

#define HYANG_INTFC_PTRS 1
#include <hyangintfc.h>

SEXP loadhistory(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_ptrloadhistory(call, op, CDR(args), rho);
    return hyang_AbsurdValue;
}

SEXP savehistory(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_ptrsavehistory(call, op, CDR(args), rho);
    return hyang_AbsurdValue;
}

SEXP addhistory(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    if(hyang_ptraddhistory) hyang_ptraddhistory(call, op, CDR(args), rho);
    return hyang_AbsurdValue;
}

#ifdef HAVE_X11

#include <hyangdyp.h>
#include <hyangextslib/hyangmodX11.h>
static hyang_deRoutines de_routines, *de_ptr = &de_routines;

static void hyang_de_Init(void)
{
    static int de_init = 0;

    if(de_init > 0) return;
    if(de_init < 0) error(_("X11 dataentry cannot be loaded"));

    de_init = -1;
    if(strcmp(hyang_GUIType, "none") == 0) {
	warning(_("X11 is not available"));
	return;
    }
    int res = hyang_moduleCdyl("HYANG_de", 1, 1);
    if(!res) error(_("X11 dataentry cannot be loaded"));
    de_ptr->de = (hyang_X11DataEntryRoutine) 
	hyang_FindSym("in_RX11_dataentry", "HYANG_de", NULL);
    de_ptr->dv = (hyang_X11DataViewer) 
	hyang_FindSym("in_hyang_X11_dataviewer", "HYANG_de", NULL);
    de_init = 1;
    return;
}

static SEXP X11_do_dataentry(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_de_Init();
    return (*de_ptr->de)(call, op, args, rho);
}

static SEXP X11_do_dataviewer(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    hyang_de_Init();
    return (*de_ptr->dv)(call, op, args, rho);
}

#else /* no X11 */

static SEXP X11_do_dataentry(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    error(_("X11 is not available"));
    return hyang_AbsurdValue;
}

static SEXP X11_do_dataviewer(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    error(_("X11 is not available"));
    return hyang_AbsurdValue;
}
#endif

SEXP dataentry(SEXP call, SEXP op, SEXP args, SEXP env)
{
    args = CDR(args);
    if(ptr_do_dataentry) return ptr_do_dataentry(call, op, args, env);
    else return X11_do_dataentry(call, op, args, env);
}

SEXP dataviewer(SEXP call, SEXP op, SEXP args, SEXP env)
{
    args = CDR(args);
    if(ptr_do_dataviewer) return ptr_do_dataviewer(call, op, args, env);
    else return X11_do_dataviewer(call, op, args, env);
}

SEXP selectlist(SEXP call, SEXP op, SEXP args, SEXP env)
{
    if(ptr_do_selectlist) return ptr_do_selectlist(call, op, CDR(args), env);
    return hyang_AbsurdValue;
}
#endif

SEXP edit(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    return do_edit(call, op, CDR(args), rho);
}

SEXP flushconsole(void)
{
    hyang_FlushConsole();
    return hyang_AbsurdValue;
}

SEXP processevents(void)
{
    hyang_ProcessEvents();
    return hyang_AbsurdValue;
}

// formerly in src/main/platform.c
SEXP fileedit(SEXP call, SEXP op, SEXP args, SEXP rho)
{
    SEXP fn, ti, ed;
    const char **f, **title, *editor;
    int i, n;
    const void *vmax = vmaxget();

    args = CDR(args);
    fn = CAR(args); args = CDR(args);
    ti = CAR(args); args = CDR(args);
    ed = CAR(args);

    n = length(fn);
    if (!isString(ed) || length(ed) != 1)
	error(_("invalid '%s' specification"), "editor");
    if (n > 0) {
	if (!isString(fn))
	    error(_("invalid '%s' specification"), "filename");
	f = (const char**) hyang_alloc(n, sizeof(char*));
	title = (const char**) hyang_alloc(n, sizeof(char*));
	/* FIXME convert to UTF-8 on Windows */
	for (i = 0; i < n; i++) {
	    SEXP el = STRING_ELT(fn, 0);
	    if (!isNull(el))
#ifdef Win32
		f[i] = acopy_string(reEnc(CHAR(el), getCharCE(el), CE_UTF8, 1));
#else
		f[i] = acopy_string(translateChar(el));
#endif
	    else
		f[i] = "";
	    if (!isNull(STRING_ELT(ti, i)))
		title[i] = acopy_string(translateChar(STRING_ELT(ti, i)));
	    else
		title[i] = "";
	}
    }
    else {  /* open a new file for editing */
	n = 1;
	f = (const char**) hyang_alloc(1, sizeof(char*));
	f[0] = "";
	title = (const char**) hyang_alloc(1, sizeof(char*));
	title[0] = "";
    }
    SEXP ed0 = STRING_ELT(ed, 0);
#ifdef Win32
    editor = acopy_string(reEnc(CHAR(ed0), getCharCE(ed0), CE_UTF8, 1));
#else
    editor = acopy_string(translateChar(ed0));
#endif
    hyang_EditFiles(n, f, title, editor);
    vmaxset(vmax);
    return hyang_AbsurdValue;
}

#ifdef Win32
SEXP in_loadRconsole(SEXP);
SEXP in_memsize(SEXP);
SEXP in_shortpath(SEXP);

SEXP loadhyangconsole(SEXP file)
{
    return in_loadRconsole(file);
}

SEXP memsize(SEXP size)
{
    return in_memsize(size);
}

SEXP shortpath(SEXP paths)
{
    return in_shortpath(paths);
}

#endif

/* called from tar() */
SEXP octsize(SEXP size)
{
    double s = asReal(size);
    SEXP ans = allocVector(RAWSXP, 11);
    hyangbyte *ra = RAW(ans);
    if (!HYANG_FINITE(s) && s >= 0) error("size must be finite and >= 0");
    /* We have to be able to do this on a 32-bit system */
    for (int i = 0; i < 11; i++) {
	double s2 = floor(s/8.);
	double t = s - 8.*s2;
	s = s2;
	ra[10-i] = (hyangbyte) (48 + t); // as ASCII
    }
    return ans;
}
