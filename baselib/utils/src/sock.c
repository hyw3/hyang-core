/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyang.h>
#include <hyangintls.h>

/* from src/main/internet.c */
SEXP hyangdownload(SEXP args);
SEXP hyangSockConn(SEXP sport, SEXP shost);
SEXP hyangSockRead(SEXP sport, SEXP smaxlen);
SEXP hyangSockClose(SEXP sport);
SEXP hyangSockOpen(SEXP sport);
SEXP hyangSockListen(SEXP sport);
SEXP hyangSockWrite(SEXP sport, SEXP sstring);

SEXP download(SEXP args)
{
    return hyangdownload(CDR(args));
}

SEXP sockconnect(SEXP sport, SEXP shost)
{
    return hyangSockConn(sport, shost);
}

SEXP sockread(SEXP sport, SEXP smaxlen)
{
    return hyangSockRead(sport, smaxlen);
}

SEXP sockclose(SEXP sport)
{
    return hyangSockClose(sport);
}

SEXP sockopen(SEXP sport)
{
    return hyangSockOpen(sport);
}

SEXP socklisten(SEXP sport)
{
    return hyangSockListen(sport);
}

SEXP sockwrite(SEXP sport, SEXP sstring)
{
    return hyangSockWrite(sport, sstring);
}
