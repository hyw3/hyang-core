/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef HYANG_HYANGSMETHODS_H
#define HYANG_HYANGSMETHODS_H

SEXP hyang_initialize_methods_metadata(SEXP table);
SEXP hyang_get_from_method_metadata(SEXP name);
SEXP hyang_assign_to_method_metadata(SEXP name, SEXP value);

SEXP hyang_methods_list_dispatch(SEXP fname, SEXP ev, SEXP must_find);

SEXP hyang_stdrGen(SEXP fname, SEXP ev, SEXP fdef);
SEXP hyang_dispatchGeneric(SEXP fname, SEXP ev, SEXP fdef);
SEXP hyang_quick_dispatch(SEXP args, SEXP mtable, SEXP fdef);

#endif   /* HYANG_HYANGSMETHODS_H */
