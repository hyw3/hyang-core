/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyangintls.h>
#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) dgettext ("methods", String)
#else
#define _(String) (String)
#endif

#define DUPLICATE_CLASS_CASE(method) TYPEOF(method) == ENVSXP


SEXP hyang_M_setPrimitiveMethods(SEXP fname, SEXP op, SEXP code_vec,
			     SEXP fundef, SEXP mlist);
SEXP hyang_clear_method_selection();
SEXP NORET hyang_dummy_extern_place();
SEXP hyang_el_named(SEXP object, SEXP what);
SEXP hyang_externalptr_prototype_object();
SEXP hyang_getGeneric(SEXP name, SEXP mustFind, SEXP env, SEXP package);
SEXP hyang_get_slot(SEXP obj, SEXP name);
SEXP hyang_getClassFromCache(SEXP class, SEXP table);
SEXP hyang_hasSlot(SEXP obj, SEXP name);
SEXP hyang_identC(SEXP e1, SEXP e2);
SEXP hyang_initMethodDispatch(SEXP envir);
SEXP hyang_methodsPackageMetaName(SEXP prefix, SEXP name, SEXP pkg);
SEXP hyang_methods_test_MAKE_CLASS(SEXP className);
SEXP hyang_methods_test_NEW(SEXP className);
SEXP hyang_missingArg(SEXP symbol, SEXP ev);
SEXP hyang_nextMethodCall(SEXP matched_call, SEXP ev);
SEXP hyang_quick_method_check(SEXP args, SEXP mlist, SEXP fdef);
SEXP hyang_selectMethod(SEXP fname, SEXP ev, SEXP mlist, SEXP evalArgs);
SEXP hyang_set_el_named(SEXP object, SEXP what, SEXP value);
SEXP hyang_set_slot(SEXP obj, SEXP name, SEXP value);
SEXP hyang_stdrGen(SEXP fname, SEXP ev, SEXP fdef);
SEXP do_substitute_direct(SEXP f, SEXP env);
SEXP hyangly_allocS4Obj();
SEXP hyang_set_method_dispatch(SEXP onOff);
SEXP hyang_get_primname(SEXP object);
SEXP new_object(SEXP class_def);
