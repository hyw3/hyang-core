/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyang.h>
#include <hyangintls.h>

#include "methods.h"
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangvis.h>

#define CALLDEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_CallMethodDef CallEntries[] = {
    CALLDEF(hyang_M_setPrimitiveMethods, 5),
    CALLDEF(hyang_clear_method_selection, 0),
    CALLDEF(hyang_dummy_extern_place, 0),
    CALLDEF(hyang_el_named, 2),
    CALLDEF(hyang_externalptr_prototype_object, 0),
    CALLDEF(hyang_getClassFromCache, 2),
    CALLDEF(hyang_getGeneric, 4),
    CALLDEF(hyang_get_slot, 2),
    CALLDEF(hyang_hasSlot, 2),
    CALLDEF(hyang_identC, 2),
    CALLDEF(hyang_initMethodDispatch, 1),
    CALLDEF(hyang_methodsPackageMetaName, 3),
    CALLDEF(hyang_methods_test_MAKE_CLASS, 1),
    CALLDEF(hyang_methods_test_NEW, 1),
    CALLDEF(hyang_missingArg, 2),
    CALLDEF(hyang_nextMethodCall, 2),
    CALLDEF(hyang_quick_method_check, 3),
    CALLDEF(hyang_selectMethod, 4),
    CALLDEF(hyang_set_el_named, 3),
    CALLDEF(hyang_set_slot, 3),
    CALLDEF(hyang_stdrGen, 3),
    CALLDEF(do_substitute_direct, 2),
    CALLDEF(hyangly_allocS4Obj, 0),
    CALLDEF(hyang_set_method_dispatch, 1),
    CALLDEF(hyang_get_primname, 1),
    CALLDEF(new_object, 1),
    {NULL, NULL, 0}
};

void attribute_visible
hyang_init_methods(DllInfo *dll)
{
    hyang_regRoutines(dll, NULL, CallEntries, NULL, NULL);
    hyang_useDynSyms(dll, FALSE);
    hyang_forceSyms(dll, TRUE);
}
