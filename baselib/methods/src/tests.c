/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyang.h>
#include <hyangdefines.h>
#include "methods.h"

SEXP hyang_methods_test_MAKE_CLASS(SEXP className)
{
  SEXP classNameChar = PROTECT(asChar(className));
  const char *class;
  class = CHAR(classNameChar);
  SEXP res = MAKE_CLASS(class);
  UNPROTECT(1);
  return res;
}

SEXP hyang_methods_test_NEW(SEXP className)
{
  SEXP classNameChar = PROTECT(asChar(className));
  const char *class;
  class = CHAR(classNameChar);
  SEXP clDef;
  PROTECT(clDef = MAKE_CLASS(class));
  SEXP res = NEW_OBJECT(clDef);
  UNPROTECT(2);
  return res;
}
