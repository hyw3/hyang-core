/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <hyang.h>
#include <hyangintls.h>
#include "tcltk.h"
#include <hyangexts/hyangdyl.h>
#include <hyangexts/hyangvis.h>

#define C_DEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_CMethodDef CEntries[] = {
#ifdef Win32
    C_DEF(tcltk_start, 0),
    C_DEF(tcltk_end, 0),
#else
    C_DEF(tcltk_init, 1),
    C_DEF(hyang_Tcl_ActivateConsole, 0),
#endif
    {NULL, NULL, 0}
};

#define EXTDEF(name, n)  {#name, (DL_FUNC) &name, n}

static const hyang_ExternalMethodDef ExternEntries[] = {
    EXTDEF(dotTcl, -1),
    EXTDEF(dotTclObjv, 1),
    EXTDEF(dotTclcallback, -1),
    EXTDEF(hyang_Tcl_ObjFromVar, 1),
    EXTDEF(hyang_Tcl_AssignObjToVar, 2),
    EXTDEF(hyang_Tcl_StringFromObj, 1),
    EXTDEF(hyang_Tcl_ObjAsCharVector, 1),
    EXTDEF(hyang_Tcl_ObjAsDoubleVector, 1),
    EXTDEF(hyang_Tcl_ObjAsIntVector, 1),
    EXTDEF(hyang_Tcl_ObjAsRawVector, 1),
    EXTDEF(hyang_Tcl_ObjFromCharVector, 2),
    EXTDEF(hyang_Tcl_ObjFromDoubleVector, 2),
    EXTDEF(hyang_Tcl_ObjFromIntVector, 2),
    EXTDEF(hyang_Tcl_ObjFromRawVector, 1),
    /* (..FromRaw... has only 1 arg, no drop=) */
    EXTDEF(hyang_Tcl_ServiceMode, 1),
    EXTDEF(hyang_Tcl_GetArrayElem, 2),
    EXTDEF(hyang_Tcl_RemoveArrayElem, 2),
    EXTDEF(hyang_Tcl_SetArrayElem, 3),
    {NULL, NULL, 0}
};


void attribute_visible hyang_init_tcltk(DllInfo *dll)
{
    hyang_regRoutines(dll, CEntries, NULL, NULL, ExternEntries);
    hyang_useDynSyms(dll, FALSE);
    hyang_forceSyms(dll, FALSE);
}

