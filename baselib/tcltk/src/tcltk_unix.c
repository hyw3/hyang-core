/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifdef HAVE_CONFIG_H
# include <config.h>
#endif
#define NO_NLS
#include <hyangdefn.h>
#include "tcltk.h" /* declarations of our `public' interface */

#ifndef Win32
#include <hyangexts/hyangloop.h>
#endif

#include <stdlib.h>

#ifdef ENABLE_NLS
#include <libintl.h>
#define _(String) dgettext ("tcltk", String)
#else
#define _(String) (String)
#endif

/* Hyang event structure */
typedef struct {
    Tcl_EventProc *proc;
    struct Tcl_Event *nextPtr;
} hyang_Tcl_Event;



#define HYANG_INTFC_PTRS 1
#include <hyangintfc.h>  /* hyang_GUIType and more for console */
/* Add/delete Tcl/Tk event handler */

static void (* OldHandler)(void);
static int OldRwait;
static int Tcl_loaded = 0;
static int Tcl_lock = 0; /* reentrancy guard */

static void TclSpinLoop(void *data)
{
    /* Tcl_ServiceAll is not enough here, for reasons that escape me */
    while (Tcl_DoOneEvent(TCL_DONT_WAIT)) ;
}

//extern hyangboolean hyang_isForkedChild;
static void TclHandler(void)
{
    if (!hyang_isForkedChild && !Tcl_lock 
	&& Tcl_GetServiceMode() != TCL_SERVICE_NONE) {
	Tcl_lock = 1;
	(void) hyang_ToplevelExec(TclSpinLoop, NULL);
	Tcl_lock = 0;
    }
    OldHandler();
}

static void addTcl(void)
{
    if (Tcl_loaded) return; // error(_("Tcl already loaded"));
    Tcl_loaded = 1;
    OldHandler = hyang_PolledEvents;
    OldRwait = hyang_waitUsec;
    hyang_PolledEvents = TclHandler;
    if ( hyang_waitUsec > 10000 || hyang_waitUsec == 0) hyang_waitUsec = 10000;
}

#ifdef UNUSED
/* Note that although this cleans up Hyang's event loop, it does not attempt
   to clean up Tcl's, to which Tcl_unix_setup added an event source.
   We could call Tcl_DeleteEventSource.
   
   But for now un/re-loading the interpreter seems to cause crashes.
*/
void delTcl(void)
{
    if (!Tcl_loaded) error(_("Tcl is not loaded"));
    Tcl_DeleteInterp(hyang_Tcl_interp);
    Tcl_Finalize();
    if (hyang_PolledEvents != TclHandler)
	error(_("Tcl is not last loaded handler"));
    hyang_PolledEvents = OldHandler;
    hyang_waitUsec = OldRwait;
    Tcl_loaded = 0;
}
#endif

/* ----- Event loop interface routines -------- */
static Tcl_Time timeout;

static void hyang_Tcl_setupProc(ClientData clientData, int flags)
{
    Tcl_SetMaxBlockTime(&timeout);
}
static void hyang_Tcl_eventProc(hyang_Tcl_Event *evPtr, int flags)
{
    fd_set *readMask = hyang_checkActivity(0 /*usec*/, 1 /*ignore_stdin*/);

    if (readMask==NULL)
	return;

    hyang_runHandlers(hyang_InputHandlers, readMask);
}
static void hyang_Tcl_checkProc(ClientData clientData, int flags)
{
    fd_set *readMask = hyang_checkActivity(0 /*usec*/, 1 /*ignore_stdin*/);
    hyang_Tcl_Event * evPtr;
    if (readMask == NULL)
	return;

    evPtr = (hyang_Tcl_Event*) Tcl_Alloc(sizeof(hyang_Tcl_Event));
    evPtr->proc = (Tcl_EventProc*) hyang_Tcl_eventProc;

    Tcl_QueueEvent((Tcl_Event*) evPtr, TCL_QUEUE_HEAD);
}


void Tcl_unix_setup(void)
{
    addTcl(); /* notice: this sets hyang_waitUsec.... */
    timeout.sec = 0;
    timeout.usec = hyang_waitUsec;
    Tcl_CreateEventSource(hyang_Tcl_setupProc, hyang_Tcl_checkProc, 0);
}



/* ----- Tcl/Tk console routines ----- */


/* Fill a text buffer with user typed console input. */

static int
hyang_Tcl_ReadConsole (const char *prompt, unsigned char *buf, int len,
		  int addtohistory)
{
    Tcl_Obj *cmd[3];
    int i, code;

    cmd[0] = Tcl_NewStringObj("hyangc_read", -1);
    cmd[1] = Tcl_NewStringObj(prompt, -1);
    cmd[2] = Tcl_NewIntObj(addtohistory);

    for (i = 0 ; i < 3 ; i++)
	Tcl_IncrRefCount(cmd[i]);

    code = Tcl_EvalObjv(hyang_Tcl_interp, 3, cmd, 0);
    if (code != TCL_OK)
	return 0;
    else {
	    char *buf_utf8;
	    Tcl_DString buf_utf8_ds;
	    Tcl_DStringInit(&buf_utf8_ds);
	    buf_utf8 =
		    Tcl_UtfToExternalDString(NULL,
		    			     Tcl_GetStringResult(hyang_Tcl_interp),
					     len,
					     &buf_utf8_ds);
            strncpy((char *)buf, buf_utf8, len);
	    Tcl_DStringFree(&buf_utf8_ds);
    }

    /* At some point we need to figure out what to do if the result is
     * longer than "len"... For now, just truncate. */

    for (i = 0 ; i < 3 ; i++)
	Tcl_DecrRefCount(cmd[i]);

    return 1;
}

/* Write a text buffer to the console. */
/* All system output is filtered through this routine. */
static void
hyang_Tcl_WriteConsole (const char *buf, int len)
{
    Tcl_Obj *cmd[2];
    char *buf_utf8;
    Tcl_DString  buf_utf8_ds;

    Tcl_DStringInit(&buf_utf8_ds);
    buf_utf8 = Tcl_ExternalToUtfDString(NULL, buf, -1, &buf_utf8_ds);

    /* Construct command */
    cmd[0] = Tcl_NewStringObj("hyangc_write", -1);
    cmd[1] = Tcl_NewStringObj(buf_utf8, -1);

    Tcl_IncrRefCount(cmd[0]);
    Tcl_IncrRefCount(cmd[1]);

    Tcl_EvalObjv(hyang_Tcl_interp, 2, cmd, 0);

    Tcl_DecrRefCount(cmd[0]);
    Tcl_DecrRefCount(cmd[1]);
    Tcl_DStringFree(&buf_utf8_ds);
}

/* Indicate that input is coming from the console */
static void
hyang_Tcl_ResetConsole (void)
{
}

/* Stdio support to ensure the console file buffer is flushed */
static void
hyang_Tcl_FlushConsole (void)
{
}


/* Reset stdin if the user types EOF on the console. */
static void
hyang_Tcl_ClearerrConsole (void)
{
}

void hyang_Tcl_ActivateConsole (void)
{
    hyang_ptrReadConsole = hyang_Tcl_ReadConsole;
    hyang_ptrWriteConsole = hyang_Tcl_WriteConsole;
    hyang_ptrResetConsole = hyang_Tcl_ResetConsole;
    hyang_ptrFlushConsole = hyang_Tcl_FlushConsole;
    hyang_ptrClearerrConsole = hyang_Tcl_ClearerrConsole;
    hyang_Consolefile = NULL;
    hyang_Outputfile = NULL;
}
