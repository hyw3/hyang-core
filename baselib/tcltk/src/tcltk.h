/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/* Ouch! - do we *really* need this? */
#ifndef _GNU_SOURCE
# define _GNU_SOURCE
#endif

#include <tcl.h>
#include <stdio.h>
#include <string.h>

#include <hyangintls.h>
#include <hyangexts/hyangextparse.h>

#ifndef Win32
/* From tcltk_unix.c */
void Tcl_unix_setup(void);
#endif

/* Globals exported from  ./tcltk.c : */

Tcl_Interp *hyang_Tcl_interp;      /* Interpreter for this application. */
void tcltk_init(int *);

SEXP dotTcl(SEXP args);
SEXP dotTclObjv(SEXP args);
SEXP dotTclcallback(SEXP args);

/* Used by .C */

#ifdef Win32
void tcltk_start(void);
void tcltk_end(void);
#else
# ifdef UNUSED
void delTcl(void);
# endif
void hyang_Tcl_ActivateConsole(void);
#endif

/* Used by .Extern */

SEXP hyang_Tcl_ObjFromVar(SEXP args);
SEXP hyang_Tcl_AssignObjToVar(SEXP args);
SEXP hyang_Tcl_StringFromObj(SEXP args);
SEXP hyang_Tcl_ObjAsCharVector(SEXP args);
SEXP hyang_Tcl_ObjAsDoubleVector(SEXP args);
SEXP hyang_Tcl_ObjAsIntVector(SEXP args);
SEXP hyang_Tcl_ObjAsRawVector(SEXP args);
SEXP hyang_Tcl_ObjFromCharVector(SEXP args);
SEXP hyang_Tcl_ObjFromDoubleVector(SEXP args);
SEXP hyang_Tcl_ObjFromIntVector(SEXP args);
SEXP hyang_Tcl_ObjFromRawVector(SEXP args);
SEXP hyang_Tcl_GetArrayElem(SEXP args);
SEXP hyang_Tcl_SetArrayElem(SEXP args);
SEXP hyang_Tcl_RemoveArrayElem(SEXP args);
SEXP hyang_Tcl_ServiceMode(SEXP args);
