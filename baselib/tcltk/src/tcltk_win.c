/* Hyang Programming Language
 *
 * Copyright (C) 2017-2019 Hilman P. Alisabana <alisabana@hyang.org>
 * Copyright (C) 2017-2019 Hyang Language Foundation, Jakarta - Indonesia
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tcltk.h"

#include <hyangexts/hyangboolean.h>
#define WIN32_LEAN_AND_MEAN 1
#include <windows.h>

hyangboolean hyang_ToplevelExec(void (*fun)(void *), void *data);

/* We don't need a re-entrancy guard on Windows as Tcl_ServiceAll
   has one -- it uses serviceMode, setting it to TCL_SERVICE_NONE
   whilst it is running.  Unlike TclDoOneEvent, it checks on entry.
 */
static void TclSpinLoop(void *data)
{
    Tcl_ServiceAll();
}

static void _hyang_tcldo(void)
{
    (void) hyang_ToplevelExec(TclSpinLoop, NULL);
}

/* import from src/hyangwin32/system.c -- private, so in no header */
typedef void (*DO_FUNC)();
extern void hyangto_setTcl(DO_FUNC ptr);
extern void hyangto_unsetTcl(DO_FUNC ptr);
static int TkUp = 0, Tcl_polled = 0;


void tcltk_start(void)
{
    HWND active = GetForegroundWindow(); /* ActiveTCL steals the focus */

    if(!TkUp) tcltk_init(&TkUp); /* won't return on error */
    if (!Tcl_polled) {
	hyangto_setTcl(&_hyang_tcldo);
	Tcl_polled = 1;
    }
    _hyang_tcldo();  /* one call to trigger the focus stealing bug */
    SetForegroundWindow(active); /* and fix it */
}

void tcltk_end(void)
{
//    Tcl_DeleteInterp(hyang_Tcl_interp);
//    Tcl_Finalize();
    hyangto_unsetTcl(&_hyang_tcldo);
    Tcl_polled = 0;
//    TkUp = 0;
}
