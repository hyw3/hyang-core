menu .menu
toplevel .tk-hyang -menu .menu
wm protocol .tk-hyang WM_DELETE_WINDOW {}
pack [frame .tk-hyang.toolbar] -anchor n -fill x
pack [text .tk-hyang.term -bg white -font [list Courier 14]] -expand true -fill both

## Implements a "stop button" which sends SIGINT to the Hyang process.
## Unfortunately, SIGINTs are not handled gracefully...
# pack [frame .tk-hyang.toolbar.stop -container true] -side right
# set stopscript $env(HYANG_HOME)/library/tcltk/exec/stopit.tcl 
# update
# exec wish -use [winfo id .tk-hyang.toolbar.stop] < $stopscript [pid] &

.tk-hyang.term mark set insert-mark "end - 1 chars"
focus .tk-hyang.term

set hist {}
set nhist 0
set saved {}

bind .tk-hyang.term <Return> {
    .tk-hyang.term see end
    .tk-hyang.term insert end "\n"
    .tk-hyang.term mark set insert-mark "end - 1 chars"
    .tk-hyang.term mark gravity insert-mark right
    set terminput [.tk-hyang.term get process-mark "end - 1 chars"]
    break
}

bind .tk-hyang.term <Up> {
    global hist phist nhist saved
    if ($phist<=0) break
    if ($phist==$nhist) {
        set saved  [.tk-hyang.term get process-mark "end - 1 chars"]
    }
    .tk-hyang.term delete process-mark "end - 1 chars"
    incr phist -1
    .tk-hyang.term insert process-mark [lindex $hist $phist]
    break
}

bind .tk-hyang.term <Down> {
    global hist phist nhist saved
    if ($phist>=$nhist) break
    .tk-hyang.term delete process-mark "end - 1 chars"
    incr phist 
    if ($phist<$nhist) {
	.tk-hyang.term insert process-mark [lindex $hist $phist]
    } else {
	.tk-hyang.term insert process-mark $saved
    }
    break
}

proc hyangc_read { prompt addtohistory } {
    global terminput hist nhist phist
    .tk-hyang.term mark set insert-mark "end - 1 chars"
    .tk-hyang.term mark gravity insert-mark left
    .tk-hyang.term insert insert-mark $prompt
    .tk-hyang.term mark gravity insert-mark right
    .tk-hyang.term mark set process-mark "end - 1 chars"
    .tk-hyang.term mark gravity process-mark left
    .tk-hyang.term see end
    set phist $nhist
    tkwait variable terminput
    .tk-hyang.term mark set insert end
    if ($addtohistory) then {
	lappend hist [string trimright $terminput]
	incr nhist
    }
    return $terminput
}

proc hyangc_write { buf } {
    .tk-hyang.term insert insert-mark $buf
    .tk-hyang.term see end
    #.tk-hyang.term mark set insert end
    #update
}
